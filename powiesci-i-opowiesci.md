---
id: 88
title: 'Powieści, opowieści i wiersze'
date: '2014-09-21T13:16:57+02:00'
author: 'Adam Boruta'
layout: page
guid: 'http://adamboruta.pl/?page_id=88'
interface_sidebarlayout:
    - default
---

<div class="wp-caption aligncenter" id="attachment_23" style="width: 138px">[![Przypadki magistra N.](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/Przypadki-magistra-N.2.pdf)Przypadki magistra N.

</div><div class="wp-caption aligncenter" id="attachment_23" style="width: 138px">[![Kącik poezji komorniczej](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/Kącik-poezji-komorniczej.pdf)Kącik poezji komorniczej

</div><div class="wp-caption aligncenter" id="attachment_23" style="width: 138px">[![Poezja sanitarna czeladzi Warczysławowej](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/Poezja-sanitarna-czeladzi-Warczysławowej.pdf)Poezja sanitarna czeladzi Warczysławowej

</div><div class="wp-caption aligncenter" id="attachment_23" style="width: 138px">[![Kronika Chichrały. Księga I](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/Kronika-Chichrały.-Księga-I.pdf)Kronika Chichrały.   
Księga I

</div>