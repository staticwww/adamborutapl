---
id: 570
title: 'Nowy rok &#8211; nowe porządki'
date: '2016-02-07T19:19:35+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2016/02/565-autosave-v1/'
permalink: '/?p=570'
---

Nowy rok – nowe porządki.

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/nowy-rok-nowe-porządki.pdf)

Na tej stronie internetowej możemy wreszcie odnotować zmiany ponoworoczne: udało się wytępić wirusa – skutecznie miejmy nadzieję. Będzie strona nudzić bez przeszkód.

\*

No to do zmian. Jak tam po Sylwestrze? Jak zwykle sprzątanie i koszta.

Choinka do śmieci, śmieci pod dywan, dywan w zastaw. Zastawiliście stół z gestem, zastawcie dywan za bezcen. Stołu zastawić się nie opłaci, bo skoro się pod dobrem ugiął, to jest wykrzywiony jak lichwiarskie oblicze byłoby, gdybyście go przed nim postawili. Zastawcie już lepiej dywan, a wtedy stół wam się ostanie na lichwiarskiego oblicza obraz i podobieństwo, więc kiedy najdzie ochota, ulżycie sobie, waląc go piąchą w blat; na dowód, że posiadacie minimum kultury, smagnijcie go czasem polubownie z liścia; broń Boże nie dźgajcie widelcem, bo to już was postawi poza etosem.

Mogą być i pozafinansowe koszta ponoworoczne. Jak mawia słusznie pan Zdzicho, to, czym się zastawiło, się na wątrobie odbiło, więc i wątroba do wymiany. Póki brak zamiennika, Pan Zdzicho wątrobę radzi konserwować, najlepiej bez zagrychy, żeby nie prowokować kolejnej spirali zadłużenia.

Słowem, nowy rok, to nowe porządki. Zmiana.

\*

Dominujące politycznie autorytety głoszą, że tegoroczna zmiana nie może być byle jaka; ta zmiana musi być dobra. Zastawianie i konserwowanie są przeciw Dobrej Zmianie, moi mili i ty, panie Zdzichu.

A jeśli wasze postanowienia noworoczne nie są częścią Dobrej Zmiany, potępcie je i porzućcie, nieszczęśni, o ile zachowaliście kompasy moralne;

jeśli wasze tęsknoty są z Dobrą Zmianą zgodne, to machnijcie na nie rękami i porzućcie, o ile zachowaliście szczyptę zdrowego samokrytycyzmu, bo pożyteczniej chyba przyłączyć się do Wielkiego Ruchu, niż dziergać samemu chałupniczo;

jeśli wasze plany mają się do Dobrej Zmiany jak piernik do wiatraka, wyśmiejcie je i porzućcie, o ile zachowaliście poprawne hierarchie wartości – chyba że od dobrych czynów wolicie zajmowanie się pierdołami.

Brak moralnego kompasu, uwiąd samokrytycyzmu i kult pierdół mogą was wpędzić w objęcia Złej Restauracji, nieszczęśni. Chcecie tłoczyć się na ulicach w zimnie i dreszczach, w skwarze i pocie czoła, na wietrze i z łupaniem w kościach, ujadając przeciw Dobrej Zmianie i podskakując Dobrej Władzy? Taki jest właśnie smętny los miłośników Złej Restauracji!

Są wypowiedzi autorytetów duchowych sugerujące, że Dobra Zmiana jest dobra również w sensie teologicznym. Skoro tak, to Zła Restauracja, która się Dobrej Zmianie przeciwi, zapewne jest zła w tym samym sensie, a zatem grzeszna. Grzeszność restauracji w ogóle jest traktowana jako pewnik w potocznej wiedzy dewocyjnej. Intuicja podpowiada, że naukowy dowód grzeszności Złej Restauracji byłby dość łatwy do przeprowadzenia.

Łatwo wskazać na twierdzenia pomocnicze do wykorzystania w tym dowodzie. Choćby takie, że urzeczywistniona Dobra Zmiana zaprowadza raj na ziemi. W tym raju wiekowi emeryci dzięki darmowym lekom będą zdolni uczestniczyć w procesie wychowywania nowych pokoleń. Dzięki temu symbolicznie opodatkowani ich potomkowie z przyjemnością będą mogli przysparzać obywateli ojczyźnie, a swoim rodzinom dokładać kolejne dotacje pięćsetzłotowe. Co rok podwyżka. Dzieci chętnie podzielą się dochodem z rodzicami. Kto wie, może oszczędności pozwolą tatusiom i mamusiom w dobrej kondycji kontynuować wysiłki przysparzające również po przejściu na wczesną emeryturę. Szczęśliwi górnicy od poniedziałku do środy będą wykopywać węgiel, a w czwartki i piątki zakopywać go z powrotem, żeby się niepotrzebnie nie utleniał. Raj, jak widać, cudem stoi. Wydaje się słuszne założenie, że co cudowne, to nie grzeszne, nieprawdaż?

Pan Henio wprowadza zamęt w te subtelne rozważania. Twierdzi, że może dowieść istnienia Dobrej Restauracji. Wskazuje na swój bar.