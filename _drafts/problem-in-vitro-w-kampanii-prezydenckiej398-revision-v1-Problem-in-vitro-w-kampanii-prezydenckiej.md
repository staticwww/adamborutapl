---
id: 402
title: 'Problem in vitro w kampanii prezydenckiej'
date: '2015-05-17T23:45:27+02:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2015/05/398-revision-v1/'
permalink: '/?p=402'
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/05/Problem-in-vitro-w-kampanii-prezydenckiej.pdf)

PROBLEM IN VITRO W KAMPANII PREZYDENCKIEJ

Kampania prezydencka toczy się w lokalu „U Henia” już od pewnego czasu, lecz małżonka pana Zdzicha pojawiła się na forum po raz pierwszy. Wrodzona nieśmiałość hamowała jej kroki i było widoczne, że dotarła na miejsce przy stoliku do spraw in vitro tylko dzięki krzepkiej prawicy małżonka.  
Dyskusję zagaił pan Henio, restaurator ze specjalizacją w zakresie wyszynku. Poddał on wątpliwość powszechnie zakładaną opozycję pomiędzy poczęciem naturalnym a poczęciem in vitro. Przypomniał, iż żargon naukowy, w tym medyczny, pospolicie bazuje na łacinie, a in vitro” po łacinie to tyle co po polsku „w szkle”, choć w kontekście poczęcia lepiej byłoby tłumaczyć „za pomocą szkła”, „z udziałem szkła” albo „poprzez szkło”. Otóż elementarne doświadczenie restauratora podpowiada, że inicjowanie prokreacji poprzez szkło jest jak najbardziej naturalnym i uświęconym przez tradycję źródłem przyrostu naturalnego.  
Pan Jacuś, znawca architektury policyjnej, wyraził szczerą wątpliwość, czy kadra naukowa jest jakoś szczególnie biegła w posługiwaniu się łaciną i tu dostrzegł powód niefortunnego przeciwstawienia in vitro naturze. Bardziej naturalne wydaje się przecież odróżnianie „in vitro” od „w puszce”.  
Pan Witek, przedstawiciel nurtu karaoke a capella dość popularnego w lokalu „U Henia”, włączył się do dyskusji cytatem „…ja się mogę wstępnie upić, bo to dobre jest na śmiałość…”; w ten sposób dowiódł, iż polskim twórcom kultury wysokiej idea zapłodnienia in vitro od dawna nie jest obca. Kiedy kolega pana Witka rozszerzył jego argumentację luźnym cytatem z twórczości biesiadnej „…więc siup, malutka i zrobimy krasnoludka…”, stało się jasne, iż również podstolny nurt kultury niskiej traktuje zapłodnienie in vitro jako normę obyczajową.

*WIEŚCI Z FRONTU*

*Ogłoszono raport, z którego wynika, że komisja złożona z pana Zenka, dzielnicowego i pana Władka, kibica, dysponuje niepodważalnymi dowodami na to, iż ręka zaciśnięta na małżonce pana Zdzicha należy do pana Zdzicha. Mają o tym świadczyć odciski palców ręki zidentyfikowane dzięki bazie danych policji jako należące do Zdzisława R., uczestnika nieporozumienia towarzyskiego ze skutkiem szpitalnym, które to nieporozumienie miało miejsce w lokalu „U Henia” w roku 1987. W konkluzji raportu stwierdzono, że w rozpętanym przez pana Zdzicha konflikcie status strony uciskanej przyznać należy małżonce pana Zdzicha.*  
*W specjalnym oświadczeniu pan Zdzicho zdementował ustalenia w/w raportu. W szczególności zaprzeczył, by ręka zaciśnięta na małżonce była jego ręką. Zwrócił uwagę opinii publicznej na zeznania Jasia, potomka obu stron konfliktu, który to Jasio odmówił rozpoznania ręki zaciśniętej na mamusi jako kończyny należącej do tatusia. Odmowa Jasia została stronniczo pominięta w raporcie – podkreślił pan Zdzicho. W dalszym ciągu oświadczenia pan Zdzicho podważył kompetencje pana Zenka, dzielnicowego, nazywając niewiarygodnym jego zapewnienie, iż z należytą starannością pobrał on odciski palców z inkryminowanej ręki. Nie da się uzyskać dobrej jakości odcisków palców z zaciśniętych dłoni – stwierdził z naciskiem, podpierając się ekspertyzą pana Jacusia, stałego bywalca aresztów policyjnych.*