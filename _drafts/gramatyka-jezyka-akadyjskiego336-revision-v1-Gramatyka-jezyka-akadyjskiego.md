---
id: 338
title: 'Gramatyka języka akadyjskiego'
date: '2014-12-29T21:19:15+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/12/336-revision-v1/'
permalink: '/?p=338'
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/12/3.-Gramatyka-języka-akadyjskiego.pdf)

GRAMATYKA JĘZYKA AKADYJSKIEGO

Tłumaczenie z „Lehrbuch des Akkadischen” von Dr Kaspar K. Riemschneider, VEB Ver-lag Enzyklopädie – Leipzig 1969  
WPROWADZENIE  
Miejsce języka akadyjskiego wśród języków semickich i jego stosunek do języka sumeryjskiego

0.1. Języki semickie.  
Akadyjski jest najstarszym zaświadczonym językiem semickim, przeciwstawianym jako (północno-)wschodniosemicki językom zachodniosemickim. Języki zachodniosemickie dzielone są na dwie grupy: północną z językami aramejskim oraz kaananejskimi (ugaryckim, amoryckim, hebrajskim, fenickim) i południową z arabskim i etiopskim.

0.2. Język akadyjski.  
Zabytki pisane zaświadczają, że akadyjski używany był w Mezopotamii od około połowy III tysiąclecia p.n.e. do I tysiąclecia n.e. Jego nazwa pochodzi od miasta Akkade, stolicy państwa Sargona (2340-2284 p.n.e.). Pierwotna, czysto semicka forma tego języka nie jest znana, bowiem już najstarsze teksty zdradzają wpływ niesemickiego języka Sumerów.

0.3. Sumerowie i Akadowie.  
Sumerowie stworzyli ok. 3000 r. p.n.e. najstarszą wyżej rozwiniętą kulturę mezo-potamską. Zasiedlili głównie południową i środkową Babilonię pomiędzy rzekami Eu-fratem i Tygrysem od Zatoki Perskiej po dzisiejszy Bagdad. Od około 2600 r. p.n.e. zaczęły tu przenikać gromady koczowniczych Akadów, którzy wzorując się na Sumerach, stopniowo przyjmowali ich osiadły tryb życia i kulturę. Mieszanie się obu ludów zaowocowało powolną asymilacją Sumerów przez Akadów. Język sumeryjski był już od ok. 1900 r. p.n.e. martwy, jednakże nadal, przez całe jeszcze wieki służył obok akadyjskiego jako język literacki i kultowy.

0.4. Języki akadyjski i sumeryjski.  
W czasie, kiedy Akadowie uzależnieni byli kulturowo od Sumerów, powolne mie-szanie się obu ludów sprzyjało wywarciu przez sumeryjski silnych wpływów na mowę akadyjską. Wpływy te widać w licznych zapożyczeniach, zwłaszcza zaś w przechodzeniu do języka akadyjskiego wyrazów związanych z sumeryjskimi zdobyczami kulturalnymi; chodzi o takie słowa jak np. tuppum „tabliczka gliniana” pochodzące od sumeryjskiego dub, czy ekallum „pałac” – od é-gal „wielki dom”. Prasemicka organizacja dźwięków w dużym stopniu w języku akadyjskim zaniknęła. Pewne doniosłe właściwości syntaktyczne akadyjskiego dają się objaśnić nie dzięki odniesieniu ich do innych języków semickich, lecz do języka sumeryjskiego. Fakt umieszczania orzeczenia na końcu zdań akadyjskich wytłumaczyć można tylko wpływem sumeryjskiego.  
Fazy rozwojowe i dialekty języka

0.5. Rozwój języka akadyjskiego.  
W okresie dwóch i pół tysiąca lat poświadczonego używania gramatyka i słow-nictwo akadyjskie ulegały ciągłym przemianom. Stąd można mówić o fazach rozwojowych i dialektach tego języka.

0.6. Język staroakadyjski.  
Najstarszą formą jest język staroakadyjski znany przede wszystkim z napisów pochodzących z południowej Babilonii z okresu od 24 do 20 wieku p.n.e. Dopiero później stwierdza się rozszczepienie akadyjskiego na dialekty babiloński i asyryjski.

0.7. Dialekt babiloński.  
W środkowej i południowej Babilonii już od 20 wieku p.n.e. mówiono dialektem babilońskim, w którym wyróżnia się cztery fazy rozwojowe. Szczególnie dobrze poświadczony jest język starobabiloński, którego zabytkami są rozmaite teksty, m.in. listy, dokumenty, prawa i wróżby pochodzące z okresu od 20 do 16 wieku. Omawiany jest on w pierwszych dziewiętnastu lekcjach tego podręcznika. Dalej interesujący nas dialekt miał następujące fazy rozwojowe: średniobabilońską (15 – 11 wiek), nowobabilońską (10 – 7 wiek) i późnobabilońską (od 6 wieku).  
0.8. Dialekt asyryjski.  
Dialekt asyryjski miał wedle badaczy trzy fazy rozwoju. Rozpoczyna ów rozwój język staroasyryjski, który jeśli pominąć kilka inskrypcji królewskich, znany jest nieomal wyłącznie z listów handlowych i dokumentów powstałych w środowisku kupców pochodzących z Asyrii, lecz działających na wschodzie Azji Mniejszej w 19 i 18 wieku p.n.e. Następnie wyróżnia się języki średnioasyryjski (16 – 11 wiek) i nowoasyryjski (10 – 7 wiek).

0.9. Język poetycki i literacki.  
Z języka starobabilońskiego wyodrębnił się język poezji używający wielu archaicznych form. Specyficzną formę językową mają też nowo- i późnobabilońskie teksty literackie, a także niektóre inskrypcje królów asyryjskich. Pisane one były w języku młodobabilońskim różniącym się znacznie od współczesnego mu nowobabilońskiego języka potocznego.

0.10. Dialekty peryferyjne i języki napisów.  
Na peryferiach Mezopotamii pojawiały się teksty pozwalające mówić o innych jeszcze dialektach języka akadyjskiego. I tak dają się odróżnić od języka starobabilońskiego używanego w Babilonii starobabilońskie dialekty z Elamu oraz rejonu Dijala położonych na wschód od Tygrysu i z Mari nad środkowym Eufratem przy dzisiejszej granicy iracko-syryjskiej. Około połowy II tysiąclecia kultura babilońska zdobyła uznanie na obszarach sąsiadujących z Mezopotamią. Tam pod wpływem miejscowych języków uformowały się specyficzne dialekty akadyjskie stosowane w formie pisemnej, takie jak język korespondencji amarneńskich oraz dialekty ze stolicy Hetytów Hattuša czy takich ośrodków jak Ugarit i Nuzis. Tymi szczególnymi formami akadyjskiego podręcznik niniejszy się nie zajmuje.

Głoski

0.11. Samogłoski.  
Poza trzema podstawowymi samogłoskami semickimi a, i oraz u język akadyjski posiada też samogłoskę e. Wszystkie one mogą występować jako krótkie: a, e, i, u lub długie a, e, i, u. Takie długie samogłoski, które powstały przez połączenie dwóch – krótkich czy długich – samogłosek, będą w transkrypcji oznaczane jako â, ě, î, û; np. idâk (&lt;iduak) „on zabija”.

0.12. Spółgłoski i półsamogłoski.  
Język akadyjski ma następujące spółgłoski zwarte: wargowe p, b oraz m, przednio-językowo-zębowe d, t, t oraz n, tylnojęzykowo-gardłowe g, k oraz q (k). Do spółgłosek szczelinowych należą przedniojęzykowo-zębowe z, s oraz c (s), przedniojęzykowo-dziąsłowa š, a ponadto tylnojęzykowo-gardłowa h. Specyficznie semickie zwarcie krtaniowe, tzw. alef oznaczane jest jako ‘. Prócz spółgłosek występują w akadyjskim pół-samogłoski w oraz j. Tylko w staroakadyjskim daje się odnotować ledwo dająca się określić głoska sycząca ś oraz pokrewny alefowi odstęp głosowy. \[Uwaga: klasyfikacja i nazewnictwo skorygowane zgodnie z polskim podręcznikiem Łyczkowskiej\] O wymowie można wygłosić kilka uwag: s jest ostre, przenikliwe jak w niemieckim „lassen”, z miękkie jak we francuskim „zéro”, š jest zbieżne z niemieckim „sch” \[polskim „sz”\], ‘ jest przerwą głosową jak w niemieckim „be’achten”. Określane konwencjonalnie jako emfatyczne semickie głoski t, q i c (s) artykułowane są głębiej, intensywniej w gardle czy ustach niż w przypadku odpowiednio głosek t, k i s.

Język i pismo

0.13. Sumeryjskie pismo klinowe.  
Akadowie używali do zapisywania swego języka pisma klinowego wynalezionego przez Sumerów. Materiałem, na którym pisano trzcinowym rylcem, były małe, uformowane z wilgotnej gliny tabliczki. Na najstarszych sumeryjskich dokumentach znaki mają formę obrazkową. Tak np. znak ryby służył do przekazu pojęcia ryby, a głowa byka oznaczała tyle, co słowo „bydło”. Ta forma obrazkowa, inaczej niż w Egipcie, używana była krótko. W wilgotnej glinie rysowane rylcem linie przybierały kształt klinów, a wygięte kreski tylko z trudnością dawały się zaznaczyć i najczęściej musiały być zastępowane przez dwa kliny tworzące kąt. Te trudności wraz z tendencją do upraszczania rysunków szybko doprowadziły do zaniku znaków obrazkowych.  
Początkowo pismo sumeryjskie miało charakter ideograficzny. Rozszerzając znaczenie poszczególnych znaków, zaczęto jednak przedstawiać pewne elementy gramatyczne, co spowodowało przekształcenie pisma ideograficznego w ideograficzno-sylabiczne.  
0.14. Pisownia języka akadyjskiego.  
Przy zapisywaniu wypowiedzi w języku akadyjskim posługiwano się nie tylko sumeryjskimi ideogramami, lecz także – i przede wszystkim – znakami sylabicznymi. Powstał jednak problem wynikający z tego, że systemy głosek języka akadyjskiego i sumeryjskiego różniły się od siebie. Nieznane w sumeryjskim głoski akadyjskie początkowo nie dawały się zapisać za pomocą pisma klinowego. Wybrnięto z tego, używając znaków z głoskami podobnymi, pokrewnymi. Na przykład charakterystyczne dla akadyjskiego emfatyczne t, q oraz c oddawano za pomocą znaków zawierających „t” lub „d”, „k” lub „g” oraz „z” lub „s”, pisząc – dajmy na to – zamiast sylaby tu sylabę „du”, zamiast qi – „ki”, czy zamiast ca – „za”. Ubocznym, niekorzystnym skutkiem takiego postępowania stała się wieloznaczność znaków sylabicznych, którą stopniowo likwidowano przez wprowadzanie osobnych znaków.

0.15. Transliteracja i transkrypcja.  
Obecnie zapisuje się teksty klinowe za pomocą liter alfabetu łacińskiego po to, by ułatwić sobie czytanie. Rozróżnia się dwa rodzaje takiego zapisu. Transliteracja odtwarza tekst klinowy znak po znaku, przy czym sylaby należące do jednego słowa spaja się łącznikami, zaś ideogramy pisze się dużymi literami (czasami ideogramy odróżnia się od innych znaków, podając całe, niepodzielone na sylaby słowo akadyjskie małymi literami). Transkrypcja jest próbą zbliżenia się w zapisie do oryginalnej wymowy słów. Sylaby są skupiane w całe słowa bez używania łączników, przy czym samogłoski długie lub ewentualne samogłoski podwójne są odpowiednio zaznaczane; jednocześnie ideogramy zamieniane są na odpowiednie słowa akadyjskie.  
Oto przykład transliteracji:  
wa-ar-du-um be-el-šu i-da-ak-ma gišGU.ZA-am i-ca-ba-at  
(lub wa-ar-du-um be-el-šu i-da-ak-ma kussiam i-ca-ba-at)  
To samo zdanie w transkrypcji wygląda następująco:  
wardum belšu idâkma kussiam icabbat  
Tłumaczenie dosłowne i zachowujące oryginalną kolejność słów jest takie: „sługa pana swego zabija i tron chwyta”.  
0.16. Znaki sylabowe.  
W tekstach klinowych występować mogą następujące typy znaków sylabowych: SP+SA (spółgłoska plus samogłoska), np. du, be, šu, da itd., SA+SP (samogłoska plus spółgłoska), np. ar, um, el, ak itd. albo SP+SA+SP (spółgłoska plus samogłoska plus spółgłoska), np. bat, tar itd. Trzeba dodać, że sylaby takie jak bat czy tar nie muszą być zapisywane jednym znakiem typu SP+SA+SP, lecz także dwoma znakami pozostałych typów, a więc (w transliteracji) jako ba-at oraz ta-ar (czytaj „bat” i „tar”, a nie „ba’at” i „ta’ar”). Na przykład słowo dannum „silny” można zapisać znakami sylabicznymi albo tak: da-an-nu-um, albo tak: dan-num, czy też: dan-nu-um, bądź da-an-num. Oprócz znaków trzech wskazanych wyżej typów istnieją też znaki oznaczające samogłoski jak a, e, i, u czy ú; przy czym umieszczane są one tylko w nagłosie słów lub też tam, gdzie powinna się znajdować sylaba typu ALEF+SA, a więc spotyka się a w miejscu ‘a czy e w miejscu ‘e.  
O wiele rzadziej używane były, zwłaszcza w tekstach nowoasyryjskich, znaki dwusylabowe typu baba czy bada.

0.17. Homofonia.  
Ta sama sylaba mogła być zapisywana różnymi znakami. Aby zaznaczyć w transliteracji, jakiego znaku użyto w tekście klinowym, stosuje się akcenty względnie cyfry. Np. sylabę gi transliteruje się jako gi, gi2 czy gi4 itp. w zależności od tego, jaki znak widnieje na oryginalnym dokumencie.

0.18. Polifonia.  
Jeden znak może określać wiele wartości dźwiękowych, np. znak czytany ut może kiedy indziej brzmieć tam, tám i per. Właściwą lekcję trzeba rekonstruować na podstawie kontekstu słowa czy zdania. Nie odróżnia się też w piśmie klinowym tego, czy wygłos słowa jest dźwięczny, bezdźwięczny czy emfatyczny: słowa im-qú-ut „on upadł”, im-du-ud „on mierzył” oraz ib-lu-ut „on żył” kończą się tym samym znakiem.

0.19. Ideogramy.  
Akadowie przejęli od Sumerów nie tylko znaki sylabiczne, ale też ideogramy, które jednak w tekstach akadyjskich czytamy zgodnie ze słownictwem tego języka. Ideogram oznaczający króla w tekście sumeryjskim brzmi „lugal”, a w akadyjskim šarrum.  
Wyraz šarrum może być zapisany zarówno za pomocą ideogramu LUGAL, jak i sylabami, np. ša-ar-rum; podobnie jest w przypadku innych słów mających ideogramy.

0.20. Determinatywy.  
Od Sumerów przejęli też Akadowie zwyczaj pisania niektórych rzeczowników z determinatywami umieszczanymi na przedzie lub na końcu wyrazu. Determinatywy wskazują na przynależność rzeczowników do określonej kategorii znaczeniowej. Znaków tych się nie czyta, zaś w transkrypcjach umieszczane są z przodu lub z tyłu nad wierszami, w których znajdują się słowa z nimi związane. Ważniejszymi determinatywami są następujące:  
d (od DINGIR, sum. „bóg”) – umieszczany prawie zawsze przed imionami bogów, np. dMarduk  
é (sum. „dom”) – rzadko przed nazwami budowli lub części budowli, np. ébe-tum, czytaj betum „dom”  
f(=MI, sum. „kobieta”) – niekiedy, przede wszystkim w czasach późniejszych, przed imionami kobiet, np. fQaqqadanitu „(kobieta imieniem) Qaqqadanitu”  
giš (sum. „drzewo”) – często przed nazwami sprzętów i innych przedmiotów z drewna, np. gišGU.ZA, czytaj kussûm „tron”  
há – stojąc na końcu wskazuje, że poprzedzające go słowo jest w liczbie mnogiej, np. UDUhá, czytaj immeru „owce”  
íd (sum. „rzeka”) – przed nazwami rzek i kanałów, np. ídBanitu „(rzeka) Banitu”  
itu (sum. „miesiąc”) – przed nazwami miesięcy, np. itusimanum „(miesiąc) siman”  
kam – po liczbach zapisanych cyframi, a nie sylabami, np. UD 2kam, czytaj um 2 „drugi dzień”  
ki (sum. „miejsce”) – po niektórych nazwach miast, np. KÁ.DINGIR.RAki, czytaj Babili „Babilon”  
kur (sum. „kraj”) – przed nazwami krajów, np. kurSangibute „(kraj) Sangibute”  
kuš (sum. „skóra”) – przed nazwami przedmiotów ze skóry, np. kušmašlium „wiadro skórzane”  
lú (sum. „człowiek”) – często przed nazwami zawodów, lúe-di-ku, czytaj ediku „koszy-karz”  
m (cyfra 1) – często przed męskimi imionami osobowymi, np. mIlum-bani „(mężczyzna imieniem) Ilum-bani”; w tekstach staroakadyjskich i starobabilońskich także przed imionami kobiet  
meš – jak há wskazuje, że poprzedzające go słowo jest w liczbie mnogiej, np. LUGALmeš, czytaj šarranu „królowie”  
mi (sum. „kobieta”) – niekiedy przed nazwami zawodów wykonywanych przez kobiety, np. mišu-gi-tum, czytaj šugitum „kapłanka”  
sar (sum. „zagon”) – często po nazwach roślin ogrodowych, np. šu-ha-ti-in-nisar, czytaj šuhatinni „sadzonka” (?)  
túg (sum. „odzież”) – przed tkaninami i częściami garderoby, np. túghurdubašše (jakaś wstydliwa część ubrania)  
ú (sum. „roślina”) – przed nazwami dotyczącymi roślin, np. úpu’u „plewa, sieczka”  
uru (sum. „miasto”) – przed nazwami miast, np. uruHundur „(miasto) Hundur”.  
Dalej w podręczniku determinatywy będą transliterowane tylko tam, gdzie wy-stępują z wyrazem zapisanym znakami sylabowymi, a opuszczane wtedy, gdy towarzyszą ideogramom.

0.21. Komplementy fonetyczne.  
Do ideogramu często przyłączana jest ostatnia sylaba słowa (niekiedy dwie ostatnie sylaby), a to po to, by określić jego formę gramatyczną (przypadek) i ułatwić odczytanie, np. LUGAL-rum, czytaj šarrum „król”, albo LUGAL-am (bądź LUGAL-ra-am), czytaj šarram „króla” (biernik/dopełniacz).  
Dalej w podręczniku komplementy takie nie będą transliterowane – podawana będzie forma do odczytania.

0.22. Długość samogłosek.  
Długa samogłoska może być oznaczana w piśmie klinowym tylko po znaku typu SP+SA w ten sposób, że dołącza się znak oznaczający samogłoskę a, e, i, ú rzadko u, np. i-ma-a-at = imât „on umierał”, ša-me-e-em = šaměm „nieba”, na-ak-ri-i-ka = nakrika „twój nieprzyjaciel”, pa-nu-ú-ša = panuša „jego przednia strona”. Co prawda, tego rodzaju zapisywanie samogłoski nie zdarza się zbyt często; częściej długość samogłoski w piśmie nie jest określana i musi być odtwarzana gramatycznie.  
W nagłosie sylab i słów długość samogłosek nie jest zaznaczana i wobec tego jest tylko gramatycznie odtwarzana, np. eribum nie było zapisywane jako e-e-ri-bum.  
Niekiedy znaki samogłoskowe są pisane także w miejscach, gdzie nie mamy do czynienia z samogłoską długą, np. i-la-a-ak = illak, šu-ul-mu-ú-um = šulmum.  
W „Hymnie do Isztar” spotyka się takie oto zaznaczanie długości samogłosek: ba-ši-a-a = bašia czy nam-ra-i-i = namra’i.

0.23. Spółgłoski podwójne.  
Podwojenie spółgłosek jest w piśmie klinowym łatwe do oddania, np. i-ca-ab-ba-at (dla icabbat) czy it-tu-ul (dla ittul).  
Bardzo często jednak pisano tylko drugą spółgłoskę, tak że podwojenie spółgłosek da się odtworzyć tylko gramatycznie, np. i-ca-ba-at (dla icabbat) czy i-tu-ul (dla ittul).

0.24. Pisownia przerwy głosowej.  
Przerwa głosowa ‘ pozostaje w nagłosie nieoznaczona, tak samo jest zwykle w śródgłosie, choć czasem jest ona oddawana w pisowni w sposób następujący: iš-al = iš’al czy ma-li-at = maliat (właściwie mali’at). Tylko w przypadku czasowników z alefem w środku stosowano niekiedy specjalne znaki oznaczające przerwę głosową, np. na-a’-da-at = na’adat czy i’-id = i’id.

Uwagi do ćwiczeń i tekstów

0.25. Ćwiczenia.  
Ćwiczenia do lekcji 1-19 opierają się na starobabilońskich tekstach wróżbiarskich. Teksty takie składają się z osobnych wróżb nie mających związku z całością, co dało większe możliwości wybierania zdań mogących być przykładami do tematów grama-tycznych każdej z lekcji.

0.26. Wróżby.  
Każda wróżba składa się z dwóch części: „protasis”, która opisuje zdarzenie wieszcze i „apodosis”, która przepowiada wydarzenie będące nieuchronnym skutkiem wydarzenia wymienionego w „protasis”. Przykładem może być wróżba z urodzin: „Kiedy potwór zęby wywiesza \[protasis\], dni króla idą ku końcowi i na jego tronie będą inni siedzieli \[apodosis\]”.  
W ćwiczeniach będą często występowały jako przykłady zdania „protasis” bez związanych z nimi „apodosis”. W przypadkach pozostałych obie części rozdzielane będą średnikiem, a wieloczłonowe „apodosis” dzielone będą przecinkami. Ma to ułatwić interpretację – trzeba jednak zdawać sobie sprawę z tego, że w akadyjskich tekstach klinowych żadnych znaków interpunkcyjnych nie ma.

0.27. „Protasis”.  
W „protasis” wróżby opisane są zjawiska, które uważa się za niezwykłe, a zatem prorocze, zapowiadające zdarzenia wskazywane w „apodosis”. „Protasis” ma zawsze formę zdania warunkowego rozpoczynającego się od słowa „kiedy” (akad. šumma). W zależności od gatunku obserwowanych i omawianych w „protasis” zdarzeń rozróżnia się liczne grupy wróżb, jak np. wróżby z urodzeń, astronomiczne i z wnętrzności, a wśród tych ostatnich wróżby z wątroby, żółci i płuc.  
0.28. „Apodosis”.  
„Apodosis” dotyczą najczęściej wydarzeń, jakich oczekiwano w najbliższej przyszłości. Rzadkie są ogólnikowe przepowiednie np. zmartwień czy radości, częściej mamy do czynienia z takimi, które wiązały się biegiem życia społecznego i dotyczyły pogody (deszczu, upału, zimna), lepszych czy gorszych plonów, nalotów szarańczy, ciężkich warunków życia, wzrostu cen, klęsk głodowych, wydarzeń wojennych i ich następstw (jak spustoszenie). Pewne „apodosis” odkrywają przeznaczenie pojedynczych osób, które we wróżbach z wnętrzności najczęściej występują pod mianem „człowiek” (awi-lum), a niekiedy określane są jako „właściciel (ofiarnej) owcy” (bel immerim); przepowiednie te dotyczą życia lub śmierci, np. „człowiek z powodu kopnięcia krowy zginie”, szczęścia albo nieszczęścia, np. „lew będzie w ogrodzeniu człowieka żarł”, albo też stosunków z władzą, np. „pałac będzie dom człowieka opieczętowywał”. Najczęściej „apodosis” mówią o królu, jego roli jako wodza (zwycięstwie lub klęsce wojska królewskiego) lub jego osobistym losie (śmierci lub przeżyciu w czasie rewolty poddanych czy urzędników pałacowych). Niektóre „apodosis” wiążą się z wydarzeniami przeszłymi i usiłują odpowiedzieć, czy będą się one powtarzały (np. „wróżby z wątroby Naram-Sina, który świat opanował”). Dość rzadko spotykane są „apodosis” mające formę nakazów („wypędź wroga!”). Często przepowiednie zawierają dwa lub większą ilość „apodosis”.

0.29. Wróżby z urodzeń.  
„Protasis” starobabilońskich wróżb z urodzeń opisują anormalne postaci nowonarodzonych owiec. Zapewne tylko część opisywanych w nich przypadków została rzeczywiście zaobserwowana jak może zęby zwieszające się z pyska jagnięcia czy jagnię z jednym tylko okiem. Większość zdaje się być owocem fantastycznej przesady i wymysłów jak np. wizje jagniąt o postaci żmii lub obliczu lwa czy wilka, ze stopami lwimi, albo też takich, którym z pyska zwisa druga owcza głowa.

0.30. Wróżby z wnętrzności.  
Potworki i niezwykłe zjawiska na niebie, które skłaniały do snucia przepowiedni, były rzadkie. Aby móc zawsze, o każdej porze przewidywać przyszłość, trzeba było znaki wróżebne uzyskiwać sztucznie. Stąd praktyka badania wnętrzności. Już u zarania swych dziejów mieszkańcy Mezopotamii zauważyli, że niektóre organy zwierząt rzeźnych czy ofiarnych mogły mieć różnorodny wygląd. Faktowi temu przypisano szczególne znaczenie. Uznano, że organy owiec ofiarnych są zwierciadłem, w którym odbijają się wszelkie zdarzenia, dzięki czemu wtajemniczeni badacze ofiar, po szczegółowym oglądzie są w stanie dać odpowiedź na określone pytania dotyczące przyszłości. Starannie więc analizowano położenie poszczególnych organów i ich części, zabarwienie i chorobliwe zmiany ich powierzchni oraz rozwinięto terminologię, za pomocą której można było charakteryzować wyniki takiej analizy.

0.31. Wróżby z wątroby.  
Najdokładniej kapłani zajmujący się składaniem ofiar studiowali wątrobę (akad. amutum; to słowo zaczęło też pełnić rolę ogólnej nazwy wszelkich wróżb, przynajmniej tych z wątroby). Z pewnością można określić znaczenie takich nazw części wątroby jak naplastum – lewy płat wątroby (lobus sinister), bab ekallim – (dosłownie „brama pałacu”) wklęsłość pomiędzy obydwoma płatami, w której mieści się woreczek żółciowy, ubanum – (dosł. „palec”) wyrostek piramidalny (processus piramidalis) i abullum – (dosł. „brama miejska”) prawdopodobnie porta hepalis. Ponadto nazwami padanum „ścieżka” i dananum „posiłki, pomoc” oznaczano jakieś dwa miejsca na dolnych krawędziach lewego płata, które trudno anatomicznie zidentyfikować. Nie jest też dokładnie ustalone położenie nirum „jarzma” i cibtum „narośli”.

0.32. Wróżby z żółci.  
W przypadku woreczka żółciowego (martum, dosł. „gorzka”) wyróżniano macrah martim – („zaczątek żółci”) przewód żółciowy w wątrobie, martum qablaša – środkową część woreczka żółciowego i reš martim – czubek woreczka żółciowego. Ponadto mianem appi martim (dosł. „nos woreczka żółciowego”) określano przypuszczalnie wierzchnią część przedniej strony woreczka żółciowego, a przeciwieństwo tej części nazywano išdi martim albo išid martim „spód woreczka żółciowego”.

0.33. Wróżby z płuc.  
Najważniejszą częścią płuc (hašûm) był dla ofiarników uban hašî qablîtum „środkowy palec płucny” – być może lobus azygos, a poza tym kišad hašîm „szyja płuc” – może część oskrzeli?

0.34. Cechy wnętrzności.  
Na powierzchni wątroby, woreczka żółciowego i innych wnętrzności zauważano znamiona, pęcherzyki, wgłębienia i zmiany chorobowe. Najważniejsze z nich to kakkum – „broń” czy kakki imittim „broń prawicy”, šepum – „stopa” („noga”), pillurtum – „krzyż”, erištum – „życzenie”, zihhum – „pęcherzyk”, šilum – „dziurka”, „otworek”, pitrum – „szczelina”, „szpara” i pilšum – rów.

0.35. Teksty.  
Od 12 lekcji obok ćwiczeń używane będą jako czytanki ciągłe teksty starobabilońskie takie jak prawa, listy i dokumenty.  
GRAMATYKA  
Lekcja pierwsza

1.1. Rdzeń semicki.  
W rdzeniu, jego stałym następstwie dwóch, trzech lub czterech spółgłosek tkwi znaczenie słowa. Do każdego rdzenia należy samogłoska, która jednak nie występuje we wszystkich formach.  
Odmiana następuje przez dołączanie samogłoskowych i spółgłoskowych elementów wchodzących pomiędzy spółgłoski rdzenia lub też poprzedzających je czy też następujących po nich.  
Od rdzenia akadyjskiego \*prus o znaczeniu podstawowym (m.in.) „rozstrzygać” utworzyć można takie np. formy jak  
purus – rozstrzygaj!  
iprus – rozstrzygnął  
(ša) taprusu – (co) ty rozstrzygniesz  
paris – jest rozstrzygnięty  
iptaras – \[bez odpowiednika w j. polskim: rozstrzygnął w czasie zaprzeszłym\]  
parasum – rozstrzygać  
parisum – rozstrzygający  
Odróżnia się rdzenie czasownikowe od rdzeni nominalnych.

1.2. Akadyjskie rdzenie czasownikowe.  
Większość rdzeni czasownikowych składa się z trzech spółgłosek i jednej krótkiej samogłoski między drugą a trzecią spółgłoską. Chodzi o samogłoski takie jak a, i oraz u.  
Rdzenie tego rodzaju to m.in. \*cbat, \*šhit, \*knuš, \*mhur.

1.3. Bezokolicznik.  
Glossariusze i słowniki posługują się nie rdzeniem, lecz formą bezokolicznika.  
Bezokolicznikiem rdzenia \*cbat jest cabatum „ujmować”, „chwytać”, słowo, które można też tłumaczyć jako rzeczownik „ujmowanie” oraz odmieniać jak rzeczow¬nik, np. w wyrażeniu ana cabatim = aby ująć = do ujęcia mamy do czynienia z bezoko¬licznikiem w dopełniaczu.  
W przypadku bezokolicznika omawianego typu samogłoską po pierwszej spółgło-sce jest a (krótkie), a po drugiej a (długie). Np. \*knuš – kanašum, \*šhit – šahatum, \*mhur – maharum (m może znikać w przypad¬kach odchodzenia od mimacji)

1.4. Czasy przeszły i teraźniejszy.  
W języku akadyjskim mamy 4 czasy \[czy może lepiej cztery formy zwane mniej czy bardziej trafnie „czasami”\]: teraźniejszy \[Präsens, w skrócie Prs\], przeszły \[Präteri-tum, w skrócie Prt\], przeszły dokonany \[Perfekt, w skrócie Pf\] oraz stativus \[w skrócie St\].  
Prs, Prt i Pf odmieniają się z pomocą przedrostków. Są to  
ta- dla 2 osoby i  
i- dla 3 osoby  
Prt ma za swą podstawę nie rozszerzony rdzeń. Prs podwaja środkową spółgłoskę rdzenia i wstawia przed nią samogłoskę a.

Rdzeń Prt Prs  
\*cbat icbat – chwytał icabbat – chwyta  
\*knuš iknuš – poddawał się ikannuš – poddaje się  
\*šhit išhit – napadał išahhit – napada  
\*mhur imhur – przyjmował imahhar – przyjmuje

W Prs samogłoska między podwojoną drugą a trzecią spółgłoską jest identyczna z samogłoską rdzeniową. Wyjątkiem jest pewna grupa czasowników, która wykazuje zmienność samogłoski rdzeniowej (apofonię); są to czasowniki z samogłoską rdzeniową u, w przypadku których owa samogłoska w Prs zastępowana jest przez a. Np. czasow¬nik maharum odmienia się następująco:  
Prt: imhur, Prs: imahhar  
\[Można dodać, że nie wszystkie czasowniki z samogłoską rdzeniową u wykazują apo-fonię – por. wyżej odmianę \*knuš\]  
Prs służy nie tylko do wyrażania czasu teraźniejszego, lecz też do wyrażania czasu przyszłego:  
imahhar – „on przyjmuje” lub „on będzie przyjmował”.  
Czas ten jest typową formą gramatyczną w „apodosis” wróżb.  
Prt jest formą czasu przeszłego. Mimo to w zdaniach warunkowych (po šumma „kiedy”) jest używany w znaczeniu czasu teraźniejszego:  
šumma martum ishur – kiedy woreczek żółciowy się obraca.  
W słownikach przy bezokoliczniku umieszcza się jego samogłoskę rdzeniową (w przypadku czasowników wykazujących apofonię a/u):  
kanašum (u)  
maharum (a/u)  
Jeśli przy czasowniku nie umieszcza się żadnej samogłoski, to wskazuje się na to, że odmienia się on tylko w St.  
1.5. Stativus.  
W przeciwieństwie do Prs i Prt, które określają działanie, St służy do wyrażania sta-nów. Nie grają przy tym roli aspekty czasowe, tzn. St może mieć znaczenie zarówno czasu teraźniejszego, jak i przyszłego i przeszłego. Najczęściej, choć nie zawsze, musi być tłumaczony jako strona bierna. Zatem cabit znaczy na ogół „on jest (był, będzie) złapany”, a niekiedy „on jest (był, będzie) łapiący”.  
Stativus ma po pierwszej spółgłosce samogłoskę a, zaś po drugiej często i:  
Bezokolicznik Stativus  
damaqum – być dobrym damiq – on jest (był, będzie) dobry  
sapahum – rozpraszać sapih – on jest (był, będzie) rozproszony  
tarakum – być ciemnym tarik – on jest (był, będzie) ciemny

1.6. Rdzeń imienny.  
Rdzeń ten, określający pierwotne imiona czyli przedmioty, istoty żyjące lub (jako przymiotnik) cechy, składa się często z dwóch lub trzech spółgłosek i krótkich lub dłu-gich samogłosek, np.  
\*bit (bitum) – dom  
\*ruba (rubûm) – książę  
\*nakr (nakrum) – wrogi, (lub rzeczownikowo) wróg  
\*rabi (rabûm) – duży, wielki  
Obok imion pierwotnych są też imiona odczasownikowe, np.  
cabtum – jeniec (od cabatum – chwytać)

1.7. Deklinacja w liczbie pojedynczej.  
Imię ma trzy przypadki określane za pomocą końcówek:  
Przypadek Końcówka  
Mianownik (Nominativus) – w skrócie N -um  
Dopełniacz (Genetivus) – w skrócie G -im  
Biernik (Akkusativus) – w skrócie A -am

N i A odpowiadają w zastosowaniu swym niemieckim odpowiednikom, G używany jest po przyimkach.  
„m” końcówek deklinacyjnych (mimacja) często ginie, tak że końcówki przybierają postać:  
N: -u  
G: -i  
A: -a  
Przypadek Odmieniane imię Tłumaczenie  
N: bitum lub bitu dom  
G: ina bitim lub ina biti w domu  
A: bitam lub bita dom

Jeśli rdzeń imienny ma w wygłosie samogłoskę, ulega ona „ściągnięciu” z sa-mogło¬ską końcówki deklinacyjnej. Zachowuje się przy tym samogłoska deklinacyjna z tymi wyjątkami, iż zdarza się, że złożenie a oraz i daje w efekcie ě, zaś złożenie i oraz a nie ulega – wbrew regule – „ściągnięciu” i zachowuje się w postaci zbitki ia.  
Przypadek Rdzeń + końcówka Efekt konstrukcji przypadku Tłumaczenie  
N: \*ruba’um rubûm książę  
G: \*ruba’im ana ruběm do księcia, księciu  
A: \*ruba’am rubâm księcia  
Uwaga: wyrażenie złożone postaci:  
ana + imię w dopełniaczu  
często należy tłumaczyć za pomocą celownika tego imienia z pominięciem przy¬imka ana, np. ana ruběm = księciu.  
Przypadek Rdzeń + końcówka Efekt konstrukcji przypadku Tłumaczenie  
N: \*rabi’um rabûm wielki (lub wielmoża)  
G: \*rabi’im rabîm wielkiego (wielmoży)  
A: \*rabi’am rabiam wielkiego (wielmożę)

We wszystkich przypadkach przymiotnik następuje po rzeczowniku, z którym się łączy \[odwrotnie niż w języku polskim\].  
Przyp. Rzeczownik z przymiotnikiem Tłumaczenie  
N: šarrum dannum potężny król  
G: ana šarrim dannim do potężnego króla lub potężnemu królowi  
A: šarram dannam potężnego króla

1.8. Szyk wyrazów.  
Czasownik stoi na końcu zdania, podmiot najczęściej na początku, a dopełnienie między podmiotem a czasownikiem. Jeśli dopełnienie jest szczególnie akcentowane, może wyprzedzić podmiot.

1.9. Zdanie złożone współrzędnie.  
Do łączenia zdań, a zwłaszcza części zdania służy enklityczna partykuła –ma „i”, „i potem”, np.  
šumma izbum tarikma calim – kiedy potworek jest ciemny i jest czarny.

Lekcja druga

2.1. Rodzaj żeński imion.  
Język akadyjski rozróżnia dwa rodzaje: męski i żeński. Aby utworzyć rodzaj żeń-ski imienia, dołącza się do rdzenia cząstkę  
-t- (lub –at-)  
Rodzaj męski Rodzaj żeński  
bel-um – pan bel-t-um – pani  
nakr-um – wrogi nakar-t-um – wroga

Żeńska forma przymiotnika służy też do tworzenia pojęć abstrakcyjnych. Np. przymiotnik dannum „silny, potężny” w formie żeńskiej dannatum znaczy jako przy-miotnik „potężna”, zaś jako rzeczownik „moc”, „siła”, a także „okrucieństwo”, „ciężkie położenie” czy „bieda”. Niekiedy pojęcia abstrakcyjne mogą przyjmować znaczenie przedmiotowe, np. dannatum „siła”, „moc” znaczy też „twierdza”.

2.2. Tworzenie rodzaju żeńskiego za pomocą –t-.  
Żeńskie zakończenie –t- przyłącza się zarówno do rdzeni, które kończą się spół-gło¬ską, jak i do takich, które mają końcówkę samogłoskową. W tym drugim przy¬padku samogłoska kończąca rdzeń ulega wydłużeniu.  
Rodzaj męski Rodzaj żeński  
belum – pan beltum – pani  
rabium – duży rabitum – duża

Jeśli rdzeń przymiotnika czy też imienia odczasownikowego ma w wygłosie dwie spółgłoski, włącza się pomiędzy nie samogłoskę pomocniczą.  
Rodzaj męski Rodzaj żeński  
rapšum – szeroki rapaštum – szeroka  
nakrum – wrogi nakartum – wroga  
damqum – dobry damiqtum – dobra  
cehrum – mały cehertum – mała  
cabtum – uwięziony, wzięty do niewoli cabittum – uwięziona, wzięta do niewoli

2.3. Tworzenie rodzaju żeńskiego przy pomocy –at-.  
Końcówka żeńska przybiera formę –at-, dołączając się albo do rdzeni zakończo-nych podwojonymi spółgłoskami albo do rdzeni imion pierwotnych zakończonych dwoma dowolnymi spółgłoskami. Przy tym w przypadku rdzeni z samogłoską e cząstka –at- przybiera postać –et-.  
Rodzaj męski Rodzaj żeński  
dannum – mocny dannatum – mocna  
ellum – czysty elletum – czysta  
šarrum – król šarratum – królowa  
kalbum – pies kalbatum – suka  
Tworzenie rodzaju żeńskiego imion odczasownikowych kończących się na dwie spół-głoski przebiega według wzorca opisanego w punkcie 2.2.

2.4. Zmiany spółgłosek przy tworzeniu rodzaju żeńskiego.  
Spółgłoski n i d są asymilowane przez końcówkę rodzaju żeńskiego –t- i tego rodzaju zbitka przybiera postać –tt-. Ponadto c przed –t- przechodzi w š.  
Rodzaj męski Rodzaj żeński  
lemnum – zły lemuttum – zła; także: niegodziwość  
madum – liczny mattum – liczna  
marcum – chory maruštum – chora; choroba  
icum – w małej liczbie ištum – w małej liczbie  
Nie stanowi to podstawy do przyjmowania, że długa samogłoska stojąca przed zdwo-joną spółgłoską ulega skróceniu. Mamy więc mattum, a nie mattum oraz qassu, a nie qassu (zob. 3.10.).

2.5. Rzeczowniki pierwotne rodzaju żeńskiego.  
Niektóre rzeczowniki nie zawierając elementu –t- w składni zachowują się jak rzeczowniki rodzaju żeńskiego. Do tej grupy należą rzeczowniki naturalne rodzaju żeń-skiego takie jak ummum „matka”, dalej liczne nazwy części ciała, np. šepum „noga” lub „stopa”, a także słowa matum „kraj”, ummanum „wojsko”, harranum „wyprawa wo¬jenna”, ekallum „pałac”, abnum „kamień” i narum „rzeka”.  
Przymiotniki łączące się z takimi rzeczownikami są rodzaju żeńskiego:  
ummanum nakartum – wroga armia  
matum rapaštum – szeroki kraj

2.6. Rodzaj żeński czasowników.  
Rodzaj orzeczenia zależy od rodzaju podmiotu. W Prs i Prt formy rodzaju żeń-skiego i męskiego 3 osoby liczby pojedynczej są identyczne:  
icbat – on chwycił, ona chwyciła  
icabbat – on chwyta, ona chwyta.  
Jednakże w St rodzaje są rozróżniane za pomocą zakończenia –at-, które dołącza się do formy rodzaju męskiego i przekształca ją w formę rodzaju żeńskiego, przy czym samogłoska i po drugiej spółgłosce zostaje odrzucona.  
Rodzaj męski Rodzaj żeński  
cabit – on jest (był, będzie) schwycony cabtat – ona jest (była, będzie) schwycona  
damiq – on jest (był, będzie) dobry damqat – ona jest (była, będzie) dobra

2.7. Status imienia.  
Imię, od którego nie zależy inne imię w dopełniaczu znajduje się w tzw. „status rectus”, kiedy to do rdzenia dołączona jest końcówka odpowiedniego przy¬padku, np. ummanum – wojsko.  
Jeśli natomiast po rzeczowniku następuje zależny odeń dopełniacz, rzeczownik ten przybiera specjalną formę zwaną „status constructus” pozbawioną końcówek przy-padków i wraz z dopełniaczem tworzy jedność akcentową, np. umman šarrim – wojsko króla.  
Status constructus brzmi jednakowo dla wszystkich przypadków, np.  
N: mari awilim – syn człowieka  
G: mari awilim – syna człowieka  
A: mari awilim – syna człowieka  
Do rzeczownika w status constructus dołączyć mogą się dwa imiona w dopełnia-czach, taka zbitka przekształca się w konstrukcję z dwoma status constructus:  
tibût mari awilim – powstanie (rebelia) syna człowieka  
(O status absolutus czytaj w 8.9.)  
2.8. Tworzenie status constructus.  
Status constructus tworzony jest przez odrzucenie końcówki wyrażającej przypa-dek, jeśli jest tak, że wyraz wzięty bez wspomnianej końcówki kończy się samo¬głoską lub tylko jedną spółgłoską, w szczególności –t- rodzaju żeńskiego.  
Status rectus Status constructus  
wacûm (&lt;wacium) – wychodzący waci abullim – wychodzący przez bramę  
belum – pan bel alim – pan miasta  
tibutûm – najście, bunt tibût erbîm – nalot szarańczy

Jeśli rdzeń kończy się zdwojoną spółgłoską to albo zostanie ona uproszczona, albo do rdzenia zostanie dołączona pomocnicza samogłoska i.  
Status rectus Status constructus  
ekallum – pałac ekal nakrim – pałac wroga  
šarrum – król šar matim – król kraju  
libbum – serce libbi matim – wnętrze kraju  
kakkum – broń kak bartim – broń buntu  
kakki imittim – broń prawicy

Jeśli rdzeń kończy się dwoma różnymi spółgłoskami, wchodzi pomiędzy nie sa-mogłoska pomocnicza odpowiadająca samogłosce rdzenia.  
Status rectus Status constructus  
alpum – krowa alap awilim – krowa człowieka  
nikpum – uderzenie nikip alpim – uderzenie krowy  
beltum – pani belet bitim – właścicielka domu  
lumnum – zło lumun libbim – troska serca

Wielosylabowe rzeczowniki z końcówką rodzaju żeńskiego –t- tworzą status constructus przez przyłączenie samogłoski pomocniczej i.  
Status rectus Status constructus  
napištum – życie napišti awilim – życie człowieka  
miqittum – klęska miqitti ummanim – klęska wojska

2.9. Nieregularne tworzenie status constructus.  
Część jednosylabowych rdzeni z długą samogłoską przybiera formę status con-structus na dwa sposoby: albo tylko przez odrzucenie końcówki, albo przez odrzucenie końcówki i dodanie samogłoski i.  
Status rectus I wersja s.c. II wersja s.c.  
alum – miasto al ali  
marum – syn mar mari  
bitum – dom bit biti  
bušum – mienie buš buši  
šumum -imię šum šumi  
Ale np. milum „obfitość” w połączeniu z irtum „pierś” przybiera formę mili, tworząc wyrażenie mili irtim – dosłownie „obfitość piersi” = „odwaga”.  
Lekcja trzecia

3.1. Liczba mnoga imion rodzaju męskiego.  
Rzeczowniki rodzaju męskiego mają w mianowniku liczby mnogiej końcówkę –u, która w przypadku rdzeni z samogłoską w wygłosie przybiera postać –û.  
Liczba pojedyncza Liczba mnoga  
N: šarrum – król šarru – królowie  
N: rubûm (&lt;ruba’um) – książę rubû (&lt;ruba’u) – książęta

Przymiotniki tworzą liczbę mnogą rodzaju męskiego za pomocą końcówki -utum, która w przypadku rdzeni zakończonych samogłoską przybiera postać –ûtum. Np. nakrutum – wrodzy; wrogowie  
rabûtum (&lt;rabiutum) – wielcy; wielmoże  
umu gamrutum – spełnione dni  
Imię šibum „stary” lub „starzec” tworzy liczbę mnogą zarówno jak rzeczownik: šibu (i znaczy wtedy „świadkowie”), jak też w sposób właściwy przymiotnikom: šibutum (i znaczy wówczas „najstarsi”.

3.2. Liczba mnoga imion rodzaju żeńskiego.  
Końcówką rodzaju żeńskiego zarówno dla rzeczowników jak i dla przymiotni¬ków jest w mianowniku liczby mnogiej –atum.  
Liczba pojedyncza Liczba mnoga  
N: bussurtum – wiadomość bussuratum – wiadomości  
N: naplastum – lewy płat wątroby naplasatum – lewe płaty wątroby  
Dla imion, które mają samogłoskę rdzeniową e, zamiast –atum mamy –etum.  
Liczba pojedyncza Liczba mnoga  
N: elletum – czysta elletum – czyste  
N: tertum – polecenie teretum – polecenia

Także rzeczowniki rodzaju żeńskiego bez –t- tworzą liczbę mnogą przy pomocy końcówki –atum.  
Liczba pojedyncza Liczba mnoga  
N: ekallum – pałac ekallatum – pałace  
N: eleppum – statek eleppatum – statki

Pewne rzeczowniki, które w liczbie pojedynczej są rodzaju męskiego, w liczbie mnogiej zmieniają rodzaj na żeński, co wiąże się z przyłączeniem końcówki –atum; np. našpakum „spichlerz” ma w liczbie mnogiej postać našpakatum.  
Liczba mnoga od martum „woreczek żółciowy” ma postać marratum (martum pochodzi od marrum „gorzki”), a od šalamtum „trup” pochodzi šalmatum (&lt;šalamat-um) „trupy”.  
W przypadku rdzeni z samogłoską w wygłosie w liczbie mnogiej rodzaju żeń-skiego nie ulegają „ściągnięciu” zbitki i’a, a zwłaszcza i’a (por. rabi’atum).

3.3. Imiona występujące tylko w liczbie mnogiej.  
Tylko w liczbie mnogiej występują słowa:  
mû (&lt;\*ma’-u) – woda  
kišpu – czary  
cummiratum – życzenie  
nišu – ludzie  
panu – strona przednia  
3.4. Deklinacja w liczbie mnogiej.  
W liczbie mnogiej dopełniacz i biernik brzmią jednakowo. Końcówkom mianow-nika odpowiadają jednoznacznie określone końcówki dopełniacza/biernika:  
N G/A  
-u -i  
-utu(m) -uti(m)  
-atu(m) -ati(m)  
-etu(m) -eti(m)

Mimacja czyli m końcówek liczby mnogiej często znika.  
Przykłady:  
N G/A  
šarru – królowie šarri – królów  
rubû – książęta rubě (&lt;ruba’i) – książąt  
ekallatu(m) – pałace ekallati(m) – pałaców, pałace  
panu – fronty pani – frontów, fronty

3.5. Status constructus w liczbie mnogiej.  
Imiona z końcówkami liczby mnogiej –utum, -atum oraz –etum tracą w status constructus dwie ostatnie głoski –um, np.  
šibut alim – starsi miasta, najstarsi mieszkańcy miasta  
buccurat haděm – poselstwo sprzymierzeńca  
Imiona z końcówką liczby mnogiej –u (w G/A –i) będą w status constructus nie-zmienione, np.  
panu martim – przednia część woreczka żółciowego  
ina pani abullim – w obliczach bram = przed bramami

3.6. Liczba mnoga czasowników.  
Końcówkami 3 osoby liczby mnogiej są w St, Prt i Prs –u dla rodzaju męskiego i –a dla rodzaju żeńskiego. W St –u i –a dołączają się do formy 3 osoby liczby pojedyn-czej rodzaju męskiego, przy czym wypada samogłoska i stojąca po drugiej spółgłosce. W Prs i Prt końcówki wspomniane dołączają się do formy 3 osoby liczby pojedynczej bez żadnych innych zmian.  
Czas Osoba Liczba pojedyncza Liczba mnoga  
Prt 3 os. r.m. imhur – on otrzymał imhuru – oni otrzymali  
3 os. r.ż imhur – ona otrzymała imhura – one otrzymały  
Prs 3 os. r.m. imahhar – on otrzymuje imahharu – oni otrzymują  
3 os. r.ż imahhar – ona otrzymuje imahhara – one otrzymują  
St 3 os. r.m. cabit – on jest złapany cabtu – oni są złapani  
3 os. r.ż cabtat – ona jest złapana cabta – one są złapane  
cabtu i cabta mogą mieć też znaczenie aktywne „oni (one) są łapiący (łapiące)”, tzn. „oni (one) są tymi, co łapią”.  
3.7. Przyrostki o charakterze zaimków dzierżawczych.  
Zaimki dzierżawcze „mój”, „twój” itd. w języku akadyjskim wyrażane są najczę-ściej przez przyrostki. Najważniejszymi z nich są:  
-ja (lub –i) – mój  
-ka – twój  
-ša – jej  
-šu – jego  
-šina – ich (o osobach rodzaju żeńskiego)  
-šunu – ich (o osobach rodzaju męskiego)

3.8. Deklinacja w liczbie pojedynczej imion z przyrostkami.  
W mianowniku i bierniku liczby pojedynczej rzeczownik przed przyrostkiem ma często formę status constructus, np. wyraz nakrum „wróg”, przybierający w status con-structus postać naker, w mianowniku i bierniku liczby pojedynczej z przyrostkiem -šu brzmi nakeršu „jego wróg”.  
Natomiast w dopełniaczu liczby pojedynczej przyrostek następuje po końcówce tego przypadku –im, przy czym mimacja odpada, zaś samogłoska i ulega wydłużeniu w wyniku przesunięcia się akcentu: nakrim „wroga” przechodzi w nakrišu „jego wroga”. Oto inne przykłady:  
Status rectus Status constructus Forma z zaimkiem  
N: ummanum – wojsko umman ummanka – twoje wojsko  
G: ummanim – wojska ummanika – twojego wojska  
A: ummanam – wojsko ummanka – twoje wojsko

Status rectus Status constructus Forma z zaimkiem  
N: alpum – krowa alap alapšunu – ich krowa  
G: alpim – krowy alpišunu – ich krowy  
A: alpam – krowę alapšunu – ich krowę

W przypadku rdzeni kończących się podwójną spółgłoską przyłączenie zaimka w mianowniku i bierniku liczby pojedynczej wiąże się ze wstawieniem pomiędzy rdzeń a przyrostek samogłoski –a, np. słowo appum „nos” ze status constructus appi przybiera w N/A z zaimkiem postać np. appašu. Dopełniacz: appišu.  
Inny przykład:  
Status rectus S.c. Forma z przyimkiem  
N: muhhum – wierzchnia strona muhhi muhhaša – jej wierzchnia strona  
G: muhhim – wierzchniej strony ina muhhiša – na jej wierzchniej stronie  
A: muhham – wierzchnią stronę muhhaša – jej wierzchnią stronę  
3.9. Zaimek dzierżawczy –ja „mój”.  
Przyrostek ten ma dwie różne formy w liczbie pojedynczej: po samogłosce przy-biera postać –ja, zaś po spółgłosce jawi się jako –i. Po rzeczowniku w dopełniaczu wy-stępować może tylko jako –ja: wyraz ummani(m) „wojska” po dołączeniu go ma kształt ummanija „mojego wojska”, tak jak šarri(m) „króla” przechodzi w šarrija „mojego kró-la”.  
Mianownik i biernik liczby pojedynczej tych samych słów z omawianym przy-rost¬kiem brzmi natomiast ummani „moje wojsko” i šarri „mój król” czy „mojego kró-la”.  
Warto może zauważyć, że transliterację um-ma-ni można transkrybować, a potem in-terpretować zarówno jako ummani „wojska” (G l.p. bez mimacji), jak też jako ummani „wojsk” (G l.mn.) czy ummani „moje wojsko” (N/A l.p. z zaimkiem –i).

3.10. Zmiany spółgłosek przy stosowaniu przyrostków.  
š zaimków dzierżawczych –šu, -ša, -šunu oraz –šina asymiluje się ze spółgło-skami przedniojęzykowymi d, t oraz t, a także s, c, z i także š, przybierając postać ss, np.  
qassu (&lt;\*qat + šu) – jego ręka  
išissa (&lt;\*išid + ša) – jej podstawa  
ressunu (&lt;\*reš + šunu) – ich głowa  
Jednak dopełniacz liczby pojedynczej obywa się bez asymilacji:  
qatišu – jego ręki  
išdiša – jej podstawy  
rešišunu – ich gowy  
choć š zaimka wraca do starego ś.  
W przypadku słów qassu i išissa można obok normalnej pisowni qa-as-sú (także qa-sú) oraz i-ši-is-sa spotkać też niekiedy zapisy qa-at-sú i I-ši-id-sa. Ta pisownia nie odpowia-da zapewne wymowie, lecz powinna być odczytywana jako qassu i išissa.  
3.11. Zaimki dzierżawcze w liczbie mnogiej.  
Do form liczby mnogiej zakończonych na –utum, -atum i –etum zaimki dzier-żaw¬cze przyłącza się, odrzucając mimację i wydłużając samogłoski końcówek deklina-cyjnych. Np.  
N: cummiratuka – twoje życzenie  
G/A: cummiratiša – jej życzenia, jej życzenie  
Formy liczby mnogiej zakończone na –u (-i w G/A) stoją przed przyrostkami nie-zmienione:  
errušu – jego wnętrzności  
mariša – jego synów  
mûša – jego płyn.

3.12. Alternatywa dla status constructus.  
Stosunek zależności wyrażany przez złożenia imion typu „wojsko króla” jest w języku akadyjskim często wyrażany za pomocą status constructus (zob. 2.7.). Takie sa-mo jednak znaczenie ma też konstrukcja polegająca na tym, że imię w dopełniaczu następuje po zaimku określającym ša „który”, „która” stojącym z kolei po imieniu w status rectus. Np.  
ummanum ša šarrim – wojsko, które (jest) króla = wojsko króla.  
Tego typu wyrażenie szczególnie akcentuje składnik šarrim – inaczej niż znana już nam konstrukcja umman šarrim.  
Jest jeszcze inna konstrukcja, używana zwłaszcza w tekstach wróżbiarskich, któ-ra przekazuje podobne znaczenie: dwa imiona stoją obok siebie w takich samych przy-padkach, zaś zaimek dzierżawczy dołączony do drugiego z nich określa relację zależno-ści pomiędzy nimi:  
martum appaša – dosłownie: woreczek żółciowy, jego „nos” = „nos” woreczka żółciowego  
rubûm qassu – książę, jego ręka = ręka księcia  
Dwa różne sposoby wyrazu mogą być połączone:  
uban šarrim ša qatišu – palec króla, który jego ręki = palec ręki króla

3.13. Przerwa głosowa w nagłosie.  
Przerwa głosowa ‘ na początku słowa często nie była zapisywana specjalnym znakiem:  
al-pi-im = alpim (właściwie ‘alpim)  
e-li = eli (‘eli) – na  
Niekiedy jednak przed znakiem typu samogłoska plus spółgłoska przerwa gło-sowa mogła być zaznaczona dodatkowym znakiem samogłoskowym a, e, i albo ú:  
ú-ul = ul (‘ul) – nie  
a-al = al (‘al) – miasto (w status constructus)  
i-ir-tum = irtum (‘irtum) – pierś  
W trabskrypcji przerwa głosowa często nie jest zapisywana: pisze się alpum czy ul, a nie ‘alpum czy ‘ul.  
Znak samogłoskowy w zapisie tego rodzaju jak ú-ul czy a-al nie służył zazna-czeniu długości samogłoski (u w ul jest krótkie, zaś a w alum długie niezależnie od spo-sobu zapisywania).  
Lekcja czwarta

4.1. Formy imienne czasownika.  
Imienne twory odczasownikowe, a więc bezokolicznik (1.3.), przymiotnik odcza-sownikowy (4.2.) i imiesłów (4.3.), mogą być deklinowane. Od przymiotnika od-czasownikowego oraz imiesłowu można też tworzyć formy liczby mnogiej.

4.2. Przymiotnik odczasownikowy.  
Przymiotnik odczasownikowy jest „znominalizowanym” stativusem (1.5.). Wi¬dać to na przykładach:  
Stativus Przymiotnik odczasownikowy  
damiq – on jest dobry damqum – dobry  
damqat – ona jest dobra damiqtum – dobra  
damqu – oni są dobrzy damqutum – dobrzy  
damqa – one są dobre damqatum – dobre  
cabit – on jest złapany cabtum – więziony  
cabtat – ona jest złapana cabittum – więziona  
cabtu – oni są złapani cabtutum – więzieni  
cabta – one są złapane cabtatum – więzione  
Przymiotnik odczasownikowy może też być używany jako rzeczownik jak np.  
cabtum – jeniec.

4.3. Imiesłów.  
W przeciwieństwie do przymiotnika odczasownikowego, mającego charakter pasywny, imiesłów ma zawsze charakter aktywny. Budowę ma następującą: po pierw-szej spółgłosce rdzeniowej następuje samogłoska a, zaś po drugiej – i. Np.  
cabitum – chwytający; ten, który chwyta  
rakibum – jeżdżący; ten, który jeździ  
Imiesłów może również występować jako rzeczownik, w tym przyjmować formę status constructus:  
rakib imerim – jeździec na ośle.

4.4. Zmiana i w e.  
W sylabach kończących się na r i znajdujących się w wygłosie i przechodzi w e. Np.  
icher (&lt;\*ichir) – stał się małym, zmalał  
pater (&lt;\*patir) – on jest rozdzielony  
maher (&lt;\*mahir) – on jest przyjęty  
Jednakże imiesłów w tym ostatnim przypadku brzmi mahirum „przyjmujący”, gdyż przejście i w e zachodzi tylko w sylabie końcowej.

4.5. Zmiana a w e.  
W przypadku niektórych czasowników, których drugą lub trzecią spółgłoską jest r lub h, a przechodzi często w e.  
qerebum (&lt;qarabum) „być bliskim, niedalekim” tworzy w Prs m.in. formę iqer-rib „zbliża się”, a w St – formy qerub (zob. 19.10.) i qerbet „jest blisko”. Podobnie jest ze słowem ceherum (&lt;caharum) „być małym”, które w Prs przybiera m.in. postać ice-hher „maleje”, a w St ceher (4.4.) „on jest mały” i cehret „ona jest mała”.  
W przypadku paharum „zbierać, gromadzić” oraz maharum „przyjmować” sa-mogłoska a nie ulega zmianie: ipahhur „zbiera”, paher „jest zebrany”.

4.6. Verba primae alef.  
Szczególne formy gramatyczne tworzą czasowniki o rdzeniu zaczynającym się od przerwy głosowej (alefu). Dzielą się one na dwie grupy: a-klasę, do której należy np. akalum „jeść” oraz e-klasę z m.in. erebum „wchodzić”. Oto przykłady konstrukcji po-chodzących od tego rodzaju rdzeni:  
a-klasa:  
Kategoria gramatyczna Konstrukcja przykładowa Tłumaczenie  
bezokolicznik akalum (a/u) (&lt;’akalum) jeść  
3 osoba l.p. Prt ikul (&lt;i’kul) jadł, jadła  
3 osoba l.p. Prs ikkal (&lt;i’akkal) je  
imiesłów akilum (&lt;’akilum) jedzący  
3 osoba St r.m. akil (&lt;’akil) jest jedzony, jest jedzący  
3 osoba St r.ż. aklat jest jedzona, jest jedząca

e-klasa  
Kategoria gramatyczna Konstrukcja przykładowa Tłumaczenie  
bezokolicznik erebum (u) (&lt;’akalum) wchodzić  
3 osoba l.p. Prt irub (&lt;i’rub) wszedł, weszła  
3 osoba l.p. Prs irrub (&lt;i’errub) wchodzi  
imiesłów eribum (&lt;’eribum) wchodzący  
3 osoba St r.m. erib (&lt;’erib) jest wchodzący  
3 osoba St r.ż. erbet jest wchodząca

Na podstawie analizy zapisów sformułować można następujące reguły:  
– wypadanie alefu kończącego sylabę powoduje wydłużenie stojącej na początku samogłoski, na co wskazuje Prt,  
– stojący w nagłosie alef nie jest w bezokoliczniku, imiesłowie i stativusie zazna-czany,  
– Prs ma zawsze w zapisie znak samogłoskowy i, a zatem zawsze pisze się:  
i-ik-ka-al względnie i-ka-al – czytaj: ikkal,  
i-ir-ru-ub względnie i-ru-ub – czytaj: irrub,  
ale nigdy \*ik-ka-al czy \*ir-ru-ub (por. a-al, ú-ul – zob. 3.13.).  
Rodzaj żeński St i formy liczby mnogiej są tworzone normalnie:  
ikulu – oni jedli,  
irruba – one wchodzą,  
erbet – ona jest wchodząca.  
Pisownia i-ka-al i i-ru-ub form ikkal i irrub przypomina pisownię i-ca-ba-at słowa icab-bat. We wszyst¬kich tych przypadkach nie wyraża się w piśmie podwojenia spółgłoski (0.23.).  
Czasowniki a-klasy zaczynały się pierwotnie na ‘1 (= ‘) oraz ‘2 (=h) {por. np. akalum „jeść” z hebrajskim ‘akal albo alakum „iść” z hebrajskim halak}. Natomiast czasowniki e-klasy pierwotnie miały na początku alefy 3, 4 i 5 {por. erebum „wchodzić” z arabskim garaba}; w ich przypadku osłabienie ‘ jest spowodo¬wane przejściem a w e asymilują-cym drugą samogłoskę bezokolicznika.

4.7. Czasownik alakum „iść”.  
alakum jest czasownikiem nieregularnym. Jako jedyny czasownik trójspółgło-skowy przejawia wymianę samogłoskową a/i oraz tworzy Prt z podwojeniem drugiej spółgłoski:  
Prt: illik – on szedł (illiku – oni szli)  
Prs: illak – on idzie (illaku – oni idą).

4.8. Verba primae Jod.  
Czasowniki, których pierwszą spółgłoską rdzeniową jest j – jak ešerum (jšr) „być prostym, właściwym” – tworzą formy gramatyczne w sposób zbieżny z e-klasą czasow-ników zaczynających się alefem:  
Prt: išer – rozwijał się, był doprowadzany do stanu właściwego  
Prs: iššer – rozwija się.

4.9. Pojęcia abstrakcyjne kończące się na –utum.  
Końcówka –utum, za pomocą której tworzy się męską liczbę mnogą przymiotni-ków, służy też do konstruowania abstrakcyjnych pojęć rzeczownikowych mających licz-bę pojedynczą i będących rodzaju żeńskiego, np. harbutum „spustoszenie” od har¬bum „pusty, wyludniony” czy też ašaredutum „stanie na pierwszym miejscu, pierwsze miej-sce” od ašaredum „pierwszy, ważny”.  
Biernik pojęć abstrakcyjnych kończących się –utum łączy się z czasownikiem alakum „iść” w zwrotach takiego rodzaju jak np.  
ašaredutam alakum – zajmować, zająć pierwsze miejsce  
harbutam alakum – stawać się pustym, pustoszeć  
wacûtam alakum – odchodzić.

Ćwiczenia

šumma pa-da-nu šu-me-lam ip-šu-uq u libbi pu-uš-qi-im pa-te-er – jeśli „ścieżka” po lewej stronie zwęża się i środek zwężenia jest rozdzielony,  
šumma mar-tum i-ši-is-sa ce-he-er – jeśli podstawa woreczka żółciowego jest mała,  
šum-ma u-ba-na-tum ši-it-ta mu-ha-ši-na qu-u-m ca-bi-it u ka-ak-ki i-mi-it-ti mar-tum i-mi-id – jeśli wierzchołki dwóch „palców” węzeł tworzą i „broń prawicy” na woreczku żółciowym się opiera,  
šum-ma pa-da-nu iš-tu šu-me-lim i-mi-ti naplastim ik-šu-ud u šepum šu-me-lam er-be-et – jeśli „ścieżka” z lewej strony prawą część płatu wątroby osiąga i „stopa” na lewo wchodzi,  
šum-ma mar-tum a-na li-ib-bi ekallim i-ti-iq – jeśli woreczek żółciowy do środka „pa-łacu” przechodzi,  
ma-tum i-še-er – kraj rozwinie się,  
e-bu-ur ma-tim u-ul i-še-er – plon kraju nie rozwinie się,  
i-li-it-ti bu-li-im i-ce-he-er – potomstwo bydła będzie małe,  
e-ri-ib ekallim zu-qi-qi-pu i-za-qa-at – wchodzącego do pałacu skorpion ugryzie,  
i-na kakkim mi-qi-it-ti a-li-ik pa-ni um-ma-ni-ja – w potyczce klęska kroczącego na czele mojego wojska,  
alam ma-ru-uš-tam i-im-ma-ar – miasto nieszczęścia dozna,  
a-hu-um a-na bi-it a-hi-im i-ir-ru-ub – jeden do domu drugiego wejdzie,  
ne-šum i-na ta-ar-ba-ci awilim i-ik-ka-al – lew w ogrodzeniu człowieka będzie żarł,  
a-wi-lum ne-me-lam i-ma-ar – człowiek korzyści zazna,  
aš-ša-at a-wi-lim wa-cu-tam i-la-ak – małżonka człowieka odejdzie,  
e-bu-ur ma-tim na-ak-rum i-ka-al – plon kraju wróg zje,  
ma-tum ha-ar-bu-tam i-la-ak – kraj stanie się pustym,  
u-mu-um i-ru-up-ma ša-mu-um u-ul i-za-nu-un – dzień zachmurzy się ale deszcz nie spadnie,  
er-bu-um a-na ekallim i-ir-ru-ub – plon do pałacu wejdzie,  
a-wi-lum a-ša-re-du-tam i-la-ak – człowiek najwyższą rangę zdobędzie,  
i-na alim bi-it a-wi-li-im i-ša-tum i-ka-al – w mieście dom człowieka ogień pożre,  
alik harranim re-eš eqli-šu i-ka-ša-ad zi-tam i-kal – podróżny cel swój osiągnie udział otrzyma,  
cerum i-na harranim a-na pa-ni um-ma-nim i-še-er – wąż w drodze do czoła wojsk po-dejdzie,  
šumma iż-bu-um ki-ma ce-ri-im bu-lum i-ce-he-er – jeśli potworek jak wąż (wygląda) bydło będzie małe,  
šumma šu-me-el ubanim i-na iš-di-i-šu e-ek-me-et a-ša-re-ed na-ak-ri-i-ka qa-at-ka i-ka-ša-ad – jeśli lewa strona „palca” w jego dolnej części jest zmarniała najważniejszego z twoich wrogów ręka twoja dosięgnie,  
šumma bab ekallim ta-ri-ik-ma a-na li-bi-šu ši-ta u-ba-na-tu-ka i-ru-ba ekallam da-an-na-tum i-ca-ba-at – jeśli „brama pałacu” jest ciemna i do jej wnętrza dwa palce pasują pałac w kłopoty wpadnie,  
šumma bab ekallim e-pi-iq nakrum a-bu-ul-lam i-di-il – jeśli „brama pałacu” jest zaro-śnięta wróg bramę zamknie.

Lekcja piąta

5.1. Klasy znaczeniowe czasowników.  
Czasowniki języka akadyjskiego dzieli się wedle znaczenia na dwie grupy: cza-sowniki opisujące zdarzenia jak np. cabatum „chwytać” i czasowniki wyrażające stany jak np. damaqum „być dobrym”.  
Kategoria gramatyczna Czasownik stanu Czasownik czynny  
bezokolicznik damaqum – być dobrym cabatum – chwytać  
3 osoba l.p. Prt idmiq – był dobry icbat – chwycił  
3 osoba l.p. Prs idammiq – jest dobry icabbat – chwyta  
3 osoba St r.m. damiq – jest dobry cabit – jest chwycony  
imiesłów cabitum – chwytający  
przymiotnik odczas. damqum – dobry cabtum – chwycony

5.2. Czasowniki stanu.  
Za formy podstawowe czasowników stanu można byłoby właściwie uważać przymiotniki odczasownikowe; jedne na drugie daje się bez problemu „tłumaczyć”. Bezokoliczniki można oddawać w języku polskim ze słowem „być”. Formy czasów prze-szłego i teraźniejszego mają znaczenie ingresywne, tzn. wyrażają stany, które są w fazie początkowej.  
qatnum „rzadki”, qatanum (i) „być rzadkim”, iqtin „był rozrzedzany”, iqattin „jest rozrzedzany”  
cehrum „mały”, ceherum (i) „być małym”, icher „był pomniejszany”, icehher „jest pomniejszany”.  
Samogłoską rdzenną czasowników stanu jest prawie zawsze i.  
Imiesłowów nie można od nich utworzyć.

5.3. Czasowniki czynne.  
Za formę podstawową czasowników wyrażających zdarzenia można uważać bezokolicznik, od którego tworzy się czynny imiesłów oraz bierny przymiotnik odcza-sownikowy, np.  
epešum „robić”, epišum „robiący” i epšum „zrobiony”.  
Ze względu na samogłoskę rdzenną, która zresztą występuje tylko w Prt i Prs (zob. 1.4.), wyróżnia się cztery klasy tego rodzaju czasowników: o zmiennej spółgłosce rdzennej a/u oraz z samogłoskami a, i oraz u. Czasowniki czynne, których rdzeń za¬wiera a lub i, są w największej swej części przechodnie, natomiast te z samogłoską u są nie-przechodnie i oznaczają ruch lub zdarzenie.  
Bezokolicznik Czas przeszły Czas teraźniejszy  
kašadum (a/u) ikšud ikaššad  
cabatum (a) icbat icabbat  
edelum (i) idil iddil  
erebum (u) irub irrub  
erepum (u) irup irrup  
Od czasowników u-klasy – ogólnie biorąc – nie będą tworzone przymiotniki odczasow-nikowe.

5.4. Druga osoba czasów teraźniejszego i przeszłego.  
Utworzenie w liczbie pojedynczej tych czasów formy 2 osoby rodzaju męskiego następuje po prostu przez zastąpienie w formie 3 osoby przedrostka i- przez przedro-stek ta-. W liczbie mnogiej forma 2 osoby jest jednakowa dla obu rodzajów i jest two-rzona przez przedrostek ta- oraz końcówkę –a.  
Czas przeszły  
Liczba 3 osoba 2 osoba  
l.p. r.m. imhur – otrzymał, otrzymała tamhur – otrzymałeś  
l.mn. r.m. imhuru – otrzymali tamhura – otrzymaliście  
– otrzymałyście  
l.mn. r.ż. imhura – otrzymały  
l.p. r.m. icbat – chwycił, chwyciła tacbat – chwyciłeś  
l.mn. r.m. icbatu – chwycili tacbata – chwyciliście  
– chwyciłyście  
l.mn. r.ż. icbata – chwyciły

Czas teraźniejszy  
Liczba 3 osoba 2 osoba  
l.p. r.m. imahhar – otrzymuje tamahhar – otrzymujesz  
l.mn. r.m. imahharu – oni otrzymują tamahhara – otrzymujecie  
j.w.  
l.mn. r.ż. imahhara – one otrzymują  
l.p. r.m. icabbat – chwyta tacabbat – chwytasz  
l.mn. r.m. icabbatu – oni chwytają tacabbata – chwytacie  
j.w.  
l.mn. r.ż. icabbata – one chwytają  
Czasowniki z samogłoską e, jak qerebum (zob. 4.5.), zamiast przedrostka ta- przyłączają w 2 osobie przedrostek te-:  
teqerrib – zbliżasz się  
O tworzeniu 2 osoby liczby pojedynczej rodzaju żeńskiego jest mowa w punkcie 19.7.

5.5. Druga osoba w odmianie czasowników „primae alef”.  
Specyficzne dla czasowników o rdzeniu zaczynającym się na przerwę głosową jest to, że wypadnięcie kończącego sylabę alefu wydłuża stojącą na początku samogło-skę (zob. 4.6.). Trzeba też pamiętać, że przedrostek 2 osoby przybiera postać ta- dla czasowników należących do a-klasy oraz postać te- dla czasowników z e-klasy.  
Forma 3 osoba 2 osoba  
Prt l.p. ikul – jadł takul (&lt;ta’kul) – jadłeś  
Prs l.p. ikkal – je takkal (&lt;ta’akkal) – jesz  
Prt l.mn. r.m. ikulu – jedli takula (&lt;ta’kula) – jedliście  
Prt l.p. irub – wchodził terub (&lt;te’rub) – wchodziłeś  
Prs l.p. irrub – wchodzi terrub (&lt;te’errub) – wchodzisz  
Prs l.mn. r.m. irrubu – wchodzą terruba (&lt;te’erruba) – wchodzicie

5.6. Ventivus.  
W Prs, Prt i częściowo w St można za pomocą przyrostków –am i –nim tworzyć specjalne formy, które spotyka się przede wszystkim u czasowników ruchu. W języku akadyjskim rozróżnia się, czy ruch polega na zbliżaniu się, czy na oddalaniu. W tym pierwszym przypadku wchodzi w grę stosowanie czasowników z ventivusem. Np. illak „idzie” może wystąpić w postaci ventivusa illakam „przychodzi”.  
Co prawda, ventivus występuje też często w przypadku czasowników, które nie wyrażają ruchu. Różnica znaczenia pomiędzy formą normalną a tą z końcówką ven-tivusa często nie daje się uchwycić.

5.7. Tworzenie ventivusa w czasach przeszłym i teraźniejszym.  
Do form bez przyrostkowego elementu tworzącego (3 os. l.p. i 2 os. l.p. r.m.) dołącza się końcówkę –am, zaś do form z takim przyrostkiem (2 i 3 os. l.mn.) – koń-cówkę –nim. Np.  
iqribam – zbliżał się, zbliżała się  
taqribam – zbliżałeś się  
iqribunim – zbliżali się  
iqribanim – zbliżały się  
taqribanim – zbliżaliście się, zbliżałyście się  
irrubam – przychodzi  
tarrubam – przychodzisz (o podmiocie rodzaju męskiego)  
tarrubanim – przychodzicie  
irrubanim – one przychodzą  
irrubunim – oni przychodzą

5.8. Przyrostki w bierniku.  
Dopełnienie zaimkowe wyrażone być może przez przyrostek dołączający się do formy czasownika. Dla określenia prostego dopełnienia służy przyrostek w bierniku. Np. izzibu-ši „oni opuścili ją (ew. stracili ją)” (por. matam izzibu „opuścili kraj”).  
Najważniejszymi przyrostkami zaimkowymi w bierniku są:  
-ka – ciebie (r.m.)  
-šu – jego  
-ši – ją  
-šunuti – ich  
-šinati – je  
š przyrostków w 3 osobie asymiluje znajdujące się w wygłosie spółgłoski sy¬czące oraz przedniojęzykowe, tworząc z nimi zbitkę ss (zob. 3.10):  
icabbassu (&lt;icabbat+šu) – on chwyta go.  
Dopełnienie rzeczownikowe może być wyrażone pleonastycznie zarazem przez rzeczownik i przyrostek zaimkowy przy czasowniku. W tych bardzo częstych w tek¬stach wróżbiarskich konstrukcjach przyrostek akcentuje, podkreśla dopełnienie:  
ilu matam izzibuši – bogowie opuszczą kraj  
5.9. Przyrostki w celowniku.  
Do określania dopełnienia zależnego służą przyrostki w celowniku, które często dołączają się do czasownika w ventivusie, np.  
išapparakkum (&lt;išappar+am+kum) – on pisze tobie  
Najważniejszymi przyrostkami są:  
-kum – tobie (r.m.)  
-šum – jemu  
-šim – jej  
m przyrostków ventivusa –am i –nim asymiluje się ze spółgłoskami w nagłosie przyrostków w celowniku, np.  
irrubunikkum (&lt;irrubu+nim+kum) – zbliżają się ku tobie  
Niekiedy m przyrostka może odpadać:  
in-na-aš-ši-a-ku = inaššiakku – on będzie przynosił tobie

5.10. Zaprzeczenie.  
Do negacji zdania głównego służy ul lub ula (w zapisie ú-ul lub ú-la) „nie” sto-jące zawsze bezpośrednio przed orzeczeniem:  
šamûm ul izannum – deszcz nie pada  
W zdaniu podrzędnym do negacji służy słowo la, które może występować mię-dzy przyimkiem a rzeczownikiem:  
ina la lamadišu – w jego niedoświadczeniu.  
Ćwiczenia  
ajju-u-um-ma a-na mi-it-gu-ur-tim i-ša-pa-ra-ak-kum – ktoś w celu dojścia do porozu-mienia napisze do ciebie  
bi-ši ma-at nakrim a-na ma-ti-ka i-ir-ru-ba-am – majątek kraju wroga do twego kraju przyjdzie  
ajja-bu-u-ka a-na a-bu-ul-li-ka i-ru-bu-nim – wróg twój pod bramę twoją podejdzie  
nakrum er-ce-et-ka i-ri-iš-ka – wróg twojej ziemi zażąda od ciebie  
er-ce-et nakri-ka te-er-ri-iš – ziemi twego wroga zażądasz  
a-bu-um ar-bu-tam i-la-a-ak i-lu ma-a-tam i-zi-bu-u-ši – ojciec do ucieczki się rzuci bo-gowie kraj swój opuszczą  
ru-ba-am i-na bi-ti-šu qa-tum i-ca-ba-as-su – księcia w domu jego ręka pochwyci  
tu-ru-ku-tum a-na šar-ri-im i-qe-er-ri-bu-nim – Turukutowie do króla nadejdą  
mi-lum ga-ap-šum i-il-la-kam – powódź wielka nadejdzie  
mi-lum i-la-ka-am ša-nu-um šum-šu za-na-an ša-me-e-em a-ka-al ša-al-la-tim – po-wódź nadejdzie druga interpretacja wróżby padanie deszczu spożycie łupu  
u-mu-um i-ru-pa-am-ma ša-mu-um i-za-nu-un – dzień zachmurzy się i deszcz spadnie  
ti-bu-um ra-bu-um um-ma-na-am i-ka-aš-ša-da-am – natarcie wielkie wojsko pokona  
nakram qi-it-ru-bi-iš te-ki-im – wroga w walce wręcz weźmiesz do niewoli  
a-al na-ak-ri-im i-na pi-il-ši ta-ca-ba-at – miasto wroga dzięki wyłomowi weźmiesz  
a-wi-lam i-na la la-ma-di-šu ma-ru-uš-tum i-ma-aq-qu-ta-aš-šum – na człowieka z jego brakiem doświadczenia nieszczęście spadnie  
er-ce-et nakri-ka ta-ta-ab-ba-al – ziemię twojego wroga zabierzesz  
šumma bab ekallim ir-pi-iš ajja-bu-u-ka a-na a-bu-ul-li-im i-ru-bu-ni-ik-kum – jeśli „brama pałacu” rozszerza się wrogowie twoi do bramy twojej podejdą  
šumma cibtum e-li cibtim ir-ka-ab e-bu-ur ma-a-at na-ak-ri-i-ka ta-ka-al – jeśli narośl na narośli wierzchem jedzie plon kraju wroga twojego spożyjesz  
šum-ma na-ap-la-as-tum re-sa ip-tu-ur i-na re-eš eqlim um-ma-na-am i-lu-ša i-zi-bu-ši – jeśli wierzchołek płata wątroby rozszczepia się u celu wojsko bogowie jego opuszczą  
šumma kar-šum ki-ma di-ši-im ma-tam hu-ša-hu-um da-an-nu-um i-ca-ba-as-si – jeśli żołądek jak trawa kraj głód potężny nawiedzi.

Lekcja szósta

6.1. Czasowniki mocne i słabe.  
Czasowniki omawiane do tego czasu miały rdzenie składające się z trzech spół-gło¬sek. Określa się je jako trójspółgłoskowe lub mocne. Prócz tego istnieją różne grupy czasowników dwuspółgłoskowych czyli słabych, których rdzenie składają się z dwóch spółgłosek.

6.2. Czasowniki „ultimae vocalis”.  
Rdzenie dwuspółgłoskowe z długą samogłoską po drugiej ze spółgłosek, określa się jako czasowniki „ultimae vocalis” (albo też „ultimae infirmae” czy „tertiae infir-mae”), np.  
\*mla – być pełnym  
\*lqe (lq’) – brać,  
\*bni – rodzić,  
\*šqu – być wysokim.  
Te z czasowników omawianego rodzaju, których rdzenie zakończone są na e, nazywane „ultimae e”, takie jak leqûm „brać” czy tebûm „podnosić”, są w istocie trój-spółgłoskowe z alefem na końcu (\*lq’, \*tb’). Ponieważ jednak ich odmiana jest analo-giczna do odmiany czasowników „ultimae vocalis”, obie grupy będą tu omawiane ra-zem.

6.3. Odmiana czasowników „ultimae vocalis”.  
Tworzenie form tej grupy czasowników słabych jest w dużym stopniu podobne do tworzenia form czasowników mocnych:  
Prt Prs St  
imla – stał się pełnym imalla – staje się pełnym mali – on jest pełny  
ilqe – brał ileqqe – bierze leqi – on jest wzięty  
ibni – rodził, dokonywał ibanni – rodzi, tworzy bani – on jest urodzony  
išqu – stawał się wysokim išaqqu – staje się wysokim šaqu – on jest wysoki

W Prt przedrostek stoi przed rdzeniem. W Prs druga spółgłoska jest podwojona, a przed nią pojawia samogłoska czasu teraźniejszego a. W St samogłoska rdzenna ginie i jest zastępowana przez i, jedynie w przypadku czasowników zakończonych na u utrzymuje się u.  
Przed lub po h samogłoska a przechodzi w e:  
itehhi – zbliża się,  
hepi – jest rozbity.  
Samogłoska e pojawia się zamiast a w przypadku czasowników zakończonych na e (czyli na ‘):  
itebbe – podnosi się.  
We wszystkich formach samogłoska rdzenna ulega skróceniu, ale w Prt i Prf zjawia się na nowo wydłużona po przyłączeniu przyrostka lub –ma „i”:  
ta-ra-aš-ši-i-šu = taraššišu – będziesz miał jego  
nadima – jest położony i…  
oraz niekiedy na końcu zdania:  
i-ba-aš-ši-i (obok i-ba-aš-ši) – on jest.  
6.4. Czasowniki „ultimae vocalis” z końcówkami samogłoskowymi.  
Końcówki z samogłoską w nagłosie jak przyrostki 3 osoby l.mn. –u i -a (zob. 3.6.), 3 osoby l.p. r.ż. St –at (zob.2.6.) oraz ventivusa –am (zob. 5.7.) będą się „ściągać” z samogłoską rdzeniową, jedynie zbitki ia (ia) oraz ea (ea) zachowują się, choć nie zaw-sze:  
imlû, imlia (imlâ) – oni (one) stawały się pełnymi,  
itehhû, itehhia (itehhâ) – oni (one) zbliżają się,  
itebbeam – podnosi się,  
maliat (malât) – ona jest pełna.

6.5. Przerwa głosowa w śródgłosie.  
Przerwa głosowa między i a a lub e (zob. 6.4.) często nie będzie wyrażana spe-cjal¬nym znakiem:  
ma-li-at = maliat (właściwie: mali’at) – ona jest pełna.  
Jednakże równie dobrze przerwa głosowa może być oznaczana tak jak na po-czątku słowa (3.13.) czyli przez znak samogłoskowy a:  
li-pi-a-am = lipiam (lipi’am) – tłuszcz (w bierniku),  
i-te-eh-hi-a-am = itehhiam (itehhi’am) – zbliża się.  
W transkrypcjach (0.15.) przerwa głosowa ‘ jest często nie zaznaczana i pisze się lipiam, a nie lipi’am.

6.6. Druga osoba u czasowników „ultimae vocalis”.  
W 2 osobie l.p. i l.mn. przedrostek 3 osoby zastępowany jest przez ta-, który przybiera formę te- w przypadku czasowników z rdzeniem kończącym się na e (‘) oraz takich, które mają spółgłoskę h w rdzeniu.

6.7. Formy odczasownikowe czasowników „ultimae vocalis”.  
Tworzenie tych form jest podobne jak w przypadku czasowników trójspółgłosko-wych (zob. 4.1-4.3.). W imiesłowie samogłoska rdzeniowa jest zastąpiona przez i, a w bezokoliczniku przez a, ulegające przy tym „ściągnięciu”:  
Im: leqûm (&lt;\*leqium) – biorący  
Bezok: banûm (&lt;\*banaum) – rodzić.  
Przymiotnik odczasownikowy tworzony jest na podstawie 3 osoby St (zob. 6.3.) przez dołączenie końcówki –um. Przymiotnik rabûm „wielki” (od rabûm „być wiel¬kim”) tworzy rodzaj żeński „rabitum”; rodzaj żeński przymiotnika šaqum „wysoki” ma nato-miast postać šaqutum.

6.8. Samodzielne zaimki dzierżawcze.  
Obok zaimków dzierżawczych mających formę przyrostków istnieją też samo-dzielne takie zaimki. Są one szczególnie zaakcentowane i używane zwłaszcza wówczas, gdy chodzi o wyrażenie przeciwieństw typu mój – twój, twój – jego itp. Najważniej¬szymi z nich są:  
jûm (r.ż. jattum) – mój (moja)  
kûm (kattum) – twój (twoja)  
šûm (šattum) – jego (jej)  
nûm (nuttum) – nasz.  
Zaimki te traktowane są jak przymiotniki, bo wchodzą w ich rolę w konstruk-cjach słownych i mogą być deklinowane:  
matam la kattam – nie twój kraj (=kraj nie należący do ciebie)  
mimma la šâm – coś nie należącego do niego.  
W przypadku liczby mnogiej por. qarradu ja’utun „moi bohaterzy”.

6.9. Zaimek nieokreślony mimma.  
Nieodmienne mimma „cokolwiek, wszystko (co)” w połączeniu z negacją może być stawiane przed rzeczownikiem jako apozycja (dopowiedzenie).  
mimma nudunnâm – wszystko posagu = cały posag  
mimma la šâm – coś, co do niego nie należy.

6.10. Konstrukcje z biernikiem.  
W przypadku czasowników takich jak np. malûm „być pełnym” czy lapatum „dotykać” dopełnienie w języku akadyjskim wyrażane jest przez biernik:  
martum lipiam maliat – wątroba jest pełna tłuszczu  
martum damam laptat – woreczek żółciowy jest zamazany krwią.

6.11. Asymilacja głosek wargowych.  
b przed m i niekiedy p przed m są asymilowane:  
terrumma (&lt;terub+ma) – idziesz i…  
izimma (&lt;izib+ma) – porzucił i…  
takimma (&lt;takip+ma) – jest zabrudzony i…  
To samo zjawisko występuje też niekiedy w przypadku status constructus:  
hicim matim (&lt;hicib matim) – plon kraju.  
Także n przed m może być asymilowane:  
šakimma (normalnie šakinma) – jest położony i…  
Ćwiczenia:

šumma iz-bu-um qi-na-as-su pe-ta-at – kiedy odbyt potworka jest otwarty  
šum-ma mar-tum li-ib-ba-ša li-pi-a-am ma-li – kiedy środek woreczka żółciowego jest pełny tłuszczu  
šum-ma mar-tum da-ma-am la-ap-ta-at – kiedy woreczek żółciowy zamazany jest krwią  
ka-al-bu i-še-gu-u – psy będą się wściekać  
e-ka-lum e-ka-lam i-re-ed-de – pałac (inny) pałac przejmie  
te-es-li-it awilim ilum iš-me – modlitwę człowieka bóg usłyszał  
na-ka-ar-ka i-na bi-ti-i-ka mi-im-ma la ša-a-am i-le-qe – wróg twój z domu twojego coś co nie jest jego zabierze  
i-na bi-ti na-ak-ri-i-ka mi-im-ma la ka-a-am te-le-qe – z domu wroga twojego coś co nie jest twoje zabierzesz  
ta-ap-pa-am ta-ra-aš-ši-i-ma na-ka-ar-ka ta-da-ak – towarzysza pozyskasz i wroga two-jego zabijesz  
na-ka-ar-ka ta-ap-pa-am i-ra-aš-ši-i-ma i-da-ak-ka – wróg twój towarzysza pozyska i zabije ciebie  
šar-ru ha-am-me-e i-te-eb-bu-u-nim – królowie rebeliantów zbuntują się  
ma-ri ši-ip-ri-im ša ma-a-tim re-eq-tim a-na šarrim i-te-eh-hi-a-am – poseł kraju dale-kiego do króla przybędzie  
mi-lik ma-a-tim i-ša-an-ni – rada kraju zmieni się  
ka-ab-tum ki-ma be-li-šu i-ma-ac-ci – ważny jak pan jego stanie się  
hi-ci-im ma-tim a-na e-kal-lim i-ru-ub – plon kraju do pałacu przyjdzie  
ma-at nakrim ta-ca-ab-ba-at a-na i-še-er-ti nakrim te-ru-um-ma ni-qi-am ta-na-qi-i – kraj wroga posiądziesz do świątyni wroga wstąpisz ofiarę złożysz  
bu-su-ra-at lu-um-ni-im a-na bi-ti awilim i-te-hi-a – wieści zła do domu człowieka na-dejdą  
šarrum er-ce-tam la ša-tam u ma-tam la ša-tam qa-as-su i-ka-aš-ša-ad – ręka króla ziemię nie jej i kraj nie jej zagarnie  
la me-he-er-ka i-te-be-a-ku-um-ma i-da-ak-ka – nierówny tobie powstanie przeciw to-bie i zabije cię  
ka-ar-ra-du ja-u-tu-un u ša nakrim iš-te-ni-iš i-ma-qu-tu – bohaterowie moi i wroga ra-zem padną  
šum-ma mar-tum ma-li-at-ma mu-ša wa-ar-qu ra-du-um i-la-ak – jeśli woreczek żółcio-wy jest pełny i jego płyn jest żółty burza przyjdzie  
šum-ma mar-tum i-ši-is-sa le-ti šu-cu-um a-na ma-tim i-te-eb-be-a-am – jeśli podstawa woreczka żółciowego jest rozdzielona wygnaniec w kraju powstanie  
šumma kakki i-mi-tim i-na re-eš mar-tim ša-ki-im-ma mar-tam ir-de-e kakki qu-li-im – jeśli „broń prawicy” na wierzchołku woreczka żółciowego jest położona i woreczek żół-ciowy porusza się broń płaczu  
šumma martum re-sa ki-ma ku-ub-ši-im šarrum la-ma-sa-am i-ra-aš-ši – jeśli wierz-chołek woreczka żółciowego (jest) jak czapka król bóstwo opiekuńcze pozyska  
šumma martum da-ma-am dan-na-am ma-li-at su-un-qum ma-tam i-ca-ba-at – jeśli woreczek żółciowy krwi wyciekłej jest pełny bieda na kraj spadnie  
šum-ma cibtum su-ma-am ca-ar-pa-at wi-li-id bu-lim i-še-er – jeśli narośl czerwoną plamą zabarwiona jest na czerwono potomstwo bydła rozwinie się  
šumma bab ekallim ma-aš-ka-an-šu i-zi-im-ma i-na šumelim ša-ki-in ma-tam la ka-tam nakrum i-ka-ša-ad – jeśli „brama pałacu” miejsce swe porzuciła i na lewo jest położona kraj nie twój wróg zdobędzie  
šumma ubanam ka-ak-kum ša-ap-li-iš ra-ki-ib ka-ak-kum nu-u-um – jeśli „broń” u dołu „palca” dosiada broń nasza  
šumma ubanam ka-ak-kum e-li-iš ra-ki-ib ka-ak-ki na-ak-ri-im – jeśli „broń” u góry „palca” dosiada broń wroga  
šum-ma mar-tum mu-uh-ha-ša ta-ki-im-ma ca-ri-ip mi-qi-it-ti barim – jeśli wierzchnia część woreczka żółciowego jest pstra i zabarwiona na czerwono klęska wróżbity  
šum-ma na-ap-la-as-tum e-li-iš iš-qu i-lu ša ma-tim i-ša-aq-qu-u – jeśli płat wątroby u góry staje się wysokim bogowie kraju staną się ważnymi

Lekcja siódma

7.1. Czasowniki „mediae vocalis”.  
Tak określa się rdzenie składające się z dwóch spółgłosek i długiej samogłoski pomiędzy nimi; nazywa się je też czasownikami „mediae infirmae” lub „rdzeniami wgłębionymi”. Samogłoskami rdzeniowymi mogą być w ich przypadku i lub u, a także – choć rzadko – a. Są to zarówno czasowniki stanu, np. tiabum (\*tib) „być dobrym, pięk-nym”, miadum (\*mid) „być licznym” czy kânum (\*kun) „być prawdziwym, trwa¬łym”, jak i czasowniki działania, np. šiamum (\*šim) „określić, wyznaczać”, dâkum (\*duk) „zabijać” czy târum (\*tur) „wracać”.  
Odmiana tych czasowników odbiega znacznie od odmiany czasowników moc-nych.

7.2. Czasy przeszły i teraźniejszy czasowników „mediae vocalis”.  
W Prt przed rdzeniem stoi przedrostek:  
3 osoba l.p.: itur – powrócił, powróciła  
2 osoba r.m. l.p.: tatur – powróciłeś  
3 osoby l.mn.: ituru, itura – powrócili, powróciły  
2 osoby l.mn.: taturu, tatura – powróciliście, powróciłyście.  
Podobnie  
išim – wyznaczył, wyznaczyła  
tašim, išimu itd.  
W Prs jest różnica pomiędzy formami bez końcówki takimi jak 3 osoba l.p. i 2 osoba r.m. l.p., w których po samogłosce rdzeniowej stoi samogłoska czasu teraźniej-szego a, a formami z końcówką takimi jak 3 i 2 osoba l.mn. czy ventivus, w przypadku których druga spółgłoska ulega podwojeniu:  
išiam – wyznacza, ale – išimmu, išimma  
itûr (&lt;ituar) – powraca, ale – iturram – wraca (zbliżając się).  
Samogłoska rdzeniowa przed podwojoną spółgłoską skraca się. Można też za-uwa¬żyć, że prawie zawsze zbitka ua ulega ściągnięciu w â, natomiast zbitka ia rzadko tylko przechodzi w â:  
idâk (&lt;iduak) – zabija, ale iqiap – ufa.  
Pozostałe formy tworzy się podobnie:  
2 osoba r.m. l.p.: tadâk (&lt;taduak) – zabijasz  
3 osoba r.ż. l.mn.: imidda – stają się liczne.

7.3. Stativus i formy imienne czasowników „mediae vocalis”.  
St zarówno czasowników „mediae i”, jak i czasowników „mediae u” zawiera samogłoskę i:  
kin – jest trwały  
kinat – jest trwała  
kinu – są trwali  
kina – są trwałe  
šim – jest określony.  
Niektóre czasowniki stanu tworzą St z samogłoską a:  
mad – jest liczny  
sam – jest czerwony  
tab – jest dobry.  
Przymiotniki odczasownikowe rodzaju męskiego mają formę pochodną od 3 osoby St:  
kinum – prawdziwy, rodzaj żeński kittum – prawdziwa (jako rzeczownik: praw-da)  
šimum – wyznaczony, r.ż. šimtum (jako rzeczownik: los)  
madum, r.ż. mattum – wielu, wiele  
samum, r.ż. samtum – czerwony, czerwona.  
W bezokoliczniku po skróconej samogłosce rdzeniowej następuje samogłoska a:  
dâkum (&lt;duakum) – zabijać  
šiamum – wyznaczać.

7.4. Tryb łączący (subiunctivus).  
Czasownik zdań zależnych przybiera formę subiunctivusa:  
ikšud – zdobył, ale: ša… ikšudu – który zdobył.  
Końcówką subiuktivusa jest –u; dołącza się ona jednak tylko do form czasowni-ko¬wych nie posiadających jeszcze innej końcówki, takich jak 3 osoba l.p. i 2 osoba l.p. rodzaju męskiego; formy czasownikowe z końcówką, jak 3 czy 2 osoba l.mn. czy ven-tivus, nie przybierają w subiunctivusie żadnej szczególnej postaci:  
iduku – zabili; (którzy) zabili,  
illikam – przyszedł; (który) przyszedł.  
Końcówka subiunctivusa następuje też po 3 osobie l.p. rodzaju męskiego St:  
ša nakru – który jest wrogi,  
ša mitu – który jest martwy.  
W przypadku czasowników z samogłoską na końcu –u będzie ulegało ściągnię¬ciu z samogłoską rdzeniową:  
ša ikmû (&lt;\*ikmiu) – który brał do niewoli.  
Jeżeli po formie subiunktywnej następuje przyrostek, u wydłuża się:  
ša ikšudušu – który go zdobył.  
Niekiedy można spotkać pisownię subiunctivusa z samogłoską długą:  
ša na-ak-ru-ú – który jest wrogi.  
7.5. Zdanie względne.  
Akadyjskie zdanie względne można traktować jako dopełnieniowe. Stoi ono przy rzeczowniku, którego bliższemu określeniu służy, pozostając w takim samym do niego stosunku jak rectum do regens w połączeniu dopełnieniowym. Może być łączone z rzeczownikiem za pomocą zaimka ša:  
Kubaba ša šarrutam icbatu – Kubaba, który zawładnął królestwem.  
Jednak ša może zabraknąć, a wtedy regens przybiera formę status constructus:  
harran illaku – droga, którą będzie chodził = wyprawa, którą podejmie.  
W obu przypadkach czasownik w zdaniu względnym przybiera formę subjunk-tywu.  
Miejscowe zdanie poboczne rozpoczynające się od ašar „dokąd, gdzie” też jest właściwie zdaniem względnym tworzonym przy użyciu formy status constructus:  
ašar illaku – miejsce, (do) którego idzie = dokąd idzie.

7.6. Liczba podwójna.  
Obok liczby pojedynczej i mnogiej imiona i czasowniki mogą też tworzyć liczbę podwójną. Oznacza ona ilość dokładnie dwóch osób czy czynności:  
qarradan šena – dwaj bohaterzy  
i parzystych części ciała:  
inan – oczy.  
Są takie szczególne słowa, które występują tylko w liczbie podwójnej:  
qablan – część środkowa  
išdan – część dolna, fundament.  
Liczba podwójna występować może wraz ze słowem šena „dwa” (r.ż. šitta) lub bez niego.  
7.7. Liczba podwójna u imion.  
W liczbie podwójnej nie rozróżnia się rodzajów męskiego i żeńskiego. Tak jak w liczbie mnogiej są tu dwa przypadki: N z końcówką –an i G/A z końcówką –in:  
šepan – stopy (dwie)  
šepin – stóp, stopy (dwie).  
W formie status constructus, jak i po przyłączeniu przyrostka głoska n ginie:  
ini awilim – oczy człowieka  
qablaša – jej środek.  
Przymiotnik ma w liczbie podwójnej końcówki liczby mnogiej rodzaju żeń¬skiego:  
šepašu warkiatum – jego tylne stopy.  
U rzeczowników rodzaju żeńskiego zakończonych na –tum liczba podwójna za-stępowana jest przez liczbę mnogą:  
marratum šitta – dwa woreczki żółciowe.  
n końcówki liczby podwójnej często znika:  
šepa, šepi.

7.8. Liczba podwójna u czasowników.  
Formy liczby podwójnej 3 osoby są zawsze identyczne z formami 3 osoby liczby mnogiej rodzaju żeńskiego:  
irruba – one wchodzą i – obaj (oboje, oba) wchodzą  
šakna – są położone i – obaj (oboje, oba) są położone.

Ćwiczenia

šumma iz-bu-um pa-ni imerim še-pa-šu ma-ah-ri-a-tum še-ep nešim še-pa-šu wa-ar-ki-a-tim še-ep imerim – jeśli potworek twarz osła (ma) jego nogi przednie stopami lwa (są) jego stopy tylnie stopami osła (są)  
šumma i-na re-eš naplastim zi-hu na-di-ma u-sa-a-am – jeśli na wierzchołku płatu wą-troby pęcherzyk jest położony i jest czerwony  
šum-ma mar-tum mu-ša a-na še-na zi-i-zu – jeśli żółć na dwoje jest podzielona  
šumma uban ha-ši qablitum a-na še-na zi-za-at-ma i-na i-mi-tim te-hi-a-at – jeśli środ-kowy palec płucny na dwoje jest podzielony i do prawej strony jest zbliżony  
šum-ma cibtum a-na li-ib-bi-im i-tu-ur – jeśli narośl do środka się odwraca  
šum-ma ma-ra-tum šitta – jeśli woreczki żółciowe dwa (są)  
um-ma-ni it-ti ilim a-na da-ki-im er-še-et – moje wojsko za zgodą boga do zabijania zostało wezwane  
kakki ši-im-tim ša a-wi-lim – broń przeznaczenia człowieka  
be-el ma-a-tim i-ma-a-at – pan kraju umrze  
šar-ru-um i-na a-li-šu li-ib-ba-šu u-la i-ti-a-ab – król w mieście jego serce jego nie bę-dzie zadowolone \[= serce króla w jego mieście nie będzie zadowolone\]  
šarrum ma-as-su u wa-ar-di-šu i-qi-a-ap – król krajowi swemu i sługom swoim zaufa  
ekallum i-ri-aq – pałac opustoszeje  
be-el sa-li-mi-ka i-ma-at-ma a-al-šu ta-la-wi – pan twojego traktatu pokojowego umrze i miasto jego otoczysz  
be-lam i-na šu-ub-ti-šu kakkum i-da-ak-šu – pana w mieszkaniu jego broń zabije  
ma-ku-ur a-wi-lim i-cu-um a-na ma-di i-ta-ar – majątek człowieka nieliczny powiększy się  
na-ra-ma-at šar-ri-im i-mi-id-da – nałożnice króla wzrosną w liczbę  
qar-ra-da-an šena i-mu-ut-ta-nim – dwaj bohaterzy umrą  
šar-rum a-na še-na ma-ri-šu ma-as-su i-za-az – król na dwie części dla synów swoich kraj swój podzieli  
na-ak-ru-um iš-da-a-šu da-an-na – podstawy wroga (są) mocne  
iš-da nakrim ki-na-a-tim – podstawy wroga (są) pewne  
i-ni a-wi-lim šar-rum i-na-as-sa-ah – oczy człowieka król wyrwie  
a-mu-ut dlu-hu-ši-im ša a-wi-lum i-na bu-ul-ti-šu mi-tu – wróżba (dla) Luhuszum który (jak każdy) człowiek o czasie życia swego umrze  
šar-ra-am ša it-ti-i-ka na-ak-ru-u i-na kakki ta-da-ak-ma a-al-šu na-wi-šu er-ce-es-su qa-at-ka i-ka-aš-ša-ad – króla który z tobą był w nieprzyjaźni w walce zabijesz i miasto jego obóz jego ziemię jego ręka twoja zdobędzie  
a-mu-ut ša-ar-ka-al-šar-ri ša wa-ar-du-u-šu i-na ku-nu-uk-ka-ti-šu-nu i-du-ku-u-šu – wróżba (dla) Szarkalszarri którego słudzy jego ze swymi tabliczkami zabiją  
a-mu-ut a-pi-ša-li-im ša mna-ra-am-dsin ik-šu-du-u-šu – wróżba (dla) Apiszalum które Naramsina pokonało  
a-wa-at ku-ub-a-ba ša ša-ar-ru-ta-am ic-ba-tu – sprawa Kubaby który władzę królew¬ską zagarnął  
a-mu-ut šul-gi ša tap-pa-dda-ra-ah ik-mu-u – wróżba (dla) Szulgi który Tappa-Darah wziął w niewolę  
um-ma-an-ka a-šar i-la-ku zi-tam i-kal – wojsko twoje gdziekolwiek pójdzie łup spo¬żyje  
um-ma-nu harran i-la-ku u-ul i-tu-ra-am – wojska na wyprawę pójdą nie wrócą  
šum-ma mar-tum qa-ab-la-ša ca-ab-ta-a šar-ra-am šu-ut-re-ši-i-šu i-du-uk-ku-šu – jeśli woreczka żółciowego część środkowa jest chwycona króla dworzanie jego zabiją  
šum-ma mar-tum ap-pa-as-sa u i-ši-is-sa ki-na-a-ma qa-ab-la-a-ša na-as-ha mi-il-kum sa-pi-ih – jeśli „nos” woreczka żółciowego i jego podstawa są stałe i jego część środ-kowa jest wyrwana plan będzie zniweczony  
Lekcja ósma

8.1. Augmenty rdzenne.  
Dwuspółgłoskowe rdzenie z krótką samogłoską między spółgłoskami mogą się upodabniać do czasowników mocnych dzięki przyłączeniu augmentu rdzennego n albo w.

8.2. Czasowniki zaczynające się na n.  
Za pomocą augmentu rdzennego n tworzone są przede wszystkim dwie grupy czasowników:  
określające kierunek jak nadanum „dawać”, natalum „spoglądać”, našûm „pod-nieść” i nadûm „położyć”, a także  
dźwiękonaśladowcze, tzn takie, których rdzeń naśladuje jakiś dźwięk jak naka-sum (\*kis) „odciąć”, natakum (\*tuk) „kapać”, nabahum (\*buh) „szczekać”.  
Nie ma różnicy w odmianie czasowników dwuspółgłoskowych z augmentem rdzennym n oraz czasowników trójspółgłoskowych zaczynających się na n. Do tych ostatnich należą przede wszystkim czasowniki stanu takie jak nawarum (i) „być błysz-czącym” i nadarum (i) „być wściekłym, złym”.

8.3. Odmiana czasowników zaczynających się na n.  
Czasowniki te odmieniają się tak samo jak trójspółgłoskowe, tyle tylko że n jest asymilowane przez następną spółgłoskę:  
Prs: inattal – spogląda, ale  
Prt: ittul (&lt;\*intul) – spojrzał.

8.4. Czasowniki zaczynające się na n z samogłoską na końcu.  
Są to np. našûm (nši) „podnieść”, nadûm (ndi) „położyć” czy naqûm (nqi) „skła-dać w ofierze”.  
Tworzenie form przebiega w ich przypadku identycznie jak u czasowników ulti-mae vocalis (6.2.):  
Prs: inašši – podnosi  
Prt: išši (&lt;\*inši) – podniósł.

8.5. Czasowniki zaczynające się na w.  
Wśród tych słów ważne jest rozróżnienie na czasowniki czynne z augmentem rdzennym w jak np. wabalum „nieść”, waladum „rodzić” czy wašabum „siadać”  
oraz na czasowniki stanu zaczynające się na spółgłoskę w będącą naturalnym składnikiem rdzenia jak np. waraqum „być zielonym, być żółtym”.

8.6. Czasy teraźniejszy i przeszły czasowników zaczynających się na w.  
W przypadku czasowników czynnych przedrostki 2 i 3 osoby ta- oraz i- zostają zastąpione przez tu- i u-:  
uššab – siada  
tuššab – siadasz.  
Prs zawiera samogłoskę a, a Prt samogłoskę i (por. odmianę alakum 4.7.), po-nadto w Prs pierwsza spółgłoska rdzeniowa podwaja się, zaś w Prt przedrostek ulega wzdłużeniu:  
ullad – rodzi  
ulid – urodziła.  
Po dołączeniu końcówki forma Prt traci samogłoskę i:  
ubil – (wy)niósł, ale  
ublam – przyniósł i ublu – nieśli.  
Czasowniki stanu, jak waraqum „być zielonym” tworzą Prs i Prt podobnie jak e-klasa czasowników primae alef (4.6.):  
irriq – będzie zielony  
iriq – był zielony.  
Długa samogłoska u w głoskach zamkniętych skraca się, stąd ubil, ale ublam.

8.7. Formy imienne czasowników zaczynających się na w.  
Formy imienne tworzone są jednakowo dla obu grup omawianych czasowników wedle wzorców stosowanych w przypadku czasowników trójspółgłoskowych:  
Im: wašibum – siedzący  
St: wariq – on jest zielony, warqu – oni są zieloni.

8.8. Czasowniki na w zakończone samogłoską.  
Np. wacûm (wci) „wychodzić” i watûm (wta) „znajdować”:  
Prs: ucci – wychodzi, utta – znajduje  
Prt: uci – wychodził  
St: waci – jest wychodzący = zwisa  
Im: wacûm (&lt;wacium) – wychodzący.

8.9. Status absolutus.  
Liczebniki główne, imiona bogów oraz pewne inne wyrazy mają szczególną for-mę status absolutus. Jest ona pozbawiona końcówki dla rodzaju męskiego, zaś dla ro-dzaju żeńskiego przyłącza końcówkę –at. Formalnie status absolutus odpowiada sta¬tus constructus bez zależnego dopełniacza lub formie 3 osoby St. Status absolutus może nie podlegać odmianie:  
ana alim išten – w jednym mieście  
dŠamaš – Szamasz, bóg słońca, ale: šamšum – słońce  
ašar išten – na jednym miejscu.

8.10. Liczebniki główne.  
Najważniejsze liczebniki to:  
išten (r.ż. ištiat) – jeden (jedna)  
šena (šitta) – dwa (dwie)  
šalaš (šalašat) – trzy  
erbe (erbet) – cztery  
hamiš (hamšat) – pięć  
sebe (šebet) – siedem  
ešer (ešeret) – dziesięć.

8.11. Konstrukcje z liczebnikami głównymi.  
Liczebniki stoją najczęściej przed wyrazami przez nie określanymi, jednakże jeśli w konstrukcji chodzi o szczególne podkreślenie ilości, umieszczane są po nich. Tylko liczebniki jeden i dwa stawiane są w odpowiednim rodzaju po wyrazie określa¬nym:  
ana alim išten – w jednym mieście  
ubanatum šitta – dwa palce.  
Liczebniki od 3 do 10 przybierają formę rodzaju żeńskiego przy połączeniu z rzeczownikiem rodzaju męskiego i na odwrót:  
marratum sebe – siedem woreczków żółciowych  
erbet išuššu – cztery (są) jego szczęki.  
Liczebnik jeden łączy się z formą w liczbie pojedynczej, a liczebnik dwa najczę-ściej z rzeczownikiem rodzaju męskiego w liczbie podwójnej i rzeczownikiem rodzaju żeńskiego w liczbie mnogiej:  
qarradan šena – dwaj bohaterzy  
ubanatum šitta – dwa palce.  
Liczebniki od 3 najczęściej łączą się z liczbą mnogą.  
Liczebniki mogą w pewnych połączeniach być konstruowane tak jak rzeczow¬niki:  
šar erbettišu – po.

8.12. Liczebniki mnożne.  
Są to np.  
šenišu – dwa razy  
šalašišu – trzy razy  
erbišu – cztery razy  
hamšišu – pięć razy.  
Mogą się one łączyć z przyimkami ana lub adi:  
ana šalašišu – trzy razy  
adi hamšišu – pięć razy.

8.13. Liczebniki porządkowe.  
Są to:  
mahrûm (r.ż. mahritum) – pierwszy (pierwsza)  
šanûm (šanitum) – drugi (druga)  
šalšum (šaluštum) – trzeci (trzecia)  
rebûm (rebutum) – czwarty (czwarta)  
hamšum (hamuštum) – piąty (piąta).  
Postępuje się z nimi tak samo jak z przymiotnikami.

8.14. Partykuły.  
Partykuła enklityczna –ma służy do szczególnego zaakcentowania poszczegól-nych słów i często jest tłumaczona przez słowo „tylko”:  
šumma izbum inšu ištiatma – jeśli oko potworka jest tylko jedno.  
Do łączenia zdań służy obok –ma „i”, „i potem”(zob. 1.9.) także u „i”, „ale”, „także”, „i ponad to” oraz –ma u „i także”, „i jednak”.

Ćwiczenia

šum-ma ma-ra-tum šalaš – jeśli (są) trzy woreczki żółciowe  
šum-ma ma-ra-tum hamiš – jeśli (jest) pięć woreczków żółciowych  
šumma re-eš bab ekallim a-na še-ni-šu (a-na ša-la-ši-šu) pa-te-er – jeśli wierzchołek „bramy pałacu” dwa razy (trzy razy) jest przedzielony  
šumma si-pi šu-me-el bab ekallim a-na er-bi-i-šu pa-te-er – jeśli bok lewej strony „bra-my pałacu” czterokrotnie jest przedzielony  
šumma iz-bu-um er-be-et i-su-šu – jeśli potworek cztery szczęki (ma)  
šumma iz-bu-um i-in-šu iš-ti-a-at-ma – jeśli oko potworka (jest) tylko jedno  
šumma iz-bu-um qa-qa-as-su ka-ajja-nu-um ša-ki-in-ma u iš-tu li-ib-bi pi-šu qa-qa-as-su ša-nu-um wa-ci – jeśli głowa potworka normalnie jest położona ale z wnętrza jego ust druga jego głowa zwisa  
šumma sinništum u-li-id-ma i-na u-su-uk-ki-šu ša šu-me-lim su-mu na-di – jeśli kobieta urodzi i na jego (noworodka) pośladku lewym czerwona plama się znajduje  
šumma a-mu-tum ša-ar er-be-ti-ša še-pe-tim ma-li-a-at – jeśli wątroba wszędzie \[dosł. ze czterech stron świata\] „nóg” jest pełna  
ca-ab-tum u-ci-i – jeniec ucieknie  
šarram ši-bu-ut a-li-šu i-na-sa-hu-šu – króla starsi jego miasta usuną  
aš-ša-at šar-ri-im zi-ka-ra-am ul-la-ad – małżonka króla chłopca urodzi  
a-wi-lum ha-li-iq-ta-šu u-ta-a – człowiek utracone dobro odnajdzie  
šep na-ah-ra-ri-im a-na na-ah-ra-ri tu-uc-ci – „noga” pomocy na pmoc wyruszysz  
ma-a-at nakrim bi-il-tam i-na-aš-ši-a-ku – kraj wroga trybut przyniesie ci  
a-mu-ut a-ku-ki ša ma-a-tum bi-il-tam iš-ši-a-šum – wróżba Akuki któremu kraj trybut przyniósł  
ma-ri ši-ip-ri ma-ah-ru-u-um bu-šu-ra-at ha-de-e-em na-ši-kum – pierwszy goniec wie-ści radości przyniesie tobie  
a-di-ra-at nakrim i-na li-ib-bi um-ma-ni na-da-a – strach wroga \[=przed wrogiem\] w sercu mojego wojska zakorzeni się  
wa-ši-ib ma-ah-ri-i-ka a-wa-ti-ka i-za-ab-bi-il – siedzący przed tobą słowa twoje zdra¬dzi  
wa-ci a-bu-ul-li-ja na-ak-rum i-da-ak – wychodzącego z mojej bramy wróg zabije  
ma-a-tum ka-lu-u-ša a-na a-li-im iš-te-en i-pa-hu-ur – kraj cały w mieście jednym się zbierze  
šumma martim ši-rum appa-ša i-im-še-ma re-sa ip-lu-uš-ma u-ci a-mu-ut šar-ru-ki-in ša eq-le-tam il-li-ku-ma nu-ru-um u-ci-aš-šu-um – jeśli „nos” woreczka zółciowego tka-nina zakrywa ale czubek jego przebija i wystaje wróżba Szarrukina który ciemność przebył i (któremu) światło wzejdzie \[wyjdzie doń\]  
šumma re-eš martim na-we-er ma-az-za-az dištar – jeśli wierzchołek woreczka żółcio-wego świeci pozycja Isztar  
šum-ma mar-tum i-na a-bu-ul-lim na-di-a-at ne-ku-ur-tum da-an-na-tum – jeśli wore-czek żółciowy w bramie jest położony wrogość potężna  
šumma martim kakkam ib-ni-ma šu-me-lam it-tu-ul ka-al pa-ni-ka te-le-eq-qe-e – jeśli woreczek żółciowy „broń” tworzy i na lewo spogląda wszystko co przed tobą weźmiesz  
šumma ma-ra-a-tum sebe šar kiššatim – jeśli woreczków żółciowych siedem król świa-ta  
šum-ma šitta ma-ra-tum ma-ac-ra-ah-ši-na iš-te-en-ma a-mu-ut sa-li-mi-im – jeśli dwóch woreczków żółciowych zaczątek (jest) tylko jeden wróżba propozycji pokoju  
šum-ma i-na ce-li-im ša-al-ši-im ša šu-me-lim su-mu-um na-di e-ri-iš-ti im-me-ri-im – jeśli na żebrze trzecim lewym czerwona plama jest położona życzenie (żądanie) owcy  
šumma i-na ce-li-im re-bi-im ša šu-me-lim su-mu-um na-di e-ri-iš-ti ni-qi-im – jeśli na żebrze czwartym lewym czerwona plama jest położona żądanie ofiary  
šumma iz-bu-um ši-in-na-šu wa-ca-a šarrum u-mu-šu ga-am-ru i-na kussi-šu ša-nu-um uš-ša-ab – jeśli potworka dwa zęby są wystające dni króla dokonane na tronie jego inny usiądzie

Lekcja dziewiąta

9.1. Perfekt  
Perfekt to jest inny jeszcze czas – prócz Prs i Prt – w którym mają zastosowanie przedrostki. Służy do wyrażania następstwa czasów w czasie przeszłym. W zdaniach warunkowych (po šumma „jeśli”) jest on używany okazyjnie zamiast Prt, szczególnie w przypadku czasowników stanu:  
šumma bab ekallim irtapiš – jeśli „brama pałacu” rozszerzyła się.  
W zdaniu warunkowym dalszy czasownik jest oddzielony od orzeczenia w Prt lub St przez –ma „i” i ma często formę perfektu (jest to tzw. „consecutio temporum”):  
šumma martum ishurma ubanam iltawe – jeśli woreczek żółciowy obraca się i palec otacza.

9.2. Perfekt czasowników trójspółgłoskowych  
Cechą charakterystyczną perfektu jest wrostek –ta- (dla czasowników z e –te-), który dołączany jest po pierwszej spółgłosce rdzeniowej. Samogłoska po drugiej spół-głosce jest taka sama jak w przypadku Prs:  
ištakan „posadził” – od šakanum (a/u)  
iktanuš „poddał się” – od kanašum (u)  
iqterib „zbliżył się” – od qerebum (i)  
Druga osoba liczby pojedynczej ma przedrostek ta- (te-): taštakan, teqterib itd., zaś w przypadku form z końcówką wypada samogłoska po drugiej spółgłosce rdzenio-wej:  
3 os. l.mn. ištaknu, ištakna  
2 os. l.mn. taštaknu, taštakna  
3 os. l.p. z ventivem iqterbam  
W przypadku czasowników zaczynających się na ’ samogłoska przedrostka wy-dłuża się dzięki wypadnięciu zamykającego sylabę alefu (4.6.):  
itakal (&lt;\*i’takal) „zjadł(a)”; tatakal „zjadłeś”  
iterub (&lt;\*i’terub) „wszedł”; teterub „wszedłeś”  
Jeśli pierwszą spółgłoską rdzeniową jest s, c, z, d lub t cecha perfektu t ulega asymilacji:  
iccabat „złapał” (&lt;\*ictabat)  
idduk „zabił” (&lt;\*idtuk).

9.3. Perfekt czasowników dwuspółgłoskowych  
Tworzenie perfektu czasowników ultimae vocalis jest analogiczne jak w przy-padku czasowników trójspółgłoskowych:  
iktari – został skrócony  
ilteqe – wziął.  
Perfekt czasowników z samogłoską w środku ma samogłoskę rdzeniową (czyli taką jak w Prt), zaś jego cechą jest –t-:  
ištim – określił  
iktun – został zestalony.  
W przypadku czasowników na „n” n asymiluje się z t:  
ittadin (&lt;\*intadin) – dał.  
W przypadku czasowników na “w” perfekt tworzony jest od czasownika wtór-nego, który zamiast nagłosu rdzeniowego w ma nagłos t (np. tbl zamiast wbl):  
ittabal – zaniósł  
ittaci – wyszedł (8.8).  
Perfekt czasownika wabalum może być tworzony przez wstawienie t przed dwu-spółgłoskowy rdzeń: itbal „przyniósł”.  
9.4. Czasowniki z podwojeniem na końcu (ultimae geminatae)  
Dwuspółgłoskowe rdzenie z krótką samogłoską między spółgłoskami mogą być sprowadzone do schematu czasowników trójspółgłoskowych przez podwojenie drugiej spółgłoski. Powstałe w ten sposób słowa zwane są czasownikami ultimae geminatae (albo też określane jako „mediae geminatae”). Są to czasowniki stanu jak:  
dananum „być silnym”  
ededum „być spiczastym”  
raqaqum „być cienkim, delikatnym”  
oraz czasowniki działania oznaczające najczęściej czynności składające się z powta-rzalnych, jednakowych zdarzeń:  
hararum (i) „robić bruzdy (marszczyć)”  
ararum (u) „drżeć”  
pašašum (a/u) „smarować”  
malalum (a) „grabić”  
zananum (u) „padać” (o deszczu).

9.5. Odmiana czasowników z podwojeniem na końcu.  
Jest ona analogiczna do odmiany czasowników trójspółgłoskowych. Jedyna róż-nica polega na tym, że 3 osoba liczby pojedynczej statywu czasowników stanu two¬rzona jest przez dwuspółgłoskowy rdzeń z długą samogłoską:  
dan – jest silny  
ed – jest spiczasty.  
Pozostałe formy statywu (z końcówkami) powstają tak samo jak w przypadku czasowników trójspółgłoskowych:  
dannat – jest silna  
danna – są silni, są silne (l. podw.).  
Dla porównania statyw czasowników działania ilustrują następujące przykłady:  
harer, harrat – jest marszczony, jest marszczona  
pašiš, paššat – jest smarowany, jest smarowana.

9.6. Czasowniki z przerwą głosową w środku (mediae alef)  
Trójspółgłoskowe rdzenie z przerwą głosową jako spółgłoską środkową, np. dâcum (d’c) „gnębić”, bělum (b’l) „panować”, rěqum (r’q) „oddalać się”, tracą w więk-szości form przerwę głosową i tworzą Prs i Prt tak samo jak czasowniki mediae vocale bądź to z samogłoską a, bądź e:  
a-klasa  
Prs: idâc „gnębi” (l.mn. idaccu)  
Prt: idac „gnębił” (l.mn. idacu)  
e-klasa  
Prs: irěq „oddala się” (l.mn. ireqqu)  
Prt: ibel „panował” (l.mn. ibelu).  
Uwaga: przerwa głosowa a-klasy czasowników mediae alef cofa się do ‘1 lub ‘2, zaś w przypadku e-klasy – do ‘3-5.

9.7. Czasownik elûm „iść do góry”  
Jest to czasownik jednocześnie primae alef (e-klasy) oraz ultimae vocalis (‘li). Tworzy on Prs illi (ventivus illiam „idzie w górę”), Prt ili i Pf iteli. Przymiotnik odcza-sownikowy elûm „wzniesiony do góry, znajdujący się u góry” używany jest w znacze¬niu „górny” jako przeciwieństwo šaplûm „dolny”.

9.8. Czasownik išûm „mieć”  
Jest to ułomny czasownik na j i ultimae vocalis, który tworzy tylko jeden czas, formalnie Prt, ale używany jako statyw (tzw. statyw z przedrostkami, 11.9):  
išu – ma  
tišu – masz  
išu – mam.  
Jako (nieprzechodnie) Prt i Prt służą w zastępstwie nieistniejących form od išûm konstrukcje powstałe na bazie czasownika rašûm „mieć, otrzymywać”:  
irašši – otrzyma  
irši – otrzymał.

9.9. Miejscownik przysłówków  
Przysłówki takie jak šaplanum „poniżej, na dole” i elenum „powyżej, u góry” (po odrzuceniu mimacji šaplanu, elenu) mają końcówkę –u(m), która nie jest identyczna z końcówką mianownika, lecz służy do oznaczania przypadku istniejącego w dialekcie starobabilońskim, a poza nim nie spotykanego (20.2), a mianowicie miejscownika. Koń-cówka –u(m) zachowała się również w dialektach postarobabilońskich, gdyż przy¬słówki łączą się jak przyimki z rzeczownikami (np. elenum abullim „powyżej bramy”) lub z zaimkami dzierżawczymi (elenušša &lt;\*elenum+ša „powyżej niej”) bądź też dla¬tego, że stawia się przed nimi przyimek ana (ana šaplanum „na dół”).  
Ćwiczenia

šumma bab ekallim pa-ši-iš – jeśli „brama pałacu” jest nasmarowana  
šumma ubanum i-na qa-ab-li-i-ša ha-ar-ra-at – jeśli „palec” w swym wnętrzu jest wy-drążony  
šum-ma mar-tum ap-pa-ša ki-ma ci-il-le-em e-ed – jesli „nos“ woreczka żółciowego jak igła jest spiczasty  
šum-ma mar-tum ki-ma zi-ib-ba-at hu-mu-uc-ci-ri-im da-an-na-at – jeśli woreczek żół-ciowy jak ogon szczura jest mocny  
šumma martum ubanam il-wi-ma re-sa i-na bab ekallim iš-ta-ka-an – jeśli woreczek żółciowy „palec” otacza i wierzchołek jego w „bramie pałacu” układa się  
šumma bab ekallim ir-ta-pi-iš – jeśli „brama pałacu” rozszerzyła się  
šumma ubanum ir-ta-qi-iq – jeśli „palec” zwężył się  
šum-ma na-ap-la-as-tum a-na pa-da-nim iq-te-er-ba-am – jeśli płat wątroby do „ścież-ki” zbliżył się  
šum-ma mar-tum it-be-e-ma mu-uh-hi u-ba-ni-im ic-ca-ba-at – jeśli woreczek żółciowy podnosi się i wierzchnią część „palca” ogarnia  
šumma bab ekallim a-na ni-ri-im i-li – jeśli „brama pałacu” do „jarzma” wznosi się  
šumma bab ekallim ma-aš-ka-an-šu i-zi-im-ma a-na e-le-nu-um i-te-li-a-am – jeśli „brama pałacu” miejsce swe opuściła i do góry się wzniosła  
šum-ma a-mu-tum iq-ru-ur-ma pi-it-ra-am ba-ba-am la i-šu ba-ab ekallim martum u u-ba-nu-um la i-ba-aš-ši – jeśli wątroba zawija się i szpary bramy nie ma „brama pałacu” woreczek żółciowy i „palec” nie istnieją  
šum-ma mar-tum i-si-is-sa a-na e-le-nu-um appa-ša a-na ša-ap-la-nu-um – jeśli wo-reczka żółciowego podstawa do góry jego „nos” do dołu  
i-na kakkim um-ma-nu-um i-ru-ur-ma kakki-ša i-ta-ba-ak – w walce wojsko zadrży i broń swą rzuci  
ša ce-ri-im ša li-ib-bi a-li-im i-da-a-ac – mieszkańcy pustyni mieszkańców miasta po-gnębią  
a-li-ik ha-ar-ra-nim ha-ar-ra-nu-um a-na ha-ar-ra-nim i-na-di-šu u-mu-šu i-re-eq-qu – podróżny podróż dla podróży będzie rzucał dzień jego (powrotu) jest daleki  
a-mu-ut dšul-gi ša a-pa-da-ra-ah i-ne-ru – wróżba Szulgiego który Appa-Darah zabije  
šum-ma mar-tum it-be-e-ma it-ta-ci ru-bu-u-um i-na da-an-na-tim uc-ci – jeśli wore-czek żółciowy podnosi się i wychodzi książę w kłopocie odejdzie  
šumma iż-bu-um ki-ma nešim a-mu-ut mna-ra-am-dsin ša ki-ša-tam i-be-lu-u – jeśli po-tworek jak lew wróżba Naramsina który świat opanuje  
šum-ma mar-tum ik-ta-ri na-ak-ru-um kussiam i-be-el – jeśli woreczek żółciowy skrócił się wróg tron opanuje  
šum-ma mar-tum is-hu-ur-ma u-ba-na-am il-ta-we-e šar-ru-um ma-ta-am na-ka-ar-ta-am i-ca-ab-ba-at – jeśli woreczek żółciowy obraca się i „palec” otacza król kraj wrogi zagarnie  
šumma hašum imittam u šumelam ca-ar-pa-at i-ša-tum i-li-a-am – jeśli płuca na prawo i na lewo zabarwione są na czerwono ogień pójdzie w górę  
šumma e-le-nu-um bab ekallim ši-lum na-di ši-hi-it ne-ši-im – jeśli u góry „bramy pa-łacu“ otworek jest położony wściekłość lwa  
šumma i-na bab ekallim zi-hu ca-bi-it-ma u da-a-an aš-qu-la-al ša-me-e-em – jeśli w „bramie pałacu” pęcherzyk jest utworzony i jest mocny sztormowa chmura deszczu  
Lekcja dziesiąta

10.1. Tematy czasowników  
Rdzeń czasownikowy można poszerzyć za pomocą przedrostków, wrostków lub przez podwojenie środkowej spółgłoski; dochodzi wtedy do modyfikacji jego znacze¬nia. Od w taki sposób rozszerzonych rdzeni wyprowadza się tematy, z których z kolei tworzy się cztery czasy, tryb rozkazujący (13.1) i formy imienne. Wszystkie formy, o których dotąd była mowa, należały do tematu podstawowego (Grundstamm), w skrócie G, ma-jącego za podstawę nierozszerzony rdzeń. Obok niego są jeszcze inne główne tematy: podwojony (Doppelungstamm), w skrócie D, ze zdwojeniem środkowej spół¬głoski rdzeniowej (11.1), kauzatywny Š z przedrostkiem š(a) przed rdzeniem i bierny N (10.2) z przedrostkiem n(a).

10.2. Temat N  
Cechą szczególną tego tematu jest przedrostek n(a). N w przypadku czasowni-ków działania ma znaczenie strony biernej od tematu podstawowego:  
imahhac – bije (Prs G)  
immahhac (&lt;\*inmahhac) – jest bity (Prs N).  
Temat N używany jest też często w znaczeniu strony zwrotnej:  
immaru (Prs G) – widzą, innammaru (Prs N) – widzą się (wzajemnie), spotykają się  
emid (St G) – jest oparty, nenmudu (l.mn. r.m. St N) – opierają się o siebie, do-tykają się (wzajemnie)  
Temat N czasowników stanu ma znaczenie ingresywne (mówiące o początku czynno-ści):  
ibašši (Prs G) – będzie istniał, ibbašši (Prs N) – będzie powstawał, zdążał ku ist-nieniu.  
Są czasowniki występujące tylko w temacie N jak np. nabutum (‘bt) „uciekać” i naplu-sum „patrzeć”.

10.3. Tworzenie Prs i Prt tematu N  
Poprzedzajace rdzeń n jest asymilowane z pierwszą spółgłoską. Przedrostkami 3 i 2 osoby są, tak jak w G, i- (1.4) i ta-, dla czasowników z e-samogłoskowością te- (5.4-5.5). Prs tworzony jest ze zdwojeniem środkowej spółgłoski:  
iššakkan (&lt;\*inšakkan) – jest posadzony,  
taššakkan – jesteś posadzony,  
iššakkanu, iššakkana – są posadzeni, są posadzone,  
taššakkana – jesteście posadzeni.  
Prt: iššakin – był posadzony, była posadzona.  
W przypadku czasowników primae alef przerwa głosowa ‘ asymiluje się z n:  
dotyczy to zarówno a-klasy:  
innammar (&lt;\*in’ammar) – jest widziany  
innamer (&lt;\*in’amer) – był widziany,  
jak i e-klasy:  
inneddil – jest zamknięty  
innedil – był zamknięty.  
Ventivus i subiunctivus tworzy się tak jak w G za pomocą przyrostków –am, -nim (5.7) i –u (7.4):  
innasham (&lt;\*innasih+am) – był oderwany  
ša iššaknušum (&lt;\*iššakin+u+šum) – któremu było przyłożone.  
Uwaga: niektóre zdwojenia spółgłosek jak np. dd albo zz mogą w pewnych formach ulegać dysymilacji i przybierać postać typu nd albo nz. W związku z tym np. Prs N ede-lum może mieć postać zarówno inned¬dil, jak i innendil.

10.4. Rdzeniowe klasy samogłoskowe w temacie N  
Czasowniki wykazujące apofonię (1.4) jak maharum (a/u) „przyjmować” (G imahhar, imhur) tworzą Prs N z samogłoską a, zaś Prt N z i:  
immahhar – jest przyjmowany  
immaher (4.4) – był przyjmowany.  
Także czasowniki a-klasy jak mahacum (a) mają w Prs N a, zaś w Prt N i:  
immahhac – jest bity  
immahic – był bity.  
Natomiast czasowniki i-klasy jak parakum (i) „ryglować” (G iparrik, iprik) mają za-równo w Prs N, jak i w Prt N samogłoskę i:  
ipparrik – jest ryglowany, ipparik – był ryglowany.

10.5. Tworzenie perfektu w temacie N  
Następuje ono inaczej niż w przypadku tematu podstawowego, gdzie wrostek –ta- dołączany jest po pierwszej spółgłosce rdzeniowej (9.2). W temacie N –ta- wchodzi pomiędzy cechę tematu n a rdzeń:  
ittaškan (&lt;\*intaškan) – został posadzony, ustanowiony.  
W przypadku czasowników zaczynających się na ‘ cecha n wtórnie przeniesiona jest ku rdzeniowi:  
ittanmar – był widziany  
ittenmid – był położony obok drugiego, zamknął się.  
Perfekt N zawsze ma taką samą samogłoskę jak Prs N:  
ittamhar – był przyjęty  
ittaprik – był zaryglowany.

10.6. Formy imienne i statyw w temacie N  
W przypadku bezokolicznika N, który jest identyczny pod względem formy z przymiotnikiem odczasownikowym, a także w przypadku statywu, przed rdzeń wsta-wiany jest przedrostek na- (u czasowników z e-samogłoskowością ne-), natomiast mię-dzy drugą a trzecią spółgłoską znajduje się samogłoska u:  
naškunum – (jako bezokolicznik N:) być posadzonym, (jako przymiotnik odcza-sownikowy N:) posadzony  
naškun, naškunat (St) – jest posadzony, jest posadzona.  
Od czasowników zaczynających się na ‘ formy te powstają albo normalnie przez wypadnięcie alefu i uwarunkowane tym wzdłużenie samogłoski, albo z pojawieniem się wtórnego unosowienia:  
nabutum (&lt;\*na’butum) – uciekać  
ale: nanmurum – być widzianym, spotykać się (wzajemnie).  
Statyw N bardzo rzadko tworzony jest od czasowników działania, bo przecież już statyw G (1.5) ma znaczenie pasywne; tworzony jest od nabutum i od czasowników mających w temacie N znaczenie zwrotne:  
nabut – jest zbiegły  
nenmudu – są położeni obok siebie.  
Przymiotnik odczasownikowy N nanmurum istnieje w tworze imiennym nan-murtum „bycie widocznym, spotykanie się”.  
Imiesłów N powstaje dzięki przyłączeniu przedrostka mu-:  
muššaknum (&lt;\*munšaknum) – istniejący jako robiony, ustanawiany  
munnabtum – zbieg (uciekający).

10.7. Temat N czasowników dwuspółgłoskowych  
Czasowniki ultimae vocalis tworzą Prs i Pf tak samo jak czasowniki trójspółgło-skowe z samogłoską rdzeniową w wygłosie, np.  
nabšum (bši) – zdążać ku istnieniu, powstawać:  
Prs: ibbašši – powstaje  
Pf: ittabši – powstał,  
neltûm (lt’) – być rozdzielonym:  
Prs: illette,  
naddûm (&lt;\*nandûm) – być rzucanym:  
Prs: innaddi, l.mn. innaddû, innaddia (6.4).  
Jednak Prt zawsze tworzony jest z samogłoską i:  
illeti – był rozdzielony  
innedi – był rzucany.  
W przypadku czasowników mediae vocalis (7.2) Prs ma postać typu  
iddâk (&lt;\*induak) – jest zabijany.  
Czasowniki zaczynające się na n (8.3) i w (8.6) tworzą formy tematu N tak jak czasow-niki trójspółgłoskowe:  
Prs: innaqqar „jest niszczony”, Prt: innaqer  
Prs: iwwallad „jest rodzony”, Prt: iwwalid.

10.8. Liczba mnoga z końcówką –anu  
Część rzeczowników rodzaju męskiego tworzy liczbę mnogą zarówno z końców-kami –u i –i (3.1; 3.4), jak i z końcówkami –anu i –ani. Te dwie ostatnie formy zwane są „indywidualną liczbą mnogą” służącą do oznaczania takiej zbiorowości, która składa się z elementów będących poszczególnymi detalami, których indywidualny cha¬rakter nale-ży podkreślić. Np.  
šarranu – (poszczególni) królowie, ale: šarru – (wszelcy, jacykolwiek) królowie  
alanu \[mianownik\] – (poszczególne, dające się wymienić) miasta, alani \[dopeł-niacz/biernik\] – (poszczególnych/poszczególne) miast/miasta.

Uwaga: rzeczownik ekallum „pałac” tworzy obok liczby mnogiej rodzaju żeńskiego z końcówką –atum (3.2) także liczbę mnogą rodzaju męskiego z końcówkami –anu, -ani: ekallanu „poszczególne pałace”, ekallani „poszczególnych pałaców”, „poszczególne pałace”.

Ćwiczenia

šumma mar-tum in-na-as-ha-am-ma i-na ba-ab ekallim ik-tu-un – jeśli woreczek żół-ciowy jest oderwany i w „bramie pałacu” się umocnił  
šum-ma šitta na-ap-la-sa-tum ce-el-lu-ši-na ne-en-mu-du – jeśli dwóch płatów wątroby boki opierają się o siebie  
ne-ku-ur-tu-um iš-ša-ak-ka-an – wrogość wybuchnie  
i-ša-tum in-na-ap-pa-ah – ogień wybuchnie  
bu-tu-uq-tum ib-ba-at-ta-aq – grobla będzie przerwana  
bi-it a-wi-lim i-sa-pa-ah – dom człowieka będzie zburzony  
wa-ci a-bu-ul-li-ka it-ti nakrim u-la in-na-mar – wychodzący z bramy twojej z wrogiem nie spotka się  
ka-ab-tum in-na-bi-it – (ktoś) ważny ucieknie  
šar-ru-um i-na li-bi e-kal-li-šu i-du-ak – król we wnętrzu pałacu swego będzie zabity  
al šarrim i-la-wi ic-ca-ba-at-ma in-na-qa-ar – miasto króla będzie otoczone zdobyte i zniszczone  
a-mu-ut amar-dsu’en-na ša še-nu-um a-na še-pi-im iš-ša-ak-nu-šum i-na ni-ši-ik še-ni-im i-mu-tu – wróżba Amarsuenny któremu but na nogę był włożony przez ukąszenie buta umrze  
ku-ru-um i-he-pe-e – miara zboża będzie podzielona na połowę  
a-lu-um a-na še-na il-le-te – miasto na dwa (stronnictwa) będzie podzielone  
i-na ku-uc-ci ku-uc-cum ib-ba-aš-ši i-na um-me-a-tim um-šum ib-ba-aš-ši – zimą chłód nastanie latem upał nastanie  
hu-ša-hu-um ša ša-al-ma-tum in-na-an-du-u ib-ba-aš-ši-i – głód (przez) który trupy będą układane w stosy nastanie  
zi-nu i-na ša-me-e iš-ša-aq-qa-lu – chmury na niebie będą zawieszone  
šar-ra-nu i-na pu-uh-ri-im in-na-am-ma-ru – królowie na zgromadzeniu spotkają się ze sobą  
a-la-nu-ka in-na-aq-qa-ru ti-la-nu in-na-ad-du-u – miasta twoje będą zniszczone wzgó-rza ruin zostaną usypane  
e-kal-la-ni qa-tum i-ka-ša-ad – pałace ręka zdobędzie  
na-ak-ru-um a-na li-ib-bi a-li-i-ka i-te-be-a-am-ma a-la-ni-i-ka i-ki-im-ma i-ta-ba-al – wróg w obrębie miast swoich powstanie i miastami twoimi owładnie i zagarnie (je)  
šar-ru-um šar-ra-am i-na kakki i-da-ak-ma a-la-ni-šu na-wi-šu du-ra-ni-šu er-ce-es-su u te-eh-hi-šu ka-as-su-u i-ka-aš-ša-ad – król króla w potyczce zabije i miasta jego obozy jego umocnienia jego ziemie jego i pogranicze jego ręka jego zdobędzie  
šumma iz-bu-um pa-ni barbarim ša-ki-in mu-ta-nu da-an-na-tum ib-ba-aš-šu u a-hu-um a-na bi-it a-hi-im u-ul i-ru-ub – jeśli potworek twarz wilka ma zgony straszne nastaną i brat do domu brata nie wejdzie  
šum-ma mar-tum i-na qa-ab-li-ša ta-ar-ka-at da-mu iš-ša-ak-ka-nu – jeśli woreczek żółciowy w swej części środkowej jest ciemny morderstwa będą popełniane  
šum-ma mar-tum appa-ša da-ma-am la-pi-it šukkalmahhum im-ma-ha-as – jeśli „nos“ woreczka żółciowego krwią jest pokryty wielki wezyr będzie bity  
šumma bab ekallim li-pi-iš-tam ma-li a-bu-ul-lum in-ne-en-di-il a-lum ma-ru-uš-tam i-mar – jeśli „brama pałacu” tłuszczu jest pełna brama będzie zamknięta miasto nie-szczę¬ścia dozna  
šumma ubanum a-na i-mi-tim ka-mi-a-at a-wi-lum i-na a-li-i-ka a-na ci-bi-it-tim in-na-ad-di – jeśli „palec” na prawo jest przymocowany człowiek w mieście twoim do wię-zienia będzie wtrącony  
šumma hašum imittam u šumelam še-pa-an ša-ak-na na-an-mu-ur-tum – jeśli od płuc na prawo i na lewo „nogi” są położone spotkanie  
šumma warkat hašim it-te-en-mi-id sa-li-mu-um iš-ša-ka-an – jeśli odwrotna część płuc połączyła się pokój będzie zawarty  
Lekcja jedenasta

11.1. Temat D  
Cechą charakterystyczną tego tematu (zob. 10.1) jest podwojenie drugiej spół-gło¬ski rdzeniowej, zaś jego funkcja znaczeniowa polega na tym, że wyraża on wzmoc-nienie zdarzeń czy stanów oznaczanych przez temat podstawowy:  
halaqu(m) – zniknąć, zmarnieć; hulluqu(m) – spowodować zaginięcie, zniszczyć  
kanašu(m) – poddawać się; kunnušu(m) – ujarzmiać, uginać  
tiabu(m) – być dobrym; tubbu(m) – robić dobrze.  
Ponadto często temat D był używany w znaczeniu zbieżnym ze znaczeniem te-matu G, jeśli wyrażał zdarzenia czy stany występujące w liczbie mnogiej:  
martum muhhaša takip – górna część woreczka żółciowego jest brudna (l.p. sta-tivu G), ale: martum budaša takkupa – obie strony woreczka żółciowego są brudne (l.pdw. stativu D);  
pater – jest (raz) rozszczepiony; puttur – jest wielokrotnie rozszczepiony.

11.2. Czasy z przedrostkami w temacie D  
Przedrostkami tematu D – w odróżnieniu od tematów G i N – są w przypadku 3 osoby liczb pojedynczej i mnogiej u- oraz tu- w przypadku 2 osoby. Rdzeniowe klasy samogłoskowe tematów G (1.4) i N (10.4) nie mają zastosowania w temacie D. Zawsze samogłoską po zdwojonej spółgłosce środkowej jest w Prs a, zaś w Prt i Pf i; natomiast po pierwszej samogłosce stoi a:  
Prs:  
ukannaš – ugina  
tukannaš – uginasz  
ukannašu, ukannaša – uginają,  
tukannaša – uginacie

Prt:  
ukanniš – uginał.  
W Pf wrostek –t- włączany jest po pierwszej spółgłosce:  
uktanniš.  
W przypadku czasowników primae alef złożenia u’a i u’e redukują się do u:  
Prs: ussar (&lt;\*u’assar) – zamyka  
Prt: usser (&lt;\*u’esser) – zamykał  
Pf: utesser (&lt;\*u’tesser) – zamknął (wzdłużenie przedrostka wynika z wypadnię-cia przerwy głosowej).  
3 osoba Prs i Prt czasowników primae alef zapisywane są tak jak Prs G (4.6), a więc zawsze z samogłoską w nagłosie słowa: u-us-sa-ar, u-us-se-er lub u-sa-ar (ussar), u-se-er (usser).

11.3. Formy imienne i stativ tematu D  
W bezokoliczniku D, który jest identyczny pod względem formy z przymiotni-kiem odczasownikowym D, a także w stativie D, samogłoską po pierwszej i podwojonej drugiej spółgłosce rdzeniowej jest u:  
bezokolicznik/przymiotnik: kunnušum – uginać; ugięty  
stativ: kunnuš, kunnušat – jest ugięty, jest ugięta.  
W przypadku czasowników zaczynających się na „’”:  
ullucum – doprowadzać do radości (bezokolicznik D od elecum – radować się),  
ulluc libbim – radość serca.  
Imiesłów jest tworzony za pomocą przedrostka mu-:  
mukannišum – uginający się.  
11.4. Czasowniki z samogłoską na końcu w temacie D  
U czasowników ultimae vocalis, tak jak u czasowników trójspółgłoskowych, w temacie D ginie samogłoska rdzeniowa i zostaje zastąpiona w Prs przez a, zaś w Prt i Pf przez i:  
Prs: urabba – robi dużym  
Prt: urabbi – robił dużym  
Pf: urtabbi.  
Czasowniki kończące się na „e” („’”) tworzą Prs z e: utebbe – zanurza  
Prt: utebbi  
Pf: uttebbi (9.2).  
Końcówki zaczynające się na samogłoskę będą ściągane:  
urabbû (&lt;\*urabbau), urabbâ (&lt;\*urabbaa) – robią dużym, dużą.  
Bezokolicznikiem jest rubbûm „robić dużym”; stativ: rubbu, rubbât „jest zro-biony dużym”, „jest zrobiona dużą”.

11.5. Czasowniki z samogłoską w środku w temacie D  
U tych czasowników podwojenie drugiej spółgłoski jest możliwe tylko wtedy, gdy mają końcówkę. Czasowniki z u i i w środku są w temacie D traktowane tak samo. Samogłoską Prs jest a, zaś Prt i Pf i:  
Prs: ukan – robi mocnym; ukannu, ukanna – robią mocnym  
Prt: ukin, ukinnu  
Pf: uktin, uktinnu.  
St: kun, kunnat; bezokolicznik: kunnum, imiesłów: mukinnum.

11.6. Czasowniki z przerwą głosową w środku w temacie D  
Czasowniki te, np. něšum (n’š) „żyć, zdrowieć” (9.6) tworzą Prs D analogicznie do czasowników mediae vocalis:  
unaš – będzie doprowadzał do życia.

11.7. Czasowniki na „n” i na „w” oraz ultimae geminatae w temacie D  
Ta grupa czasowników mocnych w temacie D upodabnia się całkiem do cza-sowni¬ków trójspółgłoskowych:  
wašarum „być luźnym”; wuššurum „rozluźniać się”; Prs: uwaššar  
alalum (i halalu) „powiesić”; St D: (h)ullul „jest obwieszony”.

11.8. Czasowniki nieregularne izuzzum i itulum  
Rdzeniem izuzzum „stać” jest \*ziz; jest on wydłużony przez postawione z przodu n.  
Prs (potraktowany jako stativ – zob. 11.9): izzaz (&lt;\*inzaz) – stoi; tazzaz – stoisz  
Prt: izziz – stawał  
Pf: ittaziz – stanął.  
Przed końcówką samogłoskową z jest podwojone:  
izzazzu, izzazza – stoją  
tazzizza – stawaliście  
mazzaz tazzazzu – miejsce, na którym stoisz (7.5).  
Rdzeniem itulum „leżeć” jest \*til (z długą samogłoską w odróżnieniu od ziz); on też jest wydłużony przez n.  
Prt: ittil – kładł się  
Pf: ittatil – położył się.

11.9. Stativ z przedrostkami  
W przypadku niektórych czasowników Prs lub Prt pełnią funkcję stativu, w związku z czym specjalna forma dla stativu nie jest tworzona, np.:  
išum – mieć (9.8); išu – ma (formalnie Prt)  
izzuzzum – stać (11.8); izzaz – stoi (formalnie Prs) i  
kullum (rdzeń \*kul); ukal – trzyma (Prs D; ten czasownik występuje tylko w D).  
Od \*kul prócz stativu tworzony jest tylko bezokolicznik kullum „trzymać” oraz imiesłów muki¬lum „trzymający” obecny w wyrażeniu mukil reš lemuttim „pomagająca złu” (dosł. „trzymająca głowę zła”) będącym imieniem kobiety.

11.10. Zaimek używany przydawkowo  
Jako zaimki 3 osoby służą šu „on” oraz ši „ona” – w G/A šuati „jego” i šiati „jej”, „ją”. Šu jest także zaimkiem wskazującym i może być użyty zarówno podmio¬towo, jak i przydawkowo; w tym ostatnim przypadku słowo šu tłumaczone jest jako „dany”, „wspomniany” lub „każdy”:  
N: matum ši – wspomniany kraj  
G: matim šâti (&lt;\*šiati) – wspomnianego kraju  
A: matam šâti (&lt;\*šiati) – wspomniany kraj.  
Liczba mnoga to šunu „oni” i šina „one” – przydawkowo „wspomniani”, „wspomniane”; G/A: šunuti, šinati.

Ćwiczenia

šumma uban ha-ši qablitum i-mi-tam u šu-me-lam šu-lu-ša pu-tu-ra-at – jeśli środkowy „palec” płuca po prawej i lewej stronie na troje (części) jest rozdzielony  
šumma naplastum zi-ih-hi u-lu-la-at – jeśli płat wątroby pęcherzykami jest obwieszony  
šum-ma i-na i-ši-id mar-tim pi-it-ru-um ša-ki-im-ma a-na zu-um-ri-ša tu-uh-hu – jeśli na podstawie woreczka żółciowego szczelina jest położona i do jego „ciała” się zbliża  
šumma kakki i-mi-tim tu-u-ur – jeśli „broń prawicy” jest odwrócona  
šumma bab ekallim ka-ajja-nu-um ša-ki-in-ma ša-nu-um u-ša-pi-il-ma warkat a-mu-tim ša-ki-in – jeśli „brama pałacu” normalna jest położona (na swoim miejscu) i druga kie-ruje się w dół i za wątrobą jest położona  
šumma bab ekallim ka-ajja-nu-um ša-ki-in-ma ša-nu-um u-ša-qi-ma e-le-nu ša-ki-in – jeśli „brama pałacu“ normalna jest położona (na swoim miejscu) i druga kieruje się w górę i u góry jest położona  
šum-ma mar-tum is-hu-ra-am-ma šu-me-la-am iz-za-az – jeśli woreczek żółciowy ob-raca się i na lewo stoi  
šum-ma i-na a-mu-tim 4 na-ap-la-sa-tum iš-te-niš iz-za-az-za – jeśli na wątrobie 4 płaty razem stoją  
šumma uban ha-ši qablitum ma-aš-ka-an-ša i-zi-ba-am-ma i-na mu-uh-hi uban ha-ši ša šu-me-li-im it-ta-zi-iz – jeśli „palec” płuca środkowy miejsce swoje opuścił i na górnej części palca płucnego lewego stanął  
šum-ma mar-tum in-na-as-ha-am-ma u-ba-ba-am i-de-er-ma u i-na ma-ajja-li-ša it-ta-ti-il – jeśli woreczek żółciowy jest oderwany i „palec” obejmuje i w „łożu” leży  
i-na ma-az-za-az ta-az-za-az-zu i-lum u-ša-lam-ka – w miejscu (na którym) stoisz bóg obdarzy zdrowiem ciebie  
bi-it a-wi-li is-sa-pa-ah ša-nu šum-šu mu-ki-il re-eš le-mu-ut-tim – dom człowieka bę-dzie zniszczony inna interpretacja Wspomagająca-zło \[imię kobiety\]  
naker-ka ma-at-ka u-ha-al-la-aq – wróg twój kraj twój zniszczy  
awilum cu-mu-ra-at u-ca-ma-ru qa-as-su i-ka-ša-ad – człowiek życzenie (o którego spełnienie) zabiega ręką swoją urzeczywistni  
ma-a-ru i-na bu-ul-ti a-bi-šu-nu bi-it a-bi-šu-nu u-sa-ap-pa-hu – synowie za życia oj¬ców swoich „dom” (=mienie) ojców swych będą rozrzucać  
ma-at na-ak-ri-im tu-sa-na-aq a-ša-ar iš-te-en tu-pa-ha-ar-ši – kraj wroga zawęzisz w jednym miejscu zbierzesz go  
šar-rum be-el te-re-ti-šu u-ra-ab-ba – król pełnomocnika swego wywyższy  
a-gu-u-um i-te-eb-be-a-am e-le-pa-tim u-te-be – przypływ spowodowany sztormem nastanie statki zatopi  
ra-ki-ib i-me-ri nakram u-ta-ar – oddział jeźdźców na osłach wroga odeprze  
a-lam ta-ca-ab-ba-at ša-al-la-su tu-wa-aš-ša-ar – miasto zagarniesz łup jego odprowa-dzisz  
u-lu-uc li-bi aš-ša-at a-wi-lim zi-ka-ra-am u-la-ad – radość serca żony człowieka chłop-ca urodzi  
šep awilim idi ilim u-ul wu-šu-ra-at – noga człowieka od strony boga (opiekuńczego) nie będzie oderwana (?)  
ma-tum ši-i in-na-an-di – kraj ten jest rzucony  
bi-ša-am ša ma-tim ša-a-ti ma-tum ša-ni-tum i-ta-ab-ba-al – majątek kraju wspomnia-nego kraj inny zabierze  
šumma bab ekallim su-un-nu-uq i-na kakkim nakrum um-ma-nam u-sa-ar u-ma-am re-qa-a-am šattammu ekallam u-sa-na-qu – jeśli „brama pałacu” jest zwężona w walce wróg wojsko otoczy w przyszłości skarbnicy pałac będą kontrolowali  
šumma iz-bu-um up-pu-uq a-mu-ut i-bi-dsin ša-ah-lu-uk-tum – jeśli potworek jest obro-śnięty (?) wróżba (dla) Ibbisina zniszczenie  
šum-ma mar-tum li-pi-a-am ku-us-sa-a-at ku-cu-um i-ba-aš-ši – jeśli woreczek żółcio¬wy tłuszczem jest zarośnięty chłód nastanie  
šum-ma mar-tum mu-uh-ha-ša ta-ki-im-ma ca-ri-ip mi-qi-it-ti barim – jeśli woreczka żółciowego strona wierzchnia jest brudna i zabarwiona na czerwono klęska wróżbity  
šum-ma mar-tum bu-da-ša tu-ku-pa-a-ma cu-ur-ru-pa mi-qi-it-ti ra-ša-ši-im – jeśli wo-reczka żółciowego bok jest brudny i zabarwiony na czerwono klęska „raszaszu” \[nazwa jakiejś funkcji\]  
šumma kakki i-mi-it-tim iz-zi-iz ka-ab-tu-um ša li-ib-bi be-li-šu u-ta-ab-bu-u ib-ba-aš-ši – jeśli „broń prawicy” stoi (ktoś) ważny który serce pana swojego uczyni dobrym po¬jawi się  
šumma iz-bu-um pa-ni nešim ša-ki-in šarrum da-an-nu-um ib-ba-aš-ši-ma ma-tam ša-ti u-na-aš – jeśli potworek twarz lwa posiada król potężny pojawi się i kraj ten ożywi  
šum-ma mar-tum ši-ši-ta-am u-ka-al a-na be-el im-me-ri-im mu-ur-ca-am u-ka-al – jeśli woreczek żółciowy błonkę przytrzymuje dla właściciela ofiary chorobę wstrzyma  
Lekcja dwunasta

12.1. Temat Š  
Cechą charakterystyczną tematu Š (10.1) jest przedrostek rdzeniowy š(a)-. Na-daje on formom tworzonym za jego pomocą charakter kauzatywny, wyraża spowodo-wanie działań oznaczanych przez formy tematu podstawowego.  
maqatu(m) – padać; šumqutum (bezokolicznik Š) – upuścić, doprowadzić do upadku  
wacûm – wychodzić; šucum – wyprowadzać.  
W przypadku czasowników stanu formy tematu Š, tak jak i tematu D, mają zna-czenie faktytywne \[odnoszące się do powodowania czegoś\].  
ešerum – być w porządku; šušurum – porządkować.

12.2. Czasy z przedrostkami w temacie Š  
Przedrostki, o które tu chodzi, są takie same jak w temacie D, tzn. u- dla 3 osoby i tu- dla 2 osoby. Między przedrostek a rdzeń wchodzi –ša-, a w przypadku czasowni¬ków primae alef e-klasy –še-. Samogłoską po środkowej spółgłosce jest a w Prs oraz i w Prt oraz Pf. W perfekcie wrostek –t- staje po š przedrostka tematowego -ša-:  
Prs:  
ušamqat – upuszcza  
tušamqat – upuszczasz  
ušamqatu, ušamqata – upuszczają  
tušamqata – upuszczacie  
Prt:  
ušamqit – upuszczał  
Pf:  
uštamqit – upuścił.  
W przypadku czasowników zaczynających się na ‘ przerwa głosowa w Prs ulega asymilacji upodabniając się do środkowej spółgłoski:  
ušallak (&lt;\*uša’lak) – wypuszcza  
ušerreb (&lt;\*uše’reb) – wpuścił.  
W Prt i Pf wypadnięcie zamykającego sylabę alefu pociąga za sobą zastępcze wzdłużenie (4.6):  
ušalik (&lt;\*uša’lik) – wypuszczał  
ušterib (&lt;\*ušte’rib) – wpuścił.

12.3. Formy imienne i stativ w temacie Š  
W formach pozbawionych przedrostków cecha tematu Š przybiera postać šu-. W bezokoliczniku, identycznym formalnie z przymiotnikiem odczasownikowym, a także w stativie po środkowej spółgłosce następuje u:  
šumqutum – upuszczać (bezokolicznik)  
upuszczony (przymiotnik odczasownikowy)  
šumqut, šumqutat – jest upuszczony, jest upuszczona (stativ).  
W przypadku czasowników na ‘ (i na j – 4.8) mamy do czynienia z zastępczym wzdłużeniem:  
šušurum – porządkować; uporządkowany  
šušur, šušurat – jest uporządkowany, jest uporządkowana.  
Imiesłów tworzony jest przy pomocy przedrostka mu-:  
mušamqitum – upuszczający.

12.4. Czasowniki ultimae vocalis w temacie Š  
Tworzenie tematu Š jest w przypadku tych czasowników analogiczne jak w przy-padku czasowników trójspółgłoskowych. Tak jak w temacie D (11.4) samogłoska rdze-niowa zastąpiona jest w Prs przez a, zaś w Prt i Pf przez i:  
Prs: ušamla – napełnia  
Prt: ušamli – napełniał  
Pf: uštamli – napełnił  
Czasowniki kończące się na e (na ‘) tworzą Prs z samogłoską e:  
ušepte – dał otworzyć, Prt: ušepti, Pf: uštepti.  
Początek końcówek samogłoskowych ulega ściągnięciu:  
ušamlû (&lt;\*ušamlau) – napełniają.  
Bezokolicznikiem jest šumlûm „napełniać”, a stativ wygląda następująco: šum-lu, šumlat „jest napełniony”, „jest napełniona”.  
Od wacûm „wychodzić” (8.8) jest w temacie Š tworzony Prs: ušecce „wyprowa-dza”, Prt: ušeci, Pf: ušteci (12.7) i przymiotnik odczasownikowy šucûm „wygnaniec”.

12.5. Czasowniki mediae vocalis w temacie Š  
W odróżnieniu od czasowników trójspółgłoskowych czasowniki z samogłoską w środku przyłączają jako tematowy przedrostek rdzeniowy š-, a nie ša-:  
Prs: ušmat – każe umrzeć  
Prt: ušmit – kazał umrzeć  
Pf (z włączeniem –ta-): uštamit – kazał umrzeć.  
W formach z końcówką samogłoskową druga spółgłoska jest podwojona jak w temacie D, a poprzedzająca ją samogłoska ulega skróceniu:  
ušmattu – każą umrzeć  
ušmittu – kazali umierać  
bezokolicznik šumutum – kazać zabijać.

12.6. Czasowniki zaczynające się na n w temacie Š  
Tworzenie w temacie Š form czasowników na n jest analogiczne jak w przy-padku czasowników trójspółgłoskowych, ale n we wszystkich konstrukcjach ulega asy-milacji:  
Prs: ušaqqar (&lt;\*ušanqar) – każe zniszczyć  
Prt: ušaqqer (&lt;\*ušanqer) – kazał niszczyć  
Pf: uštaqqer – kazał zniszczyć  
bezokolicznik: šuqqurum – kazać niszczyć.  
nadûm (ndi) „rzucać” tworzy formy tematu Š normalnie (12.4):  
Prs: ušadda – każe rzucać  
Prt: ušaddi – kazał rzucać  
Pf: uštaddi – kazał rzucić  
ša libbiša uštaddiši – który jej życie kazał jej rzucić (=spowodował przedwcze¬sny poród).

12.7. Czasowniki na w w temacie Š  
Czasowniki na w tworzą temat Š od dwuspółgłoskowego rdzenia, np. \*bil w przypadku wabalum, przy czym pierwsza spółgłoska ulega podwojeniu w Prs:  
Prs: ušabbal – każe nosić  
Prt: ušabil – kazał nosić  
Pf: uštabil  
St: šubul – jest zobowiązany do noszenia.  
Niektóre czasowniki na w są w temacie Š przeniesione do grupy czasowników zaczynających się na j, które z kolei tworzą formy identyczne jak czasowniki primae alef e-klasy. Tak jest np. z wašarum „siadać”: Prs: ušeššeb „każe zająć miejsce”, Prt: ušešib, Pf: uštešib. Identycznie traktowane jest wacûm „wychodzić” (12.4).  
Od šubulum (bezok. Š od wbl) tworzy się w temacie Š formy nieregularne róż-niące się od form od abalum „być suchym”:  
St: šabul – jest wysuszony  
przym.: šabulum – wysuszony.

Ćwiczenia

šumma i-na ma-ac-ra-ah martim zi-hu na-di-ma me-e la u-še-e-ci – jeśli na zaczątku woreczka żółciowego pęcherzyk jest położony i żółć nie wylewa się  
šumma a-na bab ekallim zi-ih-hu šu-ru-ub-ma qa-aq-qa-ar-šu a-ki-il – jeśli do „bramy pałacu” pęcherzyk wniknął i wszystko przegryzł  
šum-ma mar-tum šu-me-el-ša da-ma-am ša-bu-la-am pa-ši-iš – jeśli woreczka żółcio-wego lewa strona krwią wyschniętą jest wysmarowana  
nakrum matam u-ša-am-qa-at – wróg kraj doprowadzi do upadku  
ma-at nakri-ka tu-ša-am-qa-at – kraj wroga twojego doprowadzisz do upadku  
nakrum u-še-re-ba-am-ma i-na lib-bi ma-ti-ka ša-la-tam u-še-ec-ce-e – wróg wtargnie i ze środka kraju twojego łup wyniesie  
ša-al-la-at ta-ša-al-la-lu u-la tu-še-ce – łup zagarniesz (ale) nie wyniesiesz  
um-ma-an na-ak-ri-im i-te-eb-be-am-ma ša-al-la-at be-el im-me-ri u-še-ec-ce – wojsko wroga powstanie i łup (tu: mienie) właściciela owiec wyniesie  
wa-ši-ib ekallim pi-ri-iš-tam u-še-ec-ce – mieszkaniec pałacu tajemnicę zdradzi  
ra-bi-a-na i-na a-li-šu u-še-cu-u-šu – naczelnika miasta z miasta jego wygnają  
šumma bab ekallim šu-u-šu-ur a-na harranim šu-ul-mu-u-um – jeśli „brama pałacu” jest w porządku w drodze szczęście  
šumma šena bab ekallim ša-nu-um i-na i-mi-tim ša-ki-in na-ak-rum ma-tam u-ša-da – jeśli (są) dwie „bramy pałacu” druga na prawo jest położona wróg kraj powali  
šum-ma pa-da-nu-um ša-ki-in i-lum ki-bi-is a-wi-lim u-še-še-er – jeśli „ścieżka” jest położona bóg postępowanie człowieka doprowadzi do porządku  
Z kodeksu Hammurabiego

šum-ma a-wi-lum i-na har-ra-nim wa-ši-ib-ma kaspam huracam abnam u bi-iš qa-ti-šu a-na a-wi-lim id-di-in-ma a-na še-bu-ul-tim u-ša-bil-šu a-wi-lum šu-u mi-im-ma ša šu-bu-lu a-šar šu-bu-lu la id-di-in-ma it-ba-al be-el še-bu-ul-tim a-wi-lam šu-a-ti i-na mi-im-ma ša šu-bu-lu-ma la id-di-nu u-ka-an-šu-ma a-wi-lum šu-u adi hamši-šu mi-im-ma ša in-na-ad-nu-šum a-na be-el še-bu-ul-tim i-na-ad-di-in – jeśli człowiek w drodze się znajduje i srebro złoto drogie kamienie i majątek ręka jego człowiekowi dała i ładunek kazała przewieźć człowiek ów coś co każącego przewieżć zamiast (oddać) każącemu przewieźć nie dał i zabrał przewodniczący starszych człowiekowi temu wszystko co ka-żącemu przewieźć nie oddał udowodni i człowiek ten pięciokrotnie wszystko co było zabrane przez niego właścicielowi ładunku da  
šumma aš-ša-at a-wi-lim aš-šum zi-ka-ri-im ša-ni-im mu-sa uš-di-ik sinništam šu-a-ti i-na ga-ši-ši-im i-ša-ak-ka-nu-ši – jeśli małżonka człowieka z powodu mężczyzny innego męża każe zabić kobietę tą na pal wbiją  
šum-ma a-wi-lum marat a-wi-lim im-ha-ac-ma ša li-ib-bi-ša uš-ta-di-ši 10 šiqil kaspam a-na ša li-ib-bi-ša i-ša-qal šum-ma sinništam ši-i im-tu-ut maras-su i-du-uk-ku – jeśli człowiek córkę człowieka bił i przedwczesny poród spowodował 10 szekli srebra za płód jej zapłaci jeśli kobieta ta umrze córkę jego zabiją

Lekcja trzynasta

13.1. Tryb rozkazujący tematu podstawowego  
Tryb rozkazujący G odpowiada niewydłużonemu rdzeniowi, albo preteritum bez przedrostków (1.4), co ilustruje przykład šiamum „określić”:  
Prt: išim – określił  
rdzeń: \*šim  
2 osoba liczby pojedynczej rodzaju męskiego trybu rozkazującego: šim – określ!  
2 osoba liczby pojedynczej rodzaju żeńskiego ma końcówkę –i:  
šimi – określ!  
W liczbie mnogiej dla 2 osoby obu rodzajów stosowana jest końcówka –a:  
šima – określcie!

13.2. Tworzenie trybu rozkazującego tematu podstawowego czasowników trójspółgłos-kowych  
W tym przypadku między pierwszą a drugą spółgłoskę rdzeniową wchodzi samo-głoska pomocnicza odpowiadająca samogłosce rdzeniowej. U czasowników pri¬mae alef (4.6) przerwa głosowa wypada a samogłoska pomocnicza sprowadzana jest do a w przypadku a-klasy oraz do e w przypadku e-klasy tych czasowników.

Bezok. Prt Rdzeń 2 os. l.p. r.m. trybu rozkazującego G  
cabatum (a) icbat \*cbat cabat – chwyć!  
kanašum (u) iknuš \*knuš kunuš – ugnij się!  
caramum (i) icrim \*crim cirim – staraj się!  
parasum (a/u) iprus \*prus purus – rozstrzygnij!  
apalum (a/u) ipul \*’pul apul (&lt;\*’upul) – uspokój!  
alakum (a/i) illik \*’lik alik (&lt;\*’ilik) – idź!  
erebum (u) irub \*’rub erub (&lt;\*’urub) – wejdź!

13.3. Tworzenie trybu rozkazującego G czasowników dwuspółgłoskowych  
Czasowniki ultimae vocalis (6.2) tworzą tryb rozkazujący tak samo jak trójspół-gło¬skowe z samogłoską pomocniczą odpowiadającą samogłosce rdzeniowej. W przy-padku czasowników kończących się na e (na ‘) będzie to samogłoska e.

Bezokolicznik Prt Rdzeń 2 os.l.p.r.m. TR  
qabûm iqbi \*qbi qibi – mów!  
manûm imnu \*mnu munu – licz!  
leqûm ilqe \*lq’ leqe – bierz!

W przypadku czasowników mediae vocalis (7.1) tryb rozkazujący tworzony jest przez niewydłużony rdzeń, przy czym czasowniki mediae alef mają w trybie rozkazują-cym samogłoskę a lub e.

Bezokolicznik Prt Rdzeń 2 os.l.p.r.m. TR  
šiamum išim \*šim šim – określ!  
zâzum izuz \*zuz zuz – dziel!  
bělum ibel \*b’l bel – bierz!  
šâmum išam \*šam šam – kup!

U czasowników z uzupełnieniem rdzeniowym w (na w – 8.5) tryb rozkazujący tworzony jest przez niewzdłużony rdzeń. Tryb rozkazujący czasowników z rdzeniem wzdłużonym przez n (na n – 8.2) powstaje poprzez poprzedzenie rdzenia samogłoską odpowiadającą samogłosce rdzeniowej.

Bezokolicznik Prt Rdzeń 2 os.l.p.r.m. TR  
wabalum (a/i) ubil \*bil bil – zanieś!  
nadanum (i) iddin \*din idin – daj!  
nasahum (u) issuh \*suh usuh – usuń!

Tryb rozkazujący od izuzzum „stać” (11.8) – iziz „stań!”.

13.4. Tryb rozkazujący G z końcówkami samogłoskowymi  
Po przyłączeniu końcówki –i lub –a (13.1), albo końcówki ventivu –am u czasow-ników trójspółgłoskowych i zaczynających się na n wypada samogłoska rdze¬niowa.  
cabat – chwyć!, cabti – chwyć!, cabta – chwytajcie!  
šupram – przyślij!, šupranim – przyślijcie!  
idnam – podaj!  
W przypadku pozostałych grup czasowników jest następująco:  
qibia – mówcie!  
zuza – dzielcie!  
bilam – zanieś!

13.5. Prekativ  
Tryb rozkazujący jest w przypadku 3 osoby zastępowany przez prekativ (on musi, powinien, ma, niech …). Forma ta powstaje na bazie 3 osoby Prt w ten sposób, że przedrostek i- zamieniany jest przez przedrostek li-:  
icbat – chwyta, licbat – niech łapie, ma łapać, licbatu, licbata – niech łapią, mają łapać  
ublam – zanosi (8.6), liblam – niech zaniesie  
uballitu – zachowują przy życiu, liballitu – niech zachowają przy życiu.

13.6. Prohibitiv  
Zakaz dla 2 osoby wyrażany jest przez la „nie” (5.10) i odpowiednią formę Prs:  
tryb rozkazujący: idin – daj! prohibitiv: la tanaddin – nie daj la tanaddina – nie dajcie!  
la tazâz – nie dziel! la tazuzza – nie dzielcie (7.2)

13.7. Liczba mnoga abum i ahum  
Rzeczowniki te tworzą liczbę mnogą z podwojeniem spółgłoski rdzeniowej:  
abbu – ojcowie, G/A abbi  
ahhu – bracia, G/A ahhi.

13.8. Trójpostaciowa deklinacja imion z przyrostkami  
Większość imion podlega w połączeniu z zaimkami dzierżawczymi (3.7, 13.9) deklinacji dwupostaciowej rozróżniającej tylko dwie formy przypadków:  
N i A: ummanka – twoje wojsko  
G: ummanika – twojego wojska.  
Liczne dwuspółgłoskowe rdzenie imienne, np. bišum „mienie”, cabum „żoł-nierz”, marum „syn”, abum „ojciec”, ahum „brat” i kalum „całość” deklinowane są jed¬nak w taki sposób, że przed przyrostkami zaimkowymi stoją końcówki wszystkich trzech przypadków:  
abuka – twój ojciec, abika – twojego ojca, abaka – twojego ojca  
matum kaluša – cały kraj  
bišašina – ich majątek  
cabaka – twojego żołnierza.  
13.9. Przyrostki pełniące funkcję zaimka dzierżawczego  
Przyrostek 1 osoby liczby pojedynczej –ja „mój” (3.9) po dołączeniu do rzeczow-nika w liczbie mnogiej zakończonego na –u lub –û przybiera postać –‘a:  
redû’a (&lt;rediu + ja) – moi żołnierze.  
Do wcześniej poznanych zaimków dzierżawczych mających formę przyrostka (3.7) dodać należy jeszcze:  
-kunu, -kina – wasz (r.m., r.ż.)  
-ni – nasz  
oraz przedrostek 2 osoby liczby pojedynczej rodzaju żeńskiego  
-ki – twój:  
belkunu, belkina – wasz pan  
belini – naszych panów.

13.10. Zaimki w formie przyrostków dołączających się do czasowników  
Do wcześniej poznanych przyrostków w bierniku (5.8) dochodzą:  
-šunuti, -šinati – ich (l.mn.), je  
-kunuti, kinati – was (r.m., r.ż.)  
-ni – mnie  
-niati – nas.  
Również zasób znanych nam już przyrostków w celowniku (5.9) powiększyć można o:  
-šunušim, -šinašim – im  
-kunušim, kinašim – wam  
-ni – mi  
-niašim – nam.  
Wraz ze wszystkimi przyrostkami w celowniku występuje często ventiv (5.9):  
iknukanniašim (&lt;iknuk + am + niašim) – zapieczętował wam = opieczętowa¬nym dokumentem wam przydzielił.  
Spośród przyrostków w bierniku z ventivem łączy się tylko –ni „mnie”:  
ulammidanni (&lt;ulammid + am + ni) – zawiadamia mnie  
ulammiduninni (ulammidu + nim + ni) – zawiadomili mnie.

13.11. Pierwsza osoba w tematach G (podstawowym) i N  
Przedrostkiem 1 osoby liczby pojedynczej jest a-, a 1 osoby liczby mno¬giej – ni-, przy czym w liczbie mnogiej nie ma końcówki:  
icbat – chwycił, tacbat – chwyciłeś, acbat – chwyciłem, chwyciłam, icbatu – chwycili, icbata – chwyciły, tacbata – chwyciliście, chwyciłyście, nicbat – chwyciliśmy, chwyciłyśmy  
aqabbi – mówię, niqabbi – mówimy, Prt: aqbi, Pf: aqtabi  
ammahhac – jestem pobity, nimmahhac – jesteśmy pobici, pobite.  
W przypadku e-klasy czasowników primae alef 1 osoba liczby pojedynczej ma postać:  
Prt: erub – wchodziłem  
Prs: errub – wchodzę.  
erub w zapisie nie różni się od trybu rozkazującego tematu podstawowego erub (13.2); obydwie formy zapisuje się jako e-ru-ub

Pierwsza osoba liczby mnogiej:  
Prt: nirub – wchodziliśmy  
Prs: nirrub  
Pf: niterub.  
Także czasowniki kończące się na e (na ‘) tworzą 1 osobę liczby pojedynczej z e:  
eleqqe – biorę, elqe – brałem, elteqe – wziąłem.  
13.12. Pierwsza osoba w tematach D i Š oraz u czasowników zaczynających się na w  
W tematach D i Š, a w przypadku czasowników na w również w Prs i Prt tematu podstawowego, przedrostkami 1 osoby są w liczbie pojedynczej u- (tak jak dla 3 osoby), a w liczbie mnogiej nu-:  
ukanniš – ugiąłem się lub ugiął się  
nukanniš – ugięliśmy się  
ušamqat – każę padać lub każe padać  
nušamqat – każemy padać  
ubbal – przynoszę lub przynosi  
nubbal – przynosimy  
Prt: ubil, nubil, ale Pf (9.3): attabal, nittabal.

13.13. Bezokolicznik jako dopełnienie  
Biernik bezokolicznika może służyć jako dopełnienie czasowników przechod¬nich:  
mimma nudunnâm x turram niqbi – wszystko posagu x zwrócenie rozkazujemy = rozkazujemy, by oddać x cały posag

13.14. Spójnik okolicznikowy czasu trybu łączącego kima  
Przysłówek kima „jak” może być użyty również do tworzenia zdania pobocz¬nego okolicznikowego czasu; jest wtedy tłumaczony jako „kiedy” lub „skoro tylko”. Czasownik w zdaniu z kima ma formę trybu łączącego (subiunctivusa):  
kima unnedukki tammaru – kiedy mój list czytasz.

13.15. Uogólniony zaimek względny mala  
Po mala „o ile”, „jak” „cokolwiek” stoi – jak po ša – czasownik w subiunctivie (7.5):  
eqlum mala ina qatišunu ibaššu – pole, o ile w rękach ich się znajduje.

13.16. Starobabiloński formularz listu  
List rozpoczyna się od nazwania adresata. Forma ana NN qibima „do NN mó-wię” skierowana jest do kogoś umiejącego czytać, który odczytuje list właściwemu ad-resatowi. Następuje po tym wskazanie nadawcy: umma NN-ma „w ten sposób mówi NN”. W listach do przełożonego lub równego pozycją znajduje się formułka błogosła-wieństwa, której brakuje w korespondencji skierowanej do podwładnego; najczęściej brzmi ona następująco: dšamaš liballitka „Szamasz niech cię zachowa w zdrowiu”. Cy-taty z wypowiedzi osoby trzeciej są szczególnie zaakcentowane np. przez zwrot kiam NN ulammidanni „tak powiadamia mnie NN”; zwrot taki znajduje się nie tylko przed, ale i po cytacie.

Ćwiczenia

šumma bab ekallim ta-ri-ik-ma u ši-it-ta u-ba-na-ti-ja a-na libbim ub-ba-al – jeśli „bra-ma pałacu” jest ciemna i dwa palce moje do środka zmieszczę  
ša-al-la-at a-ša-al-la-lu dadad i-ra-ah-hi-ic – łup zdobędę Adad pobije (wroga)  
a-hu-um a-ha-šu i-da-ak – brat brata swego zabije  
a-pi-il šar-ri-im a-ba-šu i-da-ak-ma kussi abi i-be-el – syn króla ojca swego zabije i tron ojca opanuje  
te-eš-mu-um ca-ba-a-ka ša a-na ke-er-ri-im ta-ta-ra-du it-ti nakrim in-na-am-ma-ar – wysłuchanie oddział twój który na wyprawę wyślesz z wrogiem spotka się  
ni-šu bi-ša-ši-na a-na ma-hi-ri-im u-še-ce-a – ludzie mienie swoje za cenę rynkową od-dadzą  
re-du-a a-na ekallim i-ir-ru-bu – żołnierze moi do pałacu wejdą

Starobabilońskie listy  
a-na dšamaš-ha-ze-er qi-bi-ma umma ha-am-mu-ra-pi-ma mi-li-i-qi-ša-am narum rak-bum ki-a-am u-lam-mi-da-an-ni um-ma šu-u-ma mdsin-iriš u ap-lum ah-hi a-bi-ja eqli ib-ta-aq-ru-ni-in-ni ki-a-am u-lam-mi-da-an-ni wa-ar-ka-tam pu-ru-us-ma eqlam ma-la i-na qa-ti-šu-nu i-ba-aš-šu-u mi-it-ha-ri-iš zu-us-su-nu-ši-im – do Szamaszhazer mówię w ten sposób (mówi) Hammurabi Iliqiszam śpiewak posłaniec w ten sposób powiada-mia mnie tak mówi on Sinirisz i Aplum bracia ojca mojego pól odmówili mi tak powia-damia mnie sprawę zbadaj i pole o ile w rękach ich znajduje się po równo podziel im

a-na dšamaš-ha-ze-er mdsin-mu-ša-lim u tap-pe-šu-nu qi-bi-ma um-ma ha-am-mu-ra-pi-ma lue-di-kuMEŠ ki-a-am u-lam-mi-du-ni-in-ni um-ma šu-nu-ma i-na eqlim ša be-el-ni ik-nu-ka-an-ni-a-ši-im 6 bur qa-qa-ad eqli-ni dšamaš-ha-ze-er u tap-pu-šu i-ki-mu-ni-a-ti-ma pu-ha-am a-šar-ša-ni eqlam u-ka-al-lu-ni-a-ši-im ki-a-am u-lam-mi-du-ni-in-ni a-na mi-nim qa-qa-ad eqli-šu-nu te-el-qe-a-ma a-šar-ša-ni eqlam ta-ad-di-na-šu-nu-ši-im ka-ni-kam ša ak-nu-ku-šu-nu-ši-im am-ra-ma a-na pi-i ka-ni-ki-im-ma šu-a-ti eqlam id-na-šu-nu-ši-im eqlam a-šar-ša-ni la ta-na-ad-di-na-šu-nu-ti – do Szmaszhazer Sinmuszalim i towarzyszy ich mówię w ten sposób (mówi) Hammurabi koszykarze w ten sposób za-wiadamiają mnie tak mówią oni z pola które pan nasz nadał dokumentem nam 6 bur łącznie pola naszego Szamaszhazer i towarzysze jego zabrali nam i odszkodowanie (ja-kim jest) gdzie indziej (położone) pole wstrzymują nam w ten sposób powiadamiają mnie dlaczego łączną ilość pola ich wzięliście i gdzie indziej pole daliście im dokument który opieczętowałem im sprawdziłem i według brzmienia dokumentu tego właśnie pole dajcie im pola gdzie indziej nie dawajcie im

a-na dšamaš-ha-zer qi-bi-ma um-ma awil-dninurta-ma dšamaš li-ba-al-li-it-ka aš-šum eqil mdšum-ma-ilum ša mra-bi-dsataran ca-ab-tu eqlam a-na kaspim i-ša-am …. ki-ma un-ne-du-uk-ki ta-am-ma-ru eqlam ša-a-ti a-na ra-bi-dsataran-ma i-di-in a-li-ik i-na-an-na aš-pu-ra-ak-ku ci-ri-im-ma a-pu-ul-šu – do Szmaszhazer mówię w ten sposób (mówi) Awilninurta Szamasz niech zachowa w zdrowiu ciebie w sprawie pola Szum¬mailum któ-re Rabisataran wziął pole za pieniądze kupił …. kiedy list mój przeczytasz pole owo Ra-bisataranowi daj idź teraz napisałem do ciebie trud zadaj sobie i zadowol go

a-na mu-ha-ad-du-um qi-bi-ma umma dajjanu babili-ma dšamaš u dmarduk li-ba-al-li-tu-ku-nu-ti aš-šum di-nim ša mil-šu-i-bi-šu mar warad-dsin u mma-at-ta-tum a-wa-ti-šu-nu ni-mu-ur di-nam ki-ma si-im-da-at be-li-ni nu-ša-hi-is-su-nu-ti-ma mi-im-ma nu-du-un-na-a-am ša mma-at-ta-tum a-na ma-ar-ti-ša id-di-nu-ma a-na bit il-šu-i-bi-šu u-še-ri-bu-ši a-na ma-at-ta-tum tu-ur-ra-am ni-iq-bi redam it-ti-ša ni-it-tar-dam mi-im-ma ba-al-ta-am ša i-na-an-na in-na-at-ta-lu a-na ma-at-ta-tum li-id-di-nu – do Muhaddum mówią w ten sposób (mówią) sędziowie Babilonu Szamasz i Marduk niechaj zachowają w zdrowiu was co do procesu Ilszuibbiszu syna Waradsina i Mattatum słowa ich zbada-liśmy proces zgodnie z decyzją pana naszego wszczęliśmy im i całość posagu który Mat-tatum córce swojej dała i do domu Ilszuibbiszu kazała zanieść Mattatum zwrócić naka-zaliśmy żołnierza z nią posłaliśmy wszystko (co) nieuszkodzone co teraz jest wi¬doczne Mattatum mają oddać (=co znajdą, córka musi oddać)  
Lekcja czternasta

14.1. Wrostek –ta- modyfikujący temat  
Wrostek ten odróżnić należy od –ta- służącego do tworzenia perfektu. Obok tematów głównych G, D i Š są tematy przezeń wzdłużone i oznaczane jako Gt, Dt i Št. Te dodatkowe tematy, tak jak tematy główne, tworzą wszystkie czasy, formy imienne oraz tryb rozkazujący.

14.2. Temat Gt  
Jest on używany najczęściej w znaczeniu zwrotnym:  
mahacum – bić, mithucum (bezokolicznik Gt) – walczyć pomiędzy sobą  
akalum – jeść, atkulum – gryźć się pomiędzy sobą  
cabatum – chwytać, ticbutum (14.8) – dotykać się nawzajem  
ittul – spogląda, ittatlu – spoglądają wzajem na siebie.  
W przypadku niektórych czasowników temat Gt jest nośnikiem znaczenia typu „coś przez dłuższy czas robić”; dotyczy to przede wszystkim czasowników oznaczają¬cych chodzenie:  
alakum – iść, atlukum – iść bez przerwy, wlec się bez końca  
ucci – wychodzi, ittacci (14.6) – przenosi się  
irrub – wchodzi, iterrub – wchodzi na zawsze.

14.3. Tworzenie tematu Gt czasowników trójspółgłoskowych  
Wrostek –ta-, przybierający w przypadku czasowników z e-samogłoskowością postać –te-, wchodzi między pierwszą a drugą spółgłoskę i dla form bez przedrostków skraca się do –t-. Czasy z przedrostkami powstają przy użyciu przedrostków tematu podstawowego: i-, ta-, a-, ni- i mają po drugiej spółgłosce samogłoskę rdzeniową, która w Prt przed końcówką samogłoskową wypada:  
Prs: imtahhac – walczy  
Prt: imtahac – walczył, imtahcu – walczyli pomiędzy sobą.  
U czasowników primae alef (4.6) wypadnięcie zamykającego sylabę alefu po-ciąga za sobą wzdłużenie przedrostka samogłoskowego:  
iterrub, iterub – wchodzi, wszedł na stałe.  
Perfekt Gt tworzony jest z dwoma wrostkami –ta-, z których jeden jest cechą czasu (9.1), a drugi tworzy rdzeń, skracając się do –t-:  
imtathac – walczył.  
Bezokolicznik, tryb rozkazujący i stativ Gt mają przed wrostkiem –t- samogło¬skę i, ponadto w trybie rozkazującym po drugiej spółgłosce stoi samogłoska rdzeniowa, zaś w bezokoliczniku i statiwie u:  
mithaca – walczcie ze sobą!  
mithucum – walczyć pomiędzy sobą  
ritkubu – są jeżdżący na sobie.  
Imiesłów: mumtahcum – walczący.  
Prt Gt jest u wszystkich czasowników identyczny co do formy z Pf G (9.2 – 9.3). Powo-duje to, że prawi¬dłowe odczytanie takiej formy jest często trudne

14.4. Temat Gt czasowników dwuspółgłoskowych  
Czasowniki ultimae vocalis tworzą temat Gt analogicznie do czasowników trój-spółgłoskowych:  
Prs: irtaddia, Prt: irtadia – idą/szły ze sobą (są/były równoległe).  
W przypadku czasowników na n zachodzi asymilacja n na t:  
ittakkira (&lt;\*intakkira) – są wrogie wobec siebie  
ittatlu – spoglądali na siebie.  
Przy tworzeniu tematu Gt czasowników na w augment rdzeniowy w zastępo-wany jest przez t (por. uwagi na temat Pf G w punkcie 9.3 i na temat czasownika wa-cûm w punkcie 14.6).  
W przypadku czasowników mediae vocalis zaświadczony jest jedynie Prt G: id-duku – zabijają się wzajemnie (14.9).

14.5. Temat Gt czasownika alakum  
Czasownik alakum „iść” (4.7) tworzy Prs i Prt Gt z podwojeniem t:  
ittallak – odchodzi  
ittalak – odszedł (jak Pf G, najczęściej z wentywem: ittalkam – szedł).

14.6. Temat Gt czasownika wacûm  
wacûm (\*wci) „wychodzić” (8.8) tworzy Prs Gt ittacci „ciągnąć dalej” i Prt Gt ittaci „ciągnął dalej” (identyczny formalnie z Pf G „wyszedł” – 9.3).

14.7. Temat Gt czasownika izuzzum  
Nieregularny czasownik izuzzum „stać” (11.8) tworzy Prs Gt ittazzaz „staje (przed kimś)”. Nie tworzy Prt Gt.

14.8. Zamiana spółgłosek w temacie Gt  
Czasowniki, których pierwszą spółgłoską rdzeniową jest d, t, s, c albo z tworzą nieregularnie bezprzedrostkowe formy tematu Gt (tryb rozkazujący, bezokolicznik i stativ): pierwsza spółgłoska i t wrostka zamieniają się miejscami, tak że formy wspo-mniane rozpoczynają się od t:  
ticbutum – łapać się wzajemnie (zamiast \*citbutum)  
tidkušat – jest obustronnie nabrzmiały (zamiast \*ditkušat).

14.9. Asymilacja w Gt  
t wrostka –ta- ulega asymilacji w przypadku czasowników, które jako pierwszą spółgłoskę mają d, t, s, c albo z:  
taccabbata (&lt;\*tactabbata) – łapiecie się wzajemnie  
idduku (&lt;\*idtuku) – zabijają się wzajemnie.  
Tak samo asymiluje się wrostek –ta- perfektu (9.2):  
nittardam – wysłaliśmy.

14.10. Tryb rozkazujący tematu D  
Tryb rozkazujący D ma formę Prt D (ukaššid – 11.2), ale bez przedrostka i po zastąpieniu samogłoski pierwszej sylaby przez u:  
kuššid – wypędź!  
gummer – wykończ!  
W przypadku czasowników ultimae vocalis wygląda to następująco:  
putti – otwórz! (Prt: upetti – 11.4),  
w przypadku czasowników mediae vocalis tak:  
kin – zrób mocnym! kinna – zróbcie mocnym!  
ter – zwróć! terra – zwróćcie!  
a czasowników zaczynających się na w:  
wuššer – rozwiąż! (a po wypadnięciu będącego w nagłosie w: uššer).

14.11. Odmiana w liczbie pojedynczej stativu  
Obok 3 osoby liczby pojedynczej rodzaju męskiego cabit „jest łapany/jest ła-piący” oraz 3 osoby liczby pojedynczej rodzaju żeńskiego cabtat (1.5) są jeszcze druga i pierwsza osoba stativu. Mają one następujące końcówki:  
2 os. l.p. r.m.: -at lub -ati  
2 os. l.p. r.ż.: -ati  
1 os. l.p.: -aku  
(u czasowników z e-samogłoskowością odpowiednio –et, -eti oraz –eku).  
Końcówki te dołączają się – podobnie jak końcówka 3 os. l.p. r.ż. –at – do formy 3 osoby l.p. r.m., przy czym samogłoska stativu wypada (2.6):  
cabtaku – jestem łapany, łapana / jestem łapiący, łapiąca  
cehreti – jesteś mała (4.5).  
W subiunctivusie formy te brzmią tak samo, bo końcówka subiunctivusa –u dołącza się tylko do form bezkońcówkowych:  
al lawiat (= alam ša lawiat – 7.5) – miasto, które otoczyłeś.

14.12. Złożenia imienne  
Nazwy miar burum „bur” (miara powierzchni) i kurrum „korzec” (miara zboża) łączą się z wyrazami eqlum „pole” i še’um „zboże, ziarno” tak ściśle, że tworzą nowe wyrażenia podlegające jako całość regułom deklinacji:  
N: bur-eqlum  
G: bur-eqlim  
A: bur-eqlam  
(początkowo bur eqlim we wszystkich trzech przypadkach).  
Podobnie jest też z innymi wyrazami oznaczającymi miary: 10 šiqil kaspam „dzie¬sięć szekli srebra” (w bierniku).

Ćwiczenia

šumma erištum u kakkum it-ta-at-lu-u – jeśli „pragnienie” i „broń” spoglądają ku sobie  
šum-ma šitta cibatum ir-ta-di-a – jeśli dwie narośle są równoległe  
šumma martum i-mi-tam u su-me-lam ti-id-ku-ša-at – jeśli woreczek żółciowy z prawa i z lewa jest nabrzmiały  
šumma iz-bu-um qa-qa-as-su a-na ha-al-li-šu ka-mi-is-ma it-ti zi-ib-ba-ti-šu ti-ic-bu-ut – jeśli potworka głowa w kierunku uda jego jest zwrócona i z ogonem jego jest zro¬śnięta  
mi-lu-um i-na na-ri-im it-ta-za-az – powódź na rzece zatrzyma się  
ma-tum ši-i su-un-qa-am i-mar naker-ša e-li-ša it-ta-za-az – kraj ów biedy zazna, wróg jego nad nim triumfować będzie  
a-hi-ta-am na-ak-ru-um it-ta-at-ta-al – na stronie wróg odwróci spojrzenie  
wa-cum it-ta-la-ak a-na a-lim i-ru-ub – wychodzący odejdzie do (jakiegoś) miasta wej-dzie  
u-tu-uk-ku a-na ekallim i-te-er-ru-ub – dobry duch do pałacu wejdzie na stałe  
at-ta u na-ke-er-ka ta-ac-ca-ab-ba-ta-a-ma a-hu-um a-ha-am u-ša-am-qa-at – ty i wróg twój pochwycicie się wzajemnie i brat brata doprowadzi do upadku  
a-al wa-aš-ba-a-ti in-na-aq-qa-ar – miasto zamieszkane przez ciebie będzie zniszczone  
al la-wi-at a-na libbi-šu te-ru-ub – (do) miasta które otoczyłeś wkroczysz  
šumma bab ekallim 2-ma ri-it-ku-bu-u šukkalum kussi be-li-šu iš-te-ne-e – jeśli dwie „bramy pałacu” jadą wierzchem na sobie wezyr tron pana swego zmieni na długo  
šum-ma mar-tum ip-li-uš-ma it-ta-ci a-wi-lum it-ta-ac-ci – jeśli woreczek żółciowy jest przebity i oddala się człowiek odejdzie  
šum-ma mar-tum i-ši-id-sa le-ti ma-ta-an it-ta-ak-ki-ra – jeśli podstawa woreczka żół-ciowego jest rozszczepiona obydwa kraje poróżnią się  
šum-ma mar-tum u u-ba-nu-um ši-it-nu-na-a pu-uh-ru-um u-la im-ta-ga-ar – jeśli wore¬czek żółciowy i „palec” są podobne do siebie zgromadzenie nie dojdzie do porozumie¬nia  
šumma uban hašim qablitum šu-me-el-ša pa-te-er ku-ši-id la ta-ka-la – jeśli „palca” płucnego środkowego strona lewa jest podzielona wypędź nie żyw

Listy starobabilońskie

a-na dšamaš-ha-ze-er mdsin-mu-ša-lim u tap-pe-šu-nu qi-bi-ma um-ma ha-am-mu-ra-pi-ma a-šar 4 bur eqil a-na ištar-illas-su na-da-nam e-si-ha-ak-ku-nu-ši-im 5 bur eqlam bit a-bi-šu gu-um-me-ra-šum-ma id-na-šum – do Szamaszhazer Sinmuszalim i towarzy¬szy ich mówię w ten sposób (mówi) Hammurabi zamiast 4 bur pola (które) aby Iszta¬rillassu dać przydzieliłem wam 5 bur pola domowi ojca jego uczyńcie pełnymi mu i dajcie mu

a-na dšamaš-ha-ze-er qi-bi-ma um-ma ha-mu-ra-pi-ma milum a-la-kam i-ip-pe-ša-am mu-u ma-a-du nam-ka-ri ša a-na apparim ša-ak-nu pu-ut-ti-i-ma eqlam ša i-ta-at lar-sam me-e mu-ul-li – do Szmaszhazer mówię w ten sposób (mówi) Hammurabi powódź przyjście czyni (=nadchodzi) wody wielkie kanały które na bagnach znajdują się otwórz i pole które wokół Larsy (rozciąga się) wodą napełnij

a-na dšamaš-ha-ze-er qi-bi-ma um-ma awil-dnin-urta-ma dšamaš li-ba-al-li-it-ka 6 bur a-ha-mar-ši i-na uruši-ri-im-tim 12 bur dsin-iriš 12 bur bitum-ra-bi eqlam ki-in-šu-nu-ši-im la i-ta-ar-ru-ma la i-ma-ha-ru-ni-in-ni pi-qa-at um-ma a-ha-mar-ši-ma 10 bur eqlam ša-ak-na-ku 6 bur eqlam-ma tu-ka-an-šum – do Szamaszhazer mówię w ten sposób (mówi) Awilninurta Szmasz niech zachowa w zdrowiu ciebie 6 bur Ahamarszi w Szi-rimtim 12 bur Sinirisz 12 bur Bitumrabi pole zatwierdź im niech nie powracają do tego i nie zwracają się do mnie (więcej) może Ahamarszi (powie) 10 bur pola uprawiam (ale mimo to) 6 bur pola tylko zatwierdzisz mu

a-na šamaš-ha-ze-er qi-bi-ma um-ma awil-dnin-urta-ma šamaš li-ba-al-li-it-ka aš-šum eqlim ša mu-ta-mi-ša-ra-am narim ša urugu-na-tum ša a-na mi-li-e-mu-qi na-ad-nu a-wi-lum an-ni-ki-a-am a-na eqlim ša-a-ti tu-up-pu a-nu-um-ma tu-up-pi uš-ta-bi-la-ak-kum še-a-am u eqlam a-na i-li-e-mu-qi te-e-er – do Szmaszhazer mówię w ten sposób (mó-wi) Awilninurta Szmasz niech cię zachowa w zdrowiu co do pola Utamiszaram śpiewaka z Gunatum które Iliemuqi jest dane człowiek tutaj na polu tym jest zapisany odtąd do-kumenty moje każę przynosić tobie zboże i pole Iliemuqi zwróć

a-na awil-dba-u qi-bi-ma um-ma a-hu-um-ma… dsin-a-bu-šu u ma-ra-ti-šu u-še-er – do Awilbau mówię w ten sposób (mówi) Ahum… Sinabuszu i córkę jego uwolnij  
Lekcja piętnasta

15.1. Temat Dt  
Temat ten, powstający po wzdłużeniu tematu D modyfikującym wrostkiem –ta- (14.1), wyraża w temacie D (11.1) stronę bierną:  
uwaššar – poluźnia się  
utaššar – jest poluźniony.

15.2. Tworzenie tematu Dt czasowników trójspółgłoskowych  
Formy tematu Dt różnią się od odpowiadających im form tematu D tylko tym, że po pierwszej spółgłosce pojawia się w nich wrostek –ta-:  
Temat D Temat Dt  
Prs uhabbat – rabuje uhtabbat – jest rabowany  
Prt uhabbit – rabował uhtabbit – był rabowany  
Pf uhtabbit – rabował uhtatabbit – był rabowany

W przypadku e-klasy czasowników zaczynających się na ‘ wrostek –ta- przy¬biera postać –te-:  
utesser – jest zamknięty (Prs D – ussar).  
Tryb rozkazujący Dt ma postać:  
hutabbit – bądź rabowany! (tryb rozkazujący D – hubbit – 14.10).  
W temacie Dt nie są tworzone formy stativu, przymiotnika odczasownikowego i bezokolicznika.  
Uwaga: Prt Dt brzmi identycznie jak Pf D

15.3. Tworzenie tematu Dt czasowników dwuspółgłoskowych  
Prs Dt ma postać analogiczną do uhtabbat (15.2):  
uqtatta – jest zrobiony do końca (od qti „być do końca”).  
Czasowniki zaczynające się na w mają wzdłużoną samogłoskę przedrostka:  
Prs Dt: utaššar – jest rozluźniony  
Prt D: utaššer – był rozluźniony (= Pf D).

15.4. Tryb rozkazujący tematu Š  
Forma ta odpowiada Prt Š (ušamqit – 12.2) bez przedrostka i z pierwszą samo-gło¬ską zastąpioną przez u:  
šumqit – każ padać!  
w przypadku czasowników primae alef:  
šuhiz – każ brać (Prt Š: ušahiz – 12.2)  
podobnie jak w przypadku czasowników na w (12.7):  
šušiba – każcie siedzieć!  
Tryb rozkazujący czasowników ultimae vocalis ma postać typu:  
šurši – każ otrzymać! šuci – każ wyjść! czy šucia – każcie wyjść!  
u czasowników mediae vocalis (12.5) brzmi to następująco:  
šumit – każ umierać!

15.5. Czasownik warûm „dowodzić”  
Temat Š czasownika warûm (wru) tworzony jest inaczej niż w przypadku wacûm „wy-chodzić” (12.4), bo z samogłoską a:  
Prs: ušarra – każe dowodzić  
Prt: ušari – kazał dowodzić  
TR: šuriam – każ dowodzić!  
l.mn.: šurianim.  
Temat podstawowy tego czasownika jest najczęściej zastępowany przez formy wtórnie tworzonego czasownika tarûm (tru) „iść”:  
Prs: itarru – idzie  
Prt: itru – szedł  
Prek.: litrunikkum – muszą ci iść.

15.6. Czasownik wârum „odchodzić”  
Jest to czasownik zaczynający się na w i mediae alef (9.6). W temacie D tworzy Prs: uwâr „poleca” i Prt: uwěr „polecał”.

15.7. Zaimki osobowe  
Dla 1 i 2 osoby istnieją specjalne zaimki osobowe:  
atta (r.ż. atti) – ty  
attunu (r.ż. attina) – wy  
anaku – ja (dla obu rodzajów)  
ninu – my (dla obu rodzajów).  
W roli zaimków osobowych 3 osoby występują zaimki wskazujące:  
šu – ten, dany; on  
ši – ona  
šunu – oni  
šina – one (11.10).  
Zaimki te używane są w celu szczególnego podkreślenia podmiotu, przede wszyst¬kim w zdaniach nominalnych, np.  
umma atta-ma – w ten sposób ty (mówisz).

15.8. Zdanie poboczne subjunkcjonalne  
We wszystkich zdaniach pobocznych tworzonych z subjunkcjonami takimi jak kima „skoro tylko”, „jeśli” (13.4), inuma „gdy”, „kiedy” lub adi „aż”, „póki nie” cza-sownik stoi w subjunktivie:  
adi še’um… uqtattu – póki zboże… nie jest gotowe (dojrzałe?).

15.9. Zdanie główne po šumma  
Nie stoi w subjunktivie orzeczenie zdania warunkowego poprzedzonego przez wyraz šumma „jeśli”; jest ono traktowane jak zdanie główne; šumma właściwie do-słownie powinno być tłumaczone jako „jest przyjęte”:  
šumma martum ishur – jest przyjęte, że woreczek zółciowy kręci się = jeśli wo-re¬czek żółciowy kręci się.

15.10. Podporządkowanie logiczne zdania  
Po czasownikach wyrażających mówienie często następuje w jezyku akadyjskim niezwiązane zdanie główne, które należałoby tłumaczyć przez zdanie poboczne (najczę-ściej zaczynające się od „że”):  
šupur bel awatišu… litrunikkum – dosłownie: pisze jego przeciwnik procesowy ma zabrać ci = pisze, że jego przeciwnik procesowy ma ci zabrać…  
temum liššapranniašim abul dšamaš lippettima še’um… lirub – polecenie ma być do nas wysłane, że brama Szamasza ma być otwarta i zboże… ma wejść.

15.11. Bezokolicznik z przyimkami  
W roli celowego zdania pobocznego może stać bezokolicznik z ana:  
ana cenim… šubqumim – do owiec kazania strzyżenia = aby kazać strzyc owce  
Z kolei bezokolicznik z ina zastępuje zdanie czasowe:  
tuppi anniam ina amarim – moja tabliczka ta w czytaniu = skoro tylko ty (wy) tę tabliczkę moją przeczytasz (przeczytacie).

15.12. Imienna i czasownikowa rekcja bezokolicznika  
Bezokolicznik może występować w konstrukcjach zarówno w roli imienia jak i w roli czasownika. Jako imię może stać w połączeniu status constructus, np.  
zanan šaměm – padanie deszczu.  
Jako czasownik łączy się z dopełnieniem w bierniku:  
tuppi anniam ina amarim – skoro tylko tę moją tabliczkę przeczytasz.  
Kiedy dopełnienie wchodzi między przyimek a bezokolicznik przybiera formę dopełniacza zależąc pozornie od przyimka:  
ana abullim nacarim (dla abullam ana nacarim) – do bramy strzeżenia = aby strzec bramy.

15.13. Liczba mnoga słów awilum „człowiek” i cuharum „chłopiec”, „sługa”  
Słowa te od normalnego mianownika liczby mnogiej tworzą dopełniacz/biernik liczby mnogiej awile i cuhare (dopełniacz/biernik liczby podwójnej cuharen „(obaj) chłopcy”).

15.14. Zastosowanie perfektu  
Podstawowym czasem przeszłym jest Prt. Drugi z czasów przeszłych, perfekt, stosuje się wtedy, gdy należy podkreślić, że działanie dopiero co zostało zakończone; łączy się on wówczas często ze słowami anumma „odtąd, teraz” i inanna „teraz”:  
anumma tuppi uštabilakkum – teraz tabliczkę moją kazałem przynieść tobie.  
Jeśli chce się szczególnie podkreślić następstwo dwóch działań przeszłych, sto-suje się pierwszy czasownik w Prt, a drugi w Pf:  
uweramma attardam – poleciłem i (potem) posłałem.  
O znaczeniu perfektu po šumma zob. 9.1.  
Ćwiczenia

ma-ra-tum uh-ta-ab-ba-ta-ma a-bu-um ar-bu-tam i-la-a-ak – córki będą obrabowane i ojciec do ucieczki się rzuci  
bu-tu-qa-tum ub-ta-ta-qa – tamy będą przerwane  
šum-ma na-ap-la-as-tum ki-ma un-qi-im ma-a-tum u-te-es-se-er – jeśli płat wątroby (jest) jak pierścień kraj będzie okrążony  
šum-ma i-na šu-me-el u-ba-nim ka-ak-kum is-hu-ur še-pu-um a-na ma-at na-ak-ri-im u-ta-ša-ar – jeśli na lewej stronie „palca” „broń obraca się noga do kraju wroga (wejdzie? i) rozluźniona będzie  
Starobabilońskie listy

um-ma warad-dgu-la-ma dgu-la dda-mu u dur-ma-šum a-bi li-ba-al-li-tu i-na ma-za-zi-im ša ta-az-za-zu lu-ta-ab-be-er – w ten sposób (mówi) Warad-Gula Gula Damu i Urma-szum ojca mojego niech zachowają w zdrowiu w miejscu w którym stoisz (= przeby-wasz) niech pozwolą zestarzeć się (ci)

a-na … u … qi-bi-ma um-ma sa-am-su-di-ta-na-ma ša ta-aš-pu-ra-nim um-ma at-tu-nu-ma še-um ša i-na er-ce-et sippar-ja-ah-ru-um ib-ba-šu-u i-na pa-ni umman nakrim i-na ki-di a-na na-de-e u-ul na-tu be-el-ni li-iq-bi te-mu-um li-iš-ša-ap-ra-an-ni-a-ši-im abul dšamaš li-ip-pe-ti-ma še-um šu-u a-na li-ib-bi-a-lim li-ru-ub ša ta-aš-pu-ra-nim ki-ma še-um me-re-eš a-lim i-na e-ce-di uq-ta-ta-at-tu-u abul dšamaš pe-ti-a-ma a-di še-um me-re-eš a-lim uq-ta-at-tu-u dajjani šu-ši-ba-a-ma a-na abullim na-ca-ri-im la i-ig-gu-u – do … i … mówię w ten sposób (mówi) Samsuditana (to) co napisaliście w ten sposób napisaliście zboże które na terenie Sippar-jahrum dojrzało w obliczu wojska wroga na zewnątrz (=poza miastem) do usypania (=zmagazynowania) nie nadaje się pan nasz niech wyda rozkaz (by) musiano napisać nam (że) brama Szamasza ma się otworzyć i zboże wspomniane do wnętrza miasta ma wejść (oto) co napisaliście skoro tylko zboże upraw miasta dzięki żniwom będzie gotowe bramę Szamasza otwórzcie i dopóki zboże upraw miasta nie będzie gotowe (tu: zmagazynowane) sędziom każcie przebywać (przy bramie) i by w strzeżeniu bramy nie byli opieszali

a-na dšamaš-ha-ze-er qi-bi-ma um-ma ha-am-mu-ra-pi-ma a-nu-um-ma a-wi-le-e eb-bu-tim a-na cenu ša li-tim šaplitim šu-ub-qu-ni-im u-we-e-ra-am-ma at-tar-dam-ma tup-pi an-ni-a-am i-na a-ma-ri-im a-na larsam a-li-ik it-ti awile ša a-na buqumim e-si-hu i-zi-iz-ma buqumam šu-ub-qi-im – do Szmaszhazera mówię w ten sposób (mówi) Ham¬murabi odtąd aby ludziom godnym zaufania owce okręgu dolnego kazać strzyc poleci¬łem i wysłałem (o tym wiadomość) i skoro tylko tabliczkę moją tę przeczytasz do Larsy idź ludźmi których do strzyżenia przydzieliłem zajmij się i strzyżenie nakaż

a-na dsin-i-din-nam qi-bi-ma um-ma ha-am-mu-ra-pi-ma ra-bi-a-an urume-de-e-emki aš-šum hi-bi-il-ti-šu u-lam-mi-da-an-ni a-nu-um-ma ra-bi-a-an urume-de-e-emki šu-a-ti a-na ce-ri-ka at-tar-dam wa-ar-ka-as-su pu-ru-us šu-pur be-el a-wa-ti-šu li-it-ru-ni-ik-kum-ma di-nam ki-ma ci-im-da-tim šu-hi-is-su-nu-ti – do Sinidinnam mówię w ten sposób (mówi) Hammurabi naczelnik Medum o stracie jego zawiadamia mnie teraz naczelnika Medum wspomnianego posłałem do ciebie sprawę jego zbadaj napisz (że) przeciwnika procesowego jego zabrano do ciebie i proces zgodnie z prawem przyznaj im

a-na li-pi-it-dištar u awil-dba-u qi-bi-ma um-ma a-hu-um-ma a-nu-um-ma i-me-ra-am ša msa-si-ja i-iq-bu-ku-nu-ši-im šu-ri-a-ni-im – do Lipitisztar i Awilbau mówię w ten spo-sób (mówi) Ahum teraz osła o którym Sasija mówił wam sprowadzić każcie

a-na na-bi-ddu-la u bal-mu-nam-he qi-bi-ma um-ma dsin-a-ja-ba-aš-ma dšamaš li-ba-al-li-it-ku-nu-ti ig-mil-dsin u ba-aq-qum re’um ka-ni-ki it-ba-lu-ni-ku-nu-ši-im a-na pi-i ka-ni-ki še-am i-na še bilat i-din-ja-tum šu-ci-a šum-ma še-um ina še bilat i-din-ja-tum la i-ba-aš-ši i-na še bilat dmarduk-la-ma-sa-šu šukkalim šu-ci-a – do Nabi-Gula i Balmu-namhe mówię w ten sposób (mówi) Sinajabasz Szamasz niech zachowa was w zdrowiu Igmil-Sin i Baqqum pasterz dokumenty opieczętowane moje zabrali wam według brzmienia dokumentów zboże ich z daniny Iddinjattum każcie ściągnąć jeśli zboża z daniny Iddinjattum nie ma z daniny Marduklamassaszu gońca każcie ściągnąć  
Lekcja szesnasta

16.1. Temat Št  
Modyfikujący tematy wrostek –ta- (14.1) wzdłuża też temat Š. Części czasowni-ków służy on wtedy jako forma kauzatywna od zwrotnego tematu Gt (14.2):  
Bezokolicznik Gt Bezokolicznik Št  
mithurum – być wzajemnie jednakowym šutamhurum – kazać być wzajemnie jed-nakowym, zrównywać  
ticbutum – chwytać się wzajemnie šutacbutum – kazać się chwytać wzajem-nie, scalać

Niektóre czasowniki mają w Št znaczenie szczególne, np.  
šutešurum – zaprowadzać ład, ustanawiać prawem (od ešerum „być w po-rządku”)  
šutašnûm – czynić podwójnym (od šanûm „być podwójnym”)  
šutebrûm – być przejrzystym (od barûm „widzieć, patrzeć”).  
Są czasowniki występujące jedynie w temacie Št:  
šutecbûm (\*cb’) – planowo realizować, postępować zgodnie z planem  
šuta’ûm (\*whi) – być bezużytecznym.  
Uwaga: Przedstawiony tu temat Št jest w gramatykach i słownikach oznaczany jako leksykalny Št lub Št2 w odróżnieniu od bardzo rzadkiego biernego Št czyli Št1. Oba te-maty Št różnią się formą tylko podczas tworzenia Prs (Št1 – uštapras, Št2 – uśtaparras –16.2)

16.2. Tworzenie tematu Št czasowników trójspółgłoskowych  
Formy Št różnią się od analogicznych form Š tym, że po przedrostku rdzenio¬wym š(a) następuje wrostek –ta-. Jednakże w Prs środkowa spółgłoska podwaja się.  
Temat Š Temat Št  
Prs ušacbat – każe łapać uštacabbat – każe się łapać wzajem-nie  
Prt ušacbit – kazał łapać uštacbit – kazał się wzajemnie łapać  
Pf uštacbit – kazał łapać (= Prt Š) uštatacbit – kazał się wzajemnie łapać  
TR šucbit – każ łapać! šutacbit – każ się wzajemnie łapać!  
Bezok. šucbutum – kazać łapać šutacbutum – kazać się wzajem łapać  
St šucbut šutacbut – jest/był scalony  
W przypadku e-klasy czasowników na ‘ (i na j – 4.8) zamiast –ta- wchodzi –te-:  
šutešer – rób porządek.

16.3. Tworzenie tematu Št czasowników dwuspółgłoskowych  
Tworzenie to w przypadku czasowników ultimae vocalis przebiega analogicznie jak u czasowników trójspółgłoskowych:  
Prs: uštašanna (šni) – robi podwójnym  
Prt: uštebri (bri) – był przejrzystym  
St: šutašnu, r.ż. šutašnât – jest/był(a) podwojony(a)

16.4. Czasowniki z przerwą głosową w środku i spółgłoskową przerwą głosową  
Większość czasowników mediae alef naśladuje w tworzeniu form czasowniki z samogłoską w środku (9.6). Prócz nich istnieją jednak takie, u których przerwa głosowa nie wypada; tworzą one formy tak jak czasowniki trójspółgłoskowe, np. na’adum (i) „pilnować, dać znak”:  
Prs: ina’’id  
Prt: i’’id (&lt;\*in’id)  
TR: i’id (por. idin – 13.3).  
Wszystkie czasowniki mediae alef, także te, które naśladują czasowniki mediae vocalis, tworzą imiesłów ze spółgłoskową przerwą głosową:  
ša’imum – kupujący (od šâmum)  
ba’irum – rybak (od bârum „łowić”)  
Także stativ jest tworzony z alefem:  
na’id – zwraca uwagę, r.ż. na’adat.  
Uwaga: podwojenie alefu np. w słowie ina’’id jest w transkrypcji (0.15) oznaczane ze względu na mor¬fologię (tu: podwojenie środkowej spółgłoski). Nie można jednak utrzymywać, że reduplikacja przerwy głosowej jest realna fonetycznie. To samo doty¬czy zdwojonego j w wyrazach dajjanum, kajjanum itp.

16.5. Czasownik šuta’ûm „być bezczynnym”  
Rdzeń w’i występuje tylko w temacie Št:  
Prs: ušta’’a – jest bezczynny  
Prt: ušta’i – był bezczynny  
Proh: la tušta’’aši – nie bądź bezczynny wobec niej.

16.6. Czasownik idûm  
Od idûm (jd’) „wiedzieć” tworzy się tylko czas przeszły Prt, który używany jest zresztą jako stativ (tzw. stativ z przedrostkami – 11.9):  
ide – wiem  
tide – wiesz  
ide – wie.  
Imiesłów – mudûm „wiedzący”.  
16.7. Asymilacja n  
n może być asymilowane przed przyrostkami zaimkowymi i –ma:  
idiššum (&lt;idin+šum) – daj mu  
inaddišši (&lt;inaddin+ši) – daje jej  
šakimma (&lt;šakin+ma) obok šakinma – jest położony i…

16.8. Apokopowe przyimki zaimkowe  
Jeśli zaimki będące przyrostkami –šu „jego” oraz –ši „jej” (5.8) następują po formach czasowników z samogłoską w wygłosie, ulegają niekiedy skróceniu do –š:  
la tanaddiš (&lt;tanaddi+šu/ši) – nie powinieneś jego/jej rzucać (13.6).

16.9. Nazwy osobowe  
Akadyjskie nazwy osobowe są albo imionami o charakterze nazw (Aplum, Lipit-dIštar) albo imionami o charakterze zdań (dSîn-iriš, Aham-arši). Mogą być zapisywane bez determinatywu m (0.20). Imiona są nieodmienne, mają więc we wszystkich przy-padkach tylko jedną formę: mar Ahum „syn Ahuma” w odróżnieniu od mar ahim „syn brata”.

16.10. Imiona będące nazwami  
Imiona wyrażające czułość występują w postaci nazw wyrażających pokrewień-stwo czy nazw zwierząt albo imiesłowów:  
Aplum – spadkobierca, syn  
Ahum – brat  
Baqqum – komar  
Sasija – mój mól  
Muhaddum – cieszący się (imiesłów D od hadûm).  
Imiona będące nazwami to także rozmaite imiona gramatyczne zestawione z imionami bogów w formę status constructus:  
Awil-dNinurta – człowiek Ninurty  
Warad-dSîn – sługa Sina  
Lipit-dIštar – dzieło Isztar  
Nabi-dGula – nazwany Guli  
Nur-dKabta – światło Kabty  
Apil-ilim – spadkobierca boga.

16.11. Imiona będące zdaniami  
Większość imion o charakterze zdań składa się z podmiotu (będącego najczę-ściej imieniem konkretnego boga lub nazwą kojarzoną z boskością jak ilum „bóg” czy bitum tu: „świątynia”) oraz orzeczenia (najczęściej w formie Prt lub St); inne imiona składają się z dopełnienia i formy czasownikowej (najczęściej w 1 osobie liczby poje-dynczej):  
A/ imiona bogów + czasowniki w Prt:  
dSîn-iriš – Sin zażądał  
dSîn-idinnam (&lt;iddin+am) – Sin dał mi  
Ili-iqišam – mój bóg wysłał mi  
Ilšu-ibbišu – jego bóg nazwał go  
Imgur-dSîn – Sin zgodził się  
Igmil-dSîn – Sin okazał łaskę  
Iddin-jattum – mój dał (w domyśle: mój bóg opiekuńczy) (6.8).  
B/ imiona bogów + czasowniki w St:  
dŠamaš-hazer – Szamasz jest pomocnikiem  
dŠamaš-emuqi – Szamasz jest moją siłą  
Ili-emuqi – bóg jest moją siłą  
dSîn-illassu – Sin jest jego rodziną  
dMarduk-lamassašu – Marduk jest jego bóstwem opiekuńczym  
dSîn-mušallim – Sin jest podtrzymującym zdrowie  
Rabi-dSataran – Sataran jest wielki  
Bitum-rabi – świątynia jest wielka.  
C/ Imię boga + prekativ:  
dŠamaš-liwwir – Szamasz niechaj świeci (od nawarum)  
D/ Dopełnienia + czasowniki w Prt:  
Aham-arši – otrzymałem brata  
Uta-mišaram – znalazłem prawo (od wta – 8.8)  
E/ Niektóre imiona są szczególnego rodzaju:  
dSîn-aijabaš – Sinie, niechaj cię nie zniweczę (partykuła vetitivu ai – 20.14 oraz abaš od bâšum)  
Šumma-ilum – jeśli bóg (w domyśle: zechce, niech pomaga).

16.12. Imiona nieakadyjskie  
Wiele z występujących w tekstach akadyjskich imion nie jest akadyjskiego, lecz np. kanaanejskiego (Hammurapi, Samsuditana) lub sumeryjskiego pochodzenia (Bal-munamhe, Ur-dUtu, Luga, Iškur-gal-bi).  
Ćwiczenia  
šumma i-na i-mi-it-ti bab ekallim pi-il-šum ip-lu-uš-ma u e-le-nu-um uš-te-eb-ri – jeśli z prawej strony „bramę pałacu” dziura przebiła i u góry jest przejrzysta  
šumma e-le-nu pa-da-an i-mi-tim šu-ub-tum šu-ta-aš-na-a-at – jeśli u góry „ścieżki” prawej strony „siedziba” jest podwojona  
šumma a-mu-tum ši-li sa-ah-ra-at-ma u šu-te-eb-ru a-mu-ut šar-rum-ki-in ša ek-le-tam i-ih-bu-ta-ma nu-ra-am i-mu-ru – jeśli wątroba otworkami jest otoczona i także jest przejrzysta wróżba Šarrumkina który mrok przewędrował i światło dojrzał  
šum-ma mar-tum na-ah-sa-at na-a’-da-at – jeśli woreczek żółciowy jest cofnięty bę-dziesz zwracał uwagę  
šumma uban ha-ši qablitum re-sa ša-ti-iq a-na šar-ri-im na-i-id – jeśli wierzchnia część środkowego „palca” płucnego jest odgniecona królowi zwróci uwagę  
Z kodeksu Hammurabiego

šum-ma a-wi-lum a-na bit e-mi-im bi-ib-lam u-ša-bi-il ter-ha-tam id-di-in-ma a-bi mar-tim marti u-ul a-na-ad-di-ik-kum iq-ta-bi mi-im-ma ma-la ib-ba-ab-lu-šum uš-ta-ša-an-na-ma u-ta-ar – jeśli człowiek do domu teścia prezent ślubny kazał przynieść cenę na-rzeczonej dał i ojciec córki córki nie dam tobie powiedział wszystko ile przyniósł mu (człowiek) podwoi i zwróci  
šum-ma a-wi-lum naditam i-hu-uz-ma mari la u-šar-ši-šu-ma a-na mišu-gi-tum a-ha-zi-im pa-ni-šu iš-ta-ka-an a-wi-lum šu-u mišu-gi-tam i-ih-ha-az a-na biti-šu u-še-er-re-eb-ši mišu-gi-tum ši-i it-ti naditim u-ul uš-ta-ma-ah-ha-ar – jeśli człowiek kapłankę naditum poślubił \[a kapłanki te nie mogły mieć dzieci\] i syna nie kazano mieć jemu i aby poślu-bić kapłankę świecką postanowił człowiek ten kapłankę świecką poślubi do domu swego wprowadzi ją kapłanka świecka ta z naditum nie (ma prawa) równać się  
šum-ma a-wi-lum naditam i-hu-uz-ma amtam a-na mu-ti-ša id-di-in-ma mari it-ta-la-ad wa-ar-ka-nu-um amtum ši-i it-ti be-el-ti-ša uš-ta-tam-he-er aš-šum mari ul-du be-le-sa a-na kaspim u-ul i-na-ad-di-iš-ši ab-bu-ut-tam i-ša-ak-ka-an-ši-ma it-ti amatim i-ma-an-nu-ši – jeśli człowiek naditum poślubił i (ona) służącą mężowi swemu dała i (ta) syna urodziła (zaś) później służąca ta z panią swoją próbowała się równać dlatego że syna urodziła pani jej nie sprzeda jej (ale) piętnem niewolnictwa naznaczy ją i do nie¬wolnic zaliczy  
šum-ma itinnum bitam a-na a-wi-lim i-pu-uš-ma ši-pe-er-šu la uš-te-ec-bi-ma igarum iq-tu-up itinnum šu-u i-na kasap ra-ma-ni-šu igaram šu-a-ti u-dan-na-an – jeśli budow-niczy dom człowieka budował i dzieła swego nie wykonał jak trzeba i ściana grozi za-waleniem budowniczy wspomniany na własny koszt ścianę tę na nowo umocni  
Listy starobabilońskie

a-na dšamaš-ha-ze-er qi-bi-ma um-ma dsin-i-din-nam-ma dšamaš u dmarduk li-ba-al-li-tu-ka aš-šum eqil kurummat iškur-gal-bi mur-dutu ahu-šu it-ta-al-ka-ak-kum te-em-šu ma-ah-ri-ka li-iš-ku-un-ma šu-te-še-er-šu – do Szmaszhazer mów w ten sposób (mówi) Sinidinnam Szamasz i Marduk niech zachowają w zdrowiu ciebie w sprawie „pola ży-wienia” \[nadziału\] Iszkurgalbi i Urutu brat jego wyruszył w drogę do ciebie polecenie przed tobą niech wyrazi i każ być prawem mu \[temu poleceniu\]

a-na nu-ur-dkab-ta qi-bi-ma um-ma dšamaš-li-wi-ir-ma dšamaš u dnin-si-an-na a-na da-ri-a-tim li-ba-al-li-tu-u-ka ki-ma immeri ti-šu-u eš-me-ma ⅓ ma-na kaspim ša-a’i-im-ma ka-li-a-ku a-nu-um-ma tup-pi mi-ba-šar-rum cu-ha-ar a-wi-lim ša immeri ha-aš-hu uš-bi-la-ak-kum …. immeri i-di-iš-šum u cu-ha-ru ša il-li-ka-ak-kum it-ti a-la-ak-tim šu-ta-ac-bi-ta-aš-šu – do Nurkabta mów w ten sposób (mówi) Szamaszliwwir Szamasz i Nin¬sianna na zawsze niechaj zachowają cię w zdrowiu że owce masz usłyszałem i ⅓ miny srebra kupującego trzymam w pogotowiu (?) teraz tabliczkę moją Ibbaszarrum słudze człowieka który owiec potrzebuje kazałem przynieść do ciebie …. owce daj mu i słudze który przyszedł do ciebie do karawany każ się przyłączyć

a-na lu-ga-a qi-bi-ma um-ma dšamaš-ga-ar-ri-ma dšamaš li-ba-al-li-it-ka aš-šum mba-ba-a a-ha-ti-ja a-wa-ti-ša ta-mu-ur a-mi-ni di-in-ša la ta-di-in a-na bi-ti a-bi-i-ka u-da-mi-iq u at-ta ti-de-e am-mi-ni tu-uš-ta-‘i-i-ši aš-tap-ra-ak-kum di-in-ša i-ša-ri-iš di-in-ma la tu-uš-ta-‘a-a-ši a-na ha-ba-lim la ta-na-di-iš u te-e-em-ša ga-am-ra-am me-he-er un-ne-du-uk-ki-ja šu-bi-lam – do Luga mów w ten sposób (mówi) Szamaszgarri Szamasz niechaj zachowa w zdrowiu ciebie co do Baba siostry mojej sprawę jej zbadałeś dlaczego nie wymierzyłeś jej sprawiedliwości domowi ojca twego czyniła dobro i ty wiesz (o tym) dlaczego byłeś bezczynny wobec niej napisałem do ciebie osądź ją sprawiedliwie i nie bądź bezczynny wobec niej nieszczęściu nie wydawaj jej i informacją o niej całkowitą (jako) odpowiedź na list mój każ przysłać  
Lekcja siedemnasta

17.1. Tematy częstotliwe  
Na bazie wszystkich czterech tematów głównych (G, N, D i Š) tworzyć można tematy wzdłużone za pomocą wrostka –tan-, które oznacza się jako Gtn, Ntn, Dtn i Štn. Mają one funkcję częstotliwą lub habitatywną (nawykową), a więc oznaczają stałe po-wtarzanie się czynności (w tłumaczeniu wyrażane np. przez dodanie do bezokolicznika wyrażenia „wciąż na nowo”) lub charakteryzują działanie jako zwyczajne czy zwycza-jowe (co można oddać np. przez wyrażenie „mieć zwyczaj”):  
išarriq – kradnie, ištanarriq (Prs Gtn) – ma zwyczaj kraść  
innappah – jest zapalony, ittananpah – jest zapalany wciąż na nowo.

17.2. Temat Gtn  
U czasowników częstotliwych i nawykowych w temacie G (podstawowym) po pierwszej spółgłosce rdzeniowej włączany jest wrostek –tan- (w przypadku czasowni-ków z e-samogłoskowością –ten-). Spółgłoska n wrostka zachowuje się przy tym tylko w Prs (przed samogłoską); we wszystkich pozostałych formach asymilowana jest przez drugą spółgłoskę rdzeniową.

17.3. Czasy z przedrostkami w temacie Gtn  
Prs i Prt są w Gtn tworzone z przedrostkami i-, ta-, a- oraz ni-. Czasowniki wyka-zujące apofonię mają po drugiej spółgłosce samogłoskę a, a pozostałe czasowniki sa-mogłoskę rdzeniową:  
Prs: iptanallah – żyje w ciągłym strachu  
Prt: iptallah (&lt;\*ip-tan-lah) – żył w ciągłym strachu  
ištanarriqu – kradną wciąż na nowo  
imtanaqqut – pada wciąż na nowo.  
W przypadku czasowników primae alef samogłoska przedrostka ulega wzdłuże-niu, ponieważ przerwa głosowa wypada (4.6):  
itanakkal (&lt;\*i’-tan-akkal) – je raz po raz  
iterriš (&lt;\*i’-ten-riš) – uprawiać pole wciąż na nowo.  
Przedrostek a- pierwszej osoby liczby pojedynczej w przypadku e-klasy czasowni-ków primae alef przybiera postać e- (13.11):  
etennerriš (&lt;\*a’-ten-erriš) – uprawiam pole wciąż na nowo.

Uwaga: Formy Prt Gtn brzmią tak samo jak formy Prs Gt (w obu przypadkach np. iptal-lah czy ištarriq). W dodatku podwójne spółgłoski będące efektem asymilacji nie zawsze są oddawane w piśmie, co powo¬duje, że możliwe są też zbieżności form Prt Gtn z for-mami Pf G i Prt Gt (np. iptalah czy ištariq)

Perfekt Gtn (iptatallah) w dialekcie starobabilońskim nie występuje.

17.4. Formy bez przedrostków w temacie Gtn  
Tryb rozkazujący, bezokolicznik i stativ Gtn włączają samogłoskę i między pierw-szą spółgłoskę rdzeniową a wrostek –tan-. Po drugiej spółgłosce tryb rozkazujący ma taką samą samogłoskę jak Prt, zaś bezokolicznik i stativ mają u:  
šitarriq (&lt;\*ši-tan-riq) – kradnij wciąż na nowo  
šitarruqum – mieć zwyczaj kraść  
šitarruq – jest/był kradnącym raz po raz.  
W przypadku a-klasy czasowników primae alef wtrącona samogłoska i jest po wypadnięciu poprzedzającego ją alefu zamieniana na a (por. tekst na temat trybu roz-ka¬zującego G – 13.2):  
atappulum (&lt;\*’i-tan-pulum) – zadawalać wciąż na nowo (rzeczownikowo: opie-ka).

17.5. Czasowniki dwuspółgłoskowe w temacie Gtn  
Czasowniki ultimae vocalis (6.2) tworzą formy tematu Gtn analogicznie jak cza-sowniki trójspółgłoskowe:  
Prs: ištananni – jest zmieniony na stałe  
Prt: ištanni, iptette (&lt;\*ip-ten-te)  
TR: bitanni – wytwarzaj wciąż na nowo.  
Czasowniki mediae vocalis (7.1) tworzą Prs:  
ittanâr (l.mn. ittanurtu) – powraca wciąż na nowo.  
Tryb rozkazujący i bezokolicznik czasowników mediae i tworzony jest w ten sposób, że n jest asymilowane przez (tu półsamogłoskowe) i (czyli j) rdzenia:  
hitajjat (&lt;\*hit) – badaj wciąż na nowo  
bezokolicznik: hitajjutum.  
W przypadku czasowników zaczynających się na n (8.2) augment rdzeniowy n asymilowany jest przez –tan-:  
ittanaddin (&lt;\*in-tan-addin) – ma zwyczaj dawać.  
W przypadku czasowników zaczynających się na w formy tematu Gtn, tak jak Pf G (9.3) i formy tematu Gt (14.4), tworzone są w ten sposób, że augment rdzeniowy w zastępowany jest przez spółgłoskę t i dopiero tak powstały czasownik wtórny jest od-mieniany:  
ittanarrad – schodzi raz po raz.

17.6. Odmiana czasownika alakum w temacie Gtn  
Czasownik ten (4.7) tworzy formy Gtn tak samo jak formy tematu Gt (14.5) czyli z podwojeniem t:  
Prs: ittanallak – idzie wciąż na nowo  
Prt: ittallak.

17.7. Tryb rozkazujący tematu N  
W trybie rozkazującym N (10.2), podobnie jak w bezokoliczniku i statiwie N (10.6), rdzeń poprzedzony jest przez na-; przy tym samogłoską po drugiej spółgłosce jest i – jak w Prt:  
naplis – patrz.  
W przypadku czasowników zaczynających się na ‘ tryb rozkazujący N tworzony jest ze wzdłużeniem powstałym w wyniku wypadnięcia przerwy głosowej (4.6):  
nabit – uciekaj (10.2)  
lub z wtórnym unosowieniem (10.6):  
ittija nanmer – spotkaj się ze mną (10.2).

17.8. Spójnik kima „że”  
kima zależne od czasownika odnoszącego się do mowy albo spostrzeżenia, per-cep¬cji używane jest w znaczeniu „że”:  
kima eleppat ba’iri ittanarrada iqbunim – że statki rybaków pływają raz po raz, powiedziano mi.

17.9. Zdania pytajne  
Zdania pytajne formułowane być mogą albo przy użyciu zaimków pytajnych, albo bez nich. Takimi zaimkami są np.  
amminim – dlaczego?  
mannum – kto?  
mannum beri – kto jest głodny?  
W zdaniach pytajnych bez zaimków pytajnych może być tak, że słowo kończące pytanie jest szczególnie zaakcentowane poprzez przedłużenie ostatniej sylaby:  
bariaku (ba-ri-a-ku-û) – rzeczywiście mam być głodny?

Ćwiczenia

um-ma-nu-um i-na ta-ha-zi-im im-ta-na-aq-qu-ut – wojsko w bitwie będzie padać bez ustanku  
šar-ru-um ša-na-i-šu ip-ta-na-al-la-ah – król przeciwnika swego będzie bał się stale  
mi-il-la-tum ša-ta-mu ekallam iš-ta-na-ri-qu – grabież skarbnicy pałac będą wciąż na nowo okradać  
te-e-em ma-a-tim iš-ta-na-an-ni – władze (?) kraju zmienią się wielokrotnie  
ša li-ša-ni-ja i-na ma-a-tim it-ta-na-al-la-ak – mój tajny agent po kraju wciąż na nowo będzie się przechadzał  
wa-ši-ib ma-ah-ri-ka-a ka-ar-ci-ka i-ta-na-ka-al – siedzący przed tobą wielokrotnie oczerni ciebie

Listy starobabilońskie

a-na dšamaš-ha-ze-er u dmarduk-na-ce-er qi-bi-ma um-ma ha-am-mu-ra-pi-ma eqlam ša a-na kur-gal-a-mah-a-ni ka-an-ku-šum mdsin-im-gur-an-ni mar … MU 2kam i-te-er-ri-iš-ma še-šu il-te-eq-qe u ša-at-tam eqlam šu-a-ti i-ri-iš-ma še-šu il-te-qe … – do Szama-szhazer i Marduknacer mów w ten sposób (mówi) Hammurabi pole które Kurgulama-hani jest zatwierdzone dokumentem z pieczęcią Sinimguranni syn … od dwóch lat uprawiał i zboże jego sprzątał i w tym roku pole to uprawiał i zboże jego sprzątał …

a-na dsin-ša-mu-uh u a-hu-ši-na qi-bi-ma um-ma dmarduk-ka-ši-id-ma a-nu-um-ma mi-ba-aš-ši-ilum u li-pi-it-ištar cu-ha-re-e-en a-na šu-ha-ti-in-nisar ša ma-ah-ri-ku-nu e-pu-šu le-qe-e-em at-tar-dam a-di šu-ha-ti-in-nisar šu-nu in-na-ap-pa-lu hi-ta-ja-ta-šu-nu-ti-ma i-na si-ma-ni-šu-nu li-in-na-ap-lu-ma a-na babilim li-ib-lu-ni-iš-šu-nu-ti – do Sin-szamuh i Ahuszina mów w ten sposób (mówi) Mardukkaszid teraz Ibaszsziilum i Lipi-tisztar dwoje sług aby (roślinę) szuhatinni którą u was uprawiałem zebrać przysłałem dopóki szuhatinni będą dostarczać im pilnujcie ich bezustannie w odpowiednim czasie dla nich \[dla roślin\] niech dostarczą (je) i do Babilonu niech sprowadzą je

a-na i-li-im-gur-ra-an-ni qi-bi-ma um-ma ni-ši-i-ni-šu beli u be-el-ti aš-šu-mi-ja li-ba-al-li-tu-ka am-mi-nim ba-ri-a-ku-ma di-a-ti-i la ta-ša-al i-na bi-ti-ka ma-an-nu-um be-ri a-na-ku-u ba-ri-a-ku-u šipati ša tup-pi-ja a-na ci-ba-at kaspim at-ta-na-di-in u-lu ka¬spam u-lu huracam u-lu šipati ša ½ manem šu-bi-lam – do Iliimguranni mów w ten sposób (mówi) Nisziiniszu pan mój i pani moja ze względu na mnie niechaj zachowają w zdro-wiu ciebie dlaczego mam być głodny i nie dowiadujesz się (co u mnie) w domu twoim któż jest głodny ja mam być głodny wełnę (wedle zapisu) tabliczki mojej jako procent srebra wciąż daję i daję albo srebro albo złoto albo wełnę za pół miny każ przy¬słać

a-na dsin-i-din-nam kar sipparim u dajjani sipparim qi-bi-ma um-ma sa-am-su-i-lu-na-ma ki-ma a-na ugarim ra-bi-i-im u ugarim ša-am-ka-nim eleppat ba’iri it-ta-na-ar-ra-da-ma nuni i-ba-ar-ru iq-bu-nim … – do Sinidinnam urzędu handlowego w Sippar i sę-dziów Sippar mów w ten sposób (mówi) Samsuiluna że do wielkiego pola i pola Szam-kanum łodzie rybaków ustawicznie zjeżdżają i ryby łowią powiedziano (mi)

a-na a-hu-ni qi-bi-ma um-ma be-la-nu-um-ma dšamaš u dmarduk li-ba-al-li-tu-ka … eleppatu iš-tu ge-er-ri-im is-ni-qa-ni-im am-mi-ni-im karanam ta-ba-am la ta-ša-am-ma la tu-ša-bi-lam karanam ta-ba-am šu-bi-lam u at-ta um 10kam al-ka-am-ma it-ti-ja na-an-me-er – do Ahuni mów w ten sposób (mówi) Belanum Szamasz i Marduk niechaj zachowają cię w zdrowiu … statki z wyprawy handlowej przybyły dlaczego wina do-brego nie kupiłeś i nie kazałeś przysłać wina dobrego każ przysłać i ty w ciągu 10 dni przybądź i ze mną spotkaj się

a-na a-hu-ki-nu-um qi-bi-ma um-ma awil-damurrim-ma iš-tu u-mi-im ša a-na ha-ra-ni-im tu-cu-u wa-ar-ki-ka-a-ma mim-gur-dsin il-li-ka-am-ma um-ma šu-u-ma ⅓ ma-na ka-spam e-li-šu i-šu-u aš-ša-at-ka u ma-ra-at-ka it-te-pe-e … aš-ša-at-ka u ma-ra-at-ka i-na ci-bi-ti-im i-na hi-ta-šu-li-im i-mu-tu aš-ša-at-ka u ma-ra-at-ka šu-ci-i a-pu-tum – do Ahukinum mów w ten sposób (mówi) Awilamurrim po dniu w którym z powodu po¬dróży odeszłeś po tobie przyszedł Imgursin i w ten sposób on (rzekł) ⅓ miny srebra u niego mam małżonka twoja i córka twoja w niewolę za długi zostały oddane … mał¬żonka twoja i córka twoja w więzieniu przy mieleniu zboża będą umierały małżonkę twoją i córkę twoją uwolnij (to jest) pilne

Starobabiloński dokument prawny

mipiq-an-nu-ni-tum mar a-pil-i-li-šu itti a-hu-ni-ja um-mi-a-ni-šu mim-gur-dsin mar a-hu-um-wa-qar i-na ja-ab-li-jaki it-ru-šu a-ta-ap-pu-ul ma-pil-i-li-šu a-bi-šu u ta-ad-di-in-nu-nu um-mi-šu i-na qa-ti-i mim-gur-dsin – Ipiqannunitum syna Apililiszu z Ahunija mi-strzem jego Imgursin syn Ahumwaqar w Jablija zabrali opieka (zamiast) Apililiszu ojca jego i Taddinnunu matki jego w rękach Imgursina (jest)

Lekcja osiemnasta

18.1. Temat Ntn  
W temacie N u czasowników częstotliwych i nawykowych (17.1) wrostek –tan- (-ten-) wchodzi między cechę tematu n i rdzeń. Głoska n wrostka zachowuje się tylko w Prs, zaś w pozostałych formach wypada.

18.2. Czasy z przedrostkami w Ntn  
Prs i Prt Ntn tworzone są z przedrostkami i-, ta-, a- oraz nu-. Cecha n tematu N asymilowana jest przez t wrostka –tan-:  
Prs: ittanamhac (&lt;\*i-n-tan-amhac) – jest ciągle bity  
Prt: ittamhac (&lt;\*i-n-tan-mhac) – był ciągle bity.  
Prs Ntn tworzony jest inaczej niż w pozostałych tematach, bo bez podwojenia środkowej spółgłoski rdzeniowej.  
Tak jak w Gtn (17.3) samogłoską po drugiej spółgłosce jest w przypadku czasow-ników wykazujących apofonię a, zaś w pozostałych przypadkach samogłoska rdzeniowa:  
Prs: ittanapqid – jest wciąż na nowo powierzany  
Prt: ittapqid.

18.3. Formy bez przedrostków w Ntn  
W trybie rozkazującym, bezokoliczniku i stativie Ntn przed wrostek –tan- wcho-dzi głoska i-, przy czym wypada znajdujący się przed nią tworzący temat element n:  
TR: itaplas (&lt;\*nitaplas) – patrz wciąż na nowo.  
Samogłoska po drugiej spółgłosce odpowiada w trybie rozkazującym tej, która jest w Prt Ntn (18.2). Bezokolicznik i stativ mają w tym miejscu u:  
Bezokol.: itaplusum  
St: itaplus.

18.4. Temat Ntn czasowników dwuspółgłoskowych  
W przypadku czasowników na n (8.2) w Prs Ntn augment rdzeniowy n nie jest asymilowany:  
ittananpah (&lt;\*i-n-tan-anpah) – jest zapalany wciąż na nowo.  
Stativ Ntn od nadûm ma postać:  
itaddu (&lt;\*nitandu), itaddât – jest kładziony wciąż na nowo.

18.5. Temat Dtn  
W temacie D (11.1) czasowniki częstotliwe i nawykowe występują rzadko i też tworzą formy odpowiedniego tematu pochodnego, czyli Dtn, za pomocą wrostka –tan-. Wchodzi on pomiędzy pierwszą a drugą, zdwojoną społgłoskę rdzeniową. Przedrostki czasów odpowiadają tym, które mają zastosowanie w D i Dt (u-, tu-, u-, nu-). n wrostka –tan- zachowuje się tylko w Prs:  
uktanattan – ma zwyczaj ukrywać.  
W pozostałych formach to n wypada znalazłszy się przed podwojoną środkową spółgło-ską rdzeniową:  
Prt: uktattim (&lt;\*uk-tan-ttim)  
TR: kutattim  
Bezok.: kutattumum.  
Uwaga: wszystkie formy Dtn za wyjątkiem Prs brzmią tak samo jak odpowiednie formy tematu Dt (15.1), np.:  
uhtabbit – był rabowany (Prt Dt) lub rabował raz po raz (Prt Dtn).

18.6. Temat Štn  
Również w temacie Š czasowniki częstotliwe i nawykowe występują rzadko. Wrostek –tan- włączony jest pomiędzy cechę tematu š i rdzeń. Przedrostki czasów są takie same jak w Š i Št (u-, tu-, u-, nu-). n wrostka zachowuje się tylko w Prs:  
uštanamqat – każe wciąż na nowo padać.  
W pozostałych formach element n wypada:  
Prt: uštamqit (&lt;\*u-š-tan-mqit)  
TR: šutamqit  
Bezok.: šutamqutum.  
Tryb rozkazujący Štn od arahum „być pilnym” ma postać šutarrih „skłaniaj cią-gle do pośpiechu”.  
Prs Štn od wacûm (8.8) brzmi uštenecce „każe wychodzić wciąż na nowo” = „ma zwyczaj wygadywać się (zdradzać tajemnice)”.  
Uwaga: wszystkie formy Štn za wyjątkiem Prs brzmią tak samo jak odpowiednie formy tematu Št (16.1), np.:  
šutacbutum – kazać się wzajemnie chwytać (bezokolicznik Št) lub kazać chwytać wciąż na nowo (bez¬okolicznik Štn)

18.7. Czasowniki z przerwą głosową na końcu  
Większość z tych czasowników, jak np. leqûm (lq’) „brać”, naśladuje w tworze-niu form czasowniki ultimae vocalis (konkretnie ultimae e – 6.2). Jednakże w przy¬padku niektórych takich słów przerwa głosowa wcale nie wypada i zachowują się one jak cza-sowniki trójspółgłoskowe:  
maša’um (a/u) – zabierać  
Prs: imašša’  
Prt: imšu’.  
Tylko w tematach D i Dtn występuje czasownik buzzu’um – znęcać się“:  
Prs: ubazza’  
Prt: ubazzi’  
Pf D i Prt Dtn: ubtazzi’.

18.8. Voluntativ  
Jako forma wyrażająca życzenie 1 osoby liczby pojedynczej („ja chcę”) służy voluntativ. Tworzony jest on w ten sposób, że w formie 1 osoby liczby pojedynczej Prt przedrostek a- (e-) lub u- zastępowany jest przez lu-:  
ašpur – pisałem, lušpur – chcę napisać  
ubtam – przyniosłem, lublam – chcę przynieść  
udammiq – czynię dobro, ludammiq – chcę czynić dobro.  
Formą życzenia 3 osoby jest prekativ (13.5 i 18.9).

18.9. Prekativ tworzony z użyciem stativu  
Życzenie dotyczące stanu jest wyrażane przez prekativ statywny, w którym sto-jąca osobno asertoryczna partykuła lu poprzedza stativ:  
lu salim – niech będzie cały  
lu baltata – bądź żywy, długo żyj.  
18.10. Forma zapewnienia  
Aby nadać wypowiedzi szczególnego akcentu, używana jest asertoryczna party-kuła lu stojąca osobno, poprzedzająca odpowiednią formę czasownika:  
ide – wie(m), lu ide – naprawdę wie(m)  
lu ubtazzi’ – doprawdy gnębił wciąż od nowa.

18.11. Paronomastyczna konstrukcja z bezokolicznikiem  
W celu wzmocnienia wypowiedzi forma czasownika może być połączona z bez-okolicznikiem tego samego czasownika. Do stojącego na przedzie bezokolicznika dołą-cza się –ma (8.14):  
ana awilim ana qaběm-ma ul aqbi – człowiekowi naprawdę nie powiedziałem.

18.12. Odmiana zaimków osobowych  
Od zaimków osobowych, które w mianowniku mają formę atta „ty”, anaku „ja” itd. (15.7), mogą być tworzone formy biernika i celownika:  
kâta (r.m.), kâti (r.ż.) – ciebie, kâšim (r.m. i ż.) – tobie  
jâti – mnie, jâšim –mi.  
Formy te służą jako zastępstwo lub wzmocnienie enklitycznych zaimków osobo-wych (5.6, 5.9, 13.10) i są zawsze szczególnie podkreślone:  
jâti amminim mazzaranni – dlaczego wymyśla akurat mi.

18.13. Bezokolicznik z przyimkiem  
Zamiast subiunkcjonalnych zdań pobocznych używany jest niekiedy bezokolicz¬nik z przyimkiem. Konstrukcja ta może być wydłużona przez dopełnienie:  
aššum … itaplusim – że \[np. ty\] regularnie przypatrujesz się  
ištu suluppi apalim – potem daktyle podano.

Ćwiczenia

šumma mar-tum na-as-ha-at-ma it-ta-na-aq-ra-ar – jeśli woreczek żółciowy jest ode-rwany i kilka razy zwinął się  
sinništum a-wa-at pu-uh-ri-im uš-te-ne-ce – kobieta słowo zgromadzenia wypapla raz za razem  
šum-ma mar-tum bu-da-ša da-ma-am bu-ul-la-am pa-aš-ša di-pa-ar ne-ku-ur-tim i-ša-tu-um i-na ma-tim it-ta-na-an-pa-ah – jeśli woreczka żółciowego strona krwią rozło-żoną jest wysmarowana pochodnia wrogości ogień w kraju będzie zapalany wciąż na nowo  
šum-ma i-na bab ekallim ši-lum ip-lu-uš-ma uš-te-eb-ri wa-ši-ib ma-ha-ar šarrim pi-ri-iš-ti šarrim a-na ma-at nakrim uš-te-ne-ce – jeśli w „bramie pałacu” otworek jest prze-bity i jest przejrzysty siedzący przed królem tajemnicę króla krajowi wroga zdradzi  
šum-ma mar-tum zi-ih-hu-um i-ta-ad-du šar-ru-um um-ma-an-šu te-ši-tam i-le-et-te – jeśli woreczek żółciowy pęcherzykami w wielu miejscach jest upstrzony wojsko króla zamieszaniem będzie się rozdzielać  
šum-ma šu-me-el u-ba-nim pu-sa-am i-ta-da-at ti-bu-ut er-bi-im – jeśli na lewej stronie „palca” białe plamy w wielu miejscach są położone najście szarańczy  
Listy starobabilońskie

a-na a-bi-ja qi-bi-ma um-ma zi-im-ri-e-ra-ah-ma dšamaš u dmarduk da-ri-iš u-mi a-bi li-ba-al-li-tu mi-bidnin-šubur cu-ha-ri a-hi mnu-ur-i-li-šu mdna-bi-um-at-pa-lam im-qu-ut-ma ub-ta-az-zi-i’-šu u ja-a-ši-im ma-ag-ri-a-tim ša a-na e-ce-nim la na-ta-a id-bu-ub cu-ha-ri-ma lu ub-ta-az-zi-i’ ja-a-ti am-mi-nim i-na-az-za-ra-an-ni a-na a-wi-lim a-na qa-be-e-em-ma u-ul aq-bi um-ma a-na-ku-ma a-na a-bi-ja lu-uš-pur-ma te-em a-wa-tim li-iš-pur-am-ma a-na a-wi-lim lu-uq-bi … i-zi-iz wa-ar-ka-at a-wa-tim šu-a-ti pu-ru-uš-ma te-ma-am šu-up-ra-am-ma lu-u i-de-e – do ojca mojego mów w ten sposób (mówi) Zim-rierah Szamasz i Marduk na zawsze niechaj ojca zachowają w zdrowiu Ibbininszu¬bura sługę mojego brata Nuriliszu Nabiumatpalam poniżył (?) i znęcał się nad nim i mi obelgi które do zniesienia są niemożliwe powiedział sługę mojego naprawdę dręczył raz po raz dlaczego mnie przeklina człowiekowi naprawdę powiedzieć (nic) nie powie-działem w ten sposób (mówię) ja ojcu mojemu chcę napisać (o wszystkim) i (on) pole-cenie w (tej) sprawie niechaj (mi) przyśle i potem człowiekowi chcę powiedzieć … zaj-mij się stan sprawy zbadaj i postanowienie przyślij mi i niechaj wiem

a-na nu-ur-dšamaš ma-wi-il-dadad mdsin-pi-lah mcil-li-dadad u wakil 10 cabim qi-bi-ma um-ma dšamaš-na-cer-ma da-mi-iq e-pe-šum an-nu-um nukaribbi našpak suluppi ip-te-ne-tu-u-ma suluppi il-te-ne-qu-u u at-tu-nu a-wa-tim tu-uk-ta-na-ta-ma-ma a-na ce-ri-ja u-ul ta-ša-pa-ra-nim an-nu-um-ma tup-pi uš-ta-bi-la-ku-nu-ši iš-tu suluppi a-pa-li-im a-wi-le-e a-na ce-ri-ja šu-ri-a-nim … – do Nurszamasz Awiladad Sinpilah Cilliadad i strażnika dziesięciu ludzi mów w ten sposób (mówi) Szamasznacer jest dobre postępo-wanie owo ogrodnicy spichlerz daktyli coraz to otwierają i daktyle raz po raz zabierają i wy sprawę za każdym razem ukrywacie i do mnie nie piszecie teraz tabliczkę moją ka-załem przynieść wam kiedy za daktyle zapłacicie ludzi do mnie każcie sprowadzić

a-na be-el-šu-nu qi-bi-ma um-ma qur-di-ištar-ma dšamaš li-ba-al-li-it-ka lu ša-al-ma-a-ta lu ba-al-ta-a-ta … ga-me-er eqli-ka e-ci-id-ma še-am šu-li-a-am-ma i-na mu-uh-hi-šu šu-ta-ar-ri-ih i-nu-ma ta-la-ka-am qi-iš-ta-ka ta-ma-ar … – do Belszunu mów w ten sposób (mówi) Qurdiisztar Szamasz niechaj zachowa cię w zdrowiu bądź zdrowy bądź żywy … całe pole twoje jest sprzątnięte i zboże każ wnieść na górę i dlatego każ wciąż ponaglać kiedy przyjdziesz prezent twój zobaczysz (=otrzymasz)

iš-tu itusimanim aš-šum ce-eh-he-ru-ti-ja i-ta-ap-lu-si-im u-na-a’-‘i-id-ka um 4kam a-di i-na-an-na te-em ši-ip-ra-tim ma-la i-pu-šu u eqil šamaššammim ša i-pu-šu u-ul ta-aš-pu-ra-am … i-na-an-na na-bi-dsin a-na ma-ah-ri-ka at-tar-dam it-ti-šu a-na eqlim ri-id-ma eqil ši-ip-ra-tim ma-la i-pu-šu u eqil šamaššammim ša i-pu-šu i-ta-ap-la-as-ma i-na tup-pi-ka pa-nam šu-ur-ši-a-am-ma šu-up-ra-am… – od miesiąca siman (na to) że młodych ludzi moich doglądać stale (trzeba) zwracam uwagę ci cztery dni (już minęły) dotąd informacji (o) pracy ile wykonali i (o) polu sezamu które uprawiają nie przysłałeś … teraz Nabisina do ciebie wysłałem z nim na pole idź i pola pracy ile wykonali i pola sezamu które uprawiają doglądaj stale i na tabliczkach twoich przedstawiaj i przysyłaj (je)  
Lekcja dziewiętnasta

19.1. Czasowniki cztero(spół)głoskowe  
Obok rdzeni czasownikowych z trzema lub dwoma spółgłoskami występują w języku akadyjskim także rdzenie z czterema spółgłoskami, np. blkt „przekraczać” czy prqd „kłaść się na plecach”. Ponadto są trójspółgłoskowe rdzenie z długą samogłoską po trzeciej spółgłosce (ultimae vocalis), np. qlpu „płynąć z prądem” czy hlci „pośliznąć się”. Ponieważ obie grupy wspomnianych wyżej czasowników podobnie tworzą tematy i podobnie się odmieniają, są potraktowane łącznie jako jedna grupa czasowników czte-rogłoskowych i wspólnie omówione. Charakterystyczne jest to, że wszystkie one jako drugą spółgłoskę mają albo l albo r.

19.2. Tworzenie tematów przez czasowniki czterogłoskowe  
Czasowniki te tworzą tylko dwa tematy główne. Kiedy ich rdzeń poprzedzony jest przez n, tworzą one konstrukcje odpowiadające formalnie tematowi N czasowni-ków trójspółgłoskowych, ale nie mające znaczenia, które oddawać trzeba przez naszą stronę bierną. Przed rdzeń może też wchodzić š i wtedy powstaje temat odpowiadający za¬równo budową jak i znaczeniem kauzatywnemu tematowi Š czasowników trójspółgło-skowych. Od obu tematów głównych za pomocą wrostka –tan- mogą być tworzone po-chodne tematy częstotliwe.

19.3. Czasy z przedrostkami w temacie N czasowników czterogłoskowych  
Czasowniki te nie dzielą się na klasy w zależności od tego, jakie im towarzyszą samogłoski rdzeniowe. Wszystkie zawsze mają a w Prs i i w Prt. Poprzedzające rdzeń n po przedrostkach i-, ta-, a- lub ni- będzie asymilowane przez pierwszą spółgłoskę rdze-niową. W Prs podwajana jest trzecia spółgłoska.  
Prs: ibbalakkat (&lt;\*inbalakkat) – przekracza, przechodzi  
tabbalakkat – przekraczasz itd.  
Prt: ibbalkit – przechodził, l.mn. ibbalkutu – przechodzili  
Pf (z a jak Prs): ittabalkat.  
Czasowniki ultimae vocalis skracają samogłoskę w wygłosie (6.3):  
ipparakku – przestaje  
ipparku – przestawał  
ihhelecci – ślizga się.

19.4. Stativ i formy imienne tematu N czasowników czterogłoskowych  
Po trzeciej spółgłosce bezokolicznik i stativ mają samogłoskę u, zaś imiesłów, tworzony z przedrostkiem mu-, samogłoskę i:  
Bezokolicznik: nabalkutum – przekraczać  
St: naparqud, naparqudat – leży na plecach  
Imiesłów: mubbalkitum – przekraczający.  
W przypadku czasowników ultimae vocalis:  
Bezokolicznik: naparkûm – przestawać, nepelkûm – być rozciągniętym  
St: nepelku – jest rozciągnięty  
Imiesłów: muhhelcûm (&lt;\*munhelcûm) – ślizgający się, muhhelcitum – śliskość.

19.5. Temat Š czasowników czterogłoskowych (N-klasy)  
Przed poprzedzającym rdzeń š występują przedrostki u-, tu- i nu-:  
Prs: ušbalakkat – każe się buntować, podżega do buntu  
Prt: ušbalkit – kazał się buntować, podżegał do buntu.  
W przypadku czasowników ultimae vocalis:  
ušqeleppe – każe płynąć z prądem  
ušqelpe – kazał płynąć z prądem.

19.6. Š-grupa czasowników czterogłoskowych  
Za czterogłoskową uważana jest też inna grupa rdzeni czasownikowych, które łączy to, że ich pierwszą spółgłoską rdzeniową jest zawsze š. Należą do nich np. šuqal-lulum (šqll) „wisieć” i šukěnum „rzucać się (w dół)”.  
Prs od šuqallulum brzmi išqallal „wisi”; forma ta stosowana jest jako stativ (sta-tiv z przedrostkami – 11.9).  
Od šukěnum powstał imiesłów muškěnum „rzucający się (w dół)” stosowany jako nazwa grupy ludzi, którzy znajdowali się w bezpośredniej zależności od króla.

19.7. Druga osoba rodzaju żeńskiego w liczbie pojedynczej  
W Prs, Prt i Pf druga osoba rodzaju żeńskiego liczby pojedynczej, w odróżnieniu od drugiej osoby liczby pojedynczej rodzaju męskiego (5.4), tworzona jest z końcówką –i:  
tallik (r.m.) – chodziłeś, talliki (r.ż.) – chodziłaś.  
Końcówką ventivu w 2 osobie l.p. r.ż. jest –m, przy czym długa samogłoska –i ulega skróceniu (-im):  
tallikim – przychodzisz.  
Podobnie tworzona jest 2 osoba rodzaju żeńskiego trybu rozkazującego:  
idin (2 osoba r.m.), idni (2 osoba r.ż.) – daj  
z ventivem idnam, idnim – podaj  
šubilam, šubilim – każ przynieść.

19.8. Temat Rt  
Rzadkie są formy tematów R, Rt i Rtn, które są tworzone podobnie jak tematy D, Dt i Dtn, ale z reduplikacją środkowej spółgłoski. Najczęściej spotkać można się z tematem Rt:  
Prs Rt: uptararras (Prs Dt: uptarras – 15.2)  
Prt Rt: uptararris (Prt Dt: uptarris)  
TR Rt: putararris (TR Rt: putarris).  
Czasownik dananum “być mocnym” ma w Rt znaczenie „mierzyć siły ze sobą” i „okazywać się silniejszym”:  
Prs: uddanannan – okazuje się silniejszym  
tuddanannanu – mierzycie siły.

19.9. Kohortativ  
Kohortativ służy jako forma życzenia 1 osoby liczby mnogiej. Tworzony jest przy pomocy partykuły i, która poprzedza 1 osobę l.mn. Prt:  
i nillik – chodźmy, i nidbub – chcemy mówić.

19.10. Wokalizacja stativu G  
W 3 osobie l.p. stativu tematu podstawowego samogłoską przed trzecią spółgło-ską jest najczęściej i:  
cabit – jest złapany, damiq – jest dobry (1.5).  
W przypadku czasowników stanu (5.2) może taką samogłoską być także a lub u, np.  
rapaš – jest szeroki  
šabuc – jest zły  
qerub – jest bliski  
šamuh – jest wspaniały (por. imię dSîn-šamuh).  
Pozostałe formy stativu G tworzone są bez tej samogłoski:  
rapšat – jest szeroka.

19.11. Wokalizacja trybu rozkazującego G  
W trybie rozkazującym G pomiędzy pierwszą a drugą spółgłoskę rdzeniową wchodzi samogłoska, która prawie zawsze odpowiada samogłosce rdzeniowej (13.2):  
cabat – chwytaj (rdzeń \*cbat).  
Jednak w przypadku niektórych czasowników a-klasy samogłoską, o której mowa, jest i, np. w przypadku lamadum (lmad) „uczyć się, dowiadywać”:  
TR G: limad – dowiedz się  
r.ż.: limdi  
l.mn.: limda.

19.12. Czasowniki z przerwą głosową w środku i z samogłoską w wygłosie  
Czasowniki re’ûm (r’i) „paść” i le’ûm (l’i) „móc, być zdolnym” tworzą Prs ze spółgłoskową przerwą głosową (16.4):  
ire’’i – pasie  
ele’’i – jestem zdolny,  
ale Prt powstaje z wypadnięciem alefu (9.6):  
ire – pasł  
ile – był zdolny.  
Imiesłowem jest re’ûm (&lt;\*re’ium) – pasterz.  
Uwaga: od le’ûm „móc” odróżnić trzeba la’ûm (l’u) „być brudnym”, który to czasownik tworzy:  
St D: lu’’u (r.m.) i lu’’ât (r.ż.) – brudzi.

Ćwiczenia  
šumma uban ha-ši qablitum i-na ma-aš-ka-ni-ša na-ba-al-ku-ta-at – jeśli „palec” płucny środkowy w miejscu swoim jest obrócony  
šum-ma ina šu-me-el martim pi-it-rum ne-pe-el-ku – jeśli z lewej strony woreczka żół-ciowego szpara jest szeroka  
šum-ma cibtum it-ta-ba-al-ka-at – jeśli narośl obróciła się  
šumma i-na bab ekallim zihu lu-u’-‘u-ma u iš-qa-la-al – jeśli w „bramie pałacu” pęche-rzyk jest brudny i zwisa  
il-šu e-li a-wi-li-im ša-bu-us – bóg jego na człowieka będzie zły  
ma-tum ša re-e-i-ša ib-ba-al-ki-tu re-e-i-ša e-li-ša iz-za-az – kraj który pasterzowi swo-jemu opierał się pasterz jego nad nim zatriumfuje  
i-na mu-uh-he-el-ci-tim šep awilim i-he-le-ci – na czymś śliskim noga człowieka pośliź-nie się  
a-lum ib-ba-la-ka-at-ma be-el-šu i-da-ak – miasto zbuntuje się i pana swojego zabije  
naker-ka ud-da-na-an-na-na-ak-kum – wróg twój okaże się silniejszy od ciebie  
šumma ka-as-ka-su i-mi-tam u šu-me-lam ka-pi-ic at-ta u naker-ka tu-ud-da-na-an-na-na – jeśli mostek z prawej i lewej strony jest złamany ty i twój wróg będziecie mierzyć ze sobą siły  
šumma ka-as-ka-su i-mi-tam ka-pi-ic a-na nakri-ka tu-da-na-an-na-an – jeśli mostek z prawej strony jest złamany wrogowi twojemu będziesz okazywał przewagę  
šumma kišad hašim na-ba-al-ku-ut te-e-em ma-a-tim iš-ta-na-an-ni – jeśli „szyja” płuc jest obrócona dowództwo kraju wielokrotnie się zmieni  
šumma kakki i-mi-tim i-na re-eš mar-tim ša-ki-in-ma ib-ba-al-ki-it-ma ma-ac-ra-ah martim it-tu-ul šar-rum ma-li-ki u-ul i-ra-aš-ši – jeśli „broń prawicy” na wierzchołku woreczka żółciowego jest położona i obraca się i ku przewodowi żółciowemu jest skie-rowana król doradców nie będzie miał  
šumma hašum na-pa-ar-qu-da-at ma-as-su ib-ba-la-ka-su – jeśli płuca są położone na plecach kraj jego zbuntuje się przeciw niemu  
šumma bab ekallim ne-pe-el-ku-u hu-ša-ah-hu-um ib-ba-aš-ši-i – jeśli „brama pałacu” jest szeroka głód nastanie  
šumma i-na bab ekallim qu-u-um ra-pa-aš mi-li i-ir-tim – jeśli w „bramie pałacu” węzeł jest szeroki sukces  
Starobabilońskie listy

mtam-la-tum tamkarum mar qi-iš-dnu-nu eleppam ša ib-ba-tum malahim i-gu-ur-ma a-na babili uš-qe-el-pi … mib-ba-tum šu-a-ti a-na ma-ah-ri-ka at-tar-dam ki-ma ra-bu-ti-ka eleppam šu-a-ti pu-ut-te-er pi-qi-is-su-um-ma a-na sipparim li-ša-aq-qi-a-aš-ši – Tamlatum kupiec syn Qisznunu statek Ibbatum marynarza wynajął i do Babilonu po-prowadził z prądem … Ibbatum owego przed ciebie posłałem korzystnie statek ten wy-kup powierz mu i do Sippar niech poprowadzi go (statek) w górę (pod prąd)

a-na um-mi-ja qi-bi-ma um-ma a-wi-il… ma-ru-ki-ma dšamaš u d… li-ba-al-li-tu-ki … il-ku-um is-ra-an-ni-ma na-pa-ar-ka-am u-ul e-li-i u at-ti ma-ti-ma ki-ma um-ma-tim u-ul ta-aš-pu-ri-im-ma li-ib-bi u-ul tu-ba-li-ti a-nu-um-ma ma-an-na-ši aš-ta-ap-ra-ki-im 2 qa šamnim šu-bi-lim mu-ur-cu-um ic-ba-ta-ni-ma i-na na-pi-iš-tim an-na-di – do matki mojej mów w ten sposób (mówi) Awil… syn twój Szamasz i … niechaj zachowają cię w zdrowiu … obowiązek służebny (lenny) przykuwa mnie i skończyć nie mogę i ty nigdy jak matka nie napisałaś do mnie i serca mojego nie utrzymywałaś przy życiu teraz Mannaszi posłałem do ciebie 2 qa oleju każ przysłać mi choroba opadła mnie i bliski jestem śmierci

a-na ma-ti-be-lu-um qi-bi-ma um-ma ni-id-na-at-dsin-ma dšamaš li-ba-al-li-it-ka a-nu-um-ma un-ne-du-uk-ki uš-ta-bi-la-ak-kum un-ne-du-uk-ki i-na a-ma-ri-i-ka 1 kur er-bi-i 300 ka-ma-ri … i-na e-le-ep-pi-im ce-na-am-ma u at-ta al-ka-am ci-bi-a-tu-u-ka lu-u ma-da al-ka-am-ma te-e-mi li-ma-a-ad ap-pu-tum – do Matibelum mów w ten sposób (mówi) Nidnatsin Szamasz niechaj zachowa cię w zdrowiu teraz list mój kazałem przy-nieść ci kiedy list mój przeczytasz 1 kur szarańczy 300 ryb „kamaru” … na statek zała-duj mi i ty przyjdź do mnie twoje interesy zapewne są liczne (ale mimo to) przyjdź do mnie i o rozkazach dowiedz się (to jest) pilne

a-na ib-ni-dsin mar dmarduk-na-ce-er qi-bi-ma um-ma am-mi-ca-du-qa-ma bu-qu-mu i-na bit a’-ki-tim iš-ša-ka-an ki-ma tup-pi an-ni-a-am ta-am-ma-ru pa-ni ceni ša te-re-‘u-u ca-ab-tam u ka-ni-ka-a-at ceni ša a-na ci-i-tim tu-še-cu-u le-qe-a-am-ma a-na babili al-kam la tu-la-ap-pa-tam i-na ituaddarim um 1kam a-na babili si-in-qa-am – do Ibnisina syna Marduknacera mów w ten sposób (mówi) Ammicaduqa strzyżenie w domu święta nowego roku jest robione skoro tylko tabliczkę moją tę przeczytasz pierwsze (?) z owiec które pasłeś złapane (?) i dokument na owce które oddałeś zabierz i do Babilonu przyjdź nie zwlekaj w miesiąca addaru pierwszym dniu do Babilonu przybądź

a-na ipiq-ištar qi-bi-ma um-ma den-lil-lu-šaga-ma den-lil li-ba-al-li-it-ka ki-ma ti-du-u e-bu-ru-um qe-ru-ub la tu-uš-ta-a it-ti na-hi-iš-ša-al-mu-um al-kam-ma te-em bi-ti i ni-id-bu-ub – do Ipiqisztar mów w ten sposób (mówi) Enliluszaga Enlil niechaj zachowa w zdrowiu ciebie jak wiesz żniwa są blisko nie bądź bezczynny z Nahiszszalmum przyjdź i rozkazy domu niechże wypowiemy  
Lekcja dwudziesta

20.1. Język poezji starobabilońskiej  
Język starobabilońskich hymnów i eposów (0.9) różni się od współczesnego mu języka potocznego znanego z listów, dokumentów i literatury prozatorskiej (np. wróżb). Objawia się to przez mniej czy bardziej częste stosowanie starszych form i wyrażeń, które w tekstach prozatorskich nie występują wcale lub pojawiają się rzadko. Do waż-niejszych cech szczególnych języka poezji należą trwające nadal użycie dwóch, mocno poza nim zredukowanych przypadków: miejscownika przysłówkowego (20.2) oraz ter-minatywu przysłówkowego (20.3), a także posługiwanie się odmiennymi formami sta-tus constructus imion (20.4). Rytmiczna struktura poezji ujętej w wersy z głoskami ak-centowanymi i z zaniżaniem głosu pociąga za sobą konieczność wolnego szyku wyra¬zów oraz niekiedy zaniku samogłoski w wygłosie przed przyrostkami zaimkowymi (20.6), a także sztucznych wzdłużeń (20.19).

20.2. Miejscownik przysłówkowy (lokativ adverbialis)  
Bardzo często w języku poezji występują formy pierwotnego przypadku, miej-scownika z końcówką –u(m), który w prozie obecny jest jedynie u przysłówków (9.9) takich jak šaplanum „na dole”; w poezji pojawia się on przede wszystkim w połączeniu z przyrostkiem zaimkowym, przy czym –m mimacji jest asymilowane przez przyrostek:  
rešušša (&lt;rešum + ša) – na jej głowie  
siqrušša – przez jej rozkaz.  
Ten sam przypadek w status constructus przed dopełniaczem ma końcówkę –u:  
qabaltu ceri – na środku stepu.

20.3. Terminativ adverbialis  
Dawny terminativ z końcówką –iš występuje w prozie tylko u przysłówków, gdyż końcówka –iš służy do przekształcania przymiotników w przysłówki:  
išariš – w sprawiedliwy sposób (od przymiotnika išarum „sprawiedliwy”)  
mihariš – po równo (od mitharum „odpowiadający sobie wzajemnie”).  
W poezji takie końcówki przysłówkowe są szczególnie częste:  
danniš – bardzo, silnie  
šarhiš – wspaniale  
ištiniš – razem, wspólnie (od ištinum „jeden”).  
Tylko w poezji używany jest terminativ jako przypadek kierunkowy imion – albo z zależnym dopełniaczem:  
ipšiš pišunu – dla otwarcia ich ust = dla ich wypowiedzi  
albo z przyrostkiem zaimkowym:  
qatišša – w jej rękę  
simtišša – dla jej postaci  
muttiššunu – przed nimi (od muttum „strona frontowa”).  
Także przysłówek pytajny ěš (&lt;ajjiš) „dokąd?” ma końcówkę terminatywu –iš.

20.4. Status constructus z końcówką –u  
W poezji często zamiast status constructus bez końcówki (2.8), zapewne ze względu na rytmikę tworzony jest status constructus z końcówką –u:  
cabitu qatika (zamiast cabit qatika) – ten, który złapał twoją rękę.  
Status constructus od kalum „całość” ma w prozie postać kal lub kala, natomiast w po-ezji mamy często do czynienia z kalu:  
kalu marcatim – wszystkie starania.  
Status constructus form w liczbie mnogiej kończących się na –atum czy –atim (3.5) w poezji kończy się najczęściej na –atu:  
ana šimatu awilutim (zamiast ana šimat…) – do losu ludzkości.

20.5. Samogłoska wiążąca a w imionach z przyrostkiem  
Przyrostek będący zaimkiem dzierżawczym w mianowniku lub bierniku dołącza się do imienia o formie status constructus (3.8). W języku poezji w skład takiego tworu włączana jest niekiedy samogłoska wiążąca a:  
migrašunu (zamiast migeršunu) – ich ulubieniec.  
Por. też ištaša „z nią” (20.10).

20.6. Przyrostek zaimkowy z zanikiem samogłoski w wygłosie  
Wypadnięcie samogłoski w przyrostkach zaimkowych występuje w prozie tylko rzadko (16.8), ale w niektórych tekstach poetyckich jest bardzo częste. Przede wszyst-kim dotyczy to zaimków dzierżawczych –ša i –šunu (3.7), które skracane są do –š i –šun:  
nazzazuš (&lt;nazzazu-ša) – jej pozycja  
libbišun – ich serca  
mahrišun – przed nich.  
Przyrostek w bierniku –šunuti (13.10) może być skrócony do –šunut:  
ittanaqqišunut – ofiarowywał im stale (przyrostek w bierniku jest tu użyty jak by był w celowniku).

20.7. Zaimek określający šat  
Zaimek wskazujący šu „dany, ten” ma rodzaj żeński ši „dana” (11.10). Zamiast ši może być w języku poezji użyty także zaimek określający rodzaju żeńskiego šat „dana, ta”:  
šat melesim – ta, która promieniuje radością.

20.8. Celownik zaimków osobowych  
Formy w celowniku od anaku, atta, atti, šu itd. (15.7) mają postać następującą:  
jâšim – mi  
kâšim (r.m. i r.ż.) – tobie, ci  
šuašim (šâšim, w poezji także šâšum) – jemu  
šiašim – jej  
niašim – nam.  
Formy te, które używane są zawsze z przyimkiem ana, służą często do szczególnego podkreślenia:  
sabitum ana šâšum izzakaram ana dgiš – szynkarka do niego mówi, do Gilgame-sza.

20.9. Przyimki ana „do” i ina „w, na”  
Skróconymi formami tych przyimków są an i in; w poezji często są one ścią-gnięte z następującymi po nich imionami, przy czym powstające w wyniku asymilacji podwojone spółgłoski nie zawsze są zapisywane:  
a-ni-ri-i-ši-u (annirišiu &gt; ana nurišu) – pod jego jarzmem  
i-ge-e-gu-un-ni-im (iggegunnim &gt; ina gegunnim) – w wysokiej świątyni  
i-ni-li (inili &gt; ina ili) – między bogami.

20.10. Przyimek išti  
Przyimek išti (także itti) „z” może być połączony z przyrostkiem:  
ištišu – z nim  
ittija – ze mną.  
W poezji przed przyrostkiem może występować samogłoska wiążąca a (20.5):  
ištaša – z nią.  
Uwaga: z przyimkiem išti (itti) „z (kimś)” nie należy mylić przyimka ištu „z (skądś)”, którego nie można łączyć z przyrostkami zaimkowymi  
20.11. Przedrostek 3 osoby rodzaju żeńskiego  
W prozie formy 3 osoby liczby pojedynczej są jednakowe dla obu rodzajów:  
icbat – on/ona łapie.  
Natomiast w poezji jest też używana forma z przedrostkiem ta- (te-, tu-) dla 3 osoby rodzaju żeńskiego liczby pojedynczej:  
tattadin – ona dała  
teteriš – ona żądała  
tušaknišam – ona ujarzmiła.

20.12. Stativ od bašûm i izuzzum  
Niektóre czasowniki, które tworzą stativ z przedrostkami (11.9), w języku poezji tworzą także normalne formy stativu z takim samym znaczeniem:  
baši – istnieje (w miejsce ibašši)  
nazuz (l.mn. nazuzzu) – jest postawiony, stoi (zamiast izzaz, izzazzu).

20.13. Czasownik nâdum  
Czasowniki na’adum (n’d) „pilnować” (16.4) i nâdum „chwalić” są nie do odróż-nienia; ten ostatni w temacie podstawowym zachowuje się tak jak dâcum (9.6), jed-nakże temat D tworzy ze spółgłoskową przerwą głosową, np. prekativ Dt brzmi w jego przypadku następująco:  
litta’’id – był chwalony.

20.14. Vetitiv  
Partykuła zakazu, odmowy aj „nie” łączy się z 1 osobą liczby pojedynczej Prt:  
ajamur (&lt;aj + amur) – nie chcę widzieć.

20.15. Możliwość  
Do wyrażenia możliwości służy enklityczna partykuła –man:  
ibri-man itabbeam – mój towarzysz mógł przecież zbuntować się.

20.16. Zaprzeczenie la  
W języku poezji la (5.10) może być też użyte zamiast ul do zaprzeczenia zdania głównego:  
balatam … la tutta – życia … nie będziesz znajdował.

20.17. Stopniowanie  
Aby wyrazić stopień wyższy przymiotnika używa się przyimka eli „poza (coś, czymś)”:  
elšunu haptat – dosł. poza tamtych jest mocna = jest mocniejsza niż tamci.  
Stopień najwyższy może być oddany przy pomocy status constructus:  
rabit igigi – wielki Igigu = największy pośród Igigu.

20.18. Stativ imion  
Status absolutus imion odpowiada formalnie stativowi czasowników (8.9). Każde imię w status absolutus może być rozumiane i odmieniane tak jak czasownik w stativie:  
bel – jest panem  
beleta – jesteś panem  
itd. (14.11).  
Dotyczy to także przymiotników:  
šarhat – jest wspaniała  
inaša bitrama – jej oczy są kolorowe.

20.19. Samogłoski łamane  
Pisownia typu i-bi-us-se-e czasownika ibissě może służyć do wyrażenia jakiegoś „zabarwienia” samogłoskowego, w tym przypadku do oddania brzmienia samogłoski będącej czymś pośrednim pomiędzy i oraz u (czymś w rodzaju niemieckiego ü). W ję-zyku poezji samogłoski łamane mogą też oddawać dysymilację uwarunkowaną rytmiką, np. –ši-u (dla -šu) lub na-zu-iz-zu-u (dla nazuzzu).

Ćwiczenia

Fragment starobabilońskiej wersji eposu o Gilgameszu (M II 2 – III 13)

II.  
2\. en-ki-dug ša a-ra-am-mu-ma da-an-ni-iš  
Enkidu, którego kochałem tak bardzo

3\. it-ti-ja it-ta-al-la-ku ka-lu mar-ca-a-tim  
(który) ze mną przechodził wszelkie trudy

4\. il-li-ik-ma a-na ši-ma-tu a-wi-lu-tim  
odszedł właśnie ku przeznaczeniu ludzkości  
5\. ur-ri u mu-ši e-li-šu ab-ki  
dzień i noc nad nim płakałem

6\. u-ul ad-di-iš-šu a-na qe-be-ri-im  
nie dałem go, by (go) pogrzebano

7\. ib-ri-ma-an i-ta-ab-be-a-am a-na ri-ig-mi-ja  
towarzysz mój może podniesie się na biadanie moje

8\. se-be-et u-mi-im u se-be mu-ši-a-tim  
siedem dni i siedem nocy

9\. a-di tu-ul-tum im-qu-tam i-na ap-pi-šu  
dopóki robak nie padł na twarz jego

10\. iš-tu wa-ar-ki-šu u-ul u-ta ba-la-tam  
odkąd jest martwy nie znalazłem życia

11\. at-ta-na-ag-gi-iš ki-ma ha-bi-lim qa-ba-al-tu ce-ri  
błąkam się bezustannie jak złoczyńca pośrodku stepu

12\. i-na-an-na sa-bi-tum a-ta-mar pa-ni-ki  
teraz, szynkarko, ujrzałem twarz twoją

13\. mu-tam ša a-ta-na-ad-da-ru a-ja-a-mu-ur  
śmierci, której stale się boję, nie chcę oglądać

14\. sa-bi-tum a-na ša-a-šum iz-za-kar-am a-na dgiš  
szynkarka do niego mówi do Gilgamesza

III  
1\. dgiš e-eš ta-da-a-al  
Gilgameszu, dokąd biegniesz?

2\. ba-la-tam ša ta-sa-ah-ha-ru la tu-ut-ta  
życia, którego poszukujesz, nie znajdziesz

3\. i-nu-ma ilu ib-nu-u a-wi-lu-tam  
kiedy bogowie stworzyli ludzkość

4\. mu-tam iš-ku-nu a-na a-wi-lu-tim  
śmierć położyli na ludzkość

5\. ba-la-tam i-na qa-ti-šu-nu ic-ca-ab-tu  
życie w rękach swych trzymają

6\. at-ta dgiš lu ma-li ka-ra-aš-ka  
ty! Gilgameszu! niech pełny jest brzuch twój

7\. ur-ri u mu-ši hi-ta-ad-du at-ta  
dniem i nocą ciesz się ty

8\. u-mi-ša-am šu-ku-un hi-du-tam  
codziennie odprawiaj święto radości

9\. ur-ri u mu-ši su-ur u me-le-el  
dniem i nocą tańcz i skacz

10\. lu ub-bu-bu cu-ba-tu-ka  
niechaj będą wyczyszczone szaty twoje

11\. qa-qa-ad-ka lu me-se me-e ra-am-ka-ta  
głowa twoja niechaj będzie umyta wodą, bądź wykąpany

12\. cu-ub-bi ce-eh-ra-am ca-bi-tu qa-ti-ka  
spójrz na dziecko trzymające rękę twoją

13\. mar-hi-tum li-ih-ta-ad-da-a-am i-na su-ni-ka  
kobieta niechaj cieszy się na łonie twoim  
Lekcja dwudziesta pierwsza

21.1. Język średniobabiloński  
Język średniobabilońskich listów (0.7) różni się od starobabilońskiego głównie zanikiem mimacji (21.2), zmianami brzmienia pewnych grup spółgłosek (21.4 – 21.5), pewnymi szczególnymi tworami zaimkowymi (21.7 – 21.9), a pod względem syntak-tycznym zmianą w zasadach użycia perfektu (21.14) oraz tworzeniem zdań czasowych z ki (21.15).

21.2. Mimacja  
-m w wygłosie końcówek przypadków (1.7) , ventivu (5.7), zaimków osobowych w celowniku (5.9) i przyrostków zaimkowych znikało niekiedy już w czasach staroba-bilońskich. W średniobabilońskim ten zanik stał się normą i mimacja zachowała się jeszcze tylko tam, gdzie jest wsparta obecnością przyrostka lub –ma:  
lilqunikkumma (&lt;lilquni(m) + ku(m) + ma) – niech zabierze dla ciebie i …

21.3. Odpadnięcie i przeobrażenie półsamogłoski w  
w na początku słowa znika:  
ardu (&lt;wardum) – sługa, niewolnik  
ašabu (&lt;wašabum) – osiąść  
ašibu (&lt;wašibum) – siedzący.  
Między samogłoskami w przechodzi w m:  
amilu (&lt;awilum) – człowiek  
umaššer (&lt;uwaššer) – zniknął.  
Podobnie jak Prs: umaššar i Prt: umaššer (w starobabilońskim z w w nagłosie) także inne formy dawnego czasownika wuššurum tworzone są w średniobabilońskim z m: bezokolicznik nie brzmi \*uššuru, lecz muššuru „znikać” (tryb rozkazujący muššer „znikaj”).

21.4. Spółgłoski wargowe i zębowe  
Przed spółgłoskami zębowymi m przechodzi w n:  
hantiš (&lt;hamtiš) – pospiesznie  
Złożenie głosek mt przechodzi w nd:  
undeššer (&lt;\*umtaššer, starobab. utaššer) – zniknął.

21.5. š przed spółgłoskami zębowymi i szczelinowymi  
Przed spółgłoskami zębowymi (d, t, t), jak i przed z, s, c głoska š przechodzi czę-sto w l, przy czym jest obojętne, czy š jest spółgłoską rdzeniową czy elementem two-rzącym (š w temacie Š):  
kulda (&lt;kušdam) – przybądź  
ulziz (ušziz – 21.13) – kazał stać  
altaprakku (&lt;aštaprakkum) – posłałem ci  
ultu (zamiast starobab. ištu) – od.

21.6. Przejście od a do e  
a w głosce zamkniętej może w średniobabilońskim przejść w e, jeśli następna sylaba zawiera i lub e:  
undeššer (&lt;\*umtaššer) – zniknął  
ušeddi (&lt;\*ušandi) – zaniedbałem.

21.7. Zaimki osobowe  
Mianownik zaimków osobowych (15.7) i wskazujących (11.10) odpowiada staro-babilońskim anaku, atta, šu itd. Celownikiem (18.2) zaimka 1 osoby jest (ana) jâši (starobab. jâšim) „mi”, zaś zaimka 2 osoby (ana) kâša (starobab. kâšim) „tobie”. Do-pełniacz/biernik zaimków wskazujących, używanych także przydawkowo, jest šâtu (sta-robab. šuati):  
amila šâtu – tego człowieka.  
W liczbie mnogiej celownik šâšunu (starobab. šunuti) używany jest także jako biernik:  
amili šâšunu – tych ludzi.

21.8. Zaimki dzierżawcze  
Zamiast samodzielnych zaakcentowanych zaimków dzierżawczych jûm itd. (6.8) w dialekcie średniobabilońskim używane jest attu- z odpowiednim przyrostkiem dzier-żawczym:  
(šibšu) attušu – jego (wydzierżawione pole)  
harrana attu’a – moja karawana (21.9).  
21.9. Przyrostki dzierżawcze  
Przyrostki dzierżawcze odpowiadają starobabilońskim z tym wyjątkiem, że przy-ro¬stek 1 osoby liczby pojedynczej „mój” w przypadku imion ahu i abu ma postać –ija (a nie –i – 3.9):  
ahija (starobab. ahi) – mój brat  
abija – mój ojciec  
ale: beli – mój pan.  
-ja w formach liczby mnogiej zakończonych na –u i w złączeniu z –attu (21.8) przechodzi, tak jak w starobabilońskim (13.9) w –‘a:  
tamkaru’a – moi kupcy  
harrana attu’a – moja karawana.

21.10. Zaimki wskazujące i pytające  
Forma rodzaju żeńskiego od annû „ten” czyli annitu używana jest często rze-czow¬nikowo:  
annita niqtabi – powiedzieliśmy to.  
Rzeczowym zaimkiem pytajnym jest minû „co?” z biernikiem minâ „odnośnie czego?”.

21.11. Odmiana stativu w liczbie mnogiej  
Stativ ma w liczbie pojedynczej końcówki –aku, -at, -ati (14.11). Końcówkami stativu w liczbie mnogiej są staro- i średniobabilońskie –anu dla 1 osoby oraz –atunu, -atina dla 2 osoby:  
tabanu – są dobrzy  
tabatina – jesteście dobre.  
W przypadku czasowników z e-samogłoskowością końcówki tworzone są z –e- a nie z –a-:  
cehrenu – są małe.

21.12. Czasowniki z przerwą głosową w środku  
Czasowniki z przerwą głosową w środku (9.6) występują w średniobabilońskim częstokrotnie z alefem (16.4), np.  
ša’alum (obok šâlum) – pytać  
Prs: iša’’al (išal) – pyta  
Prt: iš’al (išal) – pytał  
TR: ša’al (šal) – pytaj!  
Temat Gtn zawsze jest tworzony z alefem:  
Prs: ištana’’al – pyta bez przerwy  
TR: šita’’al – pytaj wciąż na nowo.  
21.13. Temat Š od izuzzum  
Nieregularny czasownik izuzzum „stać” (11.8) tworzy w starobabilońskim takie formy jak:  
Prs Š: ušzaz (l.mn. ušazzu) – każe stać  
Prt Š: ušziz (ušzizzu)  
TR: šuziz.  
W średniobabilońskim mamy formy:  
Prs: ulzaz  
Prt: ulziz (21.5).

21.14. Użycie czasów  
Perfekt pełni w języku średniobabilońskim inną funkcję niż w starobabilońskim (15.14), a mianowicie jest ogólnym czasem przeszłym w zdaniach relacjonujących i twierdzących:  
ana šimati ittaklu – zawierzyli przeznaczeniu  
eqlati undeššer – opuścił pola.  
Präteritum używany jest natomiast głównie w zaprzeczonych zdaniach relacjo-nują¬cych i w zdaniach pytajnych:  
anaku ul umaššer – nie zrzekłem się (pola)  
minâ hita ahti ana belija – w jaki sposób zawiniłem mojemu panu?  
Także w zdaniach zależnych używany jest Prt:  
amilu ša ardija iduku – ludzie, którzy zabili mojego sługę.

21.15. Zdania czasowe  
Szczególnie charakterystyczny dla średniobabilońskiego jest subiunkcjon ki „skoro tylko, kiedy” (starobab. kima – 13.14). Stoi on zawsze bezpośrednio przed cza-sownikiem w zdaniu czasowym:  
ki z Prt – „gdy, kiedy”  
šepišu ki unakkisu itušu iktalašu – kiedy nogę jego odrąbał, zatrzymał go przy sobie  
mhuzalum ki elqâ itamarši – kiedy przyprowadziłem Huzalum, zbadałem ją  
ki z Pf – „skoro tylko”  
ana panika ki altaprakku šita’’alšu – skoro tylko napisałem tobie, pytaj go nie-ustannie.  
Kilka zdań z ki nie połączonych ze sobą może występować obok siebie:  
tem murcika ki iš’aluši riksa ki esihu urakkašuši – kiedy po wieści o twojej cho-robie zapytał ją, kiedy przydzieliłem grupę, połączono ją??  
Jako subiunkcjon czasowy używane jest też ultu (starobab. ištu) „od, kiedy”:  
ultu ahu-tabu ana muhhi ahija itiqu – kiedy Ahutabu do mojego brata był prze-niesiony.

21.16. Średniobabiloński formularz listu  
Adresat może być jak w starobabilońskim wskazany przez formułkę ana NN qibima „do NN mów”, nadawcę zaś wskazuje zwrot umma NN-ma „w ten sposób (mó-wi) NN” (13.16). Często potem imię adresata występuje jeszcze raz w formie:  
umma (um-ma-a) ana NN-ma – w ten sposób do NN (mówię).  
umma stoi też przed cytatem, podobnie jak np. akanna „tak”.  
Jako zwrot grzecznościowy często spotykamy formułę:  
ana dinan belija lullik – ku zastępstwu mego pana chcę kroczyć = mojemu panu chcę się ofiarować.  
Lekcja dwudziesta druga

22.1. Język nowobabiloński  
Język listów nowobabilońskich (0.7) obrazuje dalszy etap rozwoju języka średnio-babilońskiego (21.1). Toteż w nowobabilońskim odnaleźć można prawie wszystkie cechy szczególne języka średniobabilońskiego. Dochodzą do nich dalsze przeobrażenia w grupach spółgłosek (22.3), stopniowe zaniechanie rozróżniania przy¬padków w od-mianie (22.9), niektóre nowe twory zaimkowe (22.4 – 22.6), a także liczne nowe wyra-żenia przyimkowe oraz przysłówkowe (22.14 – 22.15). Początek wpływów języka ara-mejskiego widać nie tylko w zaniechaniu rozróżniania przypadków, lecz zwłaszcza w przyjęciu przyimka la (22.13) oraz w okazyjnych zmianach szyku wyra¬zów (22.17).

22.2. Język późnobabiloński  
Jeszcze większy jest wpływ aramejskiego na język listów późnobabilońskich (0.7). Końcówki przypadków odpadają w nim całkowicie. Jako zaimek 1 osoby liczby mnogiej używany jest w miejsce ninu wyraz aneni „my” (22.4).

22.3. Przeobrażenia głosek  
U podwójnych spółgłosek następuje często wtórna nosalizacja (10.3):  
dd &gt; nd (zapisywane często jako md), np. inamdin – daje  
zz &gt; nz, np. manzaz panija – mój pełnomocnik  
cs &gt; nc (zapisywane często jako mc), np. ninamcar – pilnujemy.  
Przed š głoska m przechodzi w n:  
attalkanšu (&lt;attalkam + šu) – przyszedłem do niego,  
mt zmienia się w nd:  
indalu (&lt;imtalu) – stali się pełnymi.  
Asymilacja d z n ma miejsce tylko w niektórych formach czasownika nadanu „dawać”:  
inna (&lt;idna) – daj mi  
ittanna (&lt;ittadna) – dał.

22.4. Cechy szczególne zaimków osobowych i przyrostków zaimkowych  
Z ana jâši (21.7) powstało w nowobabilońskim ajjâši „mi”. Obok ninu „my” pod wpływem aramejskiego pojawia się anahna, a także aneni.  
Przyrostkiem 1 osoby liczby pojedynczej (3.9) jest –a (w miejsce –i):  
puluhta (zamiast puluhti) – moja cześć = cześć dla mnie  
ahu’a (zamiast ahi) – mój brat  
abu’a (zamiast abi) – mój ojciec.  
Przyrostek –ja nie zmienił się:  
belija – moich panów.  
Przyrostkiem zaimkowym w celowniku 2 osoby liczby pojedynczej rodzaju mę-skiego (5.9) jest –ka:  
aqbakka (zamiast aqbakkum) – wydałem rozkaz tobie.

22.5. Zaimki wskazujące  
Zamiast annûm „ten” w nowobabilońskim używane jest najczęściej agû (r.ż. agatu) „ten” („ta”); to ostatnie słowo może też być użyte przysłówkowo: libbu aga’i „odpowiednio jak”, „w taki sposób”. „Ów” to ullû, ullitu.

22.6. Zaimki uogólniające  
Osobowy zaimek pytajny mannu „kto” (rzeczowo minu „co”) może być w nowo- i późnobabilońskim używany jako uogólniający zaimek względny „ów”, „ten”:  
mannu … ušuzzu – ten (ów) … depta.

22.7. Nieokreślony zaimek mamma  
„Jakikolwiek” to nieodmienne mamma:  
ina libbi ša mamma la taklaka – nikomu nie ufam (22.10).

22.8. Zaimek ilościowy gabbu  
Zamiast kalum „całość” używany jest w nowobabilońskim przymiotnik gabbu „każdy”, „wszelki”:  
matati gabbi – wszystkie kraje,  
jest on też używany rzeczownikowo:  
gabbi itamru – wszystko widzieli.

22.9. Końcówki przypadków  
Już w tekstach nowobabilońskich przypadki nie były zbyt dokładnie odróżniane. Końcówka biernika –a zastąpiona została przez –u:  
nidittu … inamdin – daninę daje.  
Niekiedy już w nowobabilońskim, zaś częściej w późnobabilońskim końcówki przypadków całkiem odpadają:  
ina muhhi micer ullû (dla micri ullî) – co do tego okręgu  
lu šulum (dla šulmu) ana abini – niech szczęście naszemu ojcu (sprzyja)  
utur (dla utra) ikkal – spożywa nadwyżkę.  
Pod wpływem asyryjskiego końcówką dopełniacza jest często –e, a nie –i:  
ina umešu – w jego dniu = późno.

22.10. Końcówki samogłoskowe form czasownikowych  
W przypadku czasowników ultimae vocalis w nowobabilońskim końcówki samo-głoskowe –i oraz –u nie są zbyt dokładnie rozróżniane, stąd mamy:  
nuccu (dla nucci) – wyszliśmy.  
W pierwszej osobie liczby pojedynczej stativu końcówka –aku (14.11) przecho¬dzi w –aka albo skraca się do –ak:  
taklaka – powierzam  
marcak – jestem chory.  
Długie samogłoski jako wygłos form czasownikowych zapisywane są niekiedy z przerwą głosową:  
hi-ra-a’ (dla hira) – kop.  
Innymi graficznymi osobliwościami nowobabilońskiego są takie zapisy jak:  
ic-bat-u (dla icbatu) – łapią  
ta-ac-bat-a dla tacbata  
tu-ša-id-an-ni (dla tuša’’idanni) – zawiadomiłeś mnie.

22.11. Czasownik izuzzu/ušuzzu  
Prt czasownika izuzzu „stać” (11.8) także w nowobabilońskim brzmi izziz, jed-nakże pozostałe formy często tworzone są od obocznej postaci tego słowa ušuzzu:  
Pf: ittašiz – stał  
St (20.12): ušuz (l.mn. ušuzzu) – jest postawiony.

22.12. Kohortativ  
W nowobabilońskim jako kohortativ (19.9) służy 1 osoba liczby mnogiej Prt (bez partykuły i):  
nilti – chcemy pić  
la nilli – nie chcemy podnosić do góry;  
„piliśmy” to w średnio- i nowobabilońskim niltati (21.14).

22.13. Przyimki  
Zamiast ana „do” i ištu „z” pojawia się często w nowobabilońskim aramejski przyimek la:  
la qati šarri la nilli – z rąk króla nie chcemy wyjść na górę = nie chcemy przez króla zginąć?  
Nie należy mylić przyimka la z zaprzeczeniem la, które występuje w połączeniu ša la „bez”:  
ša la pija – co nie jest moich ust = bez mojego rozkazu.  
Przyimek ana służy niekiedy do oznaczania biernika:  
ana šarri belija likrubu – powinni króla, mojego pana błogosławić.

22.14. Wyrażenia przyimkowe  
Nowe wyrażenia przyimkowe powstają przez połączenie przyimka z imieniem. W tym związku pierwotne znaczenia imienia ulega najczęściej daleko idącemu zani-kowi:  
muhhu – nakrycie głowy  
ana muhhi – celem, w celu, ze względu na, dla  
ina muhhi – co do, odnośnie do  
panu – przednia strona, przód  
ina pani – przed, u  
ana pani = lapani (22.13) przed, naprzeciwko  
panatu – strona frontowa  
ana panat – ze względu na, wobec, dla  
libbu – serce  
ina libbi – w; między, pomiędzy nimi  
tarcu – inna strona  
ana tarci – przeciwko  
Wyrażenia takie łączone być mogą z przyrostkiem:  
ana tarcišu – przeciw niemu.

22.15. Przysłówki  
Do oznaczania czasu służą:  
enna (starobab. inanna) – teraz, obecnie  
Miejsce oznaczają:  
akanna – tu  
ana akanna – tu, w to miejsce, w tę stronę  
Rodzaj i sposób określają:  
ma’da – bardzo  
libbu – odpowiednio  
libbu aga’i – w taki sposób (22.5).

22.16. Subiunkcjony.  
Zamiast šumma „jeśli” używane jest w nowobabilońskim ki połączone ze sta-tivem w subiunktivie:  
ki panika mahru – jeśli przed tobą jest przyjęte = jeśli jest tobie przyznane  
Inną cechą szczególną nowobabilońskiego jest użycie zaimka względnego ša w znaczeniu „że”:  
šarru ide ša lu ma’da marcak – król wie, że naprawdę bardzo jestem chory  
Pojawiło się też wyrażenie ašša (&lt;ana ša) „na podstawie tego, że”:  
ašša libbu ša aqbakku tebepšuma tattanna – że odpowiednio do tego, co rozkaza¬łem ci, uczyniłeś i dałeś

22.17. Szyk wyrazów  
W nowo- i późnobabilońskim następuje niekiedy pod wpływem aramejskiego przekształcenie utartego szyku wyrazów (1.8):  
tammar rimutka – zobaczysz twoją nagrodę (= otrzymasz ją).  
Lekcja dwudziesta trzecia

23.1. Język młodobabiloński  
Obszerna literatura okresów nowo- i późnobabilońskiego różni się językowo od współczesnych jej listów (22.1 – 22.2). Zapisywana była ona w tzw. języku młodoba-bilońskim (0.9) czyli języku literackim, który usiłował naśladować dialekt starobabiloń-ski, a zwłaszcza język poezji starobabilońskiej (20.1). Jednakże wszystkie teksty mło-dobabilońskie znajdowały się pod większym czy mniejszym wpływem nowo- i późno-babilońskiego języka potocznego. Także część asyryjskich napisów królewskich zapi-sywana była w młodobabilońskim z dającymi się odczuć wpływami asyryjskiego (23.9 – 23.11).

23.2. Mimacja  
Wyjątkowo tylko powraca w młodobabilońskim mimacja, która prawie całkowi-cie zanikła w języku średniobabilońskim (21.2):  
napišti qišam – daruj mi moje życie  
marat danim – córka Anu.

23.3. Końcówki przypadków  
Tylko w nielicznych tekstach młodobabilońskich końcówki przypadków uży¬wane są według reguł starobabilońskich (1.7). Pod wpływem nowobabilońskiego (22.9) końcówka biernika –a w znacznym stopniu zastąpiona jest przez –u:  
ul iccinu qutrinnu – nie wąchają dymu  
bitu irrub – wchodzą do domu.  
Niekiedy końcówki przypadków mogą odpaść:  
ul iššakkan naptan – posiłek nie jest przyrządzony.  
Zamiast –i względnie –u może może występować – przede wszystkim w status constructus – także asyryjska końcówka liczby mnogiej –e:  
qerâtešunu – ich spichlerz  
ana makate ilani rabûti – do pokarmu wielkich bogów.  
W liczbie podwójnej wypada n końcówek –a(n) oraz –i(n), w liczbie tej koń-cówka –a może występować także w bierniku:  
luštakkana tulâ – chcę stale kłaść piersi (przy ustach waszej córki).

23.4. Tworzenie status constructus rzeczowników rodzaju żeńskiego  
Wielosylabowe rzeczowniki rodzaju żeńskiego jak np. napištum „życie” tworzą status constructus przez dołączanie –i (2.8):  
napišti awilim – życie człowieka.  
W poezji status constructus tych rzeczowników może być tworzony przez włącza-nie a pomiędzy spółgłoskę rdzeniową a t:  
napšat bulišu – życie jego bydła  
tuklat nišišu – zaufanie jego ludzi.

23.5. Terminativ adverbialis jako przypadek porównawczy  
Cechą szczególną młodobabilońskiego jest częste używanie terminativu (20.3) do wyrażenia porównania:  
ditalliš – jak popiół  
arbutiš – jak pustynia  
alani šuatunu appul qaqqariš amnu – spaliłem te miasta (i) zrównałem je z zie-mią (jak ziemię).  
W poezji młodobabilońskiej terminativ występuje także w połączeniu z zależ¬nym dopełniaczem:  
ušumgal kališ parakki – samowładca na wszystkich tronach.  
23.6. Zaimki osobowe  
Mianownik zaimków osobowych i wskazujących jest taki sam jak w staro- i średniobabilońskim (21.7):  
anaku – ja  
atta, atti – ty  
šu, ši – on, ona itd.  
Biernik liczby mnogiej zaimków wskazujących, które używane są też przydaw-kowo, jest następujący:  
šuatunu, šuatina  
alani šuatunu appul – spaliłem te miasta.

23.7. Przyrostki zaimkowe  
Przyrostek 1 osoby liczby pojedynczej –i (-ja) używany jest w młodobabiloń¬skim tak jak w starobabilońskim (3.9):  
ummani – moje wojsko  
ina libbija – w moim sercu (por. 22.4).  
Jedynie po zakończonych samogłoską formach liczby mnogiej znajduje się przy-ro¬stek –a:  
ruhe’a – moje czary.  
Zanik samogłosek w wygłosie zaimków osobowych (20.6) występuje również w młodobabilońskim:  
tamertuš – jego łan.  
Niekiedy –šu „jego” i –ša „jej” nie są odróżniane:  
šepašu – jej nogi, obok qataša – jej ręce.  
Zamiast –ninni, czyli przyrostka zaimkowego w bierniku 1 osoby liczby pojedyn-czej z ventivem po formie czasownikowej kończącej się na –u lub –a (13.10), w młodo-babilońskim używa się formy –inni:  
kišpi cubbutu’inni – czary opadły mnie (dostały mnie w swoją moc).

23.8. Nieokreślony zaimek wskazujący ananna  
W wyniku podwojenia zaimka osobowego annû „ten” powstały słowa ananna „taki to a taki” oraz annannitu „taka to a taka”:  
annanna apil annanna – taki to a taki syn takiego to a takiego.

23.9. Czasowniki mediae alef  
W Prs i St tematu podstawowego czasowników mediae alef (21.12) a niekiedy przechodzi w e:  
ila’’ib lub ile’’ib – osłabia  
la’ib lub le’ib – jest osłabiony.  
mâdum (mad) „być licznym” zastąpione jest w nowo- i młodobabilońskim przez asyryjski rdzeń m’d:  
ma’adu – być licznym  
St: ma’id – jest liczny, r.ż. ma’dat – jest liczna  
przymiotnik odczasownikowy: ma’du, ma’attu – liczny, liczna.  
Na temat czasownika lu’’u „pobrudzić” patrz 19.12.  
23.10. Końcówki samogłoskowe form czasownikowych  
U młodobabilońskich czasowników ultimae vocalis pod wpływem nowobabiloń-skiego (22.10) nie są dokładnie rozróżniane końcówki –i (-i) oraz –u (-u):  
bitu uccu (dla ucci) – wychodzi z domu  
ammaki bitu terrubu (dla terrubi) – zamiast wejść do domu (2 osoba r.ż.).  
Niekiedy końcówki samogłoskowe pojawiają się w formach czasownikowych, w których przy normalnym ich użyciu nie powinny się znaleźć (tzw. „zawieszone” samo-głoski):  
bitu irrubu (dla irrub) – wchodzi do domu.  
Normalnie używane formy 2 osoby rodzaju żeńskiego liczby pojedynczej (19.7) także w subiunktywie (7.4) mają końcówkę –i:  
ša terrubi – która wchodzisz  
ammaki … talammani – zamiast tego, że …. traktujesz.

23.11. Asyryjskie formy czasownikowe  
W tekstach młodobabilońskich napisanych w Asyrii znajdują się liczne asyry¬zmy, przede wszystkim na modłę asyryjską tworzone formy czasownikowe:  
aqtirib (babilońskie eqtereb) – zbliżam się  
attumuš (attamuš) – zebrałem  
upatti (upetti) – otworzyłem.

23.12. Wyrażenia przyimkowe  
balum „bez” może być łączone z przyrostkiem zaimkowym:  
baluššu – bez niego.  
W młodobabilońskim słowo to występuje razem z ina:  
ina baliki – bez ciebie.

23.13. Subiunkcjony  
Do wprowadzenia zdania czasowego lub miejscowego służy ema „gdziekol-wiek”, „kiedykolwiek”:  
ema ucammaru – kiedy pragnę.  
Zdania sprzeciwu wprowadzane są przez ammaki „zamiast”:  
ammaki bitu terrubu bitu tuccî – zamiast do domu wejść, dom opuścisz.