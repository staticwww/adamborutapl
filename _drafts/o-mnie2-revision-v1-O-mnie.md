---
id: 639
title: 'O mnie'
date: '2016-09-07T17:50:21+02:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2016/09/2-revision-v1/'
permalink: '/?p=639'
---

**Adam Boruta**

[![Adam](https://adamboruta.pl/wp-content/uploads/2014/12/Adam-300x225.jpg)](https://adamboruta.pl/wp-content/uploads/2014/12/Adam.jpg)

To ja. Zdradzę też, że jestem absolwentem historii UAM w Poznaniu. Oto dowody: praca magisterska, słownik akadyjsko-polski i gramatyka języka akadyjskiego

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/12/1.-Praca-magisterska.pdf) [![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/12/2.-Słownik-akadyjsko-polski.pdf) [![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/12/3.-Gramatyka-języka-akadyjskiego.pdf)

Praca magisterska Słownik Gramatyka

Po krótkim występie w roli nauczyciela w szkole podstawowej osiadłem na rafie filozofii, co skończyło się pracą nauczyciela akademickiego w Wyższej Szkole Pedagogicznej w Zielonej Górze. Męczyłem tam studentów nie tylko filozofią, ale też logiką, co pokazuje poniższy utwór prozą wydany łaskawie przez wydawnictwo uczelniane:

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/07/Skrypt-do-logiki.pdf) [![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/07/O-możliwości-przewidywania.pdf) [![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/07/Wielotorowość-teorii.pdf)

Skrypt do logiki O przewidywaniu świadomości społecznej O nauczaniu literatury

Pozostałe dwa teksty są świadectwem mojej wszechstronności prawie na miarę Leonarda. Pierwszą moją publikacją (niedrukowaną) było opracowanie pewnego fragmentu zleconego przez rząd badania czy ekspertyzy (nie wiem jak to nazwać); działałem wtedy w zespole utworzonym w Instytucie Nauk Politycznych UAM, mogłem się zatem uważać za politologa. Kiedy jednak zostałem filozofem w Zielonej Górze, przerobiłem ów spicz politologiczny na (częściowo) filozoficzny i opublikowałem drukiem w postaci widocznej powyżej. W akademiku WSP mieszkałem obok gościa, który był polonistą i zajmował się dydaktyką literatury. Dla przypieczętowania związku sąsiedzkiego spłodziliśmy razem tekst też tu zamieszczony.

Nie dość mi było być nauczycielem, nauczycielem akademickim, historykiem, politologiem, filozofem, logikiem i dydaktykiem literatury. Kiedy komuna padła, postanowiłem przerzucić się na działalność gospodarczą i zostałem właścicielem firmy handlowej, co skończyło się eleganckim bankructwem i bezrobociem.

Wylądowałem jako pracownik najemny w hurtowni, obecnie na zwolnieniu lekarskim, bo mi się prawe oko rozkleiło.

Jakby co, to adresu adam.boruta@poczta.onet.pl na co dzień nie używam