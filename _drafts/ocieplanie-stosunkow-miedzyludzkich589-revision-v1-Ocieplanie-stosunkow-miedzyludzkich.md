---
id: 593
title: 'Ocieplanie stosunków międzyludzkich'
date: '2016-03-13T20:22:23+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2016/03/589-revision-v1/'
permalink: '/?p=593'
---

OCIEPLENIE STOSUNKÓW MIĘDZYLUDZKICH

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/03/Ocieplenie-stosunków-międzyludzkich.pdf)

W lokalu „U Henia” debatowano w czwartek nad klimatem.

Pan Henio – jak zwykle – zagaił dyskusję. „Nieubłaganie zbliża się wiosna kalendarzowa. Klimatyczna minęła i przeszła w porę gorącą” – zacytował prasę.

Pan Stanisław, konsumujący przedstawiciel świata nauki, zreferował towarzystwu teorię globalnego ocieplenia. Pan Gieniek, mistrz surwiwalu, odniósł się sceptycznie do tej koncepcji. Zdradził, że w ciągu ostatnich jedenastu lat, zawsze po obchodach Święta Kobiet, w nocy między ósmym a dziewiątym marca, hartował organizm układając się do spoczynku na chodniku przed lokalem „U Henia”; w tym jedenastoletnim przedziale czasu nie zauważył bynajmniej podniesienia komfortu snu w aspekcie termicznym. Pan Stanisław poddał jednak w wątpliwość wniosek, jaki pan Gienek zdawał się wyciągać ze swoich eksperymentów zauważając, że nie dysponował on nigdy zbyt precyzyjną aparaturą pomiarową, a ponadto z roku na rok obniżał swoją zdolność absorpcji płynów rozgrzewających, zaś jego garderoba, której faktura nabierała coraz bardziej ażurowego charakteru, stopniowo traciła swoje walory izolacyjne; zatem w sytuacji pana Gienka – podsumował pan Stanisław – subiektywne wrażenie niezmienności warunków termicznych nie może być podstawą do falsyfikacji teorii globalnego ocieplenia, lecz przeciwnie – raczej stanowi argument za tą teorią.

Kiedy pan Gieniek, znawca sztuki przetrwania, zastanawiał się nad ripostą, wsparł go niespodziewanie pan Binio, beneficjent ustawy 500+. Wyjawił, że w smutnych czasach przed Dobrą Zmianą brał udział w próbie praktycznego wykorzystania teorii globalnego ocieplenia. Grupa osób usiłowała wówczas doprowadzić do efektu cieplarnianego przez skłanianie swoich organizmów do emisji naturalnego gazu zawierającego metan. Pomimo wykorzystania monstrualnej ilości grochu, fasoli i cebuli efekt cieplarniany nie nastąpił i trzeba było ratować się odkręceniem kaloryfera. Po dociekliwych pytaniach pana Stanisława pan Binio był jednak zmuszony przyznać, że niejaka pani Bożena, sąsiadka, która nawiedziła eksperymentatorów, odczuła potrzebę otwarcia okna w pomieszczeniu, a to mogłoby świadczyć o tym, że przynajmniej ona doznała poczucia gorącości.

Pani Hela, fanka ojca dyrektora, odsunęła na bok teorię globalnego ocieplenia i wyraziła przekonanie, że to nie zjawiska przyrodnicze, takie jak efekt cieplarniany, są przyczynami podgrzewania atmosfery, lecz przyrastanie społecznej masy grzechu będące efektem mnożenia się hord wódstoków, dżenderów, ateuszów i masonów, za którymi ciągnie się gorący oddech piekła.

Pani Jadzia, jak pan Stanisław reprezentująca naukę i ruch konsumencki, uogólniła myśl pani Heli stwierdzając, że cytat przytoczony na początku przez pana Henia należy odbierać jako dotyczący nie tyle (czy nie tylko) obszaru zainteresowania nauk przyrodniczych, lecz raczej (czy również) dziedziny nauk społecznych (ewentualnie teologicznych). Słowem chodzi (też) o klimat społeczny. Zasadnym jest więc pytanie o to, co podgrzewa stosunki międzyludzkie.

Pan Witek, mistrz karaoke a capella, znany mediom jako Głębokie Gardło, zadeklarował pozytywne nastawienie do ocieplania, a nawet rozpalania stosunków międzyludzkich, jednak zalecił, by nie zapominać przy tym o higienie, przestrzegając przed odparzeniami, otarciami, przekrwieniem oraz innymi kontuzjami powłoki skórnej i tkanek miękkich. Przyczynę ocieplania dostrzegał w naturalnych acz wzniosłych skłonnościach ludzkich uświęconych przez poezję śpiewaną i inne, pośledniejsze rodzaje sztuki.

Panie Jadzia i Hela przy poparciu pana Binia rozwinęły twórczo myśl mistrza karaoke. Nie potępiły w czambuł waloryzowania stosunków międzyludzkich za pomocą wokalistyki, jednakże zwróciły uwagę na to, że zachwalana przez pana Witka poezja śpiewana nazbyt koncentruje się na uczuciowej, by nie rzec: odczuciowej stronie zagadnienia. O wiele bardziej wszechstronna jest filozofia Dobrej Zmiany, która prócz tego, że rzeczonym stosunkom nadaje wymiar narodowotwórczy, to nie odmawia im też zachęty materialnej. Zostawia też przecież miejsce na wokalizę: czyż nie można stosunków międzyludzkich kultywować przy wtórze pieśni patriotycznych czy przyśpiewek biznesowych?

Pani Hela, fanka Radia Maryja, zwróciła dodatkowo uwagę na demaskatorski skutek wprowadzania Dobrej Zmiany. Okazuje się, że u stronników dobra wyzwala ona gorącą chęć współpracy z osobą płci przeciwnej w dziele przysparzania ojczyźnie obywateli, natomiast u wspominanych już wódstoków, dżenderów i innych grzeszników wyzwala gorączkowy pęd do marszobiegów ulicznych i niezdrowej admiracji sędziowskiej obsady Trybunału Konstytucyjnego.

Ocieplanie, rozgorączkowanie, rozpalanie nie może nie mieć wpływu na klimat.