---
id: 146
title: 'Powieści, opowieści i wiersze'
date: '2014-11-08T13:51:38+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/11/88-revision-v1/'
permalink: '/?p=146'
---

[Przypadki magistra N.](https://adamboruta.pl/wp-content/uploads/2014/09/Przypadki-magistra-N..docx) [Przypadki magistra N.](https://adamboruta.pl/wp-content/uploads/2014/09/Przypadki-magistra-N.2.pdf)

[Kącik poezji komornicze](https://adamboruta.pl/wp-content/uploads/2014/09/Kącik-poezji-komorniczej.docx)j [Kącik poezji komorniczej](https://adamboruta.pl/wp-content/uploads/2014/09/Kącik-poezji-komorniczej.pdf)

[Poezja sanitarna czeladzi Warczysławowej](https://adamboruta.pl/wp-content/uploads/2014/09/Poezja-sanitarna-czeladzi-Warczysławowej.docx) [Poezja sanitarna czeladzi Warczysławowej](https://adamboruta.pl/wp-content/uploads/2014/09/Poezja-sanitarna-czeladzi-Warczysławowej.pdf)

[Kronika Chichrały. Księga I](https://adamboruta.pl/wp-content/uploads/2014/09/Kronika-Chichrały.-Księga-I.docx) [Kronika Chichrały. Księga I](https://adamboruta.pl/wp-content/uploads/2014/09/Kronika-Chichrały.-Księga-I.pdf)