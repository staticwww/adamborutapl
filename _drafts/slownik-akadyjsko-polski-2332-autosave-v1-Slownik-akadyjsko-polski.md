---
id: 334
title: 'Słownik akadyjsko-polski'
date: '2014-12-29T20:51:52+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/12/332-autosave-v1/'
permalink: '/?p=334'
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/12/2.-Słownik-akadyjsko-polski.pdf)

UWAGA: proszę raczej korzystać z wersji pdf

SŁOWNIK AKADYJSKO-POLSKI

Oznaczenia:

\[r.ż\] – słowo występujące tylko w rodzaju żeńskim  
\[l.mn.\] – słowo występujące tylko w liczbie mnogiej  
\[St\] – czasownik odmieniany tylko w tzw. statiwie (formie bezczasowej)  
Czasowniki akadyjskie występują w czterech głównych formach – tematach zwanych z niemiecka stammami (gramatykę akadyjską opracowali jako pierwsi uczeni niemieccy). Temat podstawowy (Grundstamm) podawany jest w definicji jako pierwszy bez specjalnego oznaczenia; zresztą wiele czasowników występuje tylko lub głównie w tej formie. Trzy pozostałe tematy to N-Stamm (oznaczany przez N), Doppelstamm (D) i Š-Stamm (Š). W niektórych z tych form występują jeszcze odmiany zwykła oraz T i TN (z wrostkami odpowiednio –t- i –tan-), a więc łącznie mamy G, GT, GTN, N, NTN, D, DT, DTN, Š, ŠT i ŠTN. Te wszystkie formy i odmiany mają swoje zwykłe, typowe rozumienie, ale bardzo częste są wyjątki, kiedy czasownik w danym temacie nabiera znaczenia nietypowego. Typowe znaczenia są następujące: N zazwyczaj jest stroną bierną, czasem zwrotną czasownika w G, D – nadaje mu znaczenie sprawcze, albo wzmocnione, dobitne, a Š wyraża przyczynowość, nakaz, przymus. Z kolei odmiana T jest zwykle tłumaczona jako dotycząca czynności wzajemnych dziejących się pomiędzy (dwoma) obiektami, albo też wyraża stronę bierną, TN wyraża czynność powtarzającą się, wielokrotną, wchodzącą w zwyczaj. Weźmy przykład fikcyjny, dajmy na to czasownika „mówić” (w G-Stammie). Gdyby odmieniał się on typowo, kolejne bezokoliczniki znaczyłyby mniej więcej: w GT „rozmawiać”, w GTN „powtarzać” albo „uporczywie wbijać (komuś coś) do głowy”, albo „ciągle gadać to samo”, ewentualnie „wygłaszać (powszechnie przyjętą) formułę” w N „być wypowiadanym”, w NTN „być wypowiadanym raz po raz”, w D „krzyczeć”, w DT „być wykrzykiwanym”, w DTN „wykrzykiwać w kółko to samo”, w Š „kazać mówić”, „wydobywać zeznanie” w ŠT „zobowiązać się wzajemnie do mówienia” i w ŠTN „kazać regularnie składać sprawozdanie”. Ale – jak już była o tym mowa – bardzo częste są znaczenia nietypowe; przy tym jest zrozumiałe, że w definicjach znaczenia typowe bywają pomijane, zwłaszcza jeżeli są oczywiste, natomiast znaczenia nietypowe są skrupulatnie wskazywane.  
\[N\], \[DT\], ŠTN\] itd. – czasowniki występujące tylko w N, DT, ŠTN itd.  
\[sum.\] – słowo pochodzenia sumeryjskiego  
A, I, U, E – samogłoski krótkie  
A, I, U, E – samogłoski długie  
Â, Î, Ű, € – samogłoski długie powstałe przez „zlanie się” dwóch samogłosek (krótkich lub długich)  
Kursywą pisane są słowa utworzone według reguł gramatyki akadyjskiej przeze mnie, a zatem nie takie, które są znane z oryginalnych tekstów, lecz takie, które są zrekonstruowane, dodane jako uzupełnienie braków. Znaczenia tych słów są jedynie moją hipotezą, a hipoteza nie zawsze jest przecież trafna.

‘

'ALAPPUM \[staroakad.\] lub ELEPPU(M) – STATEK, ŁÓDŹ \[r.ż\]  
'ARAŠUM (a/u) \[staroakad.\] lub EREŠU(M), ERAŠU(M) – UPRAWIAĆ (pole); SIAĆ  
\[jako rzeczownik\] UPRAWIANIE (pola); SIANIE, SIEW  
GTN: ATARRUŠUM, ITERRUŠUM – UPRAWIAĆ KILKAKROTNIE, SYSTEMATYCZNIE  
\[jako rzeczownik\] SYSTEMATYCZNE UPRAWIANIE  
'ARAŠUM (i) lub EREŠU(M) – ŻYCZYĆ, ŻĄDAĆ, WYMAGAĆ; PROSIĆ, WYPRASZAĆ, (z podwójnym Akk. osoby i rzeczy)  
PROSIĆ (kogoś o coś)  
\[jako rzeczownik\] – ŻYCZENIE, ŻĄDANIE, WYMAGANIE; PROSZENIE  
‘ARIŠUM – UPRAWIAJĄCY (pole); SIEWCA  
‘ARIŠUM – ŻĄDAJĄCY; PROSZĄCY, UBIEGAJĄCY SIĘ, PETENT  
‘ARŠUM – UPRAWIANE (pole); SIANY, ZASIANY  
‘ARŠUM – ŻĄDANY, WYMAGANY; BĘDĄCY PRZEDMIOTEM PROŚBY, ŻYCZENIA, WYPRASZANY  
'EMŰQU, 'EMŰQU – SIŁA, MOC, POTĘGA; SIŁA WOJSKOWA, WOJSKO; WIERZCHNIA CZĘŚĆ RĘKI; WNIKLIWOŚĆ,  
MĄDROŚĆ; WYSOKOŚĆ (udziału); MIENIE \[w l.mn. i l.pdw. r.ż.\]  
S.C. EMŰQ, l.pdw. EMŰQÂ(N)

A  
A – NIE (CHCĘ) \[staroakad. partykuła zakazu, odmowy\]; por. A’  
-A(M) – \[końcówka wentywna oznaczająca ruch do, zbliżenie\]  
-A(M) – zob. –IA  
A’ – W SUMIE, W ILOŚCI  
KASPU A’ 1 MANE – PIENIĄDZE W ILOŚCI 1 MINY  
A’ lub AI + następna samogłoska – NIE CHCĘ \[partykuła przed życzeniem negatywnym\]  
A’ABU lub AIABU – WRÓG  
A’AKANI lub AIAKANI – DOKĄD?  
A’ILU \[śr.asyr.\] – CZŁOWIEK; KAŻDY; zob. AMELU, AWILU(M)  
A’U lub AIU – KTÓRY?  
A’UMMA lub AIUMMA – KTOŚ; z przeczeniem LA: NIKT, ŻADEN  
A-A – BOLI! AU! ACH! \[wykrzyknik bólu\]  
A-A – NIE \[z czasownikami w 3 osobie i 1 osobie Prt\]  
AB-ABI lub ABI-ABI – DZIADEK  
ABÂKU (a/u), ABÂKU – ZWRACAĆ SIĘ; UPROWADZAĆ, UPROWADZIĆ W NIEWOLĘ  
\[jako rzeczownik\] ZWRACANIE SIĘ, UPROWADZANIE W NIEWOLĘ  
GT: ATABKU, ATABKU – UPROWADZIĆ, POPĘDZIĆ  
\[jako rzeczownik\] UPROWADZANIE, PĘDZENIE  
ABÂLU(M) (a/i) lub WABÂLU(M) – SKŁANIAĆ; PODNOSIĆ; PRZYNIEŚĆ, NOSIĆ, ODNOSIĆ; NABAWIAĆ SIĘ;  
ZDOBYWAĆ  
\[jako rzeczownik\] SKŁANIANIE, NAKŁANIANIE; PODNOSZENIE, PRZYNOSZENIE, NOSZENIE, ODNO-  
SZENIE; NABAWIANIE SIĘ; ZDOBYWANIE  
GT: ITBULUM – ZABIERAĆ, ODBIERAĆ  
\[jako rzeczownik\] ZABIERANIE, ODBIERANIE  
Š: ŠUBULUM – KAZAĆ PRZYNOSIĆ; KAZAĆ PRZESYŁAĆ; KAZAĆ PRZEWOZIĆ  
\[jako rzeczownik\] NAKAZANIE PRZYNIESIENIA; KAZANIE PRZESŁANIA, NAKAZANIE PRZEWIEZIENIA  
ŠT: ŠUTABULUM – ROZWAŻAĆ, PRZEMYŚLIWAĆ; ZDECYDOWAĆ SIĘ, POSTANOWIĆ  
ANA LIBBIM WABALUM – WNIEŚĆ, WPROWADZIĆ; DOPASOWAĆ  
LIBBAŠU UBBAL ANA – SERCE JEGO SKŁANIA GO DO  
QATATE ANA LEMNETTI INA LIBBI x UBALU – RĘCE W ZŁYM ZAMIARZE PODNIEŚĆ NA x  
ABARAKKU lub AKRIKKU – WEZYR  
ABARU – OŁÓW (Blei)  
ABATU \[nowoasyr.\] – SŁOWO; zob. AWATU(M) lub ABUTU  
ABÂTU (a/u) – G i D UBBUTU: ZBURZYĆ, ZNISZCZYĆ  
\[jako rzeczownik\] BURZENIE, NISZCZENIE  
N (o ile to ten sam rdzeń) NABUTU: UCIEKAĆ; zob. NABUTU  
\[jako rzeczownik\] UCIEKANIE  
Odmiana:  
Prs: I’ABBAT obok IBBAT; Prt: I’BUT obok IBUT; podobnie w innych formach, np.  
Prs N: I”ABBAT; Prt N: I”ABIT  
\[jako rzeczownik\] BURZENIE, NISZCZENIE  
ABÂTU – PRZEPADAĆ, GINĄĆ  
\[jako rzeczownik\] PRZEPADANIE, GINIĘCIE  
ABBUTTU lub ABBUTTU , APPUTTU – ? \[znamię niewolnictwa: być może szczególny rodzaj fryzury\]  
ABBUTU lub ABUTU – WSTAWIENIE SIĘ, WSTAWIENNICTWO (wł. OJCOSTWO)  
ABI-ABI lub AB-ABI – DZIADEK  
ABIKTU, ABIKTU lub APIKTU – KLĘSKA, UPROWADZENIE (w niewolę)  
S.C.: ABIKTI  
ABIKTA ŠAKANU (także GT) – ZADAĆ KLĘSKĘ  
ABIKU, ABIKU – ZWRACAJĄCY SIĘ; UPROWADZAJĄCY W NIEWOLĘ, ŁOWCA NIEWOLNIKÓW  
ABKU, ABKU – JENIEC, UPROWADZONY W NIEWOLĘ  
ABILU – SKŁANIAJĄCY; PODNOSZĄCY; NOSZĄCY, TRAGARZ; NABAWIAJĄCY SIĘ; ZDOBYWAJĄCY, ZDOBYWCA  
ABITU – PRZEPADAJĄCY, GINĄCY, ZNAJDUJĄCY SIĘ NA KRAWĘDZI UPADKU  
ABITU – BURZĄCY, NISZCZĄCY, BURZYCIEL, NISZCZYCIEL  
ABLU – NAKŁONIONY, SKŁONIONY, PRZEKONANY, SKAPTOWANY; PRZENIESIONY, PRZEMIESZCZONY (przez kogoś);  
ZDOBYTY, POZYSKANY  
ABNU(M), ABNU(M) – GRAD; KAMIEŃ, KAMIEŃ SZLACHETNY; KAMIEŃ WAGOWY \[zarówno rodzaju męskiego jak i żeńskiego; w wersji starobabilońskiej rodzaju żeńskiego w liczbie pojedynczej, zaś rodzaju męskiego w liczbie mnogiej\]  
ABRU – STOS DREWNA, STOS  
ABŠINNU \[sum.\] – BRUZDA NA ROLI, BRUZDA SIEWNA  
ABTU – PRZEPADŁY, ZAGINIONY  
ABTU – ZBURZONY, ZNISZCZONY, ZRUJNOWANY  
ABU(M), ABU(M) – OJCIEC  
l.mn. ABBU  
ABUBIŠ – JAK POTOP  
ABUBU – POTOP; PRZYPŁYW SPOWODOWANY SZTORMEM; CYKLON  
ABUB NASPANTI – PRZYPŁYW SPOWODOWANY SZTORMEM, KTÓRY WSZYSTKO ZRÓWNUJE Z  
ZIEMIĄ  
KIMA ABUBIM = ABUBIŠ – JAK POTOP  
ABUBU – POTOP, POWÓDŹ  
ABULLU(M) – BRAMA MIASTA, BRAMA \[r.ż.\]  
ABURRIŠ – W CISZY, W SPOKOJU, CICHO, SPOKOJNIE  
ABUTU lub AWATU(M), AMATU, AMUTU, AWUTU(M) – SŁOWO, WYRAŻENIE, WYPOWIEDŹ, ROZKAZ; RZECZ,  
SPRAWA, OKOLICZNOŚĆ; WYROCZNIA  
BEL AWATIM – OSKARŻYCIEL, PRZECIWNIK W PROCESIE  
AWATAM BATAQUM \[staroasyr.\] – ZAŁATWIAĆ SPRAWĘ  
INA AMAT lub INA QIBIT – WEDLE POLECENIA, WEDLE ROZKAZU  
KAL AMATUŠA – \[w pewnym tekście:\] WSZYSTKIE JEJ WYBRYKI  
ABUTU lub ABBUTU – WSTAWIENIE SIĘ, WSTAWIENNICTWO (właściwie: OJCOSTWO)  
ACCARU lub AZZARU – KOT BŁOTNY  
ACŰ\[średnio- i nowobabilońskie\] lub WACŰ(M) (wci), UCA’U, WACA’UM – WYCHODZIĆ, ODCHODZIĆ, UCIEKAĆ,  
REZYGNOWAĆ; WYCIĄGAĆ; WYSTAWAĆ  
\[jako rzeczownik\] WYCHODZENIE, ODCHODZENIE, UCIEKANIE; REZYGNOWANIE; WYCIĄGANIE; WYSTAWIANIE  
GT: ODCHODZIĆ, ODDALAĆ SIĘ  
Š: ŠUCŰM – WYPROWADZAĆ, KAZAĆ WYJŚĆ; WYSTĄPIĆ (z brzegów), WYLAĆ; WYNOSIĆ,  
WYWLEC, ODCIĄGNĄĆ; UWOLNIĆ; DOPROWADZIĆ DO UPADKU; ZDRADZIĆ (tajemnicę), ŚCIĄGNĄĆ  
(daninę)  
ŠTN: ŠUTECUM – CIĄGLE WYGADYWAĆ SIĘ, WYPAPLAĆ RAZ PO RAZ  
ANA CITIM ŠUCUM – WYDAWAĆ  
ACŰ – WYCHODZIĆ; WSCHODZIĆ (o słońcu); ZACZYNAĆ  
\[jako rzaczownik\] WYCHODZENIE; WSCHODZENIE (o słońcu); ZACZYNANIE  
ACŰ – WYCHODZĄCY, ODCHODZĄCY, UCIEKAJĄCY, REZYGNUJĄCY; WYCIĄGAJĄCY, WYSTAWAJĄCY  
ACŰ – WYCHODZĄCY; WSCHODZĄCY (o słońcu, księżycu); ZACZYNAJĄCY, DEBIUTANT (?)  
AD lub ADI, ADU – (przyimek:) AŻ, DO, OBOK, RAZEM (Z), Z; (spójnik zdań czasowych, subjunkcji:) DO, DOPÓKI,  
AŻ DO  
ADI BIT – AŻ DO (as.)  
ADI INANNA – DOTĄD, DOTYCHCZAS  
ADI MAHRIJA – PRZEDE MNĄ, PRZEDE MNIE, DO MNIE  
ADI MATI – JAK DŁUGO  
ADI x ŠU – x RAZY, x-KROTNIE, np. ADU 12 ŠU – W DWUNASTOKROTNYM WYMIARZE  
ADAD – ADAD \[imię boga burzy\]  
ADAGURRU – PANEW KADZIDŁA \[sum.\]  
ADANNIŠ lub ANADANNIŠ, ANDANNIŠ – BARDZO  
ADANNU – (oczekiwany, stały) MOMENT; TERMIN; PUNKT KOŃCOWY  
ADARU lub EDERU(M) (i) – OBEJMOWAĆ SIĘ, WZIĄĆ W OBJĘCIA  
N: NANDURU – OBEJMOWAĆ SIĘ (WZAJEMNIE)  
\[jako rzeczownik\] OBEJMOWANIE SIĘ, BRANIE SIĘ WZAJEMNIE W OBJĘCIA  
ADÂRU lub ATALŰ, ATTALŰ – ZAĆMIENIE (np. SŁOŃCA) \[sum.\]  
ADARU(M), ADARU(M), ADARU(M) (a/u) lub ED€RU (’dr) – G i N: BAĆ SIĘ, PRZESTRASZYĆ SIĘ, LĘKAĆ SIĘ (czegoś –  
Akk); ULEGAĆ ZAĆMIENIU (o ciele niebieskim)  
\[jako rzeczownik\] BANIE SIĘ, PRZESTRASZENIE SIĘ, LĘKANIE SIĘ; ULEGANIE ZAĆMIENIU (o ciele  
niebieskim)  
GTN: ATADDURUM – BAĆ SIĘ WCIĄŻ NA NOWO, STALE  
\[jako rzeczownik\] BANIE SIĘ STALE, BEZ USTANKU  
ŠT(N): ŠUTADURUM, ŠUTADDURUM – SMUCIĆ SIĘ, BYĆ SMUTNYM; BYĆ ZAMROCZONYM  
\[jako rzeczownik\] SMUCENIE SIĘ, BYCIE SMUTNYM; BYCIE ZAMROCZONYM  
N: NADURUM  
ADDARUM – ADAR (nazwa 12 miesiąca roku)  
ADÎ, ADI lub AD, ADU – (przyimek:) AŻ, DO, OBOK, RAZEM (Z), Z; (spójnik zdań czasowych, subjunkcji:) DO, DOPÓKI,  
AŻ DO  
ADI BIT (asyr.) – AŻ DO,  
ADI INANNA – DOTĄD, DOTYCHCZAS  
ADI MAHRIJA – PRZEDE MNĄ, PRZEDE MNIE, DO MNIE  
ADI MATI – JAK DŁUGO  
ADI x ŠU – x RAZY, x-KROTNIE, np. ADU 12 ŠU – W DWUNASTOKROTNYM WYMIARZE  
ADIRU – OBEJMUJĄCY SIĘ (WZAJEMNIE), TULĄCY (SIĘ)  
ADIRU(M), ADIRU(M), ADIRU(M) – BOJĄCY SIĘ, LĘKAJĄCY SIĘ; ULEGAJĄCY ZAĆMIENIU (o ciele niebieskim)  
ADRU – OBEJMOWANY (przez kogoś ze wzajemnością), TULONY  
ADRU(M), ADRU(M), ADRU(M) – PRZESTRASZONY, ZATRWOŻONY; ZAĆMIONY (o ciele niebieskim)  
ADŰ, ADU lub AD, ADI – j.w.  
ADŰ – UKŁAD, UMOWA  
ADŰ lub WADŰ (wd’) – POROZUMIENIE POTWIERDZONE PRZYSIĘGĄ, UKŁAD ZAPRZYSIĘŻONY, SOJUSZ UMOC-  
NIONY PRZYSIĘGĄ; POROZUMIENIE; OKREŚLENIE, USTANOWIENIE, ZGODA, POSTANOWIENIE, PO-  
LECENIE, ZASADA; PRZYSIĘGA, ŚLUBOWANIE; PRZYKAZANIE  
l. mn. (W)ADE  
BEL ADE – SOJUSZNIK, KTÓRY ZAPRZYSIĄGŁ WIERNOŚĆ, ZŁOŻYŁ PRZYSIĘGĘ  
ADŰ lub WADŰ (wd’) – USTANAWIAĆ; PRZYSIĘGAĆ, KLĄĆ SIĘ  
D: UDDŰM – USTANAWIAĆ NA STAŁE  
\[jako rzeczownik\] USTANAWIANIE; PRZYSIĘGANIE  
ADŰ lub WADŰ – USTANAWIAJĄCY; PRZYSIĘGAJĄCY  
AGÂ – TEN? TA? \[zaimek pytajny\]  
r. ż. AGÂTU, l. mn. r. m. AGANNŰTU, l. mn. r. ż. AGANNŰTU, AGANN€TU  
AGAGU (u\[a\], u) – ROZZŁOŚCIĆ  
\[jako rzeczownik\] ROZZŁASZCZANIE, DRAŻNIENIE  
AGALU – MUŁ, ONAGER  
AGAMMU – BŁOTO, BAGNO, GRZĘZAWISKO \[sum.\]  
AGANA lub GANA – NUŻE! UWAGA! (wł. UWAŻAJ!) \[wykrzyknik\]  
AGANNETU – TE? \[zaimek pytajny\]; zob. AGA  
AGANNUTU – CI? TE? \[zaimek pytajny\]; zob. AGA  
AGAPPU lub GAPPU – SKRZYDŁO  
AGARRU lub AGRU – NAJEMNIK, NIEWOLNIK, ODDANY W NAJEM  
AGARU(M) (a/u) – NAJMOWAĆ  
\[jako rzeczownik\] NAJMOWANIE  
AGÂTU – TA? \[zaimek pytajny\]; zob. AGA  
AGGU – ZDENERWOWANY, GNIEWNY  
AGIGU – ZŁOSZCZĄCY, ROZZŁASZCZAJĄCY, DRAŻNIĄCY  
AGIRU(M) – NAJMUJĄCY, NAJEMCA; PRACODAWCA (?)  
AGRU, AGRU lub AGARRU – NAJEMNIK, NIEWOLNIK, ODDANY W NAJEM  
AGŰ – OPASKA, OPASKA NA GŁOWĘ, TURBAN; CZAPKA KRÓLEWSKA, DIADEM, BOSKIE NAKRYCIE GŁOWY,  
TIARA, KORONA  
l. mn. AGE  
BEL AGI – WŁADCA KORONY (zwrot używany zwykle jako określenie boga księżyca Sina)  
AGŰ(M) – PRZYPŁYW (spowodowany sztormem); PRĄD, NURT \[sum.\]  
AGURRU, AGURRU – WYPALANA CEGŁA  
AHAMEŠ – WZAJEMNIE, NAWZAJEM, OBOPÓLNIE \[AHU + IŠ\]  
AHANNÂ – Z TEJ STRONY  
ANA AHANNÂ – NA TĘ STRONĘ, W TĘ STRONĘ, TU  
AHANNÂ – KAŻDY DLA SIEBIE  
\[AHARU\] – zob. UHHURU  
AHAT… – NAD BRZEGIEM… \[dialekt\]  
AHATU(M) – SIOSTRA  
AHAZU(M) (a/u) – BRAĆ, ŁAPAĆ; OTRZYMYWAĆ, DOSTAWAĆ; WZIĄĆ ŚLUB, OŻENIĆ SIĘ, WYJŚĆ ZA MĄŻ;  
UCZYĆ SIĘ, DOWIADYWAĆ SIĘ  
\[jako rzeczownik\] BRANIE, ŁAPANIE; OTRZYMYWANIE, DOSTAWANIE; WZIĘCIE ŚLUBU, WYJŚCIE  
ZA MĄŻ, OŻENEK, ZAŚLUBINY; UCZENIE SIĘ, DOWIADYWANIE SIĘ, ZDOBYWANIE WIEDZY  
Š: ŠUHUZUM – KAZAĆ BRAĆ  
\[jako rzeczownik\] SKŁANIANIE DO WZIĘCIA  
AŠŠATAM AHAZUM – BRAĆ ZA ŻONĘ  
QATATE AHAZU – WZIĄĆ NA SIEBIE? PRZYJĄĆ? PORĘKĘ  
MARQITA AHAZU – SZUKAĆ SCHRONIENIA  
LEMNETI AHAZU – ZADAWAĆ SIĘ ZE ZŁYM  
DINAM ŠUHUZUM – WYTOCZYĆ, WSZCZĄĆ PROCES  
AHHAZU – ŁAPACZ \[nazwa demona\]  
AHHUTU lub AHUTU – STOSUNKI BRATERSKIE, BRATERSTWO  
AHITU – STRONA  
AHITAM – PO STRONIE  
AHITU – OBCA, CUDZA; zob. AHU  
\[jako abstrakt\] OBCOŚĆ  
AHIZU(M) – BIORĄCY, ŁAPIĄCY; OTRZYMUJĄCY, DOSTAJĄCY, BENEFICJENT; PAN MŁODY, PANI MŁODA. OBLUBIE-  
NIEC, OBLUBIENICA; UCZĄCY SIĘ, DOWIADUJĄCY SIĘ, UCZEŃ  
AHU, AHU lub KIŠADU – BOK, STRONA (ciała), RAMIĘ; BRZEG (rzeki), WYBRZEŻE  
AHŰ – OBCY, CUDZY  
r. ż. AHITU  
ILKU AHŰ – SPECJALNE LENNO  
AHŰ lub BARBARU – ZWIERZĘ DRAPIEŻNE, WILK? HIENA?  
AHU(M) – BRAT  
l. mn. AHHU  
AHULAP – ŁASKI! LITOŚCI! \[wykrzyknik\]  
AHUM… AHUM – JEDEN… DRUGI…  
AHUTU lub AHHUTU – STOSUNKI BRATERSKIE, BRATERSTWO  
AHZU(M) – ZABRANY, ZŁAPANY, WZIĘTY; OTRZYMANY, DOSTANY; POŚLUBIONY, ZAŚLUBIONY, OŻENIONY; WYUCZO-  
NY, POZNANY  
AI lub A’ (przed samogłoską) – NIE CHCĘ \[partykuła przed życzeniem negatywnym\]  
AJ lub Â – NIE (CHCĘ) \[partykuła zakazu, odmowy\]  
-AJA lub –AJUM – \[końcówka służąca do tworzenia nazw ludów\]; por. CIDUNNAJJA  
AIABU lub A’ABU – WRÓG  
AIAKANI lub IA’AKAN – DOKĄD?  
AIALU – JELEŃ  
AJJABU(M) – WRÓG  
AJJAKAM lub AJJIKIAM – GDZIE  
AJJATUM – KTÓRZY; zob. AJJUM  
AJJEKÂ lub AJJIKI’AM – GDZIE?  
AJJ€Š lub AJJIŠ – GDZIE  
AJJIKI’AM – GDZIE?  
AJJIKIAM lub AJJAKAM – GDZIE  
AJJIŠ lub AJJ€Š – GDZIE  
AJJITUM – KTÓRA?; zob. AJJUM  
AJJITUMMA – JAKAKOLWIEK  
AJJUM – KTÓRY?  
r. ż. AJJITUM, l. mn. AJJUTUM, AJJATUM  
AJJUMMA, AJJUMMA – KTOKOLWIEK, KTOŚ; JAKIKOLWIEK  
AJJUTUM – KTÓRZY; zob. AJJUM  
AIU lub A’U – KTÓRY?  
-AJUM lub –AJA – \[końcówka służąca do tworzenia nazw ludów\]; por. CIDUNNAJJA  
AIUMMA lub A’UMMA – KTOŚ; z przeczeniem LA: NIKT, ŻADEN  
AKALU – CHLEB; POKARM, POŻYWIENIE, JADŁO  
AKALU(M) (a/u) – JEŚĆ, ŻREĆ, POŻERAĆ, SPOŻYWAĆ, ZAŻYWAĆ, PRZEGRYŹĆ; PRZYBYWAĆ W CELU SPOŻYCIA  
CZEGOŚ; ŻYWIĆ; BOLEĆ; MIEĆ PRAWO UŻYTKOWANIA; WYPLENIĆ, WYTĘPIĆ DO SZCZĘTU  
\[jako rzeczownik\] JEDZENIE, POŻERANIE, SPOŻYWANIE, ZAŻYWANIE, PRZEGRYZANIE; PRZYBYWANIE  
W CELU SPOŻYCIA CZEGOŚ; ŻYWIENIE; BOLENIE; POSIADANIE PRAWA DO UŻYTKOWANIA; WY-  
PLENIENIE, WYTĘPIENIE  
GTN: ATAKKULUM (a) – JEŚĆ WCIĄŻ NA NOWO, OBŻERAĆ SIĘ  
\[jako rzeczownik\] OBŻERANIE SIĘ, JEDZENIE BEZ USTANKU  
Š: ŠUKULUM – KAZAĆ JEŚĆ  
\[jako rzeczownik\] SKŁONIENIE DO JEDZENIA  
KARCI AKALUM – OCZERNIAĆ, SPOTWARZAĆ  
EQLU AKALU – PRZYCHODZIĆ, BY SPOŻYĆ PLON  
ZITTA AKALU – ZJADAĆ (swój) UDZIAŁ = MIEĆ UDZIAŁ  
KARCI ATAKKULUM – WIELOKROTNIE OCZERNIAĆ  
AKAŠU(M) (a/?) – GT: ATAKŠUM – ODCHODZIĆ  
\[jako rzeczownik\] ODCHODZENIE  
AKCU lub EKCU – DZIKI, NIEOPANOWANY  
AKILU – ŻARŁOK; JEDZĄCY; „ŻARŁOK”, „JEDZĄCY” \[rodzaj gąsienicy\]  
AKILU(M) – JEDZĄCY, SPOŻYWAJĄCY, SPOŻYWCA, UŻYTKOWNIK; ŻYWIĄCY, ŻYWICIEL; TĘPIĄCY, TĘPICIEL  
AKITU – PROCESJA ŚWIĄTECZNA; ŚWIĘTO PROCESJI \[sum.\]  
BIT AKIT(UM) – DOM UROCZYSTOŚCI  
AKITU(M) – AKITUM (nazwa święta Nowego Roku)  
BIT AKITIM – DOM ŚWIĄT NOWEGO ROKU  
AKKULLU – PIĘTA (Hacke)  
AKLU – DOZORCA, STRAŻNIK; SEKRETARZ \[sum.\]  
s.c. AKIL  
AKLU(M) – JEDZONY, SPOŻYWANY, ZAŻYWANY, POŻERANY; UŻYTKOWANY; ŻYWIONY, KARMIONY; TĘPIONY  
AKRABU lub ZUQAQIRU, AQRABU – SKORPION (zarówno zwierzę jak i znak zodiaku)  
AKRIKKU lub ABARAKKU – WEZYR  
AKŰ – SŁABY  
AKŰ lub EKŰ – SIEROTA NIE MAJĄCY OJCA (obraźliwe), BĘKART; BIEDAK, UBOGI, CIERPIĄCY BIEDĘ; ZŁAMANY,  
ZMARNOWANY  
r. ż AKŰTU, EKŰTU  
AKŰTU – zob. AKŰ; UBÓSTWO, BIEDA, NĘDZA, UPADEK  
AKŰTU – zob. AKŰ; SŁABOŚĆ  
ALADLAMŰ – ALADLAMU \[określenie męskiego typu bóstw opiekuńczych (jako figur byczych u bram pałacu)\] \[sum.\]  
ALAKTU(M) – KARAWANA; OBCHODZENIE (czegoś podczas podróży); ZMIANA (wandel)  
ALAKU lub UMMU, KUNNU – ZAKŁADAĆ  
\[jako rzeczownik\] ZAKŁADANIE  
ALAKU(M) (a/i) – IŚĆ, BIEC, MASZEROWAĆ (także z Akk.); PRZYCHODZIĆ (z Vent.); OBJEŻDŻAĆ (rydwanem); PŁY-  
NĄĆ, CIEC (o łzach); WIAĆ (o wietrze); UPŁYWAĆ (o czasie)  
\[jako rzeczownik\] CHODZENIE, BIEGANIE, MASZEROWANIE; PRZYCHODZENIE; OBJEŻDŻANIE  
(rydwanem); PŁYNIĘCIE, WYLEWANIE (o łzach); WIANIE (o wietrze); UPŁYWANIE (o czasie)  
GT: ATALKUM – ODCHODZIĆ, WYRUSZAĆ W DROGĘ (z Vent.)  
\[jako rzeczownik\] ODCHODZENIE, WYRUSZANIE W DROGĘ  
GTN: ATALLUKUM – WCIĄŻ IŚĆ, PRZECHADZAĆ SIĘ, SPACEROWAĆ  
\[jako rzeczownik\] PRZECHADZANIE SIĘ, SPACEROWANIE  
Š: ŠULUKUM – KAZAĆ IŚĆ  
\[jako rzeczownik\] KAZANIE IŚĆ  
HARRANA ALAKUM – IŚĆ DROGĄ  
WACUTAM ALAKUM – ODCHODZIĆ  
HARBUTAM ALAKUM – STAWAĆ SIĘ PUSTYM, PUSTOSZEĆ  
AŠAREDUTAM ALAKUM – ZDOBYĆ NAJWYŻSZĄ RANGĘ  
ARBŰTAM ALAKUM – RZUCAĆ SIĘ DO UCIECZKI  
IDE x ALAKU – PRZEJŚĆ NA STRONĘ x  
PAN(I)… ALAKU – DOWODZIĆ  
(U)ARKU… ALAKU – NASTĘPOWAĆ  
RECUT x ALAKU – PRZYCHODZIĆ x Z POMOCĄ  
ARBUTA lub MUŠKENUTA ALAKU – BYĆ DZIKIM lub NA WPÓŁ WOLNYM  
REQUTA ALAKU – ODCHODZIĆ Z PUSTYMI RĘKOMA  
TAPPUT x ALAKU – STAWAĆ SIĘ TOWARZYSZEM x  
ŢARIDUTA ALAKU – BYĆ WYPĘDZONYM  
ANA ŠIMTI ALAKU – UMRZEĆ ZA  
ILKA (ILIK)… ALAKU – PEŁNIĆ OBOWIĄZEK LENNY  
ADI LA BAS€ ŠULUKU – WYPLENIĆ, WYKORZENIĆ DO SZCZĘTU  
ARBUTIŠ ŠULUKU – KAZAĆ BYĆ PUSTKOWIEM, SPUSTOSZYĆ  
ALALU(M) (a) – WISIEĆ, POWIESIĆ  
\[jako rzeczownik\] WISZENIE, POWIESZENIE, WIESZANIE  
D: ULLULUM – WISIEĆ, OBWIESZAĆ  
\[jako rzeczownik\] WISZENIE; OBWIESZANIE  
INA x ALALUM – ZWIĄZAĆ Z x  
ALI – GDZIE?  
ALIDDU lub ALIDU – RODZIC  
ALIDU lub ALIDDU – j.w.  
ALIKU – ZAKŁADAJĄCY; ZAŁOŻYCIEL (?), FUNDATOR (?)  
ALIKU(M) – IDĄCY; POSŁANIEC  
ALIK PANI UMMANIM – KROCZĄCY NA CZELE WOJSKA  
ALIK HARRANIM – PODRÓŻNY; TEN, KTÓRY WYTYCZA POLA, MIERNICZY  
ALIK PAN(I) – PRZYWÓDCA; POPRZEDNIK (w czasie)  
ALIK MAHRI – POPRZEDZAJĄCY (w przestrzeni i czasie), POPRZEDNIK  
ALIK IDIŠU – IDĄCY PRZY JEGO BOKU = POMOCNIK  
ALILUM – WISZĄCY;, ZAWIESZAJĄCY, WIESZAJĄCY  
ALITTU – MATKA; RODZĄCA  
ALKA – NUŻE! ŚMIAŁO! (wł. CHODŹ!) \[wykrzyknik\]  
ALKU – ZAŁOŻONY, ZAPOCZĄTKOWANY  
ALKU(M) – PRZEBYTY, OBJECHANY; WYCIEKŁY, WYLANY (o łzach); PRZESZŁY (o czasie)  
ALLAKU – WĘDRUJĄCY; PODRÓŻNIK  
ALLUM – ZAWIESZONY, POWIESZONY  
ALMATTU – WDOWA  
ALPU(M) – KROWA, BYK; BYDŁO (zwłaszcza w l. mn.; słowo używane jako przeciwstawienie wyrazu CENU – drobny inwen-  
tarz, w tym owce)  
ALTU lub AŠŠATU – NIEWIASTA, KOBIETA  
ALU – MIASTO; STOLICA  
ALU(M) – MIASTO; ŚRÓDMIEŚCIE; GMINA  
ŠA LIBBI ALIM – MIESZKAŃCY MIASTA, MIESZCZANIE  
AL (BIT) ŠARRUTI – REZYDENCJA  
AL DANNUTI – TWIERDZA  
AMARSU’ENA – AMARSUENA \[imię króla z III dynastii z Ur\]  
AMARU(M) (a/u) – WIDZIEĆ, PATRZEĆ, OGLĄDAĆ; BADAĆ, KONTROLOWAĆ, SPRAWDZAĆ; WYBIERAĆ; DOZNA-  
WAĆ, PRZEŻYWAĆ; CZYTAĆ; St: ZNAĆ, ROZUMIEĆ  
\[jako rzeczownik\] WIDZENIE, PATRZENIE, OGLĄDANIE; BADANIE, KONTROLOWANIE, SPRAW-  
DZANIE; WYBIERANIE; DOZNAWANIE, PRZEŻYWANIE; CZYTANIE  
N: NANMURUM – WIDZIEĆ SIĘ NAWZAJEM, SPOTYKAĆ SIĘ ZE SOBĄ (z ITTI); ZBLIŻAĆ SIĘ DO  
ZIEMI (o skale); WSCHODZIĆ (o ciałach niebieskich)  
\[jako rzeczownik\] WIDZENIE SIĘ NAWZAJEM, SPOTYKANIE SIĘ ZE SOBĄ; ZBLIŻANIE SIĘ DO ZIEMI;  
WSCHODZENIE  
ŢUPPI (UNNEDUKKI) INA AMARIKA – SKORO TYLKO MOJĄ TABLICZKĘ (LIST) PRZECZYTASZ  
INA MUHHI x AMARU – BYĆ ODDANYM, ULEGŁYM x  
INA QATE x AMARU – PRZEKONAĆ SIĘ NA PRZYKŁADZIE x (w sensie: x JEST PRZESTROGĄ)  
AMATU \[średnio- i nowobabilońskie, spotykane czasem w starobabilońskim\] lub AWATU(M), ABUTU, AMUTU, AWUTU(M) (’wj) – SŁOWO, WYRAŻENIE, WYPOWIEDŹ, ROZKAZ; RZECZ, SPRAWA, OKOLICZNOŚĆ; WYROCZNIA  
S.C. AWAT lub AMAT, l. mn. AWATI lub AMATI  
BEL AWATIM – OSKARŻYCIEL, PRZECIWNIK W PROCESIE  
AWATAM BATAQUM \[staroasyr.\] – ZAŁATWIAĆ SPRAWĘ  
INA AMAT lub INA QIBIT – WEDLE POLECENIA, WEDLE ROZKAZU  
KAL AMATUŠA – \[w pewnym tekście:\] WSZYSTKIE JEJ WYBRYKI  
AMELU, AMELU \[starobab. i nowoasyr.\] lub AWILU(M), A’ILU, AMILU – CZŁOWIEK; WOLNY OBYWATEL (posiadający  
ziemię)  
AMELU lub AWELU – MĘŻCZYZNA; CZŁOWIEK; SŁUGA  
AMELUTTUM lub AWILUTU(M), AMELUTU – LUDZKOŚĆ; CZŁOWIEK  
AMELUTU lub AWILUTU(M), AMELUTTU – j.w.  
AMILU \[średniobab.\] lub AWILU(M), A’ILU, AMELU – j.w.  
AMIRUM – WIDZĄCY, OGLĄDAJĄCY, PATRZĄCY, WIDZ, OBSERWATOR; BADAJĄCY, SPRAWDZAJĄCY, KONTRO-  
LUJĄCY, KONTROLER; WYBIERAJĄCY; DOZNAJĄCY, PRZEŻYWAJĄCY; CZYTAJĄCY, CZYTELNIK  
AMIŠAM lub AMMIŠAM – TAM, TAMTĘDY  
AMMAKI – ZAMIAST \[spójnik\]  
AMMALA – zob. MALA  
AMMATU, AMMATU – ŁOKIEĆ (miara długości o wymiarze ulegającym wahaniom w zależności od czasu i miejsca)  
AMMENI lub AMMINI(M) – DLACZEGO? CZEMU?  
AMMICADUQA – AMMICADUQA (imię króla Babilonu z dynastii Hammurabiego)  
AMMIDITANA – AMMIDITANA (imię króla Babilonu, jednego z następców Hammurabiego) \[zachodniosemickie\]  
AMMINI(M) lub AMMENI – DLACZEGO? CZEMU?  
AMMIŠAM lub AMIŠAM – TAM, TAMTĘDY  
AMMITUM \[asyr.\] – TAMTE, OWE; zob. AMMŰ  
AMMU – PLEMIĘ, NARODOWOŚĆ, NARÓD  
AMMŰ – TEN; ZNAJDUJĄCY SIĘ PO TEJ STRONIE; NA TEJ STRONIE  
AMMŰ \[asyr.\] – TAMTEN, ÓW  
r.ż. AMMITUM, l.mn. AMMŰTUM, l.mn. r.ż. AMMIATUM, AMMÂTUM  
ŠA ŠEP AMMATE – NA PRZECIWLEGŁYM BRZEGU  
AMRUM – WIDZIANY, ZOBACZONY, OGLĄDANY; BADANY, SPRAWDZANY, KONTROLOWANY; WYBRANY; DOZNANY,  
PRZEŻYWANY; CZYTANY, PRZEGLĄDANY  
AMŠALI lub TIMALI – WCZORAJ  
AMŠAT – \[składnik zwrotu\]  
INA AMŠAT (N) – WCZORAJ NOCĄ  
AMTU(M) – SŁUŻĄCA, NIEWOLNICA  
AMURDINNU – ? (jakiś dziko rosnący, pospolity krzew) \[słowo obcego pochodzenia\]  
AMURRU – ZACHÓD (strona świata)  
AMUTU \[średnoasyr.\] lub AWATU(M), ABUTU, AMATU, AWUTU(M) – SŁOWO, WYRAŻENIE, WYPOWIEDŹ, ROZ-  
KAZ; RZECZ, SPRAWA, OKOLICZNOŚĆ; WYROCZNIA  
S.C. AWAT lub AMAT, l. mn. AWATI lub AMATI  
BEL AWATIM – OSKARŻYCIEL, PRZECIWNIK W PROCESIE  
AWATAM BATAQUM \[staroasyr.\] – ZAŁATWIAĆ SPRAWĘ  
INA AMAT lub INA QIBIT – WEDLE POLECENIA, WEDLE ROZKAZU  
KAL AMATUŠA – \[w pewnym tekście:\] WSZYSTKIE JEJ WYBRYKI  
AMUTU(M) – WĄTROBA; WRÓŻBA Z WĄTROBY ZWIERZĘCIA OFIARNEGO; WRÓŻBA, PRZEPOWIEDNIA  
-ANA – zob. –IA  
ANA, ANA – DO, KU \[miejscowo\]; NA (kiedyś), PO UPŁYWIE \[czasowo\]; DLA, DO, KU, NA \[celowo\]; NA PODSTAWIE, Z  
POWODU, ZE WZGLĘDU NA \[przyczynowo\]; ZGODNIE Z, STOSOWNIE DO, WEDŁUG \[o sposobie\]; NA \[o  
podziale\]; JAKO \[o postaci\]; z rzeczownikiem tłumaczyć za pomocą celownika tego rzeczownika, z bezokolicz-  
nikiem jako ABY \[zdanie celowe\]  
ANA MIŠLANI – NA PÓŁ  
ANA APŠIT€M – WEDŁUG STOSUNKU, PROPORCJI  
ANA ŢARSI – PRZECIW  
ANA METUM – W STU  
ANADANNIŠ lub ADANNIŠ, ANDANNIŠ – BARDZO  
ANAHU (i) – WZDYCHAĆ, WESTCHNĄĆ  
\[jako rzeczownik\] WZDYCHANIE, WESTCHNIENIE  
ANAHU (a) – NAMĘCZYĆ SIĘ STRASZNIE, MĘCZYĆ SIĘ, WYSILAĆ SIĘ, WYSILIĆ SIĘ; BYĆ ZAGROŻONYM ZAWA-  
LENIEM, BYĆ W STANIE GROŻĄCYM ZAWALENIEM  
\[jako rzeczownik\] MĘCZENIE SIĘ, WYSILANIE SIĘ; BYCIE ZAGROŻONYM ZAWALENIEM, WALENIE SIĘ  
ŠT: ŠUTANUHU – MĘCZYĆ SIĘ, WYSILAĆ, WYSILIĆ SIĘ  
\[jako rzeczownik\] MĘCZENIE SIĘ, WYSILANIE SIĘ  
ANAKU – JA  
Odmiana:  
l. p. ANAKU – JA l. mn. NINU lub NENU – MY  
JÂTI – MNIE NIATI – NAS  
JÂŠIM – MI NIAŠIM – NAM  
ANAKU, ANAKU lub ANNAKU – OŁÓW; CYNA \[sum.\]  
ANANTU – BÓJ, WALKA \[poet.\]  
ANDANNIŠ lub ADANNIŠ, ANDANNIŠ – BARDZO  
ANDULLU – CIEŃ; OPIEKA, OCHRONA \[sum.\]; (syn. CALULU)  
ANDURARU – UWOLNIENIE; (w kodeksie Hammurabiego:) ZWOLNIENIE Z PODATKÓW, WOLNOŚĆ PODATKOWA  
ANHU – ZMĘCZONY (?); ZAGROŻONY ZAWALENIEM, UPADKIEM  
ANHUTU – UPADEK, CHYLENIE SIĘ KU UPADKOWI  
ANIHU – WZDYCHAJĄCY  
ANIHU – MĘCZĄCY SIĘ, WYSILAJĄCY SIĘ  
-ANI – zob. –IA  
ANNA – TAK  
ANNAKU lub ANAKU – OŁÓW; CYNA \[sum.\]  
ANNATUM – CI; zob. ANNU  
ANNIAM lub ANNIM, ANNŰ (M), ANNIU, HANNIU – TEN  
l. mn. r. m. ANN(I)UTUM – CI; l. mn. r. ż. ANN(I)ATUM – TE  
(H)ANNÎMMA – TAK SAMO, ZGODNIE Z  
ANA MUHHI ANNITI – W DODATKU, PRÓCZ TEGO \[środkowobab.\]  
ANNITKA – TO OD CIEBIE  
ANNIATUM – zob. ANNIM lub ANNIAM  
ANNIKIAM – TU, TUTAJ  
ANNIM lub ANNIAM, ANNŰ (M), ANNIU, HANNIU – TEN  
l. mn. r. m. ANN(I)UTUM – CI; l. mn. r. ż. ANN(I)ATUM – TE  
(H)ANNÎMMA – TAK SAMO, ZGODNIE Z  
ANA MUHHI ANNITI – W DODATKU, PRÓCZ TEGO \[środkowobab.\]  
ANNITKA – TO OD CIEBIE  
ANNIMMA lub HANNIMMA – TAK SAMO, ZGODNIE Z; zob. ANNIU lub HANNIU  
ANNITU – TO (o rzeczy); zob. ANNU  
ANNIU \[nowoasyr.\] lub ANNU(M), ANNIAM, ANNIM, HANNIU – TEN  
l. mn. r. m. ANN(I)UTUM – CI; l. mn. r. ż. ANN(I)ATUM – TE  
(H)ANNÎMMA – TAK SAMO, ZGODNIE Z  
ANNITKA – TO OD CIEBIE  
ANNIUTUM – CI; zob. ANNIM  
ANNU lub ARNU – PRZEWINIENIE, GRZECH, PRZESTĘPSTWO; GRZYWNA, KARA  
S.C.: ANAN  
BEL ARNI – GRZESZNIK, WINOWAJCA  
ANNŰ(M), ANNŰ(M) lub ANNIAM, ANNIM, ANNIU, HANNIU – TEN  
l. mn. r. m. ANN(I)UTUM – CI; l. mn. r. ż. ANN(I)ATUM – TE  
(H)ANNÎMMA – TAK SAMO, ZGODNIE Z  
ANA MUHHI ANNITI – W DODATKU, PRÓCZ TEGO \[środkowobab.\]  
ANNITKA – TO OD CIEBIE  
ANNUTUM – CI; zob. ANNIM lub ANNIAM  
ANQULLU – ŻAR, SKWAR W POŁUDNIE \[sum.\]  
ANU – ANU (imię boga nieba)  
-ANU – zob. IA  
-ANUM – \[końcówka określająca czynność jednostkową lub osobę wykonującą tą czynność\]; por. NADINANUM  
ANUMMA – TERAZ, ODTĄD, TERAZ OTO; OTÓŻ!  
ANUNTU – WALKA (poet.)  
ANUTU – ISTOTA (boga) ANU, WŁASNOŚCI ANU; BOSKOŚĆ, (wyższa) WŁADZA BOSKA  
ANZILLU – WYSTĘPEK, ZBRODNIA  
APALU(M), APALU(M) (a/u) – ODPOWIADAĆ; BYĆ ODPOWIEDZIALNYM?; ZASPAKAJAĆ, ZADAWALAĆ (wierzyciela,  
pretensje); PŁACIĆ  
\[jako rzeczownik\] ODPOWIADANIE, BYCIE ODPOWIEDZIALNYM; ZASPAKAJANIE, ZADAWALANIE  
(wierzyciela, pretensji); PŁACENIE, UISZCZANIE  
GTN: ATAPPULUM, ATAPPULU – STALE, WCIĄŻ NA NOWO ZADAWALAĆ, TROSZCZYĆ SIĘ, DBAĆ  
\[jako rzeczownik\] ZADAWALANIE BEZ PRZERWY, TROSZCZENIE SIĘ, DBANIE  
N: NAPPULUM, NAPPULU – BYĆ DOSTARCZANYM, BYĆ ZAOPATRZENIEM  
\[jako rzeczownik\] BYCIE DOSTARCZANYM, BYCIE ZAOPATRZONYM  
D: UPPULUM, UPPULU – WYZNACZAĆ NA SPADKOBIERCĘ  
\[jako rzeczownik\] WYZNACZANIE NA SPADKOBIERCĘ  
INA x APALU (N) – ODPOWIADAĆ NA x  
x (Akk. osoby) APALU – USPRAWIEDLIWIAĆ SIĘ PRZED x  
x (Akk. osoby) y (Akk. rzeczy) APALU – UREGULOWAĆ (np. dług komuś), ZAPŁACIĆ x ZA y  
APARU (i) – NAKŁADAĆ (nakrycie głowy)  
\[jako rzeczownik\] NAKŁADANIE, UBIERANIE (nakrycia głowy)  
APÂTI – LUDZIE (właściwie: ZACHMURZENI, ZACIEMNIENI)  
APIKTU lub ABIKTU – KLĘSKA, UPROWADZENIE (w niewolę)  
ABIKTA ŠAKANU (także GT) – ZADAĆ KLĘSKĘ  
S.C.: ABIKTI  
APILUM, APILU – ODPOWIADAJĄCY, UDZIELAJĄCY ODPOWIEDZI; ODPOWIADAJĄCY (za coś), ODPOWIEDZIALNY;  
ZASPAKAJĄCY, ZADAWALAJĄCY (pretensje, wierzytelności); PŁACĄCY, PŁATNIK  
APIRU – NAKŁADAJĄCY (nakrycie głowy)  
APIŠAL – APISZAL (nazwa miasta leżącego prawdopodobnie w pn. Mezopotamii)  
APKALLU, APKALLU – MĘDRZEC, MISTRZ; MĄDRY \[sum.\]  
APKAL ILANI – WSZYSTKOWIEDZĄCY MIĘDZY BOGAMI  
APLU(M), APLU(M) – SYN, DZIEDZIC, SPADKOBIERCA; NASTĘPCA; DZIEDZICTWO, SPUŚCIZNA, DZIEDZICZENIE  
s.c.: APAL  
APLUM RABUM – NAJSTARSZY SYN  
APLUM, APLU – ZASPOKOJONY; ZAPŁACONY, UISZCZONY  
APLŰTUM – SPADEK, DZIEDZICZENIE  
APPARATE, APPARATE lub APPARU – GĘSTWINA TRZCINOWA, TRZCINOWE ZAROŚLA; SITOWIE; SZUWARY;  
BAGNO, TRZĘSAWISKO \[APPARATE = liczba mnoga od APPARU\]  
QAN APPARU – SITOWIE, TRZCINA, SZUWARY  
APPARU lub APPARATE (l. mn.) – GĘSTWINA TRZCINOWA, TRZCINOWE ZAROŚLA; BAGNO, TRZĘSAWISKO \[AP-  
PARATE = liczba mnoga od APPARU\]  
APPARU – BAGNO, TOPIEL, TRZĘSAWISKO  
APPU(M) – NOS; OBLICZE  
APPI MARTIM – PRZEDNIA CZĘŚĆ GÓRNEJ STRONY WORECZKA ŻÓŁCIOWEGO (dosłownie: NOS WO-  
RECZKA ŻÓŁCIOWEGO)  
DUR APPI – POLICZEK  
APPUTTU lub ABBUTTU – ? \[znamię niewolnictwa: być może szczególny rodzaj fryzury\]  
APPUTTUM lub APPUTUM – PROSZĘ; PILNY, NIE CIERPIĄCY ZWŁOKI  
APPUTUM – zob. ABBUTTU  
APPUTUM lub APPUTTUM – PROSZĘ; PILNY, NIE CIERPIĄCY ZWŁOKI  
APRU – NAŁOŻONY NA GŁOWĘ  
APSASITU – APSASITU \[rodzaj krowy\]  
APSAŠITU – APSASZITU \[rodzaj krowy\]  
APSAŠŰ – APSASZU \[rodzaj byka\] \[sum.\]  
APSŰ – OCEAN (słodkowodny); (przenośnie:) GŁĘBIA, ŚWIAT PODZIEMNY \[sum.\]  
APŠANU – POWRÓZ, POSTRONEK  
APŠENU – WZROST, WZRASTANIE (roślin)  
APŠITŰ – STOSUNEK, ILORAZ  
APŠU lub ENGURRU – OTCHŁAŃ WODNA  
APŠU – OTCHŁAŃ WODNA; MORZE, OCEAN  
APTU – OKNO, OTWÓR OKIENNY  
APU – TRZCINA, SZUWARY, SITOWIE  
AQRABU lub ZUQAQIRU, AKRABU – SKORPION (zarówno zwierzę jak i znak zodiaku)  
\[ARABU\] – N: NARUBU – UCIEKAĆ; zob. NARUBU  
\[jako rzeczownik\] UCIEKANIE, UCIECZKA  
ARABU – ZWIĘKSZAĆ, POTĘGOWAĆ  
\[jako rzeczownik\] ZWIĘKSZANIE, POWIĘKSZANIE, POTĘGOWANIE, NASILANIE  
ARADU lub WARADU(M) (a/i) – SCHODZIĆ, ZSTĘPOWAĆ; ZJEŻDŻAĆ; PRZENIEŚĆ SIĘ; IŚĆ  
\[jako rzeczownik\] SCHODZENIE, ZSTĘPOWANIE; ZJEŻDŻANIE; PRZENOSZENIE SIĘ; CHODZENIE  
ARAHU(M) (a) – SPIESZYĆ SIĘ  
\[jako rzeczownik\] BEZUSTANNE PONAGLANIE  
ŠTN: ŠUTARRUHUM – WCIĄŻ, BEZUSTANNIE PONAGLAĆ  
\[jako rzeczownik\] BEZUSTANNE PONAGLANIE  
ARAKU (i) – BYĆ DŁUGIM  
\[jako rzeczownik\] BYCIE DŁUGIM  
D: URRUKU(M) – PRZEDŁUŻYĆ  
\[jako rzeczownik\] PRZEDŁUŻANIE  
ARALLŰ – ŚWIAT PODZIEMNY, PODZIEMIE \[sum.\]  
ARAMMU – GROBLA  
ARAMU (i) – OKRYWAĆ, PRZYKRYWAĆ  
\[jako rzeczownik\] OKRYWANIE, PRZYKRYWANIE  
ARARU(M) (u) – DRŻEĆ  
\[jako rzeczownik\] DRŻENIE  
GIRRU ARIRU – (mniej więcej:) SYCZĄCY PŁOMIEŃ  
ARBA’U lub IRBITTU, ERBU(M), ERBETTUM – CZTERY  
S.A.: ERBE, ERBET  
ANA ERBIŠU – CZTERY RAZY  
ŠAR ERBETTIM – W CZTERECH STRONACH ŚWIATA; WSZĘDZIE  
ERBET NACMADI – CZWÓRKA, ZAPRZĘG CZTEROKONNY  
ARBŰ lub ARIBU, EREBU, ERIBU – SZARAŃCZA; STADO SZARAŃCZY  
ARBU – POWIĘKSZONY, SPOTĘGOWANY  
ARBUTIŠ – \[młodobab.\] JAK PUSTYNIA, zob. ARBUTU  
ARBUTU – DEWASTACJA, SPUSTOSZENIE, ZNISZCZENIE  
ARBUTU – \[młodobab.\] PUSTYNIA  
ARBUTIŠ – JAK PUSTYNIA  
ARBUTU(M) – UCIECZKA  
ARBUTAM ALAKUM – RZUCAĆ SIĘ DO UCIECZKI  
ARDATU lub WARDATU – PANNA (na wydaniu); KOBIETA; NIEWOLNICA  
WARDAT LILI lub ARDAT LILI – DEMON RODZAJU ŻEŃSKIEGO  
ARDU(M), ARDU(M) lub WARDU(M) – SŁUGA, PAROBEK; NIEWOLNIK \[także sum.\]  
ARDU – PRZENIESIONY; PRZENIESIONY, PRZEMIESZCZONY W DÓŁ, PONIŻONY?  
ARDUTU lub WARDUTU – SŁUŻBA; NIEWOLA  
ARGAMANNU – CZERWONOPURPUROWA WEŁNA \[transkrypcja niepewna?\]  
ARGAMANNU lub TAKILTU, ŠARTU – LUDZKA SKÓRA POKRYTA WŁOSAMI  
ARHIŠ – SZYBKO, PRĘDKO  
ARHIŠAM lub WARHIŠAM – MIESIĘCZNIE  
ARHU – DROGA, ULICA  
S.C.: ARAH  
ARHU(M) – PERZYSPIESZONY, PONAGLONY  
ARHU lub WARHU (wrh) – MIESIĄC  
S.C.: ARAH, l.mn.: ARHE, ARHI, ARHANI  
CÎT ARHI – WSCHÓD KSIĘŻYCA, NÓW  
INA ARAH UM€ (UMÂTE) – W CIĄGU MIESIĄCA  
ŠA ARHI – COMIESIĘCZNIE  
ARIBU, ARIBŰ, ARIBU, ERIBŰ lub ARBU, EREBU, ERIBU – SZARAŃCZA; STADO SZARAŃCZY  
ARIBU – ZWIĘKSZAJĄCY, POTĘGUJĄCY  
ÂRIBU, ÂRIBU – KRUK  
ARIDU – SCHODZĄCY, ZSTĘPUJĄCY; ZJEŻDŻAJĄCY; PRZENOSZĄCY SIĘ; IDĄCY  
ARIHU(M) – SPIESZĄCY SIĘ  
ARIMU – OKRYWAJĄCY, PRZYKRYWAJĄCY  
ARIRU – DRŻĄCY  
ARKA lub ARKI – POTEM, ZA, PO  
ARKATU – ODWROTNA STRONA  
ARKI lub ARKA – POTEM, ZA, PO  
ARKITU lub WARKŰ (M), WARKITU, WARKIUM – TYLNI; PÓŹNIEJSZY; PRZYSZŁY  
ANA WARKÂT UMI – DLA PRZYSZŁOŚCI  
WARKIKA – PO TOBIE  
IŚTU WARKIŠU – ODKĄD JEST MARTWY  
ARKITU – \[jako abstrakt\] PRZYSZŁOŚĆ  
ARKU – PÓŹNY, SPÓŹNIONY; PÓŹNIEJSZY; POWTÓRNY, PONOWNY  
ARKU – ZIELONY  
ARKATU – ZIELONA  
\[jako abstrakt\] ZIELEŃ  
ARKU(M), ARKU(M) – DŁUGI; ZBYT DŁUGI  
ARKATU(M), ARKATU(M) – DŁUGA, ZBYT DŁUGA  
\[jako abstrakt\] (ZNACZNA) DŁUGOŚĆ  
ARMU – KOZA GÓRSKA  
ARMU – OKRYTY, PRZYKRYTY  
ARNU lub ANNU – PRZEWINIENIE, GRZECH, PRZESTĘPSTWO; GRZYWNA, KARA  
S.C.: ANAN  
BEL ARNI – GRZESZNIK, WINOWAJCA  
ARQATU – ZIELONA, ŻÓŁTA (lub w dowolnym odcieniu pomiędzy zielonym a żółtym); ZIELEŃ, ŻÓŁĆ (o kolorze)  
ARQU lub WARQU – ZIELONY; ŻÓŁTY; nazwa każdego odcienia z przedziału kolorów pomiędzy zielonym a żółtym  
ARŰ \[nowobab.\] lub WARŰ (M) (wru) – PROWADZIĆ  
\[jako rzeczownik\] PROWADZENIE  
GTN: PROWADZIĆ CIĄGLE; WCIĄŻ SPROWADZAĆ  
\[jako rzeczownik\] PROWADZENIE BEZ PRZERWY; STAŁE SPROWADZANIE  
Š: ŠURŰ – KAZAĆ PROWADZIĆ; KAZAĆ SPROWADZIĆ  
\[jako rzeczownik\] NAKAZANIE PROWADZENIA; ZLECENIE SPROWADZENIA  
ARU – WNIKAĆ, WTARGNĄĆ, WEDRZEĆ SIĘ  
\[jako rzeczownik\] WNIKANIE, WDZIERANIE SIĘ  
AŠAR LA ARI – NIEDOSTĘPNA MIEJSCOWOŚĆ  
ARŰ (’r’) – OKRĄŻAĆ; OCHRANIAĆ, OBRONIĆ  
\[jako rzeczownik\] OKRĄŻANIE; OCHRANIANIE, OCHRONA, BRONIENIE, OBRONA  
ARU – GAŁĄŹ Z LIŚĆMI, GAŁĄŹ Z OWOCAMI  
ARŰ lub ERŰ – ORZEŁ  
ARŰ – PROWADZĄCY  
ARŰ (’r’) – OKRĄŻAJĄCY; OCHRANIAJĄCY, BRONIĄCY  
ARU – WNIKAJĄCY, USIŁUJĄCY WTARGNĄĆ  
ASAKKU – CHOROBA, CIERPIENIE; CIEMNOŚĆ  
ASAKKU – DEMON \[sum.\]  
ASKUPPATU, ASKUPPATU lub ASKUPPU – PŁYTA; PRÓG  
ASKUPPU lub ASKUPPATU – j.w.  
ASLU(M) – OWCA, OWCA SZLACHETNEJ RASY  
ASMARŰ – CIĘŻKA KOPIA  
ASPŰ – JASPIS  
ASSINNU – CHŁOPIEC SŁUŻĄCY ROZKOSZY; EUNUCH; (także:) AKTOR \[słowo obcego pochodzenia\]  
ASU – NIEDŹWIEDŹ  
ASU – MIRT  
ASŰ, ÂSŰ – LEKARZ \[sum.\]  
AŠABU(M) lub WAŠABU(M) (a/i) – SIADAĆ; ZASIADAĆ (w sądzie); MIESZKAĆ, OSIEDLAĆ SIĘ; PRZEBYWAĆ, ZNAJ-  
DOWAĆ SIĘ  
\[jako rzeczownik\] SIADANIE; ZASIADANIE (w sądzie); MIESZKANIE, OSIEDLANIE SIĘ; PRZEBYWANIE  
Š: ŠUŠUBU(M) – POSADZIĆ, KAZAĆ SIADAĆ; OSIEDLAĆ  
\[jako rzeczownik\] POSADZENIE; KAZANIE SIADAĆ; OSIEDLANIE  
INA CER x WAŠABU – SIADAĆ NA x \[możliwe też konstrukcje przechodnie\]  
INA HARRANI (W)AŠIB – JEST W PODRÓŻY HANDLOWEJ, PODRÓŻUJE W CELACH HANDLOWYCH  
INA PAŠŠURI ŠUŠUBU – KAZAĆ UMIEŚCIĆ NA TABLICZCE, W DOKUMENCIE  
AŠAGU – TARNINA, CIERŃ, TARŃ  
AŠAMŠATU lub AŠAMŠUTU – WICHER BURZOWY  
AŠAMŠUTU lub AŠAMŠATU – j.w.  
AŠAR – GDZIE, DOKĄD; GDZIEKOLWIEK, DOKĄDKOLWIEK; ZAMIAST  
AŠAR IŠTEN – W JEDNYM MIEJSCU  
AŠAREDDU lub AŠARIDU, AŠARIDDU, AŠAREDU – PIERWSZY, PIERWSZORZĘDNY  
AŠAREDU lub R€ŠTU – GŁÓWNY, PIERWSZY, NAJLEPSZY  
AŠAREDU lub GUGALLU – GŁÓWNY  
AŠAREDU lub AŠARIDU, AŠARIDDU, AŠAREDDU – PIERWSZY, PIERWSZORZĘDNY  
AŠAREDU(M) – NAJWYŻSZY RANGĄ, NAJWAŻNIEJSZY  
AŠAREDUTU(M) – NAJWYŻSZA RANGA, POZYCJA, MIEJSCE PIERWSZEGO  
AŠAREDUTAM ALAKUM – ZDOBYĆ NAJWYŻSZĄ RANGĘ  
AŠARIDDU lub AŠARIDU, AŠAREDDU – PIERWSZY, PIERWSZORZĘDNY  
AŠARIDDU lub AŠARÎDU – PIERWSZY RANGĄ, ZNAKOMITY, ZNAMIENITSZY; SZLACHETNIE, SZLACHETNIEJ  
URODZONY; KSIĄŻĘ; GENIUSZ  
r.ż. AŠARITTU, S.C. AŠARID  
AŠARIDU lub AŠAREDU, AŠARIDDU, AŠAREDDU – PIERWSZY, PIERWSZORZĘDNY  
AŠARÎDU lub AŠARIDDU – PIERWSZY RANGĄ, ZNAKOMITY, ZNAMIENITSZY; SZLACHETNIE, SZLACHETNIEJ  
URODZONY; KSIĄŻĘ; GENIUSZ  
r.ż. AŠARITTU, S.C. AŠARID  
AŠARITTU – PIERWSZA RANGĄ, ZNAKOMITA, ZNAMIENITSZA; SZLACHETNIE, SZLACHETNIEJ URODZONA;  
KSIĘŻNA; zob. AŠARIDU  
AŠARITTU – PIERWSZEŃSTWO (w systemie rang); SZLACHETNE URODZENIE  
AŠARŠANI – GDZIE INDZIEJ  
\[AŠARU\] lub EŠERU(M) (i) – BYĆ W PORZĄDKU, BYĆ DOKŁADNIE; IŚĆ WPROST, IŚĆ PROSTO; RZUCAĆ SIĘ (na ko-  
goś, coś np. z bronią); (o ziemiopłodach) ROSNĄĆ, ROZWIJAĆ SIĘ; BYĆ PROSTYM, BYĆ RÓWNYM; WY-  
PROSTOWAĆ; BŁOGOSŁAWIĆ  
\[jako rzeczownik\] BYCIE W PORZĄDKU; ZACHOWYWANIE DOKŁADNOŚCI; CHODZENIE PROSTO, NA  
WPROST; RZUCANIE SIĘ (z bronią); ROŚNIĘCIE, WZRASTANIE, ROZWIJANIE SIĘ (o uprawach); BYCIE  
PROSTYM, RÓWNYM; PROSTOWANIE; BŁOGOSŁAWIENIE  
Š: ŠUŠURU(M) – DOPROWADZAĆ DO PORZĄDKU, POKRZEPIAĆ  
\[jako rzeczownik\] DOPROWADZANIE DO PORZĄDKU, POKRZEPIANIE  
ŠT: ŠUTEŠURU(M) – KAZAĆ BYĆ PRAWEM, USTANAWIAĆ PRAWO; ZAPROWADZAĆ ŁAD, UTRZY-  
MYWAĆ W PORZĄDKU  
\[jako rzeczownik\] USTANOWIENIE PRAWEM, ZAPROWADZANIE ŁADU, UTRZYMYWANIE PORZĄDKU  
EŠERUM ANA PANI – PODCHODZIĆ KU  
HARRANA EŠERU lub HARRANA ŠUTEŠURU – MASZEROWAĆ PROSTO (PRZED SIEBIE)  
AŠARU (a/u) – PRZEPROWADZAĆ INSPEKCJĘ, DOGLĄDAĆ, NADZOROWAĆ, DOKONYWAĆ PRZEGLĄDU, LUSTRO-  
WAĆ; MIEĆ PIECZĘ, ZAJMOWAĆ SIĘ (versorgen)  
\[jako rzeczownik\] PRZEPROWADZANIE INSPEKCJI, DOGLĄDANIE, KONTROLOWANIE, NADZOROWA-  
NIE, DOKONYWANIE PRZEGLĄDU, LUSTROWANIE; SPRAWOWANIE PIECZY; ZAJMOWANIE SIĘ  
AŠARU – BYĆ RÓWNYM; BYĆ POMYŚLNYM, SZCZĘŚLIWYM; zob. EŠERU  
\[jako rzeczownik\] BYCIE RÓWNYM; BYCIE POMYŚLNYM, BYCIE SZCZĘŚLIWYM  
AŠAŠU (a/u; u) – BYĆ STROSKANYM, ZMARTWIONYM; OPALAĆ SIĘ (na słońcu)  
\[jako rzeczownik\] BYCIE STROSKANYM, ZMARTWIONYM; OPALANIE SIĘ (na słońcu)  
D: UŠŠUŠU – MĘCZYĆ, ZADRĘCZYĆ, DRĘCZYĆ  
\[jako rzeczownik\] MĘCZENIE, DRĘCZENIE  
DT: UTAŠŠUŠU – BYĆ UNIESZCZĘŚLIWIONYM, DOPROWADZONYM DO CIERPIENIA  
\[jako rzeczownik\] BYCIE UNIESZCZĘŚLIWIONYM, BCYIE DOPROWADZONYM DO CIERPIENIA  
AŠBU(M) – POSADZONY; WŁĄCZONY W SKŁAD SĄDU; ZAMIESZKAŁY, OSIEDLONY  
AŠBU lub AŠÎBU – ZAMIESZKAŁY  
l.mn. AŠBŰTI  
AŠÎBU lub AŠBU – j.w.  
ÂŠIBU – MIESZKAJĄCY, ZAMIESZKUJĄCY; MIESZKANIEC  
s.c. AŠIB, l. mn. AŠIBŰTI, s.c. l.mn. ÂŠIBŰT  
AŠIBU(M) – SIEDZĄCY; ZASIADAJĄCY (w sądzie); MIESZKAJĄCY, OSIEDLAJĄCY SIĘ; PRZEBYWAJĄCY, ZNAJDUJĄCY SIĘ  
ÂŠIPU – ZAKLINACZ  
AŠIRTU lub AŠRU – ŚWIĄTYNIA (właściwie:) MIEJSCE, GDZIE BÓG OKAZUJE ŁASKĘ, SPRAWIEDLIWOŚĆ; (potem:)  
MIEJSCE, MIEJSCOWOŚĆ, MIEJSCE ZAMIESZKANIA; TEREN, OBSZAR, DOKŁADNE POŁOŻENIE; (jako  
przyimek:) TAM, TAM GDZIE, W, U, PRZY, DOKĄD itd.  
s.c. AŠAR  
AŠIRU – INSPEKTOR, NADZORCA, LUSTRATOR, PRZEPROWADZAJĄCY INSPEKCJĘ, DOGLĄDAJĄCY; MAJĄCY PIECZĘ,  
ZAJMUJĄCY SIĘ  
AŠIŠU – OPALAJĄCY SIĘ (na słońcu), PLAŻOWICZ  
AŠNÂNU – JĘCZMIEŃ; ZIARNO  
AŠQULALU(M) – CHMURA SZTORMOWA; WICHER  
AŠRU lub AŠIRTU – ŚWIĄTYNIA (właściwie:) MIEJSCE, GDZIE BÓG OKAZUJE ŁASKĘ, SPRAWIEDLIWOŚĆ; (potem:)  
MIEJSCE, MIEJSCOWOŚĆ, MIEJSCE ZAMIESZKANIA; TEREN, OBSZAR, DOKŁADNE POŁOŻENIE; (jako  
przyimek:) TAM, TAM GDZIE, W, U, PRZY, DOKĄD itd.  
s.c. AŠAR  
AŠAR IŠTEN – W JEDNYM MIEJSCU  
AŠRU – LUSTROWANY, KONTROLOWANY, DOGLĄDANY, ZNAJDUJĄCY SIĘ POD (czyjąś) PIECZĄ  
AŠRU – RÓWNY; POMYŚLNY, SZCZĘŚLIWY  
AŠŠATU lub ALTU – NIEWIASTA, KOBIETA  
AŠŠATU(M), AŠŠATU(M), AŠŠATU(M) lub HIRTU – MAŁŻONKA, ŻONA (pierwsza)  
AŠŠU – STROSKANY, ZMARTWIONY; OPALONY (na słońcu)  
AŠŠU(M), AŠŠU(M) – (przyimek:) W SPRAWIE, PONIEWAŻ, Z POWODU, ZE WZGLĘDU NA; (z bezokolicznikiem:) ABY,  
ŻEBY; (spójnik:) PONIEWAŻ, GDYŻ (w zdaniach przyczynowych); zob. AŠŠUM  
AŠŠUM lub AŠŠUMI – Z POWODU, ZE WZGLĘDU NA, DLA, CO DO, W SPRAWIE; DLATEGO ŻE, BO, PONIEWAŻ; zob.  
AŠŠU(M)  
AŠŠUMIJA – Z MOJEGO POWODU, ZE WZGLĘDU NA MNIE, DLA MNIE, PRZEZE MNIE  
AŠŠUMI \[staroasyryjskie\] lub AŠŠUM – j.w.  
AŠŠUR – ASZSZUR, ASZUR \[imię boga)  
AŠŠUR-BANI-APLI – ASZURBANIAPLI, ASSURBANIPAL \[imię króla Asyrii\]  
AŠŠURÎUM lub AŠŠURŰM – ASYRYJCZYK  
AŠŠURITUM – ASYRYJKA  
AŠŠURŰM lub AŠŠURÎUM – ASYRYJCZYK  
AŠTAPIRU – CZELADŹ, SŁUŻBA  
AŠŰ – ZWIERZYNA, ZWIERZĘTA  
AŠUHU, AŠUHU – ŚWIERK, JODŁA, RODZAJ CEDRA  
AŠUR – ASYRIA  
ATABKU, ATABKU – UPROWADZANIE, PĘDZENIE; zob. ABAKU  
ATADDURUM – \[bezok. GTN\] BANIE SIĘ WCIĄŻ NA NOWO, STALE; zob. ADARUM  
ATALKUM – \[bezok. GT\] ODCHODZENIE, WYRUSZANIE W DROGĘ; zob. ALAKUM  
ATALLUKUM – \[bezok. GTN\] PRZECHADZANIE SIĘ, SPACEROWANIE; DŁUGOTRWAŁE CHODZENIE, MASZERO-  
WANIE; zob. ALAKUM  
ATALŰ lub ADÂRU, ATTALŰ – ZAĆMIENIE (np. słońca) \[sum.\]  
ATANU – OŚLICA (r.ż.\]  
ATAPPU – RÓW (Graben)  
ATAPPULU(M) – OPIEKA, ODPOWIEDZIALNOŚĆ; \[bezokolicznik Gtn od APALU\]  
ATARU(M) – zob. WATARU(M)  
ATMŰ – SŁOWO  
ATRU lub WATRU – PRZEWYŻSZAJĄCY, NADZWYCZAJNY, NIEZWYKŁY; ZBĘDNY; (Amm.:) DODATKOWY  
KIMA ATARTIMMA – „WEDŁUG ZYSKU”, „STOSOWNIE DO ZYSKU”, „TYLE (Z TEGO) BYŁO DO DYS-  
POZYCJI”  
ATTA, ATTA – TY \[zaimek 2 os. r. m.\]; zob. ATTI  
Odmiana: N: l. poj. ATTA – TY l. mn. ATTUNU – WY  
G/A: KÂTI lub KÂTA – CIEBIE KUNUTI – WAS  
D: KAŠI(M) (lub KAŠA) – TOBIE KUNUŠI(M) – WAM  
ATTALŰ lub ADÂRU, ATALŰ – ZAĆMIENIE (np. słońca) \[sum.\]  
ATTI – TY \[zaimek 2 os. r. ż.\]; zob. ATTA  
Odmiana: N: l. poj. ATTI – TY l. mn. ATTINA – WY  
G/A: KÂTI – CIEBIE KINÂTI – WAS  
D: KÂŠI(M) – TOBIE, CI KINAŠI(M) – WAM  
ATTINA – WY \[zaimek 2 os. l. mn. r. ż.\]; zob. ATTI  
ATTŰ – \[(wraz z sufigowanym zaimkiem) wyrażenie akcentujące daną osobę\]  
ATTUNU – WY \[zaimek 2 os. l. mn. r. m.\]; zob. ATTA  
\[ATŰ\] – WYPATRYWAĆ, ŚLEDZIĆ  
\[jako rzeczownik\] WYPATRYWANIE, ŚLEDZENIE, ŚLEDZTWO, DOCHODZENIE  
ATŰ – SZPIEG, SZPICEL (zwłaszcza przebywający przy bramie); zob. WATU  
ATUNU – PIEC DO WYPALANIA  
AŢATTU – MIEJSCE ZAMIESZKANIA; (w liczbie mnogiej:) KRAJE; \[połączenie z „Ţ” niepewne\]  
l. mn. AŢNATI  
AŢNATI – KRAJE; zob. AŢATTU  
AWATU(M), AWATU(M) lub ABUTU, AMATU, AMUTU, AWUTU(M) – SŁOWO, WYRAŻENIE, WYPOWIEDŹ, ROZ-  
KAZ; RZECZ, SPRAWA, OKOLICZNOŚĆ; WYROCZNIA  
S.C. AWAT lub AMAT, l. mn. AWATI lub AMATI  
BEL AWATIM – OSKARŻYCIEL, PRZECIWNIK W PROCESIE  
AWATAM BATAQUM – ZAŁATWIAĆ SPRAWĘ \[staroasyr.\]  
INA AMAT lub INA QIBIT – WEDLE POLECENIA, WEDLE ROZKAZU  
KAL AMATUŠA – \[w pewnym tekście:\] WSZYSTKIE JEJ WYBRYKI  
AWELTU – OBYWATELKA (wg kodeksu Hammurabiego); zob. AWELU(M)  
AWELU lub AMELU – MĘŻCZYZNA; CZŁOWIEK; SŁUGA  
AWELUTU – LUDZKOŚĆ  
AWILU(M) \[starobab.\] lub AMELU, A’ILU, AMILU – CZŁOWIEK; WOLNY OBYWATEL (posiadający ziemię)  
AWILUTU(M) lub AMELUTU, AMELUTTU – LUDZKOŚĆ; CZŁOWIEK  
AWŰ(M) – MÓWIĆ  
\[jako rzeczownik\] MÓWIENIE, MOWA  
AWUTU(M) lub AWATU(M), ABUTU, AMATU, AMUTU – SŁOWO, WYRAŻENIE, WYPOWIEDŹ, ROZKAZ; RZECZ,  
SPRAWA, OKOLICZNOŚĆ; WYROCZNIA \[staroasyr.\]  
S.C. AWAT lub AMAT, l. mn. AWATI lub AMATI  
BEL AWATIM – OSKARŻYCIEL, PRZECIWNIK W PROCESIE  
AWATAM BATAQUM – ZAŁATWIAĆ SPRAWĘ \[staroasyr.\]  
INA AMAT lub INA QIBIT – WEDLE POLECENIA, WEDLE ROZKAZU  
KAL AMATUŠA – \[w pewnym tekście:\] WSZYSTKIE JEJ WYBRYKI  
AZZARU lub ACCARU – KOT BŁOTNY

B  
BA’ATU – „NOCNA CEREMONIA”  
BA’IRU(M), BA’IRU(M) – RYBAK; MYŚLIWY \[imiesłów od BARUM\]  
BÂ’U, BÂ’U (bw’) – WCHODZIĆ, WSTĘPOWAĆ, PRZEDOSTAWAĆ SIĘ; ZWIEDZAĆ; IŚĆ WZDŁUŻ, OBOK  
\[jako rzeczownik\] WCHODZENIE, WSTĘPOWANIE, PRZEDOSTAWANIE SIĘ; ZWIEDZANIE; CHODZENIE  
WZDŁUŻ, CHODZENIE OBOK  
Š/D: RZUCIĆ SIĘ; PRZECHODZIĆ  
\[jako rzeczownik\] RZUCENIE SIĘ; PRZECHODZENIE  
Odmiana w 3 os. l. p. i tryb rozkazujący:  
Prs Prt Pf TR  
G: IBÂ IBA’ IBTA’ BA’  
Š: UŠBA’ UŠBI’ ŠUBI’  
BA’U – \[z Akk. miejsca\] PRZECHODZIĆ, PRZECIĄGAĆ, PRZYCIĄGAĆ NAD  
\[jako rzeczownik\] PRZECHODZENIE, PRZECIĄGANIE, PRZYCIĄGANIE NAD  
\[BA’Ű\] – zob. BU”U  
BABALU(M), BABALU(M) lub WABALUM – PRZYNIEŚĆ, NIEŚĆ \[Prs i Prt G nie używane\], DONOSIĆ, PRZYNOSIĆ  
\[jako rzeczownik\] PRZYNOSZENIE, PRZYNIESIENIE, NOSZENIE, NIESIENIE, TRANSPORTOWANIE ?  
N: NABBULUM, NABBULU(M) (?) – BYĆ PRZYNIESIONYM  
\[jako rzeczownik\] BYCIE PRZYNIESIONYM  
BABANŰ – ZNAJDUJĄCY SIĘ NA ZEWNĄTRZ, ZEWNĘTRZNY  
BABILU, BABILU – PRZYNOSZĄCY, NOSZĄCY, DONOSZĄCY; TRAGARZ? DOSTAWCA? SŁUGA?  
BABILU – BABILON \[nazwa głównego miasta Babilonii\]  
QERBUM BABILI – W BABILONIE  
BABLU, BABLU – PRZYNIESIONY, NIESIONY, DONIESIONY  
BABTU – (właściwie:) BRAMA; DZIELNICA MIASTA, OKRĘG BRAMY (okręg administracyjny)  
BABU – DRZWI  
BABU(M) – BRAMA, WEJŚCIE; MASYW BRAMY (budowla będąca bramą)  
BAB EKALLIM – (dosłownie:) BRAMA PAŁACU; ? \[jakaś część wątroby zwana „BRAMĄ PAŁACU”\]  
BAHULATU lub BAULATU – DRUŻYNA, ŻOŁNIERZE, PODDANI (należący do PANA – BELU) \[rzeczownik w l.mn.\]  
BAKA’U(M) \[asyr.\] lub BAKŰ (M) (i) – PŁAKAĆ, OPŁAKIWAĆ  
\[jako rzeczownik\] PŁAKANIE, OPŁAKIWANIE  
ANA x BAKŰ – OPŁAKIWAĆ x (kogoś lub coś)  
BAKŰ(M), BAKŰ(M) (i) lub BAKA’U(M) – j.w.  
BAKŰ(M), BAKŰ(M) – OPŁAKANY  
BAKŰ(M), BAKŰ(M) – PŁACZĄCY, OPŁAKUJĄCY  
BALACU (i) – D: BULLUCU – ZEZOWAĆ (wyłupiastymi oczyma), PATRZEĆ ZEZEM; WYGLĄDAĆ, WYPATRYWAĆ (ANA  
x – ZA kimś, czymś)  
\[jako rzeczownik\] PATRZENIE ZEZEM, ZEZOWANIE; WYGLĄDANIE, WYPATRYWANIE  
BALALU (a/u) – MIESZAĆ, PRZEMIESZAĆ  
\[jako rzeczownik\] MIESZANIE, PRZEMIESZANIE, POMIESZANIE, WYMIESZANIE  
DT: BUTALLULU – BYĆ POPLAMIONYM, POWALANYM, POBRUDZONYM (INA x – CZYMŚ)  
\[jako rzeczownik\] BYCIE POPLAMIONYM, BYCIE POWALANYM, BYCIE POBRUDZONYM  
BALAŢU – ŻYĆ; OŻYWIAĆ  
\[jako rzeczownik\] ŻYCIE; OŻYWIANIE  
BALAŢU(M), BALAŢU(M) (u; asyr. a) – ŻYĆ; BYĆ ZDROWYM; ZDROWIEĆ, ODZYSKIWAĆ ZDROWIE; DAWAĆ ŻYCIE,  
OŻYWIAĆ  
\[jako rzeczownik\] ŻYCIE; BYCIE ZDROWYM; ZDROWIENIE, ODZYSKIWANIE ZDROWIA, DOCHODZENIE  
DO SIEBIE; DAWANIE ŻYCIA, OŻYWIANIE  
D: BULLUŢU(M), BULLUŢU(M) ZACHOWYWAĆ (SIĘ) PRZY ŻYCIU, PODTRZYMYWAĆ ŻYCIE  
\[jako rzeczownik\] ZACHOWYWANIE PRZY ŻYCIU, PODTRZYMYWANIE ŻYCIA  
BALAŢU(M) – ŻYCIE  
BALAT NAPŠATI – ZDROWIE \[nowobab.\]  
BALI lub BALU(M) – BEZ, BEZ (zezwolenia, zgody)  
INA x BALU – BEZ (ZEZWOLENIA) x  
BALIKA – POZA TOBĄ  
BALILU – MIESZAJĄCY  
BALLU – POMIESZANY, WYMIESZANY, ZAMIESZANY  
BALIŢU(M), BALIŢU(M) – ŻYJĄCY, EGZYSTUJĄCY; ZDROWIEJĄCY, ODZYSKUJĄCY ZDROWIE; DAJĄCY ŻYCIE,  
OŻYWIAJĄCY; OZDROWICIEL? UZDROWICIEL?  
BALTU – ? \[jakieś ciernie, jakaś ciernista roślina\]  
BALTU – HONOR  
BALŢU(M) – ŻYWY; CAŁY, NIEUSZKODZONY, ZACHOWANY  
BALŢUTU – STAN BYCIA ŻYWYM, BYCIE ŻYWYM  
\[BALU\] – \[składnik wyrażenia\]  
ŠIMTU UBILŠU – PRZEZNACZENIE PORWAŁO GO  
BALU (a) – PROSIĆ  
\[jako rzeczownik\] PROSZENIE  
BALŰ (i) – SPEŁZNĄĆ NA NICZYM, ROZWIEWAĆ SIĘ  
\[jako rzeczownik\] SPEŁZANIE NA NICZYM, ROZWIEWANIE SIĘ  
D: BULLŰ (często o ogniu) UGASIĆ  
\[jako rzeczownik\] GASZENIE  
BALŰ – ROZWIANY, SPEŁZŁY NA NICZYM, NIEUDANY  
BALŰ – ROZWIEWAJĄCY, NIWECZĄCY, UDAREMNIAJĄCY  
BALU(M), BALU(M) lub BALI – BEZ (zezwolenia, zgody)  
INA x BALU – BEZ (ZEZWOLENIA) x  
BALIKA – POZA TOBĄ  
BAMÂTU – PASTWISKO; (mała) WYŻYNA  
BANA’UM lub BANŰ (M) (i) – BUDOWAĆ, WYKONAĆ, PRODUKOWAĆ, TWORZYĆ, WZNOSIĆ, ZROBIĆ; ZAKOŃ-  
CZYĆ; WYDOBYWAĆ; RODZIĆ; WYKAZAĆ, UJAWNIĆ; PLANOWAĆ, PRZEWIDYWAĆ  
\[jako rzeczownik\] BUDOWANIE, WYKONANIE, PRODUKOWANIE, TWORZENIE, WZNOSZENIE, ROBIE-  
NIE; ZAKOŃCZENIE; WYDOBYWANIE; RODZENIE; WYKAZANIE, UJAWNIENIE; PLANOWANIE, PRZE-  
WIDYWANIE  
N: NABNŰ(M) – BYĆ WYKONANYM, ZBUDOWANYM, WZNIESIONYM; BYĆ URODZONYM  
\[jako rzeczownik\] BYCIE WYKONANYM, ZBUDOWANYM, WZNIESIONYM; BYCIE URODZONYM  
D: BUNNŰ (M) – (jak w G ale intensywnie); POKRYWAĆ ORNAMENTEM, UPIĘKSZAĆ  
\[jako rzeczownik\] POKRYWANIE ORNAMENTEM, UPIĘKSZANIE  
BANIUM – TWORZĄCY  
BÂNŰ – TWÓRCA; BUDOWNICZY; RODZIC  
BANŰ(M), BANŰ(M), BANŰ(M) (i) lub BANA’UM – BUDOWAĆ, WYKONAĆ, PRODUKOWAĆ, TWORZYĆ, WZNO-  
SIĆ, ZROBIĆ; POWODOWAĆ; ZAKOŃCZYĆ; WYDOBYWAĆ; RODZIĆ; WYKAZAĆ, UJAWNIĆ; PLA-  
NOWAĆ, PRZEWIDYWAĆ  
\[jako rzeczownik\] BUDOWANIE, WYKONANIE, PRODUKOWANIE, TWORZENIE, WZNOSZENIE, ROBIE-  
NIE; ZAKOŃCZENIE; WYDOBYWANIE; RODZENIE; WYKAZANIE, UJAWNIENIE; PLANOWANIE, PRZE-  
WIDYWANIE  
N: NABNŰ (M), NABNŰ (M), NABNŰ (M) – BYĆ WYKONANYM, ZBUDOWANYM, WZNIESIONYM; BYĆ  
URODZONYM  
\[jako rzeczownik\] BYCIE WYKONANYM, ZBUDOWANYM, WZNIESIONYM; BYCIE URODZONYM  
D: BUNNŰ (M), BUNNŰ (M), BUNNŰ (M) – (jak w G ale intens.); POKRYWAĆ ORNAMENTEM, UPIĘKSZAĆ  
\[jako rzeczownik\] POKRYWANIE ORNAMENTEM, UPIĘKSZANIE  
BANŰ(M) (i) – BYĆ PRZYJEMNYM  
\[jako rzeczownik\] BYCIE PRZYJEMNYM  
PANU IBANNŰ – TWARZ ZAJAŚNIEJE? (w obecności przyjaciela) \[nowobab.\]  
BANŰ(M) – PIĘKNY, DOBRY, SZLACHETNY  
LA BANÂTI – FATALNY, BRZYDKI, ZGUBNY (czyn)  
BANŰ(M), BANŰ(M), BANŰ(M), BANŰ (M) – BUDOWANY, ZBUDOWANY, WYBUDOWANY, WYKONANY, WZNIE-  
SIONY, UTWORZONY, ZROBIONY; SPOWODOWANY; ZAKOŃCZONY; WYDOBYTY; URODZONY;  
UJAWNIONY, WYKAZANY; ZAPLANOWANY, PRZEWIDZIANY  
BANŰ(M), BANŰ(M), BANŰ(M), BANŰ (M) – BUDUJĄCY, WYKONUJĄCY, WZNOSZĄCY, TWORZĄCY, ROBIĄCY; POWO-  
DUJĄCY; KOŃCZĄCY; WYDOBYWAJĄCY; RODZĄCY;UJAWNIAJĄCY, WYKAZUJĄCY; PLANUJĄCY, PRZEWI-  
DUJĄCY  
BAQAMU(M) (a/u) – STRZYC  
\[jako rzeczownik\] STRZYŻENIE  
Š: ŠUBQUMU(M) – KAZAĆ STRZYC  
\[jako rzeczownik\] NAKAZANIE STRZYŻENIA  
BAQARU(M) (a/u) – ZAPRZECZYĆ, ZAKWESTIONOWAĆ; ODMÓWIĆ (np. prawa); WNOSIĆ SPRZECIW; WNOSIĆ O  
WINDYKACJĘ, DOCHODZIĆ WŁASNOŚCI (na kimś)  
\[jako rzeczownik\] ZAPRZECZENIE, ZAKWESTIONOWANIE, PODWAŻENIE; ODMÓWIENIE (prawa);  
WNIESIENIE SPRZECIWU, ZAWETOWANIE; WYSTĄPIENIE O WINDYKACJĘ, DOCHODZENIE WŁASNO-  
ŚCI (na kimś)  
AŠAMMA IBTAQRUNINNI – KUPIŁEM, A NASTĘPNIE ONI DOCHODZILI NA MNIE WŁASNOŚCI  
BAQIMU(M) – STRZYŻĄCY, POSTRZYGACZ  
BAQIRU(M) – KWESTIONUJĄCY, ZAPRZECZAJĄCY, ODMAWIAJĄCY (prawa); WNOSZĄCY SPRZECIW; DOCHODZĄCY  
WŁASNOŚCI, WNOSZĄCY O WINDYKACJĘ  
BAQMU(M) – OSTRZYŻONY  
BAQQU(M) – KOMAR  
BAQRU – SPRZECIW, PROTEST; WINDYKACJA  
BAQRU(M) – ZAPRZECZONY, ZAKWESTIONOWANY, OPROTESTOWANY; PODDANY WINDYKACJI, BĄDĄCY  
PRZEDMIOTEM DOCHODZENIA WŁASNOŚCI  
BARANŰ – BUNTUJĄCY SIĘ  
BARAQU (i) – BŁYSKAĆ, BŁYSZCZEĆ; (także przechodnio:) BŁYSZCZEĆ W DÓŁ  
\[jako rzeczownik\] BŁYSKANIE, BŁYSZCZENIE; BŁYSZCZENIE W DÓŁ  
BARARITU lub BARARTU – PIERWSZA NOCNA WARTA; CZAS PIERWSZEJ NOCNEJ WARTY (18-22)  
BARARTU lub BARARITU – j.w.  
BARARU (u) – BYĆ JASNYM  
\[jako rzeczownik\] BYCIE JASNYM  
BARBARU lub AHŰ – ZWIERZĘ DRAPIEŻNE, WILK? HIENA?  
BARBARU(M) – WILK  
BARIQU – BŁYSKAJĄCY, BŁYSZCZĄCY; BŁYSZCZĄCY W DÓŁ  
BARRU – JASNY  
BARTU(M) – POWSTANIE, BUNT  
\[BARU\] – zob. BURRU  
BÂRU – ŁAPAĆ  
\[jako rzeczownik\] ŁAPANIE  
BARŰ(M), BARŰ(M) (i) (brj) – WIDZIEĆ; PATRZEĆ, SPOGLĄDAĆ; PRZYGLĄDAĆ SIĘ, SPRAWDZAĆ, WYPRÓBOWY-  
WAĆ; KRYTYCZNIE ROZPATRYWAĆ, BADAĆ; PRZEWIDYWAĆ; WRÓŻYĆ  
\[jako rzeczownik\] WIDZENIE; PATRZENIE, SPOGLĄDANIE; PRZYGLĄDANIE SIĘ, SPRAWDZANIE,  
PRÓBOWANIE; KRYTYCZNE ROZPATRYWANIE, BADANIE; PRZEWIDYWANIE; WRÓŻENIE  
Š: ŠUBRŰ(M) – KAZAĆ PATRZEĆ; POKAZYWAĆ, UKAZYWAĆ, ODSŁANIAĆ, UJAWNIAĆ; POSTANA-  
WIAĆ?  
\[jako rzeczownik\] SKŁONIENIE DO PATRZENIA; POKAZYWANIE, UKAZYWANIE, ODSŁANIANIE, UJAW-  
NIANIE; POSTANAWIANIE?  
ŠT: ŠUTEBRUM – BYĆ PRZEJRZYSTYM  
\[jako rzeczownik\] BYCIE PRZEJRZYSTYM  
BARŰ(M), BARŰ(M) (bri) – WIDZIANY, OGLĄDANY; SPRAWDZANY, WYPRÓBOWANY, KRYTYCZNIE ROZPATRZONY,  
BADANY, ZBADANY; PRZEWIDZIANY; WYWRÓŻONY  
BARŰ(M), BARŰ(M) (bri) – WIDZĄCY, PATRZĄCY, OGLĄDAJĄCY; PRZYGLĄDAJĄCY SIĘ, SPRAWDZAJĄCY, PRÓBUJĄCY,  
KRYTYCZNIE ROZPATRUJĄCY, BADAJĄCY; PRZEWIDUJĄCY; WRÓŻĄCY  
BARŰ, BARŰ – BADACZ OFIAR; WRÓŻBITA, JASNOWIDZ; KAPŁAN, MAG  
BARŰ(M) lub BERŰ (M) (br’) – GŁODOWAĆ, BYĆ GŁODNYM  
\[jako rzeczownik\] GŁODOWANIE, BYCIE GŁODNYM  
BARŰ(M) (br’) – GŁODNY  
BARŰ(M) (br’) – GŁODUJĄCY  
BÂRU(M) (b’r) – ŁOWIĆ (ryby)  
\[jako rzeczownik\] ŁOWIENIE (ryb)  
BARU(M) (b’r) – ZŁOWIONY  
BARUTU – BADANIE OFIAR  
BAŠA’U lub BAŠŰ (M) (i) – BYĆ, ISTNIEĆ, ZNAJDOWAĆ SIĘ; BYĆ TU  
\[jako rzeczownik\] BYCIE, ISTNIENIE, ZNAJDOWANIE SIĘ; BYCIE TU  
N: NABŠU(M) – POWSTAWAĆ, ZDĄŻAĆ KU ISTNIENIU; BYĆ POWOŁANYM DO ISTNIENIA, BYĆ ZA-  
ISTNIAŁYM  
\[jako rzeczownik\] POWSTAWANIE, ZDĄŻANIE KU ISTNIENIU; BYCIE POWOŁANYM DO ISTNIENIA,  
BYCIE ZAISTNIAŁYM  
Š: ŠUBŠU(M) – WYWOŁYWAĆ, SPRAWIAĆ  
\[jako rzeczownik\] WYWOŁYWANIE, SPRAWIANIE  
BAŠŰ ELI – CIĄŻYĆ NA (o długu)  
BAŠALU (i) – BYĆ UGOTOWANYM, UPIECZONYM  
\[jako rzeczownik\] BYCIE UGOTOWANYM, BYCIE UPIECZONYM  
D: BUŠŠULU – GOTOWAĆ, SMAŻYĆ, PIEC  
\[jako rzeczownik\] GOTOWANIE, SMAŻENIE, PIECZENIE  
BAŠAMU – WOREK, TORBA (Sack)  
BAŠAMU (i) – TWORZYĆ, KSZTAŁTOWAĆ, WYTWARZAĆ  
\[jako rzeczownik\] TWORZENIE, KSZTAŁTOWANIE, WYTWARZANIE  
\[BAŠARU\] – zob. BUŠŠURU  
BAŠLU – UGOTOWANY, UPIECZONY  
BAŠMU – UTWORZONY, STWORZONY, UKSZTAŁTOWANY, WYTWORZONY  
BAŠTU(M) – GODNOŚĆ, DOSTOJEŃSTWO  
BAŠŰ(M), BAŠŰ(M) (i) lub BAŠA’U – BYĆ, ISTNIEĆ, ZNAJDOWAĆ SIĘ; BYĆ TU  
\[jako rzeczownik\] BYCIE, ISTNIENIE, ZNAJDOWANIE SIĘ; BYCIE TU  
N: NABŠU(M) – POWSTAWAĆ, ZDĄŻAĆ KU ISTNIENIU; BYĆ POWOŁANYM DO ISTNIENIA, BYĆ ZA-  
ISTNIAŁYM  
\[jako rzeczownik\] POWSTAWANIE, ZDĄŻANIE KU ISTNIENIU; BYCIE POWOŁANYM DO ISTNIENIA,  
BYCIE ZAISTNIAŁYM  
Š: ŠUBŠU(M) – WYWOŁYWAĆ, SPRAWIAĆ  
\[jako rzeczownik\] WYWOŁYWANIE, SPRAWIANIE  
BAŠŰ ELI – CIĄŻYĆ NA (o długu)  
BAŠŰ (M), BAŠŰ(M), BAŠŰ(M) – BĘDĄCY, ISTNIEJĄCY, ZNAJDUJĄCY SIĘ; BĘDĄCY TU  
BATAQU(M) (a/u) – PRZECINAĆ, ODCINAĆ, PRZERYWAĆ  
\[jako rzeczownik\] PRZECINANIE, ODCINANIE, PRZERYWANIE  
N: NABTUQU(M) – PRZERYWAĆ, PRZECINAĆ; PSUĆ SIĘ  
\[jako rzeczownik\] PRZERYWANIE, PRZECINANIE; PSUCIE SIĘ  
D: BUTTUQU(M) – ODCINAĆ  
\[jako rzeczownik\] ODCINANIE  
DT: BUTATTUQU(M) – BYĆ PRZERWANYM  
\[jako rzeczownik\] BYCIE PRZERWANYM  
AWATAM BATAQUM – ZAŁATWIAĆ SPRAWĘ \[staroasyr.\]  
BUTUQTU(M) NABTUQU(M) – PRZERYWAĆ TAMĘ  
BATIQU(M) – PRZECINAJĄCY, ODCINAJĄCY, PRZERYWAJĄCY  
BATQU(M) – PRZECIĘTY, ODCIĘTY, PRZERWANY  
BATU (a/i) – SPĘDZIĆ NOC, PRZENOCOWAĆ  
\[jako rzeczownik\] SPĘDZENIE NOCY, PRZESPANIE SIĘ, PRZENOCOWANIE  
BATŰLTU – DZIEWCZYNA  
BATŰLU – MŁODZIENIEC  
BAŢALU (i) – PRZESTAWAĆ, USTAWAĆ, KOŃCZYĆ  
\[jako rzeczownik\] PRZESTAWANIE, PRZESTANIE, USTANIE, USTAWANIE, KOŃCZENIE  
BAŢILTU – SKOŃCZENIE, USTANIE  
IRŠŰ BAŢLATI – ODMÓWILI SŁUŻBY  
BAŢILU – KOŃCZĄCY, USTAJĄCY  
BAŢLU – ZAKOŃCZONY, PORZUCONY (jako przedmiot oddziaływania)  
BAULATU lub BAHULATU – (dosłownie: „OPANOWANI”) DRUŻYNA, ŻOŁNIERZE, PODDANI (należący do pana – BELU)  
\[l.mn.\]  
\[BAZA’U(M)\] – zob. BUZZU’U(M)  
BELTU(M) – PANI; zob. BELU(M)  
l.mn. BELETI  
BELU(M), BELU(M) (b’l) – \[sum. EN\] PAN; WŁAŚCICIEL, POSIADACZ; WŁADCA; (także przydomek boga Marduka)  
S.C.: B€L, r.ż. B€LTU, l.mn. B€L€, l.mn.r.ż. B€L€TI  
B€L ÂLÂNI – NACZELNIK MIASTA  
BEL ALI – KSIĄŻĘ MIASTA  
B€L SALÎME – SOJUSZNIK  
BEL DABÂBI – PRZECIWNIK  
BEL LUMNIM – PRZECIWNIK  
BEL AD€ U MAMIT – DOTRZYMUJĄCY UMOWY I PRZYSIĘGI  
BEL TERETIM – PEŁNOMOCNIK  
BEL AWATIM – OSKARŻYCIEL; PRZECIWNIK W PROCESIE SĄDOWYM  
B€L PÂHÂTI, BEL PAHATI – NACZELNIK OKRĘGU  
BEL PAHITI – NAMIESTNIK  
BEL SIMMI – ZADAJĄCY RANY  
B€L HIŢ(Ţ)I – GRZESZNIK, PRZESTĘPCA  
BEL ARNI – GRZESZNIK, WINOWAJCA  
BEL AŠŠATI – MĄŻ, MAŁŻONEK  
BEL HUBULLI – WIERZYCIEL  
BEL NARKABATI – WOŹNICA  
BEL NIQ€ – SKŁADAJĄCY OFIARĘ, OFIARNIK  
B€LU(M) (b’l) lub PELU (e) – PANOWAĆ, BYĆ PANEM  
\[jako rzeczownik\] PANOWANIE, BYCIE PANEM, WŁADANIE  
BELU(M) – OPANOWANY, OWŁADNIĘTY  
BELUTU – PANOWANIE  
BENNU – ? \[jakaś choroba\]  
BERETU – \[przymiotnik r.ż.\] JASNA, CZYSTA  
\[jako rzeczownik\] JASNOŚĆ, CZYSTOŚĆ  
BERU – WYBIERAĆ; BADAĆ, KONTROLOWAĆ, SPRAWDZAĆ  
\[jako rzeczownik\] WYBIERANIE, WYBÓR; BADANIE, KONTROLOWANIE, SPRAWDZANIE  
BERU – WYBRANY  
BERU – PODWÓJNA GODZINA; MILA  
BERU – JASNY, CZYSTY  
BERŰ(M) lub BARŰ (M) (br’) – GŁODOWAĆ, BYĆ GŁODNYM  
\[jako rzeczownik\] GŁODOWANIE  
BERŰ(M) – GŁODNY  
BERŰ(M) – GŁODUJĄCY, GŁODZĄCY SIĘ, UPRAWIAJĄCY GŁODÓWKĘ  
BEŠU (e) – ODDALAĆ SIĘ \[poet.\]  
\[jako rzeczownik\] ODDALANIE SIĘ  
BEŠU – ODDALONY \[poet.\]  
BIATU(M) – PRZENOCOWAĆ  
\[jako rzeczownik\] PRZENOCOWANIE  
Š: ŠUBUTU – ZEZWOLIĆ NA PRZENOCOWANIE, PRZENOCOWAĆ  
\[jako rzeczownik\] ZEZWOLENIE NA PRZENOCOWANIE; PRZENOCOWANIE  
BIBBULU – (wartki) PRZYPŁYW  
BIBLU(M), BIBLU(M) – PREZENT ŚLUBNY, PREZENT (zaręczynowy); POWÓDŹ, WYLEW  
BIKITU – PŁACZ  
BILLITU – (mieszane, wieloskładnikowe) PIWO  
BILTU, BILTU – PLON (z pola); DOCHÓD; DANINA, TRYBUT, PODATEK; ŁADUNEK; TALENT (miara ciężaru – ok. 30 kg)  
S.C. BILAT  
NAŠI BILTI – PODLEGAJĄCY OPODATKOWANIU, PODATNIK  
BILU – PROSZONY  
BIN lub BINI – WNUK  
BINI lub BIN – j.w.  
BINU – TAMARYSZEK (?)  
BINŰTU, BINŰTU – PĘD, ODROŚL; POTOMEK, WYCHOWANEK; PRODUKT, WYRÓB; KSZTAŁT, FORMA  
BIRBIRRU – BLASK PROMIENI  
BIRITU – ŚRODEK  
BIRIT UMI – POŁUDNIE  
(INA) BIRIT – MIĘDZY  
BIRITU – KAJDANY, WIĘZY, PĘTA  
BIRKU – KOLANO  
TARBIT BIRKEIA \[przen.\] – POTOMEK MOICH LĘDŹWI  
BIRMU, BIRMU – KOLOROWA (kolorowo wyszywana) TKANINA NA SUKNIĘ; PSTROKATA TKANINA lub ODZIEŻ  
BIRQU, BIRQU – BŁYSKAWICA  
BIRTU – TWIERDZA  
BIRTUTU – STAN, POŁOŻENIE TWIERDZY  
AL BIRTUTI – TWIERDZA  
BIRU – POKAZ, OGLĘDZINY (ofiar)  
BIRU – ŚRODEK  
(INA) BIRI – MIĘDZY  
BIŠU(M), BIŠU(M) – MAJĄTEK, MIENIE, WŁASNOŚĆ; MAJĄTEK RUCHOMY, RUCHOMOŚCI; (Amm.:) TOWAR  
BITIQTU – USZKODZENIE, SZKODA (szczególnie wyrządzona przez siłę wyższą)  
BITRAMU(M) – KOLOROWY, BARDZO KOLOROWY  
BITU(M), BITU(M), BITU(M) (bjt) – DOM, CHATA; POKÓJ, POMIESZCZENIE MIESZKALNE; NISZA (dla STATUY –  
CALMU); BYT DOMOWY; MAJĄTEK; MIEJSCE, MIEJSCE POBYTU; RODZINA, RÓD; ŚWIĄTYNIA \[sum. E\]  
S.C. BÎT, l.mn. BÎTÂTE, BÎTÂTI  
BÎT DŰRÂNI – TWIERDZA, UMOCNIONY REJON, UMOCNIONY OBÓZ, OBÓZ  
BÎT NAKKAMÂTI – SKŁAD, SPICHRZ; SKARBIEC  
BÎT NICIRTI, BIT NICIRTI – SKARBIEC (dosł. DOM PRZECHOWYWANIA)  
BÎT RIDŰTI – DOM NASTĘPSTWA TRONU (HAREM?)  
BÎT ŠARRŰTI, ÂL ŠARRŰTI – STOLICA, REZYDENCJA KRÓLEWSKA  
MAR ŠARRI RABŰ ŠA BIT RIDŰTI – NAJSTARSZY SYN KRÓLEWSKI = NASTĘPCA TRONU  
BIT ILI – ŚWIĄTYNIA (dosł. DOM BOGA)  
ALANI BIT-DURANI – MIASTA Z MURAMI OBRONNYMI  
BIT TUKLATI – BAZA ZAOPATRZENIOWA  
BIT-CERI – NAMIOT  
BIT ASAK(K)I – CIEMNICA, WIĘZIENIE  
BIT QEBERI – GRÓB  
BU”Ű(M), BU’’Ű(M) – SZUKAĆ  
\[jako rzeczownik\] SZUKANIE  
Odmiana: Prt: UBA”I, Pf: UBTA”I  
BU’ARU(M) – SZCZĘŚCIE, DOBRO  
BUÂNU lub RIKSU – STAW, PRZEGUB; ŚCIĘGNO  
BUBU’TU – PĘCHERZ  
BUBUTU, BUBUTU – GŁÓD, NĘDZA; POTRZEBA; POKARM, JEDZENIE  
BUDU(M) – RAMIĘ; STRONA  
BUKRU – PIERWORODNY; SYN PIERWORODNY  
BULLŰ – (często o ogniu) UGASIĆ; zob BALŰ  
\[jako rzeczownik\] UGASZENIE  
BULLŰ – (często o ogniu) UGASZONY, WYGASZONY  
BULLŰ(M) – ROZŁOŻONY, ZEPSUTY  
BULLUCU – WYGLĄDANY, WYPATRYWANY \[również czasownik i rzeczownik; zob. BALACU\]  
BULLUŢU(M), BULLUŢU(M) \[D od BALAŢU\] – ZACHOWYWAĆ (SIĘ) PRZY ŻYCIU, PODTRZYMYWAĆ ŻYCIE  
\[jako rzeczownik\] ZACHOWYWANIE ŻYCIA, UTRZYMANIE PRZY ŻYCIU  
BULLUŢU(M), BULLUŢU(M) – \[jako przymiotnik\] ZACHOWANY PRZY ŻYCIU, UTRZYMANY PRZY ŻYCIU, OSZCZĘ-  
DZONY? URATOWANY?  
BULŢU(M) – ŻYCIE; CZAS ŻYCIA  
BULU(M) – BYDŁO (domowe); ZWIERZĘ  
BUNNANNŰ lub BUNNANŰ – TWÓR, DZIEŁO; ZEWNĘTRZNA FORMA, POWIERZCHOWNOŚĆ, WYGLĄD  
BUNNANŰ lub BUNNANNŰ – j.w.  
BUNNŰ (M), BUNNŰ (M), BUNNŰ (M) – BUDOWAĆ Z ROZMACHEM, WYKONYWAĆ lub TWORZYĆ COŚ WIELKIE-  
GO, POWODOWAĆ IMPONUJĄCO, ZAKOŃCZYĆ Z HUKIEM; WYDOBYWAĆ NA WIELKĄ SKALĘ; RO-  
DZIĆ; WYKAZAĆ, UJAWNIĆ COŚ ZASKAKUJĄCEGO?; PLANOWAĆ czy PRZEWIDYWAĆ COŚ ZNA-  
CZĄCEGO; POKRYWAĆ ORNAMENTEM, UPIĘKSZAĆ  
\[jako rzeczownik\] BUDOWANIE Z ROZMACHEM, BUDOWANIE lub TWORZENIE czy POWODOWANIE  
CZEGOŚ WIELKIEGO, ZAKOŃCZENIE Z HUKIEM, WYDOBYWANIE NA WIELKĄ SKALĘ; RODZENIE;  
WYKAZANIE czy UJAWNIENIE CZEGOŚ ZASKAKUJĄCEGO?; PLANOWANIE czy PRZEWIDYWANIE  
CZEGOŚ ZNACZĄCEGO; POKRYWANIE ORNAMENTEM, UPIĘKSZANIE, UPIĘKSZENIE  
BUNNŰ (M), BUNNŰ (M), BUNNŰ (M) – ZBUDOWANY Z ROZMACHEM, WYKONANY lub STWORZONY W IMPO=  
NUJĄCYM STYLU, SPOWODOWANY W IMPONUJĄCY SPOSÓB, ZAKOŃCZONY Z HUKIEM; URODZONY  
W ATMOSFERZE WIELKIEGO WYDARZENIA; UJAWNIONY JAKO COŚ SENSACYJNEGO; ZAPLANO-  
WANY lub PRZEWIDZIANY JAKO COŚ ZNACZĄCEGO; POKRYTY ORNAMENTEM, UPIĘKSZONY  
BUNU – WYGLĄD, POWIERZCHOWNOŚĆ; WIDOK  
BUQUMU(M) – STRZYŻENIE (owiec)  
BURAŠU – JAŁOWIEC  
BURÂŠU – CYPRYS, PINIA  
BURRU – D: WYJAŚNIĆ, WYŚWIETLIĆ (sprawę w sądzie); PODAĆ, WYMIENIĆ, OŚWIADCZYĆ; USTALIĆ  
KIMA x \[Akk. osoby\] BURRU – STWIERDZIĆ WOBEC x  
\[jako rzeczownik\] WYJAŚNIENIE, WYŚWIETLENIE (sprawy w sądzie); PODANIE, WYMIENIENIE,  
OŚWIADCZENIE; USTALENIE  
BURRU – WYJAŚNIONY, WYŚWIETLONY (w sądzie); PODANY, WYMIENIONY, OŚWIADCZONY; USTALONY  
BURRUMU – KOLOROWO NAKRAPIANY, PSTRY; KOLOROWO, PSTRO WYSZYWANY, UTKANY (o ubiorze)  
BURTU – (młoda) KROWA  
BURTU, BURTU – STUDNIA; ZBIORNIK (na wodę)  
BURU – BURU \[miara powierzchni\]  
BURU – GŁÓD  
BURU, BURU – STUDNIA, ZBIORNIK (wody)  
BŰRU – DZIKI BYCZEK  
BUSSURTU(M) – WIEŚĆ, WIADOMOŚĆ  
BUŠŠULU – GOTOWAĆ, SMAŻYĆ, PIEC \[bezok. D, zob. BAŠALU\]  
\[jako rzeczownik\] GOTOWANIE, SMAŻENIE, PIECZENIE  
BUŠŠULU – GOTOWANY, SMAŻONY, PIECZONY (mocno, mocniej niż BAŠLU, w tym) ROZGOTOWANY, USMAŻONY NA WIÓR  
BUŠŠURU lub PUŠŠURU – D: OGŁOSIĆ, OBWIEŚCIĆ, ZAPOWIADAĆ, KOMUNIKOWAĆ  
\[jako rzeczownik\] OGŁOSZENIE, OBWIESZCZENIE, ZAPOWIADANIE, KOMUNIKOWANIE  
BUŠŠURU – OGŁOSZONY, OBWIESZCZONY, ZAPOWIEDZIANY, ZAKOMUNIKOWANY  
BUŠŢITU(M) – CZERW DRZEWNY  
BUŠU(M) – MAJĄTEK, WŁASNOŚĆ; ŁADUNEK (arki)  
BUTALLULU – BYĆ POPLAMIONYM, POWALANYM, POBRUDZONYM (INA x – CZYMŚ); zob. BALALU  
\[jako rzeczownik\] BYCIE POPLAMIONYM, POWALANYM, POBRUDZONYM  
BUTATTUQU(M) – BYĆ PRZERWANYM  
\[jako rzeczownik\] BYCIE PRZERWANYM  
BUTIQTU lub BUTUQTU(M) – PRZERWANIE, PRZEBICIE GROBLI  
BUTUQTU(M) NABTUQU(M) – PRZERWAĆ GROBLĘ  
BUTTUQU(M) – ODCINAĆ  
\[jako rzeczownik\] ODCINANIE  
BUTTUQU(M) – ODCINANY, ODCIĘTY  
BUTUQQŰ – STRATA (w działalności handlowej)  
BUTUQTU(M), BUTUQTU(M) lub BUTIQTU – PRZERWANIE, PRZEBICIE GROBLI  
BUTUQTU(M) NABTUQU(M) – PRZERWAĆ GROBLĘ  
BUZZU’U(M) (bz’) – \[tylko D i Dtn\] D: ZNĘCAĆ SIĘ  
\[jako rzeczownik\] ZNĘCANIE SIĘ  
DTN BUTAZZU’U(M): WCIĄŻ NA NOWO, BEZ PRZERWY, USTAWICZNIE ZNĘCAĆ SIĘ  
\[jako rzeczownik\] ZNĘCANIE SIĘ BEZ PRZERWY

C  
CA’ADU lub CA’JADU – MYŚLIWY  
CA’IHU – ŚMIEJĄCY SIĘ  
CA’INU(M) – ŁADUJĄCY; TRAGARZ? DOKER?  
CA’JADU lub CA’ADU – j.w.  
CA’U \[asyr.\] – NISZCZYĆ (rydwan)  
\[jako rzeczownik\] NISZCZENIE (rydwanu)  
CABATU(M) (a) – BRAĆ; BRAĆ DO NIEWOLI; CHWYTAĆ, ŁAPAĆ; PRZYJMOWAĆ, WZIĄĆ; ZAGARNIAĆ, POSIĄŚĆ,  
ZAWŁADNĄĆ; DOTKNĄĆ (o klęsce); TWORZYĆ, MIEĆ KSZTAŁT  
\[jako rzeczownik\] BRANIE; BRANIE DO NIEWOLI; CHWYTANIE, ŁAPANIE; PRZYJMOWANIE, WZIĘCIE;  
ZAGARNIANIE, WEJŚCIE W POSIADANIE, ZAWŁADNIĘCIE; DOTKNIĘCIE (klęską); TWORZENIE,  
POSIADANIE KSZTAŁTU  
GT: CITBUTU(M) – CHWYTAĆ SIĘ NAWZAJEM, CHWYTAĆ JEDEN DRUGIEGO; ZRASTAĆ SIĘ  
\[jako rzeczownik\] CHWYTANIE SIĘ NAWZAJEM; CHWYTANIE JEDEN DRUGIEGO; ZRASTANIE SIĘ  
N: NACBUTU(M) – BYĆ CHWYCONYM, BYĆ BRANYM, BYĆ WZIĘTYM; BYĆ W POSIADANIU  
\[jako rzeczownik\] BYCIE CHWYCONYM, BYCIE BRANYM, BYCIE WZIĘTYM; BYCIE W POSIADANIU  
D: CUBBUTU(M) – ROBIĆ ZŁAPANYM, PRZYCZYNIĆ SIĘ DO ZŁAPANIA, SPOWODOWAĆ ZŁAPANIE  
\[jako rzeczownik\] SPOWODOWANIE ZŁAPANIA, PRZYCZYNIENIE SIĘ DO ZŁAPANIA  
Š: ŠUCBUTU(M) – KAZAĆ ŁAPAĆ; OSIEDLAĆ (z ŠUBTA); POSTAWIĆ  
\[jako rzeczownik\] NAKAZANIE ZŁAPANIA; OSIEDLANIE; POSTAWIENIE  
ŠT: ŠUTACBUTU(M) – KAZAĆ SIĘ WZAJEMNIE CHWYCIĆ  
\[jako rzeczownik\] NAKAZANIE WZAJEMNEGO SCHWYTANIA SIĘ  
PANI x CABATUM – STANĄĆ NA PRZEDZIE, NA CZELE  
ŠEPI CABATU(M) – OBEJMOWAĆ NOGI; PODDAWAĆ SIĘ; ZABIERAĆ (do sędziego)  
KUSSIU ŠUCBUTU – KAZAĆ WSTĄPIĆ NA TRON  
ITTI x ŠUTACBUTUM – KAZAĆ SIĘ PRZYŁĄCZYĆ DO x  
AB(B)UTA CABATU – ZAPEWNIAĆ WSTAWIENIE SIĘ; PRZECIĄGAĆ NA STRONĘ (czyjąś)  
ANA EŠŠUTI CABATU – REORGANIZOWAĆ  
HARRANA lub URHA CABATU – OBRAĆ DROGĘ (również eliptycznie)  
MEHRET UMMANI CABATU – STANĄĆ NA FRONCIE ODDZIAŁU  
MUCÂ CABATU – ZAGRODZIĆ, ZABLOKOWAĆ WYJŚCIE  
(W)ARKATI CABATU – NASTĘPOWAĆ, IŚĆ ZA  
CABITANU – SIEPACZ  
CABITU – GAZELA  
CABITU(M) – TRZYMAJĄCY; CĘGI? \[jakieś narzędzie\]  
CABTU(M) – ZŁAPANY; JENIEC  
CABU – WOJOWNIK; (w l.mn.) LUDZIE  
\[CABŰ(M)\] – zob. CUBBŰ (M)  
CABU(M) – GRUPA LUDZI; GRUPA PRACUJĄCYCH; ODDZIAŁ; CZŁOWIEK (zwykle w l.mn. w znaczeniu:) LUDZIE lub  
MIESZKAŃCY  
l.mn. CABE  
CAB HUBŠI – PRACOWNIK? CIURA?  
CAB TAHAZI lub CAB TIDUKI – ŻOŁNIERZ  
CAB QASTI – ŁUCZNIK  
CAHARU – BYĆ MAŁYM; STAĆ SIĘ MAŁYM  
\[jako rzeczownik\] BYCIE MAŁYM; STAWANIE SIĘ MAŁYM  
CAHARU(M) (i) – zob. CEHERU(M)  
CAHATU – WYCISKAĆ  
\[jako rzeczownik\] WYCISKANIE  
CAHITU – WYCISKAJĄCY  
CAHRU lub CIHRU – SZKRAB, MALEC, MALUCH  
CAHTU – WYCISKANY, WYCIŚNIĘTY  
CAHU (a/i) – ŚMIAĆ SIĘ  
\[jako rzeczownik\] ŚMIANIE SIĘ, ŚMIECH  
CALALU (a) – KŁAŚĆ SIĘ, POŁOŻYĆ SIĘ; UPADAĆ (o mieście)  
\[jako rzeczownik\] KŁADZENIE SIĘ, POŁOŻENIE SIĘ; UPADANIE (o mieście)  
\[CALALU\] – zob. CULLULU  
(CALALU) – OSŁONIĆ; ZABEZPIECZYĆ  
\[jako rzeczownik\] OSŁONIENIE; ZABEZPIECZANIE  
CALAMU(M) (i) – BYĆ CIEMNYM, BYĆ CZARNYM  
\[jako rzeczownik\] BYCIE CIEMNYM, BYCIE CZARNYM  
CALILU – KŁADĄCY SIĘ; UPADAJĄCY (o mieście)  
(CALILU) – OSŁANIAJĄCY; ZABEZPIECZAJĄCY  
CALIPTU lub CALPU – KRZYWY; PRZEKUPNY, SPRZEDAJNY  
CALLU – POŁOŻONY, KŁADZIONY; UPADŁY (o mieście)  
CALMU – CZARNY, CIEMNY; BRUDNY, NIECZYSTY  
r.ż. CALIMTU, l.mn. CALMÂTI  
CALMÂT QAQQADI – SEMICI (dosł. CZARNOGŁOWI)  
CALMU – OBRAZ, WIZERUNEK  
CALMU – PRZEDSTAWIENIE, WYOBRAŻENIE; STATUA, POSĄG  
CALPATU – \[jako przymiotnik r.ż.\] KRZYWA; PRZEKUPNA, SPRZEDAJNA  
\[jako rzeczownik\] POKRZYWIENIE, KRZYWIZNA; PRZEKUPNOŚĆ, SPRZEDAJNOŚĆ  
CALPU lub CALIPTU – KRZYWY; PRZEKUPNY, SPRZEDAJNY  
CALTU lub CELTU, CILTU, CASSU – BITWA, BÓJ, WALKA; WRÓG; WROGOŚĆ  
CALTU EP€ŠU – WALCZYĆ (przeciwko komuś); WYDAĆ BITWĘ, DOPROWADZIĆ DO BITWY  
\[CALŰ\] – zob. CULLU  
CALŰ (i) \[asyr.\] – RZUCAĆ, KŁAŚĆ  
\[jako rzeczownik\] RZUCANIE, KŁADZENIE  
CALŰ \[asyr.\] – RZUCONY, POŁOŻONY, KŁADZIONY  
CALŰ \[asyr.\] – RZUCAJĄCY, KŁADĄCY  
CALULU lub CULULU – OBRONA, OSŁONA; CIEŃ  
CAMADU(M), CAMADU(M) (i) – ZAPRZĘGAĆ; WIĄZAĆ; PRZYGOTOWYWAĆ, SZYKOWAĆ  
\[jako rzeczownik\] ZAPRZĘGANIE; WIĄZANIE; PRZYGOTOWYWANIE, (U)SZYKOWANIE, PRZYGOTOWA-  
NIE  
\[CAMŰ\] – zob. CUMMU  
CAMŰ – SPRAGNIONY  
CANU lub ZANU (a) – NAPEŁNIAĆ; SPOWODOWAĆ NABRZMIENIE, DOPROWADZIĆ DO NABRZMIENIA,  
OPUCHLIZNY  
\[jako rzeczownik\] NAPEŁNIENIE, NAPEŁNIANIE; POWODOWANIE NABRZMIENIA, OPUCHLIZNY  
CANU – NAPEŁNIONY; NABRZMIAŁY, OPUCHNIĘTY  
CAPPU – PLEJADY \[gwiazdozbiór\]  
CAPRATU lub ZAPRATU – \[przymiotnik r.ż.\] ZŁA; KIEPSKA, NIEPOMYŚLNA; BRZYDKA, SZPETNA, SZKARADNA  
\[jako rzeczownik\] ZŁY albo KIEPSKI STAN RZECZY; BRZYDOTA, SZPETOTA, SZKARADNOŚĆ  
CAPRU lub ZAPRU – ZŁY; KIEPSKI, NIEPOMYŚLNY, ZŁY; BRZYDKI, SZPETNY, SZKARADNY  
CAPURTU lub ZAPURTU – NIESZCZĘŚCIE; MĘKA, KATUSZE  
CARAHU (a/u) – KRZYCZEĆ, WRZESZCZEĆ  
\[jako rzeczownik\] KRZYCZENIE, WRZESZCZENIE; KRZYK, WRZASK  
CARAHU (i; u) – ŻARZYĆ SIĘ, ROZŻARZAĆ, PŁONĄĆ  
\[jako rzeczownik\] ŻARZENIE SIĘ, ROZŻARZANIE SIĘ, ROZPALANIE SIĘ  
CARAMU(M) (i) – STARAĆ SIĘ, ZADAĆ SOBIE TRUD  
\[jako rzeczownik\] STARANIE SIĘ, ZADAWANIE SOBIE TRUDU  
CARAPU(M) (a/u) – BARWIĆ (NA CZERWONO)  
D: CURRUPU(M) – j.w.  
\[jako rzeczownik\] BARWIENIE (NA CZERWONO)  
CARARU (u) – ZABŁYSNĄĆ, ZAJAŚNIEĆ; ŚWIECIĆ; ISKRZYĆ SIĘ  
CARHU – WYKRZYCZANY, WYWRZESZCZANY  
CARHU – ROZŻARZONY, ROZPALONY  
CARIHU – KRZYCZĄCY, WRZESZCZĄCY; KRZYKACZ? AWANTURNIK?  
CARIHU – ŻARZĄCY SIĘ, ROZŻARZAJĄCY SIĘ; PŁONĄCY  
CARIMU(M) – STARAJĄCY SIĘ, ZADAJĄCY SOBIE TRUD, TRUDZĄCY SIĘ  
CARIPU(M) – BARWIĄCY, MALUJĄCY (NA CZERWONO); FARBIARZ? MALARZ?  
CARMU(M) – WYSTARANY, OSIĄGNIĘTY Z TRUDEM  
CARPU(M) – ZABARWIONY, POMALOWANY (NA CZERWONO)  
CARPU lub KASPU – SREBRO  
CASSU lub CELTU, CILTU, CALTU – BITWA, BÓJ, WALKA; WRÓG; WROGOŚĆ  
CALTU EP€ŠU – WALCZYĆ (przeciwko komuś); WYDAĆ BITWĘ, DOPROWADZIĆ DO BITWY  
\[CEBŰ(M)\] (cb’) – zob. ŠUTECBŰ (M)  
CEHERU(M) (i) – BYĆ MAŁYM; BYĆ NIEWIELKIM, NIEZNACZNYM (o cenie, liczbie)  
\[jako rzeczownik\] BYCIE MAŁYM, NIEWIELKIM, NIEZNACZNYM  
CEHHERU – MAŁY, MAŁOLETNI  
CEHHERUTU – MŁODZI LUDZIE, MŁODZIEŃCY; SŁUŻĄCY \[l.mn.\]  
CEHRU(M) – MAŁY; MŁODY, DZIECINNY; DZIECKO  
CEHER RABI – MAŁY I WIELKI = WSZYSCY  
CELLU(M) lub CELU(M) – ŻEBRO; STRONA  
CELTU lub CALTU, CILTU, CASSU – BITWA, BÓJ, WALKA; WRÓG; WROGOŚĆ  
CALTU EP€ŠU – WALCZYĆ (przeciwko komuś); WYDAĆ BITWĘ, DOPROWADZIĆ DO BITWY  
CELU – SPIERAĆ SIĘ, KŁÓCIĆ  
\[jako rzeczownik\] SPIERANIE SIĘ, KŁÓCENIE SIĘ; SPÓR? KŁÓTNIA?  
CELU – SKŁÓCONY  
C€LU – KŁÓCĄCY SIĘ, SPIERAJĄCY SIĘ; KŁÓTNIK? UCZESTNIK SPORU? PROWADZĄCY SPÓR?  
CELU(M) lub CELLU(M) – ŻEBRO; STRONA  
CELŰTU – WROGOŚĆ  
CENU – DROBNIEJSZY INWENTARZ; OWCE \[l.mn.\]; (Łyczkowska:) BYDŁO  
CENU – ZŁY, LICHY, MARNY  
CENŰ(M) – PRZESTĘPSTWO  
C€NU(M) (c’n) – ŁADOWAĆ  
\[jako rzeczownik\] ŁADOWANIE  
CENU(M) – ZAŁADOWANY  
CER – NA, NAD, DLA  
CER lub CIR – PRZECIW, NA, NAD  
CERRETU – WODZE, LEJCE, CUGLE  
CERU – POLE; CERU \[imię boga pól i zwierząt polnych\]  
CERU(M), CERU(M) – PLECY, GRZBIET; KRĘGOSŁUP  
CERU(M), CERU(M) lub ERRU – WĄŻ  
CERU(M) – WOLNE POLE; RÓWNINA, PŁASZCZYZNA; STEP, PUSTYNIA; ZAPLECZE; WNĘTRZE KRAJU  
ŠA CERIM – MIESZKANIEC (MIESZKAŃCY) STEPU lub PUSTYNI  
ANA CERIJA – PRZEDE MNIE, DO MNIE  
ICCER \[=IN(A) CER\] \[staroasyr.\] – W DEBET, NA POCZET (rachunku)  
ANA CER \[średnioasyr.\] – WZGLĘDEM, CO DO, ODNOŚNIE  
CETU lub CITU – ATMOSFERA, ŚWIEŻE POWIETRZE; POGODA  
CIBATUM – PODATEK  
CIBITTU(M), CIBITTU(M) – WIĘZIENIE; NIEWOLA; (także) JENIEC  
CIBTU(M) – PROCENT; NAROŚL \[termin dotyczący wróżb z wątroby\]  
l.mn. CIBATUM  
CIBUTU – ŻYCZENIE, MARZENIE  
CIBŰTU(M) – PRZEDSIĘWZIĘCIE, INTERES; ZAMIAR, PROJEKT; ŻYCZENIE  
l.mn. CIBIATUM  
ŠIPIR CIBUTI – FUNKCJA ŻĄDANA (przezeń)  
CIDUNNAJJA – SYDOŃCZYK \[mieszkaniec miasta Sydon\]  
CIHRU lub CAHRU – SZKRAB, MALEC, MALUCH  
CIHTU(M) – ŚMIECH; WESOŁOŚĆ, RADOŚĆ  
CIHU – WYŚMIANY?  
CILLANŰ – OCIENIONY, ZACIENIONY  
CILLU, CILLU – CIEŃ; OSŁONA  
ŠA CILLI – BALDACHIM  
CILLŰ(M) – IGŁA  
CILTU lub CELTU, CALTU, CASSU – BITWA, BÓJ, WALKA; WRÓG; WROGOŚĆ  
CALTU EP€ŠU – WALCZYĆ (przeciwko komuś); WYDAĆ BITWĘ, DOPROWADZIĆ DO BITWY  
CIMDATU(M), CIMDATU(M) – NAKAZ KRÓLEWSKI; DECYZJA; ZBIÓR PRAW, STATUT; zob. CIMITTU  
CIMDU lub CINDU – UPRZĄŻ  
CIMITTU – WYKAZ CEN (regulowanych przez państwo) (?); UPRZĄŻ  
SICE CIMDAT lub SICE CINDAT – KONIE POCIĄGOWE  
CIMITTI RUKUBI – ZAPRZĘGNIĘTY DO WOZU  
CINDU lub CIMDU – UPRZĄŻ  
CIPTU – PROCENTY, OPROCENTOWANIE; DOCHÓD  
INA MUHHI RABU – WZROSNĄĆ (wyrażenie odnoszone też do CIPTU)  
CIR lub CER – PRZECIW, NA, NAD  
CIRATU – WIELKA, WZNIOSŁA  
\[jako abstrakt\] WIELKOŚĆ, WZNIOSŁOŚĆ  
CIRHU – BLASK; PŁOMIEŃ  
CIRPU – FARBOWANA WEŁNA  
CIRRU lub CIRU – WĄŻ; = CERU  
CIRU lub CIRRU – j.w.  
CIRU – WIELKI, WZNIOSŁY  
CIRU – WYSOKI, PODWYŻSZONY  
CITBUTU(M) – CHWYTAĆ SIĘ NAWZAJEM, CHWYTAĆ JEDEN DRUGIEGO; ZRASTAĆ SIĘ \[bezok. GT; zob. CABATU\]  
\[jako rzeczownik\] WZAJEMNE CHWYTANIE SIĘ; ZRASTANIE SIĘ  
CITTU, CITTU lub CITU(M) – WYJŚCIE; WEJŚCIE; WYJŚCIE, ODEJŚCIE; WYDANIE, ROZTRWONIENIE, STRATA;  
ZAGINIĘCIE, UTRATA; WYRÓB, WYTWÓR; POTOMEK; KIEŁEK; LATOROŚL  
s.c. CÎT, l.mn. CIÂTI, CÂTI  
ANA CITIM ŠUCŰM – WYCHODZIĆ; ODDAĆ; ZDAĆ?  
CÎT ŠAMŠI – WSCHÓD SŁOŃCA; WSCHÓD \[strona świata\]  
CÎT ARHI – NÓW  
CÎT LIBBI – RODZONE DZIECKO  
CITU lub CETU – ATMOSFERA, ŚWIEŻE POWIETRZE; POGODA  
CITU(M), CITU(M) lub CITTU – WYJŚCIE; WEJŚCIE; WYJŚCIE, ODEJŚCIE; WYDANIE, ROZTRWONIENIE, STRATA;  
ZAGINIĘCIE, UTRATA; WYRÓB, WYTWÓR; LATOROŚL  
s.c. CÎT, l.mn. CIÂTI, CÂTI  
ANA CITIM ŠUCŰM – WYCHODZIĆ; ODDAĆ; ZDAĆ?  
CÎT ŠAMŠI – WSCHÓD SŁOŃCA; WSCHÓD \[strona świata\]  
CÎT ARHI – NÓW  
CÎT LIBBI – RODZONE DZIECKO  
CŰ lub ZŰ (c”) – NAWÓZ; EKSKREMENTY, ODCHODY, KAŁ; MOCZ  
CUBATU – ODZIEŻ; TKANINA  
CUBATU(M) – ODZIENIE, ODZIEŻ \[l.p. i l.mn.\]  
CUBBŰ(M) – D: SPOGLĄDAĆ, OGLĄDAĆ  
\[jako rzeczownik\] SPOGLĄDANIE, OGLĄDANIE  
CUBBU(M) – OGLĄDANY, OBSERWOWANY

CUBBUTU(M) – ROBIĆ ZŁAPANYM, PRZYCZYNIĆ SIĘ DO ZŁAPANIA, SPOWODOWAĆ ZŁAPANIE  
\[jako rzeczownik\] PRZYCZYNIENIE SIĘ DO ZŁAPANIA, SPOWODOWANIE ZŁAPANIA, KIEROWANIE  
ŁAPANIEM  
CUBBUTU(M) – ZŁAPANY (za sprawą kogoś celowo działającego)  
CUCŰ – TRZCINA, ZAROŚLA TRZCINOWE; POROŚNIĘTE TRZCINĄ ŻUŁAWY RZECZNE  
CUHARU(M) – CHŁOPIEC; MŁODY CZŁOWIEK; SŁUŻĄCY  
CUHRU – DZIECIŃSTWO  
CUHU – BAWIENIE SIĘ, IGRY, ZABAWA  
CULLU – D: BŁAGAĆ, PROSIĆ  
\[jako rzeczownik\] BŁAGANIE, PROSZENIE  
CULLULU – D: POKRYWAĆ DACHEM, ZADASZAĆ \[D\]  
\[jako rzeczownik\] POKRYWANIE DACHEM, ZADASZANIE  
CULULU lub CALULU – OBRONA, OSŁONA; CIEŃ  
CUMBU – WÓZ CIĘŻAROWY  
CUMLALŰ – ? \[jakaś aromatyczna roślina będąca bardzo często surowcem wyjściowym do wyrobu perfum\] \[słowo obcego pocho-  
dzenia\]  
CUMMIRATU(M) – ŻYCZENIE, ZAMIARY \[l.mn.\] \[r.ż.\]; zob. CUMMIRTU  
CUMMIRTU – ŻYCZENIE  
CUMMU – D: CZYNIĆ SPRAGNIONYM, SPOWODOWAĆ PRAGNIENIE \[D\]  
\[jako rzeczownik\] POWODOWANIE PRAGNIENIA; ODMAWIANIE NAPOJU?  
CUMMU – POZBAWIONY MOŻLIWOŚCI ZASPAKAJANIA PRAGNIENIA  
CUMMŰ – PRAGNIENIE  
CUMU(M) – PRAGNIENIE

D  
DA’ANU – SĘDZIA  
DA’ANUTU lub DAIANUTU – GODNOŚĆ, STANOWISKO SĘDZIEGO  
DA’IKU(M) – ZABIJAJĄCY  
DA’ILU(M), DA’ILU(M) (u; a/u) – BIEGAJĄCY, BIEGNĄCY; BIEGACZ? GONIEC?  
DA’INU(M) – OSĄDZAJĄCY, SĄDZĄCY, WYDAJĄCY, OGŁASZAJĄCY WYROK  
DA’IŠU – MŁÓCĄCY; TŁUCZĄCY  
DA’MATU – CIEMNA WEŁNA  
DA’MU – CIEMNY; MĘTNY  
DABABU(M) (u) – MÓWIĆ, OMAWIAĆ, OPOWIADAĆ; PRAWOWAĆ SIĘ, ZASKARŻYĆ DO SĄDU  
\[jako rzeczownik\] MÓWIENIE, OMAWIANIE, OPOWIADANIE; PRAWOWANIE SIĘ, SKARŻENIE DO SĄDU,  
WYTACZANIE SPRAWY SĄDOWEJ  
x \[Akk. rzeczy\] ITTI y DABABUM – OPOWIADAĆ x (coś) y (komuś)  
ŢABUTA DABABU – UZGODNIĆ DOBRY STOSUNEK (?)  
DENU DABABU – WNIEŚĆ SKARGĘ  
DABBU(M) – OMÓWIONY, OPOWIEDZIANY; ZASKARŻONY DO SĄDU, POZWANY PRZED SĄD  
DABDŰ lub TAPDŰ – SKŁAD, MAGAZYN \[Niederlage\]  
DABIBU(M) – MÓWIĄCY, OPOWIADAJĄCY, MÓWCA? REFERENT? SKARŻĄCY DO SĄDU, POZYWAJĄCY PRZED SĄD,  
POWÓD  
DACŰ(M) (d’c) (a) – GNĘBIĆ; ZMUSZAĆ  
\[jako rzeczownik\] GNĘBIENIE, ZMUSZANIE  
DACŰ(M) – ZGNĘBIONY; ZMUSZONY; OFIARA PRZEMOCY  
DACŰ(M) – GNĘBIĄCY; ZMUSZAJĄCY; GNĘBICIEL? PRZEŚLADOWCA?  
DADMU lub ŢAŢMU – SIEDZIBA LUDZKA, MIEJSCE ZAMIESZKANE; MIEJSCE; KRAJE, OSIEDLA, MIASTA, PUNKTY  
ZASIEDLONE  
l.mn. DADMŰ, ŢAŢMŰ, DADMI, ŢAŢMI, DADM€, ŢAŢM€  
ŠŰT DADMI, NÎŠE DADMI – MIESZKANIEC; LUDZIE; LUDZKOŚĆ  
DADMU- \[l.mn.\] MIEJSCE ZAMIESZKANIA  
DAGALU (a/u) – PATRZEĆ, SPOGLĄDAĆ  
\[jako rzeczownik\] PATRZENIE, SPOGLĄDANIE  
PAN(I) x DAGALU – PODLEGAĆ x, BYĆ DO USŁUG x; USŁUCHAĆ x, BYĆ POSŁUSZNYM x; CZEKAĆ NA x  
DAGGASSU – BRYŁA KAMIENNA \[sum.\]  
DAGILU – OGLĄDAJĄCY, OBSERWUJĄCY  
DAGLU – OGLĄDANY, OBSERWOWANY  
DAIANUTU lub DA’ANUTU – GODNOŚĆ, STANOWISKO SĘDZIEGO  
DA’IKU(M) – ZABIJAJĄCY; BIJĄCY; ZABÓJCA? OPRAWCA?  
DAJJANU(M) – SĘDZIA  
DAKAŠU(M) (a/u) – WYPCHNĄĆ; NABRZMIEĆ  
\[jako rzeczownik\] WYPYCHANIE; NABRZMIEWANIE  
GT: DITKUSU(M) – NABRZMIEĆ Z OBU STRON  
\[jako rzeczownik\] NABRZMIEWANIE Z OBU STRON  
DAKISU(M) – WYPYCHAJĄCY; NABRZMIEWAJĄCY, PUCHNĄCY?  
DAKSU(M) – WYPCHNIĘTY; NABRZMIAŁY  
DAKU – ZNISZCZYĆ, ZBURZYĆ  
\[jako rzeczownik\] NISZCZENIE, BURZENIE  
DÂKU(M) (u) – ZABIJAĆ, BIĆ  
\[jako rzeczownik\] ZABIJANIE, BICIE  
N: NADKU(M) – BYĆ ZABITYM  
\[jako rzeczownik\] BYCIE ZABITYM  
Š: ŠUDKU(M) – KAZAĆ ZABIĆ  
\[jako rzeczownik\] SPOWODOWANIE ZABICIA, SKAZANIE NA ŚMIERĆ  
DAKU(M) – ZABITY; POBITY, ZBITY  
DALAHU (a/\[u\]) – POMIESZAĆ, ZAMIESZAĆ, ZMĄCIĆ (np. wodę, spokój)  
\[jako rzeczownik\] MIESZANIE, MĄCENIE; ZAKŁÓCANIE  
DALALU (a/u) – SŁAWIĆ, CHWALIĆ, WYCHWALAĆ, ROZSŁAWIAĆ; BYĆ POKORNYM, POSŁUSZNYM, ODDANYM  
\[jako rzeczownik\] SŁAWIENIE, CHWALENIE, ROZSŁAWIANIE, WYCHWALANIE; BYCIE POKORNYM,  
POSŁUSZNYM, ODDANYM  
DALAPU (i; u) – BYĆ PODNIECONYM, WZBURZONYM, NIESPOKOJNYM; BYĆ BEZSENNYM  
\[jako rzeczownik\] BYCIE PODNIECONYM, WZBURZONYM, NIESPOKOJNYM; POZOSTAWANIE W STANIE  
PODNIECENIA, WZBURZENIA, NIEPOKOJU; BYCIE BEZSENNYM  
DALHU – MĘTNY (o wodzie)  
DALIHTU – NIEPOKÓJ  
DALIHU – MIESZAJĄCY, MĄCĄCY; ZAKŁÓCAJĄCY (spokój)  
DALÎLU – SŁAWA, CHWAŁA, CZEŚĆ; POSŁUSZEŃSTWO, ODDANIE; WYCHWALANIE, GLORYFIKACJA  
S.C. DALÎL  
INA EPEŠ PI MUTALLI – OGŁOSZENIE CHWAŁY  
DALILU – CHWALĄCY, WYCHWALAJĄCY, SŁAWIĄCY; POCHLEBCA?  
DALLU – ROZSŁAWIONY. POCHWALONY; POKORNY, POSŁUSZNY, ODDANY  
DALPU – WZBURZONY, NIESPOKOJNY  
DALTU – SKRZYDŁO BRAMY; DRZWI, ODRZWIA  
S.C. DALAT  
DÂLU(M), DÂLU(M) (u; a/u) – BIEGAĆ, BIEC  
\[jako rzeczownik\] BIEGNIĘCIE, BIEGANIE  
DAMAMU (u) – LAMENTOWAĆ, BIADAĆ  
\[jako rzeczownik\] LAMENTOWANIE, BIADANIE  
DAMAQU – BYĆ DOBRYM; WYŚWIADCZYĆ ŁASKĘ  
\[jako rzeczownik\] BYCIE DOBRYM; WYŚWIADCZENIE ŁASKI, OBDARZENIE ŁASKĄ; UŁASKAWIENIE?  
DAMAQU(M) (i) – BYĆ DOBRYM, POMYŚLNYM; POZOSTAWAĆ DOBRYM; MIEĆ POWODZENIE  
\[jako rzeczownik\] BYCIE DOBRYM, POMYŚLNYM; POSIADANIE POWODZENIA  
D: DUMMUQU(M) – CZYNIĆ DOBRO  
\[jako rzeczownik\] CZYNIENIE DOBRA  
DAMIMU – LAMENTUJĄCY, BIADAJĄCY; ŻAŁOBNIK?  
DAMIQTU – DOBRO; DOBRODZIEJSTWO, ŁASKA; POBOŻNOŚĆ; PRAWDA; SZCZĘŚCIE, POMYŚLNOŚĆ; PIĘKNOŚĆ,  
PIĘKNO; zob. DAMQU  
l.mn. DAMKÂTI  
DAMIQU – ŚWIADCZĄCY ŁASKĘ; DOBROCZYŃCA?  
DAMIQU(M) – MAJĄCY POWODZENIE  
DAMMU – ŻAŁOWANY, BĘDĄCY POWODEM LAMENTU  
DAMQU(M) lub DE’IKTU – DOBRY (zarówno w sensie materialnym jak i etycznym)  
r.ż. DAMIQTUM  
DAMQU – DOBRY; OBDARZONY ŁASKĄ; TEN, KTÓRY DOZNAŁ DOBRA  
DAMU – DAMU \[imię boga zdrowia?\]  
DAMU(M) – KREW; MORDERSTWO  
AMIR DAMI – KRWIOŻERCZY  
DANANU – MOC, SIŁA  
DANANU – BYĆ MOCNYM, SILNYM; STAĆ SIĘ MOCNYM, SILNYM; WZMÓC, NASILIĆ  
\[jako rzeczownik\] BYCIE MOCNYM, SILNYM; STANIE SIĘ MOCNYM, SILNYM; UMOCNIENIE SIĘ?  
WZMAGANIE, NASILANIE  
DANANU(M), DANANU(M) (i) – BYĆ SILNYM, MOCNYM  
\[jako rzeczownik\] BYCIE SILNYM&lt; BYCIE MOCNYM  
D: DUNNUNU(M), DANNUNU(M) \[asyr.\] CZYNIĆ MOCNYM, UMACNIAĆ, NA NOWO CZYNIĆ MOCNYM;  
NA STAŁE UZGADNIAĆ  
\[jako rzeczownik\] CZYNIENIE MOCNYM, UMACNIANIE, UMACNIANIE NA NOWO; UZGADNIANIE NA  
STAŁE  
RT: OKAZYWAĆ SIĘ SILNIEJSZYM, MIERZYĆ ZE SOBĄ SIŁY  
\[jako rzeczownik\] OKAZYWANIE SIĘ SILNIEJSZYM, MIERZENIE SIŁ ZE SOBĄ  
DANDANNU – POTĘŻNY, PRZEMOŻNY  
DANINU – WZMAGAJĄCY, NASILAJĄCY,  
DANNATU – GŁÓD, NĘDZA  
DANNATU(M), DANNATU(M) – CIĘŻKIE POŁOŻENIE, TRUDNOŚCI, BIEDA, KLĘSKA; TWIERDZA; POTĘGA, SIŁA  
(zbrojna)  
DANNIŠ – BARDZO; MOCNO, SILNIE  
DANNIŠ lub MAGAL – BARDZO  
DANNU – BECZKA  
l.mn. DANNUTU  
DANNU – SILNY, MOCNY, UMOCNIONY; WZMOŻONY, NASILONY  
DANNU(M) – SILNY, MOCNY, POTĘŻNY, WŁADNY; MOCNY, UMOCNIONY, OBWAROWANY; CIĘŻKI (o winie); WY-  
CIEKŁY (o krwi)  
r.ż. DANNATUM  
ÂLÂNI DANNŰTI – OBWAROWANE MIASTA, TWIERDZE  
DANNUNU(M) – UMACNIAĆ; zob. DANANU(M)  
\[jako rzeczownik\] UMACNIANIE  
DANNUTU – SIŁA, MOC  
AL DANNUTI – TWIERDZA  
DÂNU(M) (din) – SĄDZIĆ, OSĄDZIĆ, WYDAĆ WYROK, OGŁASZAĆ WYROK  
\[jako rzeczownik\] SĄDZENIE, OSĄDZENIE, WYDANIE WYROKU, OGŁOSZENIE WYROKU  
DINAM DÂNUM – SĄDZIĆ, WYMIERZAĆ SPRAWIEDLIWOŚĆ  
DÂNU(M) – OSĄDZONY, ZASĄDZONY, WYDANY, OGŁOSZONY (wyrok)  
DAPPU(M) – KROPLA  
DARARU (u) – OPADAĆ (o wodzie)  
\[jako rzeczownik\] OPADANIE (wody)  
DARIRU – OPADAJĄCY (o wodzie)  
DARIŠ – zob. DARŰ (M)  
DARIŠAM – zob. DARU(M)  
DARITU(M) – OKRES, PRZECIĄG CZASU  
ANA DARIATIM – NA ZAWSZE  
DARKATU – NASTĘPNE POKOLENIE, POTOMNOŚĆ  
DARU – DŁUGO; WIECZNIE  
ANA DAR – NA ZAWSZE  
DARŰ(M) lub DARIU(M) – TRWAŁY, WIECZNY  
DARIŠ UMIM – NA ZAWSZE  
DARIŠ, ANA DARI(Š), DARIŠAM – NA ZAWSZE  
ANA DARIŠ UMI – NA ZAWSZE  
DAŠŠUPU – SŁODKI JAK MIÓD  
DAŠU (a/i) – MŁÓCIĆ; TŁUC  
\[jako rzeczownik\] MŁÓCENIE; TŁUCZENIE  
DAŠU – MŁÓCONY, WYMŁÓCONY; POTŁUCZONY, STŁUCZONY  
DATU(M) \[staroasyr.\] lub DI’ATU(M) – WIEDZA, ZNAJOMOŚĆ RZECZY  
DI’ATAM ŠÂLUM, DATAM ŠÂLUM – DOWIADYWAĆ SIĘ (O)  
DE’IKU – KAŻĄCY POWSTAĆ; ZWOŁUJĄCY; BUDZĄCY; PRZESZKADZAJĄCY; WZNIECAJĄCY (walkę); ZRZUCAJĄCY  
DE’IQTU lub DAMQU(M) – DOBRY (zarówno w sensie materialnym jak i etycznym)  
DEKŰ (i) – POZWOLIĆ NA POWSTANIE, KAZAĆ POWSTAĆ; ZWOŁYWAĆ; BUDZIĆ, OBUDZIĆ; PRZESZKODZIĆ;  
WZNIECAĆ (walkę); ZRZUCAĆ  
\[jako rzeczownik\] SPOWODOWANIE, NAKAZANIE POWSTANIA; ZWOŁYWANIE; BUDZENIE,  
OBUDZENIE; PRZESZKADZANIE; WZNIECANIE (walki); ZRZUCANIE  
IDEŠU DEKŰ – ODDAWAĆ SIĘ, POŚWIĘCAĆ SIĘ  
DEKŰ – SKŁONIONY DO POWSTANIA;ZWOŁANY; OBUDZONY; UDAREMNIONY; WZNIECONY (o walce); ZRZUCONY  
DEKŰ – POZWALAJĄCY NA POWSTANIE, KAZĄCY POWSTAĆ; ZWOŁUJĄCY; BUDZĄCY; PRZESZKADZAJĄCY; WZNIECA-  
JĄCY (walkę); ZRZUCAJĄCY  
DEKŰ – SIERŻANT?  
DENU(M) lub DINU(M) – WYROK, DECYZJA; PROCES, SPRAWA SĄDOWA, ZATARG SĄDOWY  
l.mn. DINATU  
DINAM ŠUHUZUM – UZNAĆ PRAWO WSZCZĘCIA PROCESU SĄDOWEGO, WSZCZĄĆ PROCES SĄDOWY  
(na czyjeś życzenie)  
DINAM DÂNUM – SĄDZIĆ, WYMIERZAĆ SPRAWIEDLIWOŚĆ  
DI’ATU(M) lub DATU(M) – WIEDZA, ZNAJOMOŚĆ RZECZY  
DI’ATAM ŠALUM, DI’ATAM ŠALUM – DOWIADYWAĆ SIĘ (O)  
DI’U lub DIHU – ZARAZA, EPIDEMIA  
DI’U – CHOROBA GŁOWY  
DIHU lub DI’U – ZARAZA, EPIDEMIA  
DIKTU – ZABIJANIE  
DIKTA DAKU (a/u) – WYCINAĆ W PIEŃ  
DILIPTU – WZBURZENIE, PODNIECENIE, NIEPOKÓJ, BEZSENNOŚĆ  
DIMMU (d’m) – SŁUP, OBELISK; KOŁEK DO TRZEPANIA BIELIZNY  
DIMTU – ŁZA  
DIMTU – SŁUP, FILAR, KOLUMNA; WIEŻA, BASZTA  
DINU(M), DINU(M) lub DENU(M) – WYROK, DECYZJA; PROCES, SPRAWA SĄDOWA, ZATARG SĄDOWY; SĄD  
l.mn. DINATU  
DINAM ŠUHUZUM – UZNAĆ PRAWO WSZCZĘCIA PROCESU SĄDOWEGO, WSZCZĄĆ PROCES SĄDOWY  
(na czyjeś życzenie)  
DINAM DÂNUM – SĄDZIĆ, WYMIERZAĆ SPRAWIEDLIWOŚĆ  
DIPARU – POCHODNIA \[sum.\]  
l.mn. DIPARATU  
DIQARU, DIQARU – ? \[jakieś naczynie\] \[gliniane – wg Lipina\]  
DIŠPU – MIÓD  
DIŠPU lub LALLARU – MIÓD  
DIŠŰ – LICZNI \[l.mn.\]  
DIŠU(M) – TRAWA; OBROK  
DITALLIŠ – JAK POPIÓŁ \[młodobab.\], zob. DITALLU  
DITALLU – POPIÓŁ \[młodobab.\]  
DITALLIŠ – JAK POPIÓŁ  
DUDITTU – KLEJNOT (zawieszony na piersi)  
l.mn. DUDINATI  
DULUHHU – NIEPORZĄDEK, BEZŁAD  
DUMAMU – LAMENT, BIADANIE, NARZEKANIE  
DUMMUQU(M) – OBDARZONY DOBREM  
DUMQU – zob. DAMIQTU  
DUMQU lub DUNQU – ŁASKA  
DUNNAMŰ – SŁABY, MIZERNY  
DUNNU – SIŁA, PRZEMOC  
DUNQU – zob. DAMIQTU  
DUNQU lub DUMQU – ŁASKA  
DUPPU – TABLICZKA (gliniana); ZAPISANA UMOWA, DOKUMENT \[sum.\]  
DUP-ŠIMATI – TABLICZKA LOSU  
DUPPURU lub TUPPURU – POZOSTAWAĆ Z DALA, ODDALAĆ SIĘ \[nieprzech.\]; WYPĘDZAĆ \[przech.\]  
\[jako rzeczownik\] POZOSTAWANIE Z DALA, ODDALANIE SIĘ; WYPĘDZANIE  
DUPPURU – POZOSTAŁY Z DALA, ODDALONY; WYPĘDZONY  
DUPPUSSŰ – DRUGI, MNIEJSZY RANGĄ; MŁODSZY  
DUPŠARRU – PISARZ  
DUPŠIKKU lub KUDURRU – OBOWIĄZEK PRACY, PRZYMUS PRACY  
DŰRU – WIECZNOŚĆ  
ANA DURI DARI – NA WIEKI, WIECZYŚCIE  
DURU(M) – TRWAŁOŚĆ  
DURU(M), DURU(M) – UMOCNIONA SIEDZIBA, UMOCNIENIE, TWIERDZA; WAŁ, MUR OBRONNY, MUR MIEJSKI;  
ŚCIANA  
BÎT DŰRÂNI – TWIERDZA  
DUŠŠUPU(M) – SŁODKI, SŁODKI JAK MIÓD  
DUŠŰ – SUROWA SKÓRA \[sum.\]

E  
E – PO \[sum\]  
1 GAN-e – PO JEDNYM GANIE  
€ – NIE (z czasownikami 2 os. l.p. Prt)  
€ lub Î – NUŻE! ŚMIAŁO! \[sum.\]  
E (przed spółgłoską) – NIE CHCĘ \[partykuła przed życzeniem negatywnym\]  
E’ELU(M) – WIĄZAĆ  
\[jako rzeczownik\] WIĄZANIE  
E’ILTU lub EHILTU – ZOBOWIĄZANIE DŁUŻNE  
E’ILU(M) – WIĄŻĄCY  
E’LU(M) – ZWIĄZANY  
EA – EA \[imię boga mądrości\]  
EBBU(M) – JASNY, CZYSTY; WYBRANY, GODNY ZAUFANIA  
EBEBU(M) (i) – BYĆ CZYSTYM; DOKONYWAĆ OCZYSZCZAJĄCEJ PRZYSIĘGI  
\[jako rzeczownik\] BYCIE CZYSTYM; DOKONYWANIE PRZYSIĘGI OCZYSZCZAJĄCEJ  
D: UBBUBU(M) – OCZYSZCZAĆ, OCZYSZCZAĆ SIĘ (przez przysięgę; Amm. IV3)  
\[jako rzeczownik\] OCZYSZCZANIE; OCZYSZCZANIE SIĘ (przez przysięgę)  
EBERU (i) – PRZEKRACZAĆ  
\[jako rzeczownik\] PRZEKRACZANIE  
\[EBERU\] – zob. UBBURU  
EBIBU(M) – PRZYSIĘGAJĄCY W CELU OCZYSZCZENIA SIĘ; OCZYSZCZAJĄCY, CZYSZCZĄCY SIĘ  
EBIRU – PRZEKRACZAJĄCY  
EBRU – PRZEKROCZONY  
EBURU – URODZAJ  
EBURU(M), EBURU(M) – ŻNIWA; PLON; CZAS ŻNIW; LATO; {w l.mn.) ZBIORY ŻNIWNE  
ECDU(M) – SPRZĄTNIĘTY Z POLA; PLON?  
ECEDU(M) – ŻNIWA  
ECEDU(M) (i) – SPRZĄTAĆ Z POLA  
\[jako rzeczownik\] SPRZĄTANIE Z POLA, ZBIERANIE PLONÓW, ŻNIWOWANIE  
ECEMTU – KOŚĆ  
ECENU(M) (i) – WĄCHAĆ; ZNOSIĆ, WYTRZYMYWAĆ  
\[jako rzeczownik\] WĄCHANIE; ZNOSZENIE, WYTRZYMYWANIE  
ECEPU (i) – PODWAJAĆ; MNOŻYĆ, UWIELOKROTNIAĆ  
\[jako rzeczownik\] PODWAJANIE; MNOŻENIE, ZWIELOKROTNIANIE  
ECERU (i) – WYZNACZYĆ; NAZNACZYĆ, NARYSOWAĆ; RYSOWAĆ  
\[jako rzeczownik\] WYZNACZANIE; ZAZNACZANIE, NARYSOWANIE; RYSOWANIE  
ECIDU(M) – SPRZĄTAJĄCY Z POLA, ZBIERAJĄCY PLON, ŻNIWIARZ  
ECINU(M) – WĄCHAJĄCY; ZNOSZĄCY, WYTRZYMUJĄCY  
ECIPU – PODWAJAJĄCY; MNOŻĄCY, ZWIELOKRATNIAJĄCY  
ECIRU – WYZNACZAJĄCY; ZAZNACZAJĄCY, ZARYSOWUJĄCY; RYSUJĄCY; RYSOWNIK?  
ECNU(M) – OBWĄCHIWANY; ZNOSZONY, WYTRZYMYWANY  
ECPU – PODWOJONY; POMNOŻONY, ZWIELOKROTNIONY  
ECRU – WYZNACZONY; NAZNACZONY, ZARYSOWANY; NARYSOWANY  
ECU \[asyr.\] lub ICU(M) – NIELICZNY, W MAŁEJ LICZBIE  
EDDETTU – ? \[jakiś kolczasty krzew?\]  
EDDU(M) – SPICZASTY  
EDEDU(M) – BYĆ SPICZASTYM \[tylko St\]  
\[jako rzeczownik\] BYCIE SPICZASTYM  
EDELU(M) (i) – ZAMYKAĆ  
\[jako rzeczownik\] ZAMYKANIE  
EDEQU (i) – UBIERAĆ (kogoś w coś)  
\[jako rzeczownik\] UBIERANIE  
ED€RU (’dr) lub ADARU (a/u) – G i N (NENDURU): BAĆ SIĘ, PRZESTRASZYĆ SIĘ, LĘKAĆ SIĘ (czegoś – Akk); ULEGAĆ  
ZAĆMIENIU (o ciele niebieskim)  
\[jako rzeczownik\] BANIE SIĘ, PRZESTRASZENIE SIĘ, LĘKANIE SIĘ; ULEGANIE ZAĆMIENIU  
GTN: ITEDDURU – BAĆ SIĘ WCIĄŻ NA NOWO, STALE  
\[jako rzeczownik\] BEZUSTANNE BANIE SIĘ, TRWANIE W STRACHU  
ŠT(N): ŠUTEDURU, ŠUTEDDURU – SMUCIĆ SIĘ, BYĆ SMUTNYM; BYĆ OMROCZONYM  
\[jako rzeczownik\] BYCIE SMUTNYM, SMUCENIE SIĘ; BYCIE ZAMROCZONYM  
EDERU(M) (i) lub ADARU – OBEJMOWAĆ SIĘ, WZIĄĆ W OBJĘCIA  
\[jako rzeczownik\] OBEJMOWANIE SIĘ, BRANIE W OBJĘCIA  
N: NENDURU(M) – OBEJMOWAĆ SIĘ (wzajemnie)  
\[jako rzeczownik\] OBEJMOWANIE SIĘ (wzajemne)  
ED€ŠU (i) (’dš) – BYĆ LUB STAWAĆ SIĘ NOWYM, ODNOWIĆ SIĘ  
\[jako rzeczownik\] BYCIE, STAWANIE SIĘ NOWYM, ODNAWIANIE SIĘ  
D: UDDUŠU – ODNOWIĆ, ODBUDOWAĆ, WYBUDOWAĆ NA NOWO, RESTAUROWAĆ  
\[jako rzeczownik\] ODNOWIENIE, ODBUDOWYWANIE, WYBUDOWANIE NA NOWO, RESTAUROWANIE  
EDIKU(M) – KOSZYKARZ  
EDILU(M) – ZAMYKAJĄCY  
EDIQU – UBIERAJĄCY (kogoś w coś)  
EDIRU – BOJĄCY SIĘ, OGARNIĘTY STRACHEM, LĘKAJĄCY SIĘ; ULEGAJĄCY ZAĆMIENIU (o ciele niebieskim)  
EDIRU(M) – OBEJMUJĄCY, BIORĄCY W OBJĘCIA  
EDIŠ – SAM (JEDEN)  
EDIŠU – ODNAWIAJĄCY SIĘ  
EDLU(M) – ZAMKNIĘTY  
EDLU lub EŢLU, EŢELLU, ETELLU – MĄŻ, MĘŻCZYZNA; CZŁOWIEK; BOHATER; PAN, WŁADCA, WŁAŚCICIEL,  
GOSPODARZ; „WŁADCA” \[jeden z przydomków boga Marduka); (później:} NIEWOLNIK  
ETEL ŠARRI – WŁADCA KRÓLÓW  
EDQU – UBRANY (przez kogoś w coś)  
EDRU – PRZESTRASZONY; BĘDĄCY W TRAKCIE ZAĆMIENIA (o ciele niebieskim)  
EDRU(M) – OBJĘTY, ZNAJDUJĄCY SIĘ W OBJĘCIACH, W UŚCISKU  
EDŠU – ODNOWIONY; NOWY  
EDŰ lub IDŰ, IDŰ (M), IDA’UM – ZNAĆ, ROZUMIEĆ, WIEDZIEĆ; DOWIADYWAĆ SIĘ, DOWIEDZIEĆ SIĘ; ZBADAĆ,  
POZNAWAĆ; POZNAĆ (kobietę); ZOBACZYĆ, WYBRAĆ  
\[jako rzeczownik\] ROZUMIENIE, POZNANIE, DOWIEDZENIE SIĘ, DOWIADYWANIE SIĘ; BADANIE; PO-  
ZNANIE {kobiety}; ZOBACZENIE, WYBRANIE  
D: UDDŰ – INFORMOWAĆ, OKREŚLAĆ, CZYNIĆ ZNANYM; BYĆ ROZPOZNAWALNYM  
\[jako rzeczownik\] INFORMOWANIE, OKREŚLANIE, ZAPOZNAWANIE; BYCIE ROZPOZNAWALNYM  
Š: ŠUDŰ – KAZAĆ WIEDZIEĆ, ZAWIADOMIĆ, POINFORMOWAĆ  
\[jako rzeczownik\] NAKAZANIE ZDOBYCIA WIEDZY, ZAWIADOMIENIE, POINFORMOWANIE  
INA IDU – ZA WIEDZĄ  
(często w listach:) NUDA – WIEMY  
EDU – KTOŚ; (z zaprzeczeniem) NIKT  
EGDU lub EQDU, EKDU – AROGANCKI, ZUCHWAŁY, PORYWCZY; WŚCIEKŁY, SZALONY  
EGIRRU – MYŚL, IDEA, PLAN; MYŚLENIE; WYJAŚNIENIE; ZNACZENIE \[sum.\]  
l.mn. EGIRRŰ  
EGŰ(M) – BYĆ LENIWYM, STAĆ SIĘ LENIWYM  
\[jako rzeczownik\] BYCIE LENIWYM, STAWANIE SIĘ LENIWYM  
EGŰ(M) (’gu) – MĘCZYĆ SIĘ; BYĆ OPIESZAŁYM; BYĆ lub STAWAĆ SIĘ NIEDBAŁYM, NIEUWAŻNYM, NIEOSTROŻ-  
NYM, ZMĘCZONYM, ZNUŻONYM, NIECHLUJNYM; (St) BYĆ OMINIĘTYM  
\[jako rzeczownik\] MĘCZENIE SIĘ; BYCIE OPIESZAŁYM; BYCIE, STAWANIE SIĘ NIEDBAŁYM, NIEUWAŻ-  
NYM, NIEUWAŻNYM, NIEOSTROŻNYM, ZMĘCZONYM, ZNUŻONYM, NIECHLUJNYM; BYCIE OMINIĘ-  
TYM  
GT: (to samo ze znaczeniem zwrotnym)  
M€GŰTU – NIEDBAŁOŚĆ, NIECHLUJSTWO  
ANA x EGŰ – MINĄC SIĘ Z x  
EGŰ(M) – LENIWY  
EGŰ(M) – ZMĘCZONY, OPIESZAŁY; NIEDBAŁY, NIEUWAŻNY, NIEOSTROŻNY, ZMĘCZONY, ZNUŻONY, NIECHLUJNY;  
OMINIĘTY  
EGUBBŰ – KROPIELNICA, NACZYNIE DO WODY ŚWIĘCONEJ \[sum.\]  
EHILTU \[starobab.\] lub E’ILTU – ZOBOWIĄZANIE DŁUŻNE  
EKA lub EKAMA – GDZIE?  
EKALLITU – DAMA DWORU, DAMA Z PAŁACU  
EKALLU(M) – PAŁAC; SIEDZIBA ADMINISTRACJI KRÓLEWSKIEJ \[KH\]; (przenośnie:) POTĘŻNA BUDOWLA  
BAB EKALLIM – BRAMA PAŁACU, WEJŚCIE DO PAŁACU; „BRAMA PAŁACU” \[nazwa części wątroby\]  
EKAMA lub EKA – GDZIE?  
EKCU lub AKCU – DZIKI, NIEOPANOWANY  
EKDU lub EQDU, EGDU – AROGANCKI, ZUCHWAŁY, PORYWCZY; WŚCIEKŁY, SZALONY  
EKEMU(M) (i) – ZABIERAĆ, KONFISKOWAĆ; BRAĆ DO NIEWOLI; (St) BYĆ ZMARNIAŁYM  
\[jako rzeczownik\] ZABIERANIE, KONFISKOWANIE; BRANIE DO NIEWOLI; BYCIE ZMARNIAŁYM  
EKIMU(M) – ZABIERAJĄCY, KONFISKUJĄCY; BIORĄCY DO NIEWOLI; GRABIEŻCA?  
EKKEMU lub IKKIMU – PORYWACZ \[nazwa demona\]  
EKLETU lub IKLITU – CIEMNOŚĆ  
EKLETU(M) – CIEMNOŚĆ, MROK  
EKMU(M) – ZABRANY, SKONFISKOWANY; WZIĘTY DO NIEWOLI; ZMARNIAŁY  
EKŰ lub AKŰ – SIEROTA NIE MAJĄCY OJCA (obraźliwe); BIEDAK, UBOGI, CIERPIĄCY BIEDĘ; ZŁAMANY, ZMAR-  
NOWANY  
r. ż AKŰTU, EKŰTU  
EKURRU – ŚWIĄTYNIA \[sum.\]; \[także nazwa własna świątyni Aszura Eszara – Lipin\]  
ELA – POZA  
ELÂNIŠ – KU GÓRZE  
ELANU – = ELENŰ (M)  
ELCU(M) – UCIESZONY, URADOWANY  
ELECU(M) (i) – CIESZYĆ SIĘ, RADOWAĆ SIĘ  
\[jako rzeczownik\] CIESZENIE SIĘ, RADOWANIE SIĘ  
D: ULLUCU(M) – DOPROWADZAĆ DO RADOŚCI, RADOWAĆ, CIESZYĆ  
\[jako rzeczownik\] DOPROWADZANIE DO RADOŚCI, RADOWANIE, CIESZENIE  
ELELU (i) – BYĆ CZYSTYM  
\[jako rzeczownik\] BYCIE CZYSTYM  
\[ELELU\] – zob. ULLULU  
ELEN lub ELENU – NA  
ELENITU – zob. ELENU  
ELENITU – (jakiegoś rodzaju) CZARY  
ELENU lub ELEN – NA  
ELENŰ lub ELENITU – WYŻSZY  
ELENŰ(M) – POWYŻEJ, NAD, U GÓRY  
ANA ELENŰM – DO GÓRY  
ELENUMMA – JESZCZE, OPRÓCZ TEGO  
ELEPPU(M), ELEPPU(M) lub 'ALAPPUM – STATEK, ŁÓDŹ \[r.ż.\]  
ELEPU – DRAPAĆ  
\[jako rzeczownik\] DRAPANIE  
GT: ETLUPU – STALE SIĘ DRAPAĆ  
\[jako rzeczownik\] DRAPANIE SIĘ BEZ USTANKU  
ELI, ELI – NA, NAD; PRZEZ; PRZECIW; POD, PRZY, OBOK; syn. MUHHI  
ELIŠU – NA NIM, NAD NIM; U NIEGO (mieć dług)  
ELIŠA – NA NIEJ, NAD NIĄ  
ELŠUNU = ELIŠUNU  
INA ELI – POD, PRZY, OBOK  
ELI ŠA – WIĘCEJ NIŻ  
ELI’AT – POZA  
ELICU(M) – CIESZĄCY SIĘ, RADUJĄCY SIĘ  
ELIPPU – STATEK; ŁÓDKA  
ELIPU – DRAPIĄCY  
ELIŠ – U GÓRY, W GÓRZE, NA GÓRZE  
ELLAMU – PRZED  
ELLATU – SIŁY ZBROJNE (szczególnie w liczbie mnogiej)  
ELLETU lub ELLU(M) – CZYSTY, NIESKAZITELNY, JASNY; BŁYSZCZĄCY, LŚNIĄCY; (przenośnie:) ŚNIEG  
ELLU(M), ELLU(M) lub ELLETU – j.w.  
ELPU – PODRAPANY, ZADRAPANY  
ELŰ (’lj) – WYSOKI, GÓRNY  
l.mn. ELŰTI  
ISSI… ELITA – ONA GŁOŚNO KRZYCZAŁA  
ELATI – NA GÓRZE  
ELŰ(M), ELŰ(M) (i) (’li) – IŚĆ DO GÓRY, KIEROWAĆ SIĘ DO GÓRY; BYĆ WYSOKIM; WYNURZAĆ SIĘ, POJA-  
WIAĆ SIĘ; WŁAZIĆ; WCHODZIĆ (z ANA lub Akk.); WZEJŚĆ; WYCHODZIĆ, WYJŚĆ; WYPŁYWAĆ, WY-  
NURZYĆ SIĘ; SKIEROWAĆ SIĘ; UJŚĆ, UCIEC  
\[jako rzeczownik\] WCHODZENIE NA GÓRĘ, KIEROWANIE SIĘ DO GÓRY; BYCIE WYSOKIM; WYNURZA-  
NIE SIĘ, POJAWIANIE SIĘ; WŁAŻENIE; WCHODZENIE; WZEJŚCIE; WYCHODZENIE, WYJŚCIE; WYPŁY-  
WANIE, WYNURZANIE SIĘ; KIEROWANIE SIĘ; UCHODZENIE, UCIEKANIE  
GT: jak G; PONIEŚĆ STRATĘ, STRACIĆ (wpłacone pieniądze); WYCHODZIĆ  
\[jako rzeczownik\] jak G, a ponadto PONIESIENIE STRATY, STRACENIE (pieniędzy); WYCHODZENIE  
D ULLŰ(M), ULLŰ(M): PODNOSIĆ, PODWYŻSZYĆ  
\[jako rzeczownik\] PODNOSZENIE, PODWYŻSZENIE  
Š: ŠULŰ(M), ŠULŰ(M): KAZAĆ PRZYBYĆ NA GÓRĘ, KAZAĆ PODNIEŚĆ; PODNIEŚĆ, PODNOSIĆ; CZY-  
NIĆ WYSOKIM, PODWYŻSZYĆ; PODNIEŚĆ WYSOKO; (także:) ODDALIĆ, ZABRAĆ SPRZĄTNĄĆ,  
USUNĄĆ; WYDOBYĆ (zatopiony statek; KH); ZOSTAWIĆ JAKO ZAŁOGĘ, GARNIZON  
\[jako rzeczownik\] WEZWANIE DO WEJŚCIA NA GÓRĘ, NAKAZANIE WEJŚCIA DO GÓRY, POLECENIE,  
NAKAZANIE PODNIESIENIA; PODNIESIENIE, PODNOSZENIE; PODWYŻSZENIE; PODNIESIENIE WYSO-  
KO; (także) ODDALENIE; ZABRANIE, SPRZĄTNIĘCIE, USUNIĘCIE; WYDOBYCIE, PODNIESIENIE (zato-  
pionego statku); POZOSTAWIENIE JAKO ZAŁOGI, GARNIZONU  
ŠT: WZNOSIĆ, WZNIEŚĆ, NIEŚĆ DO GÓRY  
\[jako rzeczownik\] WZNOSZENIE, WZNIESIENIE, NIESIENIE DO GÓRY  
INA x ELŰ – TRACIĆ x (o rzeczy)  
LA KATI ŠARRI ELŰ \[nowobab.\] – WYMKNĄĆ SIĘ RĘCE KRÓLA  
NAPARKAM UL ELLI – NIE MOGĘ SKOŃCZYĆ  
Odmiana 3 os. l.p. Prs: ILLI, (z V.) ILLIAM, Prt: ILI, Pf: ITELI, Tryb rozk. Š: ŠULI  
EMA – WSZĘDZIE GDZIE  
EMA – GDZIE \[określenie miejsca\]  
EMEDU(M), EMEDU(M) (i) – G (z Akk): OPIERAĆ SIĘ NA; POSTAWIĆ (coś na czymś); USTAWIĆ SIĘ, STANĄĆ; NAŁO-  
ŻYĆ (z 2 Akk. – komuś coś); SCHOWAĆ SIĘ, ZASZYĆ; UDERZYĆ SIĘ (o coś)  
\[jako rzeczownik\] OPIERANIE SIĘ (na czymś); POSTAWIENIE (czegoś na czymś); USTAWIANIE SIĘ, STANIĘ-  
CIE; NAŁOŻENIE (komuś czegoś); SCHOWANIE SIĘ, ZASZYCIE SIĘ; UDERZENIE SIĘ (o coś)  
N: NENMUDU(M), NENMUDU(M) OPIERAĆ SIĘ O SIEBIE, SKUPIĆ SIĘ BLISKO SIEBIE; SPOTYKAĆ SIĘ,  
SCHODZIĆ SIĘ; ZNISZCZYĆ SIĘ  
\[jako rzeczownik\] OPIERANIE SIĘ O SIEBIE, PODPIERANIE SIĘ WZAJEMNE; SKUPIENIE SIĘ BLISKO  
SIEBIE; SPOTYKANIE SIĘ, SCHODZENIE SIĘ; ZNISZCZENIE SIĘ  
ŠADAŠU EMEDU – (eliptycznie) ZASZYĆ SIĘ W SWEJ OSTATNIEJ KRYJÓWCE; (parafrastycznie) ZNIKNĄĆ,  
PRZEPAŚĆ BEZ WIEŚCI  
EMEDU ANA – WYLĄDOWAĆ, PRZYBIĆ  
EMDU(M), EMDU(M) – OPARTY NA, POSTAWIONY NA; USTAWIONY, WSTRZYMANY (w ruchu); NAŁOŻONY NA COŚ;  
SCHOWANY, ZASZYTY; UDERZONY O COŚ  
EMIDU(M), EMIDU(M) – OPIERAJĄCY SIĘ NA, STOJĄCY NA; USTAWIAJĄCY SIĘ, PRZYSTAWAJĄCY, STAJĄCY; NAKŁADA-  
JĄCY COŚ; CHOWAJĄCY SIĘ, ZASZYWAJĄCY SIĘ; UDERZAJĄCY SIĘ O COŚ  
EMITTU lub IMITTU – GRZYWNA; KARA; PODATEK  
EMQU – MĄDRY, ROZUMNY  
EMŰ (i) lub EWŰ – STAWAĆ SIĘ, BYĆ  
\[jako rzeczownik\] STAWANIE SIĘ, BYCIE, BYT, ISTNIENIE  
Š: ŠUMŰ – ROBIĆ, CZYNIĆ  
\[jako rzeczownik\] ROBIENIE, TWORZENIE, STWARZANIE  
EMŰ – STWORZONY, UCZYNIONY, UTWORZONY  
EMU(M) – TEŚĆ  
EMUQU, EMUQU – zob. 'EMŰQU  
ENANNA – TERAZ  
ENBU – OWOC; (metafor.) KSIĘŻYC  
ENENNA lub ENINNA – TERAZ  
ENENNAMA lub ENINNAMA – WRESZCIE TERAZ  
ENEQU (i) – SSAĆ  
\[jako rzeczownik\] SSANIE  
ENEŠU (i) – BYĆ SŁABYM  
\[jako rzeczownik\] BYCIE SŁABYM  
ENETTU(M) – GRZECH  
ENGURRU lub APŠU – OTCHŁAŃ WODNA  
ENGURRU – OTCHŁAŃ WODNA = APSU  
ENINNA lub ENENNA – TERAZ  
ENINNAMA lub ENENNAMA – WRESZCIE TERAZ  
ENIQU – SSAJĄCY; NIEMOWLĘ? OSESEK? KARMIONE ZWIERZĄTKO?  
EN.LIL, EN.LIL – ENLIL \[główny bóg panteonu sumeryjskiego\] \[sum.\]  
ENNITTU – GRZECH  
ENQU – WYSSANY  
ENŠU – SŁABY  
ENTU – ENTU \[kapłanka wyższej rangi\] \[sum.\]  
ENU\[nowobab.\] lub INU(M), ENU(M) – OKO; ŹRÓDŁO \[r.ż.\]  
l.mn. INATUM, ENATUM  
ENŰ (i) – ZMIENIAĆ, PRZEOBRAŻAĆ  
\[jako rzeczownik\] ZMIENIANIE, PRZEOBRAŻANIE  
\[ENŰ\] – zob. UTNENNU  
ENU – UGINAĆ, UCISKAĆ, POKONYWAĆ  
\[jako rzeczownik\] UGINANIE, UCISKANIE; POKONYWANIE  
ENU – ZMIENIONY, PRZEOBRAŻONY  
ENU – UGIĘTY, UCIŚNIĘTY, POKONANY  
ENU(M) \[asyr.\] lub INU(M), ENU – OKO; ŹRÓDŁO \[r.ż.\]  
l.mn. INATUM, ENATUM  
ENUMA – (WÓWCZAS, SKORO TYLKO) JAK; POTEM KIEDY; TERAZ; GDZIE  
ENZU(M) – KOZA  
EPAŠU(M) \[asyr.\] lub EPEŠU(M) – ROBIĆ, WYKONAĆ, ZREALIZOWAĆ; DOKONAĆ, SPRAWIĆ; TWORZYĆ; BUDO-  
WAĆ, WZNOSIĆ; ODBUDOWAĆ; POSTĘPOWAĆ; PODEJMOWAĆ, PRZEDSIĘBRAĆ; RODZIĆ, WYDAWAĆ  
NA ŚWIAT; TRAKTOWAĆ (kogoś); OTWORZYĆ (usta); UPRAWIAĆ (pole), UPRAWIAĆ, SADZIĆ (rośliny)  
\[jako rzeczownik\] ROBIENIE, WYKONYWANIE, REALIZOWANIE; DOKONYWANIE, SPRAWIANIE; TWO-  
RZENIE; BUDOWANIE, WZNOSZENIE; ODBUDOWYWANIE; POSTĘPOWANIE; PODEJMOWANIE,  
PRZEDSIĘBRANIE; RODZENIE, WYDAWANIE NA ŚWIAT; TRAKTOWANIE (kogoś); OTWARCIE (ust);  
UPRAWIANIE (pola); UPRAWIANIE, SADZENIE (roślin)  
GT ETPUŠU(M): (to samo co G ze znaczeniem zwrotnym)  
\[jako rzeczownik\] ROBIENIE SOBIE NAWZAJEM; POSTĘPOWANIE WOBEC SIEBIE, TRAKTOWANIE SIE-  
BIE  
GTN ITEPPUŠU(M): ZROBIĆ, STWORZYĆ, DOKONAĆ; NAROBIĆ, NAWYRABIAĆ, NAWYPRAWIAĆ  
\[jako rzeczownik\] ZROBIENIE, STWORZENIE; NAROBIENIE, NAWYRABIANIE, NAWYPRAWIANIE  
D: UPPUŠU(M) (to samo co G ze wzmocnionym znaczeniem); OWŁADNĄĆ, ZAWŁADNĄĆ, ZDOBYĆ  
\[jako rzeczownik\] OWŁADNIĘCIE, OPANOWANIE, ZDOBYCIE, ZAWŁADNIĘCIE  
HARRANAM EPAŠUM \[staroasyr.\] – PODEJMOWAĆ PODRÓŻ HANDLOWĄ  
DENU EPAŠU – ROZSTRZYGNĄĆ SPRAWĘ SĄDOWĄ  
BELUTU EPPUŠU – PANOWAĆ, SPRAWOWAĆ WŁADZĘ  
UPPIŠ x ILQE (ISSEQE) – x KUPIŁ  
EPEQU(M) (i) – ZARASTAĆ; OBEJMOWAĆ  
\[jako rzeczownik\] ZARASTANIE; OBEJMOWANIE  
D UPPUQU(M): OBROSNĄĆ, ZARASTAĆ SIĘ  
\[jako rzeczownik\] OBRASTANIE, ZARASTANIE SIĘ  
EPERU – KURZ, PYŁ  
EPEŠU (u) (’pš) – MYŚLEĆ, SĄDZIĆ (coś o czymś); LICZYĆ  
\[jako rzeczownik\] MYŚLENIE, SĄDZENIE; LICZENIE  
EP€ŠU (’pš) – ZACZAROWAĆ; OCZAROWAĆ, ZACHWYCIĆ  
\[jako rzeczownik\] ZACZAROWANIE; OCZAROWANIE, ZACHWYCENIE  
EPEŠU (a/u i inne) – (bez dopełnienia) DZIAŁAĆ, POSTĘPOWAĆ; (z dopełnieniem w Akk.) ROBIĆ, WYKONYWAĆ, CZYNIĆ;  
(także) BUDOWAĆ; (z podwójnym Akk. rzeczy) ROBIĆ COŚ Z CZEGOŚ; (z Akk. osoby i rzeczy) KOMUŚ COŚ  
ZROBIĆ; CZAROWAĆ  
QABLU EPEŠU – WALCZYĆ  
TAHAZU EPEŠU – TOCZYĆ BITWĘ  
CIBUTU EPEŠU – WYRAŻAĆ ŻYCZENIE  
ARDUTU EPEŠU – BRAĆ W NIEWOLĘ  
ISINNU EPEŠU – ŚWIĘTOWAĆ, CZCIĆ ŚWIĘTO  
NIQE EPEŠU – SKŁADAĆ OFIARĘ  
BITA EPEŠU – ZAKŁADAĆ RODZINĘ  
PAŠU EPEŠU – OTWORZYĆ MU USTA  
EPEŠU – CZAROWNIK  
EPEŠU(M), EPEŠU(M) (u; a/u i inne), EPAŠU(M) – ROBIĆ, WYKONAĆ, ZREALIZOWAĆ; DOKONAĆ, SPRAWIĆ; TWO-  
RZYĆ; BUDOWAĆ, WZNOSIĆ; ODBUDOWAĆ; POSTĘPOWAĆ; PODEJMOWAĆ, PRZEDSIĘBRAĆ; RODZIĆ,  
WYDAWAĆ NA ŚWIAT; TRAKTOWAĆ (kogoś); OTWORZYĆ (usta); UPRAWIAĆ (pole), UPRAWIAĆ, SA-  
DZIĆ (rośliny)  
\[jako rzeczownik\] ROBIENIE, WYKONYWANIE, REALIZOWANIE; DOKONYWANIE, SPRAWIANIE; TWO-  
RZENIE; BUDOWANIE, WZNOSZENIE; ODBUDOWYWANIE; POSTĘPOWANIE; PODEJMOWANIE,  
PRZEDSIĘBRANIE; RODZENIE, WYDAWANIE NA ŚWIAT; TRAKTOWANIE (kogoś); OTWARCIE (ust);  
UPRAWIANIE (pola); UPRAWIANIE, SADZENIE (roślin)  
GT ETPUŠU(M): (to samo co G ze znaczeniem zwrotnym)  
\[jako rzeczownik\] ROBIENIE SOBIE NAWZAJEM; POSTĘPOWANIE WOBEC SIEBIE, TRAKTOWANIE SIE-  
BIE  
GTN ITEPPUŠU(M): ZROBIĆ, STWORZYĆ, DOKONAĆ; NAROBIĆ, NAWYRABIAĆ, NAWYPRAWIAĆ  
\[jako rzeczownik\] ZROBIENIE, STWORZENIE; NAROBIENIE, NAWYRABIANIE, NAWYPRAWIANIE  
D: UPPUŠU(M) (to samo co G ze wzmocnionym znaczeniem); OWŁADNĄĆ, ZAWŁADNĄĆ, ZDOBYĆ  
\[jako rzeczownik\] OWŁADNIĘCIE, OPANOWANIE, ZDOBYCIE, ZAWŁADNIĘCIE  
HARRANAM EPAŠUM \[staroasyr.\] – PODEJMOWAĆ PODRÓŻ HANDLOWĄ  
DENU EPAŠU – ROZSTRZYGNĄĆ SPRAWĘ SĄDOWĄ  
BELUTU EPPUŠU – PANOWAĆ, SPRAWOWAĆ WŁADZĘ  
UPPIŠ x ILQE (ISSEQE) – x KUPIŁ  
MIMMA LA TEPPUŠ – NIE POWINIENEŚ NIC ROBIĆ  
EPINNU – PŁUG, PŁUG SIEWNY; KASJOPEA? \[nazwa jakiejś gwiazdy, może Kasjopei\] \[sum.\]  
EPIQU(M) (i) – ZARASTAJĄCY; OBEJMUJĄCY  
EPIŠTU – POSTĘPOWANIE; CZYN; DZIEŁO  
S.C. EPŠET, l.mn. EPŠETU  
EPIŠTU – CZARY; CZAROWNICA  
EPIŠU – MYŚLĄCY, SĄDZĄCY (coś o czymś), MYŚLICIEL? LICZĄCY, RACHMISTRZ?  
EPIŠU (’pš) – CZARUJĄCY; CZAROWNIK? OCZAROWUJĄCY, ZACHWYCAJĄCY; UWODZICIEL?  
EPIŠU(M), EPIŠU(M), EPIŠU(M) – ROBIĄCY, WYKONUJĄCY, SPRAWIAJĄCY, TWORZĄCY; BUDUJĄCY, WZNOSZĄCY;  
POSTĘPUJĄCY; PODEJMUJĄCY; RODZĄCY, WYDAJĄCY NA ŚWIAT; OTWIERAJĄCY (usta), UPRAWIAJĄCY  
(pole); SADZĄCY (rośliny); ROBOTNIK? PRACOWNIK? SPRAWCA? TWÓRCA? BUDOWNICZY? SIEWCA?  
EPPERU – OSŁABIENIE; BIEDA  
EPQU(M) (i) – ZAROŚNIĘTY, OBROŚNIĘTY; OBJĘTY  
EPRU – ZIEMIA; KURZ, PYŁ  
S.C. EPER  
EPRU – WYŻYWIENIE, WIKT  
S.C. EPER  
EPŠU lub IPŠU (’pš) – PRACA, ZAJĘCIE; BUDOWA; CZARY  
EPŠU – POMYŚLANY; LICZONY, POLICZONY  
EPŠU(M), EPŠU(M), EPŠU(M) – ZROBIONY, WYKONANY, SPRAWIONY, UTWORZONY; WYBUDOWANY, ZBUDOWANY,  
WZNIESIONY; PODJĘTY; URODZONY, WYDANY NA ŚWIAT; OTWIERANY (o ustach), UPRAWIANY (o polu);  
ZASADZONY (o roślinie)  
EPŰ (i) – PIEC  
\[jako rzeczownik\] PIECZENIE  
EPU – UPIECZONY  
EQDU lub EKDU, EGDU – AROGANCKI, ZUCHWAŁY, PORYWCZY; WŚCIEKŁY, SZALONY  
EQLU(M) – POLE; OBSZAR, TEREN  
l.mn. EQLETU, EQLATU \[asyr.\]  
REŠ EQLIM – CEL  
ERAŠU(M) \[staroakad.\] (a/u) lub EREŠU(M), 'ARAŠUM – UPRAWIAĆ (pole); SIAĆ  
\[jako rzeczownik\] UPRAWIANIE (pola); SIANIE  
GTN ITERRUŠU(M): UPRAWIAĆ KILKUKROTNIE  
\[jako rzeczownik\] WIELOKROTNE, STAŁE UPRAWIANIE  
ERBA – CZTERDZIEŚCI  
ERBEŠERŰ – CZTERNASTY  
ERBETTUM lub ARBA’U, IRBITTU, ERBŰ (M) – CZTERY \[r.ż.\]  
S.A.: ERBE, ERBET, r.m. ERBŰ(M)  
ANA ERBIŠU – CZTERY RAZY  
ŠAR ERBETTIM – W CZTERECH STRONACH ŚWIATA; WSZĘDZIE  
ERBET NACMADI – CZWÓRKA, ZAPRZĘG CZTEROKONNY  
ERBU – \[składnik zwrotu\]  
EREB ŠAMŠI – ZACHÓD (strona świata)  
ERBŰ(M) lub ARBA’U, IRBITTU, ERBETTUM – CZTERY  
S.A.: ERBE, ERBET, ERBA r.ż. ERBETTUM  
ANA ERBIŠU – CZTERY RAZY  
ŠAR ERBETTIM – W CZTERECH STRONACH ŚWIATA; WSZĘDZIE  
ERBET NACMADI – CZWÓRKA, ZAPRZĘG CZTEROKONNY  
ERBU(M) – DANINA, DOCHÓD  
ERBŰ(M) – SZARAŃCZA  
ERCETU(M) – OBSZAR, KRAJ; ZIEMIA, GRUNT; ŚWIAT PODZIEMNY  
EREBU, EREBŰ lub ARBU, ARIBU, ERIBU – SZARAŃCZA; STADO SZARAŃCZY  
EREBU(M) (u) – WCHODZIĆ, WSTĘPOWAĆ (z Akk. miejsca); WTARGNĄĆ  
\[jako rzeczownik\] WCHODZENIE, WSTĘPOWANIE; WTARGNIĘCIE  
GT ETRUBU(M): WEJŚĆ NA ZAWSZE  
\[jako rzeczownik\] WCHODZENIE NA ZAWSZE, PRZYBYCIE NA STAŁE  
GTN ITERRUBU(M): CZĘSTO ODWIEDZAĆ  
\[jako rzeczownik\] REGULARNE ODWIEDZANIE  
Š ŠURUBU(M): WPUŚCIĆ, KAZAĆ WEJŚĆ, WNIKNĄĆ, WTARGNĄĆ; PRZEPUSZCZAĆ; PUSZCZAĆ PŁA-  
ZEM?  
\[jako rzeczownik\] NAKAZANIE, POLECENIE WEJŚCIA; WNIKNIĘCIE, WTARGNIĘCIE; PRZEPUSZCZANIE;  
PUSZCZANIE PŁAZEM? DAROWANIE?  
ŠTN ŠUTERRUBU(M): WCHODZIĆ WCIĄŻ NA NOWO, RAZ PO RAZ  
\[jako rzeczownik\] WCHODZENIE RAZ PO RAZ, CZĘSTE  
ANA LIBBIM EREBU(M) – PASOWAĆ (do czegoś); WKRACZAĆ  
EREPU(M) (u) – CHMURZYĆ SIĘ  
\[jako rzeczownik\] CHMURZENIE SIĘ  
EREŠU – ŻYWICA Z DRZEWA; PRZYJEMNY, MIŁY ZAPACH  
EREŠU – ZASZCZEPIAĆ  
\[jako rzeczownik\] ZASZCZEPIANIE  
EREŠU(M) (i) lub 'ARAŠUM, ERAŠU(M) – UPRAWIAĆ (pole); SIAĆ  
\[jako rzeczownik\] UPRAWIANIE (pola); SIANIE  
GTN ITERRUŠU(M): UPRAWIAĆ KILKUKROTNIE  
\[jako rzeczownik\] WIELOKROTNE, STAŁE UPRAWIANIE  
EREŠU(M), EREŠU(M) (i) lub 'ARAŠUM – ŻYCZYĆ, ŻĄDAĆ, WYMAGAĆ; PROSIĆ, WYPRASZAĆ, (z podwójnym Akk.  
osoby i rzeczy) PROSIĆ (kogoś o coś)  
\[jako rzeczownik\] ŻYCZENIE, ŻĄDANIE, WYMAGANIE; WYPRASZANIE, PROSZENIE  
ERIATI – CIĘŻARNE KOBIETY  
ERIBU, ERIBŰ lub ARBU, ARIBU, EREBU – SZARAŃCZA; STADO SZARAŃCZY  
ERIBU(M) – WSTĘPUJĄCY, WCHODZĄCY  
ERIJATU – TRĄBA POWIETRZNA (szczególnie w zimnej porze roku) \[l.mn.\]  
ERIMMATU – NASZYJNIK  
ERINNU – CEDR \[sum.\]  
ERINU lub ERNU – CEDR  
ERIPU(M) – CHMURZĄCY SIĘ  
ERIQQU – WÓZ (do przewozu ciężarów) \[rzeczownik występujący w obu rodzajach: męskim i żeńskim\]  
ERIŠTU(M) – ŻYCZENIE, ŻĄDANIE; POTRZEBA, ŻYCZENIE; ? \[pewna cecha wątroby\]  
S.C. ERŠAT  
ERIŠU- ZASZCZEPIAJĄCY  
ERIŠU(M), ERIŠU(M) – ŻADAJĄCY, ŻYCZĄCY, WYMAGAJĄCY; PROSZĄCY; SUPLIKANT? INTERESANT?  
ERIŠU(M), ERIŠU(M), ERIŠU(M) – UPRAWIAJĄCY (pole), ROLNIK? SIEJĄCY, SIEWCA?  
ERITU – CIĘŻARNA KOBIETA  
ERNU lub ERINU – CEDR  
ERPITU lub IRPITU, URPATU – CHMURA, OBŁOK  
ERPU(M) – ZACHMURZONY  
ERREŠU – UPRAWIAJĄCY (pole), GOSPODARUJĄCY (na polu); GOSPODARZ (pola)  
ERREŠUTU – UPRAWA  
ERRU lub CERU(M) – WĄŻ  
ERRU(M) – JELITO; (w l.mn.) WNĘTRZNOŚCI  
ERŠU – ZASZCZEPIONY  
ERŠU – UPRAWIONY  
ERŠU(M), ERŠU(M) – ŻĄDANY, POŻĄDANY, WYMAGANY; WYPROSZONY  
ERŰ lub ARŰ – ORZEŁ  
ERŰ (i) – BYĆ W CIĄŻY  
\[jako rzeczownik\] BYCIE W CIĄŻY  
ERŰ – MIEDŹ  
ERU – MIEDŹ; BRĄZ  
€RU(M) – CZUWAĆ  
\[jako rzeczownik\] CZUWANIE  
ESEHU(M) (i) – PRZYDZIELIĆ  
\[jako rzeczownik\] PRZYDZIELANIE  
ESEPU (i) – SZUFLOWAĆ; ZAMIATAĆ NA KUPĘ  
\[jako rzeczownik\] SZUFLOWANIE; ZAMIATANIE NA KUPĘ  
ESERU(M) (i) – ZAMYKAĆ; UNIERUCHOMIĆ? TRZYMAĆ NA UWIĘZI?; ZAFASCYNOWAĆ, PRZYKUĆ (czyjąś uwagę);  
(z Akk. osoby) ŚCIĄGAĆ OD (od kogoś pieniądze)  
\[jako rzeczownik\] ZAMYKANIE; UNIERUCHAMIANIE? TRZYMANIE NA UWIĘZI? ZAFASCYNOWANIE,  
PRZYKUWANIE (uwagi), ŚCIĄGANIE (należności), EGZEKWOWANIE  
D USSURU(M): ZAMYKAĆ  
\[jako rzeczownik\] ZAMYKANIE  
DT UTESSURU(M): BYĆ ZAMKNIĘTYM  
\[jako rzeczownik\] BYCIE ZAMKNIĘTYM  
ESIHU(M) – PRZYDZIELAJĄCY  
ESIPU – SZUFLUJĄCY; ZAMIATAJĄCY NA KUPĘ  
ESIRU(M) – ZAMYKAJĄCY; UNIERUCHAMIAJĄCY? TRZYMAJĄCY NA UWIĘZI? FASCYNUJĄCY, PRZYKUWAJĄCY UWAGĘ;  
ŚCIĄGAJĄCY, EGZEKWUJĄCY (należności); POBORCA?  
ESHU(M) – PRZYDZIELONY  
ESPU – ZAMIECIONY NA KUPĘ  
ESRU(M) – ZAMKNIĘTY, UNIERUCHOMIONY? TRZYMANY NA UWIĘZI? ZAFASCYNOWANY; ŚCIĄGNIĘTY (o należności)  
€Š – DOKĄD  
EŠER – DZIESIĘĆ  
EŠERTU lub EŠRU – DZIESIĘĆ \[r.ż.\]  
EŠERU(M) (i) lub \[AŠARU\] – BYĆ W PORZĄDKU, BYĆ DOKŁADNIE; IŚĆ WPROST, IŚĆ PROSTO; RZUCAĆ SIĘ (na  
kogoś, coś np. z bronią); (o ziemiopłodach) ROSNĄĆ, ROZWIJAĆ SIĘ; BYĆ PROSTYM, BYĆ RÓWNYM; WY-  
PROSTOWAĆ; BŁOGOSŁAWIĆ  
\[jako rzeczownik\] BYCIE W PORZĄDKU, BYCIE DOKŁADNYM; CHODZENIE, POSUWANIE SIĘ WPROST,  
DO PRZODU; RZUCANIE SIĘ (na kogoś np. z bronią); WZRASTANIE, ROZWIJANIE SIĘ (o ziemiopłodach);  
BYCIE PROSTYM, BYCIE RÓWNYM; PROSTOWANIE; BŁOGOSŁAWIENIE  
Š UŠŠURU(M): DOPROWADZAĆ DO PORZĄDKU, PORZĄDKOWAĆ; POKRZEPIAĆ  
\[jako rzeczownik\] DOPROWADZANIE DO PORZĄDKU, PORZĄDKOWANIE; POKRZEPIANIE  
ŠT ŠUTEŠURU(M): KAZAĆ BYĆ PRAWEM; ZAPROWADZAĆ ŁAD, UTRZYMYWAĆ W PORZĄDKU  
\[jako rzeczownik\] USTANOWIENIE PRAWEM; ZAPROWADZENIE ŁADU, UTRZYMYWANIE W PORZĄDKU  
EŠERUM ANA PANI – PODCHODZIĆ KU  
HARRANA EŠERU lub HARRANA ŠUTEŠURU – MASZEROWAĆ PROSTO (PRZED SIEBIE)  
EŠGALLU – PAŁAC \[poet.\]  
EŠIRTU – ŚWIĄTYNIA  
EŠIRU(M) – IDĄCY, POSUWAJĄCY SIĘ NA WPROST; RZUCAJĄCY SIĘ (na kogoś); ROSNĄCY, WZRASTAJĄCY (o roślinie);  
PROSTUJĄCY; BŁOGOSŁAWIĄCY  
EŠITU – ZAMIESZANIE, CHAOS  
EŠRA – DWADZIEŚCIA  
EŠRA MANA KASPAM – 20 MIN SREBRA  
EŠRU lub EŠERTU – DZIESIĘĆ  
r.ż. EŠERTU  
EŠRU – DZIESIĄTY  
r.ż. EŠURTUM  
EŠRŰ – DWUDZIESTY  
EŠRU(M) – PORZĄDNY; DOKŁADNY; WYROSŁY, ROZWINIĘTY, ROZKWITŁY; PROSTOWANY; BŁOGOSŁAWIONY  
EŠŠEŠU- ŚWIĘTO \[sum.\]  
UMU EŠŠEŠU – ŚWIĄTECZNY DZIEŃ MIESIĄCA  
EŠŠU – NOWY  
EŠŠUTU – NOWOŚĆ, COŚ NOWEGO  
EŠŰ (i) – MIESZAĆ, GMATWAĆ, MĄCIĆ  
\[jako rzeczownik\] MIESZANIE, GMATWANIE, MĄCENIE  
EŠŰ – ZAMIESZANY, POGMATWANY, ZAMĄCONY  
EŠU lub UŠU – DRZEWO HEBANOWE  
ETELLU lub EDLU, EŢLU, EŢELLU – MĄŻ, MĘŻCZYZNA; CZŁOWIEK; BOHATER; PAN, WŁADCA, WŁAŚCICIEL,  
GOSPODARZ; „WŁADCA” \[jeden z przydomków boga Marduka); (później:} NIEWOLNIK  
ETEL ŠARRI – WŁADCA KRÓLÓW  
ETEQU – POSUWAĆ SIĘ (NAPRZÓD)  
\[jako rzeczownik\] POSUWANIE SIĘ (NAPRZÓD)  
ETEQU(M) (i) – PRZEKRACZAĆ, PRZECHODZIĆ; WYSTAWAĆ NAD; MIJAĆ (o terminie)  
\[jako rzeczownik\] PRZEKRACZANIE, PRZECHODZENIE; WYSTAWANIE NAD; MIJANIE (terminu)  
MAMITA ETEQU – ZŁAMAĆ PRZYSIĘGĘ  
ETIQU – POSUWAJĄCY SIĘ (NAPRZÓD)  
ETIQU(M) – PRZEKRACZAJĄCY, PRZECHODZĄCY; WYSTAJĄCY NAD; MIJAJĄCY, UPŁYWAJĄCY (termin)  
ETQU(M) – PRZEKROCZONY; WYSTAWIONY, WYSUNIĘTY NAD; UBIEGŁY (termin)  
EŢELLU lub EDLU, EŢLU, ETELLU – MĄŻ, MĘŻCZYZNA; CZŁOWIEK; BOHATER; PAN, WŁADCA, WŁAŚCICIEL,  
GOSPODARZ; „WŁADCA” \[jeden z przydomków boga Marduka); (później:} NIEWOLNIK  
ETEL ŠARRI – WŁADCA KRÓLÓW  
EŢERU (i) – OCALAĆ, RATOWAĆ  
\[jako rzeczownik\] OCALANIE, RATOWANIE  
INA x EŢERU – RATOWAĆ Z (jakiejś sytuacji)  
EŢERU – OCALAĆ, RATOWAĆ, BRONIĆ  
\[jako rzeczownik\] OCALANIE, RATOWANIE, BRONIENIE  
EŢIRU, EŢIRU – OCALAJĄCY, RATUJĄCY, BRONIĄCY; OBROŃCA? RATOWNIK?  
EŢIMMU – WIDMO, DUCH, MARA; DUCH ZMARŁEGO \[sum.\]  
EŢLU lub EDLU, EŢELLU, ETELLU – MĄŻ, MĘŻCZYZNA; CZŁOWIEK; BOHATER; PAN, WŁADCA, WŁAŚCICIEL,  
GOSPODARZ; „WŁADCA” \[jeden z przydomków boga Marduka); (później:} NIEWOLNIK  
ETEL ŠARRI – WŁADCA KRÓLÓW  
EŢRU, EŢRU – OCALONY, URATOWANY, OBRONIONY  
EŢŰ (i) (’t’) – OWIJAĆ, ZAWIJAĆ, OTULAĆ; POKRYWAĆ, ZAKRYWAĆ; OTACZAĆ CAŁĄ CHMARĄ; BYĆ CIEMNYM,  
MROCZNYM  
EŢŰ – OWINIĘTY, ZAWINIĘTY, OTULONY; POKRYTY, ZAKRYTY; OTOCZONY CALĄ CHMARĄ; CIEMNY, MROCZNY  
EŢUTU – CIEMNOŚĆ  
EWŰ (i) lub EMŰ – STAWAĆ SIĘ, BYĆ  
\[jako rzeczownik\] STAWANIE SIĘ, BYCIE  
Š: ROBIĆ, CZYNIĆ  
\[jako rzeczownik\] ROBIENIE, CZYNIENIE  
EZBU(M) – PORZUCONY, RZUCONY, OPUSZCZONY  
EZEBU(M) (i) – PORZUCAĆ, RZUCIĆ, ZOSTAWIAĆ, OPUSZCZAĆ  
\[jako rzeczownik\] PORZUCANIE, RZUCANIE, OPUSZCZANIE  
Š=ŠT ŠUZUBU(M), ŠUTEZUBU(M): PRZYMUSIĆ; SPORZĄDZIĆ (dokument); RATOWAĆ (życie)  
\[jako rzeczownik\] PRZYMUSZENIE; SPORZĄDZENIE, WYSTAWIENIE (dokumentu); RATOWANIE (życia)  
AŠŠATA EZEBU – BRAĆ ROZWÓD Z ŻONĄ  
KUNUKKA EZEBU lub ŢUPPA EZEBU – SPORZĄDZIĆ, WYSTAWIĆ DOKUMENT (KUNUKKA – DOKU  
MENT Z PIECZĘCIĄ)  
KUNUKKA (ŢUPPA) ŠUZUBU – SPORZĄDZIĆ DOKUMENT  
NAPIŠTA ŠUZUBU – RATOWAĆ ŻYCIE  
EZERU (i) – KLĄĆ, PRZEKLINAĆ  
\[jako rzeczownik\] PRZEKLINANIE, RZUCANIE PRZEKLEŃSTW  
EZEZU (i) – ROZZŁOŚCIĆ SIĘ (z Akk. – NA)  
\[jako rzeczownik\] ZŁOSZCZENIE SIĘ  
EZIBU(M) – PORZUCAJĄCY, RZUCAJĄCY, OPUSZCZAJĄCY  
EZIRU – KLNĄCY, PRZEKLINAJĄCY  
EZIZU – ZŁOSZCZĄCY SIĘ  
EZRU – PRZEKLEŃSTWO  
EZUZZU(M) – ZŁOŚĆ  
EZZU – WŚCIEKŁY, ZŁY, ROZZŁOSZCZONY

G  
GABBU – CAŁOŚĆ  
GABDIBBU – KRĄG BLANKÓW, GÓRNA KRAWĘDŹ BUDOWLI \[sum.\]  
GABRU lub MIHRU – ODPOWIEDNI; RÓWNY; KOPIA itp.  
GABRŰ – RYWAL \[sum.\]  
GABRU lub MAHIRU – RÓWNY URODZENIEM  
GADU – WESPÓŁ Z, WRAZ Z  
GAGU – PIERŚCIEŃ?  
GAGŰ – GAGU \[rodzaj klasztoru lub kolegium stanowiącego czasem aż część miasta\] \[sum.\]  
\[GALABU\] – zob. GULLUBU  
GALATU (u) – BAĆ SIĘ, LĘKAĆ; TRZĄŚĆ SIĘ (ze strachu)  
\[jako rzeczownik\] BANIE SIĘ, LĘKANIE SIĘ; TRZĘSIENIE SIĘ (ze strachu)  
GALITTU lub GALTU – OKROPNY, PRZERAŻAJĄCY  
GALITU – BOJĄCY, LĘKAJĄCY SIĘ; TRZĘSĄCY SIĘ (ze strachu)  
GALLABU – GOLIBRODA  
GALLATU – ? \[epitet o niejasnym znaczeniu odnoszący się jakoś do TAMTU – morza\]; zob. GALATU  
GALLU \[nowobab.\] lub KALLU – SŁUGA; NIEWOLNIK; POMOCNIK itd.  
GALLŰ – DIABEŁ, CZART \[sum.\]  
GALTU lub GALITTU – OKROPNY, PRZERAŻAJĄCY  
GAMALU (i) – CHRONIĆ, OCHRANIAĆ  
\[jako rzeczownik\] CHRONIENIE, OCHRANIANIE  
GAMARU(M), GAMARU(M) (a/u) – DOPROWADZIĆ DO KOŃCA, DOKOŃCZYĆ; ZAKOŃCZYĆ, SPEŁNIĆ, WYPEŁNIĆ;  
BYĆ ZAKOŃCZONYM; ZNISZCZYĆ, ZNIWECZYĆ  
\[jako rzeczownik\] DOPROWADZENIE DO KOŃCA, DOKOŃCZENIE; ZAKOŃCZENIE; SPEŁNIENIE, WY-  
PEŁNIENIE; BYCIE ZAKOŃCZONYM; ZNISZCZENIE, ZNIWECZENIE  
GT GITMURU(M), GITMURU(M): DOKONAĆ; PRZEPROWADZAĆ, ODBYĆ (coś w czasie)  
\[jako rzeczownik\] DOKONANIE; PRZEPROWADZANIE, ODBYCIE (czegoś w czasie)  
D GUMMURU(M) lub GAMMURU(M) {asyr.}: CZYNIĆ KOMPLETNYM, ZUPEŁNYM, DOPEŁNIĆ, UZU-  
PEŁNIĆ; ZAKOŃCZYĆ; ZADAWALAĆ  
\[jako rzeczownik\] CZYNIENIE KOMPLETNYM, ZUPEŁNYM, DEPEŁNIANIE, UZUPEŁNIANIE; KOŃCZENIE;  
ZADAWALANIE  
INA x GAMARU – UPORAĆ SIĘ Z x  
GAMILU – CHRONIĄCY, OCHRANIAJĄCY  
GAMIRU(M), GAMIRU(M) (a/u) – DOPROWADZAJĄCY DO KOŃCA, KOŃCZĄCY; SPEŁNIAJĄCY, WYPEŁNIAJĄCY;  
NISZCZĄCY, NIWECZĄCY  
GAMLU – CHRONIONY, OCHRANIANY  
GAMMALU – WIELBŁĄD \[zapożyczenie z zachodniosemickiego\]  
GAMMURU(M) -\[asyr.\]; zob. GAMARU(M)  
GAMRU(M) – DOKONANY, SKOŃCZONY, DOPROWADZONY DO KOŃCA; ZUPEŁNY, CAŁKOWITY; DOKŁADNY,  
SZCZEGÓŁOWY; CAŁY; WSZYSCY, WSZYSTKO, W CAŁOŚCI  
S.C. GAMIR  
UMU GAMRUTUM – DNI SĄ DOKONANE  
ANA ŠIM GAMER – ZA PEŁNĄ CENĘ  
ANA GAMRI(M) – KOMPLETNIE, ZUPEŁNIE, CAŁKOWICIE  
GANA lub AGANA – NUŻE! UWAGA! (wł. UWAŻAJ!) \[wykrzyknik\]  
GAPPU lub AGAPPU – SKRZYDŁO  
GAPŠU – MASOWY; OBSZERNY; POTĘŻNY; por. GIPŠU  
GARARU (a/u) – UKŁADAĆ SIĘ W FALE, FALOWAĆ  
\[jako rzeczownik\] UKŁADANIE SIĘ W FALE, FALOWANIE  
GARIRU – UKŁADAJĄCY SIĘ W FALE, FALUJĄCY  
GARRU – POFALOWANY  
GARŰ (i) lub GERŰ – UZNAWAĆ, POCZYTYWAĆ, UWAŻAĆ  
{jako rzeczownik\] UZNAWANIE, POCZYTYWANIE, UWAŻANIE  
GARŰ – WRÓG, NIEPRZYJACIEL \[imiesłów\]  
GARURU – FALOWANIE (wody)  
GAŠIŠU(M) – PAL, SŁUP, DRĄG  
INA GAŠIŠIM ŠAKANUM – WBIJAĆ NA PAL  
GAŠRU – SILNY, MOCNY  
GEGUNŰ(M) – WYSOKA ŚWIĄTYNIA; WIEŻA ŚWIĄTYNNA  
GERRU(M) – PODRÓŻ, PODRÓŻ HANDLOWA; DROGA; WYPRAWA  
l.mn. GERRETU  
GERŰ (i) lub GARŰ – UZNAWAĆ, POCZYTYWAĆ, UWAŻAĆ  
{jako rzeczownik\] UZNAWANIE, POCZYTYWANIE, UWAŻANIE  
GILGILDÂNU – SOWA? \[jakiś ptak gnieżdżący się w wąwozach, rozpadlinach\]; zob. SUDINNU  
GIMILLU – OCHRONA, OCHRANIANIE  
GIMILLI x TURRU – MŚCIĆ SIĘ, ODPŁACAĆ x  
GIMILLUM – DOBRODZIEJSTWO  
GIMIRTU – CAŁOŚĆ, OGÓŁ  
GIMRU – POWSZECHNOŚĆ; CAŁOKSZTAŁT, OGÓŁ; WSZYSCY, WSZYSTKO, W CAŁOŚCI; CAŁY  
s.c. GIMRI \[poet.\], l.mn. GIMRETU, s.c. GIMIR, l.pdw. GIMRÂ, l.mn. GEMRŰTI (?), r.ż. GIMIRTU, s.c. r.ż.  
GIMRAT, l.mn. r.ż. GIMRÂTI (?)  
ANA PÂT GIMRI – W CAŁOŚCI  
GIMIR IC€ – DRZEWA WSZELKIEGO RODZAJU  
GINÂ – STALE, CIĄGLE \[sum.\]; zob. GINU (N)  
KIMA ŠA GIN€ – JAK ZWYKLE  
GINŰ – WIECZNIE, ZAWSZE; (w l.mn.) STAŁE, REGULARNE SKŁADANIE OFIAR; OKRESOWA OFIARA \[sum.?\]; zob.  
GINA  
l.mn. GINŰ, GIN€  
ANA GIN€ lub INA GIN€ – NA ZAWSZE, NA WIECZNOŚĆ; REGULARNIE  
GIPŠU – MASA, WIELE; por. GAPŠU  
GIPŠUTU – MASOWOŚĆ  
GIRRANU – SKARGA, BIADANIE, LAMENT \[sum.\]  
GIRRU = GERRU  
GIRRU – OGIEŃ  
GIRRU – LEW? TYGRYS?  
GIRSEQU – DWORZANIN \[sum.\]  
GIŠ – GILGAMESZ \[imię herosa\]  
GIŠIMMARU – PALMA DAKTYLOWA \[sum.\]  
GIŠRU – MOST  
GITMALU, GITMALU – DOSKONAŁY; JEDNORODNY, JEDNOLITY  
GIZILLŰ – POCHODNIA, ŻAGIEW \[sum.\]  
GUGALLU lub AŠAREDU – GŁÓWNY  
GUGALLU – GUGALLU \[urzędnik zajmujący się regulacją wody\] \[sum.\]  
GUHLU – ANTYMON  
GULA – GULA \[imię bogini zdrowia?\]  
GULLUBU – GOLIĆ  
\[jako rzeczownik\] GOLENIE  
GULLUBU – OGOLONY  
GUMAHHU – „WIELKI BYK”, BYK OFIARNY \[sum.\]  
GUMMURU(M) – KOMPLETNY, ZUPEŁNY, DOPEŁNIONY, UZUPEŁNIONY; ZAKOŃCZONY; ZADOWOLONY  
GUNATUM – GUNATUM \[nazwa miejscowości położonej w Babilonii\]  
GUQQANŰ – ? \[rodzaj ofiary\] \[sum.\]  
GUŠURU – BELKA  
GUZALŰ – „NOSICIEL TRONU” \[nazwa urzędnika działającego także wśród bogów\] \[sum.\]

H  
HA’ALTU lub HAJALTU – ODDZIAŁ WOJSKA, HUFIEC  
HA’AŢU lub HAJAŢU – SZPICEL  
HA’ILU – KAPIĄCY, KROPIĄCY  
HA’IQU – ŚCIŚLE PRZYLEGAJĄCY  
HA’IRU lub HAWIRU(M) – KOCHANEK, AMANT; (pierwszy) MAŁŻONEK  
HA’IRU, HA’IRU – WYBIERAJĄCY, OBIERAJĄCY, DOBIERAJĄCY (zwłaszcza kandydatkę na żonę); WYPATRUJĄCY  
HA’IŠU – PRZYSPIESZAJĄCY  
HÂ’IŢU(M) (hit) – PILNUJĄCY, NADZORUJĄCY; ŚLEDZĄCY, BADAJĄCY, ODKRYWAJĄCY; KONTROLUJĄCY PODCZAS  
PRZECHADZANIA SIĘ  
HABALU (a/u) – SZKODZIĆ, KRZYWDZIĆ; RABOWAĆ, GRABIĆ; (KH:) POZBAWIAĆ PRAWA  
\[jako rzeczownik\] SZKODZENIE, KRZYWDZENIE; RABOWANIE, GRABIENIE; POZBAWIANIE PRAWA  
HABALU(M) – GWAŁT, PRZEMOC, UCISK; BEZPRAWIE; NIESZCZĘŚCIE  
HABATTU – RURA ODPŁYWOWA, RURA KANALIZACYJNA  
l.mn. HABANATU  
HABATU(M) (a/u) – RABOWAĆ, PLĄDROWAĆ  
\[jako rzeczownik\] RABOWANIE, PLĄDROWANIE  
DT HUTABBUTU(M): BYĆ RABOWANYM  
\[jako rzeczownik\] BYCIE RABOWANYM  
HABATU(M) (a/u) – PRZEWĘDROWAĆ, PRZEJECHAĆ  
\[jako rzeczownik\] PRZEWĘDROWANIE, PRZEJECHANIE  
HABBATU – ROZBÓJNIK, BANDYTA  
HABBILU – RABUŚ, ROZBÓJNIK, ZBÓJ  
HABBURRU – ŁODYGA TRZCINY; SADZONKA \[sum.\]  
HABILU(M) – ZŁOCZYŃCA, ROZBÓJNIK  
HABITU(M) – PRZEJEŻDŻAJĄCY  
HABLU – POKRZYWDZONY; ZRABOWANY, ZAGRABIONY; POZBAWIONY PRAWA  
HABTU – OBRABOWANY, OGRABIONY  
HABTU(M) – PRZEJECHANY  
\[HABU\] – zob. HUBBU  
HABŰ – CZERPAĆ, NABIERAĆ  
\[jako rzeczownik\] CZERPANIE, NABIERANIE  
HACABU (a/u) – ODCINAĆ, UCINAĆ (rurę)  
\[jako rzeczownik\] ODCINANIE, UCINANIE (rury)  
HACBU – ODCIĘTY, UCIĘTY (o rurze)  
HACCINNU – SIEKIERA  
HACIBU – ODCINAJĄCY, UCINAJĄCY (rury)  
HADADU (u) – ZGINAĆ SIĘ, GIĄĆ  
\[jako rzeczownik\] ZGINANIE SIĘ, GIĘCIE  
HADDU – ZGIĘTY, UGIĘTY  
HADIDU – ZGINAJĄCY SIĘ, GNĄCY  
HADIŠ – RADOŚNIE  
HADITU – ZAZDROŚNICA  
HADŰ – ZAZDROŚNIK  
HADŰ(M) – RADOŚĆ  
HADŰ(M) (u) – CIESZYĆ SIĘ  
\[jako rzeczownik\] CIESZENIE SIĘ  
GTN HITADDU(M): BEZ PRZERWY, WCIĄŻ NA NOWO CIESZYĆ SIĘ  
\[jako rzeczownik\] CIESZENIE SIĘ BEZ USTANKU  
ANA x HADŰ – CIESZYĆ SIĘ NA x  
HADŰ(M) – CIESZĄCY SIĘ  
HAJALTU lub HA’ALTU – ODDZIAŁ WOJSKA, HUFIEC  
HAJAŢU lub HA’AŢU – SZPICEL  
HAKAMU (i) – ROZUMIEĆ  
\[jako rzeczownik\] ROZUMIENIE  
HAKIMU – ROZUMIEJĄCY; MĘDRZEC?  
HAKMU – ZROZUMIANY  
HALACU(M) (a/u) – WYCISNĄĆ  
\[jako rzeczownik\] WYCIŚNIĘCIE  
HALALU (a/u) – TKWIĆ (w czymś)?  
\[jako rzeczownik\] TKWIENIE  
I’ALLALUŠU (N) – (KH:) ONI MU NARZUCILI  
HALAPU (u) – BYĆ ODZIANYM, BYĆ UBRANYM; BYĆ WYPOSAŻONYM  
\[jako rzeczownik\] BYCIE ODZIANYM, BYCIE UBRANYM; BYCIE WYPOSAŻONYM  
HALAQU(M) (i) – GINĄĆ, ZGINĄĆ; MARNIEĆ; DAWAĆ ZA WYGRANĄ; UCIEC; (St:) NIE BYĆ, NIE ISTNIEĆ  
\[jako rzeczownik\] GINIĘCIE, MARNIENIE; DAWANIE ZA WYGRANĄ, REZYGNOWANIE; UCIEKANIE  
D HULLUQU(M): RUJNOWAĆ, NISZCZYĆ  
\[jako rzeczownik\] RUJNOWANIE, NISZCZENIE  
INA x HALAQU – UCIEC Z x  
HALCU – CZYSTY (o oleju)  
HALCU – TWIERDZA; ? (Zernierungswerk)  
RAB HALCI – KOMENDANT TWIERDZY  
HALHALLATU – KOCIOŁ? \[niejasne, jakiś instrument muzyczny, może kocioł\]  
HALICU(M) – WYCISKAJĄCY; OLEJARZ? PRODUCENT OLEJU?  
HALILU – TKWIĄCY?  
HALIQTU(M) – UTRACONY MAJĄTEK; UTRACONE DOBRO, UTRACONA RZECZ; por. HALQU  
HALIQTUM – zob. HALQU(M)  
HALIQU(M) – GINĄCY, MARNIEJĄCY; DAJĄCY ZA WYGRANĄ, REZYGNUJĄCY; UCIEKAJĄCY  
HALLU – WETKNIĘTY  
HALLU(M) – UDO  
HALLUPTU – EKWIPUNEK  
HALPU – ODZIANY, UBRANY; WYPOSAŻONY  
HALPŰ – MRÓZ  
HALQU(M) – STRACONY, ZGUBIONY; ZAGINIONY; ZBIEGŁY  
r.ż. HALIQTUM  
HALU (a/u) – KAPAĆ, KROPIĆ  
\[jako rzeczownik\] KAPANIE, KROPIENIE  
HALU – SKAPNIĘTY  
HALŰ – BŁYSZCZEĆ, LŚNIĆ  
\[jako rzeczownik\] BŁYSZCZENIE, LŚNIENIE  
HALŰ – NABŁYSZCZONY, WYPOLEROWANY, DOPROWADZONY DO LŚNIENIA  
HALŰ – BŁYSZCZĄCY, LŚNIĄCY  
HAMAŠU (a/u) – MIAŻDŻYĆ, KRUSZYĆ  
\[jako rzeczownik\] MIAŻDŻENIE, KRUSZENIE  
HAMAŢU (a/u, u) – ŻARZYĆ SIĘ  
\[jako rzeczownik\] ŻARZENIE SIĘ  
HAMAŢU (u) – SPIESZYĆ SIĘ, POSPIESZAĆ  
\[jako rzeczownik\] SPIESZENIE SIĘ, POSPIESZANIE  
HAMIŠŠERET – PIĘTNAŚCIE \[r.ż.\]  
HAMIŠTU lub HANŠU, HAMŠU(M) – PIĘĆ; PIĄTY  
r.ż. HAMUŠTUM, s.a. HAMIŠ, HAMŠAT  
ADI HAMŠIŠU – PIĘCIOKROTNIE  
HAMŠAT UMU – PIĘĆ DNI  
UMUM HAMŠUM – PIĄTY DZIEŃ  
HAMIŠU – MIAŻDŻĄCY, KRUSZĄCY  
HAMIŢU – ŻARZĄCY SIĘ  
HAMIŢU – SPIESZĄCY SIĘ, POSPIESZAJĄCY  
HAMMŰ(M) – BUNTOWNIK  
HAMMURAPI \[zachodniosemickie\] – HAMMURABI \[imię króla Babilonu\]  
HAMŠA lub HANŠA – PIĘĆDZIESIĄT  
HAMŠATU – JEDNA PIĄTA \[ułamek\]  
HAMŠU – ZMIAŻDŻONY, POKRUSZONY  
HAMŠU(M) lub HANŠU, HAMIŠTU – PIĘĆ; PIĄTY  
r.ż. HAMUŠTUM, HAMIŠTUM, s.a. HAMIŠ, HAMŠAT  
ADI HAMŠIŠU – PIĘCIOKROTNIE  
HAMŠAT UMU – PIĘĆ DNI  
UMUM HAMŠUM – PIĄTY DZIEŃ  
HAMŢU – WYŻARZONY, WYPALONY  
HAMŢU – PRZYSPIESZONY  
HAMŰ – JĘCZEĆ, STĘKAĆ (?)  
\[jako rzeczownik\] JĘCZENIE, STĘKANIE  
HAMŰ – JĘCZĄCY, STĘKAJĄCY (?)  
HANAMU(M) (i) – KWITNĄĆ  
\[jako rzeczownik\] KWITNIENIE  
HANIMU(M) – KWITNĄCY  
HANNÎMMA lub ANNIMMA – TAK SAMO, ZGODNIE Z; zob. ANNIU lub HANNIU  
HANNIU\[nowoasyr.\] lub ANNŰ (M), ANNIAM, ANNIM – TEN  
(H)ANNÎMMA – TAK SAMO, ZGODNIE Z  
ANA MUHHI ANNITI \[środkowobab.\] – W DODATKU, PRÓCZ TEGO  
ANNITKA – TO OD CIEBIE  
HANŠA lub HAMŠA – PIĘĆDZIESIĄT  
HANŠU lub HAMŠU(M), HAMIŠTU – PIĘĆ; PIĄTY  
r.ż. HAMUŠTUM, s.a. HAMIŠ, HAMŠAT  
ADI HAMŠIŠU – PIĘCIOKROTNIE  
HAMŠAT UMU – PIĘĆ DNI  
UMUM HAMŠUM – PIĄTY DZIEŃ  
HANŢIŠ lub HANŢU – POSPIESZNIE, PRĘDKO  
HANŢU – ŻARZĄCY SIĘ  
HANŢU lub HANŢIŠ – POSPIESZNIE, PRĘDKO  
\[HAPADU\] – zob. HUPPUDU  
HAPATU(M) (i) – BYĆ SILNYM, BYĆ POTĘŻNYM  
\[jako rzeczownik\] BYCIE SILNYM, BYCIE POTĘŻNYM  
HAPTU(M) – SILNY, POTĘŻNY  
HAQU (a, i) – ŚCIŚLE PRZYLEGAĆ  
\[jako rzeczownik\] PRZYLEGANIE ŚCIŚLE  
ANA x HAQU – PRZYLEGAĆ ŚCIŚLE DO x  
HAQU – ŚCIŚLE PRZYLEGŁY  
HARABU (u) – BYĆ PUSTYNNYM; BYĆ OPUSTOSZAŁYM  
\[jako rzeczownik\] BYCIE PUSTYNNYM; BYCIE OPUSTOSZAŁYM  
HARACU (a/u) – WYCOFAĆ SIĘ?; ZDJĄĆ?  
\[jako rzeczownik\] WYCOFANIE SIĘ? ZDJĘCIE?  
INA x HARACU – WYCOFAĆ SIĘ Z x  
HARARU(M) (a/u) – ROBIĆ BRUZDY, RYĆ; WYDRĄŻYĆ; MARSZCZYĆ  
\[jako rzeczownik\] ROBIENIE BRUZD, RYCIE; WYDRĄŻENIE; MARSZCZENIE  
HARBU – RUINA  
HARBUTU(M) – SPUSTOSZENIE  
HARBUTAM ALAKUM – STAWAĆ SIĘ PUSTYM, PUSTOSZEĆ  
HARCU – WYCOFANY? ZDJĘTY?  
HARICU – WYCOFUJĄCY SIĘ? ZDEJMUJĄCY?  
HARIRU(M) – RYJĄCY, ROBIĄCY BRUZDY, ORACZ? DRĄŻĄCY; MARSZCZĄCY  
HARRANU – DROGA; MARSZ; PRZEDSIĘWZIĘCIE; HANDEL  
HARRANU(M) – DROGA; PODRÓŻ, PODRÓŻ HANDLOWA; KARAWANA; WYPRAWA, KAMPANIA; ULICA; (KH:)  
ODPRAWA PIENIĘŻNA \[r.ż.\]  
ALIK HARRANIM – PODRÓŻNY; OSOBA WYTYCZAJĄCA POLA, MIERNICZY  
INA PANITIM HARRANIŠU – W JEGO WCZEŚNIEJSZEJ WYPRAWIE  
HARRU – STRUGA  
HARRU(M) – ZRYTY, POBRUŻDŻONY; WYDRĄŻONY; POMARSZCZONY  
HARU, HARU (a, i) (hjr) – WYBRAĆ, OBIERAĆ, DOBIERAĆ (zwłaszcza wybrać żonę); WYPATRZEĆ, UJRZEĆ  
\[jako rzeczownik\] WYBRANIE (zwłaszcza żony), OBIERANIE, DOBIERANIE; WYPATRZENIE, UJRZENIE  
HARU, HARU – WYBRANY, OBRANY, DOBRANY (zwłaszcza o kandydatce na żonę); WYPATRZONY, UJRZANY  
HARŰ (i) lub HERŰ – KOPAĆ  
\[jako rzeczownik\] KOPANIE  
HARŰ – SKOPANY  
HARŰ – KOPIĄCY; KOPACZ?  
HASASU (a/u) – ZAPAMIĘTAĆ, PAMIĘTAĆ; BYĆ PAMIĘTNYM; MYŚLEĆ, WYMYŚLAĆ, ROZWAŻAĆ  
\[jako rzeczownik\] ZAPAMIĘTANIE, PAMIĘTANIE; BYCIE PAMIĘTNYM; MYŚLENIE, ROZWAŻANIE  
HASASU(M) – UWAŻAĆ, BYĆ CZUJNYM, STRZEC SIĘ  
\[jako rzeczownik\] UWAŻANIE, BYCIE CZUJNYM, STRZEŻENIE SIĘ  
HASISSU lub HASISU – ROZUM, ROZSĄDEK  
HASISU lub HASISSU – j.w.  
HASISU – ZAPAMIĘTUJĄCY, PAMIĘTAJĄCY; MYŚLĄCY, WYMYŚLAJĄCY, ROZWAŻAJĄCY  
HASISU(M) – UWAŻAJĄCY, STRZEGĄCY SIĘ  
HASSU – ROZTROPNY, ROZSĄDNY, MĄDRY  
HASSU(M) – UWAŻNY, CZUJNY, OSTROŻNY  
HAŠADU – KOCHAĆ, MIŁOWAĆ  
\[jako rzeczownik\] KOCHANIE, MIŁOWANIE  
HAŠAHU(M), HAŠAHU(M) (i) (a/u) – POTRZEBOWAĆ, PRAGNĄĆ, POŻĄDAĆ; ŻYCZYĆ  
\[jako rzeczownik\] POTRZEBOWANIE, PRAGNIENIE, POŻĄDANIE; ŻYCZENIE  
HAŠALU(M) (a/u) – TŁUC  
\[jako rzeczownik\] TŁUCZENIE  
GTN HITAŠŠULU(M): MLEĆ ZBOŻE  
\[jako rzeczownik\] MIELENIE ZBOŻA  
HAŠDU – KOCHANY, MIŁOWANY  
HAŠHU(M), HAŠHU(M) – POTRZEBOWANY, UPRAGNIONY, POŻĄDANY  
HAŠIDU – KOCHAJĄCY, MIŁUJĄCY  
HAŠIHU(M), HAŠIHU(M) – POTRZEBUJĄCY, PRAGNĄĆY, POŻĄDAJĄCY; ŻYCZĄCY  
HAŠILU(M) – TŁUKĄCY  
HAŠLU(M) – POTŁUCZONY  
HAŠTU – WILCZY DÓŁ  
HAŠU (a, i) – SPIESZYĆ SIĘ  
\[jako rzeczownik\] SPIESZENIE SIĘ  
HAŠŰ(M) – PŁUCA  
HAŠUTU – MACIERZANKA; TYMIAN  
HATANUM – SZWAGIER  
HATTU – OKROPNOŚCI  
HATU(M) – PANIKA, POSTRACH  
HAŢAMU – ZAŁOŻYĆ KAGANIEC  
\[jako rzeczownik\] ZAŁOŻENIE KAGAŃCA  
HAŢIMU – ZAKŁADAJĄCY KAGANIEC  
HAŢMU – OKIEŁZNANY ZA POMOCĄ KAGAŃCA  
HAŢŢU – BERŁO \[r.ż.\]  
HAŢŰ (i) (hti) – ZGRZESZYĆ, ZAWINIĆ  
\[jako rzeczownik\] ZGRZESZENIE, ZAWINIENIE  
HAŢŰ – GRZESZĄCY, POPEŁNIAJĄCY WINĘ; GRZESZNIK? WINOWAJCA?  
HÂŢU(M) (hit) – PILNOWAĆ, NADZOROWAĆ; ŚLEDZIĆ, BADAĆ, ODKRYWAĆ; KONTROLOWAĆ PRZECHADZAJĄC  
SIĘ  
\[jako rzeczownik\] PILNOWANIE, NADZOROWANIE; ŚLEDZENIE, BADANIE, ODKRYWANIE; KONTRO-  
LOWANIE PODCZAS PRZECHADZKI  
GTN: PILNOWAĆ WCIĄŻ NA NOWO, STALE  
\[jako rzeczownik\] PILNOWANIE BEZ USTANKU  
HÂŢU(M) – PILNOWANY, NADZOROWANY; ŚLEDZONY, BADANY, ODKRYWANY; KONTROLOWANY PRZEZ KOGOŚ  
PRZECHADZAJĄCEGO SIĘ  
HAWIRU(M), HAWIRU(M) lub HA’IRU – KOCHANEK, AMANT; (pierwszy) MAŁŻONEK  
HAZANNU lub QEPU – NACZELNIK MIASTA  
HEGALLU – OBFITOŚĆ \[sum.\]  
HEPŰ(M), HEPŰ(M) (e) (hpi) – ROZBIJAĆ, TŁUC (tabliczkę, dokument); PRZEPOŁAWIAĆ, DZIELIĆ NA PÓŁ  
\[jako rzeczownik\] ROZBIJANIE (tabliczki), UNIEWAŻNIANIE (dokumentu); PRZEPOŁAWIANIE, DZIELENIE  
NA PÓŁ  
N NAHRŰ(M): BYĆ ROZBITYM; BYĆ PRZEPOŁOWIONYM, BYĆ PODZIELONYM NA POŁOWY  
\[jako rzeczownik\] BYCIE ROZBITYM; BYCIE PRZEPOŁOWIONYM, BYCIE PODZIELONYM NA POŁOWY  
HEPŰ(M), HEPŰ(M) – ROZBITY, POTŁUCZONY (tabliczka), UNIEWAŻNIONY (dokument); PRZEPOŁOWIONY, PODZIELONY  
NA PÓŁ  
HEPŰ(M), HEPŰ(M) – ROZBIJAJĄCY, TŁUKĄCY (tabliczkę), UNIEWAŻNIAJĄCY (dokument); PRZEPOŁAWIAJĄCY, DZIE-  
LĄCY NA PÓŁ  
HERŰ (i) lub HARŰ – KOPAĆ  
\[jako rzeczownik\] KOPANIE  
HERŰ – WYKOPANY, SKOPANY  
HERŰ – KOPIĄCY  
HETU – GZYMS, WYSTĘP  
HIBILTU(M) – SZKODA, STRATA  
HICBU(M) – DOCHÓD  
HIDŰTU(M) – ŚWIĘTO RADOŚCI; RADOŚĆ  
HIMETU – MASŁO  
HIMETU – MASŁO; ŚMIETANA  
HINIŠANNUM – \[staroas.\] rodzaj naczynia: CZERPAK? (zapożyczenie z hetyckiego słowa oznaczającego czerpak\]  
HINQU – PRZEŁOM RZEKI  
HIRITU – KANAŁ; zob. NARU  
HIRTU lub AŠŠATU(M) – MAŁŻONKA, ŻONA (pierwsza)  
HIŠIHTU – TO, CO POTRZEBNE; POTRZEBA; ŻĄDANIE  
HIŠŠANNUM – \[staroas.\] SŁUP, PAL \[zapożyczenie z hetyckiego\]  
HITMUŢIŠ – POSPIESZNIE, W POŚPIECHU, NAPRĘDCE  
HIŢŢU lub HIŢU (ht’) – GRZECH; PRZEWINIENIE, PRZESTĘPSTWO, WINA; KARA; GRZYWNA; GRZESZNIK; PRZE-  
STĘPCA; BUNTOWNIK  
l.mn. HÎŢŰ, HIŢÂTE, HIŢÂNI  
HIŢU lub HIŢŢU (ht’) – j.w.  
HIŢITU – GRZECH; BŁĄD; DŁUG  
HUBBU – OCZYSZCZAĆ  
\[jako rzeczownik\] OCZYSZCZANIE  
HUBBU – OCZYSZCZONY  
HUBBULU – POZBAWIAJĄCY PRAWA  
HUBTU – RABUNEK, GRABIEŻ; ZDOBYCZ, ŁUP; JENIEC WOJENNY  
HUBULLU, HUBBULU – PROCENT; POŻYCZKA  
BEL HUBULLU – WIERZYCIEL  
HUBULLUM – ZOBOWIĄZANIA CZYNSZOWE  
HUCANNU – PASEK NA BRZUCH  
HUDDUDU – ZGIĘTY  
HUHARU – PUŁAPKA NA PTAKI \[sum.\]  
HULIJAM – HEŁM \[słowo obcego pochodzenia\]  
HULQU – ZAGINIONE DOBRO  
HULUKANNUM – \[staroas.\] WÓZ DWUKÓŁKA \[zapożyczenie z protohetyckiego (hatyckiego)\]  
HUMUCCIRU(M) – SZCZUR  
HUPPUDU – POPSUĆ (wzrok)  
\[jako rzeczownik\] POPSUCIE WZROKU  
HUPPUDU – POPSUTY (wzrok)  
HURACU(M) – ZŁOTO  
HURBAŠU – BOJAŹŃ, STRACH, GROZA  
HURGULLU – RYGIEL \[sum.\]  
HURHUMMATU – PĘCHERZYKI W WODZIE  
HURIBTU – PUSTYNIA  
HURRU – DZIURA; PIECZARA PODZIEMNA; WĄWÓZ GÓRSKI  
ANA HURRI PIŠU – Z GŁĘBI GARDŁA  
HURSANU lub HURŠANU – GÓRY; WYŻYNA \[l.mn.\] \[sum.\]  
HURŠANU lub HURSANU – j.w.  
HUŠAHHU(M) – GŁÓD, KLĘSKA GŁODU  
HUŠAHU – GŁÓD, KLĘSKA GŁODU  
HUTENNŰ – OSZCZEP  
HUŢARU – LASKA

I  
Î lub € – NUŻE! ŚMIAŁO! \[sum.\]  
Î – NIE \[z czasownikami w 1 os. l.mn. Prt\]; zob. E  
-I lub -IA, -JA – MÓJ, MOJA  
Odmiana  
-I, -A, -JA – MÓJ, MOJA -NI, -ANI, -ANU, -ANA – NASZ  
-A, -NI, -NE, -NU – MI -NÂŠI, -NÂŠU – NAM  
-NI, -NU – MNIE -NIATI, -NÂTI – NAS  
-IA lub -I, -JA – j.w.  
IBBI-SIN – IBBISIN \[imię króla z III dynastii z Ur\]  
IBBŰ lub IMBŰ – STRATA, UBYTEK; SZKODA, USZKODZENIE \[sum.\]  
IBILU – WIELBŁĄD; por. arabskie 'IBILUN  
IBRU(M), IBRU(M) – TOWARZYSZ, PRZYJACIEL; (KH:) CZŁOWIEK RÓWNEJ RANGI  
ICCURU – PTAK  
l.mn. ICCURATU  
ICU – DRZEWO; DREWNO  
ICU(M) lub ECU – NIELICZNY, W MAŁEJ LICZBIE  
r.ż. IŠTU(M)  
IDA’’U(M) – BYĆ CIEMNYM  
IDA’UM \[staroak. i staroas.\] lub EDU, IDŰ (M), IDU – ZNAĆ, ROZUMIEĆ, WIEDZIEĆ; DOWIADYWAĆ SIĘ; ZBADAĆ,  
POZNAWAĆ; POZNAĆ (kobietę); ZOBACZYĆ, WYBRAĆ  
\[jako rzeczownik\] ZNANIE, ROZUMIENIE, WIEDZENIE; DOWIADYWANIE SIĘ; ZBADANIE, POZNAWANIE;  
POZNANIE (kobiety); ZOBACZENIE, WYBRANIE  
D: INFORMOWAĆ, OKREŚLAĆ, CZYNIĆ ZNANYM; BYĆ ROZPOZNAWALNYM  
\[jako rzeczownik\] INFORMOWANIE, OKREŚLANIE, CZYNIENIE ZNANYM; BYCIE ROZPOZNAWALNYM  
Š: KAZAĆ WIEDZIEĆ, ZAWIADOMIĆ, POINFORMOWAĆ  
\[jako rzeczownik\] ZAWIADOMIENIE, POWIADOMIENIE, POINFORMOWANIE  
INA IDŰ – ZA WIEDZĄ  
(często w listach:) NŰDÂ – WIEMY  
IDDU lub IDU(M) – RAMIĘ; RĘKA; DŁOŃ; SKRZYDŁO; STRONA, BOK, SKRAJ; MIEDZA; SIŁA, MOC, POTĘGA; {w  
l.mn.:) SIŁYWOJSKOWE, ŻOŁNIERZE; CUDOWNE SIŁY?; CUDO; WRÓŻBA, ZNAK (ŠAME – ZIEMI, IRCITI  
– ZIEMI, SIN – KSIĘŻYCA); zob. IDI  
S.C. ID, l.pdw. IDÂ, IDÂN, l.mn. IDÂTI, IDDÂTI  
IDAT ÂLÂNI – OKOLICE  
IDDŰ – ASFALT \[słowo obcego pochodzenia\]  
IDI – PO STRONIE, OD STRONY; zob. IDU(M)  
IDIRTU – SMUTEK  
IDQU – BARANIA SKÓRA  
IDŰ – \[l.mn.\] ZAPŁATA, CZYNSZ  
IDŰ lub EDŰ, IDŰ (M), IDA’UM – ZNAĆ, ROZUMIEĆ, WIEDZIEĆ; DOWIADYWAĆ SIĘ, DOWIEDZIEĆ SIĘ; ZBADAĆ,  
POZNAWAĆ; POZNAĆ (kobietę); ZOBACZYĆ, WYBRAĆ  
\[jako rzeczownik\] ZNANIE, ROZUMIENIE, WIEDZENIE; DOWIADYWANIE SIĘ; ZBADANIE, POZNAWANIE;  
POZNANIE (kobiety); ZOBACZENIE, WYBRANIE  
D UDDU?: INFORMOWAĆ, OKREŚLAĆ, CZYNIĆ ZNANYM; BYĆ ROZPOZNAWALNYM  
\[jako rzeczownik\] INFORMOWANIE, OKREŚLANIE, CZYNIENIE ZNANYM; BYCIE ROZPOZNAWALNYM  
Š ŠUDU?: KAZAĆ WIEDZIEĆ, ZAWIADOMIĆ, POINFORMOWAĆ  
\[jako rzeczownik\] ZAWIADOMIENIE, POWIADOMIENIE, POINFORMOWANIE  
INA IDŰ – ZA WIEDZĄ  
(często w listach:) NŰDÂ – WIEMY  
IDU – ZNANY, ROZUMIANY; ZBADANY, POZNANY (w tym o kobiecie); ZOBACZONY, UJRZANY, WYBRANY  
IDU(M) (’jd} lub EDU, IDU, IDA’UM – j.w.  
IDU(M) lub IDDU – RAMIĘ; RĘKA; DŁOŃ; SKRZYDŁO; STRONA, BOK, SKRAJ; MIEDZA; SIŁA, MOC, POTĘGA; {w  
l.mn.:) SIŁYWOJSKOWE, ŻOŁNIERZE; CUDOWNE SIŁY?; CUDO; WRÓŻBA, ZNAK (ŠAME – ZIEMI, IRCITI  
– ZIEMI, SIN – KSIĘŻYCA); zob. IDI  
S.C. ID, l.pdw. IDÂ, IDÂN, l.mn. IDÂTI, IDDÂTI  
IDAT ÂLÂNI – OKOLICE  
ALIK IDIŠU – IDĄCY PRZY JEGO BOKU = POMOCNIK  
IGARU(M), IGARU(M) – ŚCIANA, MUR \[sum.\]  
l.mn. IGARU, IGARATU  
IGIGALLU – MĄDRY; MĘDRZEC  
IGIGU – IGIGU \[zbiorowa nazwa wielkich bogów\] \[l.mn.\]  
IGISŰ – DANINA, PODATEK; ODDANIE TRYBUTU \[sum.\]  
IGRU – NAJEM  
ANA IGRI – NALEŻNOŚĆ ZA NAJEM  
IKKARUM – ROLNIK  
l.mn. IKKARATUM  
IKKIBU – ZBRODNIA; OKROPNOŚĆ, OKRUCIEŃSTWO \[sum.\]  
IKKIMU lub EKKEMU – PORYWACZ \[nazwa demona\]  
IKLITU lub EKLETU – CIEMNOŚĆ  
IKRIBU – HOŁD; MODLITWA  
IKU – MAŁY RÓW \[sum.\]  
IKŰ – IKU \[miara powierzchni, ok. 3600 metrów kwadratowych\] = 100 SAR \[sum.\]  
ILITTU(M), ILITTU(M) – POTOMEK; PRODUKT  
ILKU(M), ILKU(M) – SŁUŻBA LENNA \[chyba nazwa jakiejś funkcji\]; POWINNOŚĆ LENNA, LENNO  
ILKU AHŰ – (jakiegoś rodzaju) LENNO  
BIT ILKIM – DOM OTRZYMANY WRAZ Z LENNEM  
ILLATU = ELLATU  
ILLATU – CIESZENIE SIĘ; WYKRZYKIWANIE Z RADOŚCI  
ILLIBBI = INA LIBBI \[staroasyr.\] – WEWNĄTRZ, WŚRÓD  
ILLIDIŠ = INA LIDIŠ – POJUTRZE  
ILLURTU – KAJDANY, PĘTA, WIĘZY \[poet.\]  
ILTANU lub IŠTANU – PÓŁNOC \[strona świata\]  
ILTE- lub IŠTI(-), IŠTU, ISSI – Z  
IŠTU AHA’IŠ \[średnioasyr.\] – WSPÓLNIE, RAZEM Z  
ILTENŠERŰ lub IŠTENŠERŰ – JEDENASTY  
ILTU(M) – BOGINI; DEMON RODZAJU ŻEŃSKIEGO  
ILU(M), ILU(M) – BÓG  
IL(U) ALI – BÓG MIASTA  
ILUTU – BÓSTWO  
IMBARU – MGŁA \[sum.\]  
IMBARU lub ZU – HURAGAN  
IMBŰ lub IBBŰ – STRATA, UBYTEK; SZKODA, USZKODZENIE \[sum.\]  
IMERU(M), IMERU(M) – OSIOŁ; ŁADUNEK DLA OSŁA \[miara objętości wynosząca ok. 100 l\]  
RAKIB IMERIM – JEŹDZIEC NA OŚLE; ODDZIAŁ JEŹDŹCÓW NA OSŁACH  
IMER BILTI – ŁADUNEK OSŁA  
IMHULLU – „ZŁY WIATR” \[sum.\]  
IMITTU lub EMITTU – GRZYWNA; KARA; PODATEK  
IMITTU(M), IMITTU(M) lub IMNU – PRAWA STRONA; PRAWICA; NA PRAWO  
KAKKI IMITTIM – BROŃ (DZIERŻONA W) PRAWICY; „BROŃ PRAWICY” \[cecha wątroby\]  
IMITTA(M) – PO PRAWEJ STRONIE; NA PRAWO  
IMMERU(M) – OWCA; OWCA OFIARNA  
IMMER NIQU – OWCA OFIARNA  
IMNU lub IMITTU(M) – PRAWA STRONA; PRAWICA; NA PRAWO  
KAKKI IMITTIM – BROŃ (DZIERŻONA W) PRAWICY; „BROŃ PRAWICY” \[cecha wątroby\]  
IMITTA(M) – PO PRAWEJ STRONIE; NA PRAWO  
IMTU – ŚLINA \[r.ż.\]  
IN \[staroakad. i często staroasyr.\] lub INA – \[miejscowo:\] W, WŚRÓD, PRZED, Z, DO; \[czasowo:\] W CIĄGU, W CZASIE,  
W TEJ CHWILI CO; \[instrumentalnie:\] ZA POMOCĄ, Z, PRZEZ; \[przyczynowo:\] Z POWODU, ZE WZGLĘDU  
NA; \[z bezokolicznikiem albo zastępuje zdanie czasowe i jest tłumaczone przez:\] KIEDY, SKORO TYLKO \[albo  
bezokolicznik użyty jest jako rzeczownik\]  
INA PANI lub INA MAHAR – PRZED  
(W)ACŰ INA – WYCHODZIĆ Z  
ELŰ INA – WZNOSIĆ SIĘ Z; (GT:) TRACIĆ x  
HALAQU INA – UCIEC Z  
MAHARU INA QATI – PRZYJĄĆ OD (kogoś)  
TEBŰ INA – WSTAĆ OD, WSTAĆ Z  
INA LIBBI – PRZEZ \[instrumentalnie\]  
INA LA M€ – Z POWODU BRAKU WODY  
INA ABNIM RABITIM – STOSOWNIE DO  
INA BALUM – BEZ POROZUMIENIA (KH)  
INA IDŰ – ZA WIEDZĄ  
INA, INA lub IN – j.w.  
INANNA – TERAZ  
ADI INANNA – DOTĄD, DOTYCHCZAS  
INBU(M) – OWOC; SŁODYCZ, WDZIĘK  
INCABTU – KOLCZYK  
INHU – WESTCHNIENIE  
INU(M) lub ENU(M), ENU – OKO; ŹRÓDŁO \[r.ż.\]  
l.mn. INATUM, ENATUM  
INUMA – KIEDY, GDY; WTEDY GDY, SKORO TYLKO; W CZASIE KIEDY  
INUMI – W DZIEŃ, GDY…  
INUMIŠUMA – WÓWCZAS  
IPRU – UTRZYMANIE; KOSZT; ŻYWIENIE  
IPŠU lub EPŠU (’pš) – PRACA, ZAJĘCIE; BUDOWA; CZARY  
IPŠU – CZYNIENIE, ROBIENIE; CZYN, WYSTĘPEK, POSTĘPEK  
IPIŠ PÎM – WYPOWIEDŹ, MOWA  
IPŠIŠ PIŠUNU (poet.) – DLA OTWARCIA ICH UST = DLA ICH WYPOWIEDZI  
IPŠU – OCZAROWYWANIE, ZACHWYCANIE; ZACZAROWANIE; CZARODZIEJSTWO  
IPŢERU – WYKUP, OKUP  
IRBITTU lub ARBA’U, ERBU(M), ERBETTUM – CZTERY  
S.A.: ERBE, ERBET  
ANA ERBIŠU – CZTERY RAZY  
ŠAR ERBETTIM – W CZTERECH STRONACH ŚWIATA; WSZĘDZIE  
ERBET NACMADI – CZWÓRKA, ZAPRZĘG CZTEROKONNY  
IRCITU – ZIEMIA; PIEKŁO  
IRCIT LA TARI – PIEKŁO  
IRIMU(M) – PERŁA? \[jakiś klejnot\]  
IRNITTU – OBIEKT, PRZEDMIOT ŻYCZENIA czy PRAGNIENIA; TRIUMF  
IRPITU lub ERPITU, URPATU – CHMURA, OBŁOK  
IRPU – POCHMURNY (o niebie)  
IRŠU – ŁÓŻKO; LEGOWISKO  
IRTU(M), IRTU(M) – PIERŚ \[r.ż.\]  
MILI IRTIM – SUKCES; ODWAGA  
ANA IRTI – PRZECIW  
ISI – Z; = ISSI  
I.SI.IN.NA – ISIN \[nazwa miasta, jednego z głównych ośrodków pd. Mezopotamii, a także państwa konkurującego z Babilonem\]  
ISINNU – ŚWIĘTO \[sum.\]  
ISSI lub IŠTI(-), IŠTU, ILTE- – Z  
IŠTU AHA’IŠ \[średnioasyr.\] – WSPÓLNIE, RAZEM Z  
ISSU \[nowoasyr.\] lub IŠTU, IŠTUM, ULTU – \[miejscowo:\] Z, OD, PRZY, POD, U, NA; \[czasowo:\] OD, PO, PO TYM JAK; \[jako  
spójnik:\] GDY, SKORO; por. ULTU  
ISSU LIBBI – Z  
ISSU PAN – OD?  
ULTU LIBBI – Z WEWNĄTRZ; PO, PO TYM JAK  
ISSU lub IŠTU, IŠTUM(MA), ULTU – \[subj:\] GDY, SKORO, KIEDY; OD TEGO CZASU, ODTĄD  
ISSU BET – OD  
ISU(M) – SZCZĘKA  
IŠARIŠ – W SPRAWIEDLIWY SPOSÓB; JAK NALEŻY  
IŠARTU – PRAWO \[poet.\]  
IŠARU – PROSTY; SPRAWIEDLIWY  
IŠATU(M) – OGIEŃ  
l.mn. IŠTATATUM  
IŠATUM AKALUM – PALIĆ SIĘ (o czymś)  
IŠDU(M) – PODSTAWA, FUNDAMENT; GRUNT, NA KTÓRYM COŚ STOI; DOLNA CZĘŚĆ; PEWNOŚĆ  
l.pdw. IŠDAN  
IŠID ŠAME – GŁĘBIA NIEBIOS  
IŠERTU(M) – ŚWIĄTYNIA  
IŠHILCU – SKORUPA (GLINIANA)  
IŠHIULUM – \[staroas.\] UMOWA, KONTRAKT \[zapożyczenie z hetyckiego\]  
IŠIPPU – KAPŁAN DOKONUJĄCY OCZYSZCZEŃ \[sum.\]  
IŠIPPUTU – KAPŁAŃSTWO OCZYSZCZAJĄCE; KASTA KAPŁANÓW DOKONUJĄCYCH OCZYSZCZEŃ  
IŠPARU – TKACZ  
IŠPATU – KOŁCZAN  
IŠPATALU lub IŚPATTALU – (staroas\] ZAJAZD, OBERŻA, dosł. NOCLEGOWNIA \[zapożyczenie z hetyckiego\]  
IŠPATTALU lub IŚPATALU – (staroas\] ZAJAZD, OBERŻA, dosł. NOCLEGOWNIA \[zapożyczenie z hetyckiego\]  
IŠPIKKU – SPICHRZ  
IŚPURUZZINNUM – \[sraroas.\] BELKA \[zapożyczenie z hetyckiego\]  
IŠŠAKKU – KSIĄŻĘ BĘDĄCY KAPŁANEM, KAPŁAN BĘDĄCY WŁADCĄ \[sum.\]  
IŠŠI lub ŠI, ŠIT – ONA; TA, DANA, WSPOMNIANA  
Odmiana w wersji starobabilońskiej i staroasyryjskiej  
l.p. l.mn.  
N: ŠI, ŠIT ŠINA  
G/A: ŠIATI, ŠUATI, ŠATU ŠINATI  
D: ŠIAŠIM, ŠUAŠIM, ŠAŠIM ŠINAŠI(M)  
Wersje młodsze:  
ŠI, ŠIT, (IŠŠI) ŠINA, ŠINI, (IŠŠINI)  
ŠUATI, ŠUWATI, ŠIATI, (ŠUATU) ŠÂTINA, ŠINATI, ŠUATINA  
ŠAŠI, ŠAŠA, ŠUAŠA ŠÂŠINA  
IŠŠU(M) – KOBIETA; ŻONA  
IŠTANU lub ILTANU – PÓŁNOC \[strona świata\]  
IŠTAR lub IŠTARTU – ISZTAR \[imię bogini życia i wojny\]  
Pisownia: iš-tar-ri, iš-ta-ri, iš-tar-tum, iš-tar-ta-šu  
IŠTARTU lub IŠTAR – j.w.  
IŠTENIŠ lub IŠTINIŠ – RAZEM, WSPÓLNIE; NA JEDEN RAZ  
IŠTENŠERET – JEDENAŚCIE \[liczebnik rodzaju żeńskiego\]  
IŠTENŠERŰ lub ILTENŠERŰ – JEDENASTY  
IŠTENU(M) – JEDEN; PIERWSZY; POJEDYNCZY  
r.ż. IŠTETUM, s.a. IŠTEN, IŠTIAT, IŠTET  
AŠAR IŠTEN – W JEDNYM MIEJSCU  
IŠTEŠŠU – JEDEN RAZ  
ANA IŠTEŠŠU – DLA JEDNEGO RAZU  
IŠTETUM – JEDNA; PIERWSZA; POJEDYNCZA; zob. IŠTENU(M)  
IŠTI(-) lub IŠTU, ISSI, ILTE- – Z  
IŠTU AHA’IŠ \[średnioasyr.\] – WSPÓLNIE, RAZEM Z  
IŠTINIŠ lub IŠTENIŠ – RAZEM, WSPÓLNIE; NA JEDEN RAZ  
IŠTINUM – JEDEN  
IŠTU \[średnioasyr.\] lub IŠTI(-), ISSI, ILTE- – Z  
IŠTU AHA’IŠ \[średnioasyr.\] – WSPÓLNIE, RAZEM Z  
IŠTU lub IŠTUM(MA), ULTU, ISSU – \[subj:\] GDY, SKORO, KIEDY; SKORO TYLKO; OD TEGO CZASU, ODTĄD, OD  
ISSU BET – OD  
IŠTU lub IŠTUM, ULTU, ISSU – \[miejscowo:\] Z, OD, PRZY, POD, U, NA; \[czasowo:\] OD, PO, PO TYM JAK; \[jako spójnik:\]  
GDY, SKORO; por. ULTU  
ISSU LIBBI – Z  
ISSU PAN – OD?  
ULTU LIBBI – Z WEWNĄTRZ; PO, PO TYM JAK  
IŠTUM \[staroakad.\] lub IŠTU, ULTU, ISSU – j.w.  
IŠTUM(MA) \[staroakad.\] lub IŠTU, ULTU, ISSU – \[subj:\] GDY, SKORO, KIEDY; OD TEGO CZASU, ODTĄD  
ISSU BET – OD  
IŠŰ(M), IŠŰ(M) (i, u) (jšu) – MIEĆ; MIEĆ ROSZCZENIE  
\[jako rzeczownik\] POSIADANIE; WYSTĘPOWANIE Z ROSZCZENIEM  
Odmiana G (tzw. Stativ przedrostkowy):  
IŠU – MAM  
TIŠU – MASZ  
IŠU – MA  
x (Akk. rzeczy) ELI y (imię osoby) IŠŰ – MIEĆ W STOSUNKU DO y ROSZCZENIE DOTYCZĄCE x  
ITALLUKU – CHODZIĆ TAM I Z POWROTEM  
\[jako rzeczownik\] CHODZENIE TAM I Z POWROTEM  
ITBARU – PRZYJACIEL  
ITBULUM – \[bezok. GT\] zob. WABALUM; \[jako czasownik\] ZABIERAĆ, ODBIERAĆ  
\[jako rzeczownik\] ZABIERANIE, ODBIERANIE  
ITBULUM – ZABRANY, ODEBRANY  
ITI lub ITTI – Z, RAZEM Z, W JEDNYM RZĘDZIE Z  
ITINGALLŰ – GŁÓWNY BUDOWNICZY \[sum.\]  
ITINNU(M) – BUDOWNICZY  
ITKURU – PORÓŻNIONY, ZWAŚNIONY, POGNIEWANY  
ITPEŠU – MĄDRY  
ITPEŠATU – MĄDRA  
\[jako abstrakt\] MĄDROŚĆ  
ITTI, ITTI – Z; ZA ZGODĄ; Z APROBATĄ; OD  
ITTIJA – ZE MNĄ  
ITTI ILI MAGIR – ON JEST PRZYJĘTY PRZEZ BOGA  
LEQŰ ITTI – BRAĆ OD  
ITTI lub ITI, ITTU – Z, RAZEM Z, WRAZ Z, W JEDNYM RZĘDZIE Z  
ITTU – WRÓŻBA  
l.mn. ITTATU  
ITTU – STRONA; GRANICA  
l.mn. ITATU  
ITTU lub ITI, ITTI – Z, RAZEM Z, W JEDNYM RZĘDZIE Z  
ITTU – TWARZ; ZNAK; SMOŁA, ROPA NAFTOWA  
ITTU lub ITU – STRONA  
ITU lub ITTU – j.w.  
ITŰ(M) – GRANICA; MIEDZA; STRONA; OBSZAR; SĄSIAD; SĄSIEDNI; POGRANICZNY \[r.ż.\]  
l.mn. ITATUM, s.c. IT, l.pdw. ITÂ, l.mn. IT€, ITÂNI, ITANNI, ITIÂTI, ITÂTI  
ITÂT – DOOKOŁA, NAOKOŁO, WOKÓŁ  
ITULU(M) – LEŻEĆ; UPRAWIAĆ SEKS, MIEĆ STOSUNEK PŁCIOWY (o ludziach)  
\[jako rzeczownik\] LEŻENIE; UPRAWIANIE SEKSU, BYCIE W TRAKCIE STOSUNKU PŁCIOWEGO (o lu-  
dziach)  
Prt: ITTIL – KŁADŁ SIĘ, Pf: ITTATIL – POŁOŻYŁ SIĘ  
IŢRU – SÓL (uzyskana przez odparowanie?)  
IZBU(M) – POTWOREK, NIENORMALNY PŁÓD  
IZUZZU, IZUZZU – zob. NAZAZU(M)  
IZUZZU(M) – zob. NAZAZU

J  
-JA lub -I, -IA – MÓJ, MOJA  
-I, -A, -JA – MÓJ, MOJA -NI, -ANI, -ANU, -ANA – NASZ  
-A, -NI, -NE, -NU – MI -NÂŠI, -NÂŠU – NAM  
-NI, -NU – MNIE -NIATI, -NÂTI – NAS  
JA’UM lub JŰ(M) – MÓJ; TO, CO MOJE  
r.ż. JATTUM, l.mn.r.m. NUM, NIA’UM, l.mn.r.ż. NUTTUM, NIA’TUM  
JABLIJA – JABLIJA \[miejscowość wymieniona w dokumencie starobabilońskim\]  
IARAHHU lub IARAHU – RUBIN? \[słowo obcego pochodzenia\]  
IARAHU lub IARAHHU – j.w.  
JÂŠIM – zob. ANAKU  
JATTUM – zob. JU(M)  
IATU – zob. ANAKU  
JŰ(M) lub JA’UM – MÓJ; TO, CO MOJE  
r.ż. JATTUM, l.mn.r.m. NŰM, NIA’UM, l.mn.r.ż. NUTTUM, NIA’TUM  
-IUM lub –UM – \[końcówka określająca nazwy ludów\]; por. AŠŠURIUM

K  
-KA, -KA – TWÓJ \[zaimek rodzaju męskiego\]  
Odmiana:  
l.p. G: -KA – TWÓJ l.mn. G: -KUNU, -KUN – WASZ  
D: -KUM – TWOJEMU D: -KUNUŠI(M), -KUNUTI – WASZEMU  
A: -KA – TWOJEGO A: -KUNUTI, -KUNU – WASZEGO  
KA’AMANU lub KAIAMANU – TRWAŁY; NORMALNY  
KABASU (a/u) – WCHODZIĆ, WSTĘPOWAĆ; DEPTAĆ, NADEPTYWAĆ  
\[jako rzeczownik\] WCHODZENIE, WSTĘPOWANIE; DEPTANIE, NADEPTYWANIE  
KABATU(M) (i) – BYĆ CIĘŻKIM, WAŻKIM  
\[jako rzeczownik\] BYCIE CIĘŻKIM, BYCIE WAŻKIM  
KABISU – WCHODZĄCY, WSTĘPUJĄCY; DEPTAJĄCY, NADEPTUJĄCY; TRATUJĄCY?  
KABITTU – („CIĘŻKA”) WĄTROBA \[w przeciwieństwie do „lekkiej” żółci\]; (przenośnie:) SERCE, DUSZA, CHARAKTER  
KABITTU(M) – CIĘŻKA; zob. KABTU  
KABSU – TEN, DO KTÓREGO WSTĄPIONO; DEPTANY, NADEPNIĘTY; STRATOWANY?  
KABTU(M) – CIĘŻKI (zarówno o wadze, ciężarze, jak i o karze czy grzechu); WAŻKI, WAŻNY, WIELKI; POTĘŻNY  
r.ż. KABITTUM  
KACARU (a/u) lub QACARU – WIĄZAĆ, ZAWIĄZYWAĆ; SPAJAĆ, ŁĄCZYĆ; OBMYŚLAĆ, PLANOWAĆ  
\[jako rzeczownik\] WIĄZANIE, ZAWIĄZYWANIE; SPAJANIE, ŁĄCZENIE; OBMYŚLANIE, PLANOWANIE  
UŠMANNA KACARU – ROZBIJAĆ (UMOCNIONY) OBÓZ  
CABE (ITTI) KACARU – WYPOSAŻAĆ, EKWIPOWAĆ  
KACARU – WIĄZAĆ; BUDOWAĆ  
\[jako rzeczownik\] WIĄZANIE; BUDOWANIE  
KACIRU – WIĄŻĄCY; BUDUJĄCY  
KACIRU lub QACIRU – WIĄŻĄCY, ZAWIĄZUJĄCY; SPAJAJĄCY, ŁĄCZĄCY; OBMYŚLAJĄCY, PLANUJĄCY; STRATEG?  
KACRU – ZWIĄZANY; ZBUDOWANY  
KACRU – ZWIĄŻANY, ZAWIĄZANY; SPOJONY, POŁĄCZONY; OBMYŚLONY, ZAPLANOWANY  
KACŰ – BYĆ ZIMNYM  
\[jako rzeczownik\] BYCIE ZIMNYM  
KACŰ – ZIMNY  
KAIAMANU lub KA’AMANU – TRWAŁY; NORMALNY  
KAJJANU(M) – NORMALNY  
KAJJANATU(M) – NORMALNA  
\[jako abstarkt\] NORMALNOŚĆ  
KAKKABU – GWIAZDA  
KAKKU(M) – BROŃ; WALKA, POTYCZKA; „BROŃ” \[cecha wątroby\]  
KAKKI IMITTIM – „BROŃ PRAWICY” \[cecha wątroby\]  
\[KALALU\] – zob. ŠUKLULU  
KALAMA, KALAMA lub KALAMU – KAŻDY (RODZAJ); KAŻDY, KAŻDEGO RODZAJU; WSZYSCY; WSZYSTKO, W  
CAŁOŚCI  
KALAMU lub KALAMA – KAŻDY (RODZAJ); KAŻDY, KAŻDEGO RODZAJU  
\[KALAMU\] – zob. KULLUMU  
KALBU(M) – PIES  
r.ż. KALBATUM – SUKA  
KALIŠ – CAŁKOWICIE, ZUPEŁNIE; RAZEM, OGÓŁEM  
KALLAPU – SAPER  
KALLATU – SYNOWA; PANNA MŁODA  
KALLU \[nowobab.\] lub GALLU – SŁUGA; NIEWOLNIK; POMOCNIK itd.  
KALŰ, KALŰ (a) – (przech.) PRZESZKADZAĆ; WSTRZYMYWAĆ, POWSTRZYMYWAĆ; ZAMYKAĆ; UWIĘZIĆ;  
(nieprzech., eliptycznie) PUSZCZAĆ, PONIECHAĆ  
\[jako rzeczownik\] PRZESZKADZANIE; WSTRZYMYWANIE, POWSTRZYMYWANIE; ZAMYKANIE; UWIĘ-  
ZIENIE; PUSZCZANIE, PONIECHANIE  
N NAKLŰ, NAKLŰ: BYĆ lub ZOSTAWAĆ WSTRZYMANYM; POZOSTAWIAĆ, PONIECHAĆ  
\[jako rzeczownik\] BYCIE, ZOSTANIE WSTRZYMANYM; POZOSTAWIANIE, PONIECHANIE  
KALŰ, KALŰ – WSTRZYMANY, POWSTRZYMANY; ZAMKNIĘTY; UWIĘZIONY; PUSZCZONY, PONIECHANY  
KALŰ, KALŰ – PRZESZKADZAJĄCY; WSTRZYMUJĄCY, POWSTRZYMUJĄCY; ZAMYKAJĄCY; WIĘŻĄCY;PUSZCZAJĄCY,  
PONIECHAJĄCY  
KALŰ – KAPŁAN MAJĄCY ZWIĄZEK Z POKUTĄ (Sühnepriester) \[sum.\]  
KALŰ(M), KALŰ(M) – WSZYSCY; WSZYSTKO; W CAŁOŚCI; CAŁOŚĆ, CAŁOKSZTAŁT  
s.c. KAL, KALI, KALA  
MATUM KALUŠA – CAŁY KRAJ  
KAL PANIKA – CAŁOŚĆ TWOJEJ PRZEDNIEJ CZĘŚCI = WSZYSTKO, CO PRZED TOBĄ  
KALIŠA – WSZYSTKIE, CO; WSZYSTKIE, KTÓRE  
KAL ŰMI – CAŁY DZIEŃ  
\[KÂLU(M)\] – zob. KULLU(M)  
KAMALU (i) – GNIEWAĆ SIĘ  
\[jako rzeczownik\] GNIEWANIE SIĘ  
KAMANU – BUŁKA  
KAMARU – BYĆ CIĘŻKIM, WIELE WAŻĄCYM; OBCIĄŻYĆ; CISNĄĆ, PRZYCISNĄĆ, UCISNĄĆ; UCISKAĆ, GNĘBIĆ;  
POKONAĆ, OBALIĆ, ROZGROMIĆ; OPANOWYWAĆ  
\[jako rzeczownik\] BYCIE CIĘŻKIM, WIELE WAŻĄCYM; OBCIĄŻANIE; CIŚNIĘCIE, PRZYCISKANIE, UCI-  
SKANIE, GNĘBIENIE; POKONANIE, OBALENIE, ROZGROMIENIE; OPANOWANIE, OPANOWYWANIE  
D KUMMURU: OBALIĆ  
\[jako rzeczownik\] OBALENIE  
KAMARU – SIEĆ  
KAMARU(M) – ? \[gatunek ryby\]  
KAMASU (i) – SCHYLAĆ SIĘ, POCHYLAĆ SIĘ; KLĘKAĆ; (St:) ZATRZYMYWAĆ SIĘ  
\[jako rzeczownik\] SCHYLANIE SIĘ, POCHYLANIE SIĘ; KLĘKANIE; ZATRZYMYWANIE SIĘ  
D KUMMUSU: SCHYLAĆ SIĘ, POCHYLAĆ SIĘ; KLĘKAĆ  
KAMASU(M) (i) – ZBIERAĆ, SKUPIAĆ, GROMADZIĆ  
\[jako rzeczownik\] ZBIERANIE, SKUPIANIE, GROMADZENIE  
KAMATU lub KAWATU – MUR ZEWNĘTRZNY  
KAMEŠ lub KAMIŠ – JAK SKRĘPOWANY, JAK ZWIĄZANY, BEZ SWOBODY \[przysłówek\]  
KAMILU – GNIEWAJĄCY SIĘ  
KAMIRU – OBCIĄŻAJĄCY; CISNĄCY, PRZYCISKAJĄCY, UCISKAJĄCY; GNĘBIĄCY; POKONUJĄCY, OBALAJĄCY, GRO-  
MIĄCY; OPANOWUJĄCY  
KAMISU – SCHYLAJĄCY SIĘ, POCHYLAJĄCY SIĘ; KLĘKAJĄCY; ZATRZYMUJĄCY SIĘ?  
KAMISU(M) – ZBIERAJĄCY, SKUPIAJĄCY, GROMADZĄCY  
KAMIŠ lub KAMEŠ – j.w.  
KAMRU – CIĘŻKI, WIELE WAŻĄCY; OBCIĄŻONY; ŚCIŚNIĘTY, UCIŚNIĘTY, PRZYCIŚNIĘTY, ZGNĘBIONY, POGNĘBIONY;  
POKONANY, OBALONY, ROZGROMIONY; OPANOWANY  
KAMRU – KUPA, STERTA  
KAMSU – SCHYLONY, POCHYLONY; KLĘCZĄCY  
KAMSU(M) – ZEBRANY, SKUPIONY, ZGROMADZONY  
KAMŰ(M) (kmu) (staroak. kmi) – BRAĆ W NIEWOLĘ; ZABRAĆ; WIĄZAĆ, PRZYMOCOWYWAĆ  
\[jako rzeczownik\] BRANIE W NIEWOLĘ; ZABRANIE; WIĄZANIE, PRZYMOCOWYWANIE  
KAMŰ(M) – WZIĘTY W NIEWOLĘ; ZABRANY; ZWIĄZANY, PRZYMOCOWANY  
KAMŰ(M) – BIORĄCY DO NIEWOLI; ZABIERAJĄCY;WIĄŻĄCY, PRZYMOCOWUJĄCY  
KANAKU(M) (a/u) – PIECZĘTOWAĆ, ZAPIECZĘTOWYWAĆ; POTWIERDZAĆ czy ZATWIERDZAĆ NA PIŚMIE  
(opieczętowanym dokumentem)  
\[jako rzeczownik\] PIECZĘTOWANIE, OPIECZĘTOWANIE; POTWIERDZANIE NA PIŚMIE (za pomocą  
opieczętowanego dokumentu)  
N NAKNUKU(M): BYĆ ZAPIECZĘTOWANYM  
\[jako rzeczownik\] BYCIE ZAPIECZĘTOWANYM, OPATRZONYM PIECZĘCIĄ  
KANANU (a/u) – SKRĘCAĆ (np. idąc); ZGINAĆ  
\[jako rzeczownik\] SKRĘCANIE (np. w marszu); ZGINANIE  
KANAŠU(M) (u) – UGIĄĆ SIĘ, PODDAĆ SIĘ, ULEC; POZOSTAWAĆ ZGIĘTYM, BYĆ UGIĘTYM; BYĆ POKORNYM;  
ZGIĄĆ SIĘ, SKŁONIĆ SIĘ  
\[ jako rzeczownik\] UGINANIE SIĘ, PODDANIE SIĘ, ULEGANIE; POZOSTAWANIE ZGIĘTYM, BYCIE UGIĘ-  
TYM; BYCIE POKORNYM; ZGIĘCIE SIĘ, SKŁONIENIE SIĘ  
GT KITTUŠU(M) : (to samo ze znaczeniem zwrotnym) ZGIĄĆ SIĘ, SKŁONIĆ SIĘ, PODDAĆ SIĘ  
\[jako rzeczownik\] ZGIĘCIE SIĘ, SKŁONIENIE SIĘ, PODDANIE SIĘ  
D=Š KUNNUŠU(M), ŠUKNUŠU(M): KAZAĆ SIĘ PODDAĆ; UJARZMIAĆ, PODPORZĄDKOWYWAĆ; SKŁO  
NIĆ; ZGIĄĆ  
\[jako rzeczownik\] ZMUSZENIE DO PODDANIA SIĘ; UJARZMIANIE, PODPORZĄDKOWYWANIE; SKŁO-  
NIENIE; ZGIĘCIE, WYGIĘCIE  
DT KUTANNUŠU(M): POCHYLAĆ SIĘ, ZGINAĆ SIĘ  
\[jako rzeczownik\] POCHYLANIE SIĘ, ZGINANIE SIĘ  
ŠUKNUŠU ANA NÎRI – NAŁOŻYĆ JARZMO, SKŁONIĆ DO JARZMA  
KANIKU(M) – PIECZĘTUJĄCY, WYSTAWIAJĄCY DOKUMENT (z pieczęcią)  
KANIKU(M) – ZAPIECZĘTOWANY, OPATRZONY PIECZĘCIĄ DOKUMENT  
r.ż. KANIKATUM  
ANA PI KANIKIM – WEDŁUG BRZMIENIA DOKUMENTU Z PIECZĘCIĄ  
KASAP LA KANIKIM – SUMA NIEPOKWITOWANA  
KANINU – SKRĘCAJĄCY (np. w marszu); ZGINAJĄCY  
KANIŠU(M) – UGINAJĄCY SIĘ, PODDAJĄCY SIĘ, ULEGAJĄCY; POZOSTAJĄCY ZGIĘTYM; ZGINAJĄCY SIĘ, SKŁANIAJĄCY  
SIĘ  
KANKU(M) – OPIECZĘTOWANY; WYSTAWIONY (o dokumencie z pieczęcią)  
KANNU – ZGIĘTY  
KANŠU – PODDANY, UJARZMIONY  
KANŠU(M) – UGIĘTY, PODDANY, ULEGŁY; ZGIĘTY, UGIĘTY; POKORNY  
\[KANŰ\] – zob. KUNNU  
KÂNU(M), KÂNU(M) (kun) (kwn) – BYĆ STAŁYM; BYĆ MOCNYM, TRWAŁYM; STAĆ MOCNO, TRWALE; BYĆ  
PRAWDZIWYM; BYĆ PRAWIDŁOWYM, SPRAWIEDLIWYM; BYĆ PRAWDOPODOBNYM; BYĆ RZECZY-  
WISTYM  
\[jako rzeczownik\] BYCIE STAŁYM; BYCIE MOCNYM, TRWAŁYM; STANIE MOCNO, TRWALE; BYCIE  
PRAWDZIWYM; BYCIE PRAWIDŁOWYM, BYCIE SPRAWIEDLIWYM; BYCIE PRAWDOPODOBNYM;  
BYCIE RZECZYWISTYM  
DT: POTWIERDZIĆ, UDOWODNIĆ; WYPOWIEDZIEĆ PRAWDĘ  
\[jako rzeczownik\] POTWIERDZANIE, UDAWADNIANIE; WYPOWIADANIE PRAWDY  
D KUNNU(M)?: USTAWIAĆ; UMACNIAĆ; ZAŁOŻYĆ; KŁAŚĆ, ZAKŁADAĆ; STWIERDZIĆ, POTWIERDZIĆ;  
USTANOWIĆ, USTALIĆ; PRZYŁAPYWAĆ, DEMASKOWAĆ; UMOCOWAĆ; UDOWODNIĆ, WYKAZAĆ;  
NAŁOŻYĆ (trybut); POŁOŻYĆ PODWALINY (pod pałac), SKŁADAĆ, ZŁOŻYĆ (ofiarę)  
\[jako rzeczownik\] USTAWIANIE; PRZYŁAPYWANIE, DEMASKOWANIE; UMACNIANIE; ZAKŁADANIE,  
KŁADZENIE; POŁOŻENIE, ZAŁOŻENIE; STWIERDZANIE, POTWIERDZANIE; USTANAWIANIE, USTA  
LANIE, UMOCOWANIE; UDOWODNIENIE, WYKAZANIE; NAŁOŻENIE (trybutu); POŁOŻENIE PODWA  
LIN (pod pałac); SKŁADANIE, ZŁOŻENIE (ofiary)  
ISSI x KUANU – BYĆ MOCNO ZŁĄCZONYM Z x  
UŠŠU KUNNU – KŁAŚĆ FUNDAMENT  
KIMA… KUNNU – DOWIEŚĆ (komuś), ŻE  
INA y (nazwa rzeczy) x (Akk. osoby) KUNNU – WYKAZAĆ, UDOWODNIĆ y x (coś komuś)  
KÂNU(M), KÂNU(M) – STAŁY; MOCNY, TRWAŁY; OSADZONY MOCNO, TRWALE; PRAWDZIWY; PRAWIDŁOWY,  
SPRAWIEDLIWY; PRAWDOPODOBNY; RZECZYWISTY  
KAPACU(M) (i) – ZŁAMAĆ  
\[jako rzeczownik\] ZŁAMANIE  
KAPADU (u) – KNUĆ, SPISKOWAĆ  
\[jako rzeczownik\] KNUCIE, SPISKOWANIE  
KAPADU ANA x, KAPADU ITTI y – KNUĆ PRZECIW x (y)  
KAPALU (i) – ZWIJAĆ  
\[jako rzeczownik\] ZWIJANIE  
D KUPPULU: (KH:) ZGWAŁCIĆ  
\[jako rzeczownik\] GWAŁCENIE  
KAPARRU – PASTUSZEK \[sum.\]  
KAPICU(M) – ŁAMIĄCY  
KAPIDU – KNUJĄCY, SPISKUJĄCY; SPISKOWIEC?  
KAPILU – ZWIJAJĄCY  
KAPCU(M) – ZŁAMANY  
KAPLU – ZWINIĘTY  
KAPPU – SKRZYDŁO  
KARABU (a/u) – BŁOGOSŁAWIĆ; HOŁDOWAĆ, SKŁADAĆ HOŁD  
\[jako rzeczownik\] BŁOGOSŁAWIENIE; SKŁADANIE HOŁDU  
KARABU – BŁAGAĆ \[w nazwach własnych\]  
KARACU (a/u) – POPIĆ, UCZESTNICZYĆ W PIJATYCE  
\[jako rzeczownik\] POPIJANIE, PICIE, UCZESTNICZENIE W PIJATYCE  
D KURRUCU: OCZERNIĆ, ZNIESŁAWIĆ  
\[jako rzeczownik\] OCZERNIANIE, ZNIESŁAWIANIE  
KARANU(M) – WINO Z WINOGRON \[słowo pochodzenia nieakadyjskiego\]  
KARAŠU – ŚMIERTELNE NIEBEZPIECZEŃSTWO, NIESZCZĘŚCIE  
KARAŠU – JARZYNA, WARZYWO  
KARAŠU(M) – WNĘTRZE?  
ŠA TIAMAT KARASSA – WNĘTRZE TIAMAT  
KARAŠU(M) – OBÓZ POLOWY; ODDZIAŁ POLOWY \[sum.\]  
KARBU – POBŁOGOSŁAWIONY  
KARCU – OSZCZERSTWO, POTWARZ \[l.mn.\]  
KARCI AKALUM – OCZERNIĆ, SPOTWARZYĆ  
KARCI ATAKKULU(M) – OCZERNIĆ WIELOKROTNIE  
KARIBU – BŁOGOSŁAWIĄCY; SKŁADAJĄCY HOŁD; HOŁDOWNIK? WASAL?  
KARICU – POPIJAJĄCY, UCZESTNICZĄCY W PIJATYCE; PIJAK?  
KARMU – RUINA  
KARPATU – GARNEK; NACZYNIE GLINIANE  
KARRU – ODZIEŻ ŻAŁOBNA  
KARŠU – ? \[jakaś aromatyczna roślina\]  
KARŠU(M) – ŻOŁĄDEK; BRZUCH; (metaforycznie) ROZUM, ROZSĄDEK  
KARŰ – SPICHLERZ (ZBOŻOWY); ZAPAS \[sum.\]  
KARU – \[składnik zwrotu\] KOLONIA?  
INA KAR KARMA – W KAŻDEJ KOLONII  
KARU(M) – PORT, PRZYSTAŃ; MUR NADBRZEŻA, NADBRZEŻE; MOLO OCHRONNE; GROBLA, WAŁ; URZĄD  
HANDLOWY \[także sum.\]  
KARŰ(M) (i) – BYĆ KRÓTKIM  
\[jako rzeczownik\] BYCIE KRÓTKIM  
KARŰ(M) – KRÓTKI  
KASAPU (a/u) – ŁAMAĆ  
\[jako rzeczownik\] ŁAMANIE  
KASASU (a/u) – ODGRYZAĆ  
\[jako rzeczownik\] ODGRYZANIE  
KASIPU – ŁAMIĄCY  
KASISU – ODGRYZAJĄCY  
KASKASU(M) – MOSTEK \[część ciała\]  
KASPU lub CARPU – SREBRO  
KASPU – ZŁAMANY  
KASPU(M) – SREBRO; PIENIĄDZ  
QAQQAD KASPI – KAPITAŁ  
KASPU A’ 1 MANE – PIENIĄDZE W SUMIE 1 MINY  
EŠRA MANA KASPAM – 20 MIN SREBRA  
KASSU – ODGRYZIONY  
KASU – KUBEK \[sum.\]  
KASŰ(M) (i) (u) – WIĄZAĆ  
\[jako rzeczownik\] WIĄZANIE  
D KUSSŰ(M): ZAROSNĄĆ  
\[jako rzeczownik\] ZAROŚNIĘCIE  
KASŰ(M) – ZWIĄZANY  
KASŰ(M) – WIĄŻĄCY  
KAŠA – zob. ATTA  
KAŠADU(M), KAŠADU(M) (a/u) – (przech.) BYĆ SPOZA (kogoś lub czegoś – z Akk.); DOSIĘGNĄĆ; OSIĄGNĄĆ,  
URZECZYWISTNIĆ, DOPIĄĆ (czegoś); POWIEŚĆ SIĘ; ZDOBYWAĆ, ZWYCIĘŻAĆ; (nieprzech.)  
NADCHODZIĆ, PRZYBYWAĆ  
\[jako rzeczownik\] BYCIE SPOZA; DOSIĘGANIE; OSIĄGANIE, URZECZYWISTNIANIE, DOPIĘCIE (czegoś);  
POWIEDZENIE SIĘ; ZDOBYWANIE; ZWYCIĘŻANIE; NADCHODZENIE, PRZYBYWANIE  
D KUŠŠUDU(M), KUŠŠUDU(M): WYPĘDZAĆ, SCHWYTAĆ  
\[jako rzeczownik\] WYPĘDZANIE, SCHWYTANIE  
KAŠADU ANA – DOCHODZIĆ DO  
KAŚADU ADI – POSUWAĆ SIĘ KU  
KAŠAPU (i) – CZAROWAĆ; ZACZAROWAĆ, RZUCIĆ UROK  
\[jako rzeczownik\] CZAROWANIE, UPRAWIANIE CZARÓW; ZACZAROWANIE, RZUCANIE UROKÓW  
KÂŠI(M) – zob. ATTA; zob. ATTI  
KAŠIDU(M), KAŠIDU(M) – DOSIĘGAJĄCY; OSIĄGAJĄCY, URZECZYWISTNIAJĄCY, DOPINAJĄCY (czegoś); ZDOBYWAJĄCY,  
ZWYCIĘŻAJĄCY; NADCHODZĄCY, PRZYBYWAJĄCY; ZDOBYWCA? PRZYBYSZ?  
KAŠDU(M), KAŠDU(M) – DOSIĘGNIĘTY; OSIĄGNIĘTY, URZECZYWISTNIONY, DOPIĘTY (cel); ZDOBYTY, ZWYCIĘŻONY;  
NADESZŁY, PRZYBYŁY  
KAŠIPU – CZARUJĄCY, RZUCAJĄCY UROKI  
KAŠIŠU- DYSPONENT ZASTAWU, POSIADACZ ZASTAWU  
KAŠKAŠU – SILNY, POTĘŻNY  
KAŠPU – ZACZAROWANY, DOTKNIĘTY UROKIEM  
KAŠŠAPTU – CZAROWNICA  
KAŠŠAPU – CZAROWNIK  
KAŠUŠU – PRZEPOTĘŻNY, PRZEMOŻNY  
KÂTA – zob. ATTA  
KATAMU(M), KATAMU(M) (a/u) – PRZYKRYWAĆ, NAKRYWAĆ; ZASŁANIAĆ, OKRYWAĆ; ZAMYKAĆ (usta)  
\[jako rzeczownik\] PRZYKRYWANIE, NAKRYWANIE; ZASŁANIANIE, OKRYWANIE; ZAMYKANIE (ust)  
D KUTTUMU(M), KUTTUMU(M): ODZIEWAĆ, PRZYODZIAĆ; UKRYWAĆ, ZATAJAĆ, TUSZOWAĆ  
\[jako rzeczownik\] ODZIEWANIE, PRZODZIANIE; UKRYWANIE, ZATAJANIE, TUSZOWANIE  
DTN KUTATTUMU(M), KUTATTUMU(M): CIĄGLE, WIELOKROTNIE UKRYWAĆ, ZATAJAĆ, TUSZOWAĆ  
\[jako rzeczownik\] CIĄGŁE, WIELOKROTNE UKRYWANIE, ZATAJANIE, TUSZOWANIE  
KATARU (i) – POZYSKIWAĆ (za pomocą przekupstwa); SPRZYMIERZAĆ SIĘ  
\[jako rzeczownik\] POZYSKIWANIE, UZYSKIWANIE (dzięki przekupstwu); SPRZYMIERZANIE SIĘ  
KATARU ITTI x lub KATARU x \[Akk.\] – SPRZYMIERZAĆ SIĘ Z x  
KÂTI – zob. ATTA; zob. ATTI  
KATIMTU – SIEĆ  
KATIMU(M), KATIMU(M) – PRZYKRYWAJĄCY, NAKRYWAJĄCY; ZASŁANIAJĄCY, OKRYWAJĄCY; ZAMYKAJĄCY (usta)  
KATIRU – POZYSKUJĄCY, UZYSKUJĄCY (dzięki przekupstwu), SPRZYMIERZAJĄCY SIĘ  
KATMU(M), KATMU(M) – PRZYKRYTY, NAKRYTY; ZASŁONIĘTY, OKRYTY; ZAMKNIĘTY (usta)  
KATRŰ – PODARUNEK (symbolizujący wierność przymierzu)  
KATTUM – zob. KU(M)  
KAWATU lub KAMATU – MUR ZEWNĘTRZNY  
K€MÂTI lub K€MU, KÎMTU, K€MTU, KÎMU, KÎMÂTI – RODZINA; RÓD; POKREWIEŃSTWO  
K€MTU lub K€MU, KÎMTU, KÎMU, KÎMÂTI, K€MÂTI – j.w.  
K€MU lub KÎMU, KÎMTU, K€MTU, KÎMÂTI, K€MÂTI – j.w.  
KEMU lub KIMU – ZAMIAST, MIAST  
KENA! – TAK! NAPRAWDĘ! \[wykrzyknik\]  
K€NU (kwn) lub KINU(M) – TRWAŁY, MOCNY; PRAWIDŁOWY, SŁUSZNY; PRAWDZIWY; SPRAWIEDLIWY; PEWNY;  
WIERNY; UCZCIWY; PRZYCHYLNY, ŁASKAWY; ODDANY, WIERNY  
APLU K€NU – PRAWNUK  
KERRU lub KIRRU – JAGNIĘ; JAGNIĘTA  
KERRU(M) – WYPRAWA WOJENNA  
KI – (przyimek:) JAK, TAK (JAK), PODOBNIE JAK, W MYŚL, STOSOWNIE DO; (konj.) ŻE; (interj.) JAKŻE!  
KÎ ŠA – JAK, PODOBNIE JAK  
KÎ PÎ – ZGODNIE (z czymś)  
KI AQBI – JAK MOGŁEM MÓWIĆ  
KI KI – JAKŻE  
KI’AM, KI’AM – TAK, W TEN SPOSÓB  
KIBIRTU lub QIPIRTU – OGRANICZONE POMIESZCZENIE; POKÓJ; MIEJSCOWOŚĆ; OBWÓD?; CZĘŚĆ ŚWIATA,  
STRONA ŚWIATA; OBSZAR NIEBIOS, SKRAJ NIEBIOS  
s.c. KIBRAT, QIPRAT, l.mn. KIBRÂTI, QIPRÂTI  
KIBITU – ROZKAZ  
KIBRATU – TEREN; OBRĘB NIEBIOS, NIEBO JAKO PEWNE TERYTORIUM  
KIBRU(M), KIBRU(M) – BRZEG, KRAWĘDŹ; KRAWĘDŹ ŚWIATA, STRONA ŚWIATA; por. KIBIRTU lub QIPIRTU  
l.mn. KIBRATU(M), KIBRATU(M), l.mn.r.ż. KIBRATUM  
KIBRAT ERB€M – CZTERY STRONY ŚWIATA  
KIBSU(M) – POSTĘPOWANIE (lub ŻYCIE); ŚLAD  
KIBTU – PSZENICA \[r.ż.\]  
KIBU – MÓWIĆ, ROZKAZYWAĆ; zob. QIBU  
KICRU – WĘZEŁ; HUFIEC  
KICIR ŠARRUTI – GWARDIA KRÓLEWSKA  
KICIR LIBBI – URAZA; NIENAWIŚĆ  
KICIR ŠAKDI – POROŚNIĘTA (np. omszała) SKAŁA  
KIDANU – ZEWNĄTRZ  
ANA KIDIANU – NA ZEWNĄTRZ?  
KIDU(M) – STRONA ZEWNĘTRZNA  
ANA KIDIM – NA ZEWNĄTRZ  
INA KIDIM – NA ZEWNĄTRZ (poza miastem, domem, ojczyzną)  
KIGALLU – (Postament) \[sum.\]  
KIHULLŰ – MIEJSCE ŻAŁOBY \[sum.\]  
KIKIŢŢŰ – CEREMONIA MAGICZNA \[sum.\]  
KIKKIŠU – SZAŁAS TRZCINOWY \[sum.\]  
KILALLAN – OBYDWAJ  
KILATTAN – OBYDWIE  
KIMA – (przyimek:) JAK, TAK JAK, STOSOWNIE DO, ODPOWIEDNIO; ZAMIAST; (subj.) JAK, TAK JAK, PODOBNIE;  
ŻE; JAK TYLKO, PO TYM JAK; zob. KI  
KIMA ŠA – (subj.) JAK, TAK JAK, PODOBNIE; ŻE; JAK TYLKO, PO TYM JAK  
KIMAHHU – GRÓB \[sum.\]  
KÎMÂTI lub K€MU, KÎMTU, K€MTU, KÎMU, K€MÂTI – RODZINA; RÓD; POKREWIEŃSTWO  
KIMILTU – ZŁOŚĆ, GNIEW  
KIMMATU – OKRĄŻENIE; OGRANICZENIE  
KÎMTU lub K€MU, KÎMU, K€MTU, KÎMÂTI, K€MÂTI – RODZINA; RÓD; POKREWIEŃSTWO  
KÎMU lub K€MU, KÎMTU, K€MTU, KÎMÂTI, K€MÂTI – j.w.  
KIMU lub KEMU – ZAMIAST, MIAST  
KINÂŠI(M) – zob. ATTI  
KINAŠTU lub KINIŠTU – KOLEGIUM  
KINATI – zob. ATTI  
KINIŠTU lub KINAŠTU – KOLEGIUM  
KINU(M) lub KENU (kwn) – TRWAŁY, MOCNY; PRAWIDŁOWY; PRAWDZIWY; SPRAWIEDLIWY; PEWNY; WIERNY;  
UCZCIWY; PRZYCHYLNY, ŁASKAWY; ODDANY, WIERNY  
r.ż. KITTUM  
APLU K€NU – PRAWNUK  
KINUNU – CIĄGLE PALĄCY SIĘ OGIEŃ; PIEC \[sum.\]  
KINUNU – NACZYNIE Z ŻAREM; PIEC  
KIPDU – MYŚLENIE  
KIPPŰ – MAŁY BĘBEN, TAMBURYN  
KIRCU – ZŁOŻONY? ZGIĘTY? KAWAŁEK GLINY  
KIRETU – UCZTA, BIESIADA, POSIŁEK ODBYWANY NA WESOŁO  
KIRRU lub KERRU – JAGNIĘ; JAGNIĘTA  
KIRU – PIEC (do wypalania)  
KIRŰ – SAD, PARK OWOCOWY \[sum.\]  
KISALLU – DZIEDZINIEC (świątyni, pałacu) \[sum.\]  
KISALLU – PLATFORMA, RAMPA  
KISALMAHHU – GŁÓWNY DZIEDZINIEC, DZIEDZINIEC PARADNY \[sum.\]  
KISPU – ŚMIERTELNA OFIARA  
KISU(M) – ODWAŻNIK KAMIENNY; SZALA WAGI?; SAKWA NA PIENIĄDZE; (Amm.:) KAPITAŁ OBROTOWY \[sum.\]  
KISUKKU – WIĘZIENIE \[sum.\]  
KIŠADU, KIŠADU lub AHU – BOK, STRONA (ciała), RAMIĘ; BRZEG (rzeki), WYBRZEŻE  
KIŠADU – BRZEG  
KIŠADU – SZYJA, GARDŁO  
KIŠADU(M) – SZYJA, KARK \[r. ż\]  
l. mn. KIŠADATU  
KIŠIBBU – PIECZĘĆ \[sum.\]  
KIŠITTU – PODBÓJ; ZDOBYCZ, ŁUP  
s.c. KIŠITTI  
KIŠITTI KATI – ZWYCIĘSTWO  
KIŠPU – CZARY \[l.mn.\]  
KIŠŠATU – SYSTEM ZASTAWNY \[l.mn.\]  
KIŠŠATU(M), KIŠŠATU(M) – ŚWIAT, WSZECHŚWIAT; MASA, PEŁNIA; SIŁA, MOC, WSZECHMOC, WSZECHWŁA-  
DZA; POWSZECHNOŚĆ, WSZYSTKO, MNÓSTWO, CAŁOKSZTAŁT; STRONA ŚWIATA; SZEROKI ŚWIAT;  
WYLEW RZEK  
KIŠŠAT NÎŠI – WSZYSCY LUDZIE, CAŁY RÓD LUDZKI  
KIŠŠAT KAL GEMR€TI – CAŁOKSZTAŁT WSZYSTKIEGO I WSZYSTKICH; WSZYSCY, WSZYSTKIE  
KIŠŠUTU – MOCARSTWO  
KIŠTU lub QIŠTU – LAS  
KIŠUBBŰ – OBSZAR NIEZAGOSPODAROWANY, NIEUPRAWIONY, NIEZABUDOWANY; BEZLUDZIE, PUSTKA,  
UGÓR \[sum.\]  
KITRU – ALIANS, SOJUSZ, PRZYMIERZE  
KITTU (kwn) – PRAWDA; SPRAWIEDLIWOŚĆ; PRAWO; (przymiotnikowo – r.ż. od KENU) SPRAWIEDLIWA, SZCZERA  
KITŰ – PŁÓTNO; LNIANA SZATA \[sum.\]  
KŰ(M) lub KUA’UM – TWOJE; TO, CO TWOJE  
r.ż. KATTUM, KUATUM, l.mn.r.m. KUNŰM, l.mn.r.ż. KUNUTUM  
MIMMA LA KÂM – COŚ, CO NIE JEST TWOJE  
KUA’UM lub KU(M) – j.w.  
KUATUM – zob. KU(M)  
KUBABA – KUBABA \[imię mityczne\]  
KUBRU – GRUBOŚĆ  
KUBŠU(M) – CZAPKA  
KUBU – BRYŁA MIĘSA, (właśc. PŁÓD)  
KUCCU(M) lub KUCU – ZIMA; CHŁÓD, ZIMNO  
KUCU lub KUCCU(M) – j.w.  
KUDANU – MUŁ  
KUDDIMMU lub KUTTIMMU – JUBILER  
KUDURRU lub DUPŠIKKU – OBOWIĄZEK PRACY, PRZYMUS PRACY  
KUDURRU – GRANICA, OBSZAR GRANICZNY; KORONA  
KUDURRU – KAMIEŃ GRANICZNY  
KUKKU – CIEMNOŚĆ?  
KUKRU – CYKORIA \[słowo obcego pochodzenia\]  
KULLATU – CAŁOŚĆ (zwykle poet.)  
KULLITANNUM – \[staroas.\] rodzaj naczynia \[zpożyczenie z luwijskiego od słowa oznaczającego naczynie na miód czy olej\]  
KULLU(M) – D: TRZYMAĆ, ZATRZYMAĆ; TRZYMAĆ W POGOTOWIU, MIEĆ DO DYSPOZYCJI, NOSIĆ ZE SOBĄ  
\[jako rzeczownik\] TRZYMANIE, ZATRZYMANIE; TRZYMANIE W POGOTOWIU, NOSZENIE ZE SOBĄ  
REŠAM KULLUM – TRZYMAĆ GŁOWĘ; WSPOMAGAĆ, WSPIERAĆ  
KULLU(M) – TRZYMANY, ZATRZYMANY; TRZYMANY W POGOTOWIU, NOSZONY ZE SOBĄ  
KULLUMU – POKAZYWAĆ, UKAZYWAĆ  
\[jako rzeczownik\] POKAZYWANIE, UKAZYWANIE  
KULLUMU – POKAZANY, UKAZANY  
KULMAŠITU – (pewnego rodzaju) PROSTYTUTKA ŚWIĄTYNNA  
KULTARU lub KUŠTARU – NAMIOT \[słowo obcego pochodzenia)  
KULUPINNUM – \[staroas.\] SIERP \[zapożyczenie z hetyckiego?\]  
-KUM – zob. –KA  
KUMMURU: OBALONY  
KUMMUSU: SCHYLONY, POCHYLONY; KLĘCZĄCY  
-KUN – zob. –KA  
KUNAŠU – RODZAJ PSZENICY, ORKISZ  
KUNNU – OPIEKOWAĆ SIĘ; OTACZAĆ CZUŁĄ OPIEKĄ, CZULE OPIEKOWAĆ SIĘ, TROSZCZYĆ SIĘ  
KUNNU(M)?: USTAWIONY; UMOCNIONY; ZAŁOŻONY; POŁOŻONY; STWIERDZONY, POTWIERDZONY; USTANOWIONY,  
USTALONY; PRZYŁAPANY, ZDEMASKOWANY; UMOCOWANY; UDOWODNIONY, WYKAZANY;NAŁOŻONY  
(trybut); POŁOŻONY (o podwalinach pod pałac), ZŁOŻONY (o ofierze)  
KUNNU lub ALAKU, UMMU – ZAKŁADAĆ  
\[jako rzeczownik\] ZAKŁADANIE  
KUNNU – UTRWALIĆ, WZMOCNIĆ  
\[jako rzeczownik\] UTRWALANIE, WZMACNIANIE  
KUNNUŠU(M), ŠUKNUŠU(M): PODDANY; UJARZMIONY, PODPORZĄDKOWANY; SKŁONIONY;ZGIĘTY  
-KUNU – zob. –KA  
KUNUKKU – PIECZĘĆ  
KUNUKKU(M) – (OPIECZĘTOWANY) DOKUMENT, TABLICZKA (OPATRZONA PIECZĘCIĄ)  
KUNŰM – zob. KU(M)  
(-)KUNUŠI(M) – zob. -KA; zob. ATTA  
(-)KUNUTI(M) – zob. -KA; zob. ATTA  
KUNUTUM – zob. KU(M)  
KUPPULATU: ZGWAŁCONA  
KUPRU – ASFALT, SMOŁA  
KUPURŚINNUM – \[staroas.\] ZŁOTO (niższej jakości) \[zapożyczenie z hetyckiego?\]  
KURKU – ? \[jakiś ptak\]  
KURMATU lub KURUMMATU – WYŻYWIENIE  
KURRU(M) – KURRUM \[miara zboża – pojemności wg Lipina\]  
KURRUCU: OCZERNIONY, ZNIESŁAWIONY  
KURŠINNUM -\[staroas.\] WOREK SKÓRZANY, (BUKŁAK?) \[zapożyczenie z hetyckiego?\]  
KURUMMATU lub KURMATU – WYŻYWIENIE  
KURUMMATU(M) – POŻYWIENIE, ŻYWNOŚĆ, POKARM; PLACEK; zob. KURMATU  
EKIL KURUMMATIM – POLE ODDANE DO UŻYTKU PRYWATNEGO  
KURUNNU – WINO KORZENNE \[słowo nieakadyjskie\]  
KURUNNU – NAPÓJ ALKOHOLOWY; WINO SEZAMOWE  
KURUSSU – OBICIE SKÓRZANE  
KUSAPU – ODŁAMEK  
KUSSIUM lub KUSSŰ (M) – TRON; FOTEL \[r.ż.\] \[sum.\]  
KUSSU lub KUSU – KRZESŁO; TRON  
KUSSŰ(M) lub KUSSIUM – TRON; FOTEL \[r.ż.\] \[sum.\]  
KUSSŰ(M): ZAROSNIĘTY  
KUSU lub KUSSU – KRZESŁO; TRON  
KUŠŠUDU(M), KUŠŠUDU(M): WYPĘDZONY, SCHWYTANY  
KUŠTARU lub KULTARU – NAMIOT \[słowo obcego pochodzenia)  
KUTALLU – ODWROTNA STRONA  
KUTTIMMU lub KUDDIMMU – JUBILER  
KUTTUMU(M), KUTTUMU(M): ODZIANY, PRZYODZIANY; UKRYTY, ZATAJONY  
KUZBU – URODZAJNOŚĆ, PŁODNOŚĆ  
KUZBU(M) – OBFITOŚĆ

L  
LA – NIE \[negacja pojedynczego słowa lub zdania zależnego czy pytajnego; wyrażenie zakazu\]; zob. UL  
LA’ABU lub LA’APU (i) – DOTKNĄĆ, NAWIEDZIĆ (o gorączce)  
\[jako rzeczownik\] DOTKNIĘCIE, NAWIEDZENIE (o gorączce)  
LA’APU lub LA’ABU (i) – j.w.  
LA’ATU (u) – POCHŁANIAĆ, POŻERAĆ, ŁYKAĆ  
\[jako rzeczownik\] POCHŁANIANIE, POŻERANIE, ŁYKANIE  
LA’ÂŢU (u) (l’t) – ROZSZARPAĆ; ZJADAĆ, POŻERAĆ, POŁYKAĆ; SPALAĆ, POŻERAĆ (o ogniu); NISZCZYĆ  
\[jako rzeczownik\] ROZSZARPANIE; ZJADANIE, POŻERANIE, POŁYKANIE; PALENIE; NISZCZENIE  
imiesłów LA’IŢ  
LA’BU – CIĘŻKA CHOROBA; GORĄCZKA  
LA’ITU – POCHŁANIAJĄCY, ŁYKAJĄCY, POŻERAJĄCY; ŻARŁOK?  
LA’IŢ – ROZSZARPUJĄCY; ZJADAJĄCY, POŻERAJĄCY, POŁYKAJĄCY; SPALAJĄCY, POŻERAJĄCY (o ogniu); NISZCZĄCY  
LA’MU lub LAMU – PŁOMIEŃ  
LA’ŢU – ROZSZARPANY; ZJEDZONY, POŻARTY, POŁKNIĘTY; SPALONY; ZNISZCZONY  
LA’Ű(M) (l’u) – BYĆ BRUDNYM?; zob. LU”U  
LA’Ű(M) – BRUDNY  
LABACU lub LABBACU – DEMON  
LABANU (i) – PADAĆ PLACKIEM, PADAĆ NA TWARZ  
\[jako rzeczownik\] PADANIE PLACKIEM, PADANIE NA TWARZ, PŁASZCZENIE SIĘ  
LABAN APPI – SAMOUPOKORZENIE, UPOKORZENIE SIĘ Z WŁASNEJ WOLI  
LABARU(M), LABARU(M) (i) – BYĆ STARYM; STAWAĆ SIĘ STARYM, STARZEĆ SIĘ  
\[jako rzeczownik\] BYCIE STARYM; STAWANIE SIĘ STARYM, STARZENIE SIĘ  
DT LUTABBURU(M), LUTABBURU(M): BYĆ POSTARZAŁYM, STARZEĆ SIĘ  
LABARIŠ UME – Z BIEGIEM CZASU  
LABAŠU(M) (a), LABAŠU(M (i) – ODZIEWAĆ; WYPOSAŻYĆ; UBIERAĆ SIĘ, WDZIEWAĆ (coś), OKRYWAĆ SIĘ  
\[jako rzeczownik\] UBIERANIE, ODZIEWANIE; WYPOSAŻANIE; UBIERANIE SIĘ, WDZIEWANIE, OKRY-  
WANIE SIĘ  
LABBACU lub LABACU – DEMON  
LABBU – LEW  
LABI’ANU – ŚCIĘGNO KARKU  
LABINU – PADAJĄCY NA TWARZ, PŁASZCZĄCY SIĘ  
LABIRRU lub LABIRU – ORYGINAŁ (dokumentu); STARY EGZEMPLARZ  
LABIRU lub LABIRRU – j.w.  
LABIRU – STARY  
LABISU(M), LABISU(M) – ODZIEWAJĄCY, GARDEROBIANY? WYPOSAŻAJĄCY, EKWIPUJĄCY, FUNDATOR? UBIERAJĄCY  
SIĘ, OKRYWAJĄCY SIĘ  
LABSU(M), LABSU(M) – ODZIANY, UBRANY; WYPOSAŻONY, WYEKWIPOWANY; OKRYTY  
LABŰ \[asyr.\] – zob. LAMU  
LAGARU – KAPŁAN; SŁUGA ŚWIĄTYNNY \[sum.\]  
LAHAŠU – SZEPTAĆ  
\[jako rzeczownik\] SZEPTANIE  
LAHBU = LA’BU  
LAHISU – SZEPCZĄCY, SZEPTAJĄCY  
LAHRU – OWCA MAJĄCA MAŁE?  
LAHSU – WYSZEPTANY  
LAKŰ – SŁABY  
LAKURUPPU lub NUKARRIBU – ROLNIK; OGRODNIK  
LALLARTU \[pisownia niepewna\] – PŁACZKA  
LALLARU \[pisownia niepewna\] – PŁACZEK  
LALLARU – OSKARŻYCIEL  
LALLARU lub DIŠPU – MIÓD  
LÂM lub LAMA – (zwykle z Prs; konj.} (czasowo) PRZED, PÓKI JESZCZE NIE, ZANIM; JESZCZE; ZANIM, NIM  
LAMA lub LÂM – j.w.  
LAMADU(M) (a) – UCZYĆ SIĘ; DOWIADYWAĆ SIĘ, POZNAWAĆ (także w sensie seksualnym); (Łyczkowska:) UCZYĆ  
\[jako rzeczownik\] UCZENIE SIĘ; DOWIADYWANIE SIĘ, POZNAWANIE  
GT LITMUDU(M): ZAWIADAMIAĆ, INFORMOWAĆ  
\[jako rzeczownik\] ZAWIADAMIANIE, INFORMOWANIE  
INA LA LAMADIŠU – W JEGO BRAKU DOŚWIADCZENIA  
LAMASSU(M), LAMASSU(M) – BÓSTWO OPIEKUŃCZE \[r.ż.\]  
LAMASSUM – ŻYCIODAJNA SIŁA, SIŁA WITALNA  
LAMAŠTU – DEMON RODZAJU ŻEŃSKIEGO  
LAMIDU(M) – UCZĄCY SIĘ, UCZEŃ? DOWIADUJĄCY SIĘ, POZNAJĄCY, BADACZ?  
LAMNU lub LIMNU, LEMNU – ZŁY; GNIEWNY; ZŁY, MARNY, KIEPSKI; NIESZCZĘŚLIWY  
s.c. LIMUN, l.mn. LIMNŰTU, LIMN€TI  
LAMU lub LA’MU – PŁOMIEŃ  
LAMŰ lub LAWŰ (M) (lwi) – OTOCZYĆ; OBLEGAĆ, OKRĄŻĄĆ; ZAMKNĄĆ; ODPROWADZAĆ, WYPRAWIAĆ, ŻE-  
GNAĆ  
\[jako rzeczownik\] OTOCZENIE; OBLEGANIE, OKRĄŻANIE; ZAMKNIĘCIE; ODPROWADZENIE, WYPRA-  
WIANIE, POŻEGNANIE  
N NALMŰ: BYĆ OTOCZONYM  
\[jako rzeczownik\] BYCIE OTOCZONYM, BYCIE W OKRĄŻENIU  
Š ŠULMŰ: KAZAĆ OKRĄŻYĆ, KAZAĆ OBLEGAĆ  
\[jako rzeczownik\] ZARZĄDZENIE, SPOWODOWANIE, NAKAZANIE OBLĘŻENIA  
NITA LAMŰ – OKRĄŻYĆ ZUPEŁNIE, SZCZELNIE  
LAMŰ – OTOCZONY; OBLĘŻONY, OKRĄŻONY; ZAMKNIĘTY; ODPROWADZONY, WYPRAWIONY, POŻEGNANY  
LAMŰ – OTACZAJĄCY, OBLEGAJĄCY, OKRĄŻAJĄCY; ZAMYKAJĄCY; ODPROWADZAJĄCY, ŻEGNAJĄCY  
LANU, LANU – KSZTAŁT, FORMA; STAN; OBRAZ, WYGLĄD; WYGLĄD ZEWNĘTRZNY, POWIERZCHOWNOŚĆ;  
WZROST, BUDOWA CIAŁA; CIAŁO; OSOBA; WYOBRAŻENIE; STATUA, FIGURA; SAM (ten, o kim mowa,  
jak w polskim wyrażeniu „własną osobą”\]  
s.c. LÂN, l.mn. LÂNÂTE  
LÂN IŠDI – KREW I CIAŁO = CIAŁO  
LAPAN – (przyimek:) PRZED; (wyrażenie obawy, zamiaru ucieczki itp.) OJ, O RANY itp.  
LAPATU(M) (a/u) – DOTYKAĆ; NAPASTOWAĆ; CHWYTAĆ, SCHWYTAĆ; NISZCZYĆ; PORAZIĆ; POGRYŹĆ; POŻERAĆ  
(o ogniu); WYSMAROWAĆ; POKRYWAĆ, BYĆ NA WIERZCHU; WYPACZYĆ, ZMIENIĆ, ZNIEKSZTAŁCIĆ;  
WYWRACAĆ, OBALAĆ, ZRZUCIĆ, NISZCZYĆ, BURZYĆ  
\[jako rzeczownik\] DOTYKANIE; NAPASTOWANIE; CHWYTANIE, SCHWYTANIE; NISZCZENIE; PORAŻA-  
NIE; POGRYZIENIE; POŻERANIE (o ogniu); WYSMAROWANIE; POKRYWANIE, BYCIE NA WIERZCHU;  
WYPACZENIE, ZMIENIANIE, ZNIEKSZTAŁCANIE; WYWRACANIE, OBALANIE, ZRZUCANIE, NISZCZE-  
NIE, BURZENIE  
D LUPPUTU(M): ZATRZYMYWAĆ SIĘ, ZWLEKAĆ  
\[jako rzeczownik\] ZATRZYMYWANIE SIĘ, ZWLEKANIE  
Š ŠULPUTU(M): ZŁUPIĆ, OGRABIĆ; SPUSTOSZYĆ, ZNISZCZYĆ, ZGUBIĆ; DOTYKAĆ, OBMACYWAĆ,  
PORUSZYĆ; HAŃBIĆ, BEZCZEŚCIĆ  
\[jako rzeczownik\] ŁUPIENIE, GRABIENIE; PUSTOSZENIE, NISZCZENIE, GUBIENIE; DOTYKANIE, OBMA-  
CYWANIE, PORUSZANIE; HAŃBIENIE, BEZCZESZCZENIE  
LAPITU(M) – DOTYKAJĄCY; NAPASTUJĄCY; CHWYTAJĄCY; NISZCZĄCY; PORAŻAJĄCY; GRYZĄCY; POŻERAJĄCY (o  
ogniu); SMARUJĄCY; POKRYWAJĄCY, ZNAJDUJĄCY SIĘ NA WIERZCHU; WYPACZAJĄCY, ZMIENIAJĄCY,  
ZNIEKSZTAŁCAJĄCY;WYWRACAJĄCY, OBALAJĄCY, ZRZUCAJĄCY, NISZCZĄCY, BURZĄCY  
LAPTU(M) – DOTYKANY;NAPADNIĘTY; SCHWYTANY; ZNISZCZONY, PORAŻONY; POGRYZIONY; SPALONY; POSMARO-  
WANY;POKRYTY, POŁOŻONY NA WIERZCHU; WYPACZONY, ZMIENIONY, ZNIEKSZTAŁCONY; WYWRÓCONY,  
OBALONY, ZRZUCONY; POHAŃBIONY, ZBEZCZESZCZONY  
LAQA’UM lub LEQŰ (M) (i) – BRAĆ; (w szerszym znaczeniu:) BRAĆ DLA SIEBIE, PRZYWŁASZCZAĆ SOBIE (także bez-  
prawnie); POBIERAĆ, PRZYJMOWAĆ NALEŻNOŚĆ, ZAPŁATĘ; WYSŁUCHIWAĆ, PRZYJĄĆ (prośbę);  
SPRZĄTAĆ (zboże)  
\[jako rzeczownik\] BRANIE; PRZYWŁASZCZANIE; POBIERANIE, PRZYJMOWANIE ZAPŁATY, NALEŻNO-  
ŚCI, WYSŁUCHIWANIE, PRZYJMOWANIE (prośby), SPRZĄTANIE (zboża), ŻNIWOWANIE  
GTN LITAQQŰ(M): BRAĆ WIELOKROTNIE, STALE, REGULARNIE  
LEQŰ ANA – BRAĆ; PRZYJMOWAĆ JAKO; PRZYJMOWAĆ DO  
LEQŰ INA – BRAĆ Z  
LEQŰ ITTI – BRAĆ OD, POŻYCZYĆ OD  
MANAHATI LEQŰ – WYCHODZIĆ NA SWOJE  
LAQU lub LIQU – BRAĆ, DOSTAWAĆ  
\[jako rzeczownik\] BRANIE, DOSTAWANIE, OTRZYMYWANIE  
LARDU – ? (nardenbartgras) \[słowo obcego pochodzenia\]  
LARSA – LARSA \[miasto i państwo, jeden z głównych ośrodków pd. Mezopotamii rywalizujący przez jakiś czas z Babilonem\]  
LASMU – OGNISTY (o koniu)  
LATŰ – ROZSZCZEPIAĆ  
\[jako rzeczownik\] ROZSZCZEPIANIE  
LATŰ – ROZSZCZEPIONY  
LATŰ – ROZSZCZEPIAJĄCY  
LAŢU (a/u) – ZAPRZĘGAĆ, PRZEPRZEGĄĆ; OSWOIĆ, OBŁASKAWIAĆ  
\[jako rzeczownik\] ZAPRZĘGANIE, PRZEPRZĘGANIE; OSWAJANIE, OBŁASKAWIANIE  
LAŢU – ZAPRZĘŻONY, PRZEPRZEŻONY; OSWOJONY, OBŁASKAWIONY  
LAWŰ(M) (lwi) lub LAMŰ – OTOCZYĆ; OBLEGAĆ, OKRĄŻĄĆ; ZAMKNĄĆ; ODPROWADZAĆ, WYPRAWIAĆ, ŻE-  
GNAĆ  
N: BYĆ OTOCZONYM  
Š: KAZAĆ OKRĄŻYĆ, KAZAĆ OBLEGAĆ  
NITA LAMŰ – OKRĄŻYĆ ZUPEŁNIE, SZCZELNIE  
LE’Ű (i) – MÓC; BYĆ GOTOWYM  
\[jako rzeczownik\] BYCIE ZDOLNYM DO; BYCIE GOTOWYM  
LE’Ű – ŚWIADOMY, OBEZNANY (z czymś); ZDOLNY  
LE’U – SILNY, POTĘŻNY; MĄDRY  
LEMNU lub LIMNU, LAMNU – ZŁY; GNIEWNY; ZŁY, MARNY, KIEPSKI; NIESZCZĘŚLIWY  
s.c. LIMUN, l.mn. LIMNŰTU, LIMN€TI  
LEMNU(M) – ZŁY  
r.ż. LEMUTTU(M)  
LEMUTTU(M) – zob. LEMNU(M)  
LEMUTTU(M), LEMUTTU(M) – ZŁA; ZŁOŚĆ; ZŁO; NIESZCZĘŚCIE, ZŁO  
l.mn. LEMNETU  
LEQŰ(M), LEQŰ(M) (i) lub LAQA’UM – BRAĆ; (w szerszym znaczeniu:) BRAĆ DLA SIEBIE, PRZYWŁASZCZAĆ SO-  
BIE (także bezprawnie); POBIERAĆ, PRZYJMOWAĆ NALEŻNOŚĆ, ZAPŁATĘ; WYSŁUCHIWAĆ, PRZYJĄĆ  
(prośbę); SPRZĄTAĆ (zboże)  
\[jako rzeczownik\] BRANIE; PRZYWŁASZCZANIE; POBIERANIE, PRZYJMOWANIE ZAPŁATY, NALEŻNO-  
ŚCI, WYSŁUCHIWANIE, PRZYJMOWANIE (prośby), SPRZĄTANIE (zboża), ŻNIWOWANIE  
GTN LITAQQŰ(M): BRAĆ WIELOKROTNIE, STALE, REGULARNIE  
\[jako rzeczownik\] WIELOKROTNE, REGULARNE BRANIE  
LEQŰ ANA – BRAĆ; PRZYJMOWAĆ JAKO; PRZYJMOWAĆ DO  
LEQŰ INA – BRAĆ Z  
LEQŰ ITTI – BRAĆ OD, POŻYCZYĆ OD  
MANAHATI LEQŰ – WYCHODZIĆ NA SWOJE  
LEQŰ(M), LEQŰ(M) – ZABRANY;PRZYWŁASZCZONY; POBRANY, PRZYJĘTY JAKO ZAPŁATA, NALEŻNOŚĆ; WYSŁUCHA-  
NY, PRZYJĘTY (o prośbie); SPRZĄTNIĘTY (o zbożu)  
LEQŰ(M), LEQŰ(M) – ZABIERAJĄCY; PRZYWŁASZCZAJĄCY; POBIERAJĄCY NALEŻNOŚĆ, POBORCA? SŁUCHAJĄCY,  
PRZYJMUJĄCY (prośbę); SPRZĄTAJĄCY (zboże), ŻNIWIARZ?  
LETU – POLICZEK; (przenośnie też:) STRONA \[r.ż.\]  
LETŰ(M) (lt’) – ROZDZIELAĆ, ROZSZCZEPIAĆ  
\[jako rzeczownik\] ROZDZIELANIE, ROZSZCZEPIANIE  
N NELTŰ(M): BYĆ PODZIELONYM, BYĆ ROZSZCZEPIONYM  
\[jako rzeczownik\] BYCIE PODZIELONYM, BYCIE ROZSZCZEPIONYM  
ANA ŠENA NELTŰM – BYĆ PODZIELONYM NA DWA (np. stronnictwa), ROZPAŚĆ SIĘ NA DWOJE  
LETŰ(M) – ROZDZIELONY, ROZSZCZEPIONY  
LETŰ(M) – ROZDZIELAJĄCY, ROZSZCZEPIAJĄCY  
LI’BU – CIĘŻKA CHOROBA; GORĄCZKA  
LI’MU – TYSIĄC  
LI’U – TABLICZKA, TABLICZKA GLINIANA  
LI’UM – zob. LU(M)  
LIBBATU – ZŁOŚĆ, FURIA \[l.mn.\]  
LIBBU(M), LIBBU(M) – SERCE; WNĘTRZE; ŚRODEK; ZAWARTOŚĆ, ŁADUNEK (statku); CIAŁO LUDZKIE  
s.c. LIBBI  
ŠA LIBBI ALIM – MIESZKAŃCY MIASTA  
ANA LIBBIM WABALUM – WNIEŚĆ, WPROWADZAĆ; DOPASOWAĆ  
LIBBAŠU UBBAL ANA – JEGO SERCE SKŁANIA (GO) DO  
QATATE ANA LEMNETTI INA LIBBI x UBALU – RĘCE W ZŁYM ZAMIARZE PODNIEŚĆ NA x  
(ŠA) LIBBIŠA ŠUDDŰŠI – (TEN, KTÓRY) SPOWODOWAŁ U NIEJ PRZEDWCZESNY PORÓD  
LIBBIŠUNU – WŚRÓD WAS, SPOŚRÓD WAS  
ANA LIBBI – DO, W (np. o udziale w)  
ISSU LIBBI – OD, Z  
INA LIBBI lub INA LIBBU – W, Z  
INA LIBBE – WEWNĄTRZ, W ŚRODKU, WŚRÓD  
ŢUB LIBBI – SERDECZNY, NAJMILSZY PRZYJACIEL; SERDECZNA CHĘĆ, PRAGNIENIE  
ŠA LIBBIM – PŁÓD  
INA LIBBU MATIM – WEWNĄTRZ KRAJU  
LIBITTU(M) – CEGŁA SUSZONA NA POWIETRZU  
LIBLIBBI – WNUK; PRAWNUK; POTOMEK \[transkrypcja niepewna\]  
LIIARU – RODZAJ CEDRA? \[słowo obcego pochodzenia\]  
LILÂTU – WIECZÓR \[l.mn.\]  
LÎLŰ – ZŁY DEMON \[obu rodzajów\]; DEMON WIATRU  
r.ż. LÎLÎTU  
ARDAT LILÎ – DEMONY \[obu rodzajów\]  
LIM – TYSIĄC  
ŠINA LIM – DWA TYSIĄCE  
LIMETU – OKRĄŻENIE, OTOCZENIE  
LIMMU lub LIMŰ (lwj) – RODZINA; EPONIMAT, ARCHONTAT; ROK (noszący imię określonego dostojnika)  
l.mn. LÎM€  
LIMNU lub LAMNU, LEMNU – ZŁY; GNIEWNY; ZŁY, MARNY, KIEPSKI; NIESZCZĘŚLIWY; ZŁO  
s.c. LIMUN, l.mn. LIMNUTU, LIMNETI  
LIMŰ lub LIMMU (lwj) – RODZINA; EPONIMAT, ARCHONTAT; ROK (noszący imię określonego dostojnika)  
l.mn. LÎM€  
LIMU – TYSIĄC  
s.a. LIM  
LIMUTTU – ZŁO, NIESZCZĘŚCIE  
LIPLEPU lub LIPLIPU – LATOROŚL; PŁÓD  
LIPLIPU lub LIPLEPU – j.w.  
LIPTU – STYK, KONTAKT; INGERENCJA, NARUSZENIE  
LIPIT ILI – EPIDEMIA (bydła)  
LIPIT IRRA – ZARAZA, DŻUMA  
LIPTU – ZNISZCZENIE, SPUSTOSZENIE  
LIPTU(M) – WYTWÓR, DZIEŁÓ  
LIPŰ(M) – TŁUSZCZ  
LIQU lub LÂQU – BRAĆ, DOSTAWAĆ  
LIŠANU(M) – JĘZYK (część ciała) \[r.ż.\]; JĘZYK (w sensie językoznawczym) \[zazwyczaj r.m.\]  
ŠA LIŠANIJA – MÓJ TAJNY AGENT  
LIŠAN ŠUMERI – JĘZYK SUMERYJSKI  
LITTU – DZIKA KROWA  
LITU – ZWYCIĘSTWO  
LITU(M) – OKRĘG ADMINISTRACYJNY  
LIŢU – ZAKŁADNIK  
LŰ, LŰ, LU – (partykuła) NIECH, NIECHAJ; (często nieprzetłumaczalne lub oddawane przez:) NAPRAWDĘ, RZECZYWI-  
ŚCIE, DOPRAWDY; POD (przysięgą)  
LU… LU albo LU… ULU – TO… LUB, ALBO… ALBO  
U LU… U LU – ALBO… ALBO  
LŰ(M) – DZIKI BYK; (tylko w poezji czasem:) OSWOJONY BYK  
LU”U – BRUDZIĆ  
\[jako rzeczownik\] BRUDZENIE  
LU’U – BRUDNY, NIECZYSTY, ZANIECZYSZCZONY  
LUBAŠU – UBRANIE, SZATA, ODZIEŻ  
LUBULTU lub LUBUŠTU – ODZIEŻ, UBRANIE; CZĘŚĆ UBRANIA, ODZIEŻY  
s.c. LUBULTI, LUBUŠTI  
LUBUŠTU lub LUBULTU – j.w.  
LUBUŠUM – SZATA  
LULLŰ lub LULŰ – WIELKA ZAMOŻNOŚĆ, WYSTAWNE BOGACTWO; WYBUJAŁOŚĆ, BUJNOŚĆ  
LULŰ lub LULLŰ – j.w.  
LUMNU(M) – ZŁOŚĆ  
BEL LUMNIM – PRZECIWNIK  
LUPUTTU – KAPITAN, DOWÓDCA ODDZIAŁU \[sum.\]  
LURMU – STRUŚ  
M  
MA – \[partykuła wprowadzająca mowę bezposrednią, cytat, stosowana szczególnie w inskrypcjach\]  
-MA – I; \[także podkreślenie znaczenia wyrazu\]  
MA’ADU lub MA’DU, MADU – DUŻO  
r.ż. MA’ATTU, MATTU  
MA’ADU (i) – BYĆ DUŻO  
MA’ADU ELI x – BYĆ WIĘCEJ NIŻ x  
MA’ADUTU – DUŻO  
MA’ALU – LEGOWISKO, ŁOŻE  
MA’DIŠ – BARDZO  
MA’DU lub MA’ADU, MADU – DUŻO  
r.ż. MA’ATTU, MATTU  
MA’ITU(M) – UMIERAJĄCY  
MA’IŢŰ – ZMNIEJSZAJĄCY SIĘ, MALEJĄCY; PONIŻAJĄCY, ZANIEDBUJĄCY  
MA’U(M) – z negacją: NIE CHCIEĆ  
MACCARTU – WARTA NOCNA; STRAŻ  
MACCARTU(M) – SKARBIEC  
MACCARU – STRAŻ, WARTA  
MACCARU – OBROŃCA  
MACCARUTU – DEPOZYT  
ANA MACCARUTIM NADANUM – ODDAĆ NA PRZECHOWANIE  
MACRAHU(M) – ZACZĄTEK (woreczka żółciowego)  
MACRAH MARTIM – ZACZĄTEK WORECZKA ŻÓŁCIOWEGO, PRZEWÓD ŻÓŁCIOWY W WĄTROBIE  
MACŰ lub MALA – BYĆ RÓWNYM (wielkością, ilością itd)  
\[jako rzeczownik\] BYCIE RÓWNYM  
MALA LIBBI ŠUMCŰ – DAĆ MOŻNOŚĆ DYSPONOWANIA, DAĆ WOLNĄ RĘKĘ  
MALA x MACŰ – WYSTARCZAĆ DLA  
MACŰ – RÓWNY  
\[MACŰ\] – zob. MUCCU  
MACŰ(M) (mci) – (Prt, Prs) STAWAĆ SIĘ TAKIM (JAK), UPODABNIAĆ SIĘ, DOSTOSOWYWAĆ SIĘ; (St) BYĆ ODPO-  
WIEDNIM  
\[jako rzeczownik\] UPODABNIANIE SIĘ, DOSTOSOWYWANIE SIĘ  
MACŰ(M) – UPODOBNIONY, DOSTOSOWANY; ODPOWIEDNI  
MACŰ(M) – UPODABNIAJĄCY, DOSTOSOWUJĄCY  
MADADU (a/u) – MIERZYĆ  
\[jako rzeczownik\] MIERZENIE  
MADATTU lub MANDATTU – DANINA, TRYBUT  
MADDU – ZMIERZONY  
MADIDU – MIERZĄCY; MIERNICZY?  
MADU lub MA’ADU, MA’DU – DUŻO  
r.ż. MA’ATTU, MATTU  
MADU(M) – W DUŻEJ LICZBIE; LICZNY; zob. MA’ADU  
r.ż. MATTUM  
MÂDU(M) (mid) – BYĆ LICZNYM; STAWAĆ SIĘ LICZNYM, ROSNĄĆ W LICZBĘ  
\[jako rzeczownik\] BYCIE LICZNYM; STAWANIE SIĘ LICZNYM, LICZEBNE WZRASTANIE  
ANA MADIM TÂRUM – STAWAĆ SIĘ LICZNYM, POWIĘKSZAĆ SIĘ  
MAGAL – BARDZO \[sum.\]  
MAGAL lub DANNIŠ – BARDZO  
MAGARU – ZGADZAĆ SIĘ  
\[jako rzeczownik\] ZGADZANIE SIĘ  
MÂGIRU lub MAGRU – POSŁUSZNY, POKORNY; ŻYCZLIWY, ŁASKAWY; POMYŚLNY; ZGODNY  
UMU MAGIRU – POMYŚLNY DZIEŃ  
LÂ MÂGIRU – NIEPOSŁUSZNY, NIEPOKORNY  
MAGRITU(M) – OBELGA; NIENAWIŚĆ?  
l.mn. MAGRIATU  
MAGRU lub MAGIRU – POSŁUSZNY, POKORNY; ŻYCZLIWY, ŁASKAWY; POMYŚLNY; ZGODNY  
UMU MAGIRU – POMYŚLNY DZIEŃ  
LA MAGIRU – NIEPOSŁUSZNY, NIEPOKORNY  
MAHACU(M), MAHACU(M) (a) – BIĆ, WBIJAĆ; STARANOWAĆ (statek), UDERZAĆ  
\[jako rzeczownik\] BICIE, WBIJANIE; STARANOWANIE (statku), UDERZANIE  
GT MITHUCU(M), MITHUCU(M): WALCZYĆ POMIĘDZY SOBĄ  
\[jako rzeczownik\] WALCZENIE ZE SOBĄ  
N NAMHUCU(M), NAMHUCU(M): BYĆ BITYM; OTRZYMYWAĆ KIJ DO BICIA  
\[jako rzeczownik\] BYCIE BITYM; OTRZYMYWANIE KIJA DO BICIA  
EQLAM MA’ARI MAHACU – POPRZECINAĆ POLE BRUZDAMI  
MAHAR – PRZED, WOBEC  
INA MAHAR albo INA MAHRI, albo ADI MAHRI – TYM PODOBNE  
ALIK MAHRI – IDĄCY PRZODEM; POPRZEDNIK (zarówno w czasie, jak i w przestrzeni)  
INA MAHRI (jako przysłówek) – NAPRZÓD, PRZEDTEM  
MAHRIKA – PRZED TOBĄ, U CIEBIE  
ANA MAHRIKA – PRZED CIEBIE  
MAHARIŠ = INA MAHAR  
MAHARU(M), MAHARU(M) (a/u) – PRZYJĄĆ, ODEBRAĆ (coś od kogoś – z dwoma Akk.); ŁASKAWIE PRZYJĄĆ (ofiarę),  
WYSŁUCHAĆ (modlitwy); WEJŚĆ W ŁASKI, ZDOBYĆ PRZYCHYLNOŚĆ, WZGLĘDY; ZWRACAĆ SIĘ DO,  
PROSIĆ; BYĆ ZGODNYM, ODPOWIADAĆ, BYĆ RÓWNYM; BYĆ NA CZELE, BYĆ NA PRZEDZIE; STA-  
WAĆ NAPRZECIW; UTRZYMYWAĆ, ZACHOWYWAĆ; (Łyczkowska:) OSIĄGAĆ  
\[jako rzeczownik\] PRZYJMOWANIE, ODBIERANIE (czegoś od kogoś); ŁASKAWE PRZYJĘCIE (ofiary), WY-  
SŁUCHIWANIE (modlitwy); WCHODZENIE W ŁASKI, ZDOBYWANIE PRZYCHYLNOŚCI czy WZGLĘDÓW;  
ZWRACANIE SIĘ DO, PROSZENIE; BYCIE ZGODNYM, ODPOWIADANIE, BYCIE RÓWNYM; BYCIE NA  
CZELE, BYCIE NA PRZEDZIE; STAWANIE NAPRZECIW; UTRZYMYWANIE, ZACHOWYWANIE; OSIĄ-  
GANIE  
GT MITHURU(M), MITHURU(M): WZAJEMNIE RÓWNAĆ SIĘ; DORÓWNYWAĆ SOBIE  
\[jako rzeczownik\] BYCIE SOBIE RÓWNYMI; DORÓWNYWANIE SOBIE  
ŠT ŠUTAMHURU(M), ŠUTAMHURU(M): PORÓWNYWAĆ SIĘ ZE SOBĄ, PRÓBOWAĆ SIĘ RÓWNAĆ;  
ZRÓWNAĆ SIĘ; KAZAĆ BYĆ WZAJEMNIE JEDNAKOWYMI; USTANOWIĆ, POTRAKTOWAĆ JAKO  
ODPOWIEDNIK  
\[jako rzeczownik\] PORÓWNYWANIE SIĘ ZE SOBĄ, PRÓBOWANIE ZRÓWNANIA SIĘ; ZRÓWNYWANIE  
SIĘ; SPOWODOWANIE UJEDNOLICENIA SIĘ, ZRÓWNANIA WZAJEMNEGO; USTANOWIENIE lub PO-  
TRAKTOWANIE JAKO ODPOWIEDNIKA  
MAHARU x (Akk.) INA QATI y – PRZYJĄĆ, WZIĄĆ x OD y  
IN x MAHARU – WEJŚĆ W ŁASKI x  
ANA PANIKA MAHER \[nowobab.\] – JEST CI PRZYJEMNIE  
QAQQARIŠ UŠAMHIR – ZRÓWNAŁEM Z ZIEMIĄ  
MAHAZU – MIASTO BĘDĄCE OŚRODKIEM KULTU  
MAHCU(M), MAHCU(M) – ZBITY, WBITY; STARANOWANY (statek), UDERZONY  
MAHHUTU – STAN BYCIA SENSOWNYM, POSIADANIE SENSU, SENSOWNOŚĆ  
MAHICU – RZEŹNIK  
MAHICU(M), MAHICU(M) – BIJĄCY, WBIJAJĄCY; TARANUJĄCY (statek), UDERZAJĄCY  
MAHIRTU – STATEK WIOSŁOWY  
ELEP ŠA MAHIRTI – STATEK WIOSŁOWY  
ŠA MAHIRTIM – KAPITAN STATKU WIOSŁOWEGO  
MAHIRU lub GABRU – RÓWNY URODZENIEM  
MAHIRU(M), MAHIRU(M) – CENA KUPNA; CENA HANDLOWA; KURS TOWARU; RYNEK TOWARÓW, ILOŚĆ TOWA-  
RU; RÓWNOWARTOŚĆ  
l.mn. MAHIRATU  
MAHIRU(M), MAHIRU(M) – PRZYJMUJĄCY, ODBIERAJĄCY (coś od kogoś); ŁASKAWIE PRZYJMUJĄCY (ofiarę),WYSŁU-  
CHUJĄCY (modlitwy); WCHODZĄCY W ŁASKI, ZDOBYWAJĄCY PRZYCHYLNOŚĆ, WZGLĘDY; ZWRACAJĄCY  
SIĘ DO, PROSZĄCY, BŁAGALNIK? PETENT?; PRZODUJĄCY, STOJĄCY NA CZEL;, STOJĄCY NAPRZECIW;  
UTRZYMUJĄCY, ZACHOWUJĄCY; (Łyczkowska:) OSIĄGAJĄCY  
MAHIRUM – TERAŹNIEJSZOŚĆ  
MAHRU – PRZEDNIA STRONA, PRZÓD; PRZEDNI; PIERWSZY; POPRZEDNI, DAWNY; PRZED; por. MAHAR  
s.c. MAHAR  
MAHAR – PRZED, WOBEC  
INA MAHAR – PRZED, PRZECIW, PRZY; DAWNE CZASY  
ŠAR MAHRI – DAWNY KRÓL  
MAHRU LA MAHRU – NIEZRÓWNANY, BEZ PORÓWNANIA  
UMU LA MAHRI – ATAK NIE DO ODPARCIA  
MAHRŰ(M) – PIERWSZY; DAWNY; WCZEŚNIEJSZY, POPRZEDNI; por. MAHAR  
ELI ŠA MAHRI – BARDZIEJ NIŻ DAWNY, PRADAWNY  
ALIK MAHRI – POPRZEDNIK  
MAJJALU(M) – LEGOWISKO, ŁOŻE  
MAKALTU – MISKA, NACZYNIE DO JEDZENIA  
MAKALŰ – JEDZENIE, POSIŁEK  
MAKKASU – DAKTYLE PIERWSZEJ JAKOŚCI  
MAKKURU(M), MAKKURU(M) – MAJĄTEK  
MAL lub MALA – (subj.) O ILE, O ILE TYLKO; ILE TYLKO; WSZYSTKO, CO  
MIMMA MALA – WSZYSTKO, CO  
MALA TERHATIŠA – W WYSOKOŚCI JEJ CENY JAKO NARZECZONEJ  
MALA x MACŰ – WYSTARCZAĆ DLA x  
MAC€ MAL LIBBI – DOSTARCZAĆ (WSZYSTKIEGO), CZEGO SERCE ZAPRAGNIE (?)  
AMMALA staroasyr.\] – NA PODSTAWIE, ZGODNIE Z, STOSOWNIE DO, W MYŚL, WEDŁUG  
MALA, MALA lub MAL – j.w.  
MALA lub MACU – BYĆ RÓWNYM (wielkością, ilością itd)  
MALA LIBBI ŠUMŠU – DAĆ MOŻNOŚĆ DYSPONOWANIA, DAĆ WOLNĄ RĘKĘ  
MALA x MACU – WYSTARCZAĆ DLA  
MALAHU – PRZEWOŹNIK RZECZNY  
MALAHU(M) – MARYNARZ (żeglugi rzecznej) \[sum.\]  
MALAKU – DROGA; ZMIANA; PROCESJA  
MALAKU (i) – RADZIĆ  
\[jako rzeczownik\] RADZENIE  
GT: MITLUKUM – NARADZAĆ SIĘ POMIĘDZY SOBĄ, RADZIĆ SIĘ  
\[jako rzeczownik\] NARADZANIE SIĘ POMIĘDZY SOBĄ, RADZENIE SIĘ  
MALALU(M) (a) – GRABIĆ  
\[jako rzeczownik\] GRABIENIE  
MALASU (a/u) – WYRYWAĆ (włosy)  
MALIKU lub MALKU – KSIĄŻĘ; REGENT  
s.c. MALIK, l.mn. MALIKI, MALK€, r.ż. MALKATU, l.mn.r.ż. MALKÂTI  
MALIKU(M), MALIKU(M) – DORADCA  
MALIKUTU – WŁADZA KSIĄŻĘCA; PANOWANIE  
MALILU(M) – GRABIĄCY  
MALKU lub MALIKU – KSIĄŻĘ; REGENT  
s.c. MALIK, l.mn. MALIKI, MALK€, r.ż. MALKATU, l.mn.r.ż. MALKÂTI  
MALLU(M) – ZAGRABIONY  
MALMALIŠ – RÓWNOMIERNIE, JEDNOSTAJNIE; (poet.) WSZĘDZIE  
MALTITU lub MAŠTITU – NAPÓJ  
MALŰ – PEŁNY, NAPEŁNIONY; PEŁNOŚĆ, PEŁNIA  
r.ż. MALÎTU  
MALŰ – KOŁTUN (ze sfilcowanych, brudnych włosów na ciele)  
MALŰ(M) (i) (a) – (przech.) NAPEŁNIAĆ; (nieprzech.) BYĆ PEŁNYM (St), STAWAĆ SIĘ PEŁNYM (Prt, Prs), BYĆ NAPEŁ-  
NIONYM (z Akk.)  
\[jako rzeczownik\] NAPEŁNIANIE; BYCIE PEŁNYM, STAWANIE SIĘ PEŁNYM; BYCIE NAPEŁNIONYM  
D MULLŰ(M): NAPEŁNIAĆ, ZAPEŁNIAĆ; WYNAGRADZAĆ (szkodę)  
\[jako rzeczownik\] NAPEŁNIANIE, ZAPEŁNIANIE; WYNAGRADZANIE (szkody)  
(W)ARAHŠU LA IMLA – MIESIĄC SIĘ NIE SKOŃCZYŁ  
ANA CIR€ MALŰ \[nowobab.\] – BYĆ PEŁNYM PO BRZEGI; MIEĆ DOŚĆ  
3 ARH€ UL UMALLI – (JESZCZE) PRZED UPŁYWEM TRZECH MIESIĘCY  
MALŰ(M) – NAPEŁNIAJĄCY  
MAMITU – PRZYSIĘGA, KLĄTWA  
ADŰ U MAMITU – ZAPRZYSIĘŻONA UMOWA  
MAMMA lub MAMMAN, MANMA, MANMAN – KTOKOLWIEK, KTOŚ; (w zdaniach zaprzeczonych) NIKT  
MAMMAN lub MAMMA, MANMA, MANMAN – j.w.  
MAMU lub MŰ (M) – \[poet.\] WODA \[l.mn.\]  
l.mn. i G-A M€  
-MAN – MOŻE, PEWNIE \[partykuła możliwości\]  
-MAN \[asyr.\] – \[wyrażenie irrealności treści zdania\]  
MANA’U\[asyr.\] lub MANŰ (M) – MINA \[jednostka wagi wynosząca ok. 0,5 kg, równa 60 szeklom)  
KASPU A’ 1 MANE – PIENIĄDZE WARTOŚCI 1 MINY (SREBRA)  
IŠTEN MANA’UM – W JEDNEJ MINIE  
MANAHTU – ZMĘCZENIE, ZNUŻENIE, WYCZERPANIE, WYCIEŃCZENIE; NAKŁAD PRACY  
MANAMA – KTOŚ, KTOKOLWIEK  
MANAMA MAMMA – KAŻDY  
MANDATTU lub MADATTU – DANINA, TRYBUT  
MANMA lub MAMMAN, MAMMA, MANMAN – KTOKOLWIEK, KTOŚ; (w zdaniach zaprzeczonych) NIKT  
MANMAN lub MAMMAN, MAMMA, MANMA – j.w.  
MANNU(M), MANNŰ(M) – KTÓRY? KTO?  
MANNU ŠA – KTOKOLWIEK  
-MA MANNUMMA – KTO?  
ANA MANNIIA – DLA KOGO?  
ANA MANIM – KOMU?  
MANNI(M) – KOGO?  
MANNA(M) – KOGO?  
MANŰ – LICZENIE; LICZBA  
ANA LÂ MANÎ – NIEZLICZONE, BEZ LIKU  
MANŰ(M) (mnu) – LICZYĆ; DODAWAĆ; ZALICZYĆ; RECYTOWAĆ, DEKLAMOWAĆ  
\[jako rzeczownik\] LICZENIE; DODAWANIE; ZALICZANIE; RECYTOWANIE, DEKLAMOWANIE  
QAQQARIŠ MANŰ \[mł.bab.\] – WYRÓWNYWAĆ POLE, NIWELOWAĆ GRUNT  
MANŰ ANA – ZALICZYĆ DO, POTRAKTOWAĆ JAKO  
ŠALLATIŠ MANŰ – WŁĄCZYĆ DO ŁUPU  
MANŰ INA QAT x – PODPORZĄDKOWYWAĆ x  
MANŰ ITTI – UWAŻAĆ ZA, ZALICZYĆ DO, PRZYDZIELIĆ DO  
MANŰ(M) – LICZONY; DODAWANY; ZALICZANY; RECYTOWANY, DEKLAMOWANY  
MANŰ(M) – LICZĄCY, RACHMISTRZ? DODAJĄCY; ZALICZAJĄCY; RECYTUJĄCY, DEKLAMUJĄCY, AKTOR?  
MANŰ(M) lub MANA’U – MINA \[jednostka wagi wynosząca ok. 0,5 kg, równa 60 szeklom)  
KASPU A’ 1 MANE – PIENIĄDZE WARTOŚCI 1 MINY (SREBRA)  
EŠRA MANA KASPAM – 20 MIN SREBRA  
IŠTEN MANA’UM – W JEDNEJ MINIE  
MANZAZU – STOJĄCY; MIEJSCE POBYTU  
MANZAZ PANI – RODZAJ DWORZANINA, PRZYBOCZNY  
MAQARU (a/u) – NAWADNIAĆ  
\[jako rzeczownik\] NAWADNIANIE  
MAQATU(M) (a/u) – PADAĆ, SPADAĆ, WPADAĆ; PRZYTRAFIAĆ SIĘ, SPADAĆ, SPOTYKAĆ; PONIŻYĆ; STAWAĆ SIĘ  
NIEWAŻNYM, TRACIĆ WAŻNOŚĆ (o przysiędze); (Łyczkowska:) ZAWALIĆ SIĘ (o budowli)  
\[jako rzeczownik\] PADANIE, SPADANIE, WPADANIE; PRZYTRAFIANIE SIĘ, SPADANIE, SPOTYKANIE;  
PONIŻANIE; STAWANIE SIĘ NIEWAŻNYM, TRACENIE WAŻNOŚCI (o przysiędze); ZAWALENIE SIĘ  
GTN MITAQQUTU(M): PADAĆ RAZ ZA RAZEM, WCIĄŻ NA NOWO  
\[jako rzeczownik\] PADANIE RAZ ZA RAZEM  
Š ŠUMQUTU(M): ŚCINAĆ; DOPROWADZIĆ DO UPADKU, UPUŚCIĆ  
\[jako rzeczownik\] ŚCINANIE; DOPROWADZANIE DO UPADKU, UPUSZCZANIE  
MAQATU ANA – UCIEKAĆ, SCHRONIĆ SIĘ  
MAQIRU – NAWADNIAJĄCY  
MAQITU(M) – PADAJĄCY, SPADAJĄCY, WPADAJĄCY; PRZYTRAFIAJĄCY SIĘ;STAJĄCY SIĘ NIEWAŻNYM, TRACĄCY ZNA-  
CZENIE, POZYCJĘ; TRACĄCY WAŻNOŚĆ, PRZESTAJĄCY OBOWIĄZYWAĆ (o przysiędze); WALĄCY SIĘ  
MAQQŰ – WAZA OFIARNA  
MAQRU – NAWODNIONY  
MAQTU(M) – UPADŁY, SPADŁY; ZASZŁY, ZAISTNIAŁY JAKO DOTYKAJĄCY, SPOTYKAJĄCY; POZBAWIONY ZNACZENIA  
lub POZYCJI; NIEWAŻNY, NIEOBOWIĄZUJĄCY (o przysiędze); ZAWALONY  
MAQURRU – MAKURRU \[rodzaj kultowego statku\] \[sum.\]  
MARACU – BYĆ TRUDNYM, MOZOLNYM; BYĆ NIEDOSTĘPNYM  
\[jako rzeczownik\] BYCIE TRUDNYM, MOZOLNYM; BYCIE NIEDOSTĘPNYM  
MARACU(M), MARACU(M) (a) (u) – ZACHOROWAĆ, BYĆ CHORYM  
\[jako rzeczownik\] ZACHOROWANIE, BYCIE CHORYM  
ELI x MARACU – STAWAĆ SIĘ MĘCZARNIĄ DLA x  
MARARU (a/u) – SKOPYWAĆ, PRZEKOPYWAĆ (np. ogród)  
\[jako rzeczownik\] SKOPYWANIE, PRZEKOPYWANIE  
MAR-BANI – DOBRZE URODZONY; WOLNY  
MARCATUM – zob. MARCU(M)  
MARCIŠ – MOZOLNIE, Z TRUDEM  
MARCU(M) – CHORY; TRUDNY; TRUDNO DOSTĘPNY  
l.mn.r.ż. MARCATUM – TRUDY, TRUDNOŚCI, MOZOŁY, STARANIA, ZABIEGI  
MARDUK – MARDUK \[imię boga miasta Babilon\]  
MARHITU(M) – NAŁOŻNICA; KOBIETA; MAŁŻONKA  
MARICU(M), MARICU(M) – CHORUJĄCY  
MARIRU – KOPIĄCY, PRZEKOPUJĄCY  
MARNUATUM – \[staroas.\] jakiś napój alkoholowy \[zapożyczenie z hetyckiego?\]  
MARQITU – MIEJSCE UKRYCIA, KRYJÓWKA, AZYL  
MARRU – SZPADEL, RYDEL, ŁOPATA \[sum.\]  
MARRU(M) – GORZKI  
MARTAKAL lub MAŠTAKAL – ? \[jakaś roślina\]  
MARTU(M) – CÓRKA  
MARTU(M) – WORECZEK ŻÓŁCIOWY  
l.mn. MARRATUM  
MACRAH MARTIM – ZACZĄTEK WORECZKA ŻÓŁCIOWEGO, PRZEWÓD ŻÓŁCIOWY W WĄTROBIE  
MARTUM – GORZKA  
MARŰ – TŁUSTY  
MARU(M), MARU(M), MARU(M) (m’r) – SYN; CHŁOPIEC; DZIECKO PRZYBRANE; DZIECKO; \[sum. DUMU\]  
s.c. MAR, l.mn. MÂR€  
MAR ŠARRI – KRÓLEWICZ  
MARI ŠIPRIM, MAR ŠIPRI – GONIEC, POSŁANIEC  
MÂR ÂLI – MIESZKANIEC MIASTA lub OSIEDLA  
MÂR UMMANI – RZEMIEŚLNIK  
MAR MARI – WNUK  
MAR AHI, MÂR AHÂTI – BRATANEK, SIOSTRZENIEC  
MAR-AŠŠUR – ASYRYJCZYK  
MAR-BABILI – BABILOŃCZYK  
MAR-MATI – DZIECI KRAJU = LUDNOŚĆ KRAJU  
MAR-UGARI – SĄSIAD ZZA MIEDZY  
MARE-NUNE – NARYBEK  
MARUŠTU – ZŁY, NIESZCZĘŚLIWY  
MARUŠTU(M) – NIESZCZĘŚCIE, BIEDA  
MARUTU – SYNOSTWO; DZIECIŃSTWO  
MASAKU – BYĆ W ZŁYM USPOSOBIENIU  
\[jako rzeczownik\] BYCIE W ZŁYM USPOSOBIENIU  
ŠT ŠUTAMSUKU(M): ZOSTAĆ OSZKALOWANYM, ZNIESŁAWIONYM  
\[jako rzeczownik\] BYCIE OSZKALOWANYM, ZNIESŁAWIONYM  
MASKU – ŹLE USPOSOBIONY, W ZŁYM NASTROJU  
MAŠ’ALTU – PYTANIE SIĘ, DOPYTYWANIE, ŚLEDZTWO  
MAŠ’U(M) – ZABRANY  
MAŠA’U(M) (a/u) – ZABIERAĆ  
\[jako rzeczownik\] ZABIERANIE  
MAŠAHU (a/u) – MIERZYĆ  
\[jako rzeczownik\] MIERZENIE  
MAŠALU (a/u) – PRZEPOŁAWIAĆ, DZIELIĆ NA PÓŁ; (St: MAŠIL także:) RÓWNAĆ SIĘ, BYĆ PODOBNYM  
\[jako rzeczownik\] PRZEPOŁAWIANIE, DZIELENIE NA PÓŁ  
MAŠARU (a/u) – WŁÓCZYĆ (o wykonywaniu pewnego rodzaju kary)  
\[jako rzeczownik\] WŁÓCZENIE  
MAŠARU lub WAŠARU(M) (a/i) – BYĆ LUŹNYM; OPUSZCZAĆ (miejsce)  
\[jako rzeczownik\] BYCIE LUŹNYM; OPUSZCZANIE (miejsca)  
D MUŠŠURU: POLUŹNIAĆ, ROZLUŹNIAĆ; ZWALNIAĆ, WYPUSZCZAĆ NA WOLNOŚĆ; POŚWIĘCAĆ,  
REZYGNOWAĆ; ODPROWADZIĆ, UNIEŚĆ (łup); PORZUCAĆ, ZOSTAWIAĆ NA LODZIE; ZNIKAĆ  
\[jako rzeczownik\] POLUŹNIANIE, ROZLUŹNIANIE; ZWALNIANIE, WYPUSZCZANIE NA WOLNOŚĆ; PO-  
ŚWIĘCANIE, REZYGNOWANIE; PORWANIE, UNIESIENIE (łupu); PORZUCANIE, ZOSTAWIANIE NA LO-  
DZIE; ZNIKANIE  
DT MUTAŠŠURU: BYĆ ROZLUŹNIONYM  
\[jako rzeczownik\] BYCIE ROZLUŹNIONYM  
ŠINA MUŠŠURAMA RAMANUŠŠIN – SAMI SIĘ ODDALI (? Sie waren sich selbst überlassen)  
MAŠHU – ZMIERZONY, WYMIERZONY  
MAŠI’U(M) – ZABIERAJĄCY  
MAŠIHU – MIERZĄCY, MIERNICZY?  
MAŠILU – PRZEPOŁAWIAJĄCY, DZIELĄCY NA PÓŁ  
MAŠIRU – WŁÓCZĄCY (o wykonywaniu pewnego rodzaju kary)  
MAŠIRU – OPUSZCZAJĄCY (miejsce)  
MAŠKADU – CHOROBA STAWÓW  
MAŠKANU(M), MAŠKANU(M) – MIEJSCE; MIEJSCOWOŚĆ; OBOZOWISKO; PLAC BUDOWY; KLEPISKO; ZBOŻE  
WYSYPANE NA KLEPISKO; WIĘZY, KAJDANY  
MAŠKU – SKÓRA  
MAŠLIUM – WIADRO SKÓRZANE  
MAŠLU – WPÓŁ, PÓŁ; POŁOWICZNY  
UMU MAŠLU – POŁUDNIE \[pora dnia\]  
MAŠMAŠU – ZAKLINACZ, KAPŁAN EGZORCYSTA \[sum.\]  
MAŠQITU lub MAŠQŰ – WODOPÓJ, POIDŁO  
PAN MAŠQIJA CABTŰ – ONI MAJĄ W SWYCH RĘKACH DOSTĘP DO MOJEGO WODOPOJU  
MAŠQŰ lub MAŠQITU – j.w.  
MAŠRAHU(M) – PRZEPYCH  
MAŠRU – WŁÓCZONY (o wykonywaniu pewnego rodzaju kary)  
MAŠRU – WYRUGOWANY (z miejsca); LUŹNY  
MAŠŠAKKU – (pewnego rodzaju) OFIARA CAŁOPALNA \[sum.\]  
MAŠTAKAL lub MARTAKAL – ? \[jakaś roślina\]  
MAŠTITU lub MALTITU – NAPÓJ  
MAŠŰ(M) (i) (mš’) – ZABIERAĆ; UKRYWAĆ; ZAPOMNIEĆ  
\[jako rzeczownik\] ZABIERANIE; UKRYWANIE; ZAPOMINANIE  
MAŠŰ(M) – ZABRANY; UKRYTY; ZAPOMNIANY  
MAŠŰ(M) – ZABIERAJĄCY; UKRYWAJĄCY; ZAPOMINAJĄCY  
MATI – KIEDY?  
MATIMA – KIEDYKOLWIEK; (w zdaniach negowanych) NIGDY, ANI RAZU  
ADI MATI – JAK DŁUGO  
MATIMA – KIEDYKOLWIEK; (w zdaniu z zaprzeczonym orzeczeniem) NIGDY  
INA MATEMA – KIEDYKOLWIEK  
MATITAN – WSZĘDZIE  
MATNU – CIĘCIWA (łuku)  
MÂTU(M) (mut) – UMIERAĆ  
\[jako rzeczownik\] UMIERANIE  
MATU(M) – UMARŁY, TRUP? NIEBOSZCZYK?  
MATU(M) – KRAJ, ZIEMIA \[r.ż.\]  
l.mn. MATATUM  
MAT AMURRI – ZACHÓD; KRAJ AMORYTÓW  
SEHERTI MATIM – OBSZAR KRAJU  
ANA MAT MATMA – DLA KAŻDEGO KRAJU  
INA LIBBU MATIM – WEWNĄTRZ KRAJU  
MAŢŰ (i) – BYĆ MAŁYM, BYĆ NIEZNACZNYM; ZMNIEJSZAĆ SIĘ; PONIŻYĆ, ZANIEDBAĆ; \[Lipin\] także MINUS  
\[jako rzeczownik\] BYCIE MAŁYM, BYCIE NIEZNACZNYM; ZMNIEJSZANIE SIĘ; PONIŻANIE; ZANIEDBA-  
NIE  
MAŢU – MAŁY, NIEZNACZNY; ZMNIEJSZONY; PONIŻONY, ZANIEDBANY  
MAZZAZU(M) – MIEJSCE; POZYCJA  
MAZZAZ TAZZAZZU – MIEJSCE, NA KTÓRYM STOISZ  
ME – EJ! PATRZŻE! UWAŻAJ!  
ME’AT lub METUM – STO  
s.a. ME  
ŠITTA M€TIM – DWIEŚCIE  
ŠALAŠ ME’AT – TRZYSTA  
ME’ATU lub ME’U – STO  
ANA METUM – W STU  
ME’IŠU – GARDZĄCY, POGARDZAJĄCY, LEKCEWAŻĄCY  
ME’U lub ME’ATU – j.w.  
MEDILU – ZASUWA, RYGIEL  
MEDUM – MEDUM \[nazwa miejscowości w pd. Mezopotamii\]  
MEGGŰ(M) – MĘCZĄCY SIĘ  
MEGRU lub MIGRU(M) – ULUBIENIEC, FAWORYT; FAWORYZOWANY, UPRZYWILEJOWANY; UPRAGNIONY, KO  
CHANY; KOCHANY, UMIŁOWANY; POSŁUSZNY, ZGODNY  
s.c. MIGIR  
M€GŰTU – NIEDBALSTWO, NIECHLUJSTWO; zob. EGU(M)  
MEHIRTU lub MIHIRTU – PRZYCHÓD, DOCHÓD; DAR, DATEK  
MEHRAT lub MIHRAT, MEHRET – NAPRZECIW, NAPRZECIWKO  
MEHRET lub MIHRAT, MEHRAT – j.w.  
MEHRU(M) – RÓWNY; ODPOWIEDNI; KTOŚ RÓWNY RANGĄ; RÓWNOUPRAWNIONY  
MEHRU(M) – ODPOWIEDŹ (na list)  
MEHŰ – BURZA CIĄGNĄCA Z POŁUDNIA; SZTORMOWY WIATR  
MEKŰ – ZAPLANOWANIE; ZAMIAR, CEL \[lekcja niepewna\]  
MELAMMU – BLASK, POŁYSK \[sum.\]  
MELECU(M) – RADOŚĆ, RADOWANIE SIĘ  
MELELU(M) (i) – TAŃCZYĆ; SKAKAĆ  
\[jako rzeczownik\] TAŃCZENIE; SKAKANIE  
MELILU(M) – TAŃCZĄCY, SKACZĄCY, TANCERZ? SKOCZEK?  
MELQETU – OBOWIĄZEK, POWINNOŚĆ  
MELŰ – WCHODZENIE, WZNOSZENIE SIĘ; ZBOCZE, STOK; WZGÓRZE, PAGÓREK  
MENU lub MINU – LICZBA  
ANA LA MINAM – NIEZLICZONY; BEZ LIKU  
INA LA MENI – W NIEZLICZONYCH (miejscach)  
MENŰTU lub MINŰTU – LICZBA, MIARA  
MERÂNU lub MER€NU – NAGOŚĆ  
MER€NU lub MERÂNU – j.w.  
MEREŠU(M) lub MERIŠTU – PLANTACJA, POLE UPRAWNE  
MERIŠTU lub MEREŠU(M) – j.w.  
MESŰ(M) (msi) – MYĆ  
M€ŠÂRU lub MÎŠÂRU, MEŠRU – PRAWO, PORZĄDEK PRAWNY; USTAWA; SPRAWIEDLIWOŚĆ; PORZĄDEK, UPO-  
RZĄDKOWANIE; PRAWDA  
l.mn. MÎŠÂRI  
MEŠR€TU – CZŁONKI \[l.mn.\]  
MEŠLU lub MIŠLU – POŁOWA, JEDNA DRUGA  
MEŠRU lub MÎŠÂRU, M€ŠÂRU – PRAWO, PORZĄDEK PRAWNY; USTAWA; SPRAWIEDLIWOŚĆ; PORZĄDEK, UPO-  
RZĄDKOWANIE; PRAWDA  
l.mn. MÎŠÂRI  
MEŠRŰ – BOGACTWO  
MEŠU (e) – POGARDZAĆ, GARDZIĆ, LEKCEWAŻYĆ  
\[jako rzeczownik\] POGARDZANIE, GARDZENIE, LEKCEWAŻENIE  
MEŠU – WZGARDZONY, POGARDZANY, LEKCEWAŻONY  
METEQU – PASAŻ; PRZEBIEG  
METUM lub ME’AT – STO  
s.a. ME  
ANA METUM – W STU  
MEŢŢU lub MEŢU – BROŃ BOSKA \[sum.\]  
MEŢU lub MEŢŢU – j.w.  
-MI – \[partykuła mowy bezpośredniej; różnie używana w poszczególnych dialektach\]  
MICRU – GRANICA, OBSZAR GRANICZNY; OBSZAR  
s.c. MICIR  
MIGRU(M) lub MEGRU – ULUBIENIEC, FAWORYT; FAWORYZOWANY, UPRZYWILEJOWANY; UPRAGNIONY, KO  
CHANY; KOCHANY, UMIŁOWANY; POSŁUSZNY, ZGODNY  
s.c. MIGIR  
MIHICTU – CIOS, UDERZENIE  
MIHIRTU lub MEHIRTU – PRZYCHÓD, DOCHÓD; DAR, DATEK  
MIHIRTU – PRZECIWSTAWNOŚĆ, SPRZECZNOŚĆ  
MIHIRU lub MIHRU – PRZÓD, CZĘŚĆ PRZEDNIA, PRZEDNIA STRONA, FRONT; CZOŁO (kolumny wojskowej); RÓWNY;  
PRZECIWIEŃSTWO; PRZECIW; Z PRZODU, NA CZELE  
s.c. MIHIR, r.ż. MIHIRTU, s.c.r.ż. MIHRIT, MIHRAT, MIHIRÂT  
MIHIRTA CABATU – STANĄĆ NA CZELE WOJSKA  
MIHRAT lub MEHRET, MEHRAT – NAPRZECIW, NAPRZECIWKO  
MIHRU lub MIHIRU – PRZÓD, CZĘŚĆ PRZEDNIA, PRZEDNIA STRONA, FRONT; CZOŁO (kolumny wojskowej); RÓW-  
NY; PRZECIWIEŃSTWO; PRZECIW; Z PRZODU, NA CZELE  
s.c. MIHIR, r.ż. MIHIRTU, s.c.r.ż. MIHRIT, MIHRAT, MIHIRÂT  
MIHIRTA CABATU – STANĄĆ NA CZELE WOJSKA  
MIHRU lub GABRU – ODPOWIEDNI; RÓWNY; KOPIA itp.  
MILKU(M) – PLAN, RADA; NAMYSŁ, WGLĄD, ROZWAŻANIE, ROZSTRZYGNIĘCIE \[Lipin\]  
ENINNAMA (lub ENENNAMA) MILIKŠU MILKU – TERAZ WRESZCIE JEGO NAMYSŁ BĘDZIE (WNIKLI-  
WYM) ROZWAŻANIEM  
MILLATU(M) – GRABIEŻ  
MILQITU – MAJĄTEK, MIENIE  
MILU(M) – OBFITOŚĆ; POWÓDŹ, WEZBRANIE WÓD  
MILI IRTIM – ODWAGA; SUKCES  
MIMMA – \[MIN(U)+MA\] WSZYSTKO, COKOLWIEK, COŚ; (w zdaniu zaprzeczonym) NIC  
MIMMA ŠA lub MIMMA MALA – WSZYSTKO, CO  
MIMMA ŠUMŠU – W OGÓLE WSZYSTKO, W OGÓLE COŚ  
MIMMA LA ŠÂM (KÂM) – COŚ, CO NIE JEST JEGO (TWOJE)  
MIMMA LA TEPPUŠ – NIE POWINIENEŚ NIC ROBIĆ  
MIMMAMU(M) – WSZYSCY BEZ WYJĄTKU  
MIMMŰ lub MINMŰ – WŁASNOŚĆ, DOBRO; WSZYSTKO, WSZYSTKO Z, WSZYSTKO CO; (w zdaniach zanegow.) NIC,  
NIC Z  
-MIN \[asyr.\] – \[wyrażenie irrealności treści zdania\]  
MINMŰ lub MIMMŰ – j.w.  
MINŰ, MINU lub MENU – LICZBA  
ANA LA MINAM – NIEZLICZONY; BEZ LIKU  
INA LA MENI – W NIEZLICZONYCH (miejscach)  
MINU(M), MINUM – CO?  
ANA MINIM – DLACZEGO?  
MINIM – CZEGO?  
MINAM – CO?  
ANA MINIM, AMMINIM – CZEMU?  
MINŰTU lub MENŰTU – LICZBA, MIARA  
MINUTU – LICZENIE  
MIQITTU(M) – UPADEK, KLĘSKA; PADANIE (bydła wskutek zarazy)  
MIQTU – UPADEK  
MIQIT GIRRI – POŻAR  
MIQŰ(M) – SIŁA PRZYCIĄGANIA, CHARYZMA; UROK, CZAR, POWAB  
MIRSU – (pewnego rodzaju) PIERNIK  
MISU – ŚCIERAĆ, WYCIERAĆ  
MÎŠÂRU lub M€ŠÂRU, MEŠRU – PRAWO, PORZĄDEK PRAWNY; USTAWA; SPRAWIEDLIWOŚĆ; PORZĄDEK, UPO-  
RZĄDKOWANIE; PRAWDA  
l.mn. MÎŠÂRI  
MIŠLANU – \[składnik zwrotu\]  
ANA MIŠLANI – NA POŁOWY  
MIŠLU lub MEŠLU – POŁOWA, JEDNA DRUGA  
MITGARU – PRZYCHYLNY, SPRZYJAJĄCY  
MITGURTU(M) – WZAJEMNE DOJŚCIE DO POROZUMIENIA  
MITHARIŠ, MITHARIŠ – NA RÓWNE CZĘŚCI, PO RÓWNO; JEDEN I DRUGI, RAZEM; \[Lipin\] WSPÓLNIE, ZGODNIE  
MITHARUM – ODPOWIADAJĄCY SOBIE WZAJEMNIE  
MITU – MARTWY; NIEBOSZCZYK  
MITUTU – ŚMIERĆ; (Todsfin)  
MŰ(M) lub MAMU – WODA \[l.mn.\]  
l.mn. i G-A M€  
INA LA M€ – Z BRAKU WODY  
MU’DŰ – MNÓSTWO  
MU’IRRU – NADAWCA, WYSYŁAJĄCY  
MU’URU – D: WYSYŁAĆ, SKIEROWAĆ, WZYWAĆ; zob. (W)ARU  
\[jako rzeczownik\] WYSYŁANIE, SKIEROWANIE, WZYWANIE  
MU’URU – WYSŁANY, SKIEROWANY, WZYWANY, WEZWANY  
MUBALLICU – ZEZUJĄCY, PATRZĄCY ZEZEM; WYGLĄDAJĄCY, WYPATRUJĄCY; WARTOWNIK?  
MUBALLIŢU(M), MUBALLIŢU(M) – ZACHOWUJĄCY PRZY ŻYCIU, PODTRZYMUJĄCY ŻYCIE, RATUJĄCY ŻYCIE?  
MUBALLŰ – GASZĄCY, WYGASZAJĄCY (ogień)  
MUBANNŰ (M), MUBANNŰ (M), MUBANNŰ (M) – BUDUJĄCY Z ROZMACHEM, WYKONUJĄCY lub TWORZĄCY W IM-  
PONUJĄCYM STYLU, POWODUJĄCY W IMPONUJĄCY SPOSÓB, KOŃCZĄCY Z HUKIEM; RODZĄCY  
W ATMOSFERZE WIELKIEGO WYDARZENIA; UJAWNIAJĄCY JAKO COŚ SENSACYJNEGO; PLANU-  
JĄCY lub PRZEWIDUJĄCY JAKO COŚ ZNACZĄCEGO; POKRYWAJĄCY ORNAMENTEM, UPIĘKSZAJĄCY \[imie-  
słówD od BANŰ\]  
MUBARRU? – WYJAŚNIAJĄCY, WYŚWIETLAJĄCY (sprawę w sądzie); PODAJĄCY, WYMIENIAJĄCY, OŚWIADCZAJĄCY;  
USTALAJĄCY  
MUBAŠŠILU – GOTUJĄCY, SMAŻĄCY, PIEKĄCY  
MUBAŠŠIRU – OGŁASZAJĄCY, OBWIESZCZAJĄCY, ZAPOWIADAJĄCY, KOMUNIKUJĄCY  
MUBATTIQU(M) – ODCINAJĄCY  
MUBBALKITU(M) – OBRACAJĄCY SIĘ; BUNTUJĄCY SIĘ, OPIERAJĄCY SIĘ; ODPADAJĄCY; PRZEKRACZAJĄCY, PRZE-  
CHODZĄCY (góry); ODBIEGAJĄCY  
MUBBALŢIT’U – UCIEKAJĄCY; BŁĄKAJĄCY, SCHODZĄCY Z DROGI  
MUBBANŰ (M) –TWORZĄCY \[imiesłówN od BANŰ\]  
MUBBAŠU(M), MUBBAŠU(M), MUBBAŠU(M) – POWSTAJĄCY, ZDĄŻAJĄCY KU ISTNIENIU  
MUBBATQU(M) – PRZERYWAJĄCY, PRZECINAJĄCY; PSUJĄCY SIĘ  
MUBBIRU – POSĄDZAJĄCY, OBWINIAJĄCY; SKŁADAJĄCY FAŁSZYWE DONIESIENIE; CZARUJĄCY  
MUBTAZZI’U(M) – ZNĘCAJĄCY SIĘ USTAWICZNIE  
MUCABBTU(M) – ŁAPIĄCY, ORGANIZUJĄCY ŁAPANIE  
MUCCABTU(M) – CHWYTAJĄCY, BIORĄCY; BIORĄCY W POSIADANIE  
MUCCU – D: ROZSZERZAĆ; ROZPINAĆ, ZDEJMOWAĆ (o niektórych rodzajach odzieży)  
\[jako rzeczownik\] ROZSZERZANIE; ROZPINANIE, ZDEJMOWANIE  
MUCCU – ROZSZERZONY; ROZPIĘTY, ZDJĘTY  
MUCIBBU(M) – OGLĄDAJĄCY, SPOGLĄDAJĄCY; OBSERWATOR? BADACZ?  
MUCLALU – POŁUDNIE \[część dnia\]  
MUCLALUM – ODPOCZYNEK  
MUCTABTU(M) – CHWYTAJĄCY SIĘ NAWZAJEM, TRZYMAJĄCY JEDEN DRUGIEGO; ZRASTAJĄCY SIĘ (z czymś)  
MUDABIRU – PUSTYNIA \[słowo obcego pochodzenia\]  
MUDAMMIQU(M) – CZYNIĄCY DOBRO, DOBROCZYŃCA? ŁASKAWCA?  
MUDAPPIRU – POZOSTAJĄCY Z DALA, ODDALAJĄCY SIĘ; WYPĘDZAJĄCY  
MUDATU – WIEDZĄCA, DOŚWIADCZONA  
\[jako abstrakt\] DOŚWIADCZENIE  
MUDDIŠU – ODNAWIAJĄCY, ODBUDOWUJĄCY, BUDUJĄCY NA NOWO, RESTAURUJĄCY  
MUDDŰ – INFORMUJĄCY, OKREŚLAJĄCY, ZAPOZNAJĄCY  
MUDDŰM – USTANAWIAJĄCY NA STAŁE, UTRWALAJĄCY; zob. ADŰ  
MUDTAKSU(M) – NABRZMIEWAJĄCY Z OBU STRON  
MUDŰ – WIEDZĄCY, DOŚWIADCZONY  
MUDŰTU – WIADOMOŚĆ  
MUGALLIBU – GOLĄCY; GOLARZ?  
MUGAMMIRU(M) – CZYNIĄCY KOMPLETNYM, ZUPEŁNYM, DOPEŁNIAJĄCY, UZUPEŁNIAJĄCY; KOŃCZĄCY; ZADAWA-  
LAJĄCY  
MUGTAMRU(M), MUGTAMRU(M): DOKONUJĄCY; SPRAWCA?; PRZEPROWADZAJĄCY, ODBYWAJĄCY (coś w czasie)  
MUHABBU – OCZYSZCZAJĄCY  
MUHAPPIDU – PSUJĄCY (wzrok)  
MUHHALLILU – PRZECISKAJĄCY SIĘ  
MUHHELCITU(M) – ŚLISKOŚĆ; COŚ ŚLISKIEGO \[r.ż.\]  
MUHHELCŰ(M) – ŚLIZGAJĄCY SIĘ  
MUHHI – zob. ELI  
MUHHIRU – SPÓŹNIAJĄCY SIĘ, OPÓŹNIAJĄCY SIĘ; POZOSTAJĄCY W TYLE  
MUHHIZU – POWLEKAJĄCY (metalem)  
MUHHU(M) – WIERZCH, WIERZCHNIA CZĘŚĆ, WIERZCHNIA STRONA; CZASZKA; NAKRYCIE GŁOWY  
ANA MUHHI ANNITI \[średniobab.\] – POZA TYM, W DODATKU  
ANA MUHHI – ZA, ZBYT (?)  
ANA MUHHI \[nowobab.\] – CELEM, W CELU, DLA  
INA MUHHI – Z POWODU, ZE WZGLĘDU NA, DLA, WZGLĘDEM, CO DO  
INA MUHHI – NA; Z POWODU (kogoś smucić się); Z POWODU, ZE WZGLĘDU NA; NADTO, PRÓCZ TEGO  
INA MUHHIŠU – DLATEGO  
MUHTADDU(M): BEZ PRZERWY, WCIĄŻ NA NOWO CIESZĄCY SIĘ; WESOŁEK?  
MUHTAŠŠILU(M): MIELĄCY ZBOŻE; MŁYNARZ?  
MUKALLIMU – POKAZUJĄCY, UKAZUJĄCY  
MUKAMMIRU: OBALAJĄCY  
MUKAMMISU: SCHYLAJĄCY SIĘ, POCHYLAJĄCY SIĘ; KLĘKAJĄCY  
MUKANNIŠU(M), MUŠAKNIŠU(M): ZMUSZAJĄCY DO PODDANIA SIĘ; UJARZMIAJĄCY, PODPORZĄDKOWUJĄCY; SKŁA-  
NIAJĄCY; ZGINAJĄCY; POSKROMICIEL?  
MUKASSŰ(M): ZARASTAJĄCY  
MUKAPPILU: (KH:) GWAŁCĄCY; GWAŁCICIEL?  
MUKARRICU: OCZERNIAJĄCY, ZNIESŁAWIAJĄCY; OSZCZERCA? POTWARCA?  
MUKAŠŠIDU(M), MUKAŠŠIDU(M): WYPĘDZAJĄCY, CHWYTAJĄCY  
MUKATTIMU(M), MUKATTIMU(M): ODZIEWAJĄCY, UBIERAJĄCY; UKRYWAJĄCY, ZATAJAJĄCY, TUSZUJĄCY; GARDE-  
ROBIANY? SZATNY?  
MUKILLU(M) – TRZYMAJĄCY  
MUKIL REŠ – TRZYMAJĄCY GŁOWĘ; WSPIERAJĄCY, WSPOMAGAJĄCY, POMOCNIK  
MUKKALŰ, MUKKALŰ: POZOSTAWIAJĄCY, PONIECHAJĄCY  
MUKTANNIŠU(M): POCHYLAJĄCY SIĘ, ZGINAJĄCY SIĘ  
MUKTAŠŠU(M): ZGINAJĄCY SIĘ, SKŁANIAJĄCY SIĘ, PODDAJĄCY SIĘ  
MUKTATTIMU(M), MUKTATTIMU(M): CIĄGLE, WIELOKROTNIE UKRYWAJĄCY, ZATAJAJĄCY, TUSZUJĄCY  
MULAPPITU(M) – ZATRZYMUJĄCY SIĘ, ZWLEKAJĄCY; KUNKTATOR?  
MULLICU(M) – CIESZĄCY, RADUJĄCY (kogoś), DOPROWADZAJĄCY DO RADOŚCI  
MULLILUM – ZAWIESZAJĄCY, WIESZAJĄCY  
MULLILU – KRZYCZĄCY Z RADOŚCI, WOŁAJĄCY RADOŚNIE  
MULLŰ(M), MULLŰ(M): PODNOSZĄCY, PODWYŻSZAJĄCY; MIANUJĄCY?  
MULMULLU – STRZAŁA \[sum.\]  
MULTABBIRU(M), MULTABBIRU(M) – STARZEJĄCY SIĘ  
MULTAHŢU – WYJEŻDŻAJĄCY (ZA GRANICĘ), EMIGRANT  
MULTAMDU(M) – ZAWIADAMIAJĄCY, INFORMUJĄCY; INFORMATOR? DONOSICIEL?  
MULŰ – WCHODZENIE, WSPINANIE SIĘ  
MUMA’IRU – KOMENDANT; ZARZĄDCA  
MUMA’’URU – WYSYŁAJĄCY, KIERUJĄCY; WZYWAJĄCY  
MUMACCU – ROZSZERZAJĄCY; ROZPINAJĄCY, ZDEJMUJĄCY  
MUMAŠŠIRU(M) – POLUŹNIAJĄCY, ROZLUŹNIAJĄCY; ZWALNIAJĄCY, WYPUSZCZAJĄCY NA WOLNOŚĆ; POŚWIĘCAJĄ-  
CY, PORZUCAJĄCY; ODPROWADZAJĄCY, UNOSZĄCY (łup); PORZUCAJĄCY, ZOSTAWIAJĄCY NA LODZIE;  
POWODUJĄCY ZNIKNIĘCIE, USUWAJĄCY  
MUMMAHCU(M), MUMMAHCU(M) – OTRZYMUJĄCY KIJ DO BICIA; NADZORCA?  
MUMTAHCU(M), MUMTAHCU(M) – WALCZĄCY POMIĘDZY SOBĄ  
MUMTAHRU(M), MUMTAHRU(M): WZAJEMNIE RÓWNAJĄCY SIĘ; DORÓWNUJĄCY SOBIE  
MUMTALKU – NARADZAJĄCY SIĘ Z, RADZĄCY SIĘ; UCZESTNIK NARADY? RADCA? CZŁONEK RADY?  
MUMTAQQITU(M) – PADAJĄCY RAZ ZA RAZEM, PADAJĄCY MASOWO?  
MUNA’’IDU – WYWYŻSZAJĄCY, CHWALĄCY, ODDAWAJĄCY CZEŚĆ  
MUNA’’IDU(M), MUNA’’IDU(M): ZWRACAJĄCY UWAGĘ, ZAWIADAMIAJĄCY, DONOSZĄCY  
MUNABBŰ – SKARŻĄCY SIĘ, ŻALĄCY SIĘ, NARZEKAJĄCY, BIADAJĄCY  
MUNAKKIRU – PROWADZĄCY NA INNE MIEJSCE; SPRZĄTAJĄCY, USUWAJĄCY; PRZESUWAJĄCY, PRZEMIESZCZAJĄCY;  
ZMIENIAJĄCY (ubranie), PRZEBIERAJĄCY SIĘ  
MUNAMMIRU: (przenośnie:) ROZPALAJĄCY  
MUNAMMŰ – PUSTOSZĄCY  
MUNAPPIŠU – DAJĄCY ODETCHNĄĆ, POZWALAJĄCY ODPOCZĄĆ  
MUNAŠŠIPU – WYMIATAJĄCY PRECZ  
MUNDAHCU – BOJOWNIK, ŻOŁNIERZ  
MUNNABTU – ZBIEG, UCIEKINIER  
MUNNAMRUM – WIDZĄĆY SIĘ (Z KIMŚ NAWZAJEM), SPOTYKAJĄCY SIĘ (Z KIMŚ);ZBLIŻAJĄCY SIĘ DO ZIEMI (o skale);  
WSCHODZĄCY (o ciałach niebieskich)  
MUNNAPHU(M) – WYBUCHAJĄCY, ROZPALAJĄCY SIĘ (o ogniu)  
MUNNAPLUM, MUNNAPLU – DOSTARCZAJĄCY, ZAOPATRUJĄCY, DOSTAWCA, ZAOPATRZENIOWIEC  
MUNNARBU – UCIEKAJĄCY, UCIEKINIER, ZBIEG; zob. \[ARABU\]  
MUNNEDRU – BOJĄCY SIĘ, OGARNIĘTY STRACHEM, LĘKAJĄCY SIĘ; ULEGAJĄCY ZAĆMIENIU (o ciele niebieskim)  
MUNNEDRU(M) – OBEJMUJĄCY SIĘ (wzajemnie)  
MUNNEMDU(M), MUNNEMDU(M) – OPIERAJĄCY SIĘ O SIEBIE, SKUPIAJĄCY SIĘ BLISKO SIEBIE; SPOTYKAJĄCY SIĘ,  
SCHODZĄCY SIĘ; NISZCZĄCY SIĘ  
MUPAHHIRU(M) – ZBIERAJĄCY; WZMACNIAJĄCY  
MUPAHHU –ZAMIENIAJĄCY, WYMIENIAJĄCY  
MUPAQQIDU – UMIESZCZAJĄCY, USTAWIAJĄCY  
MUPARRIRU – ROZPRASZAJĄCY, ROZGRAMIAJĄCY  
MUPAŠŠIRU – OGŁASZAJĄCY, OBWIESZCZAJĄCY, ZAPOWIADAJĄCY, KOMUNIKUJĄCY; HEROLD?  
MUPATTI’U(M) – OTWIERAJĄCY, ODSŁANIAJĄCY; WYCZULAJĄCY, UWRAŻLIWIAJĄCY; NAKŁANIAJĄCY DO ROZSĄDKU;  
PRZEBIJAJĄCY, ROZPRASZAJĄCY (ciemność)  
MUPAZZIRU – UKRYWAJĄCY  
MUPETTŰ(M), MUPETTŰ(M) – OTWIERAJĄCY, ODSŁANIAJĄCY; WYCZULAJĄCY, UWRAŻLIWIAJĄCY; NAKŁANIAJĄCY DO  
ROZSĄDKU; PRZEBIJAJĄCY, ROZPRASZAJĄCY (ciemność)  
MUPPALSU(M) – PATRZĄCY  
MUPPARDI’U –STAJĄCY SIĘ JASNYM, JAŚNIEJĄCY; ROZWESELAJĄCY, STAJĄCY SIĘ WESOŁYM; POGODNIEJĄCY,  
STAJĄCY SIĘ POGODNYM  
MUPPARQIDU(M) – KŁADĄCY SIĘ NA PLECACH  
MUPPARRIRU(M) – ROZBIEGAJĄCY SIĘ  
MUPPARSU – USTAJĄCY; USTĘPUJĄCY, WYCOFUJĄCY SIĘ  
MUPPARŠU – LECĄCY, ODLATUJĄCY  
MUPPIQU(M): OBRASTAJĄCY, ZARASTAJĄCY  
MUPTALLIHU(M) – ŻYJĄCY W CIĄGŁYM STRACHU, BOJĄCY SIĘ STALE  
MUPTARRICU – WYPEŁNIAJĄCY, CELEBRUJĄCY  
MUPTATTI’U(M) – OTWIERAJĄCY RAZ PO RAZ; ODŹWIERNY?  
MUPTATTŰ(M), MUPTATTŰ(M) – OTWIERAJĄCY RAZ PO RAZ  
MUQA’’ Ű – CZEKAJĄCY, OCZEKUJĄCY  
MUQATTŰ(M): (w tekstach astrologicznych w odniesieniu do zaćmienia:) ZBLIŻAJĄCY SIĘ DO KOŃCA  
MUQQARRU(M) – ZGINAJĄCY SIĘ  
MUQQELPITU – STATEK (ŻAGLOWY) (bez steru), ŻAGLOWIEC  
ELEP ŠA MUQQELPITI – STATEK ŻAGLOWY  
MUQQELPŰ(M) – UNOSZĄCY Z PRĄDEM, PORYWAJĄCY Z PRĄDEM  
MUQTA’ILU – CIERPIĄCY W MILCZENIU  
MUQTABLU – BOJOWNIK  
MUQTABU(M), MUQTABU(M) – MÓWIĄCY, WYJAŚNIAJĄCY, NAKAZUJĄCY; MÓWCA?  
MUQTARBU(M), MUQTARBU(M) – ZBLIŻAJĄCY SIĘ NAWZAJEM DO SIEBIE  
MUQTATTŰ(M) – WYKONUJĄCY DO KOŃCA  
MURABBITU – PRZYBRANA MATKA  
MURABBŰ – PRZYBRANY OJCIEC  
MURABBŰ(M), MURABBŰ(M) – CZYNIĄCY DUŻYM, WIELKIM; WYCHOWUJĄCY; HODUJĄCY; PODNOSZĄCY; WYNO-  
SZĄCY, WYWYŻSZAJĄCY; HODOWCA? WYCHOWAWCA?  
MURADDŰ –DODAWAJĄCY, DOŁĄCZAJĄCY  
MURAGGIBU – KŁADĄCY BELKI STROPOWE; DZIELĄCY NA PIĘTRA, POKŁADY  
MURAKKIBU(M) – ZAPŁADNIAJĄCY  
MURAKKISU – ZOBOWIĄZUJĄCY  
MURAKU – DŁUGOŚĆ  
MURAMMŰ – UMARZAJĄCY; UWALNIAJĄCY; TRAKTUJĄCY Z POBŁAŻANIEM, FOLGUJĄCY; PRZEBACZAJĄCY  
MURATTŰ – PRZYMOCOWUJĄCY (skrzydło bramy)  
MURAŢŢIBU – ZWILŻAJĄCY, NAWILŻAJĄCY, ZMACZAJĄCY  
MURCU(M) – CHOROBA  
MURNISQU – KOŃ; RUMAK  
MURRAKSU – ZNAJDUJĄCY SIĘ RAZEM; CZŁONEK, ELEMENT ZBIOROWOŚCI?  
MURRAQŰ – CHOWAJĄCY, KRYJĄCY, UKRYWAJĄCY SIĘ  
MURRIKU(M) – PRZEDŁUŻAJĄCY  
MURTABBŰ(M), MURTABBŰ(M) – PODNOSZĄCY DO GÓRY; CZYNIĄCY DUŻYM, WIELKIM; HODUJĄCY; WYCHOWUJĄCY  
MURTABŰ(M), MURTABŰ(M) – ROSNĄCY, STAJĄCY SIĘ DUŻYM  
MURTAKBU(M) – JEŻDŻĄCY WIERZCHEM JEDEN ZA DRUGIM;UCZESTNIK KAWALKADY, KARAWANY? ODDZIAŁU?  
MURTAPPIDU – BŁĄKAJĄCY SIĘ; WŁÓCZĄCY SIĘ, WŁÓCZĘGA  
MURTAŠU – PROMIENIEJĄCY  
MURTEDDŰ lub MURTEDŰ – WOŹNICA (wozu do transportu ciężarów)  
MURTEDŰ lub MURTEDDŰ – j.w.  
MURTEDŰ(M), MURTEDŰ(M) – PODĄŻAJĄCY KU SOBIE; IDĄCY RÓWNOLEGLE  
MURU – ŹREBIĘ  
MUSAHHIRU(M), MUSAHHIRU(M) – OBRACAJĄCY, ODWRACAJĄCY; ODPIERAJĄCY  
MUSAHHŰ – ZDRADZAJĄCY; PODBURZAJĄCY; BUNTUJĄCY SIĘ; ODPADAJĄCY  
MUSALLU – PROSZĄCY, BŁAGAJĄCY  
MUSANNIQU(M) – ŚCIEŚNIAJĄCY, ZAWĘŻAJĄCY; KONTROLUJĄCY, BADAJĄCY  
MUSAPPIHU(M) – ROZPRASZAJĄCY  
MUSAPPU – MODLĄCY SIĘ, BŁAGAJĄCY  
MUSAQQU – NACISKAJĄCY, NALEGAJĄCY, WYWIERAJĄCY PRESJĘ, PRZEŚLADUJĄCY; PRZEŚLADOWCA?  
MUSARRIRU – DOPUSZCZAJĄCY SIĘ WIAROŁOMSTWA, MÓWIĄCY WIAROŁOMNIE; ZEZNAJĄCY FAŁSZYWIE? FAŁSZYWY  
ŚWIADEK?  
MUSARŰ – INSKRYPCJA; GRZĄDKA, ZAGON \[sum.\]  
MUSARU lub MUŠARU – NAPIS; DOKUMENT; STELA  
MUSSAHRU(M), MUSSAHRU(M) – OBRACAJĄCY, PRZEKRĘCAJĄCY  
MUSSAHŰ – BUNTUJĄCY SIĘ; BUNTOWNIK?  
MUSSUKU lub MUŠŠUKU – ŻAŁOSNY, MIZERNY, GODNY POLITOWANIA; SKURCZONY; SPĘKANY, POPĘKANY  
MUSTAQRU – POROZUMIEWAJĄCY SIĘ; MÓWIĄCY, INFORMUJĄCY, POWIADAMIAJĄCY  
MUSUKKANNU – WARTOŚCIOWY GATUNEK DREWNA \[sum.\]  
MUŠA”ILU – OSTRZĄCY (o broni)  
MUŠABBI’U(M) – SYCĄCY, OFIARUJĄCY BY NASYCIĆ; ZADAWALAJĄCY  
MUŠABBŰ(M) – NASYCAJĄCY, SYCĄCY, OFIARUJĄCY BY NASYCIĆ; ZADAWALAJĄCY  
MUŠABILU(M), MUŠABILU(M:- PRZYNOSZĄCY; PRZESYŁAJĄCY; POSYŁAJĄCY; PRZEWOŻĄCY  
MUŠABQIMU(M) – KAŻĄCY STRZYC, POWODUJĄCY STRZYŻENIE  
MUŠABŠU(M), MUŠABŠU(M), MUŠABŠU(M) – WYWOŁUJĄCY, SPRAWIAJĄCY  
MŰŠABU – MIESZKANIE, POMIESZCZENIE MIESZKALNE (szczególnie królewskie), KOMNATA; MIEJSCE ZAMIESZ-  
KANIA  
s.c. MŰŠAB  
MUŠACBITU(M) – ZLECAJĄCY ZŁAPANIE; OSIEDLAJĄCY, OSADZAJĄCY; STAWIAJĄCY  
MUŠACŰ – WYPROWADZAJĄCY, KAŻĄCY WYJŚĆ; WYSTĘPUJĄCY (z brzegów); WYNOSZĄCY; WYWLEKAJĄCY, ODCIĄGA-  
JĄCY; UWALNIAJĄCY; DOPROWADZAJĄCY DO UPADKU; ZDRADZAJĄCY (tajemnicę); ŚCIĄGAJĄCY (daninę)  
MUŠADKU(M) – KAŻĄCY ZABIĆ; SKAZUJĄCY NA ŚMIERĆ  
MUŠAHHINU – ROZGRZEWAJĄCY, NAGRZEWAJĄCY, OCIEPLAJACY  
MUŠÂHIZUM – KAŻĄCY WZIĄĆ, KAŻĄCY BRAĆ; PRZYWŁASZCZYCIEL (?), ZABORCA (?)  
MUŠAKILUM – KAŻĄCY JEŚĆ, KARMICIEL, ŻYWICIEL  
MUŠAKLILU – KOŃCZĄCY, DOKONUJĄCY  
MUŠALIKUM – ZLECAJĄCY WYRUSZENIE W DROGĘ, DECYDUJĄCY O WYMARSZU  
MUŠALLIMU(M) – ZACHOWUJĄCY, KONSERWUJĄCY;UTRZYMUJĄCY W DOBRYM STANIE; WYKAZUJACY ZDROWIE;  
DAJĄCY ODSZKODOWANIE, WYNAGRADZAJĄCY, REKOMPENSUJĄCY (szkodę), WETUJĄCY;ZWRACAJĄCY,  
ODDAJĄCY; PRZYWRACAJĄCY DO DOBREGO STANU, NAPRAWIAJĄCY (błąd);REALIZUJĄCY, WYPEŁNIAJĄ-  
CY ( przeznaczenie)  
MUŠALLIŠU – ROBIĄCY TRZYKROTNIE, ROBIĄCY NA TRZY RAZY, W TRZECH ETAPACH  
MUŠALMŰ – KAŻĄCY OBLEGAĆ, NAKAZUJĄCY OKRĄŻENIE  
MUŠALPITU(M) – ŁUPIĄCY, GRABIĄCY, GRABIEŻCA? PUSTOSZĄCY, NISZCZĄCY, NISZCZYCIEL? DOTYKAJĄCY, OBMA-  
CUJĄCY, RUSZAJĄCY;HAŃBIĄCY, BEZCZESZCZĄCY  
MUŠAMQITU(M) –ŚCINAJĄCY, DRWAL? DOPROWADZAJĄCY DO UPADKU, UPUSZCZAJĄCY  
MUŠAN’IDU(M), MUŠAN’IDU(M): ZAWIADAMIAJĄCY, MELDUJĄCY  
MUŠANNŰ, MUŠANNŰ – ROBIĄCY DWA RAZY, ZNÓW ROBIĄCY; POWTARZAJĄCY (czasem w sensie: DONOSZĄCY)  
MUŠAPPILU(M), MUŠAPPILU(M) – KIERUJĄCY SIĘ W DÓŁ; CZYNIĄCY NISKIM; GŁĘBOKO KOPIĄCY  
MUŠAPRIŠU – ROZCIĄGAJĄCY, ROZPOŚCIERAJĄCY; KAŻĄCY LECIEĆ, POWODUJĄCY ODLOT  
MUŠAQQŰ(M), MUŠAQQŰ(M), MUŠAQQŰ(M) – KIERUJĄCY SIĘ DO GÓRY; WNOSZĄCY NA GÓRĘ; PODNOSZĄCY,  
WZNOSZĄCY; CZYNIĄCY WYSOKIM  
MUŠARBŰ(M), MUŠARBŰ(M): WYWYŻSZAJACY; CZYNIĄCY SILNYM, POTĘŻNYM  
MUŠARDŰ(M), MUŠARDŰ(M) – KAŻĄCY PŁYNĄĆ, POWODUJĄCY WYPŁYW; CIEKNĄCY  
MUŠARRIMU – SZLIFUJĄCY (róg)  
MUŠARRU – SCHYLAJĄCY SIĘ, POCHYLAJĄCY SIĘ; KŁANIAJĄCY SIĘ NISKO, ZGINAJĄCY KARK  
MUŠARŠIDU – ZAKŁADAJĄCY SOLIDNIE  
MUŠARŠŰ(M) –PRZEKAZUJĄCY, UPOSAŻAJĄCY, OBDAROWUJĄCY (?)  
MUŠARU lub MUSARU – NAPIS; DOKUMENT; STELA  
MUŠASHIRU(M), MUŠASHIRU(M) – POBUDZAJĄCY, SKŁANIAJĄCY; OBEJMUJĄCY, OTACZAJĄCY  
MUŠASQIRU – MÓWIĄCY (na rozkaz, pod przymusem),ZEZNAJĄCY; RZUCAJĄCY (klątwę); PRZYSIĘGAJĄCY (na polecenie)  
MUŠAŠNŰ, MUŠAŠNŰ – PODWAJAJĄCY  
MUŠAŠIBU(M) – KAŻĄCY SIADAĆ; OSIEDLAJĄCY, ZASIEDLAJĄCY  
MUŠAŠKINU(M),MUŠAŠKINU(M) – KAŻĄCY POŁOŻYC  
MUŠAŠPILU(M), MUŠAŠPILU(M) – NAKAZUJĄCY, POWODUJĄCY STANIE SIĘ NISKIM, PONIŻAJĄCY  
MUŠAŠQILU(M) – KAZĄCY PŁACIĆ, ŻĄDAJĄCY ZAPŁATY  
MUŠAŠQIRU – MÓWIĄCY (na rozkaz, pod przymusem),ZEZNAJĄCY; RZUCAJĄCY (klątwę); PRZYSIĘGAJĄCY (na polecenie)  
MUŠAŠQŰ – NAWADNIAJĄCY; KAZĄCY NAPOIĆ, POJĄCY  
MUŠAŠQŰ(M), MUŠAŠQŰ(M), MUŠAŠQŰ(M) – WYWYŻSZAJĄCY, AWANSUJĄCY, CZCZĄCY  
MUŠATLU(M), MUŠATLU(M) – UDZIELAJĄCY, OFIARUJĄCY, DAJĄCY, WRĘCZAJĄCY, DARUJĄCY; NAGRADZAJĄCY;  
POŻYCZAJĄCY;POWIERZAJĄCY; ODDAJĄCY NA WŁASNOŚĆ (Sarg.)  
MUŠAZKIRU – MÓWIĄCY (na rozkaz, pod przymusem),ZEZNAJĄCY; RZUCAJĄCY (klątwę); PRZYSIĘGAJĄCY (na polecenie)  
MUŠBALKITU(M:- DOPROWADZAJĄCY DO ODERWANIA;BUNTUJĄCY;PODŻEGAJĄCY;WYJMUJĄCY Z ZAWIASÓW (drzwi)  
MUŠBITU(M) – ZEZWALAJĄCY NA PRZENOCOWANIE, ORGANIZUJĄCY NOCLEG, NAKAZUJĄCY NOCLEG  
MUŠEBRŰ(M) –KAŻĄCY PATRZEĆ; POKAZUJĄCY, UKAZUJĄCY, ODSŁANIAJĄCY, UJAWNIAJĄCY; POSTANAWIAJĄCY?  
MUŠECŰM – WYPROWADZAJĄCY, ZMUSZAJĄCY DO WYJŚCIA, WYRZUCAJĄCY, WYKIDAJŁO;WYNOSZĄCY, WYWLEKA-  
JĄCY, ODCIĄGAJĄCY; WYLEWAJĄCY, ROZLEWAJĄCY (płyn);UWALNIAJĄCY, OSWOBODZICIEL; DOPRO-  
WADZAJĄCY DO UPADKU; ZDRADZAJĄCY, WYJAWIAJĄCY (tajemnicę), DONOSICIEL (?); ŚCIĄGAJĄCY,  
EGZEKWUJĄCY (daninę), POBORCA (?)  
MUŠEDU: KAŻĄCY WIEDZIEĆ, ZAWIADAMIAJĄCY, INFORMUJĄCY  
MUŠELU lub PETU – ODŹWIERNY  
MUŠELŰ(M), MUŠELŰ(M): KAZĄCY PRZYBYĆ NA GÓRĘ, KAZĄCY PODNIEŚĆ; PODNOSZĄCY; CZYNIĄCY WYSOKIM,  
PODWYŻSZAJĄCY; PODNOSZĄCY WYSOKO; (także:) ODDALAJĄCY, ZABIERAJĄCY, SPRZĄTAJĄCY, USU-  
WAJĄCY; WYDOBYWAJĄCY (zatopiony statek; KH); ZOSTAWIAJĄCY JAKO ZAŁOGĘ, GARNIZON; MIANUJĄ-  
CY? AWANSUJĄCY? PROTEKTOR?  
MUŠENIQTU – MAMKA  
MUŠERIBU(M): WPUSZCZAJĄCY, KAŻĄCY WEJŚĆ, WNIKAJĄCY, DOKONUJĄCY WTARGNIĘCIA; PRZEPUSZCZAJĄCY;  
PUSZCZAJĄCY PŁAZEM?  
MUŠERŰ – KAŻĄCY SPROWADZIĆ, KAŻĄCY PROWADZIĆ  
MUŠEŠIRU(M) – PORZĄDKUJĄCY; POKRZEPIAJĄCY; ODNOWICIEL? REFORMATOR?  
MUŠEŠQŰ – NAWADNIAJĄCY; KAZĄCY NAPOIĆ, POJĄCY  
MUŠEZIBU(M) – PRZYMUSZAJĄCY; SPORZĄDZAJĄCY (dokument); RATUJĄCY (życie)  
MUŠHARMIŢU – ROZLEWAJĄCY; KAŻĄCY SIĘ ROZPŁYNĄĆ, POWODUJĄCY ROZPŁYNIĘCIE SIĘ, ROZLANIE  
MUŠHARRIRU – STAJĄCY SIĘ SPOKOJNYM, USPAKAJAJĄCY SIĘ  
MUŠHARRIRU – DRĘTWIEJĄCY, ZAMIERAJĄCY, STAJĄCY SIĘ NIERUCHOMYM, NIERUCHOMIEJĄCY, KOSTNIEJĄCY  
MUŠITU – NOC  
MUŠKENU – PODDANY; (KH:) PÓŁWOLNY, NIEPOSIADAJĄCY ZIEMI  
MUŠK€NUM – RZUCAJĄCY SIĘ (W DÓŁ); PODDANY; (KH:) PÓŁWOLNY, NIEPOSIADAJĄCY ZIEMI  
MUŠKENUTU – PODDAŃSTWO; POŁOWICZNA WOLNOŚĆ  
MUŠLALU – NARADZAJĄCY SIĘ (z innymi), UCZESTNIK NARADY  
MUŠNAMMIRU – ROZJAŚNIAJĄCY, OŚWIETLAJĄCY \[również jako rzeczownik, a zatem można też tłumaczyć jako\]  
ŹRÓDŁO ŚWIATŁA, COŚ, CO OŚWIETLA, ROZJAŚNIA  
MUŠPALKI’U – POSZERZAJĄCY  
MUŠPALU – GŁĘBOKOŚĆ; GŁĘBIA, GŁĘBINA  
MUŠPARDI’U – ROZJAŚNIAJĄCY; ROZWESELAJĄCY  
MUŠPARRIRU – ROZPOSTARTY, ROZCIĄGNIĘTY  
MUŠPARŠIDU – ZBIEGŁY  
MUŠPAZZIRU(M) – POŻYCZAJĄCY, POŻYCZKODAWCA, WIERZYCIEL  
MUŠPELKU(M): NAWIGUJĄĆY Z PRĄDEM, PROWADZĄCY Z PRĄDEM (statek, łódź)  
MUŠQALLILU(M) – WISZĄCY; WIESZAJĄCY  
MUŠQAMMIMU(M) – STAJĄCY SIĘ ŚMIERTELNIE CICHYM, ŚMIERTELNIE CICHNĄCY  
MUŠŠAKNU(M), MUŠŠAKNU(M): SADZAJĄCY, KŁADĄCY; ROBIĄCY; ZAWIERAJĄCY (pokój); WZNIECAJĄCY (wojnę); PA-  
NUJĄCY (o głodzie, ciemności); SPRZYMIERZAJĄCY SIĘ (z ITTI)  
MUŠŠILU – OSTRZĄCY (broń)  
MUŠŠIRU(M): DOPROWADZAJĄCY DO PORZĄDKU, PORZĄDKUJĄCY; POKRZEPIAJĄCY  
MUŠŠIŠU – MĘCZĄCY, DRĘCZĄCY; zob. AŠAŠU  
MUŠŠUKU lub MUSSUKU – ŻAŁOSNY, MIZERNY, GODNY POLITOWANIA; SKURCZONY; SPĘKANY, POPĘKANY  
MUŠŠURU: POLUŹNIONY, ROZLUŹNIONY; ZWOLNIONY, WYPUSZCZONY NA WOLNOŚĆ; POŚWIĘCONY, PORZUCONY;  
ODPROWADZONY, UNIESIONY (łup); PORZUCONY, ZOSTAWIONY NA LODZIE; TEN, KTÓRY ZNIKNĄŁ, ZA-  
GINIONY  
MUŠTA’’ILU(M) – PYTAJĄCY RAZ PO RAZ, DOPYTUJĄCY SIĘ NIEUSTANNIE; PROWADZĄCY ŚLEDZTWO?  
MUŠTABBI’U(M) – ZADAWALAJĄCY SIĘ  
MUŠTABBILU – zob. ABALU (a/i); ROZWAŻAJĄCY, PRZEMYŚLIWUJĄCY, ZASTANAWIAJĄCY SIĘ, DECYDUJĄCY SIĘ, MY-  
ŚLICIEL, DECYDENT, ROZKAZODAWCA  
MUŠTABILU – zob. ABALU (a/i); KAŻĄCY PRZYNIEŚĆ, PRZESYŁAĆ, PRZEWOZIĆ; NADZORCA, ZLECENIODAWCA TRANS-  
PORTU  
MUŠTABILU(M), MUŠTABILU(M) – ROZWAŻAJACY, PRZEMYŚLIWUJĄCY; DECYDUJĄCY, POSTANAWIAJĄCY  
MUŠTACBITU(M) – POWODUJĄCY WZAJEMNE SCZEPIENIE SIĘ; POWODUJĄCY OBUSTRONNE POCHWYCENIE SIĘ  
MUŠTACŰ – WYGADUJĄCY SIĘ NOTORYCZNIE, WYPAPLUJĄCY RAZ ZA RAZEM  
MUŠTADDIRU(M) lub MUŠTADIRU(M) – SMUCĄCY SIĘ, POGRĄŻONY W SMUTKU  
MUŠTADIRU(M) lub MUŠTADDIRU(M) – SMUCĄCY SIĘ, POGRĄŻONY W SMUTKU  
MUŠTAKKINU(M), MUŠTAKKINU(M): NIEUSTANNIE, WCIĄŻ NA NOWO KŁADĄCY  
MUŠTAMHIRU(M), MUŠTAMHIRU(M): PORÓWNUJĄCY SIĘ Z, ZRÓWNUJĄCY SIĘ Z; ZRÓWNUJĄCY;CZYNIĄCY, USTA-  
NAWIAJĄCY RÓWNYM Z; USTANAWIAJĄCY, TRAKTUJĄCY JAKO ODPOWIEDNIK  
MUŠTAMMI’U – SŁUCHAJĄCY WCIĄŻ NA NOWO; PRZYSŁUCHUJĄCY SIĘ  
MUŠTAMMŰ, MUŠTAMMŰ – SŁUCHAJĄCY WCIĄŻ NA NOWO; PRZYSŁUCHUJĄCY SIĘ  
MUŠTAMRU – CZCZĄCY; SKŁADAJĄCY HOŁD; CHWALĄCY  
MUŠTANIHU – MĘCZĄCY SIĘ, WYSILAJĄCY SIĘ  
MUŠTANŰ, MUŠTANŰ – ZMIENIAJĄCY SIĘ KLIKA RAZY, PRZEOBRAŻAJĄCY SIĘ NIEJEDNOKROTNIE  
MUŠTANNU(M) – MIERZĄCY SIĘ ZE SOBĄ  
MUŠTANŰ, MUŠTANŰ – NA DŁUGO ROBIĄCY INNYM; ZMIENIAJĄCY SIĘ NA DŁUGO  
MUŠTAQRU – POROZUMIEWAJĄCY SIĘ; MÓWIĄCY, INFORMUJĄCY, POWIADAMIAJĄCY  
MUŠTARDŰ(M), MUŠTARDŰ(M) – PŁYNĄCY, CIEKNĄCY  
MUŠTARHU – PODNIOŚLE MÓWIĄCY, ZŁOTOUSTY  
MUŠTARRIHU(M) – BEZUSTANNIE PONAGLAJĄCY; zob. ARAHU(M)  
MUŠTARRIQU(M) – MAJĄCY ZWYCZAJ KRAŚĆ; OKRADAJĄCY WCIĄŻ NA NOWO; ZAWODOWY ZŁODZIEJ?  
MUŠTASHIRU(M), MUŠTASHIRU(M) – OKRĄŻAJĄCY;ZAMYKAJĄCY W OKRĄŻENIU; OBEJMUJĄCY, OGARNIAJĄCY  
MUŠTAŠNŰ, MUŠTAŠNŰ – PODWAJAJĄCY  
MUŠTEDIRU, MUŠTEDDIRU – SMUCĄCY SIĘ, OGARNIĘTY OBAWĄ  
MUŠTEPIŠTU – CZAROWNICA  
MUŠTERRIBU(M): WCHODZĄCY WCIĄŻ NA NOWO, RAZ PO RAZ  
MUŠTEŠIRU(M): KAŻĄCY BYĆ PRAWEM, PRAWODAWCA? ZAPROWADZAJĄCY ŁAD, UTRZYMUJĄCY W PORZĄDKU  
MUŠU(M) – NOC  
URRI U MUŠI – DNIEM I NOCĄ  
MUŠAM – NOCĄ  
MUTACŰ – ODCHODZĄCY, ODDALAJĄCY SIĘ  
MUTADDIRUM – BOJĄCY SIĘ BEZ PRZERWY, STALE WYSTRASZONY; PANIKARZ, TCHÓRZ  
MUTALLIKU – CHODZACY TAM I Z POWROTEM  
MUTALLIKUM – SPACERUJĄCY, SPACEROWICZ, PRZECHADZAJĄCY SIĘ  
MUTALLU(M), MUTALLU(M) – WIESZAJĄCY, OBWIESZAJĄCY, ZAWIESZAJĄCY  
MUTANU – ZARAZA, MÓR, EPIDEMIA; WIELKIE UMIERANIE; ŚMIERĆ  
MUTAMMŰ – ZAPRZYSIĘGAJĄCY, SKŁADAJĄCY PRZYSIĘGĘ  
MUTAPPILUM, MUTAPPILU – STALE, WCIĄŻ TROSZCZĄCY SIĘ, ZASPAKAJĄCY, DBAJĄCY, OPIEKUN,, DOBRO-  
CZYŃCA  
MUTAPPIRU – POZOSTAJĄCY Z DALA, ODDALAJĄCY SIĘ; WYPĘDZAJĄCY  
MUTARRIŠUM – zob. ‘ARAŠUM (a/u); (KTOŚ) SYSTEMATYCZNIE UPRAWIAJĄCY POLE, (DOŚWIADCZONY, ZATRUDNIO-  
NY NA STAŁE) PRACOWNIK ROLNY  
MUTARRU(M) – ZAWRACAJĄCY, SKŁANIAJĄCY DO POWROTU; ODPROWADZAJĄCY; ODWRACAJĄCY; OBRACAJĄCY,  
PRZEWRACAJĄCY; ZWRACAJĄCY (twarz); ZWRACAJĄCY, ODDAJĄCY, ODNOSZĄCY; ODSTAWIAJĄCY (o  
zbożu);BIORĄCY W NIEWOLĘ; NA NOWO ROBIĄCY; ODPIERAJĄCY ( wroga); REFERUJĄCY, SKŁADAJĄCY  
SPRAWOZDANIE  
MUTEDDIRU – STALE DRĘCZONY OBAWĄ, BEZ PRZERWY OGARNIĘTY STRACHEM  
MUTERRIBU(M): CZĘSTO ODWIEDZAJĄCY; STAŁY BYWALEC?  
MUTERRIŠU(M): UPRAWIAJĄCY KILKUKROTNIE,  
MUTTA’IDU – CZCIGODNY, SZANOWNY, ZNAMIENITY  
MUTTA’IDU – WIELBIĄCY, WYCHWALAJĄCY, SŁAWIĄCY  
MUTTABLU, MUTTABLU(M), MUTTABLU(M) – ZABIERAJĄCY, ODBIERAJĄCY  
MUTTADDINU(M) – DAJĄCY STALE, REGULARNIE; PŁATNIK OKRESOWO UISZCZAJĄCY NALEŻNOŚĆ?  
MUTTAGIŠU – WŁÓCZĘGA, POWSINOGA  
MUTTAHLILU – WŁAMYWACZ  
MUTTAKRU – ROZNIECAJĄCY WAŚŃ, KŁÓCĄCY, DĄŻĄCY DO PORÓŻNIENIA; INTRYGANT?  
MUTTAPLUSU(M) – SPOGLĄDAJĄCY RAZ PO RAZ, STALE PATRZĄCY; OBSERWATOR? SZPIEG?  
MUTTAQRIRU(M) – ZWIJAJĄCY SIĘ KLIKUKROTNIE  
MUTTASHIRU(M), MUTTASHIRU(M) – WIELOKROTNIE, CZĘSTO OBRACAJĄCY SIĘ; OBRACAJĄCY; KRĘCĄCY SIĘ,  
ZAKRĘCAJĄCY SIĘ; PORUSZAJĄCY SIĘ PO OKRĘGU; OKRĄŻAJĄCY, OTACZAJĄCY; TROSZCZĄCY SIĘ,  
ZAJMUJĄCY SIĘ; SZUKAJĄCY , SZUKAJĄCY; ODWIEDZA JĄCY; ZWRACAJĄCY SIĘ (do kogoś); POWTARZA  
JĄCY; ZAJMUJĄCY SIĘ CZARAMI, CZARUJĄCY;  
PONADTO  
ROBIĄCY(coś) RAZ PO RAZ, STALE  
MUTTATU – POŁOWA  
MUTTIŠ – PRZED, NA PRZEDZIE, Z PRZEDNIEJ STRONY  
MUTTU(M) – STRONA FRONTOWA, PRZÓD  
MUTU(M) – MĄŻ, MAŁŻONEK  
s.c. MUT  
MUTU(M) – ŚMIERĆ, ZGON  
MUŢABBU(M) – ULEPSZAJĄCY; UZDRAWIAJĄCY; ŁAGODZĄCY, USPAKAJAJĄCY  
MUŢABBŰ(M) – ZANURZAJĄCY, POGRĄŻAJĄCY, ZATAPIAJĄCY; ZAGŁĘBIAJĄCY, WKOPUJĄCY (w trakcie prac budowla-  
nych), WYKOPUJĄCY DÓŁ, KOPIĄCY GŁĘBOKO  
MUŢAHHŰ(M) – ZBLIŻAJĄCY SIĘ  
MUŢAHHŰ(M) – PRZYNOSZĄCY  
MUWADDŰ? – USTANAWIAJĄCY NA STAŁE  
MUWARRU(M), MUWARRU(M)? – POLECAJĄCY  
MUWAŠŠIRU(M) –POLUŹNIAJĄCY, ROZLUŹNIAJĄCY; ZWALNIAJĄCY, WYPUSZCZAJĄCY NA WOLNOŚĆ; POŚWIĘCAJĄCY,  
REZYGNUJĄCY (z czegoś); ODPROWADZAJĄCY, UNOSZĄCY (zdobycz); PORZUCAJĄCY, ZOSTAWIAJĄCY NA  
LODZIE; ZNIKAJĄCY  
MUWATTIRU(M) – CZYNIĄCY KOLOSALNYM  
MUZTAKRU – POROZUMIEWAJĄCY SIĘ; MÓWIĄCY, INFORMUJĄCY, POWIADAMIAJĄCY  
MUZZAZU – \[składnik zwrotu\]  
MUZZAZ EKALLIM – MIESZKANIEC PAŁACU (dosł. NALEŻĄCY DO PAŁACU)

N

NA’ADU (a/u) (n’d) – BYĆ WYSOKIM; UROSNĄĆ; TRZYMAĆ WYSOKO, PODNIEŚĆ; BYĆ WYWYŻSZONYM; BYĆ  
ROZSŁAWIONYM, CHWALONYM, WYNOSZONYM PO NIEBIOSA; CHWALIĆ, ROZSŁAWIAĆ, WYWYŻ-  
SZAĆ (za pomocą słów); WIELBIĆ; DZIĘKOWAĆ  
\[jako rzeczownik\] BYCIE WYSOKIM; UROŚNIĘCIE; TRZYMANIE WYSOKO, PODNIESIENIE; BYCIE WY-  
WYŻSZONYM; BYCIE ROZSŁAWIONYM, CHWALONYM, WYNOSZONYM POD NIEBIOSA; CHWALENIE,  
ROZSŁAWIANIE, WYWYŻSZANIE; WIELBIENIE; DZIĘKOWANIE  
GT NIT’UDU: WIELBIĆ, WYCHWALAĆ, SŁAWIĆ (boga – ILU)  
\[jako rzeczownik\] WIELBIENIE, WYCHWALANIE, SŁAWIENIE  
D NU’’UDU: WYWYŻSZAĆ, CHWALIĆ, ODDAWAĆ CZEŚĆ  
\[jako rzeczownik\] WYWYŻSZENIE, CHWALENIE, ODDAWANIE CZCI  
NA’ADU(M), NA’ADU(M) (i) – PILNOWAĆ, UWAŻAĆ (na coś), ZWRACAĆ UWAGĘ (na coś) \[także St\]; BYĆ PO-  
SŁUSZNYM, ODNOSIĆ SIĘ Z SZACUNKIEM, RESPEKTEM (do kogoś)  
\[jako rzeczownik\] PILNOWANIE, UWAŻANIE (na coś), ZWRACANIE UWAGI; BYCIE POSŁUSZNYM, OD-  
NOSZENIE SIĘ Z SZACUNKIEM, RESPEKTEM  
D NU’’UDU(M), NU’’UDU(M): ZWRACAĆ UWAGĘ, ZAWIADAMIAĆ, DONOSIĆ  
\[jako rzeczownik\] ZWRACANIE UWAGI, ZAWIADAMIANIE, DONOSZENIE, INFORMOWANIE  
Š ŠUN’UDU(M), ŠUN’UDU(M): ZAWIADAMIAĆ, MELDOWAĆ  
\[jako rzeczownik\] ZAWIADAMIANIE, MELDOWANIE  
NA’ALU – LEŻEĆ; zob. formy pochodne ITULU lub UTULU  
\[jako rzeczownik\] LEŻENIE  
NA’DU – WYSOKI; WYROSŁY; TRZYMANY WYSOKO, PODNOSZONY; WYWYŻSZONY, ROZSŁAWIONY, POCHWALONY,  
WYNIESIONY PO NIEBIOSA, WIELBIONY  
NA’DU(M), NA’DU(M) – PILNOWANY, OBSERWOWANY; POSŁUSZNY, PEŁEN SZACUNKU, RESPEKTU  
NA’ICU – TRAKTUJĄCY LEKCEWAŻĄCO  
NA’IDU – WYŻSZY; WZNIESIONY; SZANOWANY  
NA’IDU – TRZYMAJĄCY WYSOKO, PODNOSZĄCY; CHWALĄCY, ROZSŁAWIAJĄCY, WYWYŻSZAJĄCY (słowem);WIELBIĄCY,  
DZIĘKUJĄCY  
NA’IDU(M), NA’IDU(M) – PILNUJĄCY, UWAŻAJĄCY (na coś), ZWRACAJĄCY UWAGĘ (na coś)  
NA’IDU(M) – CHWALĄCY, WYCHWALAJĄCY, SŁAWIĄCY  
NA’IHU(M) – WYPOCZYWAJĄCY  
NA’ILU – LEŻĄCY  
NA’IQU – KRZYCZĄCY  
NA’IRU – ZABIJAJĄCY  
NA’IŠU – CHWIEJĄCY SIĘ  
NABAHU(M) – SZCZEKAĆ  
\[jako rzeczownik\] SZCZEKANIE  
NABALKATTANU – BUNTOWNIK, REBELIANT  
NABALKATTU – KIEROWNIK, PRZYWÓDCA  
NABALKUTU(M) (blkt) – N: OBRACAĆ SIĘ; OPIERAĆ SIĘ, BUNTOWAĆ SIĘ; ODPADAĆ; PRZEKROCZYĆ, PRZEJŚĆ  
(góry); ODBIEC (z powodu czegoś)  
\[jako rzeczownik\] OBRACANIE SIĘ; OPIERANIE SIĘ, BUNTOWANIE SIĘ; ODPADANIE; PRZEKROCZENIE,  
PRZEJŚCIE (gór); ODBIEGNIĘCIE  
Š ŠUBALKUTU(M): DOPROWADZAĆ DO ODERWANIA; BUNTOWAĆ; PODŻEGAĆ; WYJMOWAĆ Z ZA-  
WIASÓW (drzwi)  
\[jako rzeczownik\] DOPROWADZENIE DO ODERWANIA; BUNTOWANIE; PODŻEGANIE; WYJMOWANIE Z  
ZAWIASÓW  
ITTI x NABALKUTU – ODPADAĆ OD x  
INA QATE x NABALKUTU – DOPROWADZAĆ OD ODERWANIA, ODPADNIĘCIA OD x  
ELI x NABALKUTU – PODŻEGAĆ PRZECIWKO x  
NABALKUTU(M) – OBRÓCONY; ZBUNTOWANY; ODPADNIĘTY; PRZEKROCZONY (o górach);ODBIEGŁY (z powodu czegoś)  
NABALŢUT’U (blt’) – N: UCIEKAĆ; ZABŁĄDZIĆ, ZEJŚĆ Z DROGI  
\[jako rzeczownik\] UCIEKANIE; BŁĄDZENIE, ZEJŚCIE Z DROGI  
NABALŢUT’U – ZBIEGŁY; ZABŁĄKANY, ZAGUBIONY (w drodze)  
NABALU (a/u) – BURZYĆ, NISZCZYĆ  
\[jako rzeczownik\] BURZENIE, NISZCZENIE  
NABAŢU (i; u) – BŁYSZCZEĆ, LŚNIĆ  
\[jako rzeczownik\] BŁYSZCZENIE, LŚNIENIE  
NABBILU lub NAPPILU – TARAN  
NABBULUM, NABBULU(M) (?) – BYĆ PRZYNIESIONYM; BYCIE PRZYNIESIONYM \[bezok. N od BABALU(M)\]  
NABBULUM, NABBULU(M) (?) – \[jako przymiotnik odcz.\] PRZYNIESIONY  
NABHU(M) – OBSZCZEKANY  
NABIHU(M) – SZCZEKAJĄCY  
NABILU – BURZĄCY, NISZCZĄCY  
NABIŢU – BŁYSZCZĄCY, LŚNIĄCY  
NABLU – ZBURZONY, ZNISZCZONY  
NABLU – PŁOMIEŃ  
NABNITU – DZIEŁO, TWÓR, PŁÓD  
NABTUQU(M) – PRZERYWAĆ, PRZECINAĆ; PSUĆ SIĘ; zob. BATAQU(M)  
\[jako rzeczownik\] PRZERYWANIE, PRZECINANIE; PSUCIE SIĘ  
NABTUQU(M) – PRZERWANY, PRZECINANY, PRZECIĘTY; ZEPSUTY  
NABŢU – NABŁYSZCZONY, DOPROWADZONY DO LŚNIENIA, WYPOLEROWANY?  
NABŰ (i) – NADAWAĆ NAZWĘ, NAZYWAĆ, MIANOWAĆ, ZWAĆ  
\[jako rzeczownik\] NADAWANIE NAZWY, NAZYWANIE, MIANOWANIE, ZWANIE  
NABŰ – NAZWANY, MIANOWANY, ZWANY  
NABŰ – NAZYWAJĄCY, MIANUJĄCY  
\[NABŰ\] – zob. NUBBU  
NABURRU – GZYMS, BLANK  
NABUTU(M) (i) (’bt) – N: UCIEKAĆ \[N\]  
\[jako rzeczownik\] UCIEKANIE  
NACARU (a/u) – STRZEC, CHRONIĆ, STRÓŻOWAĆ, PILNOWAĆ, OCHRONIĆ, ZABEZPIECZYĆ; PRZECHOWYWAĆ;  
STAĆ NA WARCIE; SPRAWOWAĆ PIECZĘ, UTRZYMYWAĆ PORZĄDEK; POWAŻAĆ, SZANOWAĆ (boskie  
imię, przysięgę)  
\[jako rzeczownik\] STRZEŻENIE, CHRONIENIE, STRÓŻOWANIE, PILNOWANIE, ZABEZPIECZENIE; PRZE  
CHOWYWANIE; STANIE NA WARCIE; SPRAWOWANIE PIECZY, UTRZYMYWANIE PORZĄDKU; PO-  
WAŻANIE, SZANOWANIE  
Š ŠUCCURU: STAWIAĆ NA WARCIE  
\[jako rzeczownik\] STAWIANIE NA WARCIE  
MACCARTA NACARU – TRZYMAĆ STRAŻ, STAĆ NA WARCIE  
NACBUTU(M) – BYĆ CHWYCONYM, BYĆ BRANYM, BYĆ WZIĘTYM; BYĆ W POSIADANIU; zob. CABATU(M)  
\[jako rzeczownik\] BYCIE CHWYCONYM, BYCIE WZIĘTYM, BYCIE BRANYM; ZNAJDOWANIE SIĘ W  
POSIADANIU  
NACBUTU(M) – CHWYCONY, BRANY, WZIĘTY; ZNAJDUJĄCY SIĘ W POSIADANIU  
NACIRU – STRZEŻĄCY, CHRONIĄCY, STRÓŻUJĄCY, PILNUJĄCY, ZABEZPIECZAJĄCY; PRZECHOWUJĄCY; STOJĄCY NA  
WARCIE; SPRAWUJĄCY PIECZĘ, UTRZYMUJĄCY PORZĄDEK; POWAŻAJĄCY, SZANUJĄCY  
NACMADU – ZAPRZĘG; UPRZĄŻ (Schirrzeug)  
ERBET NACMADI – POCZWÓRNY ZAPRZĘG  
NACRU – STRZEŻONY, CHRONIONY, PILNOWANY, ZABEZPIECZONY; PRZECHOWANY; POSTAWIONY NA WARCIE;  
POWAŻANY, SZANOWANY  
NÂCU(M) – TRAKTOWAĆ LEKCEWAŻĄCO  
\[jako rzeczownik\] TRAKTOWANIE LEKCEWAŻĄCO  
NACU(M) – TRAKTOWANY LEKCEWAŻĄCO  
NADANU(M) – DAWAĆ, WYDAWAĆ; OBDARZAĆ; ROZDZIELAĆ  
\[jako rzeczownik\] DAWANIE, WYDAWANIE; OBDARZANIE; ROZDZIELANIE  
GTN ITADDUNU(M) – DAWAĆ STALE, REGULARNIE, WCIĄŻ NA NOWO  
\[jako rzeczownik\] DAWANIE STALE, REGULARNIE  
N NANDUNU(M): BYĆ DANYM  
\[jako rzeczownik\] BYCIE DANYM  
3 os. Prs: INADDAN \[staroakad.\], INADDIN \[starobab.\], INAMDIN \[nowobab.\], IDDAN \[asyr.\]; Prs: IDDIN  
ANA KASPIM NADANUM – SPRZEDAWAĆ  
BIT QATE NADANUM \[średnioasyr.\] – WYNAGRODZIĆ PRZESTÓJ  
ANA IGRI NADANU – UIŚCIĆ NALEŻNOŚĆ ZA NAJEM  
ANA ŠIME x NADANUM \[średnioasyr.\] – SPRZEDAWAĆ ZA CENĘ x  
ANA UZUBBÎM NADANU – DAĆ JAKO ODPRAWĘ (byłej małżonce); (przenośnie:) DAĆ SZANSĘ, STWO-  
RZYĆ OKA ZJĘ (komuś), ZEZWALAĆ  
PANIŠU NADANU – OKAZYWAĆ ŁASKĘ  
INA x NADANU – DAĆ Z (czegoś)  
NADARU(M) (i) – BYĆ WŚCIEKŁYM, ZŁYM  
\[jako rzeczownik\] BYCIE WŚCIEKŁYM, BYCIE ZŁYM  
\[NADARU\] – zob. NANDARU  
NADINANU – SPRZEDAWCA  
NADINANUM – SPRZEDAWCA  
NADINU(M) – DAJĄCY, WYDAJĄCY; OBDARZAJĄCY; ROZDZIELAJĄCY  
NADITU(M) – NADITUM \[kapłanka nie mogąca mieć dzieci\]  
NADNU – DANY, OFIAROWANY, PRZYDZIELONY  
NADRU(M) – ZŁY, WŚCIEKŁY  
NADU – POSŁUSZNY  
NADU – BUKŁAK (na wodę)  
NADŰ(M), NADŰ(M) (i) – RZUCAĆ, ZRZUCAĆ; POŁOŻYĆ; LEŻEĆ (St); ZAWLEC, ZACIĄGNĄĆ, (przed sędziego);  
NAKREŚLIĆ, NARYSOWAĆ (także w sensie: ZAPLANOWAĆ); ZAŁOŻYĆ (miasto); ZAMIESZKAĆ; PĘDZIĆ,  
GNAĆ (bydło, zwłaszcza na pastwisko); DAĆ ZABRZMIEĆ, WYWOŁAĆ (alarm); PRZYDZIELAĆ (coś ko-  
muś); WYPOWIEDZIEĆ (formułę czarodziejską); POZOSTAWIAĆ ODŁOGIEM (pole, sad), PORZUCAĆ (mia-  
sto); PRZYKŁADAĆ (coś do kogoś – z podw. Akk.), KŁAŚĆ (kogoś w coś – podw. Akk.); WNIKNĄĆ, WEJŚĆ;  
USYPYWAĆ (zboże); MAGAZYNOWAĆ; RZUCAĆ (na pastwę); WYDAWAĆ, WYSTAWIAĆ (na nieszczę-  
ście)  
\[jako rzeczownik\] RZUCANIE, ZRZUCANIE; POŁOŻENIE; ZAWLECZENIE, ZACIĄGNIĘCIE (do sądu); NA  
KREŚLENIE, NARYSOWANIE; PLANOWANIE; ZAŁOŻENIE (miasta); ZAMIESZKANIE; PĘDZENIE, GNA-  
NIE (bydła), WYWOŁANIE, PODNIESIENIE (alarmu); PRZYDZIELANIE; WYPOWIADANIE, RZUCANIE  
(zaklęcia), POZOSTAWIANIE ODŁOGIEM, PORZUCANIE, OPUSZCZANIE (miasta); PRZYKŁADANIE;  
WKŁADANIE; WNIKANIE, WEJŚCIE; USYPYWANIE (zboża); MAGAZYNOWANIE; RZUCANIE (na pa-  
stwę); WYSTAWIANIE, WYDAWANIE (na nieszczęście)  
N NADDŰ(M), NADDU(M): BYĆ RZUCONYM; BYĆ UŁOŻONYM W STOS  
\[jako rzeczownik\] BYCIE RZUCONYM; BYCIE UŁOŻONYM W STOS  
NTN (St): BYĆ OBSIANYM; BYĆ ROZRZUCONYM W WIELU MIEJSCACH  
Š ŠUDDŰ(M), ŠUDDŰ(M): KAZAĆ RZUCIĆ (w dół); ZANIEDBAĆ  
\[jako rzeczownik\] NAKAZANIE ZRZUCENIA; ZANIEDBANIE  
ŠA LIBBIM ŠUDDŰM – SPOWODOWAĆ PORONIENIE  
INA NAPIŠTIM NADDŰM – BYĆ BLISKIM ŚMIERCI  
ELI NADŰ – RZUCAĆ DO PRZODU  
NADŰ ANA – RZUCIĆ W  
AHA NADŰ – SIEDZIEĆ Z ZAŁOŻONYMI RĘKOMA, BYĆ BEZCZYNNYM  
LETA NADŰ ANA – PRZYWIĄZYWAĆ WAGĘ DO  
QATA NADŰ ANA – PRZYKŁADAĆ RĘCE DO  
ŠIBA NADŰ – SPLEŚNIEĆ  
NADŰ(M), NADŰ(M) (i) – RZUCONY, ZRZUCONY; POŁOŻONY; ZAWLECZONY, ZACIĄGNIĘTY (przed sędziego ); NAKRE-  
ŚLONY, NARYSOWANY (także w sensie: ZAPLANOWANY); ZAŁOŻONY (miasto); ZAMIESZKAŁY; PĘDZONY,  
GNANY (o bydle); WYWOŁANY (alarm);PRZYDZIELONY (komuś); WYPOWIEDZIANY (o formule czarodziejskiej);  
POZOSTAWIONY ODŁOGIEM (pole, sad), PORZUCONY (o mieście); PRZYŁOŻONY (do kogoś),WŁOŻONY (w  
coś); (TEN),KTÓRY WNIKNĄŁ, WSZEDŁ; USYPANY (o zbożu); ZMAGAZYNOWANY; RZUCONY (na pastwę);  
WYDANY, WYSTAWIONY (na nieszczęście)  
NADŰ(M), NADŰ(M) (i) – RZUCAJĄCY, ZRZUCAJĄCY; KŁADĄCY; WLOKĄCY, CIĄGNIĄCY (przed sędziego ); NAKRE-  
ŚLAJĄCY, RYSUJĄCY (także w sensie: PLANUJĄCY); ZAKŁADAJĄCY (miasto);ZAMIESZKUJĄCY; PĘDZĄCY,  
GNAJĄCY (bydło); WYWOŁUJĄCY, PODNOSZĄCY (alarm);PRZYDZIELAJĄCY (komuś); WYPOWIADAJĄCY  
(formułę czarodziejską);POZOSTAWIAJĄCY ODŁOGIEM (pole, sad), PORZUCAJĄCY (miasto); PRZYKŁADAJĄCY  
(do kogoś),WKŁADAJĄCY (w coś);WNIKAJĄCY, WCHODZĄCY; USYPUJĄCY (o zbożu); MAGAZYNUJĄCY;  
RZUCAJĄCY (na pastwę);WYDAJĄCY, WYSTAWIAJĄCY (na nieszczęście)  
NÂDU(M) (n’d) – CHWALIĆ, WYCHWALAĆ, SŁAWIĆ  
\[jako rzeczownik\] CHWALENIE, WYCHWALANIE, SŁAWIENIE  
DT: BYĆ POCHWALONYM  
\[jako rzeczownik\] BYCIE POCHWALONYM  
NADU(M) – CHWALONY, WYCHWALANY, SŁAWIONY  
NAGALTŰ (glt’) – PRZEBUDZIĆ SIĘ, OBUDZIĆ SIĘ  
\[jako rzeczownik\] PRZEBUDZENIE SIĘ, OBUDZENIE SIĘ  
NAGALTŰ – PRZEBUDZONY, OBUDZONY  
NAGAŠU(M) (i) – IŚĆ BEZ CELU, IŚĆ NA OŚLEP  
\[jako rzeczownik\] DĄŻENIE BEZ CELU, BŁĄDZENIE NA OŚLEP  
GTN ITAGGUŠU(M): BŁĄKAĆ SIĘ BEZ PRZERWY, BEZUSTANNIE, WCIĄŻ NA NOWO  
\[jako rzeczownik\] BEZUSTANNE BŁĄKANIE SIĘ, WIECZNE TUŁANIE SIĘ  
NAGGARU lub NANGARU – STOLARZ  
NAGIRU – HEROLD; NAGIRU \[tytuł pewnego generała elamickiego\]  
NAGIR EKALLI – HEROLD PAŁACOWY  
NAGIŠU(M) -PODĄŻAJĄCY BEZ CELU, IDĄCY NA OŚLEP; TUŁACZ?  
NAGLABU – BIODRO  
NAGŰ – PROWINCJA; OKOLICA  
NAHALLU – RWĄCY POTOK  
NAHALLULU – PRZECISKAĆ SIĘ  
\[jako rzeczownik\] PRZECISKANIE SIĘ  
NAHASU(M) (i) – USTĘPOWAĆ, COFAĆ SIĘ  
\[jako rzeczownik\] USTĘPOWANIE, COFANIE SIĘ  
NAHISU(M) – USTĘPUJĄCY, COFAJĄCY SIĘ  
NAHLAPTU – RYNSZTUNEK  
NAHLAPTU – TKANINA  
NAHLU – STRUMIEŃ, POTOK; KORYTO POTOKU  
NAHRARU(M) – POMOC  
NAHU (a/u) – BYĆ CICHYM, BYĆ SPOKOJNYM  
\[jako rzeczownik\] BYCIE CICHYM, BYCIE SPOKOJNYM  
NAHU – CICHY, SPOKOJNY  
NÂHU(M) – ODPOCZYWAĆ  
\[jako rzeczownik\] ODPOCZYWANIE  
NAHU(M) – WYPOCZĘTY  
NAKALU (i) – WYKONYWAĆ KUNSZTOWNIE, MISTRZOWSKO WYRABIAĆ  
\[jako rzeczownik\] KUNSZTOWNE WYKONYWANIE, MISTRZOWSKIE WYRABIANIE  
NAKAMTU – ZBIÓR, STERTA, KUPA  
NAKAMU (i) – GROMADZIĆ, ZEBRAĆ, NAZBIERAĆ  
\[jako rzeczownik\] GROMADZENIE, ZEBRANIE, NAZBIERANIE  
NAKAPU (i) – BÓŚĆ (o bydle)  
\[jako rzeczownik\] BODZENIE, UDERZANIE ROGAMI  
NAKARU (i) – (nieprzech.) BYĆ INACZEJ, ZMIENIAĆ SIĘ; BYĆ WROGIM, OBCYM; (przech.) ODPRAWIAĆ Z NICZYM,  
NIE PRZYJMOWAĆ; WYPIERAĆ SIĘ, ZAPRZECZAĆ, NIE PRZYZNAWAĆ SIĘ; ZAPRZECZAĆ, ODMAWIAĆ  
(komuś czegoś – podw. Akk.)  
\[jako rzeczownik\] BYCIE INACZEJ, ZMIENIANIE SIĘ; BYCIE WROGIM, BYCIE OBCYM; ODPRAWIANIE Z  
NICZYM, NIEPRZYJMOWANIE; WYPIERANIE SIĘ, ZAPRZECZANIE, NIEPRZYZNAWANIE SIĘ; ZA-  
PRZECZANIE, ODMAWIANIE  
GT ITAKRU: PORÓŻNIĆ SIĘ, POWAŚNIĆ, POGNIEWAĆ SIĘ NA SIEBIE  
\[jako rzeczownik\] PORÓŻNIENIE SIĘ, ZWAŚNIENIE SIĘ, POGNIEWANIE SIĘ NA SIEBIE  
D NUKKURU: ZAPROWADZIĆ NA INNE MIEJSCE; SPRZĄTAĆ, USUWAĆ; PRZESUWAĆ, PRZEMIESZ  
CZAĆ; ZMIENIAĆ (ubranie), PRZEBIERAĆ SIĘ  
\[jako rzeczownik\] ZAPROWADZENIE NA INNE MIEJSCE; SPRZĄTANIE, USUWANIE; PRZESUWANIE,  
PRZEMIESZCZANIE; ZMIENIANIE (ubrania), PRZEBIERANIE SIĘ  
Š: ŠUKKURU, ŠUMKURU  
ISSIŠU ŠUKKURU – ROBIĆ MU WROGA (z kogoś)  
NAKARU ITTI – ODPADAĆ OD  
NAKARU lub NAKIRU, NAKRU – WROGI; WRÓG  
NAKASU (i) – ODCINAĆ, ODKRAWAĆ; PRZECINAĆ, PRZEKRAWAĆ  
\[jako rzeczownik\] ODCINANIE, ODKRAWANIE; PRZECINANIE, PRZEKRAWANIE  
NAKILU – KUNSZTOWNIE WYKONUJĄCY, WYRABIAJĄCY MISTRZOWSKO; MISTRZ?  
NAKIMU – GROMADZĄCY, ZBIERAJĄCY  
NAKIPU – BODĄCY (o bydle)  
NAKIRU lub NAKARU, NAKRU – WROGI; WRÓG  
NAKIRU – ZMIENIAJĄCY SIĘ,; ODPRAWIAJĄCY Z NICZYM, NIEPRZYJMUJĄCY; WYPIERAJĄCY SIĘ, ZAPRZECZAJĄCY,  
NIEPRZYZNAJĄCY SIĘ; ZAPRZECZAJĄCY, ODMAWIAJĄCY  
NAKISU – ODCINAJĄCY, ODKRAWAJĄCY; PRZECINAJĄCY, PRZEKRAWAJĄCY  
NAKKAPTU – (określona) CZĘŚĆ CZOŁA  
NAKKAPŰ – BODĄCY  
NAKLU – KUNSZTOWNY, MISTRZOWSKI  
NAKMITU – SKRĘPOWANIE, SPĘTANIE, ZWIĄZANIE  
NAKMU – ZGROMADZONY, ZEBRANY; NAPEŁNIONY; UZUPEŁNIONY  
NAKNUKU(M): ZAPIECZĘTOWANY, OPIECZĘTOWANY  
NAKPU – UDERZONY ROGAMI  
NAKRU – INNY, ZMIENIONY; ODPRAWIONY Z NICZYM, NIEPRZYJĘTY; ZAPRZECZONY, ODMÓWIONY  
NAKRU lub NAKIRU, NAKARU – WROGI; WRÓG  
NAKRU(M) – WRÓG; WROGI  
NAKSU – ODCIĘTY, ODKROJONY; PRZECIĘTY, PRZEKROJONY  
NALBUBU – STAĆ SIĘ DZIKIM, ZDZICZEĆ  
\[jako rzeczownik\] ZDZICZENIE, STANIE SIĘ DZIKIM  
NAMADDU – KOCHANEK  
NAMARITU – OSTATNIA WARTA NOCNA; CZAS OSTATNIEJ NOCNEJ WARTY  
NAMARU lub NAWARU(M) (i) – ŚWIECIĆ; BYĆ BŁYSZCZĄCYM, LŚNIĄCYM, JASNYM; JAŚNIEĆ, STAWAĆ SIĘ JA-  
SNYM (o świcie); ROZJAŚNIAĆ SIĘ, ROZCHMURZYĆ (o rysach twarzy)  
\[jako rzeczownik\] ŚWIECENIE; BYCIE BŁYSZCZĄCYM, LŚNIĄCYM, JASNYM; JAŚNIENIE; ŚWITANIE;  
ROZJAŚNIANIE SIĘ, ROZCHMURZANIE SIĘ (o twarzy)  
D NUMMURU: (przenośnie:) ZAPALAĆ  
\[jako rzeczownik\] ROZPALANIE  
NAMAŠU (u) – WYŁAMAĆ  
\[jako rzeczownik\] WYŁAMANIE  
NAMCARU – MIECZ  
NAMHARU – ? \[jakieś naczynie\]  
NAMHUCU(M), NAMHUCU(M): BITY  
NAMIRTU – BLASK, POŁYSK  
NAMIRTU lub NAMRU – LŚNIĄCY, BŁYSZCZĄCY  
NAMIRU – ŚWIECĄCY, JAŚNIEJĄCY, ŚWITAJĄCY; ROZJAŚNIAJĄCY SIĘ, ROZCHMURZAJĄCY SIĘ (o twarzy)  
NAMISU – WYŁAMUJĄCY  
NAMKARU(M) – KANAŁ NAWADNIAJĄCY  
NAMKURU – POSIADANIE, WŁASNOŚĆ  
NAMQARTU – RÓW NAWADNIAJĄCY  
NAMRA’U(M) – BYDŁO TUCZNE; TUCZNY  
NAMRACIŠ – Z WIELKIM WYSIŁKIEM, Z WIELKĄ TRUDNOŚCIĄ  
NAMRACU – OBCIĄŻENIE; UCIĄŻLIWOŚĆ  
NAMRIRRU – LATARNIE, LAMPY; \[Lipin\] BLASK; JASKRAWY, PRZEJMUJĄCY BLASK  
NAMRU lub NAMIRTU – LŚNIĄCY, BŁYSZCZĄCY; \[Lipin\] JASNY  
NAMSU – WYŁAMANY  
NAMSŰ – MIEJSCE DO MYCIA  
NAMTARU – NAJGORSZE ZŁO; DŻUMA \[sum.\]  
\[NAMŰ\] lub\[NAWŰ\] (nwj) – N NAMMŰ: BYĆ SPUSTOSZONYM, ZNISZCZONYM; ROZPADAĆ SIĘ, DOJŚĆ DO UPADKU  
\[jako rzeczownik\] BYCIE SPUSTOSZONYM, ZNISZCZONYM; ROZPADANIE SIĘ, DOJŚCIE DO UPADKU  
D NUMMŰ: PUSTOSZYĆ  
\[jako rzeczownik\] PUSTOSZENIE  
NAMŰ lub NAWŰ – PASTWISKO; TEREN PASTWISKA  
NAMURRATU – BŁYSZCZENIE, ŚWIECENIE SIĘ  
NANDARU – N: WPADAĆ W ZŁOŚĆ, ROZZŁOŚCIĆ SIĘ  
\[jako rzeczownik\] WPADANIE W ZŁOŚĆ, ROZZŁOSZCZENIE SIĘ  
NANDUNU(M) – DANY  
NANDURU – OBEJMOWANIE SIĘ (WZAJEMNE); zob. ADARU  
NANGARU lub NAGGARU – STOLARZ  
NANMURTU(M) – SPOTYKANIE SIĘ; SPOTKANIE  
NANMURUM – \[bezok. N\] WIDZENIE SIĘ NAWZAJEM, SPOTYKANIE SIĘ ZE SOBĄ; ZBLIŻANIE SIĘ DO  
ZIEMI (o skale); WSCHODZENIE (o ciałach niebieskich); zob. AMARU(M)  
NANMURUM – WIDZIANY (ZE WZAJEMNOŚCIĄ), SPOTKANY; ZBLIŻONY DO ZIEMI (o skale); WZESZŁY (o ciele  
niebieskim)  
NANTALLŰ(M) – CIEMNOŚĆ  
NAPACU (a/u) – POWALIĆ  
\[jako rzeczownik\] POWALANIE  
NAPAHU(M) (a/u) – (przech.) ZAPALAĆ; (nieprzech.) WSCHODZIĆ \[astronom.\]  
\[jako rzeczownik\] ZAPALANIE; WSCHODZENIE (ciał niebieskich)  
N NANPUHU(M): BYĆ ZAPALONYM; WYBUCHNĄĆ (o ogniu)  
\[jako rzeczownik\] BYCIE ZAPALONYM; WYBUCHANIE (o ogniu)  
NTN ITAPPUHU(M): WCIĄŻ NA NOWO BYĆ ZAPALANYM  
\[jako rzeczownik\] REGULARNE ROZPALANIE  
NAPARDU’U – JASNY, ROZJAŚNIONY;WESOŁY, ROZWESELONY; POGODNY, ROZPOGODZONY  
NAPARKŰ(M) (prku) – N: PRZESTAWAĆ, USTAWAĆ, SKOŃCZYĆ  
\[jako rzeczownik\] PRZESTAWANIE, USTAWANIE, KOŃCZENIE  
NAPARKU(M) – ZAPRZESTANY, ZAKOŃCZONY  
NAPARQUDU(M) (prqd) – KŁAŚĆ SIĘ NA PLECACH, LEŻEĆ NA PLECACH  
\[jako rzeczownik\] POŁOŻENIE SIĘ NA PLECACH, LEŻENIE NA PLECACH  
NAPARQUDU(M) – POŁOŻONY NA PLECACH  
NAPARRURU(M) – ROZBIEGAĆ SIĘ  
\[jako rzeczownik\] ROZBIEGANIE SIĘ  
NAPARŠUDU lub ŠUPARŠUDU (pršd) – UCIEKAĆ  
\[jako rzeczownik\] UCIEKANIE  
NAPAŠU (u) – ROZSZERZAĆ SIĘ; ROZPOŚCIERAĆ SIĘ, ZASNUĆ (o mgle); BYĆ OBFITYM (o ilości wody), BYĆ RZĘSI-  
STYM; DĄĆ, WIAĆ, SZUMIEĆ  
\[jako rzeczownik\] ROZSZERZANIE SIĘ, ROZPOŚCIERANIE SIĘ, ZASNUWANIE (o mgle); BYCIE OBFITYM  
(o ilości wody), BYCIE RZĘSISTYM; DĘĆIE, WIANIE, SZUMIENIE  
D NUPPUŠU: DAĆ ODETCHNĄĆ  
\[jako rzeczownik\] POZWOLENIE NA ODETCHNIĘCIE  
NAPCU – POWALONY  
NAPHAR – SUMA; CAŁOKSZTAŁT  
NAPHARU(M) – CAŁOŚĆ, OGÓŁ  
NAPHAR – RAZEM, OGÓŁEM; WSPÓLNIE, RAZEM Z  
NAPHU(M) – ZAPALONY; WZESZŁY (o ciele niebieskim)  
NAPICU – POWALAJĄCY  
NAPIHU(M) – ZAPALAJĄCY; WSCHODZĄCY (o obiektach astronomicznych)  
NAPIŠTU(M), NAPIŠTU(M) – ŻYCIE; ŻYJĄCA ISTOTA; DUSZA; OSOBA; TCHNIENIE  
s.c. NAPIŠTI, NAPŠAT  
BALAŢ NAPŠATI \[nowobab.\] – ZDROWIE  
INA NAPIŠTIM NADDŰM – BYĆ BLISKIM ŚMIERCI (?)  
DIN NAPIŠTI – PROCES NA ŚMIERĆ I ŻYCIE  
ŠIKNAT NAPIŠTI – ŻYWE STWORZENIE  
NAPIŠU – ROZSZERZAJĄCY SIĘ, ROZPOŚCIERAJĄCY SIĘ, ZASNUWAJĄCY (o mgle); DMĄCY, WIEJĄCY, SZUMIĄCY  
NAPLASTU(M) – (LEWY) PŁAT WĄTROBY  
NAPLASU(M) – SPOGLĄDANIE, PATRZENIE; \[Lipin\] PATRZEĆ  
NAPLASUŠŠA – (POD) JEJ SPOGLĄDANIEM = GDZIE SPOGLĄDA  
NAPLUSU(M) (a/i) – N: PATRZEĆ  
\[jako rzeczownik\] PATRZENIE  
NTN ITTAPLUSU(M): PATRZEĆ RAZ PO RAZ, STALE DOGLĄDAĆ  
\[jako rzeczownik\] PATRZENIE RAZ PO RAZ, STAŁE SPOGLĄDANIE  
NAPPAHTU – MIECH  
NAPPAŠU – LUFCIK, OKIENKO  
NAPPILU lub NABBILU – TARAN  
NAPPULUM, NAPPULU – zob. APALU(M); DOSTARCZONY, DOSTARCZONY JAKO ZAOPATRZENIE  
NAPRUSU – WSTRZYMANY; WYCOFANY (w sensie: ten, który ustąpił)  
NAPSAMU – KAGANIEC  
NAPŠU – ROZSZERZONY, ROZPOSTARTY, ZASNUTY (mgłą); OBFITY, RZĘSISTY (o wodzie); OWIANY, ZAWIANY, NAWIANY  
NAPTANU – UROCZYSTY POSIŁEK, UCZTA  
NAPTU’U(M) – OTWARTY  
NAPŢARU(M) – PRZYJACIEL  
NAQARU(M), NAQARU(M) (a/u) – NISZCZYĆ, BURZYĆ, ROZWALAĆ; ROZESCHNĄĆ SIĘ, ROZLECIEĆ (KH: o statku)  
\[jako rzeczownik\] NISZCZENIE, BURZENIE, ROZWALANIE; ROZSYCHANIE SIĘ, ROZLATYWANIE SIĘ  
N NANQURU(M), NANQURU(M): BYĆ NISZCZONYM  
\[jako rzeczownik\] BYCIE NISZCZONYM  
NAQBU – ŹRÓDŁO; (szczególnie we wróżbach) (każdy) NATURALNY REZERWUAR WODY  
NAQIDU – PASTERZ \[sum.\]  
NAQIRU(M), NAQIRU(M) – NISZCZĄCY, BURZĄCY, ROZWALAJĄCY; ROZSYCHAJĄCY SIĘ, ROZLATUJĄCY SIĘ  
NAQMŰTU – POŻAR  
NAQRU(M), NAQRU(M) – NISZCZONY, ZBURZONY, ROZWALONY; ROZESCHNIĘTY, ROZLAZŁY  
NAQRURU(M) – ZGIĘTY (samoczynnie?)  
NÂQU – KRZYCZEĆ  
\[jako rzeczownik\] KRZYCZENIE  
NAQU – WYKRZYCZANY  
NAQŰ(M) (nqi) – OFIAROWYWAĆ, SKŁADAĆ OFIARĘ; WYLEWAĆ, ROZLEWAĆ  
\[jako rzeczownik\] OFIAROWYWANIE, SKŁADANIE OFIARY; WYLEWANIE, ROZLEWANIE  
GTN NITAQQŰ(M): OFIAROWYWAĆ WCIĄŻ NA NOWO, REGULARNIE SKŁADAĆ OFIARY  
\[jako rzeczownik\] CIĄGŁE OFIAROWYWANIE, REGULARNE SKŁADANIE OFIARY  
NARAM-SIN – NARAMSIN \[imię króla z dynastii akadyjskiej\]  
NARAMTU(M) – KOCHANKA  
NARAMU(M) – KOCHANEK, UKOCHANY; ULUBIENIEC  
NARARU – POMOCNIK  
NARARUTU – POMOC, WSPARCIE  
NARBŰ – WIELKOŚĆ  
NARKABTU – RYDWAN; ODDZIAŁ WOZÓW BOJOWYCH  
NARKUSU – ELEMENT ZBIOROWOŚCI; CZŁONEK?  
NARRUBU – ZBIEGŁY, TEN, KTÓRY UCIEKŁ  
NARTU – ŚPIEWACZKA \[sum.\]  
NARŢABU – ŻURAW \[do nawadniania\]  
NARU lub NERU (a) (a/e) – ZABIJAĆ  
\[jako rzeczownik\] ZABIJANIE  
NARU – ZABITY  
NARU – STELA; POMNIK  
NARU(M) – RZEKA; POTOK; KANAŁ  
NARU(M), NARU(M) – ŚPIEWAK \[sum.\]  
NARUBU – N: UCIEKAĆ \[N\]  
\[jako rzeczownik\] UCIEKANIE  
NARUQQUM – WOREK  
NASAHU(M) (a/u) – WYRYWAĆ, ODRYWAĆ, ZRYWAĆ; WYKORZENIĆ; DEPORTOWAĆ, ZSYŁAĆ; (KH:) WYPĘDZAĆ,  
WYDZIEDZICZAĆ  
\[jako rzeczownik\] WYRYWANIE, ODRYWANIE, ZRYWANIE; WYKORZENIENIE; DEPORTOWANIE,  
ZSYŁANIE; WYPĘDZANIE, WYDZIEDZICZANIE  
N NANSUHU(M): BYĆ ODERWANYM  
Š ŠUSSUHU(M): KAZAĆ PRZESTAWIĆ \[staroasyr.\]  
INA APLUTI NASAHU – WYDZIEDZICZAĆ Z OJCOWIZNY  
IŠDI NASAHUM \[staroakad.\] – WYRYWAĆ Z KORZENIAMI  
NASAKU (u) – KŁAŚĆ, POŁOŻYĆ; RZUCIĆ PŁASKO; RZUCIĆ  
\[jako rzeczownik\] UKŁADANIE; RZUCANIE PŁASKO; RZUCANIE  
NASAQU (a/u) – WYBIERAĆ, PRZEBIERAĆ  
\[jako rzeczownik\] WYBIERANIE, PRZEBIERANIE  
INA x NASAQU – WYBIERAĆ Z x  
NASHU(M) – WYRWANY, ODERWANY, ZERWANY; WYKORZENIONY; DEPORTOWANY, ZESŁANY;WYDZIEDZICZONY  
NASHURU(M), NASHURU(M) – OBRÓCONY, PRZEKRĘCONY  
NASIHU(M) – WYRYWAJĄCY, ODRYWAJĄCY, ZRYWAJĄCY; WYKORZENIAJĄCY; DEPORTUJĄCY, ZSYŁAJĄCY; WYDZIE-  
DZICZAJĄCY  
NASIKKU lub NASIKU – SZEJK KOCZOWNIKÓW \[zachodniosemickie\]  
NASIKU lub NASIKKU – j.w.  
NASIKU – KŁADĄCY, UKŁADAJĄCY; RZUCAJĄCY PŁASKO; RZUCAJĄCY  
NASIQU – WYBIERAJĄCY, PRZEBIERAJĄCY  
NASKU – POŁOŻONY, UŁOŻONY; RZUCONY PŁASKO; RZUCONY  
NASPANTU – DEWASTACJA  
NASPUHU(M) – ROZPROSZONY; ROZWIĄZANY; ZNISZCZONY  
NASQU – WYŚMIENITY, WYBORNY, ZNAKOMITY  
NASQU – WYBRANY, PRZEBRANY  
NAŠAKU (a/u) – GRYŹĆ, KĄSAĆ  
\[jako rzeczownik\] GRYZIENIE, KĄSANIE  
\[NAŠAPU\] – zob. NUŠŠUPU  
NAŠAQU (i) – CAŁOWAĆ  
\[jako rzeczownik\] CAŁOWANIE  
N NANŠUQU: CAŁOWAĆ \[poet.\]  
\[jako rzeczownik\] CAŁOWANIE  
-NÂŠI – zob. –IA  
NAŠIKU – GRYZĄCY, KĄSAJĄCY  
NAŠIQU – CAŁUJĄCY  
NAŠKU – POGRYZIONY, POKĄSANY  
NAŠKUNU(M), NAŠKUNU(M): POSADZONY, POŁOŻONY; ZROBIONY; ZAWARTY (o pokoju); WZNIECONY (o wojnie); ZA-  
PADŁY (o głodzie, ciemności); SPRZYMIERZONY (z ITTI)  
NAŠPAKU(M) – SPICHLERZ \[r.ż.\]  
l.mn. NAŠPAKATUM  
NAŠPAKUTU – NAGROMADZENIE, ZMAGAZYNOWANIE  
NAŠPARTU – WIEŚĆ, WIADOMOŚĆ  
NAŠQU – WYCAŁOWANY, POCAŁOWANY  
NAŠQULU(M) – ZAWIESZONY (na czymś)  
NAŠRU – ORZEŁ  
NAŠU – \[składnik zwrotu\]  
NAŠI BILTI – PODATNIK; PODLEGAJĄCY OPODATKOWANIU  
NAŠ PAŢRI – WOJOWNIK POSŁUGUJĄCY SIĘ MIECZEM  
-NÂŠU – zob. IA  
NAŠU (a/u) – CHWIAĆ SIĘ  
\[jako rzeczownik\] CHWIANIE SIĘ  
NAŠU – ZACHWIANY, WYTRĄCONY Z RÓWNOWAGI  
NAŠŰ(M) (nši) – PODNOSIĆ; NIEŚĆ; (Vent.) PRZYNIEŚĆ; UTRZYMYWAĆ (KH: żonę); PODDAĆ SIĘ (karze), PRZYJĄĆ  
NA SIEBIE (karę); \[Lipin\] PODNOSIĆ, BRAĆ  
\[jako rzeczownik\] PODNOSZENIE, NIESIENIE; PRZYNIESIENIE; UTRZYMYWANIE (np. żony), PODDANIE  
SIĘ (karze); BRANIE, WZIĘCIE  
ANA ILIM NAŠŰ – POŚWIĘCAĆ BOGU  
INAM NAŠŰ ANA – KIEROWAĆ SWOJĄ UWAGĘ NA; POŻĄDAĆ  
NAŠŰ(M) – PODNIESIONY, NIESIONY, PRZYNIESIONY; UTRZYMYWANY; WZIĘTY, BRANY  
NAŠŰ(M) – PODNOSZĄCY, NIOSĄCY, PRZYNOSZĄCY; UTRZYMUJĄCY; BIORĄCY  
NATAKU (u) – ŚCIEKAĆ, SPŁYWAĆ KROPLAMI, KROPIĆ, KAPAĆ  
\[jako rzeczownik\] ŚCIEKANIE, SPŁYWANIE KROPLAMI, KROPIENIE, KAPANIE  
\[NATALU\] (i) – LEŻEĆ, SPAĆ; por. NA’ALU  
NATBAKU – \[składnik zwrotu\]  
NATBAK ŠAD€ – POTOK GÓRSKI  
-NÂTI – zob. –IA  
NATIKU – ŚCIEKAJĄCY, SPŁYWAJĄCY KROPLAMI, KROPIĄCY, KAPIĄCY  
NATKU – OPADŁY W POSTACI KROPEL  
NATRUKU – ROZBITY  
NAŢALU(M), NAŢALU(M) (a/u) – (z Akk. lub ANA) PATRZEĆ, SPOGLĄDAĆ; BYĆ SKIEROWANYM  
\[jako rzeczownik\] PATRZENIE, SPOGLĄDANIE; BYCIE SKIEROWANYM  
GT ITAŢLU(M): SPOGLĄDAĆ NAWZAJEM NA SIEBIE; ODWRACAĆ SPOJRZENIE  
\[jako rzeczownik\] SPOGLĄDANIE NAWZAJEM NA SIEBIE; ODWRACANIE SPOJRZENIA  
NAŢILU(M), NAŢILU(M) – PATRZĄCY, SPOGLĄDAJĄCY  
NAŢLU(M), NAŢLU(M) – OGLĄDANY; SKIEROWANY  
NAŢŰ – ODPOWIEDNI, WŁAŚCIWY, STOSOWNY  
NAŢŰ(M) (ntu) – BYĆ NADAJĄCYM SIĘ, BYĆ ODPOWIEDNIM \[St\]  
\[jako rzeczownik\] BYCIE NADAJĄCYM SIĘ, BYCIE ODPOWIEDNIM  
NAWARU(M) (i) lub NAMARU – ŚWIECIĆ; BYĆ BŁYSZCZĄCYM, LŚNIĄCYM; JAŚNIEĆ, STAWAĆ SIĘ JASNYM (o  
świcie); ROZJAŚNIAĆ SIĘ, ROZCHMURZYĆ (o rysach twarzy)  
\[jako rzeczownik\] ŚWIECENIE; BYCIE BŁYSZCZĄCYM, LŚNIĄCYM, JASNYM; JAŚNIENIE; ŚWITANIE;  
ROZJAŚNIANIE SIĘ, ROZCHMURZANIE SIĘ (o twarzy)  
D NUMMURU: (przenośnie:) ZAPALAĆ  
\[jako rzeczownik\] ROZPALANIE  
\[NAWŰ\] lub \[NAMŰ\] (nwj) – N: BYĆ SPUSTOSZONYM, ZNISZCZONYM; ROZPADAĆ SIĘ, DOJŚĆ DO UPADKU  
\[jako rzeczownik\] BYCIE SPUSTOSZONYM, ZNISZCZONYM; ROZPADANIE SIĘ, DOJŚCIE DO UPADKU  
D NUMMŰ: PUSTOSZYĆ  
\[jako rzeczownik\] PUSTOSZENIE  
NAWŰ lub NAMŰ – PASTWISKO; TEREN PASTWISKA  
NAWŰ(M) – OBÓZ NAMIOTOWY  
NAZAQU (i) – GNIEWAĆ SIĘ, ZŁOŚCIĆ  
\[jako rzeczownik\] GNIEWANIE SIĘ, ZŁOSZCZENIE SIĘ  
NAZARU(M) (a/u) – PRZEKLINAĆ, ZŁORZECZYĆ  
\[jako rzeczownik\] PRZEKLINANIE, ZŁORZECZENIE  
NAZAZU(M), NAZAZU(M) lub IZUZZU, UZUZZU, UŠUZZU – STAĆ; DEPTAĆ; POMAGAĆ; ZAJMOWAĆ SIĘ (czymś)  
\[jako rzeczownik\] STANIE; DEPTANIE; POMAGANIE; ZAJMOWANIE SIĘ (czymś)  
GT: IZUZZU: DOJŚĆ DO BEZRUCHU, ZAMRZEĆ (o przypływie?); STAWAĆ, STAWIĆ SIĘ (przed kimś)  
\[jako rzeczownik\] DOJŚCIE DO BEZRUCHU, ZATRZYMANIE SIĘ (o przypływie?); STAWANIE, STAWIANIE  
SIĘ  
D: UZUZZU  
Š: UŠUZZU: POSTAWIĆ; KAZAĆ DEPTAĆ  
\[jako rzeczownik\] POSTAWIENIE; SPOWODOWANIE DEPTANIA  
ELI x IZUZZUM – TRIUMFOWAĆ NAD x, POKONYWAĆ x  
MAZZAZ TAZZAZZU – MIEJSCE, NA KTÓRYM STOISZ  
Odmiana 3 os.l.p.  
G: Prs: IZZAZ, IZZAZZU (tzw. pSt) GT: Prs: ITTAZZAZ  
Prt: IZZIZ Š: Prs: UŠZAZ  
Pf: ITTAZIZ Prt: UŠZIZ  
TR: IZIZ TR: ŠUZIZ  
NAZIQU – GNIEWAJĄCY SIĘ, ZŁOSZCZĄCY SIĘ  
NAZIRU(M) – PRZEKLINAJĄCY, ZŁORZECZĄCY  
NAZQU – ROZGNIEWANY, ROZZŁOSZCZONY  
NAZRU(M) – PRZEKLĘTY, OBRZUCONY ZŁORZECZENIAMI  
NAZZAZU(M) – MIEJSCE; POZYCJA  
-NE – zob. –IA  
NE’IŠU(M) – PRZYWRACAJACY DO ŻYCIA, WSKRZESZAJĄCY  
NE’U (e) – OBRACAĆ, ODWRACAĆ  
\[jako rzeczownik\] OBRACANIE, ODWRACANIE  
NEBERTU lub NEBIRU, NEBERU, NEBIRTU, NIPIRU – MIEJSCE PRZEPRAWY, BRÓD  
ŠA NEBIRTI, ŠA NEBERTI – PO TAMTEJ STRONIE  
NEBERU lub NEBIRU, NEBERTU, NEBIRTU, NIPIRU – j.w.  
NEBIRTU lub NEBIRU, NEBERU, NEBIRTU, NIPIRU – j.w.  
NEBIRU lub NEBERU, NEBERTU, NEBIRTU, NIPIRU – j.w.  
NEHELCŰ(M) (hlci) – N: POŚLIZNĄĆ SIĘ  
\[jako rzeczownik\] ŚLIZGANIE, WPADNIĘCIE W POŚLIZG  
NEHESU (i) – COFAĆ SIĘ, USTĘPOWAĆ  
\[jako rzeczownik\] COFANIE SIĘ, USTĘPOWANIE  
NEHISU – COFAJĄCY SIĘ, USTĘPUJĄCY  
NEHSU – WYCOFANY, ZMUSZONY DO USTĄPIENIA  
NEHU – CICHY, SPOKOJNY  
NEKURTU(M) – WROGOŚĆ  
NEMEDU – \[składnik zwrotu\]  
KUSSI NEMEDI – FOTEL  
NEMEL – PONIEWAŻ, NA SKUTEK TEGO, ŻE  
NEMELU(M) – KORZYŚĆ, POŻYTEK  
NEMEQU – MĄDROŚĆ, WIEDZA  
NENU – zob. ANAKU  
NEPELKŰ(M) (plki) – N: BYĆ SZEROKIM, BYĆ OBSZERNYM, BYĆ ROZCIĄGNIĘTYM  
\[jako rzeczownik\] BYCIE SZEROKIM, BYCIE OBSZERNYM, BYCIE ROZCIĄGNIĘTYM  
NEPELKŰ(M) – SZEROKI, OBSZERNY, ROZCIĄGNIĘTY  
NEPIŠTU – (pewnego rodzaju) BUDOWA  
NEPŰ(M) (npi) – BRAĆ JAKO ZASTAW (rzecz), WTRĄCIĆ W NIEWOLĘ ZA DŁUGI (osobę); UPROWADZIĆ  
\[jako rzeczownik\] BRANIE W ZASTAW (rzeczy), WTRĄCANIE W NIEWOLĘ ZA DŁUGI; UPROWADZANIE  
NEPŰ(M) – ZABRANY JAKO ZASTAW, WZIĘTY W ZASTAW, WTRĄCONY W NIEWOLĘ ZA DŁUGI; UPROWADZONY  
NEPŰ(M) – BIORĄCY W ZASTAW, WTRĄCAJĄCY W NIEWOLĘ ZA DŁUGI; UPROWADZAJĄCY  
NEPŰTU – SYSTEM ZASTAWNY  
NEQELPŰ(M) (qlpu) – N: PŁYNĄĆ Z PRĄDEM, UNOSIĆ SIĘ Z PRĄDEM (nieprzech.)  
\[jako rzeczownik\] PŁYNIĘCIE Z PRĄDEM, UNOSZENIE SIĘ Z PRĄDEM  
Š ŠUPELKU(M): NAWIGOWAĆ Z PRĄDEM, PROWADZIĆ Z PRĄDEM (statek, łódź)  
\[jako rzeczownik\] NAWIGOWANIE Z PRĄDEM, STEROWANIE Z PRĄDEM, PROWADZENIE Z PRĄDEM  
NEQELPŰ(M) – UNIESIONY Z PRĄDEM, PORWANY Z PRĄDEM  
NER – SZEŚĆSET  
ŠINA NER – TYSIĄC DWIEŚCIE  
HAMŠAT NER – TRZY TYSIĄCE  
NEREBU – PRZEŁĘCZ, PRZESMYK; DOSTĘP, DOJŚCIE  
NERGALU – DUŻY LEW  
NERTU – MORDERSTWO, MORD  
NERU lub NARU (a) (a/e) – ZABIJAĆ  
\[jako rzeczownik\] ZABIJANIE  
N€RU(M) (n’r) – ZABIJAĆ  
\[jako rzeczownik\] ZABIJANIE  
NERU(M) – ZABITY  
NEŠ lub NIŠ – POD, POD PRZYSIĘGĄ  
NEŠU lub NIŠU – PRZYSIĘGA  
NEŠŰ (i) – BYĆ DALEKIM; ODDALAĆ SIĘ  
\[jako rzeczownik\] BYCIE DALEKIM; ODDALANIE SIĘ  
NEŠŰ – DALEKI, ODLEGŁY  
NEŠU(M) – LEW  
NEŠU(M) (n’š) – BYĆ ŻYWYM, ŻYĆ  
\[jako rzeczownik\] BYCIE ŻYWYM  
D NUŠU(M): PRZYWRACAĆ DO ŻYCIA  
\[jako rzeczownik\] PRZYWRACANIE DO ŻYCIA  
NEŠU(M) – ŻYWY  
-NI – zob. –IA  
-NI – \[po dołączeniu do orzeczenia czasownikowego zdania werbalnego lub do orzecznika zdania nominalnego wyraża podrzęd-  
ność; tzw. subiunctivus\]  
NIA’TUM lub NUTTUM – NASZE; zob. JU(M)  
NIA’UM lub NUM – NASZE; zob. JU(M)  
NIAŠIM – zob. ANAKU  
NIÂTI – zob. ANAKU  
-NIATI – zob. -IA  
NÎBÎTU (nb’) – NAZWA, IMIĘ  
s.c. NIBIT  
NIBÎT ŠUMI – WYMIENIENIE IMIENIA; IMIĘ  
NIBITU – POWOŁANIE, MIANOWANIE; POWOŁUJĄCY, MIANUJĄCY  
NIBITA NABŰ – NAZYWAĆ, NADAWAĆ IMIĘ  
NIBRETU – GŁÓD  
NIBŢU – POŁYSK, BLASK  
NIBU – NAZWANIE, NADANIE IMIENIA  
LA NIBI lub ŠA NIBA LA IŠŰ – NIEZLICZONY, BEZ LIKU  
NICIRTU – TAJEMNICA; SKARB  
NIDITU lub NIDŰTU – UPADEK (o ludziach); ZAMIENIENIE W UGÓR, UGOROWANIE (o polu)  
NIDŰTU, NIDŰTU lub NIDITU – j.w.  
NIGICCU – WĄWÓZ; JASKINIA; ROZPADLINA; SZCZELINA, SZPARA; UKRYTY ZAKAMAREK  
l.mn. NIGICCÂTE  
NIGUTU – MUZYKA  
NIKASU – LICZENIE, RACHOWANIE; SUMA, WYNIK  
NIKILTU – SUBTELNOŚĆ, DELIKATNOŚĆ; CHYTROŚĆ, PRZEBIEGŁOŚĆ; MISTRZOWSKIE, KUNSZTOWNE DZIEŁO  
s.c. NIKLAT  
NIKKASSU – OBLICZENIE, ROZRACHUNEK \[sum.\]  
NIKNAKKU lub NIQNAQQU, NIQNAQU, NIŢNAQU – WONNE DREWNO LUB INNA ROŚLINA NADAJĄCA SIĘ DO  
OFIARNEGO KADZENIA; KADZIELNICA; PANEW KADZIDŁA; PRZYRZĄD DO WĘDZENIA \[sum.\]  
NIKPU(M) – UDERZENIE  
NIKSU – ODCINANIE, CIĘCIE, KRAJANIE; RZEŹ, MASAKRA  
-NIM – zob. -A(M)  
NINDABŰ – OFIARA Z POŻYWIENIA \[sum.\]  
NIN-LIL – NINLIL \[imię bogini\]  
NINSIANNA – WENUS \[planeta utożsamiana z boginią Isztar\]  
NINU – zob. ANAKU  
NINURTA – NINURTA \[imię boga\]  
NIPILTU – DODATEK, DOPŁATA  
NIPIRU lub NEBERTU, NEBIRU, NEBERU, NEBIRTU – MIEJSCE PRZEPRAWY, BRÓD  
ŠA NEBIRTI, ŠA NEBERTI – PO TAMTEJ STRONIE  
NIPŠU – (MIŁY, PRZYJEMNY) ZAPACH, WOŃ  
NIQNAQQU lub NIKNAKKU, NIQNAQU, NIŢNAQU – WONNE DREWNO LUB INNA ROŚLINA NADAJĄCA SIĘ DO  
OFIARNEGO KADZENIA; KADZIELNICA; PANEW KADZIDŁA; PRZYRZĄD DO WĘDZENIA \[sum.\]  
NIQNAQU lub NIKNAKKU, NIQNAQQU, NIŢNAQU – j.w.  
NIQŰ(M) – OFIARA; \[Lipin\] LIBACJA; OFIARA  
NIRARUTU – POMOC  
NIRU(M) – JARZMO; „JARZMO” \[część wątroby\]  
NISHATU – \[składnik zwrotu\]  
CAB NISHATI – DEPORTOWANY  
NISIQTU – KLEJNOT; KOSZTOWNOŚCI  
NISMATU – CEL, ŻYCZENIE  
NIŠ lub NEŠ – POD, POD PRZYSIĘGĄ  
NIŠAKKU – KAPŁAN  
NIŠKU(M) – UKĄSZENIE  
NÎŠU – PODNIESIENIE; KLĄTWA; MODLITWA  
s.c. NÎŠ  
NÎŠ QATI – PODNIESIENIE RĄK (przy modlitwie lub klątwie); MODLITWA; KLĄTWA; BŁAGANIE  
NÎŠ ILI – KLĄTWA  
NÎŠ ILÂNI UTTAMMEŠUNU – ZMUSIŁEM ICH, BY WYRZEKLI KLĄTWĘ BOSKĄ  
NÎŠ €N€ – PODNIESIENIE OCZU, SPOJRZENIE; MOMENTALNIE  
NIŠU NIŠ QATI – „PODNIESIENIE RĄK” \[rodzaj modlitwy\]  
NIŠU – LUDZIE \[r.ż.\] \[l.mn.\]  
NIŠU lub NEŠU – PRZYSIĘGA  
NIŠU lub ŠUMU, ZIKRU – IMIĘ  
NIŠUTU – POKREWIEŃSTWO; RODZINA, KREWNI  
NÎTU – OBRÓT; ZAWRÓCENIE  
NÎTA LAMŰ, NÎTA LAMI – NIEODWOŁALNIE OKRĄŻYĆ, OTOCZYĆ SZCZELNYM KRĘGIEM, Z KTÓRE-  
GO NIE MOŻNA SIĘ WYRWAĆ  
NIŢLU – SPOJRZENIE, RZUT OKA  
s.c. NIŢIL  
NIŢNAQU lub NIKNAKKU, NIQNAQU, NIQNAQQU – WONNE DREWNO LUB INNA ROŚLINA NADAJĄCA SIĘ DO  
OFIARNEGO KADZENIA; KADZIELNICA; PANEW KADZIDŁA; PRZYRZĄD DO WĘDZENIA \[sum.\]  
-NU – zob. –IA  
NŰ(M) – zob. JU(M)  
NU’’UDU: WYWYŻSZONY, CHWALONY, OBDARZONY CZCIĄ  
NU’’UDU(M), NU’’UDU(M): ZAWIADOMIONY, DONIESIONY  
NUBATTU – NOCNY SPOCZYNEK; CISZA NOCNA; PRZEDDZIEŃ, WIGILIA  
NUBBŰ – D: SKARŻYĆ SIĘ, ŻALIĆ, NARZEKAĆ, BIADAĆ  
\[jako rzeczownik\] SKARŻENIE SIĘ, ŻALENIE SIĘ, NARZEKANIE, BIADANIE  
NUBBŰ – WYCIE, SKOWYT  
NUBTU – PSZCZOŁA  
NUDUNNŰ(M) – POSAG  
NUHATIMMU – SZEF KUCHNI \[sum.\], KUCHARZ  
NUHŠU – OBFITOŚĆ; \[Lipin\] OBFITOŚĆ, PULCHNOŚĆ  
NUKARIBBU(M) – OGRODNIK \[sum.\]  
NUKARRIBU lub LAKURUPPU – ROLNIK; OGRODNIK  
NUKKURU – ZMIENIAĆ; zob. NAKARU  
NUKKURU: ZAPROWADZONY NA INNE MIEJSCE; SPRZĄTNIĘTY, USUNIĘTY; PRZESUNIĘTY, PRZEMIESZCZONY;  
ZMIENIONY (ubranie), PRZEBRANY  
NUKURTU – WROGOŚĆ  
NUM lub NIA’UM – NASZE; zob. JU(M)  
NUMATU – SPRZĘT (DOMOWY)  
NUMMŰ – SPUSTOSZONY  
NUMMURU: (przenośnie:) ROZPALONY  
NUNPUHU(M) – ZAPALONY, ROZGORZAŁY (o ogniu)  
NUNU(M), NUNU(M) – RYBA  
NUPPUŠU – (KTOŚ), KOMU POZWOLONO ODETCHNĄĆ  
NURU(M) – ŚWIATŁO  
NUŠU(M) – PRZYWRÓCONY DO ŻYCIA, ZMARTWYCHWSTAŁY  
NUŠŠUPU – D: WYMIATAĆ PRECZ (wegwehen)  
\[jako rzeczownik\] WYMIATANIE PRECZ  
NUŠŠUPU – WYMIECIONY PRECZ  
NUŠURRŰ – STRATA, OBNIŻENIE WARTOŚCI  
NUTTUM lub NIA’TUM – NASZE; zob. JU(M)

P  
PADALLUM -\[staroas.\] PRZEDMIOT MIEDZIANY, \[zapożyczenie z hetyckiego od słowa oznaczającego kajdany, obręcze na nogi?\]  
PADANU(M) – ŚCIEŻKA; „ŚCIEŻKA” \[cecha wątroby\]  
PADŰ (i) – CHRONIĆ, OCHRANIAĆ  
\[jako rzeczownik\] CHRONIENIE, OCHRANIANIE  
PADŰ – CHRONIĄCY, OCHRANIAJĄCY  
PADŰ – CHRONIONY, OCHRONIONY  
PAGRU – CIAŁO; TRUP, ZWŁOKI, CIAŁO  
PAHARU – GARNCARZ  
PAHARU(M) (u) – ZBIERAĆ SIĘ, GROMADZIĆ SIĘ  
\[jako rzeczownik\] ZBIERANIE SIĘ, GROMADZENIE SIĘ  
D PUHHURU(M): ZBIERAĆ; WZMACNIAĆ  
\[jako rzeczownik\] ZBIERANIE; WZMACNIANIE  
PAHATU – \[składnik zwrotu\]  
BEL PAHATI lub BEL PAHITI – URZĘDNIK, ZWŁASZCZA NAMIESTNIK  
PAHATU lub PIHATU – SATRAPIA, OKRĘG ADMINISTRACYJNY  
PAHATU lub ŠAKNU – NAMIESTNIK  
PAHATUTU – NAMIESTNICTWO  
PAHIRU(M) – ZBIERAJĄCY SIĘ, GROMADZĄCY SIĘ  
PAHRU(M) – ZEBRANY, ZGROMADZONY (o grupie ludzi)  
\[PAHU\] – zob. PUHHU  
PALAHU(M) (a) – BAĆ SIĘ, OBAWIAĆ SIĘ (kogoś – Akk.); BYĆ PEŁNYM SZACUNKU; CZCIĆ, WIELBIĆ; OKAZYWAĆ  
GŁĘBOKĄ CZEŚĆ (o służącym)  
\[jako rzeczownik\] BANIE SIĘ, OBAWIANIE SIĘ; BYCIE PEŁNYM SZACUNKU; CZCZENIE, WIELBIENIE;  
OKAZYWANIE SZACUNKU (przez służącego)  
GTN PITALLUHU(M): BAĆ SIĘ BEZ PRZERWY, CIĄGLE  
\[jako rzeczownik\] BANIE SIĘ BEZ PRZERWY, ŻYCIE W CIĄGŁYM STRACHU  
LAPAN x PALAHU lub ULTU PAN x PALAHU – BAĆ SIĘ x  
PALAKU (i) – BIĆ, ZARZYNAĆ (bydło, drób)  
\[jako rzeczownik\] BICIE, ZARZYNANIE  
\[PALASU\] – zob. NAPLUSU  
PALAŠU(M) (a/u) – PRZEBIJAĆ; PRZEŁAMYWAĆ SWOJĄ NIECHĘĆ  
\[jako rzeczownik\] PRZEBIJANIE; PRZEŁAMYWANIE SWOJEJ NIECHĘCI  
PALGU – KANAŁ  
PALHU(M) – PEŁEN SZACUNKU; CZCZONY, WIELBIONY; OTOCZONY SZACUNKIEM  
PALHIŠ – Z PEŁNYM SZACUNKIEM  
PALIHU(M) – BOJĄCY SIĘ, OBAWIAJĄCY SIĘ; CZCZĄCY, WIELBIĄCY; OKAZUJĄCY SZACUNEK  
PALIKU – BIJĄCY, ZARZYNAJĄCY (bydło, drób); RZEŹNIK?  
PALIŠU(M) – PRZEBIJAJĄCY; PRZEŁAMUJĄCY SWOJĄ NIECHĘĆ, PRZEKONUJĄCY SIĘ (do kogoś)  
PALKU – ZABITY, ZARŻNIĘTY (o bydle, drobiu)  
PALŠU(M) – PRZEBITY; PRZEKONANY (do kogoś), WYZUTY Z NIECHĘCI  
PALŰ – INSYGNIUM KRÓLEWSKIE; RZĄDY, PANOWANIE \[sum.\]  
PALU – ROK; CZAS RZĄDÓW  
PANATU – FRONT  
PANASSU ALAKU – PROWADZIĆ KOGOŚ  
PANI – PRZYBLIŻONY (o liczbie)  
PANI – PRZED  
PANITU(M) – WCZEŚNIEJSZA  
INA PANITIM HARRANIŠU – W JEGO WCZEŚNIEJSZEJ WYPRAWIE  
PANŰ, PANŰM – DAWNIEJSZY, WCZEŚNIEJSZY; PIERWSZY  
PANU(M), PANU(M) – PRZEDNIA STRONA, PRZÓD, PRZEDNIA CZĘŚĆ; POWIERZCHNIA (wody czy nieba)  
PANU – OBLICZE, TWARZ  
l.mn. PANU, PANI  
PANU ANA x ŠAKANUM – PRZYSTĄPIĆ DO ROBIENIA x, POSTANOWIĆ ZAJĄĆ SIĘ x  
MAZZIZ PANI \[średnioasyr.\] – DWORZANIN  
ANA PAN \[średnioasyr.\] – PRZED  
ANA PANIKA – PRZED CIEBIE  
ISSU PAN – Z, OD  
INA PAN \[nowobab., średnio- i nowoasyr.\] – PRZED, W OBLICZU  
INA PAN – PRZED  
EŠERUM ANA PANI – PODCHODZIĆ KU  
KAL PANIKA – WSZYSTKO (CO JEST) PRZED TOBĄ  
IŠTU PAN – PRZED  
PANIŠ – Z PRZODU, NA CZELE  
INA PAN ŠATTI – PRZED ROKIEM  
ANA PANIŠU – PRZED JEGO POWROTEM (KH)  
ELI ŠA PAN, ELI ŠA UM PANI – BARDZIEJ NIŻ DAWNO  
INA PANA – DAWNIEJ, NIEGDYŚ, ONGIŚ  
ULTU UM PANI – OD DAWIEN DAWNA, ZAWSZE  
PAPAHU – CELA (ŚWIĄTYNNA) \[sum.\]  
PAQADU (i) – OPIEKOWAĆ SIĘ; DOZOROWAĆ, DOGLĄDAĆ; PRZEKAZAĆ, POWIERZYĆ, ZAWIERZYĆ; WYZNA-  
CZYĆ; PORUCZYĆ; ODDAWAĆ ZGODNIE Z ZASADAMI; PRZEGLĄDAĆ, PRZELICZAĆ; UMIESZCZAĆ,  
USTAWIAĆ  
\[jako rzeczownik\] OPIEKOWANIE SIĘ; DOZOROWANIE, DOGLĄDANIE; PRZEKAZANIE, POWIERZENIE,  
ZAWIERZENIE; WYZNACZENIE; PORUCZENIE; ODDAWANIE ZGODNIE Z ZASADAMI; PRZEGLĄDA-  
NIE, PRZELICZANIE; UMIESZCZANIE, USTAWIANIE  
D PUQQUDU: UMIESZCZAĆ, USTAWIAĆ  
\[jako rzeczownik\] UMIESZCZANIE, USTAWIANIE  
PAQADU ELI – UMIESZCZAĆ NAD  
PAQDU – DOZOROWANY, DOGLĄDANY; PRZEKAZANY, POWIERZONY, ZAWIERZONY; WYZNACZONY; PORUCZONY;  
ODDANY ZGODNIE Z ZASADAMI; PRZEJRZANY, PRZELICZONY; UMIESZCZONY, USTAWIONY  
PAQIDU – OPIEKUJĄCY SIĘ; DOZORUJĄCY, DOGLĄDAJĄCY; PRZEKAZUJĄCY, POWIERZAJĄCY, ZAWIERZAJĄCY;  
WYZNACZAJĄCY; PORUCZAJĄCY; ODDAJĄCY ZGODNIE Z ZASADAMI; PRZEGLĄDAJĄCY, PRZELICZAJĄCY;  
UMIESZCZAJĄCY, USTAWIAJĄCY  
PARA’U (a/u) – ODCINAĆ, OBCINAĆ, PRZECINAĆ  
\[jako rzeczownik\] ODCINANIE, OBCINANIE, PRZECINANIE  
PAR’U – ODCIĘTY, OBCIĘTY, PRZECIĘTY  
\[PARACU\] – DT PUTARRUCU: WYPEŁNIAĆ, CELEBROWAĆ (rytuały religijne) (DT)  
\[jako rzeczownik\] WYPEŁNIANIE, CELEBROWANIE  
PARADU (i) – BYĆ LĘKLIWYM, BYĆ STRACHLIWYM  
\[jako rzeczownik\] BYCIE LĘKLIWY, BYCIE STRACHLIWYM  
PARAKKU, PARAKKU – TRON; WYSOKIE MIEJSCE; PAŁAC, SALA TRONOWA; MIEJSCE NA TRONIE (boga); ŚWIĄ-  
TYNIA; MIESZKANIE, POMIESZCZENIE MIESZKALNE (w świątyni); NAJWIĘKSZA ŚWIĘTOŚĆ \[sum.\]  
l.mn. PARAKK€, PARAKKÂNI  
PARAKU (i) – ZAMYKAĆ; KŁAŚĆ SIĘ, POŁOŻYĆ SIĘ W POPRZEK, NA UKOS  
\[jako rzeczownik\] ZAMYKANIE; KŁADZENIE SIĘ W POPRZEK, NA UKOS  
\[PARARU\] – zob. PURRURU  
PARASRAB – PIĘĆ SZÓSTYCH; dosł. WIELKIE ODDZIELENIE, WIELKI DZIAŁ  
PARASU (a/u) – ROZDZIELAĆ, ODDZIELAĆ; ROZWIĄZYWAĆ; BADAĆ, ANALIZOWAĆ, ROZSTRZYGAĆ; ODŁĄCZAĆ;  
POWSTRZYMYWAĆ, HAMOWAĆ; ZAPOBIEGAĆ; ZAMYKAĆ  
\[jako rzeczownik\] ROZDZIELANIE, ODDZIELANIE; ROZWIĄZYWANIE; BADANIE, ANALIZOWANIE,  
ROZSTRZYGANIE; ODŁĄCZANIE; POWSTRZYMYWANIE, HAMOWANIE; ZAPOBIEGANIE; ZAMYKANIE  
N NAPRUSU: USTAWAĆ, BYĆ WSTRZYMANYM; USTĘPOWAĆ  
\[jako rzeczownik\] USTAWANIE, BYCIE WSTRZYMANYM; USTĘPOWANIE  
PARASU ANA – DZIELIĆ NA  
\[PARÂŠU\] – N NAPRUŠU: LECIEĆ, ODLECIEĆ (a/i)  
\[jako rzeczownik\] LATANIE, ODLATYWANIE  
Š ŠUPRUŠU: ROZCIĄGAĆ, ROZPOŚCIERAĆ; KAZAĆ LECIEĆ, KAZAĆ ODLECIEĆ  
\[jako rzeczownik\] ROZCIĄGANIE, ROZPOŚCIERANIE; SPOWODOWANIE LECENIA, ODLECENIA  
PARCU – WŁADZA BOSKA; OBRZĄDEK, OBOWIĄZEK KULTOWY, ZWYCZAJ ZWIĄZANY Z KULTEM; KULT, RY-  
TUAŁ; BOSKI NAKAZ  
PARDU – LĘKLIWY, STRACHLIWY  
PARI’U – ODCINAJĄCY, OBCINAJĄCY, PRZECINAJĄCY  
PARIKU – ZAMYKAJĄCY; KŁADĄCY SIĘ W POPRZEK, NA UKOS  
PARISU – WIOSŁO, DRĄG UŻYWANY NA STATKU, ODBIJACZ  
PARISU – ROZDZIELAJĄCY, ODDZIELAJĄCY; ROZWIĄZUJĄCY; BADAJĄCY, ANALIZUJĄCY, ROZSTRZYGAJĄCY; ODŁĄ  
CZAJĄCY;POWSTRZYMUJĄCY, HAMUJĄCY; ZAPOBIEGAJĄCY; ZAMYKAJĄCY  
PARKU – RYGIEL, ZASUWA  
PARKU – ZAMKNIĘTY; POŁOŻONY W POPRZEK, NA UKOS  
PARSIGU – OPASKA, PRZEPASKA \[sum.\]  
PARSU – NIEDOSTĘPNY (dla świeckich)  
PARSU – ROZDZIELONY, ODDZIELONY; ROZWIĄZANY; ZBADANY, ZANALIZOWANY, ROZSTRZYGNIĘTY;ODŁĄCZONY;  
POWSTRZYMANY, HAMOWANY; ZAPOBIEŻONY; ZAMKNIĘTY  
PARŠU – GNÓJ, NAWÓZ, KAŁ  
PARŰ – MUŁ  
PARUŠŠU – ŹDŹBŁO; KIJ; KIJ DO POGANIANIA OSŁA \[sum.\]  
PARUTU – ALABASTER  
PARZILLU – ŻELAZO \[słowo obcego pochodzenia\]  
PASASU (a/u) – WYMAZYWAĆ, UMARZAĆ  
\[jako rzeczownik\] WYMAZYWANIE, UMARZANIE  
PASISU – WYMAZUJĄCY, UMARZAJĄCY  
PASPASU – KURA? \[jakiś ptak domowy\]  
PASSU – WYMAZANY, UMORZONY  
PAŠAHU (a) – USPOKAJAĆ SIĘ, UDOBRUCHAĆ SIĘ  
\[jako rzeczownik\] USPOKAJANIE SIĘ, UDOBRUCHANIE SIĘ  
PAŠALU (i) – CZOŁGAĆ SIĘ, PEŁZAĆ  
\[jako rzeczownik\] CZOŁGANIE SIĘ, PEŁZANIE  
PAŠAQU(M) (a/u) – ZWĘŻAĆ, ŚCIEŚNIAĆ SIĘ  
\[jako rzeczownik\] ZWĘŻANIE, ŚCIEŚNIANIE SIĘ  
\[PAŠARU\] – zob. BUŠŠURU  
PAŠARU (a,u) – UNIEWAŻNIAĆ, ODWOŁAĆ (zwłaszcza banicję); SPRZEDAWAĆ DETALICZNIE (Amm.)  
\[jako rzeczownik\] UNIEWAŻNIANIE, ODWOŁYWANIE, AMNESTIONOWANIE; SPRZEDAWANIE DETA-  
LICZNE  
ANA KASPI PAŠARU – SPRZEDAĆ  
PAŠARU – ZWALNIAĆ, UWALNIAĆ; ODRZUCAĆ  
\[jako rzeczownik\] ZWALNIANIE, UWALNIANIE; ODRZUCANIE  
PAŠAŠU(M) (a/u) – SMAROWAĆ, POSMAROWAĆ, NAMASZCZAĆ (coś czymś – podw. Akk.)  
\[jako rzeczownik\] SMAROWANIE, POSMAROWANIE, NAMASZCZANIE  
PAŠHU – USPOKOJONY, UDOBRUCHANY  
PAŠIHU – USPOKAJAJĄCY SIĘ  
PAŠILU – CZOŁGAJĄCY SIĘ, PEŁZAJĄCY  
PAŠIQU(M) – ZWĘŻAJĄCY, ŚCIEŚNIAJĄCY SIĘ  
PAŠIRU – UNIEWAŻNIAJĄCY, ODWOŁUJĄCY, AMNESTIONUJĄCY (w przypadku banicji); SPRZEDAJĄCY DETALICZNIE  
PAŠIRU – ZWALNIAJĄCY, UWALNIAJĄCY; ODRZUCAJĄCY  
PAŠIŠU – (pewnego rodzaju) KAPŁAN  
PAŠIŠU(M) – SMARUJĄCY, NAMASZCZAJĄCY  
PAŠQU – TRUDNY, CIĘŻKI, MOZOLNY; NIEBEZPIECZNY; TRUDNO DOSTĘPNY  
PAŠQU(M) – ZWĘŻONY, ŚCIEŚNIONY  
PAŠRU – UNIEWAŻNIONY, ODWOŁANY, AMNESTIONOWANY (w przypadku banicji); SPRZEDANY DETALICZNIE  
PAŠRU – ZWALNIONY, UWALNIONY; ODRZUCONY  
PAŠŠU(M) – POSMAROWANY, NAMASZCZONY  
PAŠŠURU – STÓŁ; CZARKA  
PATA’U(M) \[asyr.\] lub PETŰ (M) (i) (pt’) – WYJAWIĆ, ZDRADZIĆ; OTWIERAĆ; WSTĄPIĆ (na nieprzetarty szlak); ZAGO-  
SPODAROWAĆ; PRZYGOTOWAĆ POD UPRAWĘ (pole)  
\[jako rzeczownik\] WYJAWIENIE, ZDRADZENIE; OTWIERANIE; WSTĄPIENIE (na nowy szlak); ZAGOSPO-  
DAROWYWANIE, PRZYGOTOWYWANIE POD UPRAWĘ (pola)  
GTN PITATTU’U(M): OTWIERAĆ RAZ PO RAZ  
\[jako rzeczownik\] OTWIERANIE RAZ PO RAZ  
N NAPTU’U(M): BYĆ OTWARTYM  
\[jako rzeczownik\] BYCIE OTWARTYM  
D PUTTU’U(M): OTWIERAĆ, ODSŁANIAĆ; CZYNIĆ WRAŻLIWYM, WYCZULONYM, UWRAŻLIWIAĆ,  
UCZULAĆ (uszy); NAKŁANIAĆ DO ROZSĄDKU; PRZEBIJAĆ, ROZPRASZAĆ (ciemność)  
\[jako rzeczownik\] OTWIERANIE, ODSŁANIANIE; CZYNIENIE WRAŻLIWYM, WYCZULONYM, UWRAŻLI-  
WIANIE, UCZULANIE (o uszach); NAKŁANIANIE DO ROZSĄDKU; PRZEBIJANIE, ROZPRASZANIE (mroku)  
M€ PETŰ – OTWORZYĆ UPUST ZAPORY WODNEJ  
PURIDA PETŰ – BIEC  
INA PIT PURIDI – W MIG, BŁYSKAWICZNIE  
KAKKE PETŰ – PRZYSPASABIAĆ BROŃ DO WALKI  
PATALU(M) – OBWIJAĆ, OWIJAĆ JEDNO WOKÓŁ DRUGIEGO, SKRĘCAĆ RAZEM \[tylko St\]  
\[jako rzeczownik\] OBWIJANIE, OWIJANIE JEDNEGO WOKÓŁ DRUGIEGO, SKRĘCANIE RAZEM  
PATILU(M) – OBWIJAJĄCY, OWIJAJĄCY JEDNO WOKÓŁ DRUGIEGO, SKRĘCAJĄCY RAZEM  
PATLU(M) – OWINIĘTY, OWINIĘTY JEDEN WOKÓŁ DRUGIEGO, SKRĘCONY RAZEM  
PAT’U(M) – WYJAWIONY, ZDRADZONY; OTWARTY; ZAGOSPODAROWANY, PRZYGOTOWANY POD UPRAWĘ  
PATI’U(M) – WYJAWIAJĄCY, ZDRADZAJĄCY; OTWIERAJĄCY; WSTĘPUJĄCY (na nowy szlak), ODKRYWCA? ZAGOSPODA-  
ROWUJĄCY, PRZYGOTOWUJĄCY POD UPRAWĘ  
PATTU – GRANICA  
PATTU – KANAŁ  
l.mn. PATTATU  
PAŢARU(M) (a/u) – ROZDZIELAĆ, ODSZCZEPIAĆ; USUWAĆ (coś); ZMAZAĆ, WYBACZYĆ (grzech); KUPOWAĆ, WY-  
KUPYWAĆ; POWŚCIĄGAĆ (gniew); ROZWIĄZAĆ (węzeł); ZWALNIAĆ; ANULOWAĆ, UMORZYĆ (dług)  
\[jako rzeczownik\] ROZDZIELANIE, ODSZCZEPIANIE; USUWANIE; ZMAZYWANIE, WYBACZANIE (grze-  
chu); KUPOWANIE, WYKUPYWANIE; POWŚCIĄGANIE (gniewu); ROZWIĄZYWANIE (węzła); ZWALNIA-  
NIE; ANULOWANIE, UMARZANIE (długu)  
D: ROZDZIELAĆ; WYKUPYWAĆ  
\[jako rzeczownik\] ROZDZIELANIE; WYKUPOWANIE  
PAŢIRU(M) – ROZDZIELAJĄCY, ODSZCZEPIAJĄCY; USUWAJĄCY; ZMAZUJĄCY, WYBACZAJĄCYY (o grzechu); KUPUJĄ-  
CY, WYKUPUJĄCY;POWŚCIĄGAJĄCY (o gniewie); ROZWIĄZUJĄCY (o węźle); ANULUJĄCY, UMARZAJĄCY (o  
długu)  
PAŢRU – NÓŻ, SZTYLET; \[Lipin\] MIECZ  
NAŠ PAŢRI – WOJOWNIK POSŁUGUJĄCY SIĘ MIECZEM  
PAŢRU(M) – ROZDZIELONY, ODSZCZEPIONY; USUNIĘTY; ZMAZANY, WYBACZONY (o grzechu); KUPIONY, WYKUPIONY;  
POWŚCIĄGNIĘTY (o gniewie); ROZWIĄZANY (o węźle); ANULOWANY, UMORZONY (o długu)  
PAŢU(M), PATU(M) – POGRANICZE, GRANICA; SKRAJ; OBSZAR, TEREN (gminy); zob. PATTU  
PAŢ GIMRI – WSZYSTEK, CAŁY  
\[PAZARU(M)\] – Š-D ŠUPAZZURU(M): POŻYCZYĆ  
\[jako rzeczownik\] POŻYCZANIE  
\[PAZARU\] – zob. PUZZURU  
PE’TU lub PITTU, PETTU, PENTU – ŻARZĄCY SIĘ POPIÓŁ  
PECŰ (i) – BYĆ BIAŁYM; SPLEŚNIEĆ  
\[jako rzeczownik\] BYCIE BIAŁYM; PLEŚNIENIE  
PECŰ – BIAŁY  
PECŰ – BIAŁY; SPLEŚNIAŁY  
PECŰ – PLEŚNIEJĄCY  
PEHŰ (i) – USZCZELNIAĆ (statek); ZAMYKAĆ (bramę)  
\[jako rzeczownik\] USZCZELNIANIE (statku); ZAMYKANIE (bramy)  
PEHŰ – USZCZELNIONY (statek); ZAMKNIĘTY (o bramie)  
PEHŰ – USZCZELNIAJĄCY (statek); ZAMYKAJĄCY (bramę)  
PELU (e) lub BELU(M) (b’l) – PANOWAĆ, BYĆ PANEM  
\[jako rzeczownik\] PANOWANIE, BYCIE PANEM  
PELU – PAN  
PELŰ lub PILŰ – CZERWONAWY  
PENTU – WĘGIEL; zob. PITTU  
PENTU lub PITTU, PETTU, PE’TU – ŻARZĄCY SIĘ POPIÓŁ  
PETTU lub PITTU, PE’TU, PENTU – j.w.  
PETU lub MUŠELU – ODŹWIERNY  
PETŰ(M), PETŰ(M) (i) (pt’) lub PATA’U(M) – WYJAWIĆ, ZDRADZIĆ; OTWIERAĆ; WSTĄPIĆ (na nieprzetarty szlak); ZA-  
GOSPODAROWAĆ; PRZYGOTOWAĆ POD UPRAWĘ (pole)  
\[jako rzeczownik\] WYJAWIENIE, ZDRADZENIE; OTWIERANIE; WSTĄPIENIE (na nowy szlak); ZAGOSPO-  
DAROWYWANIE, PRZYGOTOWYWANIE POD UPRAWĘ (pola)  
GTN PITATTŰ(M), PITATTŰ(M): OTWIERAĆ RAZ PO RAZ  
\[jako rzeczownik\] OTWIERANIE RAZ PO RAZ  
N NAPTŰ(M), NAPTŰ(M): BYĆ OTWARTYM  
\[jako rzeczownik\] BYCIE OTWARTYM  
D PUTTŰ(M), PUTTŰ(M): OTWIERAĆ, ODSŁANIAĆ; CZYNIĆ WRAŻLIWYM, WYCZULONYM, UWRAŻ-  
LIWIAĆ, UCZULAĆ (uszy); NAKŁANIAĆ DO ROZSĄDKU; PRZEBIJAĆ, ROZPRASZAĆ (ciemność)  
\[jako rzeczownik\] OTWIERANIE, ODSŁANIANIE; CZYNIENIE WRAŻLIWYM, WYCZULONYM, UWRAŻLI-  
WIANIE, UCZULANIE (o uszach); NAKŁANIANIE DO ROZSĄDKU; PRZEBIJANIE, ROZPRASZANIE (mroku)  
M€ PETŰ – OTWORZYĆ UPUST ZAPORY WODNEJ  
PURIDA PETŰ – BIEC  
INA PIT PURIDI – W MIG, BŁYSKAWICZNIE  
KAKKE PETŰ – PRZYSPASABIAĆ BROŃ DO WALKI  
PETŰ(M), PETŰ(M) – WYJAWIONY, ZDRADZONY; OTWARTY; ZAGOSPODAROWANY, PRZYGOTOWANY POD UPRAWĘ  
PETŰ(M), PETŰ(M) – WYJAWIAJĄCY, ZDRADZAJĄCY; OTWIERAJĄCY; WSTĘPUJĄCY (na nowy szlak), ODKRYWCA?  
ZAGOSPODAROWUJĄCY, PRZYGOTOWUJĄCY POD UPRAWĘ  
PICU – BIAŁY  
PIGU – GROŹBA, POGRÓŻKA (?)  
PIHATU – OBOWIĄZEK ZASTĘPSTWA  
PIHATU lub PAHATU – SATRAPIA, OKRĘG ADMINISTRACYJNY  
PILAQQU – TOPÓR  
PILLURTU(M) – KRZYŻ  
PILŠU(M) – WYŁOM; RÓW, FOSA; ZAGŁĘBIENIE, DZIURA; KOPALNIA; KORYTARZ PODZIEMNY DRĄŻONY PRZEZ OBLEGAJĄCYCH TWIERDZĘ  
PILU – WAPIEŃ  
PILŰ lub PELŰ – CZERWONAWY  
PIQAT – MOŻE  
PIQITTU – INSPEKCJA, LUSTRACJA  
s.c. PIQITTI  
PIQU – CIASNY  
PIR’U lub PÎRU – PĘD, KIEŁEK, ODROŚL; POTOMEK, POTOMSTWO  
PIRISTU lub PIRIŠTU(M) – TAJEMNICA  
PIRIŠTU(M) lub PIRISTU – j.w.  
PÎRU lub PIR’U – PĘD, KIEŁEK, ODROŚL; POTOMEK, POTOMSTWO  
PIRU – SŁOŃ  
PIŠIRTU – ODWOŁANIE (BANICJI)  
PIŠŠATU – OLEJ DO NAMASZCZANIA; \[Lipin\] NAMASZCZANIE; MAŚĆ  
PITHALLU – KAWALERZYSTA; KAWALERIA  
SISE PITHALLI – WIERZCHOWIEC  
PITILTU(M) – SZNUR, POWRÓZ  
PITLUHU – STRASZNY, OKROPNY  
PITNU – KASETA; PITNU \[najprostszy instrument strunowy\]  
PITQUDU – ROZWAŻNY, PRZEMYŚLANY, ROZTROPNY  
PITTU lub PETTU, PE’TU, PENTU – ŻARZĄCY SIĘ POPIÓŁ  
PITU – OTWÓR  
PITU – ODŹWIERNY  
PIŢRU(M) – SZCZELINA, SZPARA  
PŰ – PLEWA; PASZA DLA BYDŁA  
l.mn. PU-E  
PŰ lub PŰ’U – SŁOMA, PLEWY, SIECZKA  
PU(M), PU(M) – USTA; PASZCZA, PASZCZĘKA; PYSK; DZIÓB; GARDŁO; WEJŚCIE; CZELUŚĆ; MATKA (u ludzi i zwie-  
rząt); OSTRZE, KLINGA; GŁOS; SŁOWO; SENTENCJA; MOWA; NAKAZ, ROZKAZ; JĘZYK; BRZMIENIE;  
TREŚĆ (dokumentu), TEKST (dosłowny); UJŚCIE (rzeki)  
l.mn. PIÂTI, PÂTI  
ANA PI ŢUPPIM (KANIKIM) – WEDŁUG BRZMIENIA DOKUMENTU  
ŠA LA PIJA \[nowobab.\] – WBREW MOJEMU ROZKAZOWI  
PŰ ŠAKÂNU – DOJŚĆ DO POROZUMIENIA, ZAWRZEĆ UMOWĘ  
KÎ PÎ – ZGODNIE, ODPOWIEDNIO  
ANA PI – ODPOWIEDNIO  
INA PÎ NAŠKUNU – WYRAŻAĆ SIĘ, WYPOWIADAĆ SIĘ; (dosł.) WŁOŻYĆ (W CZYJEŚ) USTA MOWĘ,  
POLECIĆ POWIEDZIEĆ  
ANA IŠTEN PÎ TURRU – JEDNOCZYĆ DZIĘKI ZAWARCIU UMOWY; SKŁONIĆ DO ZAWARCIA POROZU-  
MIENIA  
CIT PÎ – SENTENCJA  
PÂ IŠTEN ŠUŠKUNU – (dosł.) CZYNIĆ JEDNĄ MYŚLĄ, UJARZMIAĆ  
PŰ’U lub PŰ – SŁOMA, PLEWY, SIECZKA  
PUCCŰM – BIAŁA PLAMA  
PUCU – BIEL; PĘCHERZ  
PUHHU – D: ZAMIENIAĆ, WYMIENIAĆ  
\[jako rzeczownik\] ZAMIENIANIE, WYMIENIANIE  
PUHHU – ZAMIENIONY, WYMIENIONY  
PUHHURU(M) – ZEBRANY; WZMOCNIONY  
PUHRU(M), PUHRU(M) – ZGROMADZENIE, ZEBRANIE; TŁUM; SIŁA ZBROJNA  
PUHU(M) – NAMIASTKA; ODSZKODOWANIE; ZASTĘPCA  
PULHU – OBAWA, STRACH, BOJAŹŃ  
r.ż. PULUHTUM  
PULUHTU – OBAWA, STRACH; GŁĘBOKI SZACUNEK, CZEŚĆ; KTOŚ DRŻĄCY ZE STRACHU, WYSTRASZONY  
s.c. PULUHTI  
PULUKKU – GRANICA \[sum.\]  
PUQQUDU- UMIESZCZONY, USTAWIONY  
PURATTU – EUFRAT  
PURIDDU lub PURIDU – NOGA  
INA PIT PURIDI – W MIG, BŁYSKAWICZNIE  
PURIDU lub PURIDDU – j.w.  
PURIMU – DZIKI OSIOŁ  
PURRURU – ROZPRASZAĆ, ROZGRAMIAĆ  
\[jako rzeczownik\] ROZPRASZANIE, ROZGRAMIANIE  
PURRURU – ROZPROSZONY, ROZGROMIONY  
PURUSSŰ – DECYZJA, ROZSTRZYGNIĘCIE (sądowe); WYROCZNIA  
PUŠQU(M) – CIASNOTA, CIASNE MIEJSCE, ZWĘŻENIE  
PUŠŠURU lub BUŠŠURU – D: OGŁOSIĆ, OBWIEŚCIĆ, ZAPOWIADAĆ, KOMUNIKOWAĆ  
\[jako rzeczownik\] OGŁOSZANIE, OBWIESZCZANIE, ZAPOWIADANIE  
PUŠŠURU- OGŁOSZONY, OBWIESZCZONY, ZAPOWIEDZIANY, ZAKOMUNIKOWANY  
PUT – PRZECIWKO, NAPRZECIWKO  
PUTT’U(M): OTWARTY, ODSŁONIONY; WYCZULONY, UWRAŻLIWIONY, UCZULONY (o uszach); NAKŁONIONY DO ROZ-  
SĄDKU; PRZEBITY, ROZPROSZONY (ciemność)  
PUTTŰ(M), PUTTŰ(M) – OTWARTY, ODSŁONIONY; WYCZULONY, UWRAŻLIWIONY, UCZULONY (o uszach); NAKŁONIONY  
DO ROZSĄDKU; PRZEBITY, ROZPROSZONY (ciemność)  
PUTU – FRONT; CZOŁO \[część ciała\]  
MUTIR PUTI – GWARDZISTA  
PUZRU – SCHOWEK; UKRYCIE; TAJEMNICA; (w l.mn.) DALEKI ZAKAMAREK  
l.mn. PUZRATU  
PUZZURU – D: UKRYWAĆ, UKRYĆ  
\[jako rzeczownik\] UKRYWANIE, KRYCIE  
PUZZURU – UKRYTY

Q

QA – KA \[miara objętości\]  
QA’IDU – ZAPALAJĄCY  
QA’ILU – MILCZĄCY; CZCZĄCY, WIELBIĄCY, SZANUJĄCY; STRZEGĄCY, PRZESTRZEGAJĄCY  
QA’IPU(M) – OBDARZAJĄCY ZAUFANIEM, POWIERZAJĄCY  
QA’IPU(M) – WALĄCY SIĘ, GROŻĄCY ZAWALENIEM; STOJĄCY KRZYWO  
QA’IŠU – OFIARUJĄCY, DARUJĄCY; OFIARNIK? DARCZYŃCA?  
QA’U – PLUĆ (zapis: KA-A-A-U)  
\[jako rzeczownik\] PLUCIE  
QA’U – WYPLUTY  
\[QA’Ű\] – zob. QU”U  
QABALTU lub QABLU – STARCIE, BITWA, BÓJ; ŚRODEK  
QABALTU(M) – ŚRODEK  
QABLU – WALKA  
QABLU lub QABALTU – STARCIE, BITWA, BÓJ; ŚRODEK  
QABLU(M) – ŚRODEK; CZĘŚĆ ŚRODKOWA; WNĘTRZE, GŁĘBIA; BIODRO  
l.pdw. QABLAN  
QABAL ALI – ŚRÓDMIEŚCIE, CENTRUM MIASTA  
QABAL TÂMTI – WYSPA  
QABLŰ(M) – ŚRODKOWY  
(MACCARTU) QABLITU – ŚRODKOWA NOCNA WACHTA  
QABRU – GRÓB  
QABŰ(M), QABŰ(M) (i) lub QIBŰ (qbj) – MÓWIĆ, WYMAWIAĆ, POWIEDZIEĆ, WYPOWIEDZIEĆ, WYGŁASZAĆ, OG-  
ŁASZAĆ; WOŁAĆ, KRZYCZEĆ; WYPOWIEDZIEĆ SIĘ; WYJAŚNIAĆ, UDOWODNIĆ; WSKAZAĆ, PO-  
DAĆ; OBIECYWAĆ; DEKLAROWAĆ; NAKAZYWAĆ, KAZAĆ, ROZKAZYWAĆ; PODJĄĆ DECYZJĘ; NA-  
ZWAĆ, NADAĆ IMIĘ; MODLIĆ SIĘ  
\[jako rzeczownik\] MÓWIENIE, WYMAWIANIE, POWIEDZENIE, WYPOWIEDZENIE, WYGŁASZANIE,  
OGŁASZANIE; WOŁANIE, KRZYCZENIE; WYPOWIADANIE SIĘ; WYJAŚNIANIE, UDOWADNIANIE;  
WSKAZANIE, PODANIE; OBIECYWANIE; DEKLAROWANIE; NAKAZYWANIE, KAZANIE, ROZKAZY  
WANIE; PODEJMOWANIE DECYZJI; NAZYWANIE, NADAWANIE IMIENIA; MODLENIE SIĘ  
GT QITBU(M), QITBU(M): POWIEDZIEĆ, MÓWIĆ, WYJAŚNIAĆ, NAKAZYWAĆ  
\[jako rzeczownik\] POWIEDZENIE, MÓWIENIE, WYJAŚNIANIE, NAKAZYWANIE  
x (Dat. osoby) y (Akk. rzeczy) QABŰ – OBIECYWAĆ y (coś) x (komuś)  
QABŰ(M), QABŰ(M) – WYMÓWIONY,WYPOWIEDZIANY, WYGŁOSZONY, OGŁOSZONY;ZAWOŁANY,WYKRZYCZANY;  
WYJAŚNIONY, UDOWODNIONY; WSKAZANY, PODANY; OBIECANY; ZADEKLAROWANY; NAKAZANY,  
ROZKAZANY; NAZWANY, OPATRZONY IMIENIEM  
QABŰ(M), QABŰ(M) – WYMAWIAJĄCY, WYPOWIADAJĄCY, WYGŁASZAJĄCY, OGŁASZAJĄCY; WOŁAJĄCY, KRZYCZĄCY;  
WYJAŚNIAJĄCY, UDOWADNIAJĄCY; WSKAZUJĄCY, PODAJĄCY; OBIECUJĄCY; DEKLARUJĄCY; NAKAZU  
JĄCY, ROZKAZUJĄCY; PODEJMUJĄCY DECYZJĘ; NAZYWAJĄCY, NADAJĄCY IMIĘ; MODLĄCY SIĘ  
QACARU \[asyr.\] lub KACARU – WIĄZAĆ, ZAWIĄZYWAĆ; OBMYŚLAĆ, PLANOWAĆ  
\[jako rzeczownik\] WIĄZANIE, ZAWIĄZYWANIE; OBMYŚLANIE, PLANOWANIE  
UŠMANNA KACARU – ROZBIJAĆ (UMOCNIONY) OBÓZ  
CABE (ITTI) KACARU – WYPOSAŻAĆ, EKWIPOWAĆ  
QACIRU – WIĄŻĄCY, ZAWIĄZUJĄCY; OBMYŚLAJĄCY, PLANUJĄCY  
QACRU – ZWIĄZANY, ZAWIĄZANY; OBMYŚLONY, ZAPLANOWANY  
QADADU (u) – ZGINAĆ SIĘ  
\[jako rzeczownik\] ZGINANIE SIĘ  
QADDU – ZGIĘTY (samoczynnie, bez nacisku z zewnątrz?)  
QADIDU – ZGINAJĄCY SIĘ  
QADIŠTU – PROSTYTUTKA ŚWIĄTYNNA  
QADU (a/u) – ZAPALAĆ  
\[jako rzeczownik\] ZAPALANIE  
QADU – ZAPALONY  
QADUM – DOPÓKI, JAK DŁUGO \[wprowadza zdanie czasowe\]  
QADUTU – SZLAM, MUŁ, BŁOTO  
QALAPU (a/u) – OBIERAĆ Z ŁUPINY, OBŁUPIAĆ  
\[jako rzeczownik\] OBIERANIE Z ŁUPINY, OBŁUPIANIE  
QALIPU – OBIERAJĄCY Z ŁUPINY, OBŁUPIAJĄCY  
QALLATU – ŻÓŁĆ  
QALLIŠ – LEKKOMYŚLNIE  
QALLU – LEKKI  
QALLU – zob. GALLU  
QALPU – OBRANY Z ŁUPINY, OBŁUPIONY  
QALU – CZCZONY, SZANOWANY; STRZEŻONY, PRZESTRZEŻONY  
QÂLU (q’l?) – MILCZEĆ; CZCIĆ, WIELBIĆ, SZANOWAĆ; STRZEC, PRZESTRZEGAĆ  
\[jako rzeczownik\] MILCZENIE; CZCZENIE, WIELBIENIE, SZANOWANIE; STRZEŻENIE, PRZESTRZEGANIE  
GTN: QITA’ULU – CIERPIEĆ W MILCZENIU  
\[jako rzeczownik\] CIERPIENIE W MILCZENIU  
QALŰ (u, i) – SPALAĆ, PALIĆ  
\[jako rzeczownik\] SPALANIE, PALENIE  
QALŰ – SPALONY  
QALŰ – SPALAJĄCY, PALĄCY  
QAMŰ (u, i) – SPALAĆ  
\[jako rzeczownik\] SPALANIE  
QAMŰ – SPALONY  
QAMŰ – SPALAJĄCY  
QANŰ – TRZCINA \[sum.\]  
QAN APPARI = APPARATE  
QÂPU(M) (qip) – ZAUFAĆ; POWIERZYĆ (podw. Akk. – komuś coś)  
\[jako rzeczownik\] ZAUFANIE, POWIERZENIE  
QAPU(M) – POWIERZONY  
QÂPU(M) (qup) – BYĆ ZAGROŻONYM ZAWALENIEM, GROZIĆ ZAWALENIEM; STAĆ KRZYWO  
\[jako rzeczownik\] BYCIE ZAGROŻONYM ZAWALENIEM, GROŻENIE ZAWALENIEM; STANIE KRZYWO  
QAPU(M) – ZAGROŻONY ZAWALENIEM; POSTAWIONY KRZYWO  
QÂQÂDU lub QAQQADU(M), QAQDU – GŁOWA; CZUBEK GŁOWY, CZEREP, CIEMIĘ, POTYLICA; SZCZYT,  
WIERZCHNIA CZĘŚĆ; CZĘŚĆ CZOŁOWA, PRZEDNIA STRONA; WZNIESIENIE, PŁASKOWYŻ GÓRSKI,  
RÓWNINA GÓRSKA; KAPITAŁ (BEZ PROCENTÓW); ROZSĄDEK, ROZUM; ZMYSŁ; JAKAŚ CZĘŚĆ  
PŁUGA; RÓD ŻMIJI; ŁĄCZNA, OGÓLNA ILOŚĆ  
s.c. QAQQAD, l.pdw. QAQQADÂ, l.mn. QAQQÂDI, QAQQÂDÂTI  
QAQQÂD UMANI – CZOŁO WOJSKA  
QAQQAD KASPI – KAPITAŁ  
CALMAT QAQQADI – (dosł. CZARNOGŁOWI) = SEMICI  
MURUC QAQQADI – CHOROBA GŁOWY  
QAQDU lub QAQQADU(M), QÂQÂDU – j.w.  
QAQQADU(M), QAQQÂDU(M) lub QÂQÂDU, QAQDU – j.w.  
QAQQARIŠ – JAK ZIEMIA \[młodobab.\]  
QAQQARU – ZIEMIA, GRUNT, GLEBA  
QARABU(M), QARABU(M) (i, u) lub QEREBU(M) – ZBLIŻAĆ SIĘ, NADCHODZIĆ; ZOSTAĆ WRĘCZONYM  
\[jako rzeczownik\] ZBLIŻANIE SIĘ, NADCHODZENIE; ZOSTANIE WRĘCZONYM  
GT QITRUBU(M), QITRUBU(M): ZBLIŻAĆ SIĘ NAWZAJEM DO SIEBIE  
\[jako rzeczownik\] ZBLIŻANIE SIĘ NAWZAJEM DO SIEBIE  
ANA x QARABU lub CER x QARABU – NADCHODZIĆ KU x  
INA QITRUB TAHAZI – W ZDERZENIU PODCZAS BITWY  
QARARU – UPAŁ, SKWAR  
QARARU(M) (u) – ZWIJAĆ; ZWIJAĆ SIĘ  
\[jako rzeczownik\] ZWIJANIE; ZWIJANIE SIĘ  
N NAQRURU(M): ZGINAĆ SIĘ  
\[jako rzeczownik\] ZGINANIE SIĘ  
NTN ITTAQRURU(M): ZWIJAĆ SIĘ KILKAKROTNIE  
\[jako rzeczownik\] ZWIJANIE SIĘ KILKUKROTNE  
QARBU(M), QARBU(M) – ZBLIŻONY, NADESZŁY; WRĘCZONY  
QARDAMMU – ZŁY (i inne)  
QARDU – SILNY, MOCNY, POTĘŻNY; WOJOWNICZY; DZIELNY, ODWAŻNY; BOHATER  
s.c. QARAD, r.ż. QARATTU, QARITTI, l.mn. QARDUTI  
QARIBU(M), QARIBU(M) – ZBLIŻAJĄCY SIĘ, NADCHODZĄCY  
QARIRU(M) – ZWIJAJĄCY; ZWIJAJĄCY SIĘ  
QARITU – SZOPA  
QARNU – RÓG (bydlęcia albo księżyca) \[r.ż.\]  
QARRADU – SILNY; BOHATER; zob. QARDU  
QARRADU(M) – BOHATER  
QARRADUTU – MĘSTWO WOJENNE, ODWAGA W BOJU  
QARRASUM – BARDZO SILNY  
QARRU(M) – ZWINIĘTY  
QAŠTU(M) – ŁUK \[r.ż.\]  
QAŠU (a/i) – OFIAROWYWAĆ, DAROWAĆ  
\[jako rzeczownik\] OFIAROWYWANIE, DAROWANIE  
QAŠU – OFIAROWANY, DAROWANY  
QATANU(M) (i) – BYĆ CIENKIM  
\[jako rzeczownik\] BYCIE CIENKIM  
QATAPU (a/u) – POZBIERAĆ, ZEBRAĆ  
\[jako rzeczownik\] POZBIERANIE, ZEBRANIE  
QATNU(M) – CIENKI  
QATPU – ZEBRANY, POZBIERANY  
QATTANUM – BARDZO WĄSKI  
QATŰ(M) (kti) – KOŃCZYĆ SIĘ, SKOŃCZYĆ SIĘ  
\[jako rzeczownik\] KOŃCZENIE SIĘ, SKOŃCZENIE SIĘ  
D QUTTŰ(M): (w tekstach astrologicznych w odniesieniu do zaćmienia:) ZBLIŻAĆ SIĘ DO KOŃCA  
\[jako rzeczownik\] ZBLIŻANIE SIĘ DO KOŃCA  
DT QUTATTŰ(M): BYĆ WYKONANYM DO KOŃCA, BYĆ GOTOWYM  
\[jako rzeczownik\] BYCIE WYKONANYM DO KOŃCA, BYCIE GOTOWYM  
QATŰ(M) – SKOŃCZONY  
QATŰ(M) – KOŃCZĄCY SIĘ  
QATU(M) – RĘKA \[r.ż.\]  
l.pdw. QATAN, l.mn. QATATU  
QATATE ANA LEMNETI INA LIBBI x UBALU – RĘCE W ZŁYM ZAMIARZE NA x PODNIEŚĆ  
ŠA QÂT – MIENIE, WŁASNOŚĆ  
NIŠ QATI – PODNIESIENIE RĄK \[szczególny rodzaj modlitwy\]  
ŠI LU QATKA – TO ZNAJDUJE SIĘ W TWOICH RĘKACH \[poet.\]  
ŠALAŠ QATATU, ŠALAŠ QATATI – TRZY CZWARTE  
QEBERU(M) (i) – GRZEBAĆ, CHOWAĆ, SPRAWIAĆ POGRZEB  
\[jako rzeczownik\] GRZEBANIE, CHOWANIE, SPRAWIANIE POGRZEBU  
BIT QEBERI – GRÓB  
QEBIRU(M) – GRZEBIĄCY, CHOWAJĄCY, ODPRAWIAJĄCY POGRZEB  
QEBRU(M) – POGRZEBANY, POCHOWANY  
QEMU – MĄKA  
QEPU – NAMIESTNIK, REZYDENT  
QEPU lub HAZANNU – NACZELNIK MIASTA  
QEPU – POWIERZONY Z ZAUFANIEM  
QERBITU lub QIRBITU – RÓWNINNE POLE; POWIERZCHNIA ZIEMI  
QERBU – WNĘTRZE  
s.c. QEREB  
QEREB – W, WŚRÓD, POŚRODKU  
QERBUM BABILI – W BABILONIE  
ANA QEREB – WŚRÓD, WEWNĄTRZ  
QERBU – BLISKI, NIEDALEKI  
QEREBU(M) lub QARABU(M) – ZBLIŻAĆ SIĘ, NADCHODZIĆ; ZOSTAĆ WRĘCZONYM  
\[jako rzeczownik\] ZBLIŻANIE SIĘ, NADCHODZENIE; ZOSTANIE WRĘCZONYM  
GT QITRUBU(M): ZBLIŻAĆ SIĘ NAWZAJEM DO SIEBIE  
\[jako rzeczownik\] ZBLIŻANIE SIĘ NAWZAJEM DO SIEBIE  
ANA x QARABU lub CER x QARABU – NADCHODZIĆ KU x  
INA QITRUB TAHAZI – W ZDERZENIU PODCZAS BITWY  
QERBU(M) – ZBLIŻONY, NADESZŁY; WRĘCZONY  
QERIBU(M) – ZBLIŻAJĄCY SIĘ, NADCHODZĄCY  
QERITU lub QIRITU – MAGAZYN (ZIARNA)  
QETU – KONIEC  
QIBITU – ROZKAZ, NAKAZ, POLECENIE  
QIBŰ, QIBŰ(M) (qbj) lub QABŰ (M) (i) – MÓWIĆ, WYMAWIAĆ, POWIEDZIEĆ, WYPOWIEDZIEĆ, WYGŁASZAĆ,  
OGŁASZAĆ; WOŁAĆ, KRZYCZEĆ; WYPOWIEDZIEĆ SIĘ; WYJAŚNIAĆ, UDOWODNIĆ; WSKAZAĆ,  
PODAĆ; OBIECYWAĆ; DEKLAROWAĆ; NAKAZYWAĆ, KAZAĆ, ROZKAZYWAĆ; PODJĄĆ DECYZJĘ;  
NAZWAĆ, NADAĆ IMIĘ; MODLIĆ SIĘ  
\[jako rzeczownik\] MÓWIENIE, WYMAWIANIE, POWIEDZENIE, WYPOWIEDZENIE, WYGŁASZANIE,  
OGŁASZANIE; WOŁANIE, KRZYCZENIE; WYPOWIADANIE SIĘ; WYJAŚNIANIE, UDOWADNIANIE;  
WSKAZANIE, PODANIE; OBIECYWANIE; DEKLAROWANIE; NAKAZYWANIE, KAZANIE, ROZKAZY  
WANIE; PODEJMOWANIE DECYZJI; NAZYWANIE, NADAWANIE IMIENIA; MODLENIE SIĘ  
GT QITBU(M), QITBU(M): POWIEDZIEĆ, MÓWIĆ, WYJAŚNIAĆ, NAKAZYWAĆ  
\[jako rzeczownik\] POWIEDZENIE, MÓWIENIE, WYJAŚNIANIE, NAKAZYWANIE  
x (Dat. osoby) y (Akk. rzeczy) QABŰ – OBIECYWAĆ y (coś) x (komuś)  
QIBŰ(M), QIBŰ(M) – WYMÓWIONY,WYPOWIEDZIANY, WYGŁOSZONY, OGŁOSZONY;ZAWOŁANY,WYKRZYCZANY;  
WYJAŚNIONY, UDOWODNIONY; WSKAZANY, PODANY; OBIECANY; ZADEKLAROWANY; NAKAZANY,  
ROZKAZANY; NAZWANY, OPATRZONY IMIENIEM  
QIBŰ(M), QIBŰ(M) – WYMAWIAJĄCY, WYPOWIADAJĄCY, WYGŁASZAJĄCY, OGŁASZAJĄCY; WOŁAJĄCY, KRZYCZĄCY;  
WYJAŚNIAJĄCY, UDOWADNIAJĄCY; WSKAZUJĄCY, PODAJĄCY; OBIECUJĄCY; DEKLARUJĄCY; NAKAZU  
JĄCY, ROZKAZUJĄCY; PODEJMUJĄCY DECYZJĘ; NAZYWAJĄCY, NADAJĄCY IMIĘ; MODLĄCY SIĘ  
QILLATU – ZBRODNIA, PRZESTĘPSTWO  
QIMMATU – CZUBEK DRZEWA  
QINAZU – BICZ, BAT  
QINNATU(M) – ODBYT  
QINNU – GNIAZDO  
QIPIRTU lub KIBIRTU – OGRANICZONE POMIESZCZENIE; POKÓJ; MIEJSCOWOŚĆ; OBWÓD?; CZĘŚĆ ŚWIATA,  
STRONA ŚWIATA; OBSZAR NIEBIOS, SKRAJ NIEBIOS; por. KIBRU(M)  
s.c. KIBRAT, QIPRAT, l.mn. KIBRATI, QIPRATI  
QIPTU – \[składnik zwrotu\]  
ANA QIPTI – NA KREDYT  
QIRBITU lub QERBITU – RÓWNINNE POLE; POWIERZCHNIA ZIEMI  
QIRITU lub QERITU – MAGAZYN (ZIARNA)  
QIŠŠŰ – OGÓREK  
QIŠTU – PLANTACJA DRZEW, GAJ  
QIŠTU – DAR, PREZENT, PODARUNEK; WYNAGRODZENIE, ZAPŁATA, HONORARIUM  
QIŠTU lub KIŠTU – LAS  
QITRUBIŠ – W WALCE WRĘCZ  
QŰ – PRZĘDZA, NIĆ; KONOPIE \[sum.\]  
QŰ(M) – WĘZEŁ  
QU”Ű – CZEKAĆ, OCZEKIWAĆ  
\[jako rzeczownik\] CZEKANIE, OCZEKIWANIE  
MUQA’’ Ű – CZEKAJĄCY, OCZEKUJĄCY  
QULIPTU – SKÓRA (węża)  
QULLULTU – ZBRODNIA, HANIEBNY CZYN  
QULU – CISZA, SPOKÓJ  
QULU(M) – PŁACZ, LAMENTY  
QUPPU – KLATKA  
QURADU – WOJOWNIK, ŻOŁNIERZ  
QURBU – GWARDZISTA, CZŁONEK GWARDII KRÓLEWSKIEJ  
QURDU – CZYN WOJENNY  
QUTATTŰ(M) – WYKONANY DO KOŃCA, GOTOWY  
QUTRINNU – WĘDZENIE  
QUTRU – DYM

R  
RA’ABU (u) – SZALEĆ, WŚCIEKAĆ SIĘ  
\[jako rzeczownik\] SZALENIE, WŚCIEKANIE SIĘ  
RA’AMU (a) lub RAMU (r’m) – KOCHAĆ  
\[jako rzeczownik\] KOCHANIE  
RA’ASU – POWALIĆ  
\[jako rzeczownik\] POWALENIE  
RA’IBU – SZALEJĄCY, WŚCIEKAJĄCY SIĘ  
RA’IBU – ZASTĘPUJĄCY  
RA’ICU lub RECU, RICU (rwc) – POMOCNIK  
RA’ICU – POMAGAJĄCY  
RA’IMU(M) – KOCHAJĄCY; KOCHANEK  
RA’ISU – POWALAJĄCY  
RA’MU – KOCHANY  
RA’SU – POWALONY  
RA’SUM lub REŠU(M), RAŠU – GŁOWA, ŁEB; WIERZCHOŁEK; POCZĄTEK; WYŻSZA CZĘŚĆ (budowli); NIEWOLNIK  
REŠ EQLIM – CEL  
REŠAM KULLUM – TRZYMAĆ GŁOWĘ; WSPOMAGAĆ, WSPIERAĆ  
INA REŠUŠŠU (lok-adv) – NA JEGO GŁOWIE  
ŠAREŠI – EUNUCH  
RABABU (u) – BYĆ DELIKATNYM, SŁABYM  
\[jako rzeczownik\] BYCIE DELIKATNYM, BYCIE SŁABYM  
RABACU (i) – OBOZOWAĆ; \[Lipin\] KŁAŚĆ SIĘ, WALIĆ SIĘ  
\[jako rzeczownik\] OBOZOWANIE; KŁADZENIE SIĘ, WALENIE SIĘ  
RABÂT lub RABIAT, REBÂT – JEDNA CZWARTA, ĆWIERĆ  
l.mn. RABIATUM  
RABBU – DELIKATNY, SŁABY  
RABBŰ – zob. REBU(M)  
RABCU – POŁOŻONY, ZWALONY  
RABIANU(M) – NACZELNIK MIASTA  
RABIAT lub RABÂT, REBÂT – JEDNA CZWARTA, ĆWIERĆ  
l.mn. RABIATUM  
RABICU – OBOZUJĄCY; KŁADĄCY SIĘ, WALĄCY SIĘ  
RABICU – RABICU \[imię złego ducha\]; TABORET?  
RABITU(M), RABITU(M) – WIELKA, DUŻA; SZLACHETNA, ZNAKOMITA; POWIĘKSZONA, ROZROŚNIĘTA  
\[jako abstrakt\] WIELKOŚĆ, DOSTOJEŃSTWO  
RABIU(M) \[asyr.\] lub RABŰ (M) – WIELKI, DUŻY; POWIĘKSZONY, ROZROŚNIĘTY; SZLACHETNY, ZNAKOMITY;  
STARSZY (rangą); WIELMOŻA, DOSTOJNIK \[sum. GAL\]  
s.c. RAB, l.mn. RABŰTI, r.ż. RABÎTU, l.mn. RABÂTI  
APLUM RABŰM – NAJSTARSZY SYN  
RABI EKALLE \[śr.asyr.\] – INSPEKTOR PAŁACOWY  
RABI URTANE – SKARBNIK (?)  
RABI ZAMMARE – MUZYKANT (?)  
RAB HALCI, RAB HALCU – KOMENDANT TWIERDZY  
RAB EŠRI – DZIESIĘTNIK  
RABU (a/i) – ZASTĘPOWAĆ  
\[jako rzeczownik\] ZASTĘPOWANIE  
RABU – ZASTĄPIONY  
RABŰ – ZACHODZIĆ (o słońcu)  
\[jako rzeczownik\] ZACHODZENIE (słońca)  
RABŰ – ZASZŁY (o słońcu)  
RABŰ – ZACHODZĄCY (o słońcu)  
RABŰ lub REBŰ (M), RABBŰ – CZWARTY; JEDNA CZWARTA \[ułamek\], ĆWIARTKA  
r.ż. REBUTUM, REBITU  
RABŰ(M), RABŰ(M) (rbi) – BYĆ DUŻYM, WIELKIM; STAWAĆ SIĘ DUŻYM, WIELKIM, ROSNĄĆ, UROSNĄĆ, ZWIĘK-  
SZAĆ SIĘ, POWIĘKSZAĆ SIĘ  
\[jako rzeczownik\] BYCIE DUŻYM, BYCIE WIELKIM; STAWANIE SIĘ DUŻYM, WIELKIM, ROŚNIĘCIE,  
UROŚNIĘCIE, ZWIĘKSZANIE SIĘ, POWIĘKSZANIE SIĘ  
GT RITBŰ(M), RITBŰ(M): UROSNĄĆ, STAĆ SIĘ DUŻYM  
\[jako rzeczownik\] UROŚNIĘCIE, STANIE SIĘ DUŻYM  
D RUBBŰ(M), RUBBŰ(M) : UCZYNIC DUŻYM, WIELKIM; CHOWAĆ, WYCHOWAĆ; HODOWAĆ, WYHO-  
DOWAĆ; PODNIEŚĆ; WYNIEŚĆ, WYWYŻSZYĆ  
\[jako rzeczownik\] CZYNIENIE DUŻYM, WIELKIM; CHOWANIE, WYCHOWANIE; HODOWANIE, WYHO  
DOWANIE; PODNOSZENIE; WYNOSZENIE, WYWYŻSZANIE  
DT RUTABBŰ(M), RUTABBŰ(M): PODNIEŚĆ DO GÓRY; UCZYNIĆ DUŻYM, WIELKIM; WYHODOWAĆ;  
WYCHOWAĆ  
\[jako rzeczownik\] PODNOSZENIE DO GÓRY; CZYNIENIE DUŻYM, WIELKIM; CHOWANIE, WYCHOWA-  
NIE; HODOWANIE, WYHODOWANIE  
D/Š: WYWYŻSZYĆ; KAZAĆ STAĆ SIĘ DUŻYM, UMOŻLIWIĆ STANIE SIĘ DUŻYM, WIELKIM; KAZAĆ  
PODNIEŚĆ, UMOŻLIWIĆ PODNIESIENIE; KAZAĆ ZWIĘKSZYĆ SIĘ, UMOŻLIWIĆ ZWIĘKSZENIE SIĘ  
\[jako rzeczownik\] WYWYŻSZANIE; SPOWODOWANIE STANIA SIĘ DUŻYM, WIELKIM; NAKAZANIE  
PODNIESIENIA, UMOŻLIWIENIE PODNIESIENIA; ZLECENIE, SPOWODOWANIE POWIĘKSZENIA SIĘ  
Š ŠURBŰ(M), ŠURBŰ(M): WYWYŻSZYĆ; UCZYNIĆ SILNYM, POTĘŻNYM  
\[jako rzeczownik\] WYWYŻSZANIE; CZYNIENIE SILNYM, POTĘŻNYM  
INA MUHHI RABŰ – WZROSNĄĆ (procenty – CIPTU)  
ELI x KAKKE ŠURBŰ – PRZYZNAWAĆ ZWYCIĘSTWO NAD x  
RABU(M) lub RABIU(M) – WIELKI, DUŻY; POWIĘKSZONY, ROZROŚNIĘTY; SZLACHETNY, ZNAKOMITY; STAR-  
SZY (rangą); WIELMOŻA, DOSTOJNIK \[sum. GAL\]  
s.c. RAB, l.mn. RABŰTI, r.ż. RABÎTU, l.mn. RABÂTI  
APLUM RABŰM – NAJSTARSZY SYN  
RABI EKALLE \[śr.asyr.\] – INSPEKTOR PAŁACOWY  
RABI URTANE – SKARBNIK (?)  
RABI ZAMMARE – MUZYKANT (?)  
RAB HALCI, RAB HALCU – KOMENDANT TWIERDZY  
RAB EŠRI – DZIESIĘTNIK  
RAB CAQE – KRAJCZY  
CEHER RABI – MAŁY I WIELKI = WSZYSCY  
RABŰ(M), RABŰ(M) – POWIĘKSZAJĄCY SIĘ, ROSNĄCY  
RABŰTU(M) – WIELKOŚĆ; GODNOŚĆ  
KIMA RABŰTIKA – STOSOWNY, ODPOWIEDNI DO TWEJ GODNOŚCI, KORZYSTNY, DOGODNY  
RACAPU (i) – SPAJAĆ, ŁĄCZYĆ; BUDOWAĆ  
\[jako rzeczownik\] SPAJANIE, ŁĄCZENIE; BUDOWANIE  
RACIPU – SPAJAJĄCY, ŁĄCZĄCY; BUDUJĄCY  
RACPU – SPOJONY, POŁĄCZONY; ZBUDOWANY  
RACU (a/u) (a/i) – POMAGAĆ  
\[jako rzeczownik\] POMAGANIE  
RACU – WSPOMOŻONY, OBDARZONY POMOCĄ  
RADADU (a/u) – ŚCIGAĆ  
\[jako rzeczownik\] ŚCIGANIE  
RADDU – ŚCIGANY  
RADIDU – ŚCIGAJĄCY  
\[RADŰ\] – zob. RUDDU  
RADU(M) – BURZOWY; BURZA; ULEWA PODCZAS BURZY, OBERWANIE SIĘ CHMURY  
\[RAGABU\] – zob. RUGGUBU  
RAGAGU (i) – POSTĘPOWAĆ ŹLE  
\[jako rzeczownik\] POSTĘPOWANIE ZŁE  
RAGAMU (u) – KRZYCZEĆ, WRZESZCZEĆ  
\[jako rzeczownik\] KRZYCZENIE, WRZESZCZENIE  
ANA x ANA y RAGAMU – MIEĆ WOBEC x ZASKARŻALNE ROSZCZENIA O y (KH)  
RAGAMU(M) (a) – SKARŻYĆ  
\[jako rzeczownik\] SKARŻENIE  
RAGGU – ZŁY  
RAGGU(M) – ZŁO  
RAGIGU – POSTĘPUJĄCY ŹLE  
RAGIMU – KRZYCZĄCY, WRZESZCZĄCY  
RAGIMU – SKARŻĄCY  
RAGMU – WYKRZYCZANY, WYWRZESZCZANY  
RAGMU – SKARGA?  
RAHACU (u) (asyr. i) – MIEĆ ZAUFANIE  
\[jako rzeczownik\] POSIADANIE ZAUFANIA  
RAHACU(M) (i) – POWALIĆ, POBIĆ; SPUSTOSZYĆ; ZATOPIĆ, ZALEWAĆ, ZALAĆ  
\[jako rzeczownik\] POWALANIE; PUSTOSZENIE; ZATAPIANIE, ZALEWANIE, ZALANIE  
RAHCU – OBDARZONY ZAUFANIEM  
RAHCU(M) – POWALONY, POBITY; SPUSTOSZONY; ZATOPIONY, ZALANY  
RAHICU – MAJĄCY ZAUFANIE  
RAHICU(M) – POWALAJĄCY, BIJĄCY; PUSTOSZĄCY; ZATAPIAJĄCY, ZALEWAJĄCY  
RAKABU(M) (a) – JEŹDZIĆ WIERZCHEM; JEŹDZIĆ (na czymkolwiek, rydwanem, statkiem); WYRASTAĆ NA (czymś)  
\[jako rzeczownik\] JEŻDŻENIE WIERZCHEM; JEŻDŻENIE; WYRASTANIE NA  
GT RITKUBU(M): JEŹDZIĆ WIERZCHEM JEDEN ZA DRUGIM  
\[jako rzeczownik\] JEŻDŻENIE WIERZCHEM JEDEN ZA DRUGIM, UCZESTNICZENIE W KAWALKADZIE  
D RUKKUBU(M): ZAPŁADNIAĆ  
\[jako rzeczownik\] ZAPŁADNIANIE  
RAKASU (a, u) – WIĄZAĆ, ZAWIĄZYWAĆ, PRZYWIĄZYWAĆ (niemowlę); UŁOŻYĆ; PRZYGOTOWYWAĆ, PRZYGO-  
TOWYWAĆ KULTOWO  
\[jako rzeczownik\] WIĄZANIE, ZAWIĄZYWANIE, PRZYWIĄZYWANIE; UKŁADANIE; PRZYGOTOWYWA-  
NIE, PRZYGOTOWYWANIE DO CELÓW KULTOWYCH  
N NARKUSU: ZNAJDOWAĆ SIĘ RAZEM  
\[jako rzeczownik\] ZNAJDOWANIE SIĘ RAZEM  
D RUKKUSU: ZOBOWIĄZYWAĆ (kogoś do czegoś – z biernikiem osoby i ANA z nazwą rzeczy)  
\[jako rzeczownik\] ZOBOWIĄZYWANIE  
MEŠIR ER€ RUKKUSU – (coś) POKRYWAĆ BRĄZEM  
HALCU RUKKUSU – USYPAĆ UMOCNIENIA? WZNIEŚĆ TWIERDZĘ? (Zernierungswerk aufwerfen)  
RAKBŰ(M) – GONIEC, POSŁANIEC  
RAKIBU(M) – JEŹDZIEC; JADĄCY WIERZCHEM; PODRÓŻNY JADĄCY (na czymś); NAROŚL?  
RAKIB IMERI – JEŹDZIEC NA OŚLE; ODDZIAŁ JEŹDŹCÓW NA OSŁACH  
RAKISU – ZAPRZĘGANIE (do wozu); \[Lipin\] ZWIĄZYWANIE  
RAKISU – WIĄŻĄCY, ZAWIĄZUJĄCY, PRZYWIĄZUJĄCY; UKŁADAJĄCY; PRZYGOTOWUJĄCY (w tym na potrzeby kultu)  
RAKSU – ZWIĄZANY, ZAWIĄZANY, PRZYWIĄZANY; UŁOŻONY; PRZYGOTOWANY; GOTOWY (do udziału w kulcie)  
RAMAKU(M) (u) – MYĆ SIĘ, KĄPAĆ SIĘ (w – Akk.)  
\[jako rzeczownik\] MYCIE SIĘ, KĄPANIE SIĘ  
RAMAMU (u) – GRZMIEĆ (o piorunie)  
\[jako rzeczownik\] GRZMIENIE  
RAMANU(M) lub RAMENU, RAMNU, RAMUNU(M) – WŁASNA OSOBA  
INA KASAP RAMANIŠU – WŁASNYM KOSZTEM, NA WŁASNY KOSZT  
INA RAMANIKA \[staroakad.\] – W TWOJEJ OSOBIE, TY SAM  
ANA RAMINIŠU \[średnioasyr.\] – DLA SIEBIE (?)  
ANA RAMANIŠA – (TYLKO) SAM DLA SIEBIE  
RAMENU lub RAMANU(M), RAMNU, RAMUNU(M) – j.w.  
RAMIKU(M) – MYJĄCY SIĘ, KĄPIĄCY SIĘ  
RAMIMU – GRZMIĄCY (o piorunie)  
RAMKU – (pewnego rodzaju) KAPŁAN  
RAMKU(M) – UMYTY, WYKĄPANY  
RAMNU lub RAMENU, RAMANU(M), RAMUNU(M) – WŁASNA OSOBA, SAM  
INA KASAP RAMANIŠU – WŁASNYM KOSZTEM, NA WŁASNY KOSZT  
INA RAMANIKA \[staroakad.\] – W TWOJEJ OSOBIE, TY SAM  
ANA RAMINIŠU \[średnioasyr.\] – DLA SIEBIE (?)  
ANA RAMANIŠA – (TYLKO) SAM DLA SIEBIE  
RÂMU (r’m) lub RA’AMU (a) – KOCHAĆ  
\[jako rzeczownik\] KOCHANIE  
RAMU – KOCHANY  
RAMŰ (u) – ZRYWAĆ (fundament); UWALNIAĆ SIĘ; POBŁAŻAĆ, FOLGOWAĆ  
\[jako rzeczownik\] ZRYWANIE (fundamentu); UWALNIANIE SIĘ; POBŁAŻANIE, FOLGOWANIE  
D RUMMŰ: UMARZAĆ; UWALNIAĆ; USTĘPOWAĆ, POBŁAŻAĆ; (poet.) PRZEBACZAĆ  
\[jako rzeczownik\] UMARZANIE; UWALNIANIE; USTĘPOWANIE, POBŁAŻANIE; PRZEBACZANIE  
RAMŰ – ZERWANY (fundament); UWOLNIONY; TRAKTOWANY Z POBŁAŻANIEM  
RAMŰ – ZRYWAJĄCY (fundament); UWALNIAJĄCY SIĘ; POBŁAŻAJĄCY, FOLGUJĄCY  
RAMŰ (rmi) – SKŁADAĆ, KŁAŚĆ; RZUCAĆ; MIESZKAĆ; ZNAJDOWAĆ SIĘ (St.) ?  
\[jako rzeczownik\] SKŁADANIE; KŁADZENIE; RZUCANIE; MIESZKANIE; ZNAJDOWANIE SIĘ  
ŠUBTA RAMŰ – ZAKŁADAĆ DOM, OSIEDLAĆ SIĘ, OSIADAĆ  
RAMŰ – ZŁOŻONY, POŁOŻONY; RZUCONY; ZAMIESZKAŁY  
RAMŰ – SKŁADAJĄCY, KŁADĄCY; RZUCAJĄCY; ZAMIESZKUJĄCY  
RAMUNU(M) \[asyr.\] lub RAMENU, RAMNU, RAMANU(M) – WŁASNA OSOBA, SAM  
INA KASAP RAMANIŠU – WŁASNYM KOSZTEM, NA WŁASNY KOSZT  
INA RAMANIKA \[staroakad.\] – W TWOJEJ OSOBIE, TY SAM  
ANA RAMINIŠU \[średnioasyr.\] – DLA SIEBIE (?)  
ANA RAMANIŠA – (TYLKO) SAM DLA SIEBIE  
RAPADU (u) – BIEGAĆ  
\[jako rzeczownik\] BIEGANIE  
RAPADU – PARALIŻ (?) \[jakaś choroba paralityczna\]  
RAPAŠTU – SZEROKI  
RAPAŠU(M) (i) – BYĆ SZEROKIM; POSZERZAĆ, ROZSZERZAĆ; BYĆ OBFITYM, SOWITYM  
\[jako rzeczownik\] BYCIE SZEROKIM; POSZERZANIE, ROZSZERZANIE; BYCIE OBFITYM, BYCIE SOWI-  
TYM  
RAPIDU – BIEGAJĄCY; BIEGACZ?  
RAPIŠU(M) – POSZERZAJĄCY, ROZSZERZAJĄCY  
RAPPU – GŁOWNIA, OŻÓG; PŁOMIEŃ?  
RAPPU – DYBY  
RAPŠU(M) – DALEKI; SZEROKI, POSZERZONY, ROZSZERZONY; OBFITY, SOWITY  
r.ż. RAPAŠTUM – DALEKA, SZEROKA; \[jako abstrakt\] SZEROKOŚĆ  
RAQAQU(M) (i) – BYĆ CIENKIM, DELIKATNYM  
\[jako rzeczownik\] BYCIE CIENKIM, DELIKATNYM  
RAQQU(M) – CIENKI; DELIKATNY  
RAQŰ (i) – CHOWAĆ, KRYĆ, UKRYWAĆ  
\[jako rzeczownik\] CHOWANIE, KRYCIE, UKRYWANIE  
N NARQŰ: CHOWAĆ, KRYĆ, UKRYWAĆ SIĘ  
\[jako rzeczownik\] CHOWANIE, KRYCIE, UKRYWANIE SIĘ  
RAQŰ – SCHOWANY, UKRYTY  
RAQŰ – CHOWAJĄCY, KRYJĄCY, UKRYWAJĄCY  
RÂQU(M) (riq) – BYĆ PUSTYM  
\[jako rzeczownik\] BYCIE PUSTYM  
RASABU lub RASAPU (i) – ZABIJAĆ, WYCINAĆ W PIEŃ  
\[jako rzeczownik\] ZABIJANIE, WYCINANIE W PIEŃ  
RASAPU lub RASABU (i) – j.w.  
RASBU – ZABITY, WYCIĘTY W PIEŃ  
RASIBU – ZABIJAJĄCY, WYCINAJĄCY W PIEŃ  
\[RAŠADU\] – zob. ŠURŠUDU  
RAŠAŠU(M) – RASZASZU \[jakiś funkcjonariusz\]  
RAŠBU(M) – POLECAJĄCY Z GŁĘBOKĄ CZCIĄ, NAKAZUJĄCY Z USZANOWANIEM; WZBUDZAJĄCY LĘK  
r.ż. RAŠUBTUM  
RAŠU lub REŠU(M), RA’SUM – GŁOWA, ŁEB; WIERZCHOŁEK; POCZĄTEK; WYŻSZA CZĘŚĆ (budowli); NIEWOLNIK  
REŠ EQLIM – CEL  
REŠAM KULLUM – TRZYMAĆ GŁOWĘ; WSPOMAGAĆ, WSPIERAĆ  
INA REŠUŠŠU (lok-adv) – NA JEGO GŁOWIE  
ŠAREŠI – EUNUCH  
RAŠU – WIERZYCIEL, POŻYCZKODAWCA  
RAŠŰ(M) (rši) – MIEĆ (St.); DOSTAWAĆ (Prs, Prt); NABYWAĆ; PRZYNIEŚĆ (Amm.)  
\[jako rzeczownik\] POSIADANIE; DOSTAWANIE; NABYWANIE; PRZYNIESIENIE  
Š ŠŰRŠŰ(M): KAZAĆ OTRZYMYWAĆ  
\[jako rzeczownik\] BYCIE SKŁONIONYM DO OTRZYMYWANIA, DOSTAWANIA  
ŠUM DAMIQTI RAŠU – OTRZYMAĆ WYGLĄD  
PANAM ŠURŠUM – STWIERDZIĆ; PRZEDSTAWIĆ  
RAŠŰ(M) – OTRZYMANY, NABYTY  
RAŠŰ(M) – POSIADAJĄCY? POSIADACZ? OTRZYMUJĄCY, NABYWAJĄCY; PRZYNOSZĄCY  
RAŠUBBATU – OKROPNOŚĆ  
RAŢBATU – WILGOTNA  
\[jako abstrakt\] WILGOTNOŚĆ, WILGOĆ  
RAŢBU – WILGOTNY  
RE’IMU – LITUJĄCY SIĘ, OKAZUJĄCY MIŁOSIERDZIE  
RE’IŠU – RADUJĄCY SIĘ, TRIUMFUJĄCY  
RE’Ű(M) (r’i) – PAŚĆ, PASAĆ  
\[jako rzeczownik\] PASIENIE, PASANIE  
RE’Ű(M) – PAŚIONY  
RE’Ű(M), RE’Ű(M) – PASTERZ; WŁADCA, PRZYWÓDCA, REGENT; OBROŃCA, STRÓŻ  
s.c. RE, RI, r.ż. RETU, s.c.r.ż. RE’AT  
RE’ŰTU – PASTERSTWO  
REBÂT lub RABIAT, RABÂT – JEDNA CZWARTA, ĆWIERĆ  
l.mn. RABIATUM  
ŠALAŠ REBÂT – TRZY CZWARTE  
R€BETU lub R€BITU, RÎBITU (r’b) – PLAC; PLAC CENTRALNY; BAZAR, RYNEK; OBSZAR MIASTA;  
PRZEDMIEŚCIE, OSADA PODMIEJSKA; OKRĘG  
l.mn. R€BÂTI  
R€BITU lub R€BETU, RÎBITU (r’b) – j.w.  
REBITU – zob. REBU(M)  
REBŰ(M), REBŰ(M) lub RABŰ, RABBŰ – CZWARTY; JEDNA CZWARTA \[ułamek\], ĆWIARTKA  
r.ż. REBUTUM, REBÎTU  
RECU – POMOCNIK  
R€CU lub RÎCU, RA’ICU (rwc) – POMOCNIK  
R€CŰTU lub RÎCŰTU – POMOC  
REDIUM lub REDŰ (M) – PRZYWÓDCA, WÓDZ; OFICER; (pewnego rodzaju) ŻOŁNIERZ  
REDŰ’A – MOI ŻOŁNIERZE  
REDŰ(M) lub REDIUM – j.w.  
REDŰ(M), REDŰ(M) (i) (rd’) – PÓJŚĆ; IŚĆ, ZDĄŻAĆ, ZMIERZAĆ (także z (W)ARKI); PRZEJMOWAĆ; PRZYPROWA-  
DZAĆ, DOPROWADZAĆ (świadków); PRZYNOSIĆ  
\[jako rzeczownik\] CHODZENIE, ZDĄŻANIE, ZMIERZANIE; PRZEJMOWANIE; DOPROWADZANIE (świadka);  
PRZYNOSZENIE  
GT RIDDŰ(M), RIDDŰ(M): IŚĆ DO SIEBIE, JEDEN DO DRUGIEGO; IŚĆ RÓWNOLEGLE; BYĆ RÓWNOLE  
GŁYM  
\[jako rzeczownik\] ZDĄŻANIE KU SOBIE; CHODZENIE, POSUWANIE SIĘ RÓWNOLEGLE; BYCIE RÓWNO  
LEGŁYM  
Š ŠURDŰ(M), ŠURDŰ(M): KAZAĆ PŁYNĄĆ, CIEC  
\[jako rzeczownik\] CIEKNIĘCIE; SPOWODOWANIE PŁYNIĘCIA  
ŠT ŠUTARDŰ(M), ŠUTARDŰ(M): PŁYNĄĆ, CIEC  
URHAŠU ŠURDŰ – IŚĆ SWOJĄ DROGĄ  
REDŰ(M), REDŰ(M) – PRZEJĘTY; DOPROWADZONY (o świadku); PRZYNIESIONY  
REDŰ(M), REDŰ(M) – CHODZĄCY, IDĄCY, ZDĄŻAJĄCY, ZMIERZAJĄCY; PRZEJMUJĄCY; DOPROWADZAJĄCY (świadka);  
PRZYNOSZĄCY  
REHTU lub RIHTU – RESZTA, POZOSTAŁOŚĆ \[r.ż.\]  
REMENU – ŁASKAWY  
REMU (e) – ZLITOWAĆ SIĘ, ZMIŁOWAĆ SIĘ; BYĆ ŁASKAWYM  
\[jako rzeczownik\] LITOWANIE SIĘ, ZMIŁOWANIE SIĘ; BYCIE ŁASKAWYM  
REMU – ŁASKAWY  
REMU – LITOŚĆ, POLITOWANIE  
REMUTU – ŁASKA  
R€QU – PUSTY  
REQU(M) – DALEKI  
UMAM REQAM – KTÓREGOŚ DALEKIEGO DNIA, W PRZYSZŁOŚCI  
R€QU(M) (e) (r’q) – BYĆ DALEKIM  
\[jako rzeczownik\] BYCIE DALEKIM  
REQU(M) – DALEKI  
REQUTU – PUSTKA, PRÓŻNIA  
REQUTA ALAKU – ODCHODZIĆ Z PUSTYMU RĘKOMA  
REQUTU(M) – DAL; PRZYSZŁOŚĆ?  
REQUSSA – W PRZYSZŁOŚCI  
REŠATU lub RIŠATU(M) – RADOWANIE SIĘ, CIESZENIE  
REŠTATU – PIERWSZORZĘDNA; POCZĄTKOWA, DAWNA  
\[jako abstrakt\] PRZESZŁOŚĆ?  
R€ŠTŰ lub AŠAREDU – GŁÓWNY, PIERWSZY, NAJLEPSZY  
ŠAMAN REŠTI – OLEJ ŚWIĘTY  
REŠTU – PIERWSZORZĘDNY; POCZĄTKOWY, DAWNY  
R€ŠU (a/i\[e\]) (r’š) – RADOWAĆ SIĘ, TRIUMFOWAĆ, ŚWIĘCIĆ TRIUMF  
\[jako rzeczownik\] RADOWANIE SIĘ, TRIUMFOWANIE  
GT RITUŠU: PROMIENIEĆ (twarz – PANU)  
\[jako rzeczownik\] PROMIENIENIE  
D RUŠU: UCIESZYĆ, CIESZYĆ (kogoś)  
\[jako rzeczownik\] UCIESZENIE, CIESZENIE  
REŠU – URADOWANY  
REŠU(M) lub RA’SUM, RAŠU – GŁOWA, ŁEB; WIERZCHOŁEK; POCZĄTEK; WYŻSZA CZĘŚĆ (budowli); NIEWOLNIK  
REŠ EQLIM – CEL  
REŠAM KULLUM – TRZYMAĆ GŁOWĘ; WSPOMAGAĆ, WSPIERAĆ  
INA REŠUŠŠU (lok-adv) – NA JEGO GŁOWIE  
ŠAREŠI – EUNUCH  
RETŰ (i) – D RUTTŰ: PRZYMOCOWAĆ (skrzydło bramy do słupa w bramie)  
\[jako rzeczownik\] PRZYMOCOWYWANIE (skrzydła bramy)  
RÎBITU lub R€BETU, R€BITU (r’b) – PLAC; PLAC CENTRALNY; BAZAR, RYNEK; OBSZAR MIASTA; PRZEDMIEŚCIE,  
OSADA PODMIEJSKA; OKRĘG  
l.mn. R€BÂTI  
RIBU – NAMIASTKA, SUROGAT, MATERIAŁ ZASTĘPCZY  
RÎCU lub R€CU, RÂ’ICU (rwc) – POMOCNIK  
RÎCŰTU lub R€CŰTU – POMOC  
RIDU – PRZYWÓDZTWO, KIEROWNICTWO  
RIDU – PĘDZIĆ, GNAĆ; PROWADZIĆ; KIEROWAĆ  
\[jako rzeczownik\] PĘDZENIE, GNANIE; PROWADZENIE; KIEROWANIE  
RIDŰTU – DZIEDZICTWO; NASTĘPSTWO TRONU  
BÎT RIDŰTI – DOM NASTĘPSTWA TRONU \[harem? kancelaria?\]  
MAR ŠARRI ŠA BIT RIDŰTI – NASTĘPCA TRONU  
RIDŰTU – PROCESJA; ŚCIGANIE, POGOŃ; OBSZAR PAŃSTWA, OBSZAR PANOWANIA, ZASIĘG PANOWANIA  
RIDUTU – WYTWÓR, PŁÓD  
RIGMU(M) – LAMENT, BIADANIE; HAŁAS, WRZASK, ŁOMOT; GŁOS  
RIHCU – ULEWA, DESZCZ  
RIHCUM – POWÓDŹ  
RIHICTU – POWÓDŹ \[r.ż.\]  
RIHTU lub REHTU – RESZTA, POZOSTAŁOŚĆ \[r.ż.\]  
RIKISTU – ZAŁATWIENIE WEDŁUG UMOWY  
RIKSATU(M) – UMOWA \[Amm.\]  
RIKSU lub BUANU – STAW; ŚCIĘGNO  
RIKSU – PRZYGOTOWANIE (kultowe); STAW, PRZEGUB; zob. BUANU  
RIMITU – MIESZKANIE  
RIMU – TUR; \[Lipin\] DZIKI BYK  
RIQQU lub RIQU – ZIOŁA PRZYPRAWOWE, PRZYPRAWA ZIOŁOWA; (ROŚLINA PERFUMOWA; WIECZNIE ZIELONA  
ROŚLINA); \[Lipin\] WONNE DREWNO lub ZIOŁA  
RIQU lub RIQQU – j.w.  
RIQU(M) – PUSTY  
RISIBTU lub RISIPTU – BÓJ, WALKA; BÓJKA, BIJATYKA  
RISIPTU lub RISIBTU – j.w.  
RIŠATU(M) \[l.mn.\], RIŠATU(M) lub REŠATU – RADOWANIE SIĘ, CIESZENIE  
RITPAŠU – SZEROKO ROZCIĄGNIĘTY  
RITTU – RĘKA; KOŃCZYNA \[r.ż.\]  
RITU – PASTWISKO \[r.ż.\]  
RŰ lub RU’U (r’) – BLISKI, PRZYJACIEL, TOWARZYSZ  
r.ż. RUTTU, l.mn. RU’ATU  
RU’A lub RUA – TOWARZYSZ, KOMPAN  
RU’AMU(M) – WDZIĘK, POWAB, UROK  
RU’TU lub RUTU – ŚLINA \[r.ż.\]  
RU’U lub RŰ (r’) – BLISKI, PRZYJACIEL, TOWARZYSZ  
r.ż. RUTTU, l.mn. RU’ATU  
RUA lub RU’A – TOWARZYSZ, KOMPAN  
RUBATU – KSIĘŻNA  
RUBBŰ(M), RUBBŰ(M) – UCZYNIONY DUŻYM, WIELKIM; WYCHOWANY; WYHODOWANY; PODNIESIONY; WYNIESIO-  
NY, WYWYŻSZONY  
RUBCU – GNÓJ, NAWÓZ, MIERZWA; \[Lipin\] PRZEGRODA; ZAGRODA, CHLEW  
RUBŰ(M) – KSIĄŻĘ; WIELMOŻA  
RUBŰTU(M) – WŁADZA KSIĄŻĘCA; DOSTOJEŃSTWO, WIELKOŚĆ; KSIĄŻĘ  
RUDDŰ – D: DODAWAĆ, DOŁĄCZAĆ  
\[jako rzeczownik\] DODAWANIE, DOŁĄCZANIE  
RUDDŰ – DODANY, DOŁĄCZONY  
RUGGUBU – D: KŁAŚĆ BELKI STROPOWE; PODZIELIĆ NA PIĘTRA (budynek) lub POKŁADY (arkę) \[D\]  
\[jako rzeczownik\] KŁADZENIE BELEK STROPOWYCH; DZIELENIE NA PIĘTRA, POKŁADY  
RUGGUBU – WYPOSAŻONY W BELKI STROPOWE; PODZIELONY NA PIĘTRA, POKŁADY  
RUGGUGU – BEZPRAWIE (poet.) (Missbraucht)  
RUGUMMŰ – SKARGA POŁĄCZONA Z ROSZCZENIEM  
RUHŰ – UPIÓR, WIDMO, ZJAWA; CZARY  
RUKKUBU(M) – ZAPŁODNIONY  
RUKKUSU – ZOBOWIĄZANY  
RUKUBU – WÓZ  
RUMMŰ – UMORZONY; UWOLNIONY; TRAKTOWANY Z POBŁAŻANIEM; (poet.) TEN, KTÓREMU PRZEBACZONO  
RUPŠU – SZEROKOŚĆ  
RUQQU lub RŰQU (r’q) – DAL, ODLEGŁOŚĆ (zarówno w czasie jak i w przestrzeni); DALEKI, ODDALONY; DAWNY \[sum.  
SU\]  
l.mn. RUQŰTI, r.ż. RŰQTU, RŰQATU, l.mn.r.ż. RŰQÂTU, RŰQETI  
ULTU ŰM€ RŰQŰTE – OD DAWNA, OD DAWIEN DAWNA  
RUQTU – DAL, ODDALENIE  
RUQU lub RUQQU (r’q) – DAL, ODLEGŁOŚĆ (zarówno w czasie jak i w przestrzeni); DALEKI, ODDALONY; DAWNY \[sum.  
SU\]  
l.mn. RUQUTI, r.ż. RUQTU, RUQATU, l.mn.r.ż. RUQATU, RUQQETI  
ULTU UME RUQUTE – OD DAWNA, OD DAWIEN DAWNA  
RUŠŠŰ – LŚNIĄCY JASNO; WSPANIAŁY, ŚWIETNY, WYBORNY  
RUŠŠU – CZERWONY  
RUŠŰ – CZARY  
RUŠU – UCIESZONY  
RUTTŰ – PRZYMOCOWANY (o skrzydle bramy)  
RUTU lub RU’TU – ŚLINA \[r.ż.\]  
RUŢŢUBU – D: ZWILŻAĆ, NAWILŻAĆ, ZMOCZYĆ  
\[jako rzeczownik\] ZWILŻANIE, NAWILŻANIE, ZMACZANIE  
RUŢŢUBU – ZWILŻONY, NAWILŻONY, ZMOCZONY

S  
SA’IRU – TAŃCZĄCY, SKACZĄCY, PLĄSAJĄCY; MIOTAJĄCY SIĘ, SZALEJĄCY; ROZBIJAJĄCY SIĘ, AWANTURUJĄCY SIĘ;  
AWANTURNIK? SZLENIEC?  
SA’ITU – ZOSTAWIAJĄCY; POZOSTAWIAJĄCY (jako resztę); DOPUSZCZAJĄCY  
SABBIU lub SABI’U, SABITU, (SEBI’U) – SZYNK, KARCZMA; SZYNKARKA  
SABI’U lub SABBIU, SABITU, (SEBI’U) – j.w.  
SABITU lub SABI’U, SABBIU, (SEBI’U) – j.w.  
SABITU(M) – SZYNKARKA  
SADARU (a) (a/u) (i) – STAWIAĆ W RZĘDZIE, USTAWIAĆ W RZĘDZIE; NAWLEKAĆ, NANIZAĆ; ZAKSIĘGOWAĆ  
\[jako rzeczownik\] STAWIANIE W RZĘDZIE; NAWLEKANIE, NIZANIE; KSIĘGOWANIE  
KASPA SADARU – ZAKSIĘGOWAĆ  
SADIRU – USTAWIAJĄCY W RZĘDZIE; NAWLEKAJĄCY; KSIĘGUJĄCY  
SADRIŠ – W RZĘDZIE, PO KOLEI; STALE, REGULARNIE  
SADRU – USTAWIONY W RZĘDZIE; NAWLECZONY, NANIZANY; ZAKSIĘGOWANY  
SADU (a) – ZABIJAĆ (?)  
\[jako rzeczownik\] ZABIJANIE (?)  
SAGŰ – ORKIESTRA \[sum.\]  
SAHALU (a/u) – PRZEBIJAĆ  
\[jako rzeczownik\] PRZEBIJANIE  
SAHAPU (a/u) – RZUCAĆ, POWALIĆ  
\[jako rzeczownik\] RZUCANIE, POWALANIE  
SAHARU(M), SAHARU(M) (u) (a/u) (i) lub ŠAHARU, SAH€RU – OBRACAĆ SIĘ; OBRACAĆ; KRĘCIĆ SIĘ, ZAKRĘCIĆ  
SIĘ; PORUSZAĆ SIĘ PO OKRĘGU; OKRĄŻAĆ, OTACZAĆ; TROSZCZYĆ SIĘ (o – Akk.), ZAJMOWAĆ SIĘ (z  
Akk.); SZUKAĆ, POSZUKIWAĆ; ODWIEDZAĆ; ZWRÓCIĆ SIĘ (do kogoś); POWTÓRZYĆ; ZAJMOWAĆ SIĘ  
CZARAMI, CZAROWAĆ; ROBIĆ (coś) PO RAZ WTÓRY lub STALE  
\[jako rzeczownik\] OBRACANIE SIĘ; OBRACANIE; KRĘCENIE SIĘ, ZAKRĘCANIE SIĘ; PORUSZANIE SIĘ  
PO OKRĘGU; OKRĄŻANIE, OTACZANIE; TROSZCZENIE SIĘ, ZAJMOWANIE SIĘ; SZUKANIE, POSZU-  
KIWANIE; ODWIEDZANIE; ZWRACANIE SIĘ; POWTARZANIE; ZAJMOWANIE SIĘ CZARAMI, CZA-  
ROWANIE; ROBIENIE CZEGOŚ POWTÓRNIE lub STALE  
N NASHURU(M), NASHURU(M): OBRÓCIĆ SIĘ, PRZEKRĘCIĆ SIĘ  
\[jako rzeczownik\] OBRÓCENIE SIĘ, PRZEKRĘCENIE SIĘ  
NTN ITTASHURU(M), ITTASHURU(M): jak G ze znaczeniem częstotliwościowym  
D SUHHURU(M), SUHHURU(M): OBRACAĆ, ODWRACAĆ; ODEPRZEĆ  
\[jako rzeczownik\] OBRACANIE, ODWRACANIE; ODPARCIE  
Š ŠUSHURU(M), ŠUSHURU(M): POBUDZAĆ, SKŁANIAĆ; OBEJMOWAĆ, OTACZAĆ  
\[jako rzeczownik\] POBUDZANIE, SKŁANIANIE; OBEJMOWANIE, OTACZANIE  
ŠT ŠUTASHURU(M), ŠUTASHURU(M): OKRĄŻAĆ; BYĆ OKRĄŻONYM, ZNALEŹĆ SIĘ W OKRĄŻENIU;  
OBJĄĆ, OGARNĄĆ  
\[jako rzeczownik\] OKRĄŻANIE; BYCIE OKRĄŻONYM, ZNALEZIENIE SIĘ W OKRĄŻENIU; OBJĘCIE,  
OGARNIĘCIE  
ARKATA ŠUHHURU – RZUCAĆ SIĘ DO UCIECZKI  
ANA IDÂ SAHÂRU – OBRÓCIĆ SIĘ W (czyjąś) STRONĘ; WALCZYĆ RAZEM Z (kimś)  
IDA x SAHARU – STANĄĆ PO STRONIE x  
SAH€RU lub SAHARU(M), ŠAHARU – j.w.  
SAHHU – (m.in.) ŁĄKA  
SAHILU – PRZEBIJAJĄCY  
SAHIPU – RZUCAJĄCY, POWALAJĄCY  
SAHIRU(M), SAHIRU(M) – OBRACAJĄCY SIĘ; OBRACAJĄCY; KRĘCĄCY SIĘ, ZAKRĘCAJĄCY SIĘ; PORUSZAJĄCY SIĘ PO  
OKRĘGU; OKRĄŻAJĄCY, OTACZAJĄCY; TROSZCZĄCY SIĘ (o – Akk.), ZAJMUJĄCY SIĘ; SZUKAJĄCY , SZUKA-  
JĄCY; ODWIEDZAJĄCY; ZWRACAJĄCY SIĘ (do kogoś); POWTARZAJĄCY; ZAJMUJĄCY SIĘ CZARAMI, CZA-  
RUJĄCY; ROBIĄCY(coś) PO RAZ WTÓRY lub STALE  
SAHLU – PRZEBITY  
SAHPU – RZUCONY, POWALONY  
SAHRU(M), SAHRU(M) – OBRÓCONY;ZAKRĘCONY; OKRĄŻONY, OTOCZONY; ODSZUKANY; ODWIEDZONY; POWTÓ-  
RZONY;ZACZAROWANY; ROBIONY PO RAZ WTÓRY lub STALE  
SAKALU (i) – (niesłusznie) NABYWAĆ, ZDOBYWAĆ  
\[jako rzeczownik\] NABYWANIE, ZDOBYWANIE  
SAKAPU (i) – UPADAĆ, PRZEWRACAĆ SIĘ, RUNĄĆ  
\[jako rzeczownik\] UPADANIE, PRZEWRACANIE SIĘ  
SAKBU, SAKBŰ – AWANGARDA, STRAŻ PRZEDNIA; PATROL?  
SAKILU – (niesłusznie, bezprawnie) NABYWAJĄCY, ZDOBYWAJĄCY  
SAKIPU – UPADAJĄCY, PRZEWRACAJĄCY SIĘ  
SAKKŰ – ZWYCZAJ KULTOWY, REGUŁA ZWIĄZANA Z KULTEM, RYTUAŁ \[sum.\]  
SAKLU – (niesłusznie, bezprawnie) NABYTY, ZDOBYTY  
SAKPU – UPADŁY, PRZEWRÓCONY  
SALAHU (a/u) – SKRAPIAĆ, POKROPIĆ  
\[jako rzeczownik\] SKRAPIANIE  
SALAMU (i) – BYĆ POJEDNANYM, POGODZONYM  
\[jako rzeczownik\] BYCIE POJEDNANYM, POGODZONYM  
SALATU – RODZINA, KREWNI  
SALHU – SKROPIONY, POKROPIONY  
SALIHU – SKRAPIAJĄCY, KROPIĄCY  
SALIMU(M) – PROPOZYCJA POKOJU; ZAWARCIE POKOJU; SOJUSZ, PRZYMIERZE  
BEL SALIMI – SOJUSZNIK, SPRZYMIERZENIEC  
SALMU – POJEDNANY, POGODZONY  
SALŰ \[asyr.\] – ZRZUCIĆ  
\[jako rzeczownik\] ZRZUCENIE  
NIR BELŰTI SALŰ – ZRZUCIĆ JARZMO PANOWANIA, WYZWOLIĆ SIĘ; podobnie  
NIR ELI SALŰ – ZRZUCIĆ BOSKIE JARZMO  
SALŰ – ZRZUCONY  
SALŰ – ZRZUCAJĄCY  
\[SALŰ\] – zob. SULLU  
SAMANEŠER – OSIEMNAŚCIE  
r.ż. SAMANEŠERET  
SAMANTU lub SAMANU, ŠAMANU, ŠAMANTU – OSIEM  
SAMANU, SAMANŰM lub ŠAMANU, ŠAMANTU, SAMANTU – j.w.  
r.ż. SAMANUTUM  
SAMANŰ lub ŠAMANŰ, ŠAMANUTU, SAMANUTU – ÓSMY  
SAMANUTU lub SAMANŰ, ŠAMANŰ, ŠAMANUTU – j.w.  
SAMÂRU (u) (i) lub SAWÂRU, ŠAMÂRU, SÂRU, ŠÂRU – TAŃCOWAĆ, SKAKAĆ, PLĄSAĆ; MIOTAĆ SIĘ, SZALEĆ;  
ROZBIJAĆ SIĘ, AWANTUROWAĆ SIĘ  
\[jako rzeczownik\] TAŃCOWANIE, SKAKANIE, PLĄSANIE; MIOTANIE SIĘ, SZALENIE; ROZBIJANIE SIĘ,  
AWANTUROWANIE SIĘ  
SAMIRU – TAŃCZĄCY, SKACZĄCY, PLĄSAJĄCY; MIOTAJĄCY SIĘ, SZALEJĄCY; ROZBIJAJĄCY SIĘ, AWANTURUJĄCY SIĘ;  
AWANTURNIK? SZLENIEC?  
SAMNUM – ÓSMY  
r.ż. SAMUNTUM  
SAMSUDITANA – SAMSUDITANA \[imię króla Babilonu\] \[zachodniosemickie\]  
SAMSUILUNA – SAMSUILUNA \[imię króla Babilonu\] \[zachodniosemickie\]  
SAMU – \[składnik zwrotu\]  
SAM WETU – MUR ZEWNĘTRZNY  
SAMU – CZERWONY; \[Lipin\] CIEMNOCZERWONY; BRUNATNY  
SÂMU(M) (sim) – BYĆ CZERWONYM, BYĆ BRUNATNYM  
\[jako rzeczownik\] BYCIE CZERWONYM, BYCIE BRUNATNYM  
SANAQU(M) (i) – (nieprzech.) PRZYBLIŻYĆ SIĘ, ZBLIŻYĆ SIĘ; PRZYBYWAĆ, NADCHODZIĆ; (przech.) PRZYWIĄZY-  
WAĆ, UWIĄZYWAĆ; PORZĄDKOWAĆ; BADAĆ, SPRAWDZAĆ (PONOWNIE); BYĆ CIASNYM  
\[jako rzeczownik\] PRZYBLIŻANIE SIĘ, ZBLIŻANIE SIĘ; PRZYBYWANIE, NADCHODZENIE; PRZYWIĄ-  
ZYWANIE, UWIĄZYWANIE; PORZĄDKOWANIE; BADANIE, SPRAWDZANIE (PONOWNE); BYCIE  
CIASNYM  
D SUNNUQU(M): ŚCIEŚNIAĆ, ZAWĘŻAĆ; KONTROLOWAĆ, BADAĆ  
\[jako rzeczownik\] ŚCIEŚNIANIE, ZAWĘŻANIE; KONTROLOWANIE, BADANIE  
SIS€ CINDEŠU SANAQU – ZAPRZĘGAĆ KONIA  
SANIQU(M) – PRZYBLIŻAJĄCY SIĘ, ZBLIŻAJĄCY SIĘ; PRZYBYWAJĄCY, NADCHODZĄCY; PRZYWIĄZUJĄCY, UWIĄZUJĄ-  
CY; PORZĄDKUJĄCY; BADAJĄCY, SPRAWDZAJĄCY (PONOWNIE)  
SANQU(M) – ZBLIŻONY, PRZYBYŁY, NADESZŁY; PRZYWIĄZANY, UWIĄZANY; UPORZĄDKOWANY; ZBADANY, SPRAW-  
DZONY (PONOWNIE); CIASNY  
SAPADU (i) – SMUCIĆ SIĘ, BYĆ SMUTNYM  
\[jako rzeczownik\] SMUCENIE SIĘ, BYCIE SMUTNYM  
SAPAHU(M) (a/u) – ROZPRASZAĆ; ROZPUSZCZAĆ; ROZGROMIĆ (armię), NIWECZYĆ; SPRZEDAWAĆ ZA BEZCEN  
(dom, sprzęty domowe); UDAREMNIAĆ, OBRACAĆ W NIWECZ (urok); NIE DOJŚĆ DO SKUTKU  
\[jako rzeczownik\] ROZPRASZANIE; ROZPUSZCZANIE; GROMIENIE (armii), NIWECZENIE; SPRZEDAWA-  
NIE ZA BEZCEN; UDAREMNIANIE, OBRACANIE W NIWECZ (uroku); NIE DOJŚCIE DO SKUTKU  
N NASPUHU(M): BYĆ ROZPROSZONYM; BYĆ ROZWIĄZANYM; BYĆ ZNISZCZONYM  
\[jako rzeczownik\] BYCIE ROZPROSZONYM; BYCIE ROZWIĄZANYM; BYCIE ZNISZCZONYM  
D SUPPUHU(M): ROZPRASZAĆ  
\[jako rzeczownik\] ROZPRASZANIE  
SAPANU (a/u) – ZRÓWNYWAĆ; ZABIJAĆ; ŚCINAĆ (drzewo)  
\[jako rzeczownik\] ZRÓWNYWANIE; ZABIJANIE; ŚCINANIE (drzewa)  
SAPARU – SIEĆ \[sum.\]  
SAPDU – SMUTNY  
SAPHU(M) – ROZPROSZONY; ROZPUSZCZONY; ROZGROMIONY (o armii), ZNIWECZONY; SPRZEDANY ZA BEZCEN;  
UDAREMNIONY, ODCZYNIONY (urok); NIEDOSZŁY DO SKUTKU  
SAPIHU(M) – ROZPRASZAJĄCY; ROZPUSZCZAJĄCY; GROMIĄCY, NIWECZĄCY; SPRZEDAJĄCY ZA BEZCEN; UDAREM-  
NIAJĄCY, OBRACAJĄCY W NIWECZ; NIE DOCHODZĄCY DO SKUTKU  
SAPINU – ZRÓWNUJĄCY; ZABIJAJĄCY; ŚCINAJĄCY (drzewo); DRWAL?  
SAPNU – ZRÓWNANY; ZABITY; ŚCIĘTY (o drzewie)  
\[SAPŰ\] – zob. SUPPU  
SAQARU (a/u) lub ZAKARU, ŠAQARU – MÓWIĆ, POWIEDZIEĆ; WZYWAĆ, WOŁAĆ; MODLIĆ SIĘ; POROZUMIEĆ SIĘ,  
DOGADAĆ SIĘ; NAZYWAĆ; PRZYSIĘGAĆ, ZAKLINAĆ SIĘ; DOKONAĆ, WYWOŁAĆ, STWORZYĆ  
\[jako rzeczownik\] MÓWIENIE, POWIEDZENIE; WZYWANIE, WOŁANIE; MODLENIE SIĘ; POROZUMIE-  
WANIE SIĘ, DOGADYWANIE SIĘ; NAZYWANIE; PRZYSIĘGANIE, ZAKLINANIE SIĘ; DOKONANIE, WY  
WOŁYWANIE, TWORZENIE  
GT SITQURU: POROZUMIEWAĆ SIĘ; MÓWIĆ, INFORMOWAĆ, POWIADOMIĆ  
\[jako rzeczownik\] POROZUMIEWANIE SIĘ; MÓWIENIE, INFORMOWANIE, POWIADAMIANIE  
Š ŠUSQURU: KAZAĆ MÓWIĆ, ZMUSIĆ DO MÓWIENIA; RZUCIĆ (klątwę); ZAPRZYSIĄC, KAZAĆ ZŁO-  
ŻYĆ PRZYSIĘGĘ (z ADU, NIŠ QATI, NIŠ ILANI)  
\[jako rzeczownik\] NAKAZANIE MÓWIENIA, ZMUSZENIE DO MÓWIENIA; RZUCANIE (klątwy); ZAPRZY-  
SIĘGANIE, NAKAZANIE ZŁOŻENIA PRZYSIĘGI  
SAQIRU – MÓWIĄCY; WZYWAJĄCY, WOŁAJĄCY;MODLĄCY SIĘ; POROZUMIEWAJĄCY SIĘ, DOGADUJĄCY SIĘ; NAZYWA-  
JĄCY; PRZYSIĘGAJĄCY, ZAKLINAJĄCY SIĘ; DOKONUJĄCY, WYWOŁUJĄCY, TWORZĄCY  
SAQRU – WYPOWIEDZIANY; WEZWANY, ZAWOŁANY; WYMODLONY; DOGADANY; NAZWANY; ZAPRZYSIĘŻONY; DOKO-  
NANY, WYWOŁANY, TWORZONY  
ŠAQQŰRUM – PIJAK, PIJANICA  
SARAQU (a/u) – SKŁADAĆ OFIARĘ (nalewając lub nasypując)  
\[jako rzeczownik\] SKŁADANIE OFIARY (przez nalewanie lub nasypanie)  
SARARU – BYĆ KŁAMLIWYM  
\[jako rzeczownik\] BYCIE KŁAMLIWYM  
D SURRURU: DOPUŚCIĆ SIĘ WIAROŁOMSTWA  
\[jako rzeczownik\] DOPUSZCZENIE SIĘ WIAROŁOMSTWA  
SARIQU – SKŁADAJĄCY OFIARĘ (przez nalanie czy nasypanie czegoś)  
SARQU – OFIAROWANY, ZŁOŻONY JAKO OFIARA (przez nalanie czy nasypanie)  
SARRATU lub SARTU – KŁAMSTWO, FAŁSZ  
SARRU – KŁAMCA, OSZUST; KŁAMLIWY  
SARTU lub SARRATU – KŁAMSTWO, FAŁSZ  
SARU – WACHLARZ \[l.mn.\]  
SÂRU (u) (i) lub SAMÂRU, ŠAMÂRU, SAWÂRU, ŠÂRU – TAŃCOWAĆ, SKAKAĆ, PLĄSAĆ; MIOTAĆ SIĘ, SZALEĆ;  
ROZBIJAĆ SIĘ, AWANTUROWAĆ SIĘ  
\[jako rzeczownik\] TAŃCOWANIE, SKAKANIE, PLĄSANIE; MIOTANIE SIĘ, SZALENIE; ROZBIJANIE SIĘ,  
AWANTUROWANIE SIĘ  
SÂRU(M) (sur) – TAŃCZYĆ, SZALEĆ  
\[jako rzeczownik\] TAŃCZENIE, SZALENIE  
SASSATU – TRAWA  
SASU(M) – MÓL  
SATTUKU – REGULARNE OFIARY \[sum.\]  
SÂTU (a/i) (sjt) – ZOSTAWIAĆ; POZOSTAWIAĆ (jako resztę); DOPUSZCZAĆ  
\[jako rzeczownik\] ZOSTAWIANIE; POZOSTAWIANIE (jako reszty); DOPUSZCZANIE  
SATU – ZOSTAWIONY; POZOSTAWIONY (jako reszta); DOPUSZCZONY  
SAWÂRU (u) (i) lub SAMÂRU, ŠAMÂRU, SÂRU, ŠÂRU – TAŃCOWAĆ, SKAKAĆ, PLĄSAĆ; MIOTAĆ SIĘ, SZALEĆ;  
ROZBIJAĆ SIĘ, AWANTUROWAĆ SIĘ  
\[jako rzeczownik\] TAŃCOWANIE, SKAKANIE, PLĄSANIE; MIOTANIE SIĘ, SZALENIE; ROZBIJANIE SIĘ,  
AWANTUROWANIE SIĘ  
SAWIRU – TAŃCZĄCY, SKACZĄCY, PLĄSAJĄCY; MIOTAJĄCY SIĘ, SZALEJĄCY; ROZBIJAJĄCY SIĘ, AWANTURUJĄCY SIĘ;  
AWANTURNIK? SZLENIEC?  
SEBEŠER – SIEDEMNAŚCIE  
SEBETTU lub SIBŰ, ŠIBŰ, SEBŰ, ŠEBŰ, ŠEBETTU – SIEDEM \[r.ż.\]  
s.a. SEBE, SEBET  
SEBI’U lub SABBIU, SABITU, SABI’U – SZYNK, KARCZMA; SZYNKARKA  
SEBŰ lub ŠEBŰ, ŠEBUTU, SEBUTU – SIÓDMY \[r.m.\]  
r.ż. SEBUTUM  
SEBŰ lub SIBŰ, ŠEBŰ, ŠIBŰ, ŠEBETTU, SEBETTU – SIEDEM \[r.m.\]  
s.a. SEBE, SEBET  
SEBUTU lub ŠEBŰ, ŠEBUTU, SEBŰ – SIÓDMY \[r.ż.\]  
SEHERTU – \[składnik zwrotu\]  
SEHERTI MATIM – OBSZAR KRAJU  
SEHŰ (i) lub SIHŰ – WYWOŁYWAĆ ZAMIESZKI, BUNTOWAĆ SIĘ, POWSTAWAĆ; SPESZYĆ SIĘ, SKONFUDOWAĆ  
SIĘ, ZMIESZAĆ SIĘ  
\[jako rzeczownik\] WYWOŁYWANIE ZAMIESZEK, BUNTOWANIE SIĘ, POWSTAWANIE; SPESZENIE SIĘ,  
KONFUDOWANIE SIĘ, ZMIESZANIE SIĘ  
N NASHŰ: BUNTOWAĆ SIĘ  
\[jako rzeczownik\] BUNTOWANIE SIĘ  
D SUHHŰ: ZDRADZAĆ; PODBURZAĆ; BUNTOWAĆ SIĘ, ZBUNTOWAĆ SIĘ, POWSTAĆ; ODPADAĆ  
\[jako rzeczownik\] ZDRADZANIE, PODBURZANIE, BUNTOWANIE SIĘ, POWSTAWANIE; ODPADANIE  
SEHŰ – ZBUNTOWANY; SPESZONY, SKONFUDOWANY, ZMIESZANY  
SEHŰ – WYWOŁUJĄCY ZAMIESZKI, BUNTUJĄCY SIĘ, POWSTAJĄCY; PESZĄCY SIĘ, ULEGAJĄCY KONFUZJI  
SEKERTU – NAŁOŻNICA  
SEKERU (i) – ZAMYKAĆ, PRZEGRADZAĆ (groblą)  
\[jako rzeczownik\] ZAMYKANIE, PRZEGRADZANIE (groblą)  
SEKIRU – ZAMYKAJĄCY, PRZEGRADZAJĄCY (groblą)  
SEKRETI – DAMY HAREMU, NAŁOŻNICE  
SEKRETU – DAMA PAŁACOWA, DWÓRKA, PANI DWORU  
SEKRU – ZAMKNIĘTY, PRZEGRODZONY (groblą)  
SEQRU – ROZKAZ, POLECENIE; IMIĘ, NAZWA  
SIBŰ lub ŠIBŰ, ŠEBŰ, SEBŰ, ŠEBETTU, SEBETTU – SIEDEM  
s.a. SEBE, SEBET  
SIDIRTU lub SIDRU – SZEREG BITEWNY  
SIDRU lub SIDIRTU – j.w.  
SIHIRTU – (CAŁY) OBWÓD; ROZLEGŁOŚĆ; CAŁOŚĆ  
ANA SIHIRTI – CAŁY  
SIHLU – KOLEC, CIERŃ  
SIHPU – PŁASZCZYZNA  
SIHIP MATATI, SIHIP DADME – KRAJ RÓWNINNY  
SIHRU – OKRĘG; OKOLICA; JAZDA KONNA PO OKRĘGU  
SIHŰ (i) lub SEHŰ – WYWOŁYWAĆ ZAMIESZKI, BUNTOWAĆ SIĘ, POWSTAWAĆ; SPESZYĆ SIĘ, SKONFUDOWAĆ  
SIĘ, ZMIESZAĆ SIĘ  
\[jako rzeczownik\] WYWOŁYWANIE ZAMIESZEK, BUNTOWANIE SIĘ, POWSTAWANIE; SPESZENIE SIĘ,  
KONFUDOWANIE SIĘ, ZMIESZANIE SIĘ  
N NASHŰ: BUNTOWAĆ SIĘ  
\[jako rzeczownik\] BUNTOWANIE SIĘ  
D SUHHŰ: ZDRADZAĆ; PODBURZAĆ; BUNTOWAĆ SIĘ, ZBUNTOWAĆ SIĘ, POWSTAĆ; ODPADAĆ  
\[jako rzeczownik\] ZDRADZANIE, PODBURZANIE, BUNTOWANIE SIĘ, POWSTAWANIE; ODPADANIE  
SIHŰ – ZBUNTOWANY; SPESZONY, SKONFUDOWANY, ZMIESZANY  
SIHŰ – WYWOŁUJĄCY ZAMIESZKI, BUNTUJĄCY SIĘ, POWSTAJĄCY; PESZĄCY SIĘ, ULEGAJĄCY KONFUZJI  
SIHU – POWSTANIE  
SIKILTU – COŚ (niesłusznie) NABYTEGO  
SIKILTAM ISAKKIL – (JEŚLI) JEST ROZRZUTNA, MARNOTRAWNA  
SIKKATU – KOŁEK; GWÓŹDŹ  
SIKKURU – RYGIEL  
SILI’TU – CHOROBA, NIEDOMAGANIE; CHERLACTWO  
SIMANU(M) – DOGODNY TERMIN, ODPOWIEDNI CZAS; SIMAN \[nazwa trzeciego miesiąca\]  
SIMTU(M) – POSTAĆ; CZŁONKI; OZDOBA; WŁASNOŚĆ, CECHA (osoby, rzeczy), NIEZBĘDNY ATRYBUT  
LA SIMATIŠU – NIENALEŻNY MU  
ANA LA SIMATIŠU – MIMO, ŻE JEST NIEGODZIEN  
SIN – SIN \[imię boga\]; KSIĘŻYC  
SINNIŠ lub SINNIŠTU – KOBIECY, ŻEŃSKI; ŻONA  
SINNIŠTU lub SINNIŠ – j.w.  
SINNIŠTU(M) – KOBIETA  
ZIKAR SINIŠ – MĘŻCZYZNA I KOBIETA = WSZYSCY  
SINNU – ZĄB; KOŚĆ SŁONIOWA  
\[SINŠER\] lub \[SINŠERET\] – DWANAŚCIE  
\[SINŠERET\] lub \[SINŠER\] – j.w.  
SINUNTU – JASKÓŁKA  
SIPARRU – BRĄZ \[sum.\]  
SIPDU – SMUTEK; ŻAŁOBA  
SIPPAR – SIPPAR \[miasto w środk. Mezopotamii\]  
SIPPAR-JAHRURUM – CZĘŚĆ MIASTA SIPPAR  
SIPPU(M) – BOK DRZWI; SIPPUM \[termin architektoniczny oznaczający umieszczone symetrycznie po obu stronach bramy  
części budynku, w których m.in. obracają się skrzydła wrót\]  
SIQRU(M) – POLECENIE, ROZKAZ  
SIREŠ – (być może) MOSZCZ, WINO \[słowo obcego pochodzenia\]  
SIRIJAM – PANCERZ (ze skóry) \[słowo obcego pochodzenia\]  
SIRQU lub SURQINNU – OFIARA (rozrzucona lub rozlana, nalana)  
SIRRIMU – DZIKI OSIOŁ  
SISSIKTU – RĄBEK SZATY \[sum.\]  
SISSINNU – KŁOS DAKTYLA  
SISŰ – KOŃ  
SISE PITHALLI – WIERZCHOWIEC  
SITTATU – POZOSTAŁE; zob. SITU lub SITTU  
SITTU lub SITU – (rzeczownik) RESZTA, POZOSTAŁOŚĆ  
l.mn. SITTUTU, SITTATU  
SITTUTU – POZOSTALI; zob. SITU lub SITTU  
SITU lub SITTU – (rzeczownik) RESZTA, POZOSTAŁOŚĆ  
l.mn. SITTUTU, SITTATU  
SUALU – KASZEL  
SUDINNU – SOWA? \[jakiś ptak gnieżdżący się w wąwozach i rozpadlinach\], synonim GILGILDANU?; NIETOPERZ \[sum.\]  
SUGULLU – STADO BYDŁA  
l.mn. SUGULLATU  
SUHHŰ – ZDRADZONY; PODBURZANY; ZBUNTOWANY; ODPADŁY  
SUHHURTU – OBRÓCENIE, DOPROWADZENIE DO OBRÓCENIA; UPADEK, KLĘSKA  
SUHHURU(M), SUHHURU(M) – OBRÓCONY, ODWRÓCONY; ODPARTY  
SUKALLU lub SUKKALLU – SUKALLU \[jeden z najwyższych urzędników pałacowych\] \[sum.\]  
SUKKALLU lub SUKALLU – j.w.  
SUKKU – ŚWIĄTYNIA  
SUKKUKU lub ŢUMMUMU – GŁUCHY  
SULLŰ lub SULŰ – ULICA; DROGA \[sum.\]  
SULLU – D: PROSIĆ, BŁAGAĆ  
\[jako rzeczownik\] PROSZENIE, BŁAGANIE  
SULLU – WYPROSZONY, WYBŁAGANY?  
SULŰ lub SULLŰ – ULICA; DROGA  
SULUPPU(M) – DAKTYL, DAKTYLE \[sum.\]  
SUMMATU lub SUMMU – GOŁĄB  
SUMMU lub SUMMATU – j.w.  
SUMU(M) – CZERWONA PLAMA; CZERWIEŃ  
SUNNUQU(M) – ŚCIEŚNIONY, ZAWĘŻONY; SKONTROLOWANY, ZBADANY  
SUNQU(M) – BIEDA  
SUNU(M) – ŁONO; BIODRO; UDO; NOGA  
SUPPU – D: MODLIĆ SIĘ, BŁAGAĆ  
\[jako rzeczownik\] MODLENIE SIĘ, BŁAGANIE  
SUPPU – WYMODLONY, WYBŁAGANY?  
SUPPUHU(M) – ROZPROSZONY  
SUPURU – PŁOT; OBMUROWANIE (w eposie o Gilgameszu epitet dotyczący miasta Uruk)  
SURRURU – WIAROŁOMNY?  
SUQU – ULICA; DROGA  
SUQU RAPŠU – SZEROKA ULICA, PLAC  
SŰQUM – D: NALEGAĆ, NACISKAĆ; DOSKWIERAĆ  
\[jako rzeczownik\] NALEGANIE, NACISKANIE; DOSKWIERANIE  
SŰQUM – NACISKANY, PODDANY NALEGANIOM, PRESJI, PRZEŚLADOWANY  
SURDU – SOKÓŁ  
SURQINNU lub SIRQU – OFIARA (rozrzucona lub rozlana, nalana)  
SURRATU – KŁAMSTWO  
SURRIŠ – NAGLE, RAPTOWNIE; „TYLKO OKA MGNIENIE”  
SUSSULU – KOSZ, CEBER

Š

-Š lub –ŠU – JEGO \[sufigowany zaimek osobowy 3 osoby rodzaju męskiego\]; zob. też -ŠA  
Odmiana:  
-ŠU, -Š – JEGO -ŠUNU, -ŠUN – ICH  
-ŠU(M) – -ŠUNUŠI(M), ŠUNUTI  
-ŠU, -Š -ŠINATI, -ŠINA  
ŠA – TEN KTÓRY, TEN CO; TA KTÓRA, TA CO; TO CO, TO KTÓRE; TEN, TA, TO; KTÓRY, KTÓRA; RZECZ, PRZED-  
MIOT, COŚ  
ŠA AKÂLIM – COŚ DO JEDZENIA, POTRAWA  
ŠA ŠADÂDI – WÓZ, FURMANKA; LEKTYKA  
ŠA ILI – COŚ, CO NALEŻY DO BOGA (do świątyni)  
ŠA €KALLI – COŚ, CO NALEŻY DO PAŁACU (do króla, państwa)  
ŠA LA – POZA  
-ŠA – JEJ \[zaimek sufigowany 3 osoby rodzaju żeńskiego\]  
Odmiana: JEJ ICH  
l.p. G. -ŠA lub -Š l.mn. -ŠINA lub -ŠIN  
D. -ŠI(M) -ŠINAŠI(M) lub -ŠINATI  
A. -ŠI lub -Š -ŠINATI lub -ŠINA  
\[ŠA’ALU\] lub \[ŠELU\] – zob. ŠU”ULU  
ŠA’ALU(M) lub ŠÂLU(M) (a) – PYTAĆ (o – Akk.); DOPYTYWAĆ SIĘ; WYPRASZAĆ, UPRASZAĆ, PROSIĆ  
\[jako rzeczownik\] PYTANIE; DOPYTYWANIE SIĘ; WYPRASZANIE, UPRASZANIE, PROSZENIE  
GTN ŠITA’’ULU(M): PYTAĆ WCIĄŻ NA NOWO, RAZ PO RAZ  
\[jako rzeczownik\] PYTANIE WCIĄŻ NA NOWO, DOPYTYWANIE SIĘ NIEUSTANNE  
GT ŠITULU(M)  
DI’ATAM (DATAM) ŠÂLUM – DOWIADYWAĆ SIĘ O  
ŠULUM x ŠA’ALU – DOWIADYWAĆ SIĘ O ZDROWIE, SAMOPOCZUCIE x  
ŠA’AMANU lub ŠAJAMANU – NABYWCA, KUPUJĄCY  
ŠA’ILU(M) – PYTAJĄCY  
ŠA’ILU – TŁUMACZ SNÓW  
ŠA’IMU(M) – KUPUJĄCY, NABYWAJĄCY  
ŠA’IMU – OKREŚLAJĄCY, USTALAJĄCY  
ŠA’INU – SZCZAJĄCY? SIKAJĄCY?  
ŠA’IRU – TAŃCZĄCY, SKACZĄCY, PLĄSAJĄCY; MIOTAJĄCY SIĘ, SZALEJĄCY; ROZBIJAJĄCY SIĘ, AWANTURUJĄCY SIĘ;  
AWANTURNIK? SZLENIEC?  
ŠA’IRU – POJMUJĄCY, ROZUMIEJĄCY  
ŠA’IŢU – CIĄGNĄCY, WLOKĄCY; STARAJĄCY DOWLEC SIĘ  
ŠA’U (u) – LATAĆ  
\[jako rzeczownik\] LATANIE  
ŠAB’U(M) – SYTY; ZADOWOLONY  
ŠABA’U(M) lub ŠEBŰ (M) (i) (šb’) – BYĆ SYTYM; BYĆ ZADOWOLONYM (z – Akk.); NASYCAĆ SIĘ  
\[jako rzeczownik\] BYCIE SYTYM, BYCIE ZADOWOLONYM; SYCENIE SIĘ  
D ŠUBBU’U(M): NASYCAĆ, SYCIĆ, OFIAROWAĆ BY NASYCIĆ; ZADAWALAĆ  
\[jako rzeczownik\] NASYCANIE, SYCENIE, OFIAROWANIE BY NASYCIĆ; ZADAWALANIE  
DT ŠUTAŠBU’U(M): BYĆ NASYCONYM; BYĆ ZADOWOLONYM, ZADAWALAĆ SIĘ  
\[jako rzeczownik\] BYCIE NASYCONYM; BYCIE ZADOWOLONYM, ZADAWALANIE SIĘ  
ŠABARU lub ŠEBERU (i) – STŁUC, ROZBIĆ  
\[jako rzeczownik\] STŁUCZENIE, ROZBICIE  
ŠABASU(M) (a/u) – BYĆ ZŁYM; GNIEWAĆ SIĘ, ODWRACAĆ SIĘ (od kogoś) \[tylko St. ŠABUS\]  
\[jako rzeczownik\] BYCIE ZŁYM; GNIEWANIE SIĘ, ODWRACANIE SIĘ (od kogoś)  
ŠABAŠU (a/u) (u) – ROZGNIATAĆ W RĘCE, ZGNIATAĆ W RĘCE  
\[jako rzeczownik\] ROZGNIATANIE W RĘCE, ZGNIATANIE W RĘCE  
ŠABI’U(M) – SYCĄCY SIĘ  
ŠABIRU – TŁUKĄCY, ROZBIJAJĄCY  
ŠABIŠU – ROZGNIATAJĄCY W RĘCE, ZGNIATAJĄCY W RĘCE  
ŠABRU – STŁUCZONY, ROZBITY  
ŠABSU – ODWRÓCONY, ZAGNIEWANY  
ŠABŠU – ROZGNIECIONY W RĘCE, ZGNIECIONY W RĘCE  
ŠABULU(M) – SUCHY; WYSUSZONY, WYSCHNIĘTY  
ŠADADU (a/u) – CIĄGNĄĆ, WLEC; CIĄGNĄĆ SIĘ; MIEĆ WYROZUMIAŁOŚĆ, CIERPLIWOŚĆ  
\[jako rzeczownik\] CIĄGNIĘCIE, WLECZENIE; CIĄGNIĘCIE SIĘ; BYCIE WYROZUMIAŁYM, CIERPLIWYM  
ŠADAHU (i) – IŚĆ, PRZECIĄGAĆ (o procesji świątecznej)  
\[jako rzeczownik\] KROCZENIE, PRZECIĄGANIE (o procesji)  
ŠADALU (i) – BYĆ SZEROKIM  
\[jako rzeczownik\] BYCIE SZEROKIM  
ŠADANU – DROGOCENNY KAMIEŃ  
ŠADDŰ lub ŠADŰ – GÓRA; KRAJ GÓRSKI; WSCHÓD; WSCHODNI; WIATR WSCHODNI  
s.c. ŠAD, l.mn. ŠADŰ, ŠAD€, ŠADÂNI  
ŠADDU – CIĄGNIĘTY, WLECZONY; POTRAKTOWANY Z WYROZUMIAŁOŚCIĄ, CIERPLIWOŚCIĄ?  
ŠADHU– TEN, KTÓRY PRZESZEDŁ, PRZECIĄGNĄŁ (o procesji świątecznej)  
ŠADIDU – CIĄGNĄCY, WLOKĄCY; CIĄGNĄCY SIĘ; MAJĄCY WYROZUMIAŁOŚĆ, CIERPLIWOŚĆ  
ŠADIHU – IDĄCY, PRZECIĄGAJĄCY (o procesji świątecznej)  
ŠADILTU lub ŠADLU – SZEROKI, ROZLEGŁY; OBFITY, SUTY  
ŠADLU lub ŠADILTU – j.w.  
ŠADŰ lub ŠADDŰ – GÓRA; KRAJ GÓRSKI; WSCHÓD; WSCHODNI; WIATR WSCHODNI  
s.c. ŠAD, l.mn. ŠADŰ, ŠAD€, ŠADÂNI  
ŠAGAMU (u) – RYCZEĆ, WYĆ  
\[jako rzeczownik\] RYCZENIE, WYCIE  
ŠAGAŠU (i) – ZAMORDOWAĆ, ZABIJAĆ  
\[jako rzeczownik\] MORDOWANIE, ZABIJANIE  
ŠAGGAŠTU – MASAKRA, RZEŹ  
ŠAGIMU – RYCZĄCY, WYJĄCY  
ŠAGIŠU – MORDUJĄCY, ZABIJAJĄCY; ZABÓJCA? MORDERCA?  
ŠAGŠU – ZAMORDOWANY, ZABITY  
ŠAHANU – \[najczęściej D ŠUHHUNU:\] ROZGRZEWAĆ, NAGRZEWAĆ, OCIEPLAĆ  
\[jako rzeczownik\] ROZGRZEWANIE, NAGRZEWANIE, OCIEPLANIE  
ŠAHARU lub SAHARU(M), SAH€RU – OBRACAĆ SIĘ; OBRACAĆ; KRĘCIĆ SIĘ, ZAKRĘCIĆ SIĘ; PORUSZAĆ SIĘ  
PO OKRĘGU; OKRĄŻAĆ, OTACZAĆ; TROSZCZYĆ SIĘ (o – Akk.), ZAJMOWAĆ SIĘ (z Akk.); SZUKAĆ,  
POSZUKIWAĆ; ODWIEDZAĆ; ZWRÓCIĆ SIĘ (do kogoś); POWTÓRZYĆ; ZAJMOWAĆ SIĘ CZARAMI,  
CZAROWAĆ; ROBIĆ (coś) PO RAZ WTÓRY lub STALE  
\[jako rzeczownik\] OBRACANIE SIĘ; OBRACANIE; KRĘCENIE SIĘ, ZAKRĘCANIE SIĘ; PORUSZANIE SIĘ  
PO OKRĘGU; OKRĄŻANIE, OTACZANIE; TROSZCZENIE SIĘ, ZAJMOWANIE SIĘ; SZUKANIE, POSZU-  
KIWANIE; ODWIEDZANIE; ZWRACANIE SIĘ; POWTARZANIE; ZAJMOWANIE SIĘ CZARAMI, CZA-  
ROWANIE; ROBIENIE CZEGOŚ POWTÓRNIE lub STALE  
N NAŠHURU(M), NAŠHURU(M): OBRÓCIĆ SIĘ, PRZEKRĘCIĆ SIĘ  
\[jako rzeczownik\] OBRÓCENIE SIĘ, PRZEKRĘCENIE SIĘ  
NTN ITTAŠHURU(M), ITTAŠHURU(M): jak G ze znaczeniem częstotliwościowym  
D ŠUHHURU(M), ŠUHHURU(M): OBRACAĆ, ODWRACAĆ; ODEPRZEĆ  
\[jako rzeczownik\] OBRACANIE, ODWRACANIE; ODPARCIE  
Š ŠUŠHURU(M), ŠUŠHURU(M): POBUDZAĆ, SKŁANIAĆ; OBEJMOWAĆ, OTACZAĆ  
\[jako rzeczownik\] POBUDZANIE, SKŁANIANIE; OBEJMOWANIE, OTACZANIE  
ŠT ŠUTAŠHURU(M), ŠUTAŠHURU(M): OKRĄŻAĆ; BYĆ OKRĄŻONYM, ZNALEŹĆ SIĘ W OKRĄŻENIU;  
OBJĄĆ, OGARNĄĆ  
\[jako rzeczownik\] OKRĄŻANIE; BYCIE OKRĄŻONYM, ZNALEZIENIE SIĘ W OKRĄŻENIU; OBJĘCIE,  
OGARNIĘCIE  
ARKATA ŠUHHURU – RZUCAĆ SIĘ DO UCIECZKI  
ANA IDÂ SAHÂRU – OBRÓCIĆ SIĘ W (czyjąś) STRONĘ; WALCZYĆ RAZEM Z (kimś)  
IDA x SAHARU – STANĄĆ PO STRONIE x  
ŠAHATU lub ŠAHAŢU(M) (i) – SKAKAĆ (na kogoś – z Akk. lub z ANA); NAPADAĆ  
\[jako rzeczownik\] SKAKANIE; NAPADANIE  
ANA x ŠAHAŢU – SKAKAĆ NA x  
ŠAHAŢU (a/u) – STRZĄSNĄĆ, ZRZUCAĆ  
\[jako rzeczownik\] STRZĄSANIE, ZRZUCANIE  
ŠAHAŢU(M) (i) lub ŠAHATU – SKAKAĆ (na kogoś – z Akk. lub z ANA); NAPADAĆ  
\[jako rzeczownik\] SKAKANIE; NAPADANIE  
ANA x ŠAHAŢU – SKAKAĆ NA x  
ŠAHINU – ROZGRZEWAJĄCY, NAGRZEWAJĄCY, OCIEPLAJĄCY  
ŠAHITU(M) – SKACZĄCY; NAPADAJĄCY  
ŠAHIŢU – STRZĄSAJACY, ZRZUCAJĄCY  
ŠAHIŢU(M) – SKACZĄCY; NAPADAJĄCY  
ŠAHLUQTU(M) – ZNISZCZENIE; ŚMIERĆ  
ŠAHNU – ROZGRZANY, NAGRZANY, OCIEPLONY  
ŠAHTU(M) – TEN, NA KTÓREGO SKOCZONO; NAPADNIĘTY  
ŠAHŢU – STRZĄŚNIĘTY, ZRZUCONY  
ŠAHŢU(M) – TEN, NA KTÓREGO SKOCZONO; NAPADNIĘTY  
ŠAHŰ – ŚWINIA \[sum.\]; \[Lipin\] ŚWINIA; DZIK  
ŠAHURRATU – PARALIŻUJĄCE, OBEZWŁADNIAJĄCE PRZERAŻENIE, ZGROZA; MARTWA CISZA  
ŠAJAMANU lub ŠA’AMANU – NABYWCA, KUPUJĄCY  
ŠAKAKU (a/u) – BRONOWAĆ  
\[jako rzeczownik\] BRONOWANIE  
ŠAKAKU(M) – NAWLEKAĆ  
\[jako rzeczownik\] NAWLEKANIE  
ŠAKANAKKU lub ŠAKKANAKKU – MINISTER; STARSZY PREFEKT, NAMIESTNIK \[sum.\]  
ŠAKANU(M), ŠAKANU(M) (a/u) – KŁAŚĆ, NAKŁADAĆ; SADZAĆ, POSADZIĆ; WSADZIĆ; STAWIAĆ; WYZNACZAĆ;  
(St:) BYĆ POŁOŻONYM, LEŻEĆ (o miejscowości), BYĆ, ISTNIEĆ; MIEĆ (na miejscu); ŚWIĘCIĆ (święto);  
UPRAWIAĆ (pole); NAPIĘTNOWAĆ (z ABBUTTUM); ZAOPATRYWAĆ, DOSTARCZAĆ (komuś coś – podw.  
Akk.)  
\[jako rzeczownik\] KŁADZENIE, NAKŁADANIE; SADZANIE; WSADZANIE; STAWIANIE; WYZNACZANIE;  
BYCIE POŁOŻONYM, LEŻENIE (o miejscowości); BYCIE, ISTNIENIE; POSIADANIE (na miejscu); ŚWIĘCE-  
NIE, OBCHODZENIE (święta); UPRAWIANIE (pola); PIĘTNOWANIE; ZAOPATRYWANIE, DOSTARCZANIE  
GTN ŠITAKKUNU(M), ŠITAKKUNU(M): NIEUSTANNIE, WCIĄŻ NA NOWO KŁAŚĆ  
\[jako rzeczownik\] KŁADZENIE WCIĄŻ NA NOWO  
N NAŠKUNU(M), NAŠKUNU(M): BYĆ POSADZONYM, POŁOŻONYM; BYĆ ROBIONYM; BYĆ ZAWAR-  
TYM (o pokoju); WYBUCHNĄĆ (o wojnie); ZAPANOWAĆ (o głodzie, ciemności); SPRZYMIERZAĆ SIĘ (z ITTI)  
\[jako rzeczownik\] BYCIE POSADZONYM, POŁOŻONYM; BYCIE ROBIONYM; BYCIE ZAWARTYM (o poko-  
ju); WYBUCHANIE (o wojnie); ZAPANOWANIE (o głodzie, ciemności); SPRZYMIERZANIE SIĘ  
Š ŠUŠKUNU(M), ŠUŠKUNU(M): KAZAĆ KŁAŚĆ  
\[jako rzeczownik\] KAZANIE KŁADZENIA  
PÂ IŠTEN ŠUŠKUNU (dosł.) CZYNIĆ JEDNĄ MYŚLĄ, (przenośnie) UJARZMIAĆ, PODPORZĄDKOWAĆ  
TAHAZAM ŠAKANUM – KAZAĆ STOCZYĆ BITWĘ  
TEMA ŠAKANUM – WYDAĆ ROZKAZ  
PANI ANA x ŠAKANUM – POSTANOWIĆ ZROBIĆ x  
INA GAŠIŠIM ŠAKANUM – WBIĆ NA PAL  
ISSI x ŠAKANUM – WSPÓLNIE Z x ROBIĆ (coś)  
ANA ŠARRUTI ŠAKANU – USTANOWIĆ KRÓLEM  
PAŠU ITTI x ŠAKANU – POROZUMIEĆ SIĘ Z x  
ABIKTA (TAHTÂ) ŠAKANU – ZADAĆ KLĘSKĘ, ZWYCIĘŻYĆ, POKONAĆ  
ANA LIBBI ZITTI ŠAKANU – WLICZAĆ W UDZIAŁ  
INA MUHHI ŠAKANU – NAKŁADAĆ (trybut)  
NAPIŠTA ŠAKANU – ZGINĄĆ; UMRZEĆ, ZDECHNĄĆ  
PANA, PANI ŠAKANU (INA) – ZWRÓCIĆ OBLICZE (KU)  
REMUTA ŠAKANU – OKAZAĆ ŁASKĘ  
PANA ŠAKANU ANA x \[bezokol.\] – PODJĄĆ SIĘ x, PRZYSTĄPIĆ DO x  
RIKSATI ŠAKANU – SPORZĄDZIĆ KONTRAKT  
UZNA ŠAKANU – ZWRACAĆ, KIEROWAĆ UWAGĘ, MYŚL  
ŢEMA ŠAKANU – DOWODZIĆ  
ŠAKIKU – BRONUJĄCY  
ŠAKIKU(M) – NAWLEKAJĄCY  
ŠAKINU(M), ŠAKINU(M) – KŁADĄCY, NAKŁADAJĄCY; SADZAJĄCY; WSADZAJĄCY; STAWIAJĄCY; WYZNACZAJĄCY;  
BĘDĄCY, ISTNIEJĄCY; MAJĄCY (na miejscu); ŚWIĘCĄCY, OBCHODZĄCY (święto);UPRAWIAJĄCY (pole);  
PIĘTNUJĄCY; ZAOPATRUJĄCY, DOSTARCZAJĄCY (komuś coś)  
ŠAKINUM – NAMIESTNIK  
ŠAKKANAKKU lub ŠAKANAKKU – MINISTER; STARSZY PREFEKT, NAMIESTNIK \[sum.\]; \[Lipin\] ZARZĄDCA  
ŠAKKU – ZABRONOWANY  
ŠAKKU(M) – NAWLECZONY  
ŠAKKŰRU(M) – PIJANICA, PIJAK, OPÓJ  
ŠAKNU – POŁOŻONY; GUBERNATOR, NAMIESTNIK  
ŠAKNU lub PAHATU – NAMIESTNIK  
ŠAKNU(M), ŠAKNU(M) – POŁOŻONY, NAŁOŻONY; POSADZONY; WSADZONY; POSTAWIONY; WYZNACZONY; POŁOŻO-  
NY, LEŻĄCY (o miejscowości),POSIADANY (na miejscu);UCZCZONY, OBCHODZONY (święto); UPRAWIONY (o  
polu); NAPIĘTNOWANY (z ABBUTTUM); ZAOPATRZONY, DOSTARCZONY.  
ŠAKRU – PIJANY  
ŠALALU(M) (a/u) – UPROWADZAĆ; ZDOBYĆ JAKO ŁUP; GRABIĆ, PLĄDROWAĆ  
\[jako rzeczownik\] UPROWADZANIE; ZDOBYWANIE JAKO ŁUPU; GRABIENIE, PLĄDROWANIE  
ŠALAMTU(M) – TRUP  
ŠALAMU(M) (i) – BYĆ ZDROWYM; ROZWIJAĆ SIĘ, ROSNĄĆ; BYĆ CAŁYM, NIENARUSZONYM; BYĆ SZCZĘŚLIWYM  
\[jako rzeczownik\] BYCIE ZDROWYM; ROZWIJANIE SIĘ, ROŚNIĘCIE; BYCIE CAŁYM, NIENARUSZONYM;  
BYCIE SZCZĘŚLIWYM  
D ŠULLUMU(M): ZACHOWYWAĆ, KONSERWOWAĆ; CZYNIĆ DOBRZE UTRZYMANYM; WYKAZY-  
WAĆ ZDROWIE; ODSZKODOWAĆ, WYNAGRODZIĆ, ZREKOMPENSOWAĆ (szkodę), POWETOWAĆ;  
ZWRÓCIĆ, ODDAĆ; CZYNIĆ ZNÓW DOBRYM, NAPRAWIĆ (błąd); REALIZOWAĆ, WYPEŁNIAĆ (przezna-  
czenie)  
\[jako rzeczownik\] ZACHOWYWANIE, KONSERWOWANIE; CZYNIENIE DOBRZE UTRZYMANYM; WY-  
KAZYWANIE ZDROWIA; WYPŁACANIE ODSZKODOWANIA, WYNAGRADZANIE, REKOMPENSOWA-  
NIE, WETOWANIE; ZWRACANIE, ODDAWANIE; CZYNIENIE ZNÓW DOBRYM, NAPRAWIANIE (błędu);  
REALIZOWANIE, WYPEŁNIANIE (przeznaczenia)  
ŠARRANU IŠLIMU – KRÓLOWIE ZAWARLI POKÓJ?  
ŠALAQU (a/u) – WYCINAĆ, WYKRAWAĆ  
\[jako rzeczownik\] WYCINANIE, WYKRAWANIE  
ŠALAŠA – TRZYDZIEŚCI; zob. ŠALAŠTU  
ŠALAŠŠERŰ – TRZYNASTY  
ŠALAŠTU lub ŠALAŠU – TRZY \[r.ż.\]  
s.a. ŠALAŠ, ŠALAŠAT, l.pdw. ŠALAŠA – TRZYDZIEŚCI  
ŠALAŠ QATATU, ŠALAŠ QATATI – TRZY CZWARTE  
ŠALAŠ REBÂT – TRZY CZWARTE  
ŠALAŠU lub ŠALAŠTU – j.w. \[r.m.\]  
\[ŠALAŠU\] – zob. ŠULLUŠU  
ŠALAŢU (a/u) – PRZECINAĆ, PRZEKRAWAĆ  
\[jako rzeczownik\] PRZECINANIE, PRZEKRAWANIE  
ŠALBUBUM – DZIKI  
ŠALGU – ŚNIEG  
ŠALILU(M) – UPROWADZAJĄCY; ZDOBYWAJĄCY JAKO ŁUP; GRABIĄCY, PLĄDRUJĄCY  
ŠALIMTU – zob. ŠALMU  
ŠALIMU(M) – ROZWIJAJĄCY SIĘ, ROSNĄCY  
ŠALIQU – WYCINAJĄCY, WYKRAWAJĄCY  
ŠALIŢU – PRZECINAJĄCY, PRZEKRAWAJĄCY  
ŠALLATU – ŁUP, ZDOBYCZ  
ŠALLU(M) – UPROWADZONY; ZDOBYTY JAKO ŁUP; ZAGRABIONY, SPLĄDROWANY  
ŠALMU lub ŠALIMTU – CAŁY, NIEUSZKODZONY; ZDROWY, POMYŚLNY; POKOJOWY, SPOKOJNY; WŁAŚCIWY, W  
PORZĄDKU (w sensie kultowym)  
ŠALAM ŠAMŠI – ZACHÓD SŁOŃCA; ZACHÓD  
ŠALMU(M) – ZDROWY; ROZWINIĘTY, WYROŚNIĘTY; CAŁY, NIENARUSZONY; SZCZĘŚLIWY  
ŠALQU – WYCIĘTY, WYKROJONY  
ŠALŠU lub ŠALUŠTU – TRZECI  
r.ż. ŠALUŠTUM  
ANA ŠALUŠ – NA TRZY CZĘŚCI  
ŠALŠU(M) – TRZY; TRZECI  
r.ż. ŠALAŠTUM, s.a. ŠALAŠ, ŠALAŠAT  
ANA ŠALAŠIŠU – TRZY RAZY  
ŠALAŠ SÂT QEMUM – 3 SUT MĄKI  
ŠALŠIŠU, ŠALAŠIŠU – TRZY RAZY  
ŠALŠU(M) – JEDNA TRZECIA  
s.a.r.m. ŠALUŠ, r.ż. ŠALUŠTUM, ŠALIŠTUM  
ŠALŢIŠ – JAKO WŁADCA  
ŠALŢU – PRZECIĘTY, PRZEKROJONY  
ŠALŰ (i) – ZANURZYĆ, ZAMOCZYĆ  
\[jako rzeczownik\] ZANURZANIE, ZAMACZANIE  
ŠALŰ – ZANURZONY, ZAMOCZONY  
ŠALŰ – ZANURZAJACY, ZAMACZAJĄCY  
ŠALU(M), – PYTANY  
ŠALU(M) – PYTANY; PROSZONY, UPROSZONY  
ŠÂLU(M) (a) lub ŠA’ALU(M) – PYTAĆ (o – Akk.); DOPYTYWAĆ SIĘ; WYPRASZAĆ, UPRASZAĆ, PROSIĆ  
\[jako rzeczownik\] PYTANIE; DOPYTYWANIE SIĘ; WYPRASZANIE, UPRASZANIE, PROSZENIE  
GTN ŠITA’’ULU(M): PYTAĆ WCIĄŻ NA NOWO, RAZ PO RAZ  
\[jako rzeczownik\] PYTANIE WCIĄŻ NA NOWO, DOPYTYWANIE SIĘ NIEUSTANNE  
GT ŠITULU(M)  
DI’ATAM (DATAM) ŠÂLUM – DOWIADYWAĆ SIĘ O  
ŠULUM x ŠA’ALU – DOWIADYWAĆ SIĘ O ZDROWIE, SAMOPOCZUCIE x  
ŠALUMMATU – SPLENDOR, CHWAŁA  
ŠALUŠTU lub ŠALŠU – TRZECI  
ANA ŠALUŠ – NA TRZY CZĘŚCI  
ŠAM’U – SŁUCHANY; TEN, KTÓRY OTRZYMAŁ ODCZYTANĄ (tabliczkę)  
ŠAMA’U lub ŠAMŰ (M) – NIEBO  
ELAT ŠAME – WYŻYNY NIEBIESKIE  
ŠAMA’U \[asyr.\] lub ŠEMŰ (M) (e) (šm’) – SŁUCHAĆ; OTRZYMYWAĆ ODCZYTANĄ (tabliczkę)  
\[jako rzeczownik\] SŁUCHANIE; OTRZYMYWANIE ODCZYTANEGO (tekstu)  
GTN ŠITAMMU’U: SŁUCHAĆ WCIĄŻ NA NOWO; PRZYSŁUCHIWAĆ SIĘ  
\[jako rzeczownik\] SŁUCHANIE WCIĄŻ NA NOWO; PRZYSŁUCHIWANIE SIĘ  
ŠAMALLŰ – HANDLARZ, KOMISJONER \[sum.\]  
ŠAMAMU – NIEBO  
ŠAMANTU lub SAMANU, ŠAMANU, SAMANTU – OSIEM  
ŠAMANU lub SAMANU, ŠAMANTU, SAMANTU – j.w.  
ŠAMANŰ lub SAMANŰ, ŠAMANUTU, SAMANUTU – ÓSMY  
ŠAMANUTU lub SAMANŰ, ŠAMANŰ, SAMANUTU – j.w.  
ŠAMÂRU (u) (i) lub SAMÂRU, SAWÂRU, SÂRU, ŠÂRU – TAŃCOWAĆ, SKAKAĆ, PLĄSAĆ; MIOTAĆ SIĘ, SZALEĆ;  
ROZBIJAĆ SIĘ, AWANTUROWAĆ SIĘ  
\[jako rzeczownik\] TAŃCOWANIE, SKAKANIE, PLĄSANIE; MIOTANIE SIĘ, SZALENIE; ROZBIJANIE SIĘ,  
AWANTUROWANIE SIĘ  
\[ŠAMARU\] – zob. ŠITMURU  
ŠAMARU (u; i) – BYĆ GWAŁTOWNYM, BYĆ Z IMPETEM; BYĆ NAROWISTYM (o koniu); PŁYNĄĆ WARTKO, BYSTRO  
(o wodzie)  
\[jako rzeczownik\] BYCIE GWAŁTOWNYM, BYCIE Z IMPETEM; BYCIE NAROWISTYM (o koniu); PŁYNIĘ-  
CIE WARTKO, BYSTRO (o wodzie)  
ŠAMAŠ – SZAMASZ \[mię boga słońca\]  
ŠAMAŠŠAMMU – SEZAM  
ŠAMI’U – SŁUCHAJACY; SŁUCHACZ? OTRZYMUJĄCY ODCZYTANĄ (tabliczkę)  
ŠAMIRU – TAŃCZĄCY, SKACZĄCY, PLĄSAJĄCY; MIOTAJĄCY SIĘ, SZALEJĄCY; ROZBIJAJĄCY SIĘ, AWANTURUJĄCY SIĘ;  
AWANTURNIK? SZLENIEC?  
ŠAMIRU – PŁYNĄCY WARTKO, BYSTRO (o wodzie)  
ŠAMKANUM – SZAMKANUM \[nazwa geograficzna związana z Sippar\]  
ŠAMMU – ROŚLINA; ZIELEŃ; TRAWA; ZADRZEWIENIE?; WARZYWO; determinatyw przed nazwami warzyw i traw  
s.c. ŠAM, l.mn. ŠAMMI, ŠAMM€, ŠAMMŰTI, ŠAMMÂTI  
ŠAMNU(M) – OLEJ; \[Lipin\] OLEJ ŚWIĘTY; TŁUSZCZ  
ŠAMRU lub ŠITMURU – GWAŁTOWNY, DZIKI  
ŠAMRU – GWAŁTOWNY, Z IMPETEM; NAROWISTY (o koniu); WARTKI, BYSTRY (o wodzie)  
ŠAMŠU – SŁOŃCE  
ŠALAM ŠAMŠI – ZACHÓD SŁOŃCA; ZACHÓD (strona świata\]  
EREB ŠAMŠI – ZACHÓD SŁOŃCA; ZACHÓD (strona świata\]  
ŠAMU (a/i) – OKREŚLAĆ, USTALAĆ  
\[jako rzeczownik\] OKREŚLANIE, USTALANIE  
ŠAMU – OKREŚLONY, USTALONY  
ŠAMŰ(M) – DESZCZ  
ŠAMŰ(M) lub ŠAMA’U – NIEBO  
ELAT ŠAME – WYŻYNY NIEBIESKIE  
ŠAMU(M), ŠAMU(M) (a) (š’m) – KUPIĆ  
\[jako rzeczownik\] KUPOWANIE  
INA QAT x ŠAMU – KUPIĆ OD x  
ŠAMU(M), ŠAMU(M) – KUPIONY  
ŠAMUTU – DESZCZ  
ŠANA’Ű(M) – INNY; PRZECIWNIK  
ŠANANU(M) (a/u) – BYĆ JEDNAKOWYM, TAKIM SAMYM; RYWALIZOWAĆ (z kimś – Akk.)  
\[jako rzeczownik\] BYCIE JEDNAKOWYM, BYCIE TAKIM SAMYM; RYWALIZOWANIE  
GT ŠITNUNU(M): BYĆ WZAJEMNIE PODOBNYM DO SIEBIE; ZMIERZYĆ SIĘ ZE SOBĄ  
\[jako rzeczownik\] BYCIE WZAJEMNIE PODOBNYM DO SIEBIE; MIERZENIE SIĘ ZE SOBĄ  
LA ŠANAN – NIEZRÓWNANY, BEZ PORÓWNANIA  
ŠANATU – zob. ŠATTU(M)  
ŠANGAMAHHU – ARCYKAPŁAN, KAPŁAN WYŻSZEJ RANGI \[sum.\]  
ŠANGŰ – KAPŁAN \[sum.\]  
ŠANGUTU – KAPŁAŃSTWO  
ŠANINU – RÓWNY; PRZECIWSTAWNY; PRZECIWNIK  
ŠANIŠ – PODOBNIE, RÓWNIEŻ, TAKŻE  
ŠANNU(M) – JEDNAKOWY, TAKI SAM  
ŠANŰ, ŠANŰ (i) – ROBIĆ DWA RAZY (coś), ZNÓW (coś) ROBIĆ; BYĆ INNYM (St.); ZMIENIAĆ SIĘ (Prs, Prt); BYĆ DRU-  
GIM  
\[jako rzeczownik\] ROBIENIE DWA RAZY, ROBIENIE ZNÓW; BYCIE INNYM; ZMIENIANIE SIĘ, BYCIE  
DRUGIM  
GT ŠITNŰ, ŠITNŰ: NA DŁUGO ROBIĆ INNYM; ZMIENIĆ SIĘ NA DŁUGO  
\[jako rzeczownik\] ROBIENIE INNYM NA DŁUGO; ZMIENIANIE SIĘ NA DŁUGO  
GTN ŠITANNŰ, ŠITANNŰ: ZMIENIAĆ SIĘ KILKA RAZY  
\[jako rzeczownik\] ZMIENIANIE SIĘ KILKA RAZY  
D ŠUNNŰ, ŠUNNŰ: ROBIĆ PO DWA RAZY, ZNÓW ROBIĆ; POWTÓRZYĆ (z ANA) (czasem w sensie: DO-  
NIEŚĆ)  
\[jako rzeczownik\] ROBIENIE PO DWA RAZY, ROBIENIE PONOWNIE; POWTARZANIE (czasem:) DONIESIE-  
NIE  
Š i ŠT ŠUŠNŰ, ŠUŠNŰ, ŠUTAŠNŰ, ŠUTAŠNŰ: PODWAJAĆ  
\[jako rzeczownik\] PODWAJANIE  
ŢEMA ŠUNNŰ – POSTRADAĆ ROZUM, STRACIĆ ROZUM  
ŠANŰ, ŠANŰ – ZROBIONY DWA RAZY, ZNÓW ZROBIONY; INNY; ZMIENIONY; DRUGI  
ŠANŰ, ŠANŰ – ROBIĄCY DWA RAZY, PONOWNIE ROBIĄCY; ZMIENIAJĄCY SIĘ  
ŠANU (a/i) – NASZCZAĆ (w – Akk.) ?  
\[jako rzeczownik\] NASZCZANIE?  
ŠANU – NASZCZANY? NASIKANY?  
ŠANŰ(M), ŠANŰ(M) – INNY; DRUGI; NASTĘPNY \[często wydłużone przez przyrostek -MA\]  
r.ż. ŠANITUM  
LA ŠANAN– BEZ PORÓWNANIA  
ŠAPAKU (a/u) – NASYPAĆ; NAGROMADZIĆ  
\[jako rzeczownik\] NASYPANIE; NAGROMADZENIE  
ŠAPAKU – WYLEWAĆ, ODLEWAĆ  
\[jako rzeczownik\] WYLEWANIE, ODLEWANIE  
ŠAPALU(M), ŠAPALU(M) – BYĆ NA DOLE  
\[jako rzeczownik\] BYCIE NA DOLE  
D ŠUPPULU(M), ŠUPPULU(M): KIEROWAĆ SIĘ W DÓŁ; CZYNIĆ NISKIM; GŁĘBOKO KOPAĆ  
\[jako rzeczownik\] KIEROWANIE SIĘ W DÓŁ; CZYNIENIE NISKIM; KOPANIE GŁĘBOKO  
Š ŠUŠPULU(M), ŠUŠPULU(M): KAZAĆ STAWAĆ SIĘ NISKIM, PONIŻAĆ  
\[jako rzeczownik\] NAKAZANIE STANIA SIĘ NISKIM, PONIŻANIE  
ŠAPARU(M) (a/u) – POSYŁAĆ, WYSYŁAĆ; PISAĆ; DAWAĆ ODPOWIEDŹ  
\[jako rzeczownik\] POSYŁANIE, WYSYŁANIE; PISANIE; DAWANIE ODPOWIEDZI  
ŠAPCU lub ŠEPCU – POTĘŻNY, SILNY; SROGI (mowa – DIBBU); (DOKONANY) PRZY POMOCY SIŁY; BOLESNY;  
NIEZALEŻNY, NIEPODLEGŁY; GWAŁCICIEL, CIEMIĘŻCA; SIŁA, MOC  
l.mn. ŠEPCUTI  
ŠAPIKU – SZLAM, MUŁ, IŁ  
ŠAPIKU – NASYPUJĄCY, SYPIĄCY; GROMADZĄCY  
ŠAPIKU – WYLEWAJĄCY, ODLEWAJĄCY  
ŠAPIRU(M) – POSYŁAJĄCY, WYSYŁAJĄCY; PISZĄCY; DAJĄCY ODPOWIEDŹ, ODPOWIADAJĄCY  
ŠAPKU – NASYPANY; ZGROMADZONY  
ŠAPKU – WYLANY, ODLANY  
ŠAPLANIŠ – NA DÓŁ  
ŠAPLANU(M), ŠAPLANU(M) – NA DOLE, U DOŁU, PONIŻEJ  
ANA ŠAPLANU(M) – W DÓŁ  
ŠAPLÂTI – TO, CO DOLNE, DÓŁ  
ŠAPLIŠ – NA DOLE, U DOŁU  
ŠAPLŰ – DRZEWCE; KOPIA  
r.ż. ŠAPLÎTU, l.mn. ŠAPLÂTI  
ŠAPLU lub ŠUPALU – DOLNY, ZNAJDUJĄCY SIĘ NA DOLE  
ŠAPLŰ(M) lub ŚAPLUM – DOLNY; DÓŁ, DOLNA CZĘŚĆ; GLEBA, ZIEMIA; POD  
r.ż. ŠAPLITUM, ŚAPILTUM  
INA ŠAPLI lub ŠAPAL – POD  
ŠAPRU(M) – POSŁANY, WYSŁANY; NAPISANY; ODPOWIEDZIANY  
ŠAPŠAQUM – OGRANICZENIE  
ŠAPTU(M) – WARGA \[r.ż.\]  
l.pdw. ŠAPTAN  
ŠAQA’U lub ŠAQŰ (šqi), ŠEQŰ (šqj) – NAWADNIAĆ; DAĆ (komuś) PIĆ, ZASPOKOIĆ (czyjeś) PRAGNIENIE, POIĆ, NA-  
POIĆ  
\[jako rzeczownik\] NAWADNIANIE; DAWANIE PIĆ, ZASPAKAJANIE PRAGNIENIA, POJENIE, NAPOJENIE  
Š ŠUŠQU’U: KAZAĆ NAWODNIĆ; KAZAĆ DAĆ PIĆ, KAZAĆ POIĆ  
\[jako rzeczownik\] NAKAZANIE NAWODNIENIA, SPOWODOWANIE NAWODNIENIA; KAZANIE NAPOIĆ  
ŠAQALU(M) (a/u) – WAŻYĆ; PŁACIĆ (za – z ANA rzeczy i Akk. ceny)  
\[jako rzeczownik\] WAŻENIE; PŁACENIE  
N NAŠQULU(M): BYĆ ZAWIESZONYM  
\[jako rzeczownik\] BYCIE ZAWIESZONYM  
Š ŠUŠQULU(M) – KAZAĆ PŁACIĆ  
\[jako rzeczownik\] NAKAZANIE PŁACENIA  
KIPPAT MATATI … ŠAQLATA – SKRAJE KRAJU TRZYMASZ JAK SZALE WAGI  
ŠAQARU (a/u) lub ZAKARU, SAQARU – MÓWIĆ, POWIEDZIEĆ; WZYWAĆ, WOŁAĆ; MODLIĆ SIĘ; POROZUMIEĆ SIĘ,  
DOGADAĆ SIĘ; NAZYWAĆ; PRZYSIĘGAĆ, ZAKLINAĆ SIĘ; DOKONAĆ, WYWOŁAĆ, STWORZYĆ  
\[jako rzeczownik\] MÓWIENIE, POWIEDZENIE; WZYWANIE, WOŁANIE; MODLENIE SIĘ; POROZUMIE-  
WANIE SIĘ, DOGADYWANIE SIĘ; NAZYWANIE; PRZYSIĘGANIE, ZAKLINANIE SIĘ; DOKONANIE, WY  
WOŁYWANIE, TWORZENIE  
GT ŠITQURU: POROZUMIEWAĆ SIĘ; MÓWIĆ, INFORMOWAĆ, POWIADOMIĆ  
\[jako rzeczownik\] POROZUMIEWANIE SIĘ; MÓWIENIE, INFORMOWANIE, POWIADAMIANIE  
Š ŠUŠQURU: KAZAĆ MÓWIĆ, ZMUSIĆ DO MÓWIENIA; RZUCIĆ (klątwę); ZAPRZYSIĄC, KAZAĆ ZŁO-  
ŻYĆ PRZYSIĘGĘ (z ADU, NIŠ QATI, NIŠ ILANI)  
\[jako rzeczownik\] NAKAZANIE MÓWIENIA, ZMUSZENIE DO MÓWIENIA; RZUCANIE (klątwy); ZAPRZY-  
SIĘGANIE, NAKAZANIE ZŁOŻENIA PRZYSIĘGI  
ŠAQILU(M) – WAŻĄCY; PŁACĄCY  
ŠAQIRU – MÓWIĄCY; WZYWAJĄCY, WOŁAJĄCY;MODLĄCY SIĘ; POROZUMIEWAJĄCY SIĘ, DOGADUJĄCY SIĘ; NAZYWA-  
JĄCY; PRZYSIĘGAJĄCY, ZAKLINAJĄCY SIĘ; DOKONUJĄCY, WYWOŁUJĄCY, TWORZĄCY  
ŠAQLU(M) – ZWAŻONY; ZAPŁACONY  
ŠAQRU – WYPOWIEDZIANY; WEZWANY, ZAWOŁANY; WYMODLONY; DOGADANY; NAZWANY; ZAPRZYSIĘŻONY; DOKO-  
NANY, WYWOŁANY, TWORZONY  
ŠAQŰ (šqi) lub ŠAQA’U, ŠEQŰ (šqj) – NAWADNIAĆ; DAĆ (komuś) PIĆ, ZASPOKOIĆ (czyjeś) PRAGNIENIE, POIĆ, NA-  
POIĆ  
\[jako rzeczownik\] NAWADNIANIE; DAWANIE PIĆ, ZASPAKAJANIE PRAGNIENIA, POJENIE, NAPOJENIE  
Š ŠUŠQŰ: KAZAĆ NAWODNIĆ; KAZAĆ DAĆ PIĆ, KAZAĆ POIĆ  
\[jako rzeczownik\] NAKAZANIE NAWODNIENIA, SPOWODOWANIE NAWODNIENIA; KAZANIE NAPOIĆ  
ŠAQŰ – NAWODNIONY; NAPOJONY  
ŠAQŰ – NAWADNIAJĄCY; DAJĄCY (komuś) PIĆ, ZASPAKAJAJĄCY (czyjeś) PRAGNIENIE, POJĄCY  
ŠAQŰ – WYSOKI  
r.ż. ŠAQITU, ŠAQUTU  
ŠAQŰ(M), ŠAQŰ(M), ŠAQŰ(M) (najczęściej u) (šqw) – BYĆ WYSOKIM (St.); STAWAĆ SIĘ WYSOKIM, ROSNĄĆ (Prs,  
Prt); STAWAĆ SIĘ WAŻNYM (Prs, Prt); PODNIEŚĆ SIĘ, WZNIEŚĆ SIĘ; BYĆ WIELKIM, WZNIOSŁYM  
\[jako rzeczownik\] BYCIE WYSOKIM; STAWANIE SIĘ WYSOKIM, ROŚNIĘCIE; STAWANIE SIĘ WAŻNYM;  
PODNOSZENIE SIĘ, WZNOSZENIE SIĘ; BYCIE WIELKIM, WZNIOSŁYM  
D ŠUQQŰ(M), ŠUQQŰ(M), ŠUQQŰ(M): KIEROWAĆ SIĘ DO GÓRY; WNOSIĆ NA GÓRĘ; PODNIEŚĆ,  
WZNOSIĆ; CZYNIĆ WYSOKIM  
\[jako rzeczownik\] KIEROWANIE SIĘ DO GÓRY; WNOSZENIE NA GÓRĘ; PODNOSZENIE, WZNOSZENIE;  
CZYNIENIE WYSOKIM  
Š ŠUŠQŰ(M), ŠUŠQŰ(M), ŠUŠQŰ(M): WYWYŻSZYĆ, AWANSOWAĆ, UCZCIĆ  
\[jako rzeczownik\] WYWYŻSZANIE, AWANSOWANIE, UCZCZENIE  
ŠAQŰ(M), ŠAQŰ(M), ŠAQŰ(M) – WYSOKI; WYROSNIĘTY; WAŻNY; PODNIESIONY, WZNIESIONY; WIELKI, WZNIOSŁY  
ŠAQŰ(M), ŠAQŰ(M), ŠAQŰ(M) – STAJĄCY SIĘ WYSOKIM, ROSNĄCY; STAJĄCY SIĘ WAŻNYM; PODNOSZĄCY SIĘ,  
WZNOSZĄCY SIĘ  
ŠAR – TRZY TYSIĄCE SZEŚĆSET  
ŠARAHU (u) – BYĆ WSPANIAŁYM  
\[jako rzeczownik\] BYCIE WSPANIAŁYM  
ŠARAKU(M) (a/u) – PODAROWAĆ, OFIAROWAĆ  
\[jako rzeczownik\] PODAROWANIE, OFIAROWANIE  
\[ŠARAMU\] – zob. ŠURRUMU  
ŠARAPU (a/u) – SPALAĆ; ROZPALAĆ, WZNIECAĆ (ogień)  
\[jako rzeczownik\] SPALANIE; ROZPALANIE, WZNIECANIE (ognia)  
ŠARAQU(M) (i) – KRAŚĆ, OKRADAĆ  
\[jako rzeczownik\] KRADZENIE, OKRADANIE  
GTN ŠITARRUQU(M): MIEĆ ZWYCZAJ KRAŚĆ; OKRADAĆ WCIĄŻ NA NOWO  
\[jako rzeczownik\] ZAJMOWANIE SIĘ KRADZIEŻĄ, OKRADANIE WCIĄŻ NA NOWO  
ŠARHIŠ – WSPANIALE  
ŠARHU(M) – WSPANIAŁY  
ŠARIKU(M) – DARUJĄCY, OFIARUJĄCY; OFIARODAWCA? DARCZYŃCA?  
ŠARIPU – SPALAJĄCY, PALĄCY; ROZPALAJĄCY, WZNIECAJĄCY (ogień)  
ŠARIQU(M) – KRADNĄCY, OKRADAJĄCY  
ŠAR-KAL-ŠARRI – SZARKALSZARRI \[imię króla z dynastii akadyjskiej\]  
ŠARKU(M) – DAROWANY, OFIAROWANY  
ŠARPU – SPALONY, PALONY; ROZPALONY, WZNIECONY (ogień)  
ŠARQU(M) – UKRADZIONY, SKRADZIONY  
ŠARRAHU – DUMNY, WYNIOSŁY, PAŃSKI  
ŠARRAQANU – ZŁODZIEJ  
ŠARRAQU – ZŁODZIEJ  
ŠARRATU – KRÓLOWA  
ŠARRU(M), ŠARRU(M) (šrr) – KRÓL \[sum. LUGAL\]  
s.c. ŠAR, r.ż. ŠARRATU, l.mn. ŠARRÂNI  
ŠAR! – KRÓLU  
ŠARRUKIN – SARGON, wł. SZARRUKIN \[imię króla z dynastii akadyjskiej\]  
ŠARRUTU(M), ŠARRUTU(M) – WŁADZA KRÓLEWSKA, PANOWANIE KRÓLEWSKIE, KRÓLOWANIE; KRÓLESTWO;  
KRÓLEWSKI  
s.c. ŠARRŰT  
ŠARTU lub TAKILTU, ARGAMANNU – LUDZKA SKÓRA POKRYTA WŁOSAMI  
ŠARTU – WŁOSY \[r.ż.\]  
ŠARTU – WEŁNA; MATERIA WEŁNIANA  
ŠARU lub ZAQIKU – WIATR, HURAGAN  
ŠÂRU (u) (i) lub SAMÂRU, ŠAMÂRU, SAWÂRU, SÂRU – TAŃCOWAĆ, SKAKAĆ, PLĄSAĆ; MIOTAĆ SIĘ, SZALEĆ;  
ROZBIJAĆ SIĘ, AWANTUROWAĆ SIĘ  
\[jako rzeczownik\] TAŃCOWANIE, SKAKANIE, PLĄSANIE; MIOTANIE SIĘ, SZALENIE; ROZBIJANIE SIĘ,  
AWANTUROWANIE SIĘ  
ŠÂRU (šjr) – POJMOWAĆ, POJĄĆ, ZROZUMIEĆ  
\[jako rzeczownik\] POJMOWANIE, ROZUMIENIE  
ŠARU – POJĘTY, ZROZUMIANY  
ŠARŰ (u; rzadko i) – BYĆ BUJNYM, OBFITYM  
\[jako rzeczownik\] BYCIE BUJNYM, BYCIE OBFITYM  
ŠARŰ – BUJNY, OBFITY  
\[ŠARU\] – zob. ŠURRU  
ŠARU(M) – WIATR; STRONA ŚWIATA  
ŠAR ERBETTIM – W CZTERECH STRONACH ŚWIATA; WSZĘDZIE  
ŠARURU – BLASK  
ŠASQŰ – SŁODKA MĄKA?  
ŠASŰ (i) – KRZYCZEĆ; WOŁAĆ, PRZYWOŁYWAĆ  
\[jako rzeczownik\] KRZYCZENIE; WOŁANIE, PRZYWOŁYWANIE  
ŠASŰ – WYKRZYCZANY; ZAWOŁANY, PRZYWOŁANY  
ŠASŰ – KRZYCZĄCY; WOŁAJĄCY, PRZYWOŁUJĄCY  
ŠÂŠA – zob. ŠI  
ŠAŠANUM – \[staroas.\] LAMPA \[ zapożyczenie z hetyckiego?\]  
ŠAŠI – zob. ŠI  
ŠAŠMU – WALKA (dwóch osób), POJEDYNEK; ZWADA, ZATARG  
ŠAŠU – zob. ŠU  
ŠAŠUNU – zob. ŠUNU  
ŠAT=ŠA; =ŠI  
INA ŠAT MUŠI – TEJ NOCY  
ŠATAQU(M) – ODGNIEŚĆ (?) \[tylko St.\]  
\[jako rzeczownik\] ODGNIATAĆ?  
ŠATIQU(M) – ODGNIATAJĄCY?  
ŠATQU(M) – ODGNIECIONY?  
ŠATTAMMU(M) – PODSKARBI  
ŠATTU(M) – ROK; (także w sensie: WYPRAWA)  
l.mn. ŠANATU, ŠANATI  
ŠATTAM – W TYM ROKU  
INA ŠANAT – OD ROKU  
ŠATTIŠAMMA lub ŠATTIŠAMA – COROCZNIE, CO ROK  
ŠATU – zob. ŠU; zob. ŠI  
ŠATŰ (i) – PIĆ  
\[jako rzeczownik\] PICIE  
INA ŠATŰ lub INA LIBBI ŠATŰ – PIĆ Z  
ŠATŰ – PITY  
ŠATŰ – PIJĄCY  
ŠATURRI – TRZECIA WACHTA NOCNA (od 2 do 6 godziny); NAD RANEM  
MACCARTA ŠAT URRI – j.w.  
ŠAŢARU (a/u) – PISAĆ  
\[jako rzeczownik\] PISANIE  
CER ŠAŢARU albo ELI ŠAŢARU albo INA LIBBI ŠAŢARU – PISAĆ NA  
INA x ANA y ŠAŢARU – PISAĆ DO y O SPRAWIE x  
ŠAŢIRU – PISZĄCY  
ŠAŢRU – PISANY, NAPISANY  
ŠAŢU (a/u) – (przech.) CIĄGNĄĆ, WLEC; (nieprzech.) DOWLEC SIĘ  
\[jako rzeczownik\] CIĄGNIĘCIE, WLECZENIE; DOWLECZENIE SIĘ  
ŠAŢU – ZACIĄGNIĘTY, ZAWLECZONY  
ŠE’ITU – UCHODZĄCY, UCIEKAJĄCY  
ŠE’IŢU – LEKCEWAŻĄCY, BAGATELIZUJĄCY; WYKRACZAJĄCY, DOPUSZCZAJĄCY SIĘ PRZEKROCZENIA; OBRAŻAJĄCY  
ŠE’Ű (i) – SZUKAĆ  
\[jako rzeczownik\] SZUKANIE  
ŠE’Ű – ODSZUKANY  
ŠE’Ű – SZUKAJĄCY; POSZUKIWACZ?  
ŠE’U(M) – ZBOŻE; JĘCZMIEŃ; ZIARNO NA SIEW; SZEU \[miara wagi – ok. 0,05 g)  
ŠEBERTU lub ŠEBRU – ROZBITY, STŁUCZONY  
ŠEBERU lub ŠABARU (i) – STŁUC, ROZBIĆ  
\[jako rzeczownik\] TŁUCZENIE, ROZBIJANIE  
ŠEBERU(M) (e) – ŁAMAĆ, ZŁAMAĆ  
\[jako rzeczownik\] ŁAMANIE, ZŁAMANIE  
ŠEBETTU lub SIBŰ, ŠIBŰ, SEBŰ, ŠEBŰ, SEBETTU – SIEDEM  
s.a. SEBE, SEBET  
ŠEBIRU – TŁUKĄCY, ROZBIJAJĄCY  
ŠEBIRU(M) – ŁAMIĄCY  
ŠEBRU lub ŠEBERTU – ROZBITY, STŁUCZONY  
ŠEBRU(M) – ZŁAMANY, ŁAMANY  
ŠEBŰ lub SEBŰ, ŠEBUTU, SEBUTU – SIÓDMY  
ŠEBŰ lub SIBŰ, ŠIBŰ, SEBŰ, ŠEBETTU, SEBETTU – SIEDEM  
s.a. SEBE, SEBET  
ŠEBU – STARZEC; STARSZY RODU  
ŠEBŰ(M) lub ŠABA’U(M) (i) (šb’) – BYĆ SYTYM; BYĆ ZADOWOLONYM (z – Akk.); NASYCAĆ SIĘ  
\[jako rzeczownik\] BYCIE SYTYM, BYCIE ZADOWOLONYM; SYCENIE SIĘ  
D ŠUBBŰ(M): NASYCAĆ, SYCIĆ, OFIAROWAĆ BY NASYCIĆ; ZADAWALAĆ  
\[jako rzeczownik\] NASYCANIE, SYCENIE, OFIAROWANIE BY NASYCIĆ; ZADAWALANIE  
DT ŠUTABBŰ(M): BYĆ NASYCONYM; BYĆ ZADOWOLONYM, ZADAWALAĆ SIĘ  
\[jako rzeczownik\] BYCIE NASYCONYM; BYCIE ZADOWOLONYM, ZADAWALANIE SIĘ  
ŠEBŰ(M) – SYTY; ZADOWOLONY; NASYCONY  
ŠEBŰ(M) – SYCĄCY; ZADAWALAJĄCY; NASYCAJĄCY  
ŠEBULTU(M) – TRANSPORT, PRZEWOŻONE DOBRO, ŁADUNEK  
ŠEBUTU lub SEBŰ, ŠEBŰ, SEBUTU – SIÓDMY  
ŠEDU(M) – BÓSTWO OPIEKUŃCZE; \[Lipin\] BÓSTWO W KSZTAŁCIE SKRZYDLATEGO BYKA; CZŁEKOGŁOWY BYK?  
ŠEDUŠTU? lub ŠEŠŠU – SZÓSTY \[r.ż.\]  
ŠEGŰ(M) (šg’) – SZALEĆ  
\[jako rzeczownik\] SZALENIE  
ŠEGŰ(M) – OSZALAŁY, SZALONY  
ŠEGŰ(M) – SZALEJĄCY  
ŠELABU lub ŠELIBU – LIS  
ŠELAŠŰ – TRZYDZIESTY  
ŠELIBU lub ŠELABU – j.w.  
\[ŠELU\] lub \[ŠA’ALU\] – zob. ŠU”ULU  
ŠELUTU – OSTRY KONIEC, GROT, OSTRZE (lancy)  
ŠEMIRU – PIERŚCIONEK; SPRZĄCZKA, KLAMERKA \[samogłoski niepewne\]  
ŠEMŰ – PRZYCHYLNY, POMYŚLNY; ODPOWIEDNI, STOSOWNY; POSŁUSZNY  
ŠEMŰ(M), ŠEMŰ(M) (e) (šm’) lub ŠAMA’U – SŁUCHAĆ; OTRZYMYWAĆ ODCZYTANĄ (tabliczkę)  
\[jako rzeczownik\] SŁUCHANIE; OTRZYMYWANIE ODCZYTANEGO (tekstu)  
GTN ŠITAMMŰ, ŠITAMMŰ: SŁUCHAĆ WCIĄŻ NA NOWO; PRZYSŁUCHIWAĆ SIĘ  
\[jako rzeczownik\] SŁUCHANIE WCIĄŻ NA NOWO; PRZYSŁUCHIWANIE SIĘ  
ŠEMŰ(M), ŠEMŰ(M) – USŁYSZANY, WYSŁUCHANY; TEN, KTÓRY OTRZYMAŁ ODCZYTANĄ (tabliczkę)  
ŠEMŰ(M), ŠEMŰ(M) – SŁUCHAJĄCY; OTRZYMUJĄCY ODCZYTANĄ (tabliczkę)  
ŠENA, ŠENA, ŠINA – DWA  
r.ż. ŠITTA  
ANA ŠENIŠU – DWA RAZY  
ŠENU(M) – BUT  
ŠEPCU lub ŠAPCU – POTĘŻNY, SILNY; SROGI (mowa – DIBBU); (DOKONANY) PRZY POMOCY SIŁY; BOLESNY; NIE-  
ZALEŻNY, NIEPODLEGŁY; GWAŁCICIEL, CIEMIĘŻCA; SIŁA, MOC  
l.mn. ŠEPCUTI  
Š€PU(M), Š€PU(M) – NOGA; PODNÓŻE (np. góry – ŠADU; BRZEG (rzeki, kanału – NARU); PODSTAWA; KORZEŃ (zęba –  
ŠINNU); CHÓD; KROK \[r.ż.\]  
l.pdw. ŠEPA(N); s.c. ŠEP, l.mn. Š€P€, Š€PÎTU, s.c. Š€PÎT, l.mn. Š€PÂTI  
INA ŠEP x – U PODNÓŻA x  
Š€PA – PIECHOTĄ; W NOGACH, ZNAJDUJĄCA SIĘ W DOLE \[r.ż.\]; DOLNY KRAJ; PASEK (materiału)  
Š€PÎT MICRI – DOLNA GRANICA  
INA lub ŠA ŠEP ANNATE albo AMMATE – NA TYM SAMYM BRZEGU, NA PRZECIWLEGŁYM  
BRZEGU  
ŠEQŰ (šqj) lub ŠAQŰ (šqi), ŠAQA’U – NAWADNIAĆ; DAĆ (komuś) PIĆ, ZASPOKOIĆ (czyjeś) PRAGNIENIE, POIĆ, NA-  
POIĆ  
\[jako rzeczownik\] NAWADNIANIE; DAWANIE PIĆ, ZASPAKAJANIE PRAGNIENIA, POJENIE, NAPOJENIE  
Š ŠUŠQŰ: KAZAĆ NAWODNIĆ; KAZAĆ DAĆ PIĆ, KAZAĆ POIĆ  
\[jako rzeczownik\] NAKAZANIE NAWODNIENIA, SPOWODOWANIE NAWODNIENIA; KAZANIE NAPOIĆ  
ŠEQŰ – NAWODNIONY; NAPOJONY  
ŠEQŰ – NAWADNIAJĄCY; DAJĄCY (komuś) PIĆ, ZASPAKAJAJĄCY (czyjeś) PRAGNIENIE, POJĄCY  
ŠERHANU lub ŠIR’ANU – MIĘSIEŃ, MUSKUŁ; ŚCIĘGNO  
ŠERIKTU lub ŠIRIKTU – PODARUNEK, PREZENT; POSAG (KH)  
ŠERTU – WYSTĘPEK, PRZEWINIENIE; KARA  
ŠERU – RANO, PORANEK  
URRAM U ŠERAM – W PRZESZŁOŚCI, ZAWSZE  
ŠEŠŠETU lub ŠEŠŠU – SZEŚĆ  
ŠEŠŠU, ŠEŠŠUM lub ŠEŠŠETU – j.w.  
r.ż. ŠEDIŠTUM, s.a. r.ż. ŠEŠŠET  
ŠEŠŠU lub ŠEDUŠTU ? – SZÓSTY \[r.m.\]  
r.ż. ŠEDUŠTUM  
ŠETU (e) – UCHODZIĆ, UCIEKAĆ  
\[jako rzeczownik\] UCHODZENIE, UCIEKANIE  
INA x ŠETU – UCIEKAĆ Z x  
LAPAN x ŠETU – UCIEKAĆ PRZED x  
ŠETU – ZBIEGŁY  
ŠEŢU (e) – LEKCEWAŻYĆ, BAGATELIZOWAĆ; WYKROCZYĆ, DOPUŚCIĆ SIĘ PRZEKROCZENIA; OBRAZIĆ  
\[jako rzeczownik\] LEKCEWAŻENIE; WYKRACZANIE, DOPUSZCZANIE SIĘ PRZEKROCZENIA;  
OBRAŻANIE  
ŠEŢU – ZLEKCEWAŻONY, ZBAGATELIZOWANY; OBRAŻONY  
ŠEWERU – PIERŚCIEŃ, BRANSOLETA  
ŠI lub ŠIT, IŠŠI – ONA; TA, DANA, WSPOMNIANA  
Odmiana w wersji starobabilońskiej i staroasyryjskiej  
l.p. l.mn.  
N: ŠI, ŠIT – ONA ŠINA – ONE  
G/A: ŠIATI, ŠUATI, ŠATU – JĄ ŠINATI – JE  
D: ŠIAŠIM, ŠUAŠIM, ŠAŠIM – JEJ ŠINAŠI(M) – IM  
Wersje młodsze:  
ŠI, ŠIT, (IŠŠI) ŠINA, ŠINI, (IŠŠINI)  
ŠUATI, ŠUWATI, ŠIATI, (ŠUATU) ŠÂTINA, ŠINATI, ŠUATINA  
ŠAŠI, ŠAŠA, ŠUAŠA ŠÂŠINA  
-ŠI – JĄ \[zaimek osobowy rodzaju żeńskiego w bierniku\]; JEJ \[zaimek osobowy rodzaju żeńskiego w celowniku\]  
l.mn. ŠINA  
ŠIATI – zob. ŠI  
ŠIBBU – PAS  
ŠIBISTU – ZŁOŚĆ, GNIEW  
s.c. ŠIBSAT  
ŠIBSU lub ŠIBŠU – GNIEW, ZŁOŚĆ  
ŠIBŠU lub ŠIBSU – j.w.  
ŠIBŢU lub ŠIPŢU – CIOS  
l.mn. ŠIBŢATU, ŠIPŢATU  
ŠIBŰ lub SIBŰ, ŠEBŰ, SEBŰ, ŠEBETTU, SEBETTU – SIEDEM  
s.a. SEBE, SEBET  
ŠIBU(M), ŠIBU(M) – STARZEC; ŚWIADEK; STARY, SIWY; SPLEŚNIAŁY  
l.mn. przymiotnika ŠIBUTU, l.mn. rzeczownika ŠIBU  
ŠIBUTTU – ŚWIADECTWO  
ŠIBUTU, ŠIBUTU – STARSI \[l.mn.\]; zob. ŠIBU(M)  
ŠIBUTU – ŚWIADECTWO, ZEZNANIE ŚWIADKA  
ŠIDDU – ZASŁONA; PAS ZIEMI; OKOLICA; BOK, FLANKA  
INA ŠID – WZDŁUŻ  
ŠIGARU – ZAMKNIĘCIE DRZWI, ZAMKNIĘCIE BRAMY; NASZYJNIK \[sum.\]; \[Lipin\] ZAMEK, ZAMKNIĘCIE, KLATKA  
ŠIGŰ – PIEŚŃ SKARGI \[pieśń kultowa, element rytuału\]  
ŠIHTU(M) – WŚCIEKŁOŚĆ, WŚCIEKANIE SIĘ  
ŠIHU – DŁUGI (o belce) (dosł. WYROSŁY)  
ŠIKARU – PIWO  
ŠIKNU – STWORZENIE, ISTOTA; WYRÓB, DZIEŁO, TWÓR, URZĄDZENIE; MUR  
ŠIKIN QATEJA – (często w sensie:) USTANOWIONY PRZEZE MNIE  
ŠIKRUM – PIWO  
ŠILAN lub ŠILLAN – NA ZACHODZIE  
ŠILLAN lub ŠILAN – j.w.  
ŠILLATU – ZUCHWALSTWO, ZUCHWAŁOŚĆ, AROGANCJA; BLUŹNIERSTWO; POTWARZ  
ŠILTAHU, ŠILTAHU – WYSTRZELENIE, WYPUSZCZENIE STRZAŁY \[słowo obcego pochodzenia\]; \[Lipin\] OSZCZEP,  
DZIRYT DO MIOTANIA  
ŠILU(M) – OTWOREK, DZIURKA  
-ŠIM – zob. –ŠA  
ŠIMTU – ZNAK ROZPOZNAWCZY; ZNAK WŁASNOŚCI (umieszczany na bydle)  
ŠIMTU(M) – LOS, PRZEZNACZENIE; INTERES; ZLECENIE; USTALENIE, ZDEFINIOWANIE  
s.c. ŠIMAT  
ANA ŠIMTI ALAKUM – IŚĆ KU PRZEZNACZENIU; UMIERAĆ  
INA ŠIMATI – ZGODNIE Z LOSEM (wyrażenie mające związek ze śmiercią)  
INA UM LA ŠIMATI lub INA UME LA ŠIMTIŠU – PRZEDWCZEŚNIE  
ŠIMTU(M) – KOLOR?  
ŠIMU(M), ŠIMU(M) – KUPNO, KUPOWANIE; CENA KUPNA, CENA ZAKUPU; TOWAR? (Amm.)  
ŠINA – zob. ŠI  
ŠINA lub ŠINAN, ŠITTA, ŠITTAN – DWA; DRUGI  
r.ż. ŠITTA  
ANA ŠINEŠU – PO DWA RAZY; (gdy mowa o dzieleniu) NA DWOJE  
ŠITTAN – DWIE; DWIE CZĘŚCI; DWIE TRZECIE (KH)  
ŠINA ŠUŠI – STO DWADZIEŚCIA  
ŠINIŠU – DWA RAZY  
ADI ŠINIŠU – DWA RAZY  
ŠINAN lub ŠINA, ŠITTA, ŠITTAN – j.w.  
-ŠINAŠI(M) – zob. ŠA  
ŠINATU – MOCZ \[l.mn.\]  
ŠINEPÂTUM lub ŠINEPIATUM – DWIE TRZECIE  
s.c. ŠINEPIAT, ŠINEPÂT, ŠITTAN  
ŠINEPIATUM lub ŠINEPÂTUM – j.w.  
ŠINEPŰ – DWIE TRZECIE  
ŠINNAN – ZĘBY, UZĘBIENIE; zob. ŠINNU(M)  
ŠINNU(M) – ZĄB; (specj.) KOŚĆ SŁONIOWA  
l.pdw. ŠINNAN  
ŠIN PIRI – KOŚĆ SŁONIOWA  
ŠINŠERŰ – DWUNASTY  
ŠIPATU – WEŁNA \[l.mn.\]; \[Lipin\] SKÓRA ZWIERZĘCA POKRYTA SIERŚCIĄ  
ŠIPIRTU – WIEŚĆ, PRZESŁANIE  
ŠIPRU – DORĘCZONY; PRZYGOTOWANY POD SIEW (o polu)  
ŠIPRU(M) – WIEŚĆ, WIADOMOŚĆ; PRZESYŁKA; PRACA, DZIEŁO, PRZEDSIĘWZIĘCIE; FUNKCJA, DZIAŁALNOŚĆ;  
\[Lipin\] LIST; PISMO  
l.mn. ŠIPRATUM  
MARI ŠIPRIM – GONIEC, POSEŁ; ŁĄCZNOŚĆ PRZEZ GOŃCÓW  
ŠIPIR QATI – DZIEŁO RĄK  
ŠIPTU – PRZYSIĘGANIE, ZAPRZYSIĘGANIE; \[Lipin\] ZAKLINANIE  
ŠIPŢU lub ŠIBŢU – CIOS  
l.mn. ŠIBŢATU, ŠIPŢATU  
ŠIQITU – IRYGACJA  
ŠIQLU(M) – SZEKEL \[jednostka pieniężna\]  
ŠIR’ANU lub ŠERHANU – MIĘSIEŃ, MUSKUŁ; ŚCIĘGNO  
ŠIRIKTU lub ŠERIKTU – PODARUNEK, PREZENT; POSAG (KH)  
ŠIRU – WYROCZNIA; PRZEPOWIEDNIA  
ŠIRU lub ZUMRU(M) – CIAŁO; KORPUS  
ŠIRU(M) – TKANINA  
ŠIRU(M) – MIĘSO  
ŠISITU – WOŁANIE, WEZWANIE, ZEW; OKRZYK  
ŠIT lub ŠI, IŠŠI – ONA; TA, DANA, WSPOMNIANA  
Odmiana w wersji starobabilońskiej i staroasyryjskiej  
l.p. l.mn.  
N: ŠI, ŠIT – ONA ŠINA – ONE  
G/A: ŠIATI, ŠUATI, ŠATU – JĄ ŠINATI – JE  
D: ŠIAŠIM, ŠUAŠIM, ŠAŠIM – JEJ ŠINAŠI(M) – IM  
Wersje młodsze:  
ŠI, ŠIT, (IŠŠI) ŠINA, ŠINI, (IŠŠINI)  
ŠUATI, ŠUWATI, ŠIATI, (ŠUATU) ŠÂTINA, ŠINATI, ŠUATINA  
ŠAŠI, ŠAŠA, ŠUAŠA ŠÂŠINA  
ŠIT’ARU(M) – BŁYSZCZĄCY SIĘ; MIENIĄCY SIĘ  
ŠITKUNU – POŁOŻONE; ZROBIONE  
ŠITMURU – GT: CZCIĆ; SKŁADAĆ HOŁD; CHWALIĆ  
\[jako rzeczownik\] CZCZENIE, SKŁADANIE HOŁDU, CHWALENIE  
ŠITMURU – zob. ŠAMRU  
ŠITTA – zob. ŠINA; zob. ŠENA  
ŠITTA lub ŠINAN, ŠINA, ŠITTAN – DWA  
ANA ŠINEŠU – PO DWA RAZY; (gdy mowa o dzieleniu) NA DWOJE  
ŠITTAN – DWIE; DWIE CZĘŚCI; DWIE TRZECIE (KH)  
ŠITTAN lub ŠINAN, ŠINA, ŠITTA – j.w.  
ŠITTAN – zob. ŠINA; zob. ŠENA  
ŠITTU – SEN \[r.ż.\]  
ŠITULLUM – SZITULLUM \[nazwa miasta\]  
ŠIŢRU – PISMO  
s.c. ŠIŢIR  
ŠIŢIR ŠUMIJA – INSKRYPCJA IMIENNA  
ŠIZBU – MLEKO  
ŠU lub ŠUT – ON \[zaimek osobowy 3 osoby rodzaju męskiego\]  
Odmiana w wersji starobabilońskiej i staroasyryjskiej  
ŠU, ŠUT – ON ŠUNU – ONI  
ŠUATI, ŠUATU, ŠATI, ŠATU – JEGO ŠUNUTI – ICH  
ŠUAŠIM, ŠÂŠUM, ŠAŠIM – MU ŠUNUŠI(M) – IM  
-ŠU lub -Š – JEGO \[sufigowany zaimek osobowy 3 osoby rodzaju męskiego\]  
Odmiana:  
-ŠU, -Š – JEGO -ŠUNU, -ŠUN – ICH  
-ŠU(M) – -ŠUNUŠI(M), ŠUNUTI  
-ŠU, -Š -ŠINATI, -ŠINA  
ŠU – PAŃSTWO; MIASTO  
ŠU”ULU lub ŠULU – D: NAOSTRZYĆ, WYOSTRZYĆ (broń)  
\[jako rzeczownik\] NAOSTRZENIE, WYOSTRZENIE (broni)  
ŠU”ULU – NAOSTRZONY, WYOSTRZONY (o broni)  
ŠUATU – TEN, TA, TO; TAMTEN, ÓW  
ŠUBALKUTU(M) – DOPROWADZONY DO ODERWANIA; ZBUNTOWANY; PODŻEGANY; WYJMOWANY Z ZAWIASÓW (drzwi)  
ŠUBBU’U(M) – NASYCONY, SYCONY, OFIAROWANY BY NASYCIĆ; ZADOWOLONY  
ŠUBBURUM – CAŁKIEM ZŁAMANY  
ŠUBQUMU(M) – KAZAĆ STRZYC; \[bezok. Š, zob. BAQAMU(M)\]  
\[jako rzeczownik\] NAKAZANIE STRZYŻENIA, WSZCZĘCIE STRZYŻY  
ŠUBQUMU(M) – \[przymiotnik odczas. Š\] – PRZEZNACZONY DO STRZYŻENIA, PODDANY STRZYŻENIU, ZMUSZONY DO  
OSTRZYŻENIA SIĘ  
ŠUBBŰ(M) – NASYCONY, SYCONY, OFIAROWANY BY NASYCIĆ; ZADOWOLONY  
ŠUBRŰ(M) (bri) – KAZAĆ PATRZEĆ; POKAZYWAĆ, UKAZYWAĆ, ODSŁANIAĆ, UJAWNIAĆ; POSTANAWIAĆ?  
\[jako rzeczownik\] POKAZYWANIE, UKAZYWANIE, ODSŁANIANIE, UJAWNIANIE; POSTANAWIANIE?  
ŠUBRŰ(M) – OBEJRZANY NA (czyjeś) POLECENIE; POKAZANY, UKAZANY, ODSŁONIONY, UJAWNIONY;POSTANAWIONY?  
ŠUBŠU(M), ŠUBŠU(M), ŠUBŠU(M) – WYWOŁYWAĆ, SPRAWIAĆ; zob. BAŠU(M)  
\[jako rzeczownik\] WYWOŁYWANIE, SPRAWIANIE  
ŠUBŠU(M), ŠUBŠU(M), ŠUBŠU(M) – WYWOŁANY, SPRAWIONY  
ŠUBTU – MIESZKANIE, POMIESZCZENIE (MIESZKALNE); MIEJSCE ZAMIESZKANIA; SIEDZIBA; SIEDZENIE, KRZE-  
SŁO; SKŁADNICA (broni)  
ŠUBULUM – NAKAZANIE, ZLECENIE PRZYNIESIENIA, PRZESŁANIA, PRZEWIEZIENIA  
ŠUBULU(M) – KAŻĄCY PRZEWIEŹĆ, KAŻĄCY PRZYNIEŚĆ; TEN, KTÓRY WYNAJĄŁ TRANSPORT, WYNAJMUJĄCY  
TRANSPORT, NAJEMCA ŚRODKA TRANSPORTU  
ŠUBULU(M), ŠUBULU(M): PRZYNIESIONY; PRZESŁANY; POSŁANY; PRZEWIEZIONY  
ŠUCBUTU(M) – KAZAĆ ŁAPAĆ; OSIEDLAĆ (z ŠUBTA); POSTAWIĆ  
\[jako rzeczownik\] NAKAZANIE ZŁAPANIA; OSIEDLANIE (z ŠUBTA); POSTAWIENIE  
ŠUCBUTU(M) – ZŁAPANY (na czyjś rozkaz); OSIEDLONY; POSTAWIONY  
ŠUCCURU – POSTAWIONY NA WARCIE  
ŠUCŰ – WYPROWADZONY, ZMUSZONY DO WYJŚCIA; WYLANY (z brzegów); WYNIESIONY, WYWLECZONY, ODCIĄ-  
GNIĘTY; UWOLNIONY; DOPROWADZONY DO UPADKU;ZDRADZONY (o tajemnicy), ŚCIĄGNIĘTY (o  
daninie)  
ŠUCŰ(M) – WYGNANIEC, BANITA  
ŠUCŰM – WYPROWADZONY, ZMUSZONY DO WYJŚCIA, WYRZUCONY; WYLANY, ROZLANY (o płynie); WYNIESIONY, WY-  
WLECZONY, ODCIĄGNIĘTY;UWOLNIONY; DOPROWADZONY DO UPADKU; ZDRADZONY, WYJAWIONY (o  
tajemnicy); ŚCIĄGNIĘTY, POBRANY (o daninie)  
ŠUCŰM – \[bezok. Š\] WYPROWADZANIE, NAKAZANIE WYJŚCIA; WYSTĄPIENIE (z brzegów), WYLANIE; WYNOSZE-  
NIE, WYWLECZENIE, ODCIĄGANIE; UWOLNIENIE; DOPROWADZENIE DO UPADKU; ZDRADZENIE  
(tajemnicy), ŚCIĄGNIĘCIE, POBRANIE (daniny)  
ŠUDKU(M) – ZABITY; OFIARA EGZEKUCJI?  
ŠUDU: ZAWIADOMIONY, POINFORMOWANY  
ŠUGETU – (prawdopodobnie) KAPŁANKA ŚWIECKA \[sum.\]  
ŠUGITU(M) – KAPŁANKA ŚWIECKA  
ŠUHARMUŢU (hrmt) – KAZAĆ SIĘ ROZPŁYNĄĆ, DOPROWADZIĆ DO ROZLANIA SIĘ, ROZLAĆ  
\[jako rzeczownik\] SPOWODOWANIE ROZPŁYNIĘCIA SIĘ, DOPROWADZENIE DO ROZLANIA SIĘ,  
ROZLEWANIE  
ŠUHARMUŢU – ROZLANY  
ŠUHARRATU – ZDRĘTWIENIE, ODRĘTWIENIE; SZTYWNOŚĆ  
ŠUHARRURU – STAWAĆ SIĘ SPOKOJNYM, USPAKAJAĆ SIĘ  
\[jako rzeczownik\] STAWANIE SIĘ SPOKOJNYM, USPAKAJANIE SIĘ  
ŠUHARRURU – SPOKOJNY, USPOKOJONY  
ŠUHARRURU – DRĘTWIEĆ, ZAMIERAĆ, STAWAĆ SIĘ NIERUCHOMYM, NIERUCHOMIEĆ, KOSTNIEĆ  
\[jako rzeczownik\] DRĘTWIENIE, ZAMIERANIE, STAWANIE SIĘ NIERUCHOMYM, NIERUCHOMIENIE,  
KOSTNIENIE  
ŠUHARRURU – ZDRĘTWIAŁY, ZAMARŁY, NIERUCHOMY, ZNIERUCHOMIAŁY, SKOSTNIAŁY  
ŠUHATINNU(M) – ? \[nazwa jakiejś rośliny\]  
ŠUHHUNU – ROZGRZANY, NAGRZANY, OCIEPLONY  
ŠUHURRATU – CISZA, SPOKÓJ  
ŠUHUZUM – \[bezok. Š\] NAKAZANIE ZABRANIA, POLECENIE WZIĘCIA  
ŠŰHUZUM – ZABRANY, WZIĘTY, ZAWŁASZCZONY, PRZEZNACZONY, WSKAZANY DO WZIĘCIA, DO ZABRANIA  
ŠUKANU – OZDOBA; KLEJNOTY  
ŠUKENNU lub ŠUKINNU – (ŠT od k’n?) UGINAĆ SIĘ (przed bogiem), KORZYĆ SIĘ; RZUCAĆ SIĘ NA ZIEMIĘ; PODDAĆ  
SIĘ  
\[jako rzeczownik\] UGINANIE SIĘ (przed bogiem), KORZENIE SIĘ; RZUCANIE SIĘ NA ZIEMIĘ, PODDAWA-  
NIE SIĘ  
ŠUK€NU(M) – RZUCAĆ SIĘ (W DÓŁ), ZESKAKIWAĆ  
\[jako rzeczownik\] RZUCANIE SIĘ (W DÓŁ), ZESKAKIWANIE  
ŠUKINNU lub ŠUKENNU – j.w.  
ŠUKKALMAHHU(M) – WIELKI WEZYR  
ŠUKKALU(M) – WEZYR; GONIEC  
ŠUKLULU – KOŃCZYĆ, DOKONYWAĆ  
\[jako rzeczownik\] KOŃCZENIE, DOKONYWANIE  
ŠUKLULU – SKOŃCZONY, PEŁNY; DOSKONAŁY  
ŠUKULUM – \[bezok. Š\] NAKAZANIE JEDZENIA, KARMIENIE, ŻYWIENIE  
ŠUKULUM – KARMIONY, ŻYWIONY  
ŠUKURRU – LANCA \[sum.\]  
ŠULGI – SZULGI \[imię króla z III dynastii z Ur\]  
ŠULLUMU(M) – ZACHOWANY, ZAKONSERWOWANY; DOBRZE UTRZYMANY; ZDROWY; ODSZKODOWANY, WYNAGRO-  
DZONY, ZREKOMPENSOWANY (o szkodzie), POWETOWANY;ZWRÓCONY, ODDANY; PRZYWRÓCONY DO  
DOBREGO STANU, NAPRAWIONY (błąd); ZREALIZOWANY, WYPEŁNIONY (o przeznaczeniu)  
ŠULLUŠU – D: ROBIĆ (coś) TRZYKROTNIE, ROBIĆ (coś) NA TRZY RAZY \[D\]  
\[jako rzeczownik\] ROBIENIE TRZYKROTNIE, ROBIENIE W TRZECH ETAPACH, NA TRZY RAZY  
ŠULLUŠU – ZROBIONY TRZYKROTNIE, ZROBIENIE NA TRZY RAZY, W TRZECH ETAPACH  
ŠULMANU – DAR; PRZYJACIELSKI PODARUNEK; OKUP; GRZYWNA (za karę); SZCZĘŚCIE  
ŠULMU(M), ŠULMU(M) – SZCZĘŚCIE; ZDROWIE; \[Lipin\] POKÓJ, DOBROBYT  
ŠULMŰ – OBLĘŻONY, OKRĄŻONY Z CZYJEGOŚ ROZKAZU  
ŠULPUTU(M): ZŁUPIONY, OGRABIONY; SPUSTOSZONY, ZNISZCZONY, ZGUBIONY; DOTKNIĘTY, OBMACANY, PORU-  
SZONY; ZHAŃBIONY, ZBEZCZESZCZONY  
ŠŰLU lub ŠU”ULU – D: NAOSTRZYĆ, WYOSTRZYĆ (broń)  
\[jako rzeczownik\] NAOSTRZENIE, WYOSTRZENIE (broni)  
ŠŰLU – NAOSTRZONY, WYOSTRZONY (o broni)  
ŠULŰ(M), ŠULŰ(M): PRZYBYŁY NA GÓRĘ (NA ROZKAZ, ŻYCZENIE KOGOŚ), PODNIESIONY; UCZYNIONY WYSOKIM,  
PODWYŻSZONY; PODNIESIONY WYSOKO;(także:) ODDALONY, ZABRANY, SPRZĄTNIĘTY, USUNIĘTY; WY-  
DOBYTY (zatopiony statek; KH); ZOSTAWIONY JAKO ZAŁOGA,, GARNIZON  
ŠULUKU – ODPOWIEDNI, STOSOWNY; zob. ALAKU  
ŠULUKUM – \[bezok. Š\] WYPRAWIANIE W DROGĘ, ZLECENIE RUSZENIA W DROGĘ, ZLECENIE WYMARSZU; zob.  
ALAKUM  
ŠULUKUM – WYPRAWIONY W DROGĘ, ZMUSZONY DO WYRUSZENIA  
ŠULUŠA – TRZY RAZY; PO TRZY  
ŠULUTU – GARNIZON, ZAŁOGA  
(-)ŠUM – zob. -ŠU; zob. ŠU  
ŠUMDULU – SZEROKI \[poet.\]  
ŠUMELU(M) – LEWA STRONA; LEWY  
ANA ŠUMELIM – NA LEWO; Z LEWEJ STRONY  
ŠUMELAM – NA LEWO  
ŠUMMA – JEŚLI, JEŻELI, KIEDY; (w komentarzach do wróżb często w sensie: ALBO, LUB, WZGLĘDNIE); (jako partykuła  
zdania warunkowego) GDY  
ŠUMMANNU – POWRÓZ, LINA  
ŠUMRUCU – MĘCZĄCY, TRAPIĄCY; SKRAJNIE TRUDNY, MOZOLNY  
ŠUMQUTU(M): ŚCIĘTY; DOPROWADZONYDO UPADKU, UPUSZCZONY  
ŠUMU lub ZIKRU, NIŠU – IMIĘ  
ŠUMU – CEBULA; CZOSNEK \[sum.\]  
ŠUMŰ – MIĘSO Z RUSZTU  
ŠUMU(M), ŠUMU(M) – IMIĘ, NAZWA; TYTUŁ; (w imionach własnych w znaczeniu: SYN) \[sum. MU\]  
s.c. ŠUM, l.mn. ŠUMŰ, ŠUM€, ŠUMÂTE, ŠUMATU  
ŠANŰM ŠUMŠU – INNA INTERPRETACJA (przepowiedni)  
MIMMA ŠUMŠU – WSZYSTKO CO MA NAZWĘ, WSZYSTKO CO ISTNIEJE, WSZYSTKO CO DA SIĘ PO-  
MYŚLEĆ  
ŠUMU SAŢRU – NAPIS IMIENNY, IMIENNY DOKUMENT  
ŠUN’UDU(M), ŠUN’UDU(M): ZAWIADOMIONY, ZAMELDOWANY  
ŠUNNŰ, ŠUNNŰ – ZROBIONY DWA RAZY, ZNÓW ZROBIONY; POWTÓRZONY(z ANA) (czasem w sensie: DONIESIONY)  
ŠUNU – zob. ŠU  
ŠUNUHU – BOLEŚNIE ZMĘCZONY; STRAPIONY, UDRĘCZONY  
-ŠUNUŠIM – zob. – ŠU  
ŠUPALKU’U (plk’) – Š: CZYNIĆ SZEROKIM, POSZERZAĆ  
\[jako rzeczownik\] CZYNIENIE SZEROKIM, POSZERZANIE  
ŠUPALKU’U – SZEROKI, POSZERZONY  
ŠUPALU lub ŠAPLU – DOLNY, ZNAJDUJĄCY SIĘ NA DOLE  
ŠUPARDU’U (prd’) – N NAPARDU’U: STAWAĆ SIĘ JASNYM, JAŚNIEĆ, ROZJAŚNIAĆ SIĘ; STAWAĆ SIĘ WESOŁYM,  
WESELEĆ; STAWAĆ SIĘ POGODNYM, POGODNIEĆ  
\[jako rzeczownik\] STAWANIE SIĘ JASNYM, JAŚNIENIE, ROZJAŚNIANIE SIĘ; STAWANIE SIĘ WESOŁYM,  
WESELENIE; STAWANIE SIĘ POGODNYM, POGODNIENIE  
Š ŠUPARDU’U: ROZJAŚNIAĆ; ROZWESELAĆ  
\[jako rzeczownik\] ROZJAŚNIANIE; ROZWESELANIE  
ŠUPARDU’U – ROZJAŚNIONY; ROZWESELONY  
ŠUPARRURU – ROZPOŚCIERAĆ, ROZCIĄGAĆ  
\[jako rzeczownik\] ROZPOŚCIERANIE, ROZCIĄGANIE  
ŠUPARRURU – ROZPOSTARTY, ROZCIĄGNIĘTY  
ŠUPARŠUDU lub NAPARŠUDU (pršd) – UCIEKAĆ  
\[jako rzeczownik\] UCIEKANIE  
ŠUPARŠUDU – ZBIEGŁY  
ŠUPELKU(M) – PROWADZONY Z PRĄDEM (statek, łódź)  
ŠUPELTU – ZMUSZENIE, PRZYMUS \[transkrypcja niepewna\]  
ŠUP€LU(M) – ZAMIENIAĆ  
\[jako rzeczownik\] ZAMIENIANIE  
ŠUPPULU(M), ŠUPPULU(M) – SKIEROWANY W DÓŁ; UCZYNIONY NISKIM; GŁĘBOKO WYKOPANY  
ŠUPRUŠU: ROZCIĄGNIĘTY, ROZPOŚTARTY; ZMUSZONY DO ODLECENIA  
ŠUPŠUQU – UCIĄŻLIWY; TRUDNY DO OBEJŚCIA, NIE DAJĄCY SIĘ OBEJŚĆ (? schwer begehbar)  
ŠUQALLULU(M) (šqll) – WISIEĆ; WIESZAĆ  
\[jako rzeczownik\] WISZENIE; WIESZANIE  
ŠUQALLULU(M) – POWIESZONY  
ŠUQAMMUMU(M) – STAĆ SIĘ ŚMIERTELNIE CICHYM  
\[jako rzeczownik\] STANIE SIĘ ŚMIERTELNIE CICHYM  
ŠUQAMMUMU(M) – ŚMIERTELNIE CICHY  
ŠUQQŰ(M), ŠUQQŰ(M), ŠUQQŰ(M) – SKIEROWANY DO GÓRY; WNIESIONY NA GÓRĘ; PODNIESIONY, WZNIESIONY;  
UCZYNIONY WYSOKIM  
ŠURBŰ lub ŠURBUTU – DOSTOJNY, WIELKI; NAJWIĘKSZY  
ŠURBŰ(M), ŠURBŰ(M) – WYWYŻSZONY; UCZYNIONY SILNYM, POTĘŻNYM  
ŠURBUTU lub ŠURBŰ – j.w.  
ŠURDŰ(M), ŠURDŰ(M) – WYCIEKŁY  
ŠURINNU – EMBLEMAT; SZTANDAR  
ŠURIPPAK – SZURUPAK \[miasto nad Eufratem\]  
ŠURQU – ZRABOWANE DOBRO, UKRADZIONE MIENIE  
ŠURRU – D: SCHYLAĆ SIĘ, POCHYLAĆ SIĘ; KŁANIAĆ SIĘ NISKO, ZGINAĆ KARK  
\[jako rzeczownik\] SCHYLANIE SIĘ, POCHYLANIE SIĘ; KŁANIANIE SIĘ NISKO, ZGINANIE KARKU  
ŠURRU – SCHYLONY, POCHYLONY; SKŁONIONY NISKO, ZE ZGIĘTYM KARKIEM  
ŠURRUMU – D: OSZLIFOWAĆ (róg)  
\[jako rzeczownik\] OSZLIFOWANIE (rogu)  
ŠURRUMU – OSZLIFOWANY (róg)  
ŠŰRŠŰ(M) – UPOSAŻONY (z czyjegoś polecenia)  
ŠURŠUDU – Š: ZAŁOŻYĆ SOLIDNIE  
\[jako rzeczownik\] SOLIDNE ZAŁOŻENIE  
ŠURŠUDU – ZAŁOŻONY SOLIDNIE  
ŠURŰ – PROWADZONY, SPROWADZONY (z czyjegoś rozkazu, czyjejś woli)  
ŠURŰ(M) – SPROWADZONY (na polecenie)  
ŠURUBTU – TO CO PRZYNIESIONE, DOSTAWA, PRZESYŁKA  
ŠURUBU(M): WPUSZCZONY, WPROWADZONY, WSADZONY DO ŚRODKA; PRZEPUSZCZONY;UWOLNIONY OD  
ODPOWIEDZIALNOŚCI?  
ŠURUPPAKŰ – MIESZKANIEC SZURUPPAK  
ŠUSHURU(M), ŠUSHURU(M) – POBUDZONY, SKŁONIONY; OBJĘTY, OTOCZONY  
ŠUSQURU – POWIEDZIANY (na rozkaz, pod przymusem), ZEZNANY; RZUCONY (o klątwie); ZAPRZYSIĘŻONY (na polecenie)  
ŠUSU – SZEŚĆDZIESIĄT  
ŠUŠKUNU(M), ŠUŠKUNU(M) – POŁOŻONY (z czyjegoś nakazu)  
ŠUŠNŰ, ŠUŠNŰ – PODWOJONY  
ŠUŠPULU(M), ŠUŠPULU(M) – NISKI (z czyjejś woli), PONIŻONY  
ŠUŠQŰ – NAWODNIONY; NAPOJONY (z czyjegoś polecenia)  
ŠUŠQŰ(M), ŠUŠQŰ(M), ŠUŠQŰ(M) – WYWYŻSZONY, AWANSOWANY, UCZCZONY  
ŠUŠQULU(M) – ZAPŁACONY (na czyjeś żądanie)  
ŠUŠQURU – POWIEDZIANY (na rozkaz, pod przymusem), ZEZNANY; RZUCONY (o klątwie); ZAPRZYSIĘŻONY (na polecenie)  
ŠUŠ-ŠI – KOPA, SZEŚĆDZIESIĄT  
ŠUŠŠU – KOPA, SZEŚĆDZIESIĄT  
ŠUŠUBU(M) – POSADZENIE, SADZANIE, ZMUSZENIE DO SIEDZENIA; OSIEDLANIE; zob. AŠABU(M)  
ŠUŠUBU(M) – POSADZONY, ZMUSZONY DO SIEDZENIA; OSIEDLONY  
ŠUŠUM – SZEŚĆDZIESIĄT  
ŠINA ŠUŠI – STO DWADZIEŚCIA  
ERBET ŠUŠI – DWIEŚCIE CZTERDZIEŚCI  
ŠUŠURU(M) – DOPROWADZANIE DO PORZĄDKU, POKRZEPIANIE; zob. AŠARU(M)  
ŠUŠURU(M) – DOPROWADZONY DO PORZĄDKU, POKRZEPIONY  
ŠUT lub ŠU – ON \[zaimek osobowy 3 osoby rodzaju męskiego\]  
Odmiana w wersji starobabilońskiej i staroasyryjskiej  
ŠU, ŠUT – ON ŠUNU – ONI  
ŠUATI, ŠUATU, ŠATI, ŠATU – JEGO ŠUNUTI – ICH  
ŠUAŠIM, ŠÂŠUM, ŠAŠIM – MU ŠUNUŠI(M) – IM  
ŠUT – TE, KTÓRE  
ŠUT’Ű(M) – BEZCZYNNY  
ŠUTA’Ű(M) (w’i) – BYĆ BEZCZYNNYM \[ŠT\]  
\[jako rzeczownik\] BYCIE BEZCZYNNYM  
ŠUTABULUM – \[bezok. ŠT\] ROZWAŻANIE, PRZEMYŚLIWANIE; ZDECYDOWANIE SIĘ, POSTANOWIENIE  
ŠUTABULU(M), ŠUTABULU(M) – ROZWAŻONY, PRZEMYŚLANY; ZDECYDOWANY, POSTANOWIONY  
ŠUTACBUTU(M) – KAZAĆ SIĘ WZAJEMNIE CHWYCIĆ  
\[jako rzeczownik\] NAKAZANIE, SPOWODOWANIE WZAJEMNEGO UCHWYCENIA SIĘ, SCZEPIENIA  
ŠUTACBUTU(M) – SCZEPIONY NAWZAJEM Z KIMŚ, ZNAJDUJĄCY SIĘ WE WZAJEMNYM POCHWYCENIU  
ŠUTADDURUM lub ŠUTADURUM – SMUCENIE SIĘ, BYCIE SMUTNYM; BYCIE ZAMROCZONYM; zob. ADARU(M)  
ŠUTADURUM lub ŠUTADDURUM – SMUCENIE SIĘ, BYCIE SMUTNYM; BYCIE ZAMROCZONYM; zob. ADARU(M)  
ŠUTAMHURU(M), ŠUTAMHURU(M): PORÓWNANY Z, ZRÓWNANY Z; ZRÓWNANY; (z czyjegoś nakazu, z czyjegoś powodu)  
UCZYNIONY JEDNAKOWYM JAK; USTANOWIONY, POTRAKTOWANY JAKO ODPOWIEDNIK  
ŠUTAMSUKU(M): ZOSTAĆ OSZKALOWANYM, ZNIESŁAWIONYM  
\[jako rzeczownik\] ZOSTANIE OSZKALOWANYM, ZNIESŁAWIONYM  
ŠUTAMSUKU(M) – OSZKALOWANY, ZNIESŁAWIONY  
ŠUTANUHU – \[bezok. ŠT\] MĘCZENIE SIĘ, WYSILANIE SIĘ; zob. ANAHU  
ŠUTASHURU(M), ŠUTASHURU(M) – OKRĄŻONY;ZAMKNIĘTY W OKRĄŻENIU; OBJĘTY, OGARNIĘTY  
ŠUTAŠNŰ, ŠUTAŠNŰ – PODWOJONY  
ŠUTEBRŰM – zob. BARU(M) (bri)  
ŠUTECBŰ(M) (cb’) – WYKONYWAĆ PLANOWO, WYKONYWAĆ JAK TRZEBA \[ŠT\]  
\[jako rzeczownik\] PLANOWE WYKONYWANIE, WYKONYWANIE JAK TRZEBA  
ŠUTECUM – CIĄGŁE WYGADYWANIE SIĘ, PAPLANIE RAZ PO RAZ, NIEDYSKRECJA, PLOTKARTSWO, SKŁON-  
NOŚĆ DO WYJAWIANIA SEKRETÓW; zob. WACUM  
ŠUTEŠURUM – zob. EŠERU(M); ŠT: ZAPROWADZAĆ ŁAD; KAZAĆ BYĆ PRAWEM  
\[jako rzeczownik\] ZAPROWADZENIE ŁADU; USTANAWIANIE PRAWEM (PRAWA)  
ŠUTEŠURUM – UŁADZONY, DOPROWADZONY DO ŁADU, UPORZĄDKOWANY;USTANOWIONY PRAWEM  
ŠUTLU(M), ŠUTLU(M) lub ŠUTLUMU(M) (tlw) – D TULLU(M), TULLU(M): POWIESIĆ, OBWIESIĆ, ZAWIESIĆ, OBWIE-  
SZAĆ  
Š ŠUTLU(M), ŠUTLU(M): UDZIELAĆ, OFIAROWAĆ, DAĆ, WRĘCZAĆ, DAROWAĆ; NAGRODZIĆ; POŻY  
CZYĆ; POWIERZYĆ; ODDAĆ NA WŁASNOŚĆ (Sarg.)  
ŠUTLU(M), ŠUTLU(M) – UDZIELONY, OFIAROWANY, DANY, WRĘCZONY, DAROWANY; NAGRODZONY; POŻYCZONY;  
POWIERZONY; ODDANY NA WŁASNOŚĆ (Sarg.)  
ŠUT-REŠI lub ŠUTREŠIM – SZUTRESZI \[rodzaj dworzan\]  
ŠUTREŠIM lub ŠUT-REŠI – j.w.  
ŠUTU – WIATR POŁUDNIOWY; POŁUDNIE  
ŠUTURU – PRZEWYŻSZAJĄCY  
ŠUZKURU – POWIEDZIANY (na rozkaz, pod przymusem), ZEZNANY; RZUCONY (o klątwie); ZAPRZYSIĘŻONY (na polecenie)  
ŠUZUBU(M) – PRZYMUSZONY; SPORZĄDZONY, WYSTAWIONY (dokument); URATOWANY (życie)

Ś  
ŚAPLUM lub ŠAPLU(M) – DOLNY; DÓŁ, DOLNA CZĘŚĆ; GLEBA, ZIEMIA; POD  
r.ż. ŠAPLITUM, ŚAPILTUM  
INA ŠAPLI lub ŠAPAL – POD

T

TA’ARTU lub TAJARTU – POWRÓT  
TA’IRU(M) – ODWRACAJĄCY Z POWROTEM; OBRACAJĄCY;ZAWRACAJĄCY, PODEJMUJĄCY NA NOWO ( jakąś sprawę);  
OTRZYMUJĄCY, DOSTAJĄCY NA NOWO (np. o dawnym właścicielu odzyskującym dom);ROBIĄCY NA NOWO  
TABAKU – NAPISAĆ, WYPISAĆ  
\[jako rzeczownik\] NAPISANIE, WYPISANIE  
TABAKU(M) (a/u) – ROZLEWAĆ, WYLEWAĆ; WYSYPYWAĆ, ROZSYPYWAĆ; WYRZUCIĆ  
\[jako rzeczownik\] ROZLEWANIE, WYLEWANIE; WYSYPYWANIE, ROZSYPYWANIE; WYRZUCANIE  
TABALATTUM – staroas.\] jakiś napój alkoholowy \[zapożyczenie z hetyckiego?\]  
TABALU(M) (a) – WYNIEŚĆ, ZABIERAĆ; PRZENIEŚĆ NA BOK, ODSTAWIĆ NA BOK  
\[jako rzeczownik\] WYNOSZENIE, ZABIERANIE; PRZENOSZENIE NA BOK, ODSTAWIANIE NA BOK  
TABIKU – PISZĄCY, WYPISUJĄCY  
TABIKU(M) – ROZLEWAJĄCY, WYLEWAJĄCY; WYSYPUJĄCY, ROZSYPUJĄCY; WYRZUCAJĄCY  
TABILU(M) – WYNOSZĄCY, ZABIERAJĄCY; PRZENOSZĄCY NA BOK, ODSTAWIAJĄCY NA BOK  
TABKU – NAPISANY, WYPISANY  
TABKU(M) – ROZLANY, WYLANY; WYSYPANY, ROZSYPANY; WYRZUCONY  
TABLU(M) – WYNIESIONY, ZABRANY; PRZENIESIONY NA BOK, ODSTAWIONY NA BOK  
TABRATU – ZADZIWIAJĄCY WIDOK, SZOKUJĄCY OBRAZ  
TABŰ(M) lub TEBŰ(M) (tb’) – PODNOSIĆ SIĘ, PODNIEŚĆ SIĘ; BYĆ NA NOGACH, BYĆ W DRODZE; POWSTAWAĆ,  
BUNTOWAĆ SIĘ?  
\[jako rzeczownik\] PODNOSZENIE SIĘ, PODNIESIENIE SIĘ; BYCIE NA NOGACH, BYCIE W DRODZE;  
POWSTAWANIE, BUNTOWANIE SIĘ?  
TABŰ(M) – PODNIESIONY; NA NOGACH, W DRODZE; ZBUNTOWANY?  
TABŰ(M) – PODNOSZĄCY; STAJĄCY NA NOGI, RUSZAJĄCY W DROGĘ; POWSTAJĄCY, BUNTUJĄCY SIĘ?  
TACLILTU – DACH  
TACLITU – MODLITWA  
TADMIQTU – PROCENTOWANIE, PRZYNOSZENIE DOCHODU Z KAPITAŁU? (KH); KAPITAŁ OPERACYJNY  
TAGMIRTU – USŁUŻNOŚĆ, UCZYNNOŚĆ  
INA TAGMIRTI LIBBI – Z ODDANIEM, Z PRZYWIĄZANIEM (in Ergebenheit)  
TAHAZU(M), TAHAZU(M) – BITWA, POTYCZKA; WALKA, BÓJ, STARCIE; NAPAD; NATARCIE  
s.c. TÂHÂZ  
TÂHÂZ C€RI – STARCIE NA POLU  
KAKK€ TÂHÂZI – BROŃ BOJOWA  
CAB€ TÂHÂZI – LUDZIE WOJENNI, ŻOŁNIERZE, WOJOWNICY  
TAHLIPTUM – ODZIEŻ  
TAHTŰ – KLĘSKA  
TAJARTU lub TA’ARTU – POWRÓT  
TAKAPU(M) – BYĆ PSTROKATYM, BYĆ PSTRYM \[St\]  
\[jako rzeczownik\] BYCIE PSTROKATYM, PSTRYM  
TAKBITTU – WIELKA ILOŚĆ, MASA, OGROM  
TAKILTU lub ARGAMANNU, ŠARTU – LUDZKA SKÓRA POKRYTA WŁOSAMI  
TAKILTU – PURPUROWA WEŁNA \[transkrypcja niepewna\]  
TAKPU(M) – PSTROKATY, PSTRY  
TAKULTU – ŻYWNOŚĆ, PROWIANT  
\[TALAMU(M)\] – zob. ŠUTLUMU(M)  
TALIMU – BLIŹNIAK, BRAT  
TALITTU – POMIOT, POTOMSTWO; MŁODE POKOLENIE, MŁODZIEŻ  
TALLU(M) – PRZEPONA  
\[TALŰ\] – zob. TULLU; zob. ŠUTLUMU  
TALUKU – MARSZ  
TAMAHU(M) (a/u) – UJMOWAĆ, ŁAPAĆ; TRZYMAĆ  
\[jako rzeczownik\] UJMOWANIE, ŁAPANIE; TRZYMANIE  
TAMARTU – OBSERWACJA; PODARUNEK (powitalny)  
TÂMDU lub TI’ÂMTU, TÂMTU – SŁONA, MORSKA WODA; MORZE, OCEAN  
s.c. TI’ÂMAT, l.mn. TÂMÂTE  
TAMERTU – ŁAN \[młodobab.\]  
TAMHARU – WALKA  
TAMHU(M) – UJĘTY, ZŁAPANY; TRZYMANY  
TAMHUCU – UDERZENIE, CIOS  
TAMIHU(M) – UJMUJĄCY, ŁAPIĄCY; TRZYMAJĄCY  
TAMÎRTU lub TAWÎRTU (twr) – OKOLICA (miasta); ŁĄKA; MIEJSCOWOŚĆ, MIEJSCE; KRAJ  
l.mn. TAM€RÂTI  
TAMKARU(M) – KUPIEC; WIERZYCIEL, POŻYCZKODAWCA  
TAMLŰ (ml’) – NASYP, TERASA ZIEMNA  
TAMQITU – OFIARA, DAR OFIARNY  
TAMŠILU – ODBICIE; TOŻSAMOŚĆ, RÓWNOŚĆ, BYCIE TAKIM SAMYM  
TÂMTU lub TI’ÂMTU, TÂMDU – SŁONA, MORSKA WODA; MORZE, OCEAN  
s.c. TI’ÂMAT, l.mn. TÂMÂTE  
TAMŰ (a lub e) – MÓWIĆ; PRZYSIĘGAĆ, KLĄĆ SIĘ  
\[jako rzeczownik\] MÓWIENIE; PRZYSIĘGANIE, ZAKLINANIE SIĘ  
D TUMMŰ: ZAPRZYSIĘGAĆ  
\[jako rzeczownik\] ZAPRZYSIĘGANIE  
NIŠ x TAMŰ – PRZYSIĘGAĆ NA x  
TAMŰ – POWIEDZIANY; ZAPRZYSIĘŻONY  
TAMŰ – ZAKLĘTY  
TAMŰ – MÓWIĄCY; PRZYSIĘGAJĄCY, ZAKLINAJĄCY SIĘ  
TANATTU – SŁAWA, CHWAŁA, NIMB  
l.mn. TANADATU  
TANIHU – CIERPIENIE, MĘKA  
TANITTU – POCHWAŁA  
TANZIMTU – SKARGA  
TAPDŰ lub DABDŰ – SKŁAD, MAGAZYN \[Niederlage\]  
TAPPŰ(M) – TOWARZYSZ, KOLEGA; SPRZYMIERZENIEC, SOJUSZNIK \[sum.\]  
TAPPŰTU – SĄSIEDZTWO; KOLEŻEŃSTWO; POMOC, WSPARCIE; BYCIE CZŁONKIEM GMINY?  
TAPPŰTU ALÂKU – PRZYJŚĆ NA POMOC  
TAPPŰT x ALAKU – STAWAĆ SIĘ TOWARZYSZEM x  
TAQLITU – KONIEC (roku)  
TAR’UM \[staroakad.\] – ZABRANY, ZABIERANY, ODEBRANY  
TARA’UM \[staroakad.\] lub TARU(M) (tru) – BRAĆ, ZABIERAĆ, ODBIERAĆ  
\[jako rzeczownik\] BRANIE, ZABIERANIE, ODBIERANIE  
TARACU – WYCIĄGAĆ, ROZCIĄGAĆ  
\[jako rzeczownik\] WYCIĄGANIE, ROZCIĄGANIE  
TARAKU (a/u) – BIĆ (o sercu)  
\[jako rzeczownik\] BICIE (serca)  
N NATRUKU: BYĆ ROZBITYM  
\[jako rzeczownik\] BYCIE ROZBITYM  
TARAKU(M) – BYĆ CIEMNYM \[St\]  
\[jako rzeczownik\] BYCIE CIEMNYM  
TARBACU(M), TARBACUM – PODWÓRZE, PLAC OBOZOWY, OGRODZENIE; RZEŹNIA; HALO, LISIA CZAPA KSIĘ-  
ŻYCA  
TARBITU – WYCHOWANEK, PRZYSPOSOBIONE DZIECKO; PĘD, ROŚLINA; MAJESTAT  
TARCU – WYCIĄGNIĘTY, ROZCIĄGNIĘTY  
TARGUMANNUM – \[staroas.\] TŁUMACZ \[zapożyczenie z hetyckiego\]  
TARI’UM \[staroakad.\] – BIORĄCY, ZABIERAJĄCY, ODBIERAJĄCY  
TARICU – WYCIĄGAJĄCY, ROZCIĄGAJĄCY  
TARIKU – BIJĄCY (o sercu)  
TARKIBTU – ZAPŁODNIENIE  
TARKU(M) – CIEMNY  
TARŰ(M) (tru) lub TARA’UM – BRAĆ, ZABIERAĆ, ODBIERAĆ  
\[jako rzeczownik\] BRANIE, ZABIERANIE, ODBIERANIE  
TARŰ(M) – BRANY, ZABRANY, ODEBRANY  
TARŰ(M) – BIORĄCY, ZABIERAJĄCY, ODBIERAJĄCY  
TÂRU(M) (tur) (a/u) – ODWRÓCIĆ SIĘ Z POWROTEM; OBRACAĆ SIĘ; WRACAĆ, POWRACAĆ; WRACAĆ DO (jakiejś  
sprawy); PRZYPADAĆ, DOSTAWAĆ SIĘ NA NOWO (np. dom dawnemu właścicielowi); (z innym czasownikiem)  
NA NOWO (coś) (ROBIĆ)  
\[jako rzeczownik\] ODWRACANIE SIĘ Z POWROTEM; OBRACANIE SIĘ; WRACANIE, POWRACANIE;  
WRACANIE DO (sprawy); PRZYPADANIE, DOSTAWANIE SIĘ NA NOWO; ROBIENIE NA NOWO (czegoś)  
D TURRU(M): KAZAĆ WRÓCIĆ, SKŁONIĆ DO POWROTU; ODPROWADZAĆ; ODWRÓCIĆ; OBRACAĆ,  
PRZEWRACAĆ; ZWRÓCIĆ (twarz); ZWRÓCIĆ, ODDAĆ, ODNIEŚĆ; ODSTAWIAĆ (zboże); BRAĆ W NIE-  
WOLĘ; (z innym czasownikiem) NA NOWO (coś) (ROBIĆ); ODPIERAĆ (wroga); ZDAWAĆ SPRAWĘ, SKŁA-  
DAĆ SPRAWOZDANIE  
\[jako rzeczownik\] NAKAZANIE POWROTU, SKŁONIENIE DO POWROTU; ODPROWADZANIE; ODWRA-  
CANIE; OBRACANIE, PRZEWRACANIE; ZWRACANIE (twarzy); ZWRACANIE, ODDAWANIE, ODNOSZE-  
NIE; ODSTAWIANIE (zboża); BRANIE W NIEWOLĘ; ROBIENIE NA NOWO; ODPIERANIE (wroga); ZDA-  
WANIE SPRAWY, SKŁADANIE SPRAWOZDANIA  
TARU ANA – STAWAĆ SIĘ  
ANA x TUARUM \[staroasyr.\] – POWRACAĆ DO x  
INA x TUARUM \[średnioasyr.\] – WRÓCIĆ DO x  
ANA MADIM TÂRUM – STAWAĆ SIĘ LICZNYM; POWIĘKSZAĆ SIĘ  
ANA ARKIŠU TÂRU – UCIEKAĆ, RZUCAĆ SIĘ DO UCIECZKI  
TURRU ANA – ROBIĆ  
DAMA TURRU \[średniobab.\] – MŚCIĆ ROZLANĄ KREW  
ANA AŠRIŠU TURRU – ODSTAWIĆ (coś) NA NALEŻNE MU MIEJSCE  
ANA IŠTEN PI TURRU – UCZYNIĆ JEDNYM WYRAŻENIEM, UCZYNIĆ WSPÓLNOTĄ  
GIMILLI x TURRU – POMŚCIĆ x  
PAN NIRI TURRU – ZAWRACAĆ; OBRACAĆ  
ŢEMA TURRU – DONIEŚĆ, POINFORMOWAĆ  
TARU(M) – ODWRÓCONY Z POWROTEM; OBRÓCONY;ZAWRÓCONY, PODJĘTY NA NOWO (o jakiejś sprawie); OTRZYMA-  
NY, DOSTANY NA NOWO (np. o domu zwróconym dawnemu właścicielowi);ZROBIONY NA NOWO  
TASLITU – MODLITWA, PACIERZ  
TASSUHTU lub TASUHTU – UBYTEK, STRATA  
TASUHTU lub TASSUHTU – j.w.  
TAŠMŰ lub TEŠMŰ – WYSŁUCHANIE  
TATTURRU – BOGACTWO, OBFITOŚĆ  
TAWÎRTU lub TAMÎRTU (twr) – OKOLICA (miasta); ŁĄKA; MIEJSCOWOŚĆ, MIEJSCE; KRAJ  
l.mn. TAM€RÂTI  
TEBU – NAPADAJĄCY WRÓG, NAJEŹDŹCA  
TEBŰ(M) lub TABŰ(M) (tb’) – PODNOSIĆ SIĘ, PODNIEŚĆ SIĘ; BYĆ NA NOGACH, BYĆ W DRODZE; POWSTAWAĆ,  
BUNTOWAĆ SIĘ?  
\[jako rzeczownik\] PODNOSZENIE SIĘ, PODNIESIENIE SIĘ; BYCIE NA NOGACH, BYCIE W DRODZE;  
POWSTAWANIE, BUNTOWANIE SIĘ?  
TEBŰ(M) – PODNIESIONY; NA NOGACH, W DRODZE; ZBUNTOWANY?  
TEBŰ(M) – PODNOSZĄCY; STAJĄCY NA NOGI, RUSZAJĄCY W DROGĘ; POWSTAJĄCY, BUNTUJĄCY SIĘ?  
TECLITU – MODLITWA  
TEDEQU lub TEDIQU – SZATA, ODZIEŻ  
TEDIQU lub TEDEQU – j.w.  
TEHHIUM lub ŢEHHU(M) – POGRANICZE, KRESY  
TELILTU – OCZYSZCZENIE  
TELŰ lub TULŰ – PIERŚ KOBIECA; WYMIĘ  
TENEQU lub TENIQU – NIEMOWLĘ  
TENEŠETU (’nš) lub T€NIŠ€TU – LUDZIE, LUDZKOŚĆ, RÓD LUDZKI \[l.mn.\]  
TENIQU lub TENEQU – NIEMOWLĘ  
T€NIŠ€TU (’nš) lub TENEŠETU – LUDZIE, LUDZKOŚĆ, RÓD LUDZKI \[l.mn.\]  
TEPTITU – PRZYGOTOWANIE POD UPRAWĘ (pola)  
TERHATU(M) – CENA NARZECZONEJ  
TERTU – WRÓŻBA, OMEN  
TERTU(M) – POLECENIE, NAKAZ  
BEL TERETIM – PEŁNOMOCNIK  
TESLITU(M) – MODLITWA; BŁAGANIE  
TEŠITUM – ZAMIESZANIE  
TEŠMŰ lub TAŠMŰ – WYSŁUCHANIE  
TEŠMŰ(M) – WYSŁUCHANIE  
T€ŠU (jš’) – ZAMIESZKI, ROZRUCHY, POWSTANIE, BUNT, REBELIA  
T€ŠŰ (tš’) – DZIEWIĘĆ; DZIEWIĄTY  
r.ż. TILTU, TIŠITTU  
TEŠŰ – ZAMIESZANIE, CHAOS  
TI’ÂMTU lub TÂMTU, TÂMDU – SŁONA, MORSKA WODA; MORZE, OCEAN  
s.c. TI’ÂMAT, l.mn. TÂMÂTE  
TIÂMAT – TIAMAT \[imię bóstwa personifikującego morze\]; zob. TI’AMTU  
ŠA TIAMAT KARASSA – WNĘTRZE TIAMAT  
TIBKU – STOK, ZBOCZE (górskie); WARSTWA (cegły)  
TIBNU – SŁOMA  
TIBU(M) – NATARCIE  
TIBŰTU(M) – NAJAZD, NAPAD, NAJŚCIE; NATARCIE; POWSTANIE, BUNT  
TIDUKU – WALKA, BÓJ  
CABE TIDUKI – WALCZĄCY  
TIKLU – POMOCNIK; POMOC, POPARCIE; PEWNOŚĆ  
TILLU(M) lub TILU – WZGÓRZE RUIN, PAGÓREK GRUZÓW \[sum.\]  
TILTU – zob. TEŠU  
TILU lub TILLU(M) – WZGÓRZE RUIN, PAGÓREK GRUZÓW \[sum.\]  
TIMALI lub AMŠALI – WCZORAJ  
TIŠŰM – DZIEWIĘĆ; DZIEWIĄTY  
r.ż. TIŠITUM, TEŠITUM – DZIEWIĘĆ, TIŠUTUM – DZIEWIĄTA, s.a. r.m. TIŠE, s.a. r.ż. TIŠET, TIŠIT  
TU’AMU – BLIŹNIĘTA  
TUBQU – STRONA  
TUHTUHANNUM – \[staroas.\] jakieś święto, nazwa też jakiegoś przedmiotu \[zapożyczenie z hetyckiego od słowa  
oznaczającego kołysanie się?\]  
TUKULTU – POMOC, OBRONA, WSPARCIE, OSTOJA; NADZIEJA; ZAUFANIE; LICZENIE NA KOGOŚ; SIŁA; SIŁY  
WOJSKOWE, WOJSKO, ŻOŁNIERZE  
s.c. TUKLAT, l.mn. TUKLÂTI  
BÎT TUKLÂTI – MIASTO GARNIZONOWE  
ÂL TUKULTI – TWIERDZA; MIASTO NADZIEI  
TULLU(M), TULLU(M) – POWIESIĆ, OBWIESIĆ, ZAWIESIĆ, OBWIESZAĆ  
\[jako rzeczownik\] WIESZANIE, POWIESZANIE, OBWIESZANIE, ZAWIESZANIE  
TULLU(M), TULLU(M) – POWIESZONY, OBWIESZONY, ZAWIESZONY, OBWIESZONY  
TULTU(M) – ROBAK  
TULŰ lub TELŰ – PIERŚ KOBIECA; WYMIĘ  
TUMMŰ – ZAPRZYSIĘŻONY  
TUPPURU lub DUPPURU – POZOSTAWAĆ Z DALA, ODDALAĆ SIĘ \[nieprzech.\]; WYPĘDZAĆ \[przech.\]  
\[jako rzeczownik\] POZOSTAWANIE Z DALA, ODDALANIE SIĘ; WYPĘDZANIE  
TUPPURU – POZOSTAWIONY Z DALA, ODDALONY \[nieprzech.\]; WYPĘDZONY \[przech.\]  
TUQMATU – BITWA, WALKA  
TUQUMTU lub TUQUNTU – WALKA; ZWADA, SPÓR, ZATARG  
TUQUNTU lub TUQUMTU – j.w.  
TURAHU – KOZIOŁ GÓRSKI  
TURRU(M) – ZAWRÓCONY, SKŁONIONY DO POWROTU; ODPROWADZONY; ODWRÓCONY; OBRÓCONY, PRZEWRÓ-  
CONY; ZWRÓCONY (twarz); ZWRÓCONY, ODDANY, ODNIESIONY; ODSTAWIONY (o zbożu);WZIĘTY W NIE-  
WOLĘ; (z innym czasownikiem) NA NOWO ZROBIONY; ODPARTY (o wrogu); ZREFEROWANY, SPRAWOZDANY  
TURUKKUTUM – TURUKKUTU \[nazwa jednego z ludów górskich\]  
TUSSINNUM – \[staroas.\] OBÓZ WOJSKOWY \[zapożyczenie z hetyckiego?\]  
TUŠŠŰ – POTWARZ; PRZESTĘPSTWO, ZBRODNIA; PLĄTANINA, BEZŁADNOŚĆ (mowy – DABABU); ZAMIESZANIE,  
ZAMĘT, BEZŁAD; NIESNASKI  
l.mn.r.ż. TUŠŠÂTI  
Ţ

ŢABIHU – SIEPACZ, EGZEKUTOR, KAT  
ŢABTU – DOBRO; SEDNO, ISTOTA  
ŢABTU – DOBRO, DOBRODZIEJSTWO  
ŢÂBU(M) (tib) (a/i) – BYĆ DOBRYM; BYĆ ODPOWIEDNIM; BYĆ ZDROWYM; BYĆ ZADOWOLONYM, BYĆ ZASPOKO-  
JONYM  
\[jako rzeczownik\] BYCIE DOBRYM, BYCIE ODPOWIEDNIM; BYCIE ZDROWYM; BYCIE ZADOWOLO-  
NYM, BYCIE ZASPOKOJONYM  
D ŢUBBU(M): CZYNIĆ DOBRYM; CZYNIĆ ZDROWYM, UZDRAWIAĆ; CZYNIĆ ŁAGODNYM, USPOKA-  
JAĆ  
\[jako rzeczownik\] CZYNIENIE DOBRYM, ULEPSZANIE; CZYNIENIE ZDROWYM, UZDRAWIANIE; CZY-  
NIENIE ŁAGODNYM, USPOKAJANIE  
INA ŢABI – KIEDY DOBRZE IDZIE  
ŢABU(M – DOBRY; ODPOWIEDNI; ZDROWY; ZADOWOLONY, ZASPOKOJONY  
ŢABU(M), ŢABU(M) – DOBRY; BEZ PROBLEMU DOSTĘPNY; DŹWIĘCZNY, DONOŚNY (głos); ŁADNIE PACHNĄCY,  
PRZYJEMNIE PACHNĄCY; PIĘKNY; ZADOWOLONY; PRZYJACIELSKI  
ŢABU ELI – PRZYJEMNY, MIŁY  
LA ŢABU – BRZYDKI  
ŢAHU – ZBLIŻAĆ SIĘ  
\[jako rzeczownik\] ZBLIŻANIE SIĘ  
ŢAHU – ZBLIŻONY, PRZYBLIŻONY  
ŢARADU(M) (a/u) – POSYŁAĆ, WYSYŁAĆ (osoby); ODPĘDZAĆ, WYPĘDZAĆ; PRZEPĘDZAĆ, ROZPĘDZAĆ  
\[jako rzeczownik\] POSYŁANIE, WYSYŁANIE (osoby); ODPĘDZANIE, WYPĘDZANIE; PRZEPĘDZANIE,  
ROZPĘDZANIE  
ŢARDU(M) – POSŁANY, WYSŁANY (osoby); ODPĘDZONY, WYPĘDZONY; PRZEPĘDZONY, ROZPĘDZONY  
ŢARIDU(M) – POSYŁAJĄCY, WYSYŁAJĄCY (osoby); ODPĘDZAJĄCY, WYPĘDZAJĄCY; PRZEPĘDZAJĄCY, ROZPĘDZAJĄCY  
ŢAŢMU lub DADMU – SIEDZIBA LUDZKA, MIEJSCE ZAMIESZKANE; MIEJSCE; KRAJE, OSIEDLA, MIASTA, PUNKTY  
ZASIEDLONE  
l.mn. DADMŰ, ŢAŢMŰ, DADMI, ŢAŢMI, DADM€, ŢAŢM€  
ŠŰT DADMI, NÎŠE DADMI – MIESZKANIEC; LUDZIE; LUDZKOŚĆ  
ŢEBŰ(M) (tb’) (i) – TONĄĆ, POGRĄŻAĆ SIĘ  
\[jako rzeczownik\] TONIĘCIE, POGRĄŻANIE SIĘ  
D ŢUBBŰ(M): ZANURZAĆ, POGRĄŻAĆ, TOPIĆ; IŚĆ W GŁĄB, WKOPYWAĆ SIĘ (w trakcie prac budowla-  
nych), WYKOPYWAĆ DÓŁ, KOPAĆ GŁĘBOKO  
\[jako rzeczownik\] ZANURZANIE, POGRĄŻANIE, TOPIENIE; POSUWANIE SIĘ W GŁĄB, WKOPYWANIE  
SIĘ; WYKOPYWANIE DOŁU, KOPANIE GŁĘBOKO  
ŢEBŰ(M) – UTOPIONY, ZATOPIONY, POGRĄŻONY  
ŢEBŰ(M) – TONĄCY, POGRĄŻAJĄCY SIĘ  
ŢEHHŰ(M) lub TEHHIUM – POGRANICZE  
ŢEHŰ(M) (thi) – PRZYBLIŻAĆ SIĘ, NADCHODZIĆ  
\[jako rzeczownik\] PRZYBLIŻANIE SIĘ, NADCHODZENIE  
D ŢUHHŰ(M): ZBLIŻAĆ SIĘ  
\[jako rzeczownik\] ZBLIŻANIE SIĘ  
(INA) ŢEHI – OBOK  
ŢEHŰ(M) – PRZYBLIŻONY, ZBLIŻONY, NADESZŁY  
ŢEHŰ(M) – PRZYBLIŻAJĄCY SIĘ, NADCHODZĄCY  
ŢEMU(M) – RAPORT, INFORMACJA, SPRAWOZDANIE, DONIESIENIE; ROZKAZ, POLECENIE; POSTANOWIENIE;  
KOMENDA, DOWÓDZTWO  
ŢIŢU – GLINA  
ŢUBBATU – SERDECZNOŚĆ, UPRZEJMOŚĆ, ŁASKAWOŚĆ \[l.mn.\]  
ŢUBBU – RADOŚĆ; ZDROWIE  
ŢUBBU(M) – UCZYNIONY DOBRYM, ULEPSZONY; UCZYNIONY ZDROWYM, UZDROWIONY; UCZYNIONY ŁAGODNYM,  
USPOKOJONY  
ŢUBBŰ(M) – ZANURZONY, POGRĄŻONY, ZATOPIONY; ZAGŁĘBIONY, WKOPANY (w trakcie prac budowlanych), WYKOPANY  
(dół), WYKOPANY GŁĘBOKO  
ŢUHHŰ(M) – ZBLIŻONY, NIEDALEKI  
ŢUHHŰ(M) – PRZYNIEŚĆ  
\[jako rzeczownik\] PRZYNIESIENIE  
ŢUHHŰ(M) – PRZYNIESIONY  
ŢUMMUMU lub SUKKUKU – GŁUCHY  
ŢUPPU – \[Stativ\] (ON) JEST (BYŁ, BĘDZIE) ZAPISANY  
ŢUPPU(M) – TABLICZKA; TABLICZKA GLINIANA; DOKUMENT  
ŢUPPUM HARMUM \[staroasyr.\] – PODWÓJNY DOKUMENT, DOKUMENT Z DUPLIKATEM (tabliczka w  
kopercie glinianej, która jest zapisana takim samym tekstem jak zawartość)  
ŢUPPU ŠA DAJJANE \[średnioasyr.\] – DOKUMENT SĄDOWY  
ŢUPPU DANNUTU \[średnioasyr.\] – WAŻNY DOKUMENT  
ŢUPŠARRU – PISARZ

U  
U – I  
Ú – ALBO; LECZ TAKŻE; TEŻ; BA, NAWET; POZA TYM, DALEJ, NASTĘPNIE, PONADTO  
U… U – ALBO… ALBO  
U LU… U LU – ALBO… ALBO  
Ű – TEN SAM, TO SAMO  
Ű’A – BOLI! ACH! AU! \[wykrzyknik bólu\]  
U’A – BIADA!  
UBABAN – DZIESIĘĆ PALCÓW; zob. UBANU(M)  
UBANU(M) – PALEC; WIERZCHOŁEK GÓRY, SZCZYT; „PALEC” \[część wątroby\] \[r.ż.\]  
l.pdw. UBABAN  
UBARTUTU – UBARTUTU \[postać mityczna; ojciec Utnapiszti\]  
UBBUBU(M) – OCZYSZCZONY, WYCZYSZCZONY; OCZYSZCZONY (przez przysięgę; Amm. IV3)  
UBBURU – POSĄDZAĆ, OBWINIAĆ; ZŁOŻYĆ FAŁSZYWE DONIESIENIE; ZACZAROWAĆ  
\[jako rzeczownik\] POSĄDZANIE, OBWINIANIE; SKŁADANIE FAŁSZYWEGO DONIESIENIA; CZAROWANIE  
UBBURU – POSĄDZONY, OBWINIONY; FAŁSZYWIE OSKARŻONY; ZACZAROWANY  
UCA’U lub WACŰ(M) (wci), WACA’UM – \[średnio- i nowobabilońskie\] WYCHODZIĆ, ODCHODZIĆ, UCIEKAĆ, REZY-  
GNOWAĆ; WYCIĄGAĆ; WYSTAWAĆ  
\[jako rzeczownik\] WYCHODZENIE, ODCHODZENIE, UCIEKANIE, REZYGNOWANIE; WYCIĄGANIE;  
WYSTAWIANIE  
GT ITCU’U: ODCHODZIĆ, ODDALAĆ SIĘ  
\[jako rzeczownik\] ODCHODZENIE, ODDALANIE SIĘ  
Š ŠUCU’U: WYPROWADZAĆ, KAZAĆ WYJŚĆ; WYSTĄPIĆ (z brzegów), WYLAĆ; WYNOSIĆ, WYWLEC,  
ODCIĄGNĄĆ; UWOLNIĆ; DOPROWADZIĆ DO UPADKU; ZDRADZIĆ (tajemnicę), ŚCIĄGNĄĆ (daninę)  
\[jako rzeczownik\] WYPROWADZANIE, NAKAZANIE WYJŚCIA; WYSTĄPIENIE (z brzegów), WYLANIE;  
WYNOSZENIE, WYWLECZENIE, ODCIĄGANIE; UWALNIANIE; DOPROWADZANIE DO UPADKU;  
ZDRADZANIE (tajemnicy); ŚCIĄGANIE (daniny)  
ŠTN ŠUTACU’U: CIĄGLE WYGADYWAĆ SIĘ, WYPAPLAĆ RAZ ZA RAZEM  
\[jako rzeczownik\] WYGADYWANIE SIĘ RAZ PO RAZ, CZĘSTE WYPAPLYWANIE  
ANA CITIM ŠUCUM – WYDAWAĆ  
UCURTU – RYSUNEK KONTUROWY; KOŁO CZARODZIEJSKIE; \[Lipin\] GRANICA; PRZEDSTAWIENIE  
UDDU (’d’) – POTRZEBA, BIEDA; KLĘSKA, PLAGA; UCISK  
UDDŰ – OKREŚLONY, POZNANY; ROZPOZNAWALNY  
UDDŰM – \[bezok. D\] USTANAWIANIE NA STAŁE; zob. ADŰ  
UDDŰM – USTANAWIONY NA STAŁE, UTRWALONY; zob. ADŰ  
UDDUŠU – ODNOWIONY, ODBUDOWANY, WYBUDOWANY NA NOWO, ODRESTAUROWANY  
UDRATE – DROMADERY  
UGARU(M) – POLE, NIWA, ŁAN  
UGGATU – ZŁOŚĆ, GNIEW  
UGGUGU – ROZGNIEWANY, ROZZŁOSZCZONY  
UHHURU – D: SPÓŹNIAĆ SIĘ, OPÓŹNIAĆ SIĘ; BYĆ NIEWIDOCZNYM (dosł. POZOSTAWAĆ W TYLE) \[termin astrono-  
miczny\] \[D\]  
\[jako rzeczownik\] SPÓŹNIANIE SIĘ, OPÓŹNIANIE SIĘ; (astronom.) BYCIE NIEWIDOCZNYM, (dosł.) POZO-  
STAWANIE W TYLE  
UHHURU – SPÓŹNIONY, OPÓŹNIONY; NIEWIDOCZNY (astronom.), (dosł.) POZOSTAŁY W TYLE  
UHHUZU – POWLEKAĆ (metalem)  
\[jako rzeczownik\] POWLEKANIE (metalem)  
UHHUZU – POWLECZONY (metalem)  
UKULLŰ – PASZA, POŻYWIENIE  
UKULTU – POKARM, ŻARCIE  
UL – NIE \[negacja zdania głównego, oznajmującego itp.\]  
UL… UL – ANI… ANI  
UL – ALBO  
ULA \[starobab. i staroasyr.\] – NIE \[negacja zdania głównego, oznajmującego itp.\]  
ULLITU – TAMTA  
l.mn. ULLIATUM, ULLÂTUM  
ULLUCU(M) – RADOWANIE SIĘ, RADOŚĆ  
ULLULU – WYKRZYKIWAĆ Z RADOŚCI, RADOŚNIE WOŁAĆ  
\[jako rzeczownik\] WYKRZYKIWANIE Z RADOŚCI, RADOSNE WOŁANIE  
ULLULU – WYKRZYCZANY Z RADOŚCI, ZAWOŁANY RADOŚNIE  
ULLULUM – \[bezok. Š\] WISZENIE, OBWIESZANIE, ZAWIESZANIE; zob. ALALUM  
ULLULUM – POWIESZONY, ZAWIESZONY  
ULLŰM – TAMTEN  
r.ż. ULLITU, l.mn. ULLŰTU(M), l.mn. r.ż. ULLIATUM, ULLÂTUM  
ULLŰ(M), ULLŰ(M): PODNIESIONY, PODWYŻSZONY; MIANOWANY?  
ULTU \[średnio- i nowobab.\] lub IŠTU, IŠTUM, ISSU – \[miejscowo:\] Z, OD, PRZY, POD, U, NA; \[czasowo:\] OD, PO, PO TYM  
JAK; \[jako spójnik:\] GDY, SKORO; por. ULTU  
ISSU LIBBI – Z  
ISSU PAN – OD?  
ULTU LIBBI – Z WEWNĄTRZ; PO, PO TYM JAK  
ULTU\[średniobab.\] lub IŠTU, IŠTUM(MA), ISSU – \[subj:\] GDY, SKORO, KIEDY; OD TEGO CZASU, ODTĄD  
ISSU BET – OD  
ULTU – OD (czasowo i miejscowo); Z (miejscowo); (jako spójnik) GDY, SKORO, SKORO TYLKO  
ULTU ULLA – OD DAWNA  
ULU – \[składnik zwrotu\]  
ULU… ULU – ALBO… ALBO  
UM – W DNIU, GDY \[wprowadza zdanie czasowe\]  
-UM lub –IUM – \[końcówka określająca nazwy ludów\]; por. AŠŠURUM  
UMAM – DZIŚ, DZISIAJ  
UMAMU – ZWIERZĘ  
UMAMUM – ZWIERZĘTA, FAUNA  
UMIŠAM – CODZIENNIE  
UMMA – \[partykuła wprowadzająca mowę bezpośrednią\]  
UMMA… X-MA – TAK (W TEN SPOSÓB) (MÓWI) x  
UMMANU – RZEMIEŚLNIK \[sum.\]  
MAR UMMANI – RZEMIEŚLNIK  
UMMANU(M), UMMANU(M) – LUD; ODDZIAŁ; WOJSKO \[sum.\]  
UMME’ANUM \[staroasyr.\] lub UMMI’ANU(M) – WIERZYCIEL; OFIARODAWCA PIENIĘDZY; RZEMIEŚLNIK, MAJSTER,  
MISTRZ  
UMMEATU(M) – LATO  
INA UMMEATIM – LATEM  
UMMI’ANU(M) lub UMME’ANUM – WIERZYCIEL; OFIARODAWCA PIENIĘDZY; RZEMIEŚLNIK, MAJSTER, MISTRZ  
UMMU lub ALAKU, KUNNU – ZAKŁADAĆ  
\[jako rzeczownik\] ZAKŁADANIE  
UMMU(M) – MATKA  
l.mn. UMMATUM  
UMŠU(M) – GORĄCO, UPAŁ  
UMU(M) (jwm) – DZIEŃ; ŚWIATŁO; CZAS, KIEDY \[sum. UD\]  
s.c. ŰM, l.mn. ŰM€, UMÂTE  
UM 4 – CZTERY DNI  
HAMŠAT UMU – PIĘĆ DNI  
UMUM HAMŠUM – PIĄTY DZIEŃ  
INA UM 20 – DWUDZIESTEGO DNIA  
ANA UM 10 – W CIĄGU DZIESIĘCIU DNI  
IN UMIŠU \[staroakad.\] – WÓWCZAS, WTEDY  
INA UMEŠU \[średniobab.\] – PÓŹNIEJ  
UMAM REQAM – DALEKIEGO DNIA = W PRZYSZŁOŚCI  
DARIŠ UMIM – NA ZAWSZE  
UMU ANNIU – DZISIEJSZY DZIEŃ, DZISIAJ  
IŠTU UMIM – PO DNIU  
INA ŰM€ ŠŰMA – W TE DNI, WTEDY, O TYM CZASIE  
INŰMIŠU – WTEDY, O TYM CZASIE; DO TEGO CZASU  
ŰM PÂNI – PRZESZŁE CZASY, PRZED  
UMAKKAL – JEDEN (CAŁY) DZIEŃ  
UMAM – DZIŚ; DZIENNIE  
UM – W DNIU, GDY \[wprowadza zdanie czasowe\]  
UNNEDUKKU(M) – LIST  
UNNINNU – MODLITWA  
UNQU(M) – PIERŚCIEŃ  
UPATINNUM – \[staroas.\] CHŁOPI, PRACOWNICY ROLNI (grupa ludzi pracująca na jednym gruncie), GRUNT  
LENNY \[zapożyczenie z hetyckiego, do którego z kolei przeszło z luwijskiego\]  
UPPULUM, UPPULU – WYZNACZONY NA SPADKOBIERCĘ, SPADKOBIORCA; zob. APALU(M)  
UPPUQU(M): OBROSNIĘTY, ZAROŚNIĘTY  
UPŠAŠŰ – ROBIENIE, CZYNIENIE (? Machenschaft)  
UQNU – LAPIS-LAZULI  
UR – UR \[miasto w południowej Mezopotamii, jeden z najważniejszych ośrodków Sumeru\]  
URIGALLU – WYSOKO POSTAWIONY KAPŁAN  
URKARINNU – DRZEWO BUKSZPANOWE  
URMAŠUM – URMASZU \[imię boga\]  
URPATU lub IRPITU, ERPITU – CHMURA, OBŁOK  
URQITU – ZIELENINA; TRAWA; ROŚLINNOŚĆ  
URRU – DZIEŃ; ŚWIATŁO  
URRI U MUŠI – DNIEM I NOCĄ  
URRAM – RANO; JUTRO  
URRAM U ŠERAM – W PRZESZŁOŚCI, ZAWSZE  
URRUHIŠ – NAGLE, RAPTOWNIE; NATYCHMIAST, ZARAZ, Z MIEJSCA, NA POCZEKANIU  
URRUKU(M) – PRZEDŁUŻYĆ \[D od ARAKU(M)\]  
\[jako rzeczownik\] PRZEDŁUŻANIE  
URRUKU(M) – PRZEDŁUŻONY  
URŠANAKU – WOJOWNICZY  
USÂTU – POMOC, WSPARCIE, POKRZEPIENIE  
s.c. USÂT  
USUKKU(M) – POŚLADEK  
UŠŠE lub UŠŠU – PODSTAWA, FUNDAMENT  
UŠŠU lub UŠŠE – j.w.  
UŠŠURU(M): DOPROWADZONY DO PORZĄDKU, PORZĄDKOWANY, UPORZĄDKOWANY; POKRZEPIONY  
UŠŠUŠU – ZMĘCZONY, UDRĘCZONY, ZADRĘCZONY  
UŠŠUŠU – MĘCZYĆ, ZADRĘCZYĆ, DRĘCZYĆ \[D od AŠŠAŠU\]  
\[jako rzeczownik\] MĘCZENIE, DRĘCZENIE  
UŠU lub EŠU – DRZEWO HEBANOWE  
UŠUZZU, UŠUZZU – zob. NAZAZU(M)  
UTARU \[asyr.\] lub WATARU(M) (i) – PRZEKRACZAĆ PRZECIĘTNOŚĆ; PRZEWYŻSZAĆ; BYĆ KOLOSALNYM, BYĆ  
ZBYT WIELKIM  
\[jako rzeczownik\] PRZEKRACZANIE PRZECIĘTNOŚCI; PRZEWYŻSZANIE; BYCIE KOLOSALNYM, BYCIE  
ZBYT WIELKIM  
D UTTURU(M): CZYNIĆ KOLOSALNYM  
\[jako rzeczownik\] CZYNIENIE KOLOSALNYM  
ANA KAT x UTTURU – KAZAĆ PRZEKROCZYĆ KWOTĘ x  
UTIRU? – PRZEKRACZAJĄCY PRZECIĘTNOŚĆ; PRZEWYŻSZAJĄCY  
UTRU? – KOLOSALNY, ZBYT WIELKI  
UTAŠŠUŠU – BYĆ UNIESZCZĘŚLIWIONYM, DOPROWADZONYM DO CIERPIENIA \[DT od AŠAŠU\]  
\[jako rzeczownik\] BYCIE UNIESZCZĘŚLIWIONYM, DOPROWADZONYM DO CIERPIENIA  
UTLU – BIODRO; UDO; KOLANA (kiedy się mówi o siedzeniu na kolanach); WNĘTRZE, ŁONO (ziemi)  
s.c. UTUL, l.pdw. UTLÂ  
UTNENNU – ŻARLIWIE SIĘ MODLIĆ; zob. \[ENU\]  
\[jako rzeczownik\] MODLENIE SIĘ Z ŻARLIWOŚCIĄ  
UTUKKU(M) – DOBRY DUCH, PRZYJAZNY DEMON \[sum.\]  
UTULUM – zob. ITULUM  
UTTURU(M) – KOLOSALNY, UCZYNIONY KOLOSALNYM  
UZNU(M) – UCHO  
l.pdw. UZNAN  
UZUZZU, UZUZZU – zob. NAZAZU(M)

W

WA’IRU(M), WA’IRU(M) – ZACZYNAJĄCY; WYRUSZAJĄCY  
WABALU(M), WABALU(M) (a/i) lub ABALU(M) – SKŁANIAĆ; PODNOSIĆ; PRZYNIEŚĆ, NOSIĆ, ODNOSIĆ; NABAWIAĆ  
SIĘ; ZDOBYWAĆ  
\[jako rzeczownik\] SKŁANIANIE; PODNOSZENIE; PRZYNOSZENIE, NOSZENIE, ODNOSZENIE; NABAWIA-  
NIE SIĘ; ZDOBYWANIE  
GT ITBULU(M), ITBULU(M): ZABIERAĆ, ODBIERAĆ  
\[jako rzeczownik\] ZABIERANIE, ODBIERANIE  
Š ŠUBULU(M), ŠUBULU(M): KAZAĆ PRZYNOSIĆ; KAZAĆ PRZESYŁAĆ; KAZAĆ POSYŁAĆ; KAZAĆ  
PRZEWOZIĆ  
\[jako rzeczownik\] NAKAZANIE PRZYNIESIENIA; NAKAZANIE PRZESYŁANIE; NAKAZANIE POSŁANIA;  
NAKAZANIE PRZEWIEZIENIA  
ŠT ŠUTABULU(M), ŠUTABULU(M): ROZWAŻAĆ, PRZEMYŚLIWAĆ; ZDECYDOWAĆ SIĘ, POSTANOWIĆ  
\[jako rzeczownik\] ROZWAŻANIE, PRZEMYŚLIWANIE; DECYDOWANIE SIĘ, POSTANAWIANIE  
ANA LIBBIM WABALUM – WNIEŚĆ, WPROWADZIĆ; DOPASOWAĆ  
LIBBAŠU UBBAL ANA – SERCE JEGO SKŁANIA GO DO  
QATATE ANA LEMNETTI INA LIBBI x UBALU – RĘCE W ZŁYM ZAMIARZE PODNIEŚĆ NA x  
WABALUM \[wariant BABALU(M)\] – PRZYNIEŚĆ, NIEŚĆ \[Prs i Prt G nie używane\], DONOSIĆ, PRZYNOSIĆ  
\[jako rzeczownik\] PRZYNIESIENIE, NOSZENIE, DONOSZENIE, PRZYNOSZENIE  
N NABBULU(M)?: BYĆ PRZYNIESIONYM  
\[jako rzeczownik\] BYCIE PRZYNIESIONYM  
WACA’UM \[staroasyr.\] lub WACŰ(M) (wci), UCA’U – WYCHODZIĆ, ODCHODZIĆ, UCIEKAĆ, REZYGNOWAĆ; WYCIĄ-  
GAĆ; WYSTAWAĆ; (w St) ZWISAĆ  
{jako rzeczownik\] WYCHODZENIE, ODCHODZENIE, UCIEKANIE, REZYGNOWANIE; WYCIĄGANIE;  
WYSTAWIANIE  
GT ITCU’U: ODCHODZIĆ, ODDALAĆ SIĘ  
\[jako rzeczownik\] ODCHODZENIE, ODDALANIE SIĘ  
Š ŠUCU’U: WYPROWADZAĆ, KAZAĆ WYJŚĆ; WYSTĄPIĆ (z brzegów), WYLAĆ; WYNOSIĆ, WYWLEC,  
ODCIĄGNĄĆ; UWOLNIĆ; DOPROWADZIĆ DO UPADKU; ZDRADZIĆ (tajemnicę), ŚCIĄGNĄĆ (daninę)  
\[jako rzeczownik\] WYPROWADZANIE, NAKAZANIE WYJŚCIA; WYSTĄPIENIE (z brzegów), WYLANIE;  
WYNOSZENIE, WYWLECZENIE, ODCIĄGANIE; UWALNIANIE; DOPROWADZANIE DO UPADKU;  
ZDRADZANIE (tajemnicy); ŚCIĄGANIE (daniny)  
ŠTN ŠUTACU’U: CIĄGLE WYGADYWAĆ SIĘ, WYPAPLAĆ RAZ ZA RAZEM  
\[jako rzeczownik\] WYGADYWANIE SIĘ RAZ PO RAZ, CZĘSTE WYPAPLYWANIE  
ANA CITIM ŠUCUM – WYDAWAĆ  
WACŰ(M) (wci) lub ACŰ, UCA’U, WACA’UM – WYCHODZIĆ, ODCHODZIĆ, UCIEKAĆ, REZYGNOWAĆ; WYCIĄ-  
GAĆ; WYSTAWAĆ; (w St) ZWISAĆ  
\[jako rzeczownik\] WYCHODZENIE, ODCHODZENIE, UCIEKANIE, REZYGNOWANIE; WYCIĄGANIE;  
WYSTAWIANIE; ZWISANIE  
GT ITCŰ: ODCHODZIĆ, ODDALAĆ SIĘ  
\[jako rzeczownik\] ODCHODZENIE, ODDALANIE SIĘ  
Š ŠUCŰ: WYPROWADZAĆ, KAZAĆ WYJŚĆ; WYSTĄPIĆ (z brzegów), WYLAĆ; WYNOSIĆ, WYWLEC,  
ODCIĄGNĄĆ; UWOLNIĆ; DOPROWADZIĆ DO UPADKU; ZDRADZIĆ (tajemnicę), ŚCIĄGNĄĆ (daninę)  
\[jako rzeczownik\] WYPROWADZANIE, NAKAZANIE WYJŚCIA; WYSTĄPIENIE (z brzegów), WYLANIE;  
WYNOSZENIE, WYWLECZENIE, ODCIĄGANIE; UWALNIANIE; DOPROWADZANIE DO UPADKU;  
ZDRADZANIE (tajemnicy); ŚCIĄGANIE (daniny)  
ŠTN ŠUTACŰ: CIĄGLE WYGADYWAĆ SIĘ, WYPAPLAĆ RAZ ZA RAZEM  
\[jako rzeczownik\] WYGADYWANIE SIĘ RAZ PO RAZ, CZĘSTE WYPAPLYWANIE  
ANA CITIM ŠUCUM – WYDAWAĆ  
WACŰ(M) – ZBIEGŁY, ZREZYGNOWANY; WYCIĄGNIĘTY, WYSTAWIONY  
WACŰ(M) – WYCHODZĄCY; ODCHODZĄCY, UCIEKAJĄCY, REZYGNUJĄCY; WYCIĄGAJĄCY; WYSTAWAJĄCY; ZWISA-  
JĄCY  
WÂCU(M) – BYĆ MAŁYM  
\[jako rzeczownik\] BYCIE MAŁYM  
WACU(M) – MAŁY  
WACŰTU(M) – ODEJŚCIE  
WACŰTAM ALAKUM – ODCHODZIĆ  
WADŰ lub ADŰ (wd’) – POROZUMIENIE POTWIERDZONE PRZYSIĘGĄ, UKŁAD ZAPRZYSIĘŻONY, SOJUSZ UMOC-  
NIONY PRZYSIĘGĄ; POROZUMIENIE; OKREŚLENIE, USTANOWIENIE, ZGODA, POSTANOWIENIE, PO-  
LECENIE, ZASADA; PRZYSIĘGA, ŚLUBOWANIE; PRZYKAZANIE  
l. mn. (W)AD€  
B€L AD€ – SOJUSZNIK, KTÓRY ZAPRZYSIĄGŁ WIERNOŚĆ, ZŁOŻYŁ PRZYSIĘGĘ  
WADŰ lub ADŰ (wd’) – USTANAWIAĆ; PRZYSIĘGAĆ, KLĄĆ SIĘ  
\[jako rzeczownik\] USTANAWIANIE; PRZYSIĘGANIE, ZAKLINANIE SIĘ  
D UDDŰ?: USTANAWIAĆ NA STAŁE  
\[jako rzeczownik\] USTANAWIANIE NA STAŁE  
WADŰ – USTANOWIONY; ZAPRZYSIĘŻONY  
WADŰ – USTANOWIAJĄCY; ZAPRZYSIĘGAJĄCY, ZAKLINAJĄCY SIĘ  
WAKLU(M) – STRAŻNIK, DOZORCA  
WAKIL 10 CABIM – STRAŻNIK PILNUJĄCY DZIESIĘCIU LUDZI  
WALADU(M) (a/i) – RODZIĆ; ŚWIADCZYĆ, ŚWIADKOWAĆ  
\[jako rzeczownik\] RODZENIE; ŚWIADCZENIE, ŚWIADKOWANIE  
WALIDU(M) – RODZĄCY; ŚWIADCZĄCY, ŚWIADKUJĄCY  
WALDU(M) – URODZONY  
WAPŰ(M) – BYĆ WIDOCZNYM  
\[jako rzeczownik\] BYCIE WIDOCZNYM  
WAPŰ(M) – WIDOCZNY  
WAQARU(M) – BYĆ DROGOCENNYM  
\[jako rzeczownik\] BYCIE DROGOCENNYM  
WAQRU(M) – DROGOCENNY  
WARADU(M) (a/i) lub ARADU – SCHODZIĆ, ZSTĘPOWAĆ; ZJEŻDŻAĆ; PRZENIEŚĆ SIĘ; IŚĆ  
\[jako rzeczownik\] SCHODZENIE, ZSTĘPOWANIE; ZJEŻDŻANIE; PRZENOSZENIE SIĘ; CHODZENIE  
WARAQU(M) (i) – BYĆ ZIELONYM, BYĆ ŻOŁTYM; (przenośnie to samo o cerze)  
\[jako rzeczownik\] BYCIE ZIELONYM, BYCIE ŻÓŁTYM (także o cerze)  
WARDATU lub ARDATU – PANNA (na wydaniu); KOBIETA; NIEWOLNICA  
WARDAT LILÎ lub ARDAT LILÎ – DEMON RODZAJU ŻEŃSKIEGO  
WARDU(M) – SCHODZĄCY, ZSTĘPUJĄCY; ZJEŻDŻAJĄCY; PRZYNOSZĄCY SIĘ; IDĄCY  
WARDU(M), WARDU(M) lub ARDU(M) – SŁUGA, PAROBEK; NIEWOLNIK \[także sum.\]  
WARDUTU lub ARDUTU – SŁUŻBA; NIEWOLA  
WARHIŠAM lub ARHIŠAM – MIESIĘCZNIE  
WARHU (wrh) lub ARHU – MIESIĄC  
S.C.: ARAH, l.mn.: ARH€, ARHI, ARHANI  
CÎT ARHI – WSCHÓD KSIĘŻYCA, NÓW  
INA ARAH UM€ (UMÂTE) – W CIĄGU MIESIĄCA  
ŠA ARHI – COMIESIĘCZNIE  
WARIDU(M) – SCHODZĄCY, ZSTĘPUJĄCY; ZJEŻDŻAJĄCY; PRZENIESIONY  
WARKA lub IŠTU – SKORO, GDY \[w zdaniach czasowych\]  
WARKANU(M) – PÓŹNIEJ  
WARKAT – ZA, POZA  
WARKATU(M) – COŚ CO LEŻY Z TYŁU; ODWROTNA STRONA; SPADEK; STAN FAKTYCZNY, STAN RZECZY;  
SPRAWA, INTERES  
ARKAT AMARU – PATRZEĆ DO TYŁU  
ARKATI CABATU – NASTĘPOWAĆ  
ARKAT ŠATTI – OKRES PO PORZE ŻNIW  
WARKITU lub WARKŰ (M), ARKITU, WARKIUM – TYLNI; PÓŹNIEJSZY; PRZYSZŁY  
ANA WARKÂT UMI – DLA PRZYSZŁOŚCI  
WARKIKA – PO TOBIE  
IŚTU WARKIŠU – ODKĄD JEST MARTWY  
WARKIUM, WARKIUM lub WARKITU, ARKITU, WARKŰ(M) – j.w.  
WARKŰ(M), WARKŰ(M) lub WARKITU, ARKITU, WARKIUM – j.w.  
WARQU lub ARQU – ZIELONY; ŻÓŁTY; każdy odcień z przedziału kolorów pomiędzy zielonym a żółtym  
WARŰ(M) (wru) lub ARŰ – PROWADZIĆ  
\[jako rzeczownik\] PROWADZENIE  
GTN: PROWADZIĆ CIĄGLE; WCIĄŻ SPROWADZAĆ  
\[jako rzeczownik\] PROWADZENIE CIĄGLE; SPROWADZANIE RAZ PO RAZ  
Š ŠURŰ(M): KAZAĆ PROWADZIĆ; KAZAĆ SPROWADZIĆ  
\[jako rzeczownik\] NAKAZANIE SPROWADZENIA, NAKAZANIE PROWADZENIA  
WARŰ(M) – PROWADZONY, SPROWADZONY  
WARŰ(M) – PROWADZĄCY  
WÂRU(M) (w’r), WÂRU(M) – ZACZĄĆ; WYRUSZYĆ (?)  
\[jako rzeczownik\] ZACZYNANIE; WYRUSZANIE?  
D WURRU(M), WURRU(M)? – POLECIĆ  
\[jako rzeczownik\] POLECANIE  
WARU(M), WARU(M) – ZACZĘTY  
WASAMU(M) – NALEŻEĆ DO KOGOŚ  
\[jako rzeczownik\] NALEŻENIE DO KOGOŚ  
WAŠABU(M) lub AŚABU(M) (a/i) – SIADAĆ; ZASIADAĆ (w sądzie); MIESZKAĆ, OSIEDLAĆ SIĘ; PRZEBYWAĆ, ZNAJ-  
DOWAĆ SIĘ  
\[jako rzeczownik\] SIADANIE, ZASIADANIE (w sądzie); MIESZKANIE, OSIEDLANIE SIĘ; PRZEBYWANIE,  
ZNAJDOWANIE SIĘ  
Š ŠUŠUBU(M): POSADZIĆ, KAZAĆ SIADAĆ; OSIEDLAĆ  
\[jako rzeczownik\] POSADZENIE, USADZENIE, KAZANIE SIADAĆ; OSIEDLANIE  
INA CER x WAŠABU – SIADAĆ NA x \[możliwe też konstrukcje przechodnie\]  
INA HARRANI (W)AŠIB – JEST W PODRÓŻY HANDLOWEJ, PODRÓŻUJE W CELACH HANDLOWYCH  
INA PAŠŠURI ŠUŠUBU – KAZAĆ UMIEŚCIĆ NA TABLICZCE, W DOKUMENCIE  
WAŠARU(M) (a/i) lub MAŠARU – BYĆ LUŹNYM; OPUSZCZAĆ (miejsce)  
\[jako rzeczownik\] BYCIE LUŹNYM; OPUSZCZANIE (miejsca)  
D WUŠŠURU(M): POLUŹNIAĆ, ROZLUŹNIAĆ; ZWALNIAĆ, WYPUSZCZAĆ NA WOLNOŚĆ; POŚWIĘCAĆ,  
REZYGNOWAĆ; ODPROWADZIĆ, UNIEŚĆ (łup); PORZUCAĆ, ZOSTAWIAĆ NA LODZIE; ZNIKAĆ  
\[jako rzeczownik\] ODPROWADZANIE, UNOSZENIE (łupu); POLUŹNIANIE, ROZLUŹNIANIE; ZWALNIANIE,  
WYPUSZCZANIE NA WOLNOŚĆ; POŚWIĘCANIE, REZYGNOWANIE; PORZUCANIE, ZOSTAWIANIE NA  
LODZIE; ZNIKANIE  
DT UTAŠŠURU(M): BYĆ ROZLUŹNIONYM  
\[jako rzeczownik\] BYCIE ROZLUŹNIONYM  
ŠINA MUŠŠURAMA RAMANUŠŠIN – SAMI SIĘ ODDALI (? Sie waren sich selbst überlassen)  
WAŠIBU(M) – SIEDZĄCY; ZASIADAJĄCY (w sądzie); MIESZKAJĄCY, OSIEDLAJĄCY SIĘ;PRZEBYWAJĄCY, ZNAJDUJĄCY  
SIĘ  
WASIMU(M) – NALEŻACY DO KOGOŚ  
WAŠIRU(M) – OPUSZCZAJĄCY (miejsce)  
WAŠRU(M) – LUŹNY  
WATARU(M) (i) lub UTARU – PRZEKRACZAĆ PRZECIĘTNOŚĆ; PRZEWYŻSZAĆ; BYĆ KOLOSALNYM, BYĆ ZBYT  
WIELKIM  
\[jako rzeczownik\] PRZEKRACZANIE PRZECIĘTNOŚCI; PRZEWYŻSZANIE; BYCIE KOLOSALNYM, BYCIE  
ZBYT WIELKIM  
D UTTURU(M): CZYNIĆ KOLOSALNYM  
\[jako rzeczownik\] CZYNIENIE KOLOSALNYM  
ANA KAT x UTTURU – KAZAĆ PRZEKROCZYĆ KWOTĘ x  
WATIRU – PRZEKRACZAJĄCY PRZECIĘTNOŚĆ; PRZEWYŻSZAJĄCY  
WATRU – KOLOSALNY, ZBYT WIELKI  
WATRU lub ATRU – PRZEWYŻSZAJĄCY, NADZWYCZAJNY, NIEZWYKŁY; ZBĘDNY; (Amm.:) DODATKOWY  
KIMA ATARTIMMA – „WEDŁUG ZYSKU”, „STOSOWNIE DO ZYSKU”, „TYLE (Z TEGO) BYŁO DO DYS-  
POZYCJI”  
WATŰ(M) (wta) – ZNAJDOWAĆ  
\[jako rzeczownik\] ZNAJDOWANIE  
WATŰ(M) – ZNALEZIONY  
WATŰ(M) – ZNAJDUJĄCY, ZNALAZCA  
WILDU(M) – POTOMSTWO  
WUDDŰ? – USTANOWIONY NA STAŁE  
WURRU(M), WURRU(M)? – POLECONY  
WUŠŠURU(M) – POLUŹNIONY, ROZLUŹNIONY; ZWOLNIONY, WYPUSZCZONY NA WOLNOŚĆ; POŚWIĘCONY, PORZUCO-  
NY; ODPROWADZONY, UNIESIONY (łup); PORZUCONY, ZOSTAWIONY NA LODZIE; ZAGINIONY  
Z

ZA’ARU lub ZA’IRU, ZAJARU – WRÓG  
ZA’INU – NAPEŁNIAJĄCY; POWODUJĄCY NABRZMIENIE  
ZA’IRU lub ZA’ARU, ZAJARU – WRÓG  
ZA’IRU – POGARDZAJĄCY, GARDZĄCY; NIENAWIDZĄCY  
ZA’IZU(M) – DZIELĄCY; OTRZYMUJĄCY UDZIAŁ  
ZABALU (i) – NIEŚĆ (ciężar); PONOSIĆ (winę)  
\[jako rzeczownik\] NIESIENIE (ciężaru); PONOSZENIE (winy)  
ZABALU(M) (i) – ROZPOWIADAĆ, ZDRADZAĆ  
\[jako rzeczownik\] ROZPOWIADANIE, ZDRADZANIE  
ZABBILUM – TRAGARZ  
ZABILU – NIOSĄCY (ciężar); PONOSZĄCY (winę)  
ZABILU(M) – ROZPOWIADAJĄCY, ZDRADZAJĄCY  
ZABLU – NIESIONY; PONOSZONY (o winie)  
ZABLU(M) – WYGADANY, ZDRADZONY  
ZAJARU lub ZA’IRU, ZA’ARU – WRÓG  
ZAKARU (a/u) lub SAQARU, ŠAQARU – MÓWIĆ, POWIEDZIEĆ; WZYWAĆ, WOŁAĆ; MODLIĆ SIĘ; POROZUMIEĆ SIĘ,  
DOGADAĆ SIĘ; NAZYWAĆ; PRZYSIĘGAĆ, ZAKLINAĆ SIĘ; DOKONAĆ, WYWOŁAĆ, STWORZYĆ  
\[jako rzeczownik\] MÓWIENIE, POWIEDZENIE; WZYWANIE, WOŁANIE; MODLENIE SIĘ; POROZUMIE-  
WANIE SIĘ, DOGADYWANIE SIĘ; NAZYWANIE; PRZYSIĘGANIE, ZAKLINANIE SIĘ; DOKONANIE, WY  
WOŁYWANIE, TWORZENIE  
GT ZITKURU: POROZUMIEWAĆ SIĘ; MÓWIĆ, INFORMOWAĆ, POWIADOMIĆ  
\[jako rzeczownik\] POROZUMIEWANIE SIĘ; MÓWIENIE, INFORMOWANIE, POWIADAMIANIE  
Š ŠUZKURU: KAZAĆ MÓWIĆ, ZMUSIĆ DO MÓWIENIA; RZUCIĆ (klątwę); ZAPRZYSIĄC, KAZAĆ ZŁO-  
ŻYĆ PRZYSIĘGĘ (z ADU, NIŠ QATI, NIŠ ILANI)  
\[jako rzeczownik\] NAKAZANIE MÓWIENIA, ZMUSZENIE DO MÓWIENIA; RZUCANIE (klątwy); ZAPRZY-  
SIĘGANIE, NAKAZANIE ZŁOŻENIA PRZYSIĘGI  
ZAKIRU – MÓWIĄCY; WZYWAJĄCY, WOŁAJĄCY;MODLĄCY SIĘ; POROZUMIEWAJĄCY SIĘ, DOGADUJĄCY SIĘ; NAZYWA-  
JĄCY; PRZYSIĘGAJĄCY, ZAKLINAJĄCY SIĘ; DOKONUJĄCY, WYWOŁUJĄCY, TWORZĄCY  
ZAKRU – WYPOWIEDZIANY; WEZWANY, ZAWOŁANY; WYMODLONY; DOGADANY; NAZWANY; ZAPRZYSIĘŻONY; DOKO-  
NANY, WYWOŁANY, TWORZONY  
ZAKKARU – CHŁOPIEC  
ZAMÂNU – ZŁY, WROGI; WRÓG, PRZECIWNIK  
l.mn. ZAMÂNI, ZAMÂN€  
ZAMARU(M) (u) – ŚPIEWAĆ, OPIEWAĆ  
\[jako rzeczownik\] ŚPIEWANIE, OPIEWANIE  
ZAMIRU(M) – ŚPIEWAJĄCY, OPIEWAJĄCY  
ZAMMARU lub ZAMMERU – ŚPIEWAK, MUZYKANT  
ZAMMERU lub ZAMMARU – j.w.  
ZAMRU(M) – WYŚPIEWANY, ZAŚPIEWANY, OPIEWANY  
ZANANU(M), ZANANU(M) (u) – PADAĆ (o deszczu), LAĆ  
\[jako rzeczownik\] PADANIE (deszczu), LANIE  
ŠUZUNNUM – SPOWODOWAĆ DESZCZ, SPROWADZIĆ DESZCZ  
\[jako rzeczownik\] SPOWODOWANIE DESZCZU, SPROWADZENIE DESZCZU  
ZANINU(M), ZANINU(M) – PADAJĄCY (deszcz), LEJĄCY  
ZANNU(M), ZANNU(M) – SPADŁY (deszcz)  
ZANU lub CANU (a) – NAPEŁNIAĆ; SPOWODOWAĆ NABRZMIENIE, DOPROWADZIĆ DO NABRZMIENIA  
\[jako rzeczownik\] NAPEŁNIANIE; SPOWODOWANIE NABRZMIENIA; DOPROWADZANIE DO NA-  
BRZMIENIA  
ZANU – NAPEŁNIONY; NABRZMIAŁY, DOPROWADZONY DO NABRZMIENIA  
ZÂNU(M) (z’n) – BYĆ OZDOBIONYM \[St\]  
\[jako rzeczownik\] BYCIE OZDOBIONYM  
ZANU(M) – OZDOBIONY  
ZAPRU lub CAPRU – ZŁY; KIEPSKI, NIEPOMYŚLNY, ZŁY; BRZYDKI, SZPETNY, SZKARADNY  
ZAPURTU lub CAPURTU – NIESZCZĘŚCIE; MĘKA, KATUSZE  
ZAQATU(M) (a/u) – UGRYŹĆ, UKĄSIĆ  
\[jako rzeczownik\] GRYZIENIE, KĄSANIE, UGRYZIENIE, UKĄSZENIE  
ZAQIKU lub ŠARU – WIATR, HURAGAN  
ZAQITU(M) – GRYZĄCY, KĄSAJĄCY  
ZAQTU(M) – UGRYZIONY, POKĄSANY  
ZARU (a, i) – POGARDZAĆ; NIENAWIDZIEĆ  
\[jako rzeczownik\] POGARDZANIE; NIENAWIDZENIE  
ZARU – POGARDZONY, WZGARDZONY; ZNIENAWIDZONY  
ZAZU – STAĆ; STAWIAĆ; WYSTĘPOWAĆ JAKO ŚWIADEK  
ZÂZU(M) (zuz) (a/u) – DZIELIĆ; OTRZYMYWAĆ UDZIAŁ  
\[jako rzeczownik\] DZIELENIE; OTRZYMYWANIE UDZIAŁU  
D ZUZZU(M): PODZIELIĆ, ROZDZIELIĆ  
\[jako rzeczownik\] PODZIELENIE, ROZDZIELENIE  
INA x ZÂZU – OTRZYMYWAĆ UDZIAŁ Z x  
ANA (ŠENA) ZÂZUM – DZIELIĆ NA (DWIE) CZĘŚCI, DZIELIĆ NA (DWOJE)  
ZAZU(M) – PODZIELONY; OTRZYMANY JAKO UDZIAŁ  
ZERU, ZERU – ZIARNO SIEWNE; NASIENIE, ZIARNO; POTOMSTWO; ROLA, POLE  
ZIBBATU(M) – OGON; KONIEC  
ZIBU – SZAKAL  
ZIHHU(M) – PĘCHERZYK \[cecha wątroby\]  
ZIKARU(M) lub ZIKRU – MĘŻCZYZNA; CHŁOPIEC; MĘSKI; BOHATER; BOHATERSKI  
s.c. ZIKAR  
ZIKAR SINIŠ – MĘŻCZYZNA I KOBIETA = WSZYSCY  
ZIKRU lub ZIKARU(M) – j.w.  
ZIKRU lub ŠUMU, NIŠU – IMIĘ  
ZINNU(M) – DESZCZ, ULEWA  
ZITTU(M) – UDZIAŁ; ŁUP  
ZŰ lub CŰ (c”) – NAWÓZ; EKSKREMENTY, ODCHODY, KAŁ; MOCZ  
ZU lub IMBARU – HURAGAN  
ZUMBU – MUCHA  
ZUMRU(M) lub ŠIRU – CIAŁO; KORPUS  
ZUNNU – DESZCZ  
ZUQAQÎRU lub AKRABU, AQRABU – SKORPION (zarówno zwierzę jak i znak zodiaku)  
ZUQIQIPU(M) – SKORPION