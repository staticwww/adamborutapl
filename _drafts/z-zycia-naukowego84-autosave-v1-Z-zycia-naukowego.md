---
id: 86
title: 'Z życia naukowego'
date: '2014-11-09T13:26:55+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/09/84-autosave-v1/'
permalink: '/?p=86'
---

###### *Oto efekt badań przeprowadzonych w ramach projektu „Wpływ pracy naukowo-dydaktycznej na uczelni wyższej na życie codzienne kadry profesorskiej”. Z siedemnastomiesięczych nagrań z salonu Profesora program komputerowy wybrał sześć powiązanych treściowo fragmentów tworzących ciąg przyczynowo-skutkowy. Okazało się przy tym, że program wyposażono w moduł sceniczny)* 

##### **Tragedia kryminalna z kręgów profesury**

(w pięciu odsłonach wierszem)

Osoby: Profesor, jego żona, narrator i napięcie

ODSŁONA PIERWSZA

*Żona:*  (szepce coś profesorowi do ucha)

*Profesor:* (z wyrzutem)

 **Moja najmilsza, czym przykry ci ja,**

 **Że mi szpileczki co chwila wbijasz?**

*Napięcie:* (pojawia się nieśmiało)

ODSŁONA DRUGA

*Żona:*  (znów szepce profesorowi do ucha)

*Profesor:* (obrusza się)

 **Dajże mi spokój, moja królowo,**

 **Bo co za dużo, to i niezdrowo!**

*Napięcie:* (przyrasta)

##### ODSŁONA TRZECIA

*Żona:*  (jak wprzódy)

*Profesor:* (z gniewem)

 **Dość tego, żono, bo to nieładnie**

 **Gdy się do męża mówi szkaradnie!**

*Napięcie:*  (czuje się jak u siebie)

##### ODSŁONA CZWARTA

*Żona:*  (jak wyżej)

*Profesor:* (wybucha)

 **Dalibóg, piekło z tego małżeństwa!**

 **Ty mnie przywodzisz do szaleństwa!**

*Napięcie:* (nabrzmiewa)

##### ODSŁONA PIĄTA

*Żona:*  (nareszcie głośno)

 **Jakiś ty dobry, jakiś ty fajny.**

 **Ty zawsze będziesz *nadzwyczajny*.**

*Profesor:* (popełnia czyn przestępny)

*Badacz:*  (komentuje)

 **Zaniósł się szlochem, chwycił siekierę**

 **I przeniósł w niebyt żonę heterę.**

*Napięcie:*  (pęka z hukiem)

KURTYNA SPADA

**Utw0ry okolicznościowe z okazji powołania gazetki ściennej dla personelu naukowo-dydaktycznego WSP w Zielonej Górze**

*W kąciku poezji zamieszczamy poemat wyróżniający się prostotą wyrażanych uczuć i środków wyrazu. Napisał go pragnący zachować anonimowość asystent z Wydziału Matematyki Fizyki i Techniki poruszony wieścią o powołaniu Kolegium Redakcyjnego. Anonim opatrzył swój utwór tytułem „Dobra Nowina”; wszakże nagłówek ów wydaje się redakcji nazbyt pompatyczny. Czy nie brzmiałoby lepiej: „Wielka Inicjatywa”?*

**(problem)**

Pod krawatem, wygolony

Wychowany, wyszkolony,

Elegancki i pachnący,

Tchnący, lśniący i wionący,

Moralnością przepełniony,

Twórczo, światle zamyślony…

Taki okaz asystenta

Nagle zgina się i stęka;

Nagle słania się i pada,

Puszcza teczkę, puszcza pawia,

Rzuca mięsem, lży zwierzchników

I przeklina polityków,

Boskie też obraża uszy –

Szczęściem czkawka wszystko głuszy.

**(rozwiązanie)**

Leży szczęściem upojony,

A nie – chory czy szalony;

Nie na fleku, nie na gazie,

Lecz w ekstazie, lecz w ekstazie!

Bo ma wreszcie organ własny,

Nic, że ścienny, nic, że ciasny.

**(c.b.d.o.)**

*Adiunkt Y przeżył tragedię, która kto wie, czy nie zahamuje jego habilitacji. Niech Was nie zwiedzie pozornie filuterny tytuł; okrzyk rozpaczy, który bije z poniższych strof, ma być ostrzeżeniem dla jego braci w adiunkturze. Strzeżcie się ci, którzy zdążacie do raju samodzielności! Baczcie na od węża kusicielki! A ty, szlauchu, bądź przeklęty.*

**Kochaj mądrość, a nie – filuj Zofię.**



Przyszła Zofia do doktora

I powiada: „Jestem chora.

Brak mi czegoś, źle się czuję,

Może mnie pan zoperuje?

Jak się pan uwinie ładnie,

Cosik pewnie panu wpadnie”.

Nie odmówił pannie Zofii,

Boć to doktor filo-zofii;

Choć nie medyk, lecz amator,

Przecież zręczny operator.

Tak jak chciała, wypadł ładnie…

Aż za ładnie – a więc wpadnie.