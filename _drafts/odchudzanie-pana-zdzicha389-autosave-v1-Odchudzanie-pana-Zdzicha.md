---
id: 393
title: 'Odchudzanie pana Zdzicha'
date: '2015-05-03T14:53:21+02:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2015/04/389-autosave-v1/'
permalink: '/?p=393'
---

ODCHUDZANIE PANA ZDZICHA

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/05/Odchudzanie-pana-Zdzicha.pdf)

Pan Zdzicho przeżył przełom mentalny i uzgodnił z żoną, iż od tej pory stosunki małżeńskie kształtować będą wedle najlepszych wzorców dyplomacji międzynarodowej i kanonów poprawności politycznej.

Toteż kiedy małżonka pana Zdzicha wygłosiła memorandum ustne poświęcone potrzebie pilnego odchudzenia pana Zdzicha, pan Zdzicho okazał się być za, a nawet zadeklarował sam z siebie, że tym razem nie oznacza to bynajmniej, że będzie przeciw.

Kiedy pan Zdzicho sięgnął po golonę z bułą, małżonka uściśliła, że interpretuje słowa pana Zdzicha jako zgodę na wdrożenie reżymu odchudzającego w trybie natychmiastowym, co oznacza, iż zamiast golony z bułą pan Zdzicho spożyje dzisiaj kapustę z marchewką.

Pan Zdzicho odparł, iż generalnie solidaryzuje się z branżą ogrodniczą, jednakowoż nie chce uczestniczyć w przemyśle pogardy wymierzonym w działalność hodowlaną. Poprawność polityczna podpowiada, że warzywa właściwiej będzie docenić jutro, bo dziś odtrącenie golony mogłoby być odebrane jako manifestacja przeciwko trzodzie chlewnej, która przecież poświęca życie dla dobra konsumentów, a zatem ludzkości. To pachnie znieczulicą – wyrzekł wyraźnie poruszony.

Małżonka uspokoiła go argumentem, iż tak daleko idące zaangażowanie trzody na rzecz konsumentów, o jakim sam słusznie przed chwilą wspomniał, czyni rzeczą nie do pomyślenia, by owa trzoda nad odchudzanie pana Zdzicha przedłożyła prestiż swoich doczesnych szczątków w postaci golony; przecież odchudzanie jest dla dobra pana Zdzicha, a dobro pana Zdzicha jest dobrem konsumenta! Co więcej – dodała małżonka z niecodziennym u niej uznaniem – dobro pana Zdzicha jest dobrem konsumenta nieprzeciętnego, bo śmiało można założyć, że nie znajdzie się taka świnia, która by miała czelność zaprzeczyć, iż pan Zdzicho oddał się konsumpcji bez reszty i niczemu poza spożyciem nie poświęca uwagi.

Pan Zdzicho skromnie zaprotestował, iż jego oddanie zostało przez połowicę przecenione, gdyż oprócz spożycia bardzo dba o finanse ogniska domowego i teraz żal mu gardło ściska na myśl, jak nieoszczędnym będzie zmarnowanie golony i buły na rzecz warzyw, które mogą przecież poczekać do jutra bez istotnego uszczerbku dla swych właściwości odżywczych – jak by znakomite nie były.

Małżonka uśmierzyła jego szlachetne obiekcje zapewnieniem, że do zmarnowania golony i buły nie dojdzie bynajmniej, gdyż w trosce o dobro męża oraz pomyślność finansową rodziny, spożytkuje i golonę, i bułę osobą własną, rezygnując przy tym wspaniałomyślnie z porcji marchewki i kapusty, które przekaże panu Zdzichowi w charakterze dokładki do jego kapusty z marchewką.

Pan Zdzicho podziękował małżonce, lecz dodał natychmiast, że niegodnym dżentelmena byłoby samolubne wykorzystanie jej dobrych chęci. Nie uchodzi bowiem, by jego, pana Zdzicha, odchudzanie zostało zainicjowane kosztem przerwania zabiegów odchudzających bez rozgłosu podejmowanych przez małżonkę od lat. A że te zabiegi nie przynoszą pożądanych skutków, a nawet – przeciwnie – problem wyraźnie przybiera na wadze, zadbanie o gabaryty małżonki wydaje się pilniejszym zadaniem niż analogiczna troska o skromną posturę pana Zdzicha. Dlatego lepiej i dla obojga małżonków, i dla dzielnej trzody chlewnej, a też dla szeroko pojętego rolnictwa jako całości , będzie spożytkowanie golony z bułą przez pana Zdzicha.

Małżonka w odpowiedzi wyraziła zaniepokojenie o pana Zdzicha, u którego prócz symptomów nadmiernego spożycia ujawniły się oznaki zaburzenia percepcji , zwłaszcza widzenia trójwymiarowego. Co gorsza, nie można wykluczyć zmian osobowościowych, skoro image bywalca lokalu „U Henia”, które pasuje do pana Zdzicha jak ulał, usiłuje on zastąpić ewidentnie obcym mu image dżentelmena. Podanie panu Zdzichowi kolejnej dawki cholesterolu, tłuszczów nienasyconych i szczeciny naskórnej może tylko pogłębić dramat. Dlatego odpowiedzialna za męża, a przy tym oszczędna pani domu, musi usunąć golonę i bułę poza zasięg chwytu pana Zdzicha.

Pan Zdzicho odparł, że wobec słusznych i oczywistych argumentów przemawiających za jego stanowiskiem, oceny i zamiary szanownej małżonki nie mogą być odebrane inaczej niż jako akt niczym nie sprowokowanej agresji. Hipotetycznie rzecz rozważając, nie dałoby się zaprzeczyć, że pan Zdzicho byłby w prawie użyć nawet siły w celu odparcia imperialistycznych ciągot połowicy, uratowania honoru głowy rodziny i zaprowadzenia sprawiedliwego porządku w postaci obgryzienia przezeń kośćca golony i zakąszenia bułą. Jeśli małżonka chce zakończyć konflikt pokojowo, niechże otworzy piwo, które ułatwi golonie i bule przedostanie się przez gardło pana Zdzicha ściśnięte z powodu jaskrawej niesprawiedliwości.

Małżonka nie mogła jednak przystać na łamanie przez pana Zdzicha dopiero co zawartej umowy. W jej kontekście to pan Zdzicho jawił się jako agresor. A wygłoszona przezeń groźba użycia siły tylko to potwierdziła. W tej sytuacji małżonka ostrzegła w osobnej nocie, iż w przypadku czynnej napaści pana Zdzicha poczuje się zmuszona do uruchomienia sojuszników w postaci dzielnicowego, pana Zenka i spokrewnionego kibica, pana Władka.

Pan Zdzicho wystosował protest; to oburzające – stwierdził – że gdy powołał się po prostu na swoje niezbywalne prawa, został niezgodnie z prawdą oskarżony o grożenie użyciem siły. Tymczasem to jego połowica zagroziła mu interwencją popędliwego dzielnicowego, pana Zenka i znanego z brutalności pana Władka, powszechnie znanego jako kibol.

Napięcie narosło do tego stopnia, że niektóre organy pana Zdzicha i jego małżonki bez porozumienia z ośrodkami decyzyjnymi posunęły się do użycia szelek, ściery i latających talerzy.

Wobec tego, że dalsza eskalacja groziłaby nieobliczalnymi skutkami , strony zdecydowały się na arbitraż. Wezwany jako rozjemca pan Henio, znany restaurator, dla usunięcia przyczyny konfliktu ofiarował się spożyć golonę z bułą a kość niezgody po obgryzieniu zdeponować w śmietniku. Z niejasnych powodów to salomonowe rozstrzygnięcie nie zadowoliło jednak adwersarzy.

Wobec groźby klinczu postanowiono wzmocnić arbitraż osobą pani Heli, sąsiadki. Pani Hela zadeklarowała się jako zwolenniczka praw zwierząt i wyraziła opinię, iż najsprawiedliwiej byłoby, gdyby to golona zdecydowała, przez kogo chce być skonsumowana. Ponieważ jednak świnia macierzysta golony padła ofiarą przemysłu spożywczego, na skutek czego i ona sama, i tym bardziej golona zostały wyzute ze świadomości i pozbawione wolnej woli , pani Hela zaproponowała odwołać się do najbliższych krewnych denatki. To śmiałe posunięcie rozbiło się o problem buły. Po przyznaniu prawa decyzji najbliższym golony pojawiło się bowiem pytanie, czy analogiczne prawo nie powinno przysługiwać także tej społeczności zbożowej, której przedstawicieli przetworzono na bułę. Rokowania utknęły na dyskusji o tym, czy świadomość i wolną wolę ma tylko trzoda, czy może też i łan. Strony, nie czując się dostatecznie kompetentne, postanowiły powołać komisję ekspertów.

W ten sposób konflikt został chwilowo zamrożony, a o dalszym rozwoju wypadków będę Państwa sukcesywnie informował.