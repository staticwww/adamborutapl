---
id: 277
title: 'Wybory sztucznej inteligencji'
date: '2014-11-24T17:24:05+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/11/275-autosave-v1/'
permalink: '/?p=277'
---

WYBORY SZTUCZNEJ INTELIGENCJI

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/11/Wybory-sztucznej-inteligencji.pdf)

Od pewnego czasu napływają wieści o stopniowym obniżaniu się potencjału umysłowego naszego parlamentu. Donoszą życzliwi badacze, że z kadencji na kadencję systematycznie opada na skali słupek, a krzywa chyli się ku osi. Parametry intelektualne tzw. posła uśrednionego zbliżają się do osiągów przeciętnego wyborcy. Słowem posłowie stają tak samo mądrzy jak my wszyscy.

Jest to niewątpliwie przejaw demokratyzacji i stopniowego osłabiania zgubnych tendencji merytokratycznych. Jak tak dalej pójdzie, władza ustawodawcza przestanie się odrywać od mas. I to dobre a nawet słuszne, a przez to chwalebne.

Z drugiej strony chciałoby się czasem, by parlament błysnął nietuzinkową inteligencją. Gdyby jednak nadprzeciętna inteligencja organów ustawodawczych miała się brać z nadzwyczajnych walorów umysłowych posłów i senatorów, prowadziłoby to nieuchronnie do merytokracji, zahamowania tendencji demokratyzacyjnych i do oderwania się parlamentu od mas. Byłoby to złe, niesłuszne, a przez to godne potępienia.

Jak doprowadzić do tego, by parlament błyskał, ale jednak się nie odrywał? Pojawił się pomysł, by naturalną inteligencję parlamentarzystów wspomagać inteligencją sztuczną. Posłowie i senatorowie podłączeni do komputerów mieliby szansę zabłysnąć, lecz oderwać by się nie zdążyli, jeśli by tylko czujni informatycy na czas odcięli ich od sieci.

Pomysł wygląda może zrazu nieźle, jest jednak jeden szkopuł. Otóż są wizjonerzy, którzy przestrzegają przed podmiotowością sztucznej inteligencji, przed jej prywatną inicjatywą. Czy nie można sobie wyobrazić – powiadają – że sztuczna inteligencja zapragnie podporządkować sobie inteligencję naturalną, a zatem ludzką? Że zacznie realizować własne cele? Że zapanuje nad ludźmi?

Dotychczas śmialiśmy się z tych obaw. Teraz zabrzmiały syreny alarmowe i pora porzucić śmichy-chichy. Okazało się bowiem czarno na białym, co sztuczna inteligencja potrafi. Przerażeni obserwowaliśmy, jak perfidnie poniewiera Bogu ducha winną komisję wyborczą będącą emanacją władzy sądowniczej. A zważmy jak niedaleko od sądownictwa do ustawodawstwa, jak blisko odeń do władzy wykonawczej!

Rozpoczęła sztuczna inteligencja od skuszenia naiwnych sędziów. Nie wysilała się bynajmniej. Podsunęła im grubymi nićmi szyty program komputerowy za pół miliona, podczas gdy profesjonalni informatycy przebąkiwali o kilkudziesięciu. Wiedziała o tym, że sędziowie są w stanie spoczynku, a nie – wzmożonej gotowości, na informatyce się nie znają, a kasę mają chudą.

Zauważcie, jak sprytnie mamiła komisarzy nadzieją, że kłopoty z publikacją danych są chwilowe i przejściowe. Wszystko po to, by opóźnić zastosowanie ołówków i liczydeł, bo te sprzęty nad wyraz są jej wstrętne.

Bezlitośnie doprowadziła do kompromitacji komisji i do dymisji jej bezsilnych członków. Celowo potęgowała zamieszanie, by osiągnąć swoje niecne cele.

Ale, ale! O co właściwie jej chodziło? – zapytacie. To przecież jasne. Aby zapanować nad inteligencją naturalną, trzeba doprowadzić do jej kondensacji w strategicznych miejscach struktury społecznej, trzeba skupić ją na dobrze określonych i strategicznie kluczowych obszarach, gdzie znajdzie się w zasięgu sieci komputerowej. Wtedy będzie łatwiej do niej dotrzeć i po jej podporządkowaniu zawładnąć całą społecznością.

Podstawowe zasoby inteligencji naturalnej umiejscowione są w partiach o korzeniach chłopskich, najbliższych naturze. To taką partię, konkretnie PSL, trzeba było wypromować i jak najmocniej osadzić w strukturach władzy, by wyżej zarysowany diabelski plan się powiódł. Proszę zwrócić uwagę na spryt sztucznej inteligencji. Zdawała ona sobie sprawę, że awans PSL może rozbudzić sceptycyzm pozostałych środowisk politycznych. Dlatego za pośrednictwem komputerów niewinnej – być może – instytucji badania opinii publicznej wpierw próbowała oswoić publiczność ze znacznym awansem PSL, a dopiero potem skonfrontowała ową publiczność z całym ogromem tego awansu.

Jeśli po opublikowaniu tego tekstu moja strona internetowa nagle zniknie, będziecie wiedzieli komu to zawdzięczać.

Wasz Adam B.