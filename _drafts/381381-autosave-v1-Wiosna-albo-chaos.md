---
id: 384
title: 'Wiosna albo chaos'
date: '2015-03-22T18:33:51+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2015/03/381-autosave-v1/'
permalink: '/?p=384'
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)

WIOSNA ALBO CHAOS

Być może słusznie się obawiał pan Zdzicho o to, że w ślad za zimą inne pory roku też nie nastąpią!

Sądzą niektórzy, że wiosna powinna nastąpić teraz właśnie, czy jednak tak uczyni? Tradycja wymaga od niej, by zaczekała na odstąpienie zimy, jak jednak zima ma odstąpić, skoro nie nastąpiła? Czy nie dość szarga się tradycję, by do tego szargania namawiać jeszcze wiosnę?

Problem jest rozleglejszy, niż się wam wydaje. Skoro zima nie odstąpi, bo pierwej nie nastąpiła, to i wiosna nie nastąpi, a co za tym idzie, nie odstąpi latu po przepisowych trzech miesiącach. No bo jak by miała odstąpić, skoro nie nastąpiła? Zatem lato, nie nastąpiwszy, nie będzie mogło ustąpić pola jesieni , zaś jesień z kolei z analogicznej przyczyny nie ustąpi, by utorować drogę zimie. Oznacza to ruinę naszego tradycyjnego systemu pór roku.

Nie łudźmy się! Znajdą się siły, które wykorzystają tę anomalię klimatyczną! Przypuszczalnie zaatakują pokrewną ostoję tradycji, jaką jest kalendarz. Obszczekają kalendarz, że nie odzwierciedla rzeczywistości, że jest błędny, nieadekwatny, fikcyjny, a zatem zużyty, nieaktualny, wypalony, przeto też niepotrzebny, zbędny, bez pożytku, co gorsza szkodliwy, mylący, wprowadzający w błąd, wobec czego do kasacji, likwidacji, eliminacji.

Czy nie widzicie oczyma wyobraźni, jak skwapliwie wykorzystuje to niecny agresor i zgodnie z wypróbowanymi regułami wojny hybrydowej, wykorzystując prostoduszność naszych poczciwych prawosławnych, wkłada w ich niewinne usta zdradziecki postulat zastąpienia skompromitowanego kalendarza gregoriańskiego rzekomo lepszym kalendarzem juliańskim? A do stodół wciska im wyrzutnie rakietowe Grad, iżby mieli czym argumentować za zmianą?

Czy inni wrogowie tradycji nie zechcą narzucić nam mahometańskiej rachuby czasu, albo – przeżegnajcie się mili – żydowskiej zgoła? Ten drugi przypadek jest politycznie i ekonomicznie wręcz zgubny, bo zastanówcie się, kochani, jak nasz wątły system ubezpieczeń społecznych wytrzyma roszczenia cwanych weteranów pracy, którzy od jutra masowo zaczną przedstawiać dokumenty na to, iż na emeryturę ściubili od roku –dajmy na to – 1970 do roku 5775? Stać ich będzie na to, by manifestować spolegliwość wobec ZUS-u i przyznawać się do nawet kilkuletnich okresów bezskładkowych. – Bo nawet grosza nie chcemy, co by się uczciwie nie należał – będą deklarować z faryzejską obłudą.

Dalej widzę jeszcze. Czy nie pojawią się za chwilę pogubieni naukowcy, którzy owładnięci grzeszną żądzą cytowań nie zawahają się zredagować tez o rewolucji klimatycznej w naszej ojczystej przyrodzie, a potem przybić owe tezy na odrzwiach Instytutu Meteorologii i Gospodarki Wodnej? A ta rewolucja nie pogrzebie aby umiłowanego przez naród cyklu czterech pór roku i nie zastąpi go prymitywnym następstwem pór suchej i deszczowej? Przecież to już z daleka cuchnąć będzie pogaństwem! Pół biedy, jeśli zaprzańcy proponować będą liczenie lat wedle olimpiad. Liczenie od założenia miasta Rzymu może dałoby się z trudem tolerować. Ale czy kto nie nawiąże do wszetecznego Babilonu? To wprowadziłoby rozłam wśród rodaków. Przecież rachuba czasu wedle postępków aktualnego prezydenta nieuchronnie starłaby się z rachubą wedle dokonań prezesa. Zali wypada, by eponimowie obrzucali się epitetami?

Pozostaje liczyć na niespodziewany atak zimy. Ponoć w marcu jak w garncu, a kwiecień to plecień. Niech wróci normalność.

Pan Zdzicho wznosi za to toast w znanym wam lokalu „U Henia”. Znajoma z Nowej Soli niech też za to wychyli.