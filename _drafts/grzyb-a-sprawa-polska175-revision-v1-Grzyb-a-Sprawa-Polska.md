---
id: 177
title: 'Grzyb a Sprawa Polska'
date: '2014-11-11T17:06:29+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/11/175-revision-v1/'
permalink: '/?p=177'
---

GRZYB A SPRAWA POLSKA

Przeróżne okazy znajdujemy w gąszczach jesiennych. Tu przycupnął jakiś stary grzyb, tam pręży się kozaczek albo gąska; ówdzie grzyb płowy sąsiaduje z czarnym łebkiem, zaś śliniący się maślaczek – z obleśnym sromotnikiem; pod sosną pieprznik nadaje pieprzne dowcipy, tak jak by chciał zagłuszyć rydzyka spod jałowca. Wśród grzybów taka rozmaitość jak i wśród ludzi. Czy może dziwić, że relacje pomiędzy grzybami, między grzybami a ludźmi, a także między ludźmi ze względu na grzyby odzwierciedlają naszą polską rzeczywistość?

**Grzyb a obyczaj staropolski**

Pan Zdzicho uwielbia grzybobranie z nagonką. Zabiera w knieje liczną swą rodzinkę, zasiada pod dębem na rybackim stołeczku i goni swoich po okolicznych chaszczach, by mu napędzali grzybostan do wielkiego kosza. Sam pociąga sobie wesoło z piersióweczki to, co mu ostatnio napędził szwagier. Kosz się napełnia, gdy trzecia piersióweczka pustoszeje. Za kierownicą siada synek Jasio, a pan Zdzicho śpiewa popularne pieśni grzybiarskie, a gdy tych zbraknie – również myśliwskie, rybackie i nieprzyzwoite.

**Grzyb a wymiar sprawiedliwości**

Inaczej sobie poczynał niejaki Antoni N. zwany w rodzinnej wsi „Rekietierem”. Ten amator grzybów przechadzał się po lesie z dwururką i udając gajowego rekwirował napotkanym grzybiarzom ich plon (zazwyczaj połowę). Początkującym zbieraczom z miasta opróżniał reklamówki pod pretekstem, iż rozpoznał w ich zdobyczy nadzwyczaj trujące okazy, innym wlepiał mandaty za niszczenie ściółki albo za nielegalny wjazd pojazdem mechanicznym na teren leśny. Krewki z natury, nie cierpiał oporu, przeto też ustrzelił kilku najbezczelniejszych niszczycieli środowiska.  
Przed sądem tłumaczył się, że należy do zielonych i nie może ścierpieć wandali płoszących zwierzynę. Jego znajomy, gajowy Zenon J., zeznał pod przysięgą, iż oskarżony uwielbiał faunę, a w szczególności jedną wiewiórkę, która padła po spożyciu chlorowanej wody z kranu podanej jej podstępnie przez chuliganów z miasta w butelce po „Żytniej” (do Żytniej” była przyzwyczajona); ta tragedia (potwierdzili to biegli psychiatrzy) wywarła na panu Antonim piorunujące wrażenie i wprawiła go stan permanentnej pomroczności (na przemian jasnej i ciemnej), przez co nie jest poczytalny do dziś. Że rzeczywiście nie jest, sąd mógł się przekonać naocznie, gdy oskarżony odgryzł palec woźnemu sądowemu.  
Lekarz z ośrodka zdrowia wystawił dodatkowo zaświadczenie, że Antoni N. jest nieuleczalnie uzależniony od grzybków w occie, a nie jest w stanie pozyskiwać runa osobiście, bo schorzały kręgosłup uniemożliwia mu schylanie. Sołtys znał go jako jedynego żywiciela Genowefy N., zgrzybiałej staruszki nieporadnej życiowo, potrafiącej jedynie wytwarzać słoiki z grzybkami w occie. Brat przypomniał sobie, że Antek w dzieciństwie chciał być leśnikiem.  
W świetle powyższych ustaleń sąd słusznie zbagatelizował upierdliwie podnoszoną przez prokuratora okoliczność, że oskarżony jest recydywistą, bo wszedł w posiadanie dubeltówki po zabójstwie Grzegorza W., jej poprzedniego użytkownika. Zresztą okazało się, że Grzegorz W. był kłusownikiem, zaś Antoni N. jako leśnik z przekonania miał prawo położyć go trupem w samoobronie.  
Sprawę podsumował obrońca przedstawiając swego klienta jako człowieka schorowanego fizycznie i psychicznie, a przy tym zbyt biednego, by mógł grzyby skupować i zbyt dumnego, by o nie żebrać. Ponieważ sąd się wahał, a prokurator wykazywał zajadłość, mecenas sięgnął po broń ostateczną i udowodnił, iż ofiary Antoniego N. nie były mu wcześniej znane, a ponadto należały do grona osób zupełnie przeciętnych, nie karanych, bez związków ze światem przestępczym i miernie zarabiających. Stało się jasne, że zostali zgładzeni nie dlatego bynajmniej, że zabójca był egzekutorem mafii, ani też z zemsty i nie dla zysku. Pobudki Antoniego N. musiały być czyste, tak więc sąd nie miał wyjścia i wypuścił go do domu. Przewodniczący składu upraszał go tylko serdecznie, by ładował dwururkę raczej kaczym śrutem niż brenekami.  
Dodajmy, że pan Antoni padł niedługo potem ofiarą grzybiarza, który podał mu w charakterze okupu kilka muchomorów sromotnikowych zręcznie podretuszowanych przez naturę na wzór i podobieństwo czubajki kani. Ponieważ grzybiarz mieszkał cztery przecznice od jednej z ofiar Antoniego N., udowodniono mu zbrodnię z premedytacją i zasądzono na dożywocie. Szczególnie obciążający był dlań fakt, że czyn jego miał wszelkie znamiona samosądu

**Grzyb a wielka polityka**

Na czym polega zbieractwo grzybne? Chodzi o to, by zgromadzić taką koalicję okazów, która nie zawierałaby owocników szkodliwych dla ustroju konsumenta. Dobry grzybiarz to taki, który potrafi sformułować i zastosować kryterium odróżniania grzybów niepożądanych, dobrych i szczególnie smakowitych. Czyż nie ma tu analogii do polityki? Decydujemy na przykład, że nie zbieramy czerwonych kozaków i muchomorów, odgarniamy na bok przaśne zielonki i zaropiałe purchawki. Koncentrujemy się na prawdziwkach, rydzykach i borowikach, dobieramy do smaku maślaków z koźlakami. Okazy rozlazłe, miękkie i przejrzałe rozwieszamy na tylnych ławkach sejmu i senatu, by stwardniały i nabrały aromatu. Owocniki młodsze i twardsze wrzucamy do gara z napisem: parlamentarne stanowiska funkcyjne, zaś szczególnie urodziwe grzybki umieszczamy w przejrzystym gabinecie i zaprawiamy octem egzekutywy. Jedyny problem to – jak uniknąć robaczywków.