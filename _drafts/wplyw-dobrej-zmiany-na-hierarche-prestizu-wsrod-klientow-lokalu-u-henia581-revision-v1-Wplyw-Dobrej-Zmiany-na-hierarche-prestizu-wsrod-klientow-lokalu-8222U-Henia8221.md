---
id: 586
title: 'Wpływ Dobrej Zmiany na hierarchę prestiżu wśród klientów lokalu &#8222;U Henia&#8221;'
date: '2016-03-02T22:44:09+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2016/03/581-revision-v1/'
permalink: '/?p=586'
---

DOBRA ZMIANA W HIERARCHII PRESTIŻU

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/03/Wpływ-Dobrej-Zmiany-na-hierarchię-prestiżu-wśród-bywalców-lokalu-U-Henia.pdf)

Wszyscy na pewno są ciekawi, jak powiodła się misja dziennikarska pani Misi w kontrowersyjnym lokalu „U Henia”, którego właściciel zaszokował parafię śmiałą tezą o możliwości Dobrej Restauracji.  
Tak jak Wergiliusz oprowadzał Dantego po zakamarkach piekła, tak pan Witek, mistrz karaoke a capella, wiódł panią Misię od stolika do stolika. Ale proszę tę informację zachować dla siebie i nie dzielić się nią z nikim, bo złośliwe języki wrogów Dobrej Zmiany nie przepuszczą sposobności, by pani Misi wytknąć brak dbałości o anonimowość źródła. A prawda przecież jest inna: przed publikacją pani Misia spytała uczciwie pana Witka, czy chce być rozsławionym na łamach „Aniołka Parafialnego”, czy też woli skromnie pozostać w cieniu. To pan Witek wyraził własną osobą życzenie, by występować w medium parafialnym jako Głębokie Gardło. Przydomek Głębokiego Gardła zyskał na dorocznych zawodach fanów browarnictwa ojczystego i był z niego dumny. W decyzji Pana Witka dostrzec można wymiar salomonowy: wybierając taki a nie inny pseudonim, z jednej strony pozostał nieznany szerokiej publiczności, a z drugiej – uchylił przyłbicy przed gronem wtajemniczonych. To z kolei tłumaczy chyba, że wskazując palcem na konkretną osobę i jednocześnie prosząc was o dyskrecję, wcale nie przejawiliśmy niekonsekwencji, lecz raczej postąpiliśmy zgodnie z intencjami pana Witka.

**Stolik pana Binia**

Lustrację lokalu „U Henia” rozpoczęła pani Misia od najbardziej zatłoczonego stolika. Z powodu niedostatku miejsc siedzących Głębokie Gardło dżentelmeńsko zaoferował dziennikarce swoje kolano. Pani Misi trudno było odmówić tej dobrej usługi, bo obserwacja uczestnicząca ma swoje wymogi, nastrój w pomieszczeniu panował walentynkowy, a źródłu wypadało okazać elementarne zaufanie. Czyż zresztą nie została wcześniej ostrzeżona, że kto wstępuje do lokalu o niejasnej reputacji, w którym odprawować się mają niepokojące obrzędy o zgniłozachodniej proweniencji, ten siłą rzeczy musi się liczyć ze smutną koniecznością tolerowania elementów rozpusty o co najmniej umiarkowanym stopniu wyuzdania?  
Prawdziwe to szczęście w nieszczęściu, że kolano pana Witka było prawe.  
Przy stoliku królował pan Binio, który właśnie raczył zgromadzonych kolejką. Z iście staropolską gościnnością dopilnował, by nowo przybyła pani Misia też skorzystała.  
Uraczeni traktowali pana Binia z atencją, uważnie wsłuchując się w jego słowa, milknąc, kiedy otwierał usta i donośnie rechocąc, kiedy bawił ich dowcipami. Ten ostatni przejaw szacunku nie wszyscy dozowali z należytą precyzją; rechocący w nieodpowiednim momencie lub nierechocący w momencie do tego przeznaczonym zderzali się z chłodną pogardą pozostałych i odpadali, grawitując ku innym stolikom. Na ich miejsce napływali inni.  
– Kim jest pan Binio? – zwróciła się pani Misia z dyskretnym pytaniem do źródła – To jakiś dygnitarz gminny czy poważany biznesmen?  
Okazało się, że jeszcze parę dni temu pan Binio należał do szarej masy klientów dosiadających się do zasobniejszych gości; nie miał dobrej passy, bo kiepsko dozował reakcję na dowcipy.  
– Skąd ten awans? – zadziwiła się pani Misia.  
Głębokie Gardło objął wargami jej ucho w obawie przed podsłuchem. Pani Misia dowiedziała się, że pan Binio jest szczęśliwym ojcem siódemki dzieci i po uchwaleniu ustawy 500+ należy potencjalnie do grona najzasobniejszych klientów pana Henia. W oczekiwaniu na pierwszą wypłatę zaciągnął pożyczkę i teraz bryluje w towarzystwie.  
Pan Binio wszedł właśnie w fazę rozrzewnienia.  
– Dzieci, moje dzieci – zaintonował laudację – jak wy kochacie tatusia! Wszystko, co warte, byście mu dały!  
Jego wyznanie wywołało u obecnych falę uczuć prorodzinnych. Dwóch fanów pana Binia zwolniło siedzenia po obu stronach idola i utworzyło podstolik u stóp pani Misi.  
Pani Misia zdawała sobie sprawę z tego, że wylewność polibacyjna jest następstwem grzechu głównego, mimo to jednak nie mogła powstrzymać wzruszenia, kiedy dwóch dżentelmenów wróciło mentalnie do błogiego stanu dziecięcego, by ufnie szukać u niej matczynego ciepła, obłapiać kolana i szukać schronienia pod spódnicą. Jej twarde zasady moralne nie pozwalały jednak na lekkomyślne wchodzenie w rolę matki zastępczej w sytuacji, kiedy nie było pewności, że oseski są sierotami bez żadnych bliskich krewnych.  
Dlatego pani Misia wsparła się na źródle i odpłynęła ku następnemu stolikowi.

***Z łamów „Aniołka Parafialnego”:***

*Nie ulega wątpliwości, że bar „U Henia” gromadzi grzeszników. Któż z nas jest jednak wolny od grzechu? Kto może rzucić kamieniem? Bywający „U Henia” wymagają z pewnością nawrócenia i powinni zostać objęci ścisłym nadzorem duszpasterskim. Nie są jednak chyba straceni. Można dostrzec u nich pozytywy. Jest wśród nich grono entuzjastów Dobrej Zmiany. Pod wpływem Dobrej Zmiany następują „U Henia” przemiany w hierarchii prestiżu. Zwolennicy wartości rodzinnych i przeciwnicy antykoncepcji – kiedyś maluczcy, teraz doznają słusznego wywyższenia. Popieranie Dobrej Zmiany poprzez wznoszenie toastów na pewno nie zasługuje na pochwałę, czy jednak wymaga wypalania rozżarzonym żelazem? Przynajmniej nie są to toasty na cześć Złej Restauracji.*

*Nie udało się dotąd ustalić, co właściwie społeczność lokalu „U Henia” rozumie przez Dobrą Restaurację. Wymaga to dalszych badań.*

*Redakcja stanowczo dementuje niewybredne plotki o tym, że Pani Misia i pan Witek, działacz ruchu śpiewaczego, spodziewają się swoich pierwszych pięciuset złotych.*