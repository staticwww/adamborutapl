---
id: 489
title: 'Październik miesiącem oszczędności'
date: '2015-10-18T21:42:54+02:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2015/10/485-autosave-v1/'
permalink: '/?p=489'
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/10/październikowa-oszczędność.pdf)

PAŹDZIERNIK MIESIĄCEM OSZCZĘDZANIA

Październik miesiącem oszczędzania – mówi się od lat. I to jest tradycja. Ale czy ten październik jeśli nie skąpstwem, to choć umiarem stoi?  
Media zdają się być przekonane, że stoi wyborami i rozrzutnością. To błąd perspektywy. Nie może tak być. Wyborami zawiadują politycy, a politycy szanują władzę, którą podczas wyborów rozdaje frekwencja. Potoczna mądrość polityczna podpowiada, że frekwencja ceni sobie tradycję i najchętniej daje władzę tym, co chwalą głośno to, co ona sobie ceni. Nie sposób więc być politykiem i w miesiącu wyborczym ignorować tradycję. Skoro się utarło, że październik jest miesiącem oszczędności, politycy nie mogą się temu przeciwstawiać.  
Dlatego w październiku wskazują politycy na dominację oszczędności w życiu społecznym. Ci dysponujący najbardziej przenikliwym spojrzeniem dostrzegają nędzę pośród ruin, mniej spostrzegawczy ubolewają nad skromnym poziomem życia, okularnicy mrużąc oczy wzdychają, że mogłoby być lepiej, zaś osobnicy z zaćmą przebąkują o nadaniu przyspieszenia stopie.  
Dawniej frekwencji, a i całemu elektoratowi, wystarczał zgodny z tradycją opis rzeczywistości. Pojawili się jednak tu i ówdzie wyborcy rozkapryszeni, którzy spodziewają się, że politycy będą się też do tradycji stosować. Dlatego politycy czują się zmuszeni do okazywania oszczędności w październiku. Stąd się bierze owo osławione gospodarowanie prawdą, umiar w epatowaniu etykietą, skromność w ocenie kondycji ojczyzny i powściągliwość w prognozach odbicia się finansów publicznych od dna.  
Logika podpowiada, że skoro tradycja nakazuje oszczędzanie w październiku, to najwidoczniej toleruje rozpasanie wydatkowe w innych miesiącach. Toteż od listopada elektorat zasypany zostanie przez wygranych polityków górą pieniędzy, ulg i świadczeń.  
Ale w październiku zaciskamy pasa!