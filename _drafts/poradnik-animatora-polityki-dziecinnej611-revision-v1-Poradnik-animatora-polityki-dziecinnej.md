---
id: 614
title: 'Poradnik animatora polityki dziecinnej'
date: '2016-06-14T19:34:47+02:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2016/06/611-revision-v1/'
permalink: '/?p=614'
---

PORADNIK ANIMATORA POLITYKI DZIECINNEJ

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/06/Poradnik-animatora-polityki-dziecinnej2.pdf)

W Dniu Strażaka uszczęśliwia się strażaków samochodzikami z drabiną i motopompą, kolejarze dostają w Dniu Kolejarza lokomotywki z wagonikami, a górnicy z okazji Barbórki słyszą, jak mocno rząd ich kocha i za buziaka przedłuży im kieszonkowe.

Po organizatorach Dnia Dziecka spodziewamy się jednak większego wyrafinowania. To prawda, że dzieci mniej skomplikowane mogą się zadawalać samochodzikami, kolejkami i kieszonkowym. Co jednak ze skomplikowanymi?

Pomyślmy – dajmy na to – o dzieciach z tzw. ADHD. Czy w Dniu Dziecka nie ucieszyłyby się z udostępnienia im toru przeszkód wiodącego do klasy z kukłą pedagoga przywiązaną do krzesła i z kijem baseballowym na podorędziu? Czy niektórym z nich przeszkadzałoby, gdyby pedagog był prawdziwy? Ta wizja nie jest bynajmniej powodowana uprzedzeniami do dziecin nadpobudliwych. Kiedy odrzucimy stereotypy, nie będziemy się zdumiewać, gdy dzieci z Bullerbyn przyłączą się do takiej zabawy, aby choć raz, dla odmiany, przyprawić pieprzem mdłą słodycz bytu.

Jak widać, wyrafinowanie animatora obchodów Dnia Dziecka polegać mogłoby na dostosowywaniu daru do kondycji dziecięcia. Nie wypada ofiarowywać dziecku Rosemary poświęconego medalika.

Wielu darczyńców rozumie taką chęć dostosowywania jako imperatyw prezentu pożytecznego. Uważają, że dzieci kapitana Granta dobrze jest obdarzyć lekiem na chorobę morską i maścią przeciw moskitom.

Są animatorzy Dnia Dziecka owładnięci imperatywem prezentu pożytecznego, a zarazem przekonani, że nie ma sensu dostosowywanie go ani do obiektywnych, lecz chwilowych potrzeb milusińskich, ani do ich równie niestałych, subiektywnych gustów. Poszukują daru uniwersalnego, który przyda się każdemu dziecięciu niezależnie od tego, jaka jest jego konkretna sytuacja, potrzeba czy gust. Koperta nadziewana mamoną to genialny wynalazek zwolenników takiego podejścia.

Co mają jednak począć zwolennicy uniwersalnego daru pożytecznego, którzy uważają, że nie przystoi przaśną mamoną kalać niewinności małoletnich? Muszą im ofiarować dobro niematerialne. Stąd niektórzy rodzice i politycy oświatowi, ci zwłaszcza, których nocą dręczyły koszmary szkolne, dostrzegli dziecięce szczęście w odroczeniu obowiązku szkolnego o rok. Im koszmary szkolne są straszniejsze a empatia większa, tym silniejsza bywa wola odroczeniowa. Być może w tym albo następnym Dniu Dziecka milusińscy zostaną obdarowani przesunięciem obowiązku szkolnego do momentu osiągnięcia pełnoletniości.

Innym przykładem darczyńców, którzy sami z siebie wiedzą, co dla milusińskich dobre, są niektórzy rodzice tzw. cudownych dzieci i pedofile.

Na przeciwległym biegunie plasują się ci animatorzy Dnia Dziecka, którzy są wrogami arbitralności, utylitaryzmu i urawniłowki głoszącymi, że istotą prezentu jest uszczęśliwianie konkretnych osób lub zbiorowości. Stoją zatem na stanowisku, że prezenty powinny odpowiadać autentycznym pragnieniom milusińskich. Preferują wychowanie bezstresowe dalekie od nachalnego ingerowania w dziecięce systemy wartości. Uważają, że zamiast dzieciom kukurydzy cokolwiek brutalnie narzucać, poprawniej jest dyskretnie zwrócić ich uwagę na tych dorosłych, których brak nie okaże się dla społeczności zbyt dotkliwy.

Uważni odbiorcy tego wykładu zauważyli z pewnością, że jednym z powodów lawirowania pomiędzy biegunami arbitralności i spolegliwości w teorii animacji Dnia Dziecka są trudności, jakie nastręcza ustalanie rzeczywistych pragnień dzieci. Niekiedy te trudności są zbyt duże i arbitralność wydaje się koniecznością.

Są jednak przypadki, które nie są kłopotliwe. Wydaje się oczywiste, że dzieci ulicy pragną dobrej pogody, pustych ulic, flag, transparentów i portretów sędziego Rzeplińskiego. Dzieci resortowe bez wątpienia łakną łamów, mikrofonów i kamer.

Z dużym prawdopodobieństwem można przypuszczać, że niewiniątka marzą o kulturalnym, dyskretnym i bezbolesnym sposobie pozbycia się niewinności.

Specjalną kategorią są w czepku urodzone dzieci szczęścia, których pragnieniem jest zapewne pozbycie się nudy towarzyszącej monotonnemu spełnianiu się pragnień.

Warto może dodać, że pragnienia dzieci mogą się różnić w zależności od ich płci. Córy Koryntu, czyli dziewczynki, które postawiły na sukces w życiu publicznym, marzą o domatorach z konikiem kulinarnym i zacięciem pedagogicznym, zaś synowie pułku poszukują pań domu, które realizują się w kwatermistrzostwie, markietanctwie, logistyce i musztrowaniu narybku.

P.S.  
Nie zapomnieliśmy bynajmniej o dzieciach kwiatach. Szczegółową analizę ich preferencji utrudnia jednak wszechobecny syndrom popychania. Otóż demencja utrudnia dzieciom kwiatom przypomnienie sobie, do czego wcześniej popychały je żądze, osłabienie zarówno popędów jak i jakości percepcji źle wpływa na ich zdolność rozpoznania, do czego żądze popychają je obecnie, werbalizację szczątkowych ustaleń zakłóca im konieczność ciągłego popychania sztucznych szczęk na miejsce, zaś bariery architektoniczne uniemożliwiają popychanie wózków w kierunku mass mediów i ośrodków badania opinii publicznej.