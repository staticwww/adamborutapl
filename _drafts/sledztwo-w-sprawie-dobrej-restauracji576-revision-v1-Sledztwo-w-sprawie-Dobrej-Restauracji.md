---
id: 579
title: 'Śledztwo w sprawie Dobrej Restauracji'
date: '2016-02-21T20:24:37+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2016/02/576-revision-v1/'
permalink: '/?p=579'
---

CZY MOŻLIWA JEST DOBRA RESTAURACJA?

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/02/Czy-możliwa-jest-Dobra-Restauracja.pdf)

 Nieprzemyślane być może słowa pana Henia o tym, że Dobra Restauracja jest możliwa, spowodowały poruszenie w naszej parafii.  
Na razie poruszyły elity. Mógł to być niegroźny ferment, który po fazie bulgotania i puszczania gazów, w sposób naturalny rozpływa się łagodnym roztworze alkoholu etylowego. Jednak ksiądz proboszcz nie chciał bagatelizować zagrożenia. Kto zaręczy odpowiedzialnie, że ferment nie zostanie wykorzystany przez potężne siły grzechu, które wydestylują zeń stężoną używkę do tumanienia nosicieli wątłych kręgosłupów?  
– Nie z takimi używkami dawaliśmy sobie radę – usiłował pocieszać pan Józef, kościelny, chłop na schwał. Nikt jednak nie miał wątpliwości, że gdyby trucizna przesączyła się do mas, mogłaby niekorzystnie oddziałać na słabych i bezbronnych.  
Wikary Szymon skomplikował zagadnienie. – Nie da się logicznie wykluczyć, że Dobra Zmiana dałaby się jakoś doktrynalnie uzgodnić z Dobrą Restauracją – powiedział. – Czasem lepiej jest obrócić potencjalnego wroga w sprzymierzeńca.  
Wielowiekowe doświadczenie sił dobra podpowiadało, że nie można z góry odrzucić takiego wyjścia. Jednak to samo doświadczenie przekonywało, że im przekaz jest prostszy, tym lepszy. Skoro Zmiana jest dobra, to prima facie Restauracja jawi się jako zła. Dopuszczenie możliwości, że Restauracja może być również dobra, zmusza do odróżnienia Dobrej Restauracji od Złej Restauracji. Powstaje problem kryteriów, obraz staje się skomplikowany, maluczcy się gubią, robią błędy i w rezultacie głosują na wilki w owczych skórach.  
Postanowiono wydelegować panią Misię, redaktorkę gazetki parafialnej, do baru „U Henia”, by rozeznała sytuację.  
Ksiądz proboszcz niepokoił się o panią Misię. – Pani Misiu – westchnął – pani zdaje sobie sprawę, że rzetelne badanie może wymagać dostosowania się do otoczenia, a zatem niewykluczone, że okoliczności zmuszą panią do chwilowego pogrążenia się w grzechu. Symulowanie upadku może nie wystarczyć i trzeba będzie naprawdę wnijść w bagno, by nie zostać uznaną za obcą i nie wzbudzić nieufności. I nie mówię tylko o grzechu wiadomego nadużywania, który w sumie nie jest pewnie aż tak groźny, skoro przydarza się nawet najlepszym z nas, jak pan Józef przykładowo. Nie wiadomo jednak do końca, czy nie będzie pani zmuszona do akceptacji jakichś elementów rozpusty o niebezpiecznym stopniu wyuzdania – tym bardziej, że donoszą życzliwi, iż pan Henio ma zamiar jutro zaszczepić na naszym znękanym gruncie niepokojącą tradycję walentynkową.  
Tu rycerski wikary Szymon nie mógł znieść narażania wątłej niewiasty, dlatego zaproponował, iż ją zastąpi.  
– Wypada pochwalić poświęcenie księdza – odparł rozsądnie proboszcz – lecz z kilku naszych niewinnych, parafialnych uroczystości płynie nauka, że księdza pamięć zachowuje jedynie początkowe fragmenty biegu wydarzeń, natomiast szwankuje w przypadku ich zasadniczej części i kompletnie zawodzi, jeśli chodzi o końcówkę.  
– Może pan Józef by rozeznał – zaproponowała pani Marta, podpora kółka różańcowego – nasz drogi kościelny na pewno jest odporny na grzech w formie ciekłej. – Ten pomysł też upadł; przecież pan Józef regularnie nawiedzał bar „U Henia”, dzięki czemu zresztą wzbogacał parafię o – pożyteczne skądinąd – elementy kroniki towarzyskiej, natomiast kompletnie nie sprawdzał się w analizach natury socjologicznej i psychospołecznej.

 \*  
Pani Misia, udając się do baru „U Henia”, dla dobra sprawy wyposażyła się w garderobę o stopniu pruderyjności plasującym się znacznie poniżej właściwej sobie normy; możliwe, że z tej właśnie przyczyny, albo też ze względu na frywolny nastrój panujący w lokalu, od razu po wejściu została zaczepiona przez pana Witka, mistrza karaoke a capella.  
– Cóż to widzę – zagaił pan Witek szarmancko – pani Misia łaskawa jest dysponować nogą i głębokim oddechem, czego dotąd wielbicielom broniła kontemplować!  
Panią Misię zraziła bezpośredniość pan Witka, mimo to na jego dobro należało zapisać elementarny może, ale jednak zmysł estetyczny. Należało podjąć szybką decyzję. Pani Misia zaryzykowała i mianowała pana Witka źródłem dziennikarskim.  
\*  
O efektach dziennikarskiego śledztwa pani Misi informować będziemy w miarę ukazywania się kolejnych odcinków jej reportażu w periodyku „Aniołek Parafialny”.