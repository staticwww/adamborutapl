---
id: 153
title: 'Konkurs poetycki służb sanitarnych księcia Warczysława'
date: '2014-11-09T14:03:09+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/11/66-revision-v1/'
permalink: '/?p=153'
---

*Od podkomorzego Przecława: Przyjdzie czas na krotochwile, śmiechy i podrwiwania. Zacznijmy jednak od poezji zaangażowanej. Oto grupka utalentowanych pracowników komory, która postanowiła roz­propagować wśród załogi czytelnictwo prasy. I to wcale nie kolorowej, bo ta rani ich wrażliwość śliską sztywnością połyskliwego papieru! Nie, zachwalają oni prasę codzienną, którą miękko otulić można się jak kocem – taka jest przytulna i przyjazna, a nade wszystko pożyteczna!*

Dzień dobry, koleżanki i koledzy czytelnicy! My, koledzy i znajomi z komory prześwietnego Warczysława, knezia opowiemy Wam o spotkaniu pary mieszanej hurtowników z Pokrzywna, a potem udzielimy Wam dobrej rady za darmo.

I

ON:

Ach, miła pani, cóż w mym ogrodzie

o ciemnej nocy robisz i chłodzie?

Może ci, pani, czego potrzeba?

Rad byłbym pani przychylić nieba!

ONA:

Och, miły panie, dzięki serdeczne,

jeśli zaś pomóc chcesz mi koniecznie,

papieru daj mi gazetowego,

bom tu przybyła, panie, z potrzebą.

II

Wspomnij tę scenkę, handlowcze młody,

kiedy w gorączce szukasz metody,

co by uwiodła od klawiatury

matki – hurtowni powabne córy

.

Bezsilny dowcip i smakołyki,

tańce, imprezy, wina, muzyki?

Pomyśl! A jeśli w tej właśnie chwili

twoją wybrankę głód prasy pili?

To ułożyliśmy my, hurtownicy z komory książęcej

*A teraz utwory pod hasłem „miłość w plenerze”. Publikują doręczyciel towarowy Zenek, magazynier wannowo-brodzikowy Zdzicho i asystent do spraw fakturowania komputerowego Rumcio.*


### Natura kusi, bo musi

Pośród listowia, łodyg i naci

człowiek najchętniej cnotę swą traci

W powietrzu, wodzie, albo i bagnie

jarzmu uczucia kornie kark nagnie

Bluszcze, jaśminy, nawet pokrzywy

wielce wzmacniają serca porywy

Bzyki, pomruki albo kląśnięcia

wnet zarażają chęcią poczęcia

Pąki, jagody, kwiaty, owoce

przywodzą myśli o stadku pociech

Jamy, szczeliny oraz czeluście

rodzą marzenia o rozpuście

Korzeń czy żołądź, pień lada jaki

to niezawodne afrodyzjaki

Pleśnie, pająki, syki żmijowe

kuszą pośrednio wizją teściowej

Co jest w przyrodzie, co się w niej zdarzy

wszystko z miłością mi się kojarzy.

Dr Zenek

#### Sonet o naturze i kulturze


Wiedz, moja pani, że mam kulturę

dlatego wielce cenię naturę;

stąd też jak Plato czy Arystotel

prowadzać chcę Cię tam i z powrotem

po łąkach, lasach w upojnym rajdzie.

Taki już ze mnie perypatetyk

który w przełajach szuka podniety,

z dysputą łącząc uciechy dreszcze.

A żem światowiec, to dodam jeszcze:

choć chodzić będziesz, jednak nie zajdziesz.

Asys. Rumcio

**O syfonie wannowym song**

Nie dla mnie panny

– kocham inaczej

uwielbiam wanny

wanienki raczej

Wczoraj nieśmiało

wziąłem na ręce

dwudziestkę białą,

piękną wanienkę;

gdy układałem

miłą w ziół bukiet,

węzłem związałem

z naturą sztukę.

Biel niewinności!

zieleń nadziei!

jam te wzniosłości

w ten padół wcielił.

Tak się wzruszyłem,

że jakem Zdzicho,

z wanną złączyłem

swój własny syfon

 Mgr Zdzicho

P.S. Ewo, Ewo, gdybyś ty była emaliowaną!