---
id: 104
date: '2014-10-25T03:23:31+02:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/10/103-revision-v1/'
permalink: '/?p=104'
---

**PRZYPADKI MAGISTRA N.**

(Opowieść z lat pierwszej „Solidarności”)

# Część pierwsza: zalążek afery

W burzliwych czasach zarania lat osiemdziesiątych ob. mgr Aleksander N. padł ofiarą spisku; spisek ów polegał na tym, że nieznany sprawca dopisał na karteluszku autorstwa ob. mgr-a N. trzy na pozór niewinne literki „doc”. Literki owe, umieszczone zostały z lewej strony autografu mgr-a N. w sposób niedwuznacznie sugerujący, iż należy go tytułować per „panie (obywatelu, towarzyszu, kolego) docencie”.

A oto okoliczności, w jakich dopuszczono się wzmiankowanego czynu. Nie jest tajemnicą, że naukowcom, nawet wybitnym, nie są bynajmniej obce czysto ludzkie słabostki: jakoż i mgr N., zmęczony czuwaniem nad młodzieżą zdającą egzaminy wstępne, dał się namówić na kufelek piwa. W związku z tym napisał na karteczce sakramentalne „zaraz wracam”, a pod spodem – trzeba tu nadmienić, że brzydzi się zasadniczo anonimami[\[1\]](#_ftn1) – złożył osobisty podpis. Całość umieścił obyczajem akademickim na drzwiach gabinetu, który to gabinet dzielił ze swym przełożonym, docentem X.

\*

Pierwsza zauważyła karteczkę mgr Y; przeczytawszy ją, wyciągnęła chusteczkę, przetarła okulary, przeczytała raz jeszcze i udała się do dr Z.

– Nasz drogi Olek został docentem – rzekła z progu, siląc się na obojętność.

– Jak to? – odparła dr Z przeszyta dreszczem zaskoczenia. Dowiedziawszy się szczegółów, wyszła z gabinetu, by z właściwą historykom sumiennością osobiście zapoznać się ze źródłem.

– No tak – skomentowała sucho – spodziewałam się czegoś takiego po nim.

– Docent *mianowany* – odezwała się mgr Y jadowicie[\[2\]](#_ftn2).

– Ciekawe, czym się zasłużył? – spytała dr Z, używając podobnego tonu.

Obie uczone spojrzały po sobie.

– Trzeba byłoby przygotować dla kolegi kwiatki i laurkę – westchnęła mgr Y.

– Tak, tak – przytaknęła dr Z w zamyśleniu.

**Część druga: pierwsze reakcje.**

A dnia następnego…

– Gratuluję, gratuluję! – uśmiechnęła się sekretarka do zaskoczonego mgr-a N.

Dr Z i mgr Y wśród westchnień i okrzyków wręczyły mu laurkę i kosz róż oraz zadeklarowały się między całusami jako dozgonne wielbicielki. Zrobiły też kawę, odbiły sprawnie ryzlinga i zjadły zakupioną dla uczczenia docentury bombonierę.

Prof. Q, oglądając się na boki, wymamrotał coś, co trudno było zrozumieć, po czym uścisnął prawicę świeżo upieczonego docenta, by oddalić się szybko, kręcąc głową.

Dyrektor instytutu nie dostał jeszcze oficjalnego pisma, więc na wszelki wypadek został w domu, przysyłając w zastępstwie nieletniego bratanka ze zwolnieniem lekarskim na trzy dni.

Pojawiła się też kartka informująca o nadzwyczajnym posiedzeniu egzekutywy; aktyw „Solidarności” natomiast zamknął się gabinecie doc. V.

Mgr-owi N. włos zjeżył się na głowie. Instynktownie wyczuwał zagrożenie. Poprzedniego dnia, wróciwszy z piwa (nieudanego zresztą), zdjął zaraz nieszczęsną kartkę, a nawet spalił ją w popielniczce. Nic z tego…

– Muszę wyjść – wyrzekł nieswoim głosem. – Do Collegium Minus.

W „Cechowej” po czwartej setce wyprostował się nagle i zapiął wszystkie guziki.

– Kto wie, kto wie… – wyszeptał w kierunku literatki.

\*

Podczas gdy mgr N. pod wpływem napoju wyskokowego snuł myśli o potędze, panie z dziekanatu swoimi kanałami dotarły do kadr. Kadry nic nie wiedziały o nowej nominacji, co z początku wzbudziło wątpliwości. Doktor Y, mąż zresztą znanej nam mgr Y, bąknął nawet coś o Marynie[\[3\]](#_ftn3). Jednak mgr Y i dr Z, obawiając się niezdrowej popularności z powodu swego zaangażowania w obchody przyznania docentury, domyśliły się szybko, że decyzja zapadła w stolicy, zaś odpowiedni dokument ugrzązł na poczcie. Resztę podejrzeń rozpędziła wiadomość doniesiona z „Cechowej”. Okazało się, że mgr N. pobił tam dr-a A za to, iż ten nie dość szybko mu się ukłonił. Było to oczywistym dowodem na to, że mgr N. rzeczywiście został mianowany[\[4\]](#_ftn4).

\*

Docent X zachowywał się dwuznacznie.

Pobierając uposażenie, pospieszył poinformować kolejkę do kasy o dokonaniach zakładu, które to dokonania pozwoliły na rozbudowę reprezentowanego przez zakład kierunku badań. – Świadectwem tego – stwierdził – jest nowa nominacja.

W klubie inną kolejkę zapoznał z faktem, że mgr N. jest jego najwybitniejszym pracownikiem, uczniem i wychowankiem.

Spotkawszy zaś na osobności dr-a B, prominentnego działacza „Solidarności”, zwierzył mu się ze swych obaw o samorządność uczelni wyższych. – Mnożą się ostatnio – rzekł dr-owi B na ucho – niekonsultowane nominacje. Rodzi to wątpliwości co do intencji władz.

Sekretarzowi OOP zwrócił uwagę, że obsadzanie stanowisk bez zasięgnięcia opinii zainteresowanej komórki partyjnej nie licuje z duchem odnowy.

Do spotkania skacowanego mgr-a N. z docentem X doszło na korytarzu. Docent, objąwszy go kordialnie, zaprosił do gabinetu.

– W obecnej, zmienionej sytuacji – wyrzekł serdecznie – należałoby przyspieszyć sprawę pańskiego doktoratu. Jest on, rzecz jasna, daleko zaawansowany, sądzę.

Mgr N. przybladł mocniej jeszcze.

– Ma pan grypę? – spytał docent X z troską. – Wobec tego niech pan wypocznie i przyniesie pierwszą wersję za tydzień.

Wygłosiwszy tę kwestię, docent X z diabolicznym uśmiechem na twarzy opuścił pomieszczenie.

# Część trzecia: właściwy początek afery

Wszystko być może skończyłoby się w miarę pomyślnie, gdyby nie mgr Y i dr Z. Znane nam już uczone – częściowo pozazawodowo – interesowały się problematyką środków masowego przekazu, a zgłębiały rzeczoną problematykę głównie na podstawie obserwacji własnych dziennikarza Zyzia. Badania prowadzone były – jeśli nie liczyć kwatery Zyzia – głównie w „Polonezie”, a w chwilach kryzysu – zdarzało się – we „Współczesnej”, przy czym przenosiny takie wiązały się z koniecznością szybkiego znalezienia chwytliwego tematu dla dziennikarza Zyzia. Nieszczęściem moment ostatnich przenosin zbiegł się w czasie z wypadkami opisanymi powyżej.

\*

Inteligentnego czytelnika nie zdziwi fakt wizyty dziennikarza Zyzia u prorektora C.

– Kilka pytań – zaczął dziennikarz Zyzio. – Czy często zdarzają się nominacje magistrów na stanowiska docentów?

– No…, nie – odparł zdziwiony prorektor C.

– Na czym więc polegają zasługi mgr-a N.?

Prorektor C nie doznał dotąd przyjemności poznania mgr-a N.[\[5\]](#_ftn5). Wszakże w owych czasach, kiedy to prasie zdarzało się być nie do końca oględną, przyznanie się do niewiedzy w zakresie stosunków panujących w dziale czy komórce, którą się kierowało, mogło mieć niezbyt dobre konsekwencje.

– No, sądzę, że sam mgr N. lepiej to panu przedstawi…., to jego specjalność… Sądzę, że w dobie odnowy niewłaściwym jest, jeśli zwierzchnik wkracza w kompetencje podległych mu pracowników, prawda…

Prorektor C, wypowiadając owe słowa, usiłował w popłochu czynić remanent w swoich wspomnieniach urzędowych; w trakcie tego kilkakrotnie nerwowo przewrócił papiery leżące na biurku.

– Ale, w każdym razie, owe dokonania zasługują na najwyższą ocenę, nieprawdaż?

– No, zapewne…, tak, chyba… – wybełkotał prorektor C, strącając telefon na dywan.

– A więc to nasz wkład w naukę światową! Postęp! Sukces polskiej nauki! – podniecał się dziennikarz Zyzio, który oczyma duszy widział już swój szlagier na czołówce.

– Jak pan ocenia szanse mgr-a N. na nagrodę Nobla? – parł konsekwentnie naprzód.

– Nie wiem! – krzyknął prorektor C w przypływie odwagi, po czym pod pozorem pilnej konferencji zakończył audiencję.

\*

Mgr-a N. zastał dziennikarz Zyzio w stanie załamania po rozmowie z docentem X.

– Tu prasa… Jak się czuje świeżo upieczony docent? – spytał przymilnie.

Mgr N. machnął z rezygnacją ręką.

– Rozumiem – powiedział dziennikarz Zyzio. – A nad czym pan obecnie pracuje?

Mgr N. spojrzał na intruza spode łba.

– Nad wpływem kroku marszowego wojsk napoleońskich na rozmnażanie mrówek czerwonych w lasach pod Frydlandem – odparł słabym głosem i zbiegł, by w ten sposób zakończyć rozmowę.

\*

Przytoczone tu wywiady, poparte rozmową z doc. X i inwencją własną dziennikarza Zyzia, dały w efekcie sążnisty artykuł, którego treść pokrótce tu przedstawimy.

Doc. mgr N. został uznany za wschodzącą gwiazdę nauki polskiej, jej największą ozdobę i nadzieję na rychły sukces, zaś jego nominacja – za zielone światło dla nowej dziedziny badań. Stworzona przez doc. mgr-a N. nowa gałąź wiedzy to dyscyplina stykowa z pogranicza biologii oraz historii, tzw. biohistoria. Podkreślono znaczenie wyników biohistorii dla jakże aktualnej dziś problematyki ochrony środowiska naturalnego. Nieco miejsca zajęły dość zawiłe dywagacje filozoficzno-metodologiczne na temat tego, że powstanie i rozwój biohistorii rozstrzygnie wreszcie definitywnie spór między naturalistami a antynaturalistami. Doc. mgr N. stał twardo na gruncie Marksowskiego esencjalizmu dialektycznego, odcinając się jednocześnie od niektórych nazbyt rewizjonistycznych wypaczeń tego stanowiska. Z wielką bezkompromisowością zarzucono byłemu pierwszemu sekretarzowi, Gierkowi Edwardowi, iż niesłusznie hamował metodami administracyjnymi rozwój biohistorii. Nowa, postępowa nauka mogła rozwinąć się dopiero dzięki robotniczemu protestowi oraz poparciu sił odnowy w partii i zdrowego nurtu w „Solidarności”. Stało się to wbrew wszelkiej maści ekstremistom, którzy dybią na biohistorię, widząc w niej – słusznie poniekąd – groźny oręż w rękach sił rozsądku i umiarkowania.

Artykuł kończył się apelem o priorytet dla biohistorii.

\*

Sława doc. mgr-a N. zataczała coraz to szersze kręgi. Wspominała o nim telewizja, pisały gazety. Organizacja partyjna jednego z wielkich zakładów pracy wystąpiła do komisji Grabskiego z żądaniem wskazania winnych dyskryminacji biohistorii[\[6\]](#_ftn6). Jedynie autorytety naukowe milczały złowieszczo, jednakże i wśród nich wyczuwało się dezorientację.

Być może postanowieniem opinii publicznej nominacja mgr-a N. stałaby się faktem, gdyby nie zawistny doc. X i sformowana przezeń grupa nacisku. Spowodowała ona ukazanie się ulotki następującej treści: „Jak długo jeszcze KC będzie narzucał nam swoich sługusów? Żądamy demokratycznego głosowania nad docenturą mgr-a N.! Niech żyje autonomia nauki i samorządność uczelni wyższych! Precz z ingerencją kacyków z ministerstwa w sprawy naszej Alma Mater!”. Ulotkę ową intryganci podpisali mało odważnie: „NZS”.

Zaraz potem ukazały się plakaty NZS-u wołające gromko: „Hańba prowokatorom! Nie dopuścimy do drugiej Bydgoszczy!”. Była tam też mowa o kulturze i godności osobistej, które są najlepszą bronią przeciw przemocy Służby Bezpieczeństwa.

„Solidarność” uczelniana i Komitet Zakładowy PZPR nad podziw zgodnie potępiły nieodpowiedzialność i brak skonsultowania decyzji ministerstwa, występując o jej rewizję. Dodano ostrożnie, że protest nie jest skierowany przeciwko mgr-owi N. i postępowi w nauce, a przeciw niezgodnym z duchem czasu praktykom administracyjnym.

„Pro Patria” nie wiedziała, jak się ustosunkować do mgr-a N., zaproponowała więc mszę polową i oddanie sprawy do rozsądzenia papieżowi.

\*

Narobiło się, jak widać, sporo hałasu i za cichym poduszczeniem wicepremiera Rakowskiego wyjaśnienia afery nominacji podjął się jeden z pracowników „Polityki”. Nie była to sprawa prosta, jako że nikt do końca nie wiedział nie tylko tego, czy biohistoria jest na fali, czy też nie, ale również i tego, z której strony wieje wiatr, który popycha ową falę. Z tego to powodu czynnikom teoretycznie kompetentnym niezmiernie trudno przychodziło zajęcie określonego stanowiska.

Prorektor C, dla przykładu, wziął nagły urlop, zabronił referentkom i sekretarkom udzielania informacji o składzie osobowym uniwersytetu, po czym zaszył się w samym środku Puszczy Białowieskiej. Skryty w szałasie – dokąd kuzyn, dawny właściciel ziemski, obecnie kłusownik donosił racje żywnościowe – nasłuchiwał za pośrednictwem tranzystorowego telewizorka wieści, które pozwoliłyby wreszcie na wyrobienie sobie poglądu.

Natomiast właściwy urzędnik ministerstwa przygotował dwie wersje antydatowanego dokumentu: w jednym nadawał, a w drugim – stanowczo odmawiał nadania nieszczęsnej docentury. Po namyśle zresztą dodał jeszcze trzecią, w której z pozytywną opinią odsyłał rzecz do decyzji władz uczelni. Pierwszą wersję prezentował do wglądu referent, drugą – sekretarka, trzecią zaś – pełnomocnik do specjalnych poruczeń; oczywiście na zmianę, zależnie od koniunktury. Procederem tym nasz właściwy urzędnik zawiadywał osobiście za pomocą telefonu udostępnionego przez szpital, gdzie przebywał z racji ostrego ataku wątroby.

W końcu jednak, po długich trudach, ustalono, że nikt mgr-owi N. żadnej docentury nie nadał.

**Część czwarta: rozwinięcie**

W znanym periodyku „Rzeczywistość” afera mgr-a N. znalazła oddźwięk następujący: „Jak długo jeszcze nie będzie się dostrzegać coraz zuchwalszych poczynań sił antysocjalistycznych?” – pytał retorycznie autor artykułu. – „Zgodnie z doktryną pełzającej kontrrewolucji różnej maści wrogowie ludu przystąpili do działania. Ich zamierzeniem jest przejęcie władzy drogą tworzenia własnych ośrodków dyspozycyjnych. Mgr N., płatny agent imperializmu, rozpoczął byt samozwańczego docenta. Wkrótce pojawi się samozwańczy dziekan i rektor, potem wojewoda! Trzeba być ślepcem, by nie widzieć, do czego zmierza banda kontrrewolucjonistów!”. Z dalszego ciągu artykułu można było się dowiedzieć, że mgr N., syjonista o trockistowskich sympatiach, strojący się w piórka apostoła nowej nauki, jest bliskim krewnym Kuronia, Michnika i Moczulskiego, z którymi łączy go zapiekła nienawiść do demokracji ludowej i Układu Warszawskiego.

Specjalny wysłannik agencji prasowej kraju ościennego, głosząc podobne tezy, poszedł jednak dalej po nitce aż do cuchnącego imperialistyczną zgnilizną kłębka. Dowiódł on, iż podejrzany mgr N. utrzymuje dość zażyłe stosunki z akademikiem, dokąd przenikał pod pozorem przyjacielskich odwiedzin, a w rzeczywistości po to, by spiskować z grupą reakcyjnych studentów drugiego roku historii. Narady szajki odbywały się pod niewinną przykrywką imprez z nadużyciem napojów wyskokowych. Jak ustalił reporter, spiskowcy posługiwali się pseudonimami (m. in. „Komendant”) oraz hasłem („Dziś balanga”) i odzewem („Coś by się wypieło”). Z analizy przytoczonych tu okoliczności wynikało ponad wszelką wątpliwość, że mgr N. z kamratami planowali zbrojny pucz i odbywali w tym celu ćwiczenia wojskowe. Cała akcja inspirowana była przez zachodnie ośrodki dywersji. Świadczy o tym fakt, że pierwsze zebranie zorganizował doktorant M, łącznik centrali, kursujący corocznie jako rzekomy gastarbeiter wakacyjny pomiędzy Polską, Szwecją i innymi krajami kapitalistycznymi.

\*

Informacje „Polityki” obaliły mit docentury, biohistorii i nagrody Nobla. Natomiast wspomniane wyżej rewelacje otoczyły mgr-a N. nimbem tajemniczej wielkości. Jako agent imperializmu cieszył się odtąd szacunkiem całej społeczności akademickiej. Niektórzy profesorowie i docenci, kłaniając się z daleka, uważali za stosowne wymienić z nim kilka uwag na temat pogody[\[7\]](#_ftn7). Inni znów schodzili mu uprzejmie z drogi, a nawet ustępowali miejsca w tramwaju, przenosząc się w przeciwległy jego kraniec, by go nie krępować swoją obecnością[\[8\]](#_ftn8). Doc. V błagał go codziennie na osobności o stypendium zagraniczne. Ambitny starożytnik, dr J, ofiarowywał mu się bez reszty w zamian za wypożyczenie na kilka godzin kieszonkowej Enigmy, za pomocą której miał nadzieję złamać kreteńskie pismo linearne A. Miłym zaskoczeniem był obfity wysyp przyjaciół, którzy ufali mu do tego stopnia, iż bez cienia obaw, w windzie lub na schodach, pragnęli właśnie odeń zakupić większe ilości dolarów. Niezamężne pracownice instytutu zapraszały go na kawę i czyhały na moment, kiedy będą mogły razem z nim opuścić gmach; zawsze przy tym było im po drodze. Mgr Y, padając na kolana, wyznała mu swe uczucia i plan rozwodu z dr-em Y; nie wykluczała zresztą pozbycia się męża w inny sposób („wy macie swoje metody” – zasugerowała). Dwie studentki nawiedziły go w godzinach dyżuru specjalnie po to, by się zadeklarować jako istoty tak naiwne, że aż dziw, iż jeszcze nie stały się obiektem handlu żywym towarem („Chyba się nadajemy?” – spytały i nim zdążył zaprotestować, rozebrały się do naga). Szczególnie dużo odwiedzin miał, gdy mgr O rozpuścił o nim wieść, iż opłaca usługi seksualne włoskimi rajstopami, które to rajstopy nadsyła mu fundusz operacyjny w paczkach po dziesięć sztuk.

\*

Wszystko, co dobre, wreszcie się kończy. Grupa nacisku perfidnego docenta X cierpliwie rozpuszczała plotki negujące powiązania mgr-a N. ze światem imperializmu. Plotki te trafiły w końcu na podatny grunt, jako że mgr N. nie spełniał nadziei swych licznych petentów[\[9\]](#_ftn9). Jak to zwykle bywa, nastrój ogółu zmienił się diametralnie. Podniosły się głosy żądające surowej kary dla uzurpatora i los niedoszłego docenta stał się przedmiotem ogólnonarodowej debaty.

Projekt surowej kary upadł na samym początku storpedowany przez „Solidarność”. Rzecznik prasowy tego związku stwierdził, że prawa człowieka pozwalają każdemu żywić przekonania zupełnie dowolne i to bezkarnie, w tym także przekonanie o tym, że zasługuje na docenturę.

Ekstremistyczny odłam w „Pro Patrii” uznał, że konieczne jest egzorcyzmowanie, ponieważ z mgr-a N. należy wypędzić diabła.

I ten projekt upadł w wyniku sprzeciwu gremiów partyjnych. Uchwaliły one, że diabła nie ma, a gdyby nawet był, to wbrew zasadom sprawiedliwości społecznej byłoby pozbawiać go lokum w dobie powszechnych trudności mieszkaniowych.

Na zebraniu Sekcji Nauki ZNP wezwano władze uczelni do zastosowania wypróbowanych zabiegów dydaktyczno-wychowawczych: nakazania mgr-owi N., by został po godzinach i dziewięćset dziewięćdziesiąt dziewięć razy przepisał zdanie „Nie jestem docentem i nie będę nim w najbliższej przyszłości”.

Natomiast grupa nacisku, za plecami której skrywał się fatalny doc. X, stała na stanowisku, że mgr N. obraził kolegów, instytut i dyscyplinę naukową, którą nieudolnie reprezentuje. Wnioskowała zatem, by postawić go przed wyborem: albo doktorat i habilitacja w ciągu roku, albo wymówienie. Doc. V, zawiedziony z powodu stypendium zagranicznego, sarkał głośno, iż należałoby przedtem po ojcowsku, staropolskim obyczajem, wybatożyć przykładnie samozwańca.

Plany te spotkały się z gwałtowną reakcją młodszych pracowników nauki, którzy dostrzegali w nich niebezpieczny precedens. Zdenerwowany mgr O w gronie zaufanych wysunął tezę, iż mgr N. jest Jonaszem i jako taki ściągnie na resztę magistrów nieszczęście. W związku z tym zaproponował standardowy w takich przypadkach rytuał akademicki: Jonasza należało ogolić, unurzać oraz zawiesić do czasu, aż zdoła sobą zainteresować jakąś grubą rybę.

**Część piąta: faza dochodzeniowo-penitencjarna**

Wobec bezliku stanowisk trudnych do uzgodnienia sprawa sankcji wobec mgr-a N. pozostawała na razie w zawieszeniu. Nie cieszyło to bynajmniej bohatera całej afery. Zaszczuty groźbami rychłej obrony bądź rotacji, snuł się smętnie po instytucie, drżącymi palcy miętosząc papierosa. Opuściły go niezamężne koleżanki; nawet mgr Y ostentacyjnie przechadzała się pod rękę z dr-em Y. Nie nawiedzali go już przyjaciele i naiwne studentki…

A tymczasem nowe niebezpieczeństwa zbierały się nad jego głową niczym gradowe chmury. Pewnego razu, wchodząc do gmachu, ujrzał osobnika w ciemnych okularach zasłoniętego gazetą; osobnik ów spojrzał na mgr-a N. tak badawczo, iż ten, zdenerwowany nie wiedzieć czemu, wkroczył do toalety, by napić się zimnej wody z kranu. I tu aż podskoczył, czując czyjąś dłoń na ramieniu. Nieznajomy w ciemnych okularach odchylił klapę z wiadomym znaczkiem i bez słowa zaprosił go do kabinki. Rozsiadłszy się tam na sedesie, założył nogę na nogę i wpatrzył się w ofiarę wzrokiem bazyliszka.

Mgr N. pocąc się uczuł, iż zwiotczałe kończyny dolne, podpierające jego korpus, przestają równoważyć siłę grawitacji.

– Jestem niewinny – wyjąkał.

– Ba!

– To nie ja – ciągnął rozpaczliwie.

– A kto? – spytał szybko nieznajomy.

– Nie wiem… – wyszeptał zgnębiony mgr N.

Osobnik w ciemnych okularach milczał złowróżbnie. Słychać było tylko szum wody w samoczynnie włączającym się klozecie.

– No, N. – wyrzekł w końcu – przemyślicie to sobie po drodze. Pójdziecie ze mną.

I opuścił ciasne pomieszczenie, wypuszczając przodem roztrzęsionego naukowca.

Zmierzający akurat do pisuaru doc. V aż otworzył usta ze zdumienia, po czym zapomniawszy w podnieceniu zapiąć spodnie, wypadł na korytarz.

– To już koniec! – pomyślał nieszczęsny mgr N.

\*

W gmachu użyteczności publicznej, do którego trafił mgr N., hulały najwidoczniej przeciągi, bo naszemu bohaterowi wciąż było zimno, skutkiem czego nie mógł opanować dreszczy oraz szczękania zębami.

Siedzący za biurkiem funkcjonariusz przyglądał się ofierze z ponurą satysfakcją, jako że mgr N. przypominał mu z pewnych względów polonistę, który to polonista pod pretekstem sanacji ortograficznej zostawiał go notorycznie w kozie. Inne znów aspekty osoby podejrzanego przywodziły raczej na myśl zacofanego metodycznie matematyka utrwalającego uczniom algorytm rozwiązywania słupka za pomocą linijki.

Mgr N. został przed chwilą uświadomiony o wszechwiedzy instytucji, w której się znalazł, jak również otrzymał informację o tym, iż szczere przyznanie się do winy, poparte wyczerpującymi zeznaniami, bywa uznawane za okoliczność łagodzącą. Teraz, zielony na twarzy, spożywał bezwiednie papierosa na kształt słonego paluszka.

– Nie udawajcie idioty, N. – zwrócił mu uwagę zniesmaczony funkcjonariusz.

– Jestem niewinny, nic nie wiem, nie rozumiem, o co chodzi – wyjąkał mgr N. – Jestem niewinny i praworządny.

– Ha! – zakrzyknął jego prześladowca z ironią. – A działalność antypaństwowa, próba puczu? A spekulacja rajstopami? Wykorzystywanie pomieszczeń służbowych do celów nierządnych? Wszystko to nic?

Podejrzany był przytłoczony ogromną ilością zarzuconych mu przestępstw.

– Powieszę się albo zastrzelę! – zakrzyknął nawet.

Funkcjonariusz poderwał się z krzesła.

– Czym się zastrzelicie? – spytał łagodnie, tłumiąc podniecenie.

– Pistoletem…, albo karabinem!…, ewentualnie granatnikiem! – jęknął przesłuchiwany na obraz i podobieństwo niejakiego Piekarskiego[\[10\]](#_ftn10).

Nie ulegało wątpliwości, że informacje specjalnego wysłannika agencji prasowej kraju ościennego odpowiadały prawdzie. Sytuacja była poważna, jako że nastąpiło zagrożenie bezpieczeństwa kraju, a kto wie, czy nie całego Układu Warszawskiego; nie dziwmy się więc, że mgr N. w oka mgnieniu skuty został kajdankami, a jego prześladowca z taśmą magnetofonową w ręku pobiegł do gabinetu szefa.

\*

Tymczasem instytut wstrząsany był rewelacjami doc. V. Niezamężne koleżanki oraz – w szczególności – mgr Y odzyskały poczucie własnej wartości. Tylko posiadaczki imperialistycznych rajstop kręciły głowami.

Mafia doc. X triumfowała i nawet rozpoczęła zabiegi zmierzające do wyrotowania mgr-a N., gdy gruchnęła wieść o aresztowaniu niedoszłego docenta. Plany rotacji musiały upaść, gdyż wyglądały na zbyt daleko posunięte współdziałanie z władzami, niegodne instytucji samorządnej.

Ogólne zamieszanie pogłębiły koła zbliżone do szatni, które ogłosiły dogmat o nieuchronnej inwazji Najwyższej Izby Kontroli. Portierka E rozpoznała nawet w tłumie ob. Moczara Mieczysława; jej bystrego wzroku nie zwiodła przyprawiona broda i nasunięty na oczy kapelusz. Plotka ta wprowadziła nastrój wielkiej nerwowości. Podobno prof. Q oddał książki do biblioteki, a dr Z po przeprowadzeniu rachunku sumienia popadła w histerię. Histeria owa zawiodła ją do gabinetu wicedyrektora F, gdzie przyznała się do kradzieży paczki kredy w roku pańskim 1976 n. e.[\[11\]](#_ftn11). Wyznawszy swój czyn, poprosiła o surową karę, podając jednocześnie jako okoliczności łagodzące staropanieństwo i wystawanie w kolejkach. Następnie padła na kolana deklarując, że jest gotowa na wszystko, byle tylko zmazać swój szpetny występek. Sprzątaczka G, wycierająca przypadkowo kurz z dziurki od klucza, twierdziła potem, że wicedyrektor F wyciągnął wnioski ze wspomnianej gotowości. Zastrzegając, że wiadomość powyższa jest niesprawdzona, trzeba jednak odnotować fakt, iż histeria dr Z przeszła.

\*

Szefem złego, nadgorliwego funkcjonariusza okazał się kapitan B, dobry funkcjonariusz, który rozrzedził nieco duszącą mgr-a N. atmosferę obcości, służąc słowną pomocą, dzięki której podejrzany skojarzył go, choć mgliście, ze swym studentem sprzed lat kilku. Nadmieńmy przy okazji, że jako uzdolniony absolwent studiów historycznych, kapitan B miał profesjonalnie obiektywny stosunek do procesu dziejowego, co objawiało się m. in. sprawnością w wychwytywaniu trendów rozwojowych i brakiem dogmatyzmu w kwestii pryncypiów rządzących oglądem rzeczywistości społecznej.

– Słuchajcie, podejrzany – wyrzekł do uwolnionego z pęt – uznaliście zatem, że NATO górą, jak rozumiem.

– Jak…, co? – wybełkotał przerażony mgr N., znów niebezpiecznie zbliżając się do właściwego Piekarskiemu poziomu wypowiedzi.

– Hm, tak – chrząknął kapitan B – widzę, że atmosfera wam nie służy. Udajmy się lepiej na spacerek. Czy pamiętacie może – zaciekawił się – że swego czasu przegraliście ze mną w akademiku, w pingponga, pół litra w trzech setach?

– Nnnie – wymamrotał mgr N. pełen podziwu dla wszechstronności instytucji.

– Jak to! – zakrzyknął kapitan B – do piętnastu, do szesnastu i do osiemnastu!

I w tym miejscu zadecydował, ku rozpaczy nadgorliwego funkcjonariusza, że podejrzanego należy chwilowo uwolnić od zarzutów, by ten mógł udać się z nim do „Arkadii” w celu realizacji zadawnionych zobowiązań honorowych.

\*

Czysto ludzkie uczucie wdzięczności, tudzież poczucie winy z powodu zwłoki w realizacji zobowiązań honorowych skłoniły mgr-a N. do duplikacji należnego kapitanowi B trofeum, zaś szczupłość zasobów – do zminimalizowania tzw. zakąski. Doskonale to zresztą współgrało z kawaleryjsko-mołojecką tradycją, w jaką biesiadników wyposażył ukończony przez nich kierunek studiów[\[12\]](#_ftn12).

Kapitan B czuł się w obowiązku zadbać o to, by konsumpcji towarzyszył komponent konwersacyjny.

– Ludzie wyobrażają sobie naiwnie – zagaił – że szpiegostwo… pardon, terenowe pozyskiwanie informacji… to romantyczne pasmo emocjonujących przygód. Nie zdają sobie sprawy ze wstrząsającej samotności szpiega… pardon, wywiadowcy. My, fachowcy…

– Ja nie! – jęknął żałośnie mgr N. – Nie jestem fachowcem!

– Rozumiem – zgodził się łatwo kapitan B. – Dzieje wywiadu zaświadczają nam wielokrotnie, iż uzdolnieni amatorzy, zwłaszcza motywowani ideowo, dokonują rzeczy, na które nie ważyliby się bezduszni profesjonaliści.

– Zaręczam panu, kapitanie, że nie mam nic wspólnego…

– To oczywiste. Rozmawiamy czysto teoretycznie; jesteście wszak historykiem wojskowości i w tym sensie fachowcem. Zgódźcie się zatem, że będę z wami rozmawiał jak fachowiec z fachowcem. Dobrze?

Mgr N. zmilczał rozumiejąc, iż ludzie, którzy w trakcie biesiady dopadli swojego konika, nie dadzą się z niego żadnym sposobem ściągnąć.

– Profesjonalizm wyzwala w człowieku szczególnego rodzaju motywacje – ciągnął w zadumie kapitan B. – Dajmy na to oficer taki jak ja… Gdyby się dał zwerbować drugiej stronie… Nie ma w tym nic ze zdrady, bynajmniej. To po prostu chęć sprostania wyzwaniu, podjęcia trudnej gry z dotychczasowymi współpracownikami… Ileż tu możliwości sprawdzenia się, pokazania kunsztu… Rzecz jasna, pełny profesjonalizm wymaga przestrzegania wszystkich reguł gry, co obejmuje, niestety, wobec ułomności tego świata, znaczną sumę w dolarach amerykańskich…

Tu kapitan B przerwał monolog, zauważywszy, iż sterany przejściami mgr N. stracił kontakt z rzeczywistością.

\*

Tymczasem na uczelni trwała normalna robota polityczna. Jak to się stało czymś zwykłym w ostatnim okresie, robota ta koncentrowała się wokół postaci mgr-a N.

I tak NZS uchwaliło potępienie brutalnego aktu ingerencji w wewnętrzne życie uczelni. Przypominając zasługi mgr-a N. na polu dydaktycznym, uznało, że jego aresztowanie jest dziełem antyspołecznych sił zmierzających poprzez eliminację co bardziej zdolnych i nie ulegających naciskom pracowników do uczynienia z uniwersytetu narzędzia prymitywnej indoktrynacji i prania mózgów.

„Solidarność” uczelniana również potępiła wybryk aparatu przemocy wskazując, że aresztowanie bez podania powodów jest zaprzeczeniem praworządności. Dalej w odezwie stwierdzono, że trudno będzie powstrzymać radykalnie nastawionych studentów od akcji protestacyjnej mającej na celu uwolnienie umiłowanego pedagoga.

Podobnie SZSP negatywnie odniósł się do poczynań organów ministerstwa spraw wewnętrznych. W ogłoszonej w związku z tym deklaracji sprzeciwiono się prześladowaniom ludzi za to, że sądzą o sobie, iż mogą i powinni piastować kierownicze stanowiska. Takie przekonania rozpowszechnione wśród szeregowych pracowników wyzwalają przecież inicjatywę, szerzą ducha szlachetnej rywalizacji i sprzyjają społecznej kontroli działania władzy.

Komitet Zakładowy PZPR wyraził zdziwienie, że przed aresztowaniem mgr-a N. nie potrudzono się, by zasięgnąć o nim opinii w środowisku akademickim. Potępiono jednocześnie uzurpatorskie przywłaszczenie sobie tytułu docenta, jak i zbyt ostrą reakcję władz, która niepotrzebnie podgrzewa nastroje na uczelni. Jedynym usprawiedliwieniem mogłaby tu być tylko udowodniona współpraca mgr-a N. z imperialistycznymi ośrodkami dywersji.

\*

Trzeba przyznać kapitanowi B, że okazał się lojalnym współbiesiadnikiem. Widząc niemoc mgr-a N., nie oddalił się bynajmniej po angielsku, by go wydać na pastwę brutalnych kelnerów, lecz przeciwnie, zatroszczył się o niego jak o brata i postarał odtransportować w kierunku najbliższego miejsca nadającego się do spoczynku. Najbliższym takim miejscem był areszt śledczy na Młyńskiej.

Wieczorem następnego dnia troskliwy kapitan B przypuszczając, że mgr N. miał dość czasu, by wypocząć i zregenerować siły, wpadł towarzysko do jego celi, zasiadł na sąsiedniej pryczy i poczęstował znajomka papierosem. Co więcej, postanowiwszy nieco go rozerwać, a przez to odprężyć, zaproponował mu słowną grę towarzyską.

– Jak tam nocne przemyślenia? – zapytał żartobliwie. – Czy mogę liczyć na werbunek?

– Kapitanie – zachrypiał mgr N. usiłując zebrać całą moc magisterskiego intelektu – pan raczy żartować. A gdybym jako praworządny obywatel poinformował pańską firmę o tego typu pańskiej gotowości?

– To pan raczy żartować – odparł kapitan B wyraźnie rozbawiony. – Dostałbym pochwałę za próbę infiltracji wrogiej agentury.

– A więc gdybym nawet był obcym agentem, nie mógłbym pana zwerbować z obawy przed taką infiltracją.

– Dlaczego? – zdziwił się wesoło kapitan B. – Przecież chcąc tak pana podejść, musiałbym pana puścić; pan miałby czas mnie sprawdzić (o ile mówiąc nawiasem, po prostu by pan nie uciekł), a ja musiałbym z panem współpracować, by zdobyć zaufanie. Nie wiadomo z góry, kto kogo by przechytrzył. To jest właśnie ta gra, ta emocja, nie sądzi pan?

Mgr N. nie sądził, a że jego intelekt domagał się wytchnienia, niezbyt ambitnie wycofał się z owej błyskotliwej przekomarzanki.

– Ale ja, tak naprawdę, jestem ofiarą nieporozumienia – powiedział z rezygnacją i zastygł w tragicznej pozie, obejmując głowę rękoma.

Kapitan B wydawał się głęboko rozczarowany rozmiarami obserwowanego marazmu. Zaklął z cicha, wydobył machinalnie portmonetkę i spojrzawszy na nią z dziwną żałością, schował na powrót. Po długim i brzemiennym w namysł epizodzie perypatetyckim odzyskał widać nadzieję na tchnienie w mgr-a N. pogodnego optymizmu, bo z nagła zaproponował filuternie:

– A może zrobimy to inaczej i to ja was zwerbuję, N.?

Nie widząc o interlokutora oznak ożywienia, uznał, iż wymaga on dłuższego odpoczynku, więc wyszedł, zamykając kratę, by mgr-owi N. nie przeszkadzano.

\*

Minęło czterdzieści osiem godzin. Bezduszne przepisy nie pozwalały kapitanowi B gościć dłużej mgr-a N. mimo, iż było widoczne, że oderwanie od trosk życia działało na niego kojąco. Obiecawszy mu więc usługę hotelową w najbliższym możliwym czasie, rozstał się z nim z żalem. Przedtem jednak, zniewolony nękającą nawet jego, wszechobecną biurokracją, podsunął mgr-owi N. do podpisu stosik papierków natury formalnej. Mgr N. podpisał je bez czytania, na co kapitan B zareagował z sympatią, gdyż miło mu się zrobiło na myśl, iż nie tylko on dystansuje się od krępujących wszelką inicjatywę objawów przerostu administracyjnego.

**Część szósta: samo dno**

Uwolnienie mgr-a N. uczelnia początkowo powitała z uczuciem zawodu: kampania na jego rzecz dopiero się rozkręcała i nieplanowany powrót bohatera był równoznaczny z marnotrawstwem społecznej energii.

Wkrótce jednak niesmak zamienił się w głęboką niechęć i konsternację pod wpływem wieści o libacji w „Arkadii”[\[13\]](#_ftn13). Stało się jasne, że mgr N. – choć nie imperialistycznym – to jednak agentem jest.

Doc. X rwał włosy z głowy; obawiając się odwetu, nosił stale przy sobie ręcznik, mydło, szczoteczkę do zębów i zmianę skarpetek. Myśl o wyrotowaniu mgr-a N. całkiem już porzucił: o ile z punktu widzenia władz uczelnianych zwolnienie aresztanta trąciłoby kolaboracją, to wymówienie agentowi kojarzyło się natychmiast z opozycjonizmem ekstremalnym.

Aktywiści wszelkiej maści chyłkiem zrywali plakaty nawołujące do zbierania podpisów pod petycją w obronie więźnia politycznego N.; jednocześnie jeden po drugim udawali się na krótki wypoczynek. Niewydolność kierownictwa politycznego spowodowała brak oficjalnych komentarzy. Tylko dzięki akcjom oddolnym wykwitały anemicznie pojedyncze ulotki, których treść oscylowała pomiędzy ubolewaniem nad nieufnością władz, a nawoływaniem „Czuwaj! Wróg nie śpi!”. We wszystkich kręgach dał się odczuć renesans kawałów o milicjantach.

\*

Mgr N. nie zdawał sobie początkowo sprawy z nastrojów. Nie zastanowiło go ani to, dr Z w odpowiedzi na jego przyjacielskie klepnięcie w ramię popadła w godzinne omdlenie, ani też fakt zawału serca u prof. Q, do którego wyrzekł niewinne: „Panie profesorze, miałbym do pana sprawę na boku…”.

Za niesmaczny żart uznał skropienie go wodą święconą; czynu tego dokonało dwóch działaczy „Pro patrii” w kominiarkach. Podobnie zakwalifikował wybryk studentki, która wyrwana do odpowiedzi odmówiła wszelkich zeznań pod pretekstem, że każde jej słowo może być użyte przeciwko niej.

Poważnego natomiast wstrząsu doznał w momencie, gdy zaproponował mgr-owi K. zwyczajową po godzinach dyżuru degustację symbolicznej ilości piwa. Mgr K na ową propozycję zareagował chorobliwym drżeniem („In vino veritas”), oddalił się biegiem, a po pół godzinie dostarczył zeznania na piśmie. Co gorsza, podobną niechęć do napojów wyskokowych demonstrowali również pozostali asystenci i doktoranci.

Całkowite zrozumienie osiągnął mgr N. wówczas, gdy sprzątaczka G uprzejmie „panu inspektorowi” doniosła o tym, co zauważyła podczas pucowania dziurki od klucza. Zastrzegła się tylko, że w sądzie nie przysięgnie; postarała się to uzasadnić (bądź co bądź stykała się z nauką na co dzień) względem na drugą pozycję dekalogu.

\*

Pięć dni podobnego traktowania nadwerężyło psychikę mgr-a N. Wieczorem dnia piątego zastajemy go na strychu pewnej kamienicy skręcającego powrósło z materiałów zastępczych. Kto wie, co byłoby dalej, gdyby nie wtargnięcie na strych czterech zdeterminowanych obywateli wiedzionych palącą potrzebą konsumpcji wysoce kalorycznego napoju, który mieli zamiar zakąszać winem krajowym.

Serce mgr-a N., którego zresztą wzięli początkowo za chałupnika, podbili zwłaszcza głęboką filozoficznie tezą o tożsamości bytu z odbytem; podkreślamy to, by przeciwstawić się rozpuszczanym przez mgr-a O plotkom o tym, jakoby zmiękczenie serca mgr-a N. osiągnięte zostało za pomocą musztardówki przechodniej.

Młody uczony również przypadł przybyszom do gustu. Przekonawszy się o rozległości jego wykształcenia i bezinteresowności finansowej, mianowali go szybko swym guru. Wkrótce zresztą zapragnęli przygarnąć do rzewnych piersi całą społeczność akademicką, w związku z czym zażądali od guru, by ich wwiódł w żeński dom studencki. Mgr N. podzielając ich sympatie, podjął się chętnie przewodnictwa, wszakże dla osiągnięcia odpowiedniego stanu ducha zasugerował uprzednie ćwiczenia w wykonawstwie pieśni masowych, zwłaszcza studenckich.

Niestety, lokatorzy kamienicy nie wykazali zrozumienia dla ich produkcji i przy użyciu telefonu spowodowali przeniesienie prób do noclegowni z wygórowaną odpłatnością.

**Część siódma: szczyt**

Kiedy mgr N. opuścił noclegownię o przesadnej odpłatności, sytuacja znów zmieniła się radykalnie. Niedoszłym docentem zainteresował się imperialistyczny Zachód. Najwidoczniej nie dotarły tam jeszcze wieści o jego komitywie z kapitanem B, bo wylansowano go na symbol uciśnionej inteligencji niezależnej.

Za pośrednictwem dywersyjnych rozgłośni dotarł do skołowanej społeczności uniwersyteckiej świetlany obraz mgr-a N. jako zdolnego uczonego, którego dorobek nie może przebić się przez cenzurę, gdyż jest znanym z prawości i bezkompromisowości bojownikiem o prawa człowieka i demokrację.

Pracownicy instytutu byli zmęczeni i zdezorientowani ciągłymi metamorfozami mgr-a N. Przeważał sceptycyzm, czekano na fakty. Nikt nie ryzykował oficjalnych komentarzy; panowała opinia, że wypowiedź na temat mgr-a N. przynosi nieszczęście; sprytni komiwojażerowie zarobili sporo rozprowadzając wśród działaczy amulety chroniące przed zetknięciem się z odpryskami „Afery Docenta”.

Wkrótce jednak gwiazda mgr-a N. zajaśniała pełnym blaskiem, a niewiernym Tomaszom podstawiono pod nos niezbite dowody.

\*

Kilka dni później na parking przed Collegium Novum wjechały dwa potężne samochody ciężarowe. Elegancki cudzoziemiec z aktówką podążył w kierunku portierni i za chwilę gruchnęła wieść, iż poszukuje on mgr-a N. W atmosferze sensacji i podniecenia przerwano zajęcia, a Instytut Anglistyki w komplecie zbiegł do hallu, by się narzucić jako tłumacz kolektywny. Wyłowiono mgr-a N. z bufetu. Nadbiegły władze dziekańskie i instytutowe, a że fama doszła już do rektoratu, po jakimś czasie, nie bacząc na godność, nadbiegli spoceni rektorzy.

Cudzoziemiec okazał się być plenipotentem Akcji Dobroczynnej z Edynburga obarczonym misją dostarczenia darów prześladowanym naukowcom polskim. Wiadomość owa wywołała duże ożywienie i samorzutnie uformował się Społeczny Komitet Rozdziału Darów z rektorem J na czele.

Niestety, wyszło na jaw, że sponsorzy zostali omamieni wrogą naszemu krajowi propagandą i nie mają zaufania do nikogo prócz znanego im z nieposzlakowanej opinii mgr-a N. Jemu to nadali wyłączne prawo dysponowania darowizną.

\*

Kiedy mgr N. wrócił do swego gabinetu, wtargnął za nim tłum prześladowanych uczonych wyraźnie oczekujących odeń natychmiastowego podjęcia obowiązków.

Sytuację uporządkował nieco dziekan L., przekazując mgr-owi N. swój gabinet, sekretarkę i ekspres do kawy.

– Proszę ustawić się w kolejce za drzwiami – polecił petentom i kiedy zostali sami z mgr N., wygłosił mowę demaskującą ciemne machinacje kliki profesorów z powiązaniami. Profesorowie ci – twierdził – co rusz podstawiali mu nogę, hamowali habilitację i usiłowali nie dopuścić do objęcia przezeń piastowanego obecnie stanowiska. Powodem takiego traktowania był jego pozytywny, by nie rzec: przyjacielski stosunek do młodszych pracowników nauki, którym jak mógł, ułatwiał karierę wbrew tym wszystkim konserwatystom, despotom i feudałom.

– Ja też jestem na dorobku – chrząknął na koniec znacząco. Wkrótce się okazało, że problemy dziekana L były typowe dla samodzielnych pracowników nauki.

Działacze społeczni nawiedzający mgr-a N. byli ożywieni duchem altruizmu. Bezinteresownie proponowali mu pomoc w trudnym dziele sprawiedliwego rozdziału. Dla usprawnienia pracy podsunęli pomysł zaprowadzenia pisemnych podań, a nawet opracowali listę koniecznych załączników. W międzyczasie wdawali się w szczere rozmowy na temat katorżniczej doli społeczników, którzy harują dla dobra ogółu, za co są opluwani i karmieni czarną niewdzięcznością; inny ich problem polegał na tym, że ustawicznie wtrącają im do ich działania swoje trzy grosze administracja oraz różnego kalibru komitety.

Młodzi pracownicy nauki byli wszyscy bez wyjątku zabiedzeni, zaganiani w pracy, tyranizowani i niedocenieni, a ponadto nie mieli pleców, jak niektórzy inni (tu wskazywali niekiedy przedstawicieli tych niektórych innych).

**Epilog: i co dalej?**

Przez dwa tygodnie mgr N. był centralną postacią uczelni. Pławił się w szacunku otoczony grzecznościami. Całowany ukradkiem po rękach, podejmowany pod kolana zatracił poczucie rzeczywistości.

Ale w końcu odjechały do Edynburga puste ciężarówki. Dziekan L zażądał zwrotu gabinetu, kawy i sekretarki; mruczał przy tym coś o niedopuszczalnym stopniu ich zużycia. Samodzielni pracownicy nauki poczęli bezwzględnie egzekwować ukłony. Wszyscy zaś snuli arytmetyczne rozważania na temat różnicy pomiędzy dobrem dostarczonym a rozdysponowanym. Na całe szczęście nikt nie był pewny, czy przypadkiem nie nadjadą następne kontenery, więc wątpliwości były wyrażane po cichu.

\*

Niebawem wprowadzono stan wojenny. Mgr N. na czas jakiś zniknął. Dr Z woziła mu paczki do internatu w Gębarzewie, ale dr Z widziała go w okolicach Collegium Minus w mundurze i hełmie z plastykową przyłbicą. Natomiast profesorowej Q mgr N. objawił się we śnie jako mieszkaniec czeluści piekielnej.

Po wznowieniu zajęć mgr N. powrócił do pracy, jednakże przeważnie przebywał na zwolnieniu lekarskim. Małżeństwo Z, będąc razu pewnego w stolicy, zaobserwowało go w chwili, gdy wysiadał z limuzyny w towarzystwie gen. Jaruzelskiego. Jakiś czas potem sekretarz komórki partyjnej dostrzegł go, jak odprawiał nabożeństwo polowe w towarzystwie Jana Pawła II, który mu zresztą służył do mszy.

Wśród pracowników instytutu pojawili się mistycy, dla których mgr N. jest Fantomasem, Magiem i Wielkim Kameleonem; raz w tygodniu składają mu oni stosowne ofiary w „Cechowej”. A cyniczny mgr O przyjmuje zakłady, kim okaże się jutro mgr N.

My tego nie wiemy.

[](#_ftnref1)1/Jedynym anonimem tolerowanym przez mgr-a N. jest tzw. Gall Anonim; mgr. N. sądzi zresztą, że przypisywany wspomnianemu anonimowi grubymi nićmi szyty panegiryk jest autorstwa niejakiego Bolka (ps. „Krzywousty”), prekursora propagandy sukcesu.

[](#_ftnref2)2/Termin „docent mianowany” miał w języku uczelnianym onego czasu dwa znaczenia o odmiennym zabarwieniu wartościującym. W oficjalnych wystąpieniach donotował po prostu pewien szczebel hierarchii akademickiej ze wskazaniem metody jego obsadzenia, zaś w rozmowach nieoficjalnych (z naciskiem na człon drugi) używany był w charakterze epitetu o znaczeniu pośrednim między znaczeniami terminów „cymbał” i „skurwysyn”.

[](#_ftnref3)3/Nie chodziło tu o tzw. dupę Maryni, co mogłoby sugerować, że mgr N. jest znajomym żony wiceministra. W fachowym gronie historyków Maryna kojarzona jest z niejakim Dymitrem Samozwańcem z epoki, w której polska inicjatywa prywatna miała wpływ na obsadę najwyższych stanowisk państwowych w kraju naszych wschodnich pobratymców.

[](#_ftnref4)4/Dla osób bez wykształcenia historycznego podajemy pierwszą część tego rozumowania. Współczesny relacjonowanym tu wypadkom stan refleksji nad dziejami polskiego czynu zbrojnego upoważniał do akceptacji następującej tezy: „Dla każdego x (w dowolnym punkcie czasoprzestrzeni): jeżeli x jest Polakiem, to o ile x nie podejmuje ekspansji na wschód, to x bije się tylko o słuszną sprawę”. Z tej przesłanki oraz zdań: „Mgr N. jest Polakiem”, „Mgr N. pobił dr-a A” i „Mgr N. bijąc dr-a A nie podjął ekspansji na wschód” wynika logicznie zdanie: „Pobicie dr-a A przez mgr-a N. było słuszne”. Reszta jest kwestią prostej interpretacji humanistycznej.

[](#_ftnref5)5/Pewnego razu, co prawda, przebywali razem na przyjęciu u prof. D, gdzie mgr N. z sympatii dla gospodarza podjął się pieczy nad kuchnią i zaopatrzeniem, by ułatwić profesorowej zajmowanie się gośćmi. Zasłabli nawet jednocześnie z prorektorem C w łazience, której parametry sprawiły, iż na posadzce weszli w nader bliski kontakt fizyczny. Rozdzielono ich jednak, zanim zdołali wymienić wizytówki.

[](#_ftnref6)6/Winnych znalazła wkrótce „Wolna Europa”. Jak wynikało z jej doniesień, praca magisterska ob. E. Gierka zawierała tezy sprzeczne z aksjomatami biohistorii.

[](#_ftnref7)7/Jednostki obdarzone stalowym charakterem śmiało, choć dyskretnie, wypytywały o poziom opadów na wschodnim wybrzeżu Stanów Zjednoczonych, a zwłaszcza w Wirginii; prof. R wprost zainteresował się średnimi opadami lipca w Langley.

[](#_ftnref8)8/Prof. W ustąpił mu razu pewnego miejsca na korytarzu, kurtuazyjnie usuwając się do windy; zaś kiedy się zorientował, iż mgr N. w ślad za nim wszedł do tejże, przez klapę w suficie, uniesiony rewerencją, wydostał się na dach kabiny.

[](#_ftnref9)9/Jeżeli chodzi o rajstopy, które podkradał rodzinie i znajomym, to skończyły mu się po wizycie pewnej damy, która zgłosiła zapotrzebowanie od razu na dwie paczki; nawiasem mówiąc, interweniowało pogotowie.

[](#_ftnref10)10/Piekarski był, podobnie jak mgr N., znanym zamachowcem; tyle, że siedemnastowiecznym. Swego czasu napadł był z bronią w ręku na VIP-a rodem ze Szwecji, Zygmunta III Wazę, który nie wiedzieć czemu piastował akurat stanowisko głowy państwa w Polsce. Mgr N., bredząc bez sensu w śledztwie tak, jak to w podobnych okolicznościach czynił Piekarski, dostarczył organom ścigania przesłanek do posądzeń o zamiar eliminacji I sekretarza PZPR z powodu powiązań sojuszniczych Polskiej Rzeczypospolitej Ludowej.

[](#_ftnref11)11/Łup wymieniła z rolnikiem indywidualnym na kilogram kaszanki; kreda posłużyła do bielenia kurnika.

[](#_ftnref12)12/Pod względem rozrzutności w zakresie dania głównego i oszczędności w zakresie zakąski kierunek historyczny ulegał jedynie szacownemu wydziałowi prawa. Tradycja wspomnianego wydziału nie zasadzała się, oczywiście, na legendzie kawaleryjsko-mołojeckiej, lecz miała swoje korzenie w prawie rzymskim, którego to prawa paragrafy zaczynały się od słynnej maksymy „In vino veritas”. Wśród prawników ta kwintesencja starożytnych kodeksów wszelkiego rodzaju postepowań traktowana była tak poważnie, iż każdego roku promotorzy byli nękani licznymi propozycjami dysertacji mających udowadniać celowość przeprowadzania procesów poszlakowych na terenie placówek gastronomicznych z wyszynkiem; postulowano przy tym, rzecz jasna, by w trakcie posiedzeń zakazane było spożywanie dań o konsystencji stałej i płynów nie zawierających serum prawdy – i to nie tylko dlatego, iż mlaskanie i odgłosy trawienia uwłaczałyby powadze sądu.

Jeśli chodzi o tradycję kawaleryjsko-mołojecką patrz też przypis 13.

[](#_ftnref13)13/Nie wspominaliśmy o tym dotąd, ale jest faktem, iż w pewnym momencie doszło tam do zaimprowizowanego meczu, w którym sznycel zastąpił piłeczkę pingpongową, zaś półmiski pełniły funkcję rakietek. Singel zamienił się w debla, gdy z rozkazu kapitana B do gry włączył się zawezwany przez kierownictwo lokalu patrol MO.