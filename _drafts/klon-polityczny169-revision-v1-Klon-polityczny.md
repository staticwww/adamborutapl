---
id: 171
title: 'Klon polityczny'
date: '2014-11-11T15:56:37+01:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2014/11/169-revision-v1/'
permalink: '/?p=171'
---

KLONY POLITYCZNE

Ci nieobliczalni Szkoci sklonowali owcę (zapewne po to, by oszczędzić na inseminatorze). Zważywszy na skłonność naszych współobywateli do beczenia, łatwo o wniosek, że tylko krok dzieli nas od klonowania statystycznych Polaków.  
Byłoby to bardzo dobre dla wszelakich sił politycznych: partii, stronnictw, zrzeszeń, związków i lobby. Można byłoby pobrać od lidera paznokieć albo i nagniotek, a potem wyprodukować na obraz i podobieństwo odpowiednią ilość egzemplarzy wyposażonych w prawo do głosowania. Cały potencjał inwestycyjny państwa poszedłby w kliniki położnicze (cieszcie się, lekarze) i żłobki; po paru latach rozwinęłoby się budownictwo przedszkoli, potem szkół; jeśli chodzi o budownictwo mieszkaniowe, zostawiłoby się klonom wolną rękę, nie szczędząc im bynajmniej tektury, pustych kanistrów, ulistnionych gałęzi i igliwia  
Ale nie. Nie tędy droga. Klony liderów jęłyby się polityki i hurmem obskoczyły dolne szczeble drabiny, zębami i pazurami torując sobie drogę ku górze. Każdy kto z wyżyn obsunąłby się ku grawitacji, niechybnie by został zdeptany lub uduszony, a może i pożarty.  
Znacznie lepiej klonować sympatyków. Sympatyk – wierny wyborca wie, że jego idol – lider jest mądry i dzielny; sam nie pcha się do władzy, a tylko z cielęcym uśmiechem wpatruje się w oblicze wybrańca swego.  
No to teraz mamy następny problem: jak wytypować obywateli do sklonowania. Trzeba byłoby znaleźć takich wyborców, którzy reprezentują pożądaną średnią. Pana i panią. Zbadajmy zatem wizerunki statystycznych sympatyków w oczach ich politycznych rywali.  
Sympatyk SLD to osobnik z nadwagą. Oczka ma małe, bo świńskie. Nos czerwony od podlewania idei. Garnitur przyrośnięty do ciała. Czerwony krawat, muszka bądź apaszka – niekoniecznie. U pasa wiszą mu czaszki nienarodzonych dzieci. Sympatyczka SLD to zasuszona wiedźma z haczykowatym nosem. W sklepach nuci „Międzynarodówkę”. W butonierce – prezerwatywa zamiast fiołków.  
Sympatyk PSL to chłop żylasty i kołtuniasty. Oczka ma średniej wielkości, bo co mu je okowita zwęzi, to zdziwienie Europą – rozszerzy. Kufajkę chętnie zamienia na podkoszulek. Jest nastawiony prokreacyjnie, bo w gospodarstwie rąk do pracy nigdy dosyć. Sympatyczka PSL chustkę ma na głowie. W prawej kieszeni trzyma książeczkę do nabożeństwa, w lewej – pigułki antykoncepcyjne, z których codziennie się spowiada. Patrzy koso.  
A oto sympatyk ZChN-u. Ubrany jak kościelny w Boże Ciało. Do rogatywki ma zręcznie uczepione skalpy nieprawdziwych Polaków. Oczy szeroko otwarte, bo a nuż trafi się po drodze kiosk z pornografią; obejrzy sobie, zanim wybuchnie oburzeniem. No proszę, a tu widzimy połowicę jego, ślepka ma załzawione od ciągłego wzdychania na ciężkie czasy i upadek obyczajów. Zadek wdzięcznie jej się kołysze, bo znowu przed wyjściem zapomniała nałożyć pas cnoty.  
Takie klony podstawiliby wzmiankowanym ugrupowaniom rywale. Sobie przydzieliliby inne: przystojnego Europejczyka z energiczną inteligentką pracującą, pracowitego Sarmatę z zapobiegliwą jejmością, cnotliwego rodaka z Matką Polką. Jedno ich łączy: wyobrażenie o przeciwniku jako indywiduum paskudnym i śmiesznym.  
Wróćmy do klonowania. Wypisuje się o nim w kółko i straszy. Że a nóż powielą Hitlera, Łukaszenkę, Kwaśniewskiego czy generała jezuitów. Ale z dotychczasowych badań wynika tylko tyle, że klon jest w miarę fizycznie podobny do dawcy. Wszystkie chyba doktryny psychologiczne i prawie wszystkie filozoficzne zakładają, że poglądów się nie dziedziczy (co najwyżej jakieś predyspozycje). Klon Hitlera może przystąpić do pacyfistów, klon jezuity stać się zajadłym ateistą, zaś klon Kwaśniewskiego – upodobać sobie profesję organisty.