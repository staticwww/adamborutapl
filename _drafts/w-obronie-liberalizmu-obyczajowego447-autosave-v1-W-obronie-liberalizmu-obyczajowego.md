---
id: 449
title: 'W obronie liberalizmu obyczajowego'
date: '2015-09-27T20:10:25+02:00'
author: 'Adam Boruta'
layout: revision
guid: 'http://adamboruta.pl/2015/09/447-autosave-v1/'
permalink: '/?p=449'
---

W OBRONIE LIBERALIZMU OBYCZAJOWEGO

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)

Emeryci polscy z niepokojem oczekują na zalew imigrantów muzułmańskich. Opiniotwórcze media przestrzegają, że przybysze są nosicielami fundamentalizmu obyczajowego, który zagraża liberalnym wartościom, jakie emeryci dopiero co sobie przyswoili . Bo liberalizm obyczajowy stał się częścią polskiej kultury, ważną składową narodowej tożsamości. Toteż szczególne obawy weteranów pracy rodzi sojusz islamistów z katolicką konserwą. Niby dlaczego hierarchowie zaplanowali umieszczenie w każdej parafii muzułmańskiej rodziny? Czy ruch księży patriotów, jaki się tu i ówdzie objawił, zdoła dać temu odpór?  
Postęp medycyny i idące w ślad za tym wydłużenie wieku oraz ogólnej sprawności podtrzymuje w emerytach zainteresowanie seksem, a przemiany obyczajowe pozwalają im się tego nie wstydzić. Nie znaczy to, oczywiście, że po przekroczeniu progu emerytury nic się w tej materii nie zmienia. Weteran pracy z upływem czasu coraz częściej zaczyna przechodzić od fizycznie wyczerpującej erotyki czynnej do form wysublimowanych o charakterze przeżycia estetycznego. Proces ten już w starożytności udokumentowała niejaka Zuzanna, prekursorka higieny.  
Niestety, emerytki niezbyt często są w stanie zastąpić Zuzannę. A pozadomowe wrażenia estetyczne zazwyczaj kosztują. Szczupłość emerytur, zachłanność wnuków i procesy inflacyjne utrudniają korzystanie z wyspecjalizowanych kanałów telewizyjnych, stron internetowych i wydawnictw ilustrowanych. O zatrudnianiu trzymających poziom modelek lepiej nie wspominać.  
Jedyne, co bez specjalnych ograniczeń jest dostępne emerytom, to kontemplacja przestrzeni publicznej wymagająca od nich co najwyżej jednorazowych wydatków na wyroby przemysłu optycznego. I ta ostatnia ich pociecha jest zagrożona przez inwazję fundamentalistów islamskich dyszących nienawiścią do oszczędnych spódniczek, niskonakładowych bluzeczek i fryzur niezabezpieczonych przed czynnikami atmosferycznymi. Uduchowione kazania i protesty aktywistek parafialnych nie były dotąd w stanie zatamować postępu w modzie. Teraz wszystko się odmieni. Konserwatywny kler i fanatyczne kółka różańcowe wezmą na siebie pijar, zaś najemnicy parafialni z Bliskiego Wschodu, przechadzając się w pasach szachidów, wybiją kobietom postępową modę z głowy.  
Dlatego emeryci będą głosować na partie panów Kaczyńskiego i Gowina jedynych prawdziwych obrońców liberalizmu obyczajowego.