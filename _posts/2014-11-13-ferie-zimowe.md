---
id: 222
title: 'Ferie zimowe'
date: '2014-11-13T17:50:38+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=222'
permalink: /2014/11/ferie-zimowe/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony z lat 90-tych'
tags:
    - ferie
    - 'żywiołowa młodzież'
---

**JAK PRZEŻYĆ FERIE**

Ledwieśmy odpoczęli po świętach Bożego Narodzenia, a już dopadł nas Sylwester. Kiedy wspomnienia naszych wyczynów z przełomu lat jęły się z wolna zacierać, nadeszły zimowe ferie w szkołach. I znowu trzeba odpoczywać. A mało jest tak wyczerpujących zajęć jak odpoczynek; tak naprawdę wytchnienie przychodzi do nas tylko w pracy – od czasu do czasu.  
Ferie to okres, w którym nauczyciele nie pilnują przez połowę dnia naszych pociech i kiedy dostajemy wezwanie na komisariat w celu odebrania zatrzymanego właśnie potomka, nie możemy sobie ponarzekać na nieróbstwo i niekompetencję ciała pedagogicznego. Zamiast tego musimy bąkać coś na temat demoralizacyjnego wpływu nadmiaru czasu na nadwrażliwą młodzież. Kiedy sąsiad domaga się odszkodowania za lakier samochodu, w którym to lakierze nasza dziecina pracowicie wyryła gwoździem napis „brudas”, zmuszeni jesteśmy improwizować, tłumacząc sąsiadowi, iż o tej porze dziecina zwykła być wzywana do tablicy, by smarować po niej kredą; „to odruch warunkowy” pleciemy żałośnie „nawyk rodem ze szkoły, który musi się zrealizować, a tymczasem klasy zamknięte na trzy spusty”. Zaraz potem nadbiega sąsiadka, której nasza córunia wyrwała torebkę i znów musimy wyjaśniać, że to po prostu pora dużej przerwy, kiedy to dzieci młodsze dzielą się tradycyjnie swoim drugim śniadaniem z dziećmi starszymi, o wyższej kategorii wzrostowej i wagowej. „Pani taka drobniutka” wzdychamy „a ta pani torebka wygląda jak tornister”. Wiśniewskiego, któremu syn nasz przypalantował w ucho, uspokajamy z trudem prosząc, by nie widział w tym nic osobistego a tylko zbieg okoliczności, który spowodował, iż jest on w typie urody zbliżony do pana od matematyki.  
Niektórzy rodzice, chcąc uniknąć nieprzyjemności wskazanego wyżej rodzaju, na czas ferii wysyłają dzieci do krewnych na wieś, sami zaś pławią się w ciszy i spokoju. Po paru dopiero dniach przychodzi telegram z zawiadomieniem o wydaleniu potomstwa ze spokrewnionej chałupiny i żądaniem ich natychmiastowego odbioru. Na miejscu dowiadujemy się o paru niewinnych psotach, które zbulwersowały nietolerancyjnych wieśniaków. Skarży się kot Mruczek z puszką przymocowaną do ogona, pies Burek odmawiający ciągnięcia sanek i koń upodobniony do zebry przy zastosowaniu szwarcpasty. Sytuacji nie poprawia ani bałwan uczyniony na obraz i podobieństwo miejscowego sołtysa, ani płaczliwa Krysia z sąsiedztwa ciągle przywiązywana do pala męczarni. Mimo, że straty materialne nie są duże (jeden spalony stóg), opinia wiejska stawia zdecydowane veto.  
Dobrym pomysłem, łatwym do zrealizowania dla co majętniejszych rodziców, wydają się kolonie. Nie dziwi nas, że kierowca kolonijnego autobusu stawia się do pracy w stanie wskazującym na spożycie. Nikt trzeźwy nie usiadłby za kierownicą pojazdu wypełnionego wyjącą tłuszczą podekscytowanych małolatów. Tylko zamroczeni wychowawcy mogą zdzierżyć gry i zabawy podróżne uprawiane przez podopiecznych. Aby dotrzeć na miejsce, zlikwidować trzeba dwie próby uprowadzenia pojazdu, trzy usiłowania podpalenia, cztery przypadki rozboju i dwadzieścia dwa omdlenia. U celu podróży rozdzielić należy drogą losowania wymieszane dokładnie bagaże i trzynaście portmonetek znalezionych za pazuchą przerośniętego Józia. Rozdzielić trzeba też Beatkę, Iwonkę i Kasię, które zżera zazdrość o przystojnego Waldka. Ale to przecież zmartwienie najętego personelu. Rodzice mogą zająć się domem albo chociażby sobą. Muszą tylko uodpornić się na listy upominające się o gotowiznę, bądź przepełnione chęcią powrotu z uwagi na nudę i przerażający reżim nakazujący codzienne mycie zębów. Za to te z dzieci, które przeżyły trzy kocówy i dwa zatrucia salmonellą, wracają do domu nieco wyładowane. Wyładowane z energii, zaskórniaków, dwóch swetrów i kożuszka.  
Rodziciele szczególnie żywotni i tacy, którym wszystko jedno, mogą się zdecydować na czwarty sposób odbycia ferii, a mianowicie na wyjazd w Polskę razem z milusińskimi. Jeśli tylko są w stanie przełajowo ciągnąć sanki, wyławiać topielców z przerębla, gasić góralskie chaty z drewna i nie popadać w obłęd od solidnej dawki decybeli. Jeśli potrafią pogodzić Jasia, który chce lepić bałwana, z Gosią, która zamierza iść na ślizgawkę.  
W przyszłości ferie się będzie załatwiać dzięki technice kosmicznej. W okresie wolnym od nauki potomstwo będzie się hibernować w rodzinnej zamrażarce. Nareszcie będzie higienicznie, w ciszy i bez problemów wychowawczych. Daj, panie Boże, doczekać.