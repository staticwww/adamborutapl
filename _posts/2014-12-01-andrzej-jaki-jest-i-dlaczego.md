---
id: 289
title: 'Andrzej jaki jest i dlaczego'
date: '2014-12-01T17:47:48+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=289'
permalink: /2014/12/andrzej-jaki-jest-i-dlaczego/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - Andrzej
    - Herod
    - 'imię Andrzej'
    - Jędrzej
    - męskość
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/12/Andrzej-jaki-jest-i-dlaczego.pdf)

ANDRZEJ JAKI JEST I DLACZEGO

(rozprawa ta jest prezentem imieninowym dla mojego brata)

Podobno imię „Andrzej” wywodzi się ze starożytnego greckiego słowa lub kilku pokrewnych słów, które można było odnosić do ludzi w ogóle, ale szczególnie do osób o cechach męskich. Najprostsza wykładnia jest więc taka, że tym imieniem nazywać się powinno mężczyznę, ale chłopa – albo też kobietę, ale babę chłopa.

Jak wiadomo, cechy męskie dzielą się na anatomiczne i charakterologiczne, a te dwa typy przymiotów nie zawsze w pełnym zakresie sobie towarzyszą. Przeciwnie, występują w rozmaitych kombinacjach. Z tego względu imię Andrzej też występuje w wielu odmianach, które umożliwiają dość precyzyjne rozróżnianie stanów faktycznych.

Zwłaszcza u kobiet męskość anatomii dość rzadko towarzyszy męskości charakteru; co więcej, koincydencja taka bywa ukrywana – niegdyś za pomocą krynolin, obecnie za pomocą chirurgii plastycznej. Ale nie raz już tak było, że szeptem rozpowszechniane doniesienia obnażały ten czy ów sekret kobiecości. Niektórzy językoznawcy w obnażeniu tajemnicy wzmiankowanej koincydencji dostrzegają źródłosłów imienia Jądrula, które jest jedną z żeńskich form imienia Andrzej.

Sekrety kobiecości, nawet obnażone, nie wychodziły jednak w przeszłości poza dość wąski krąg wtajemniczonych. Dlatego nie dziwi, że imię Jądrula było równolegle używane jako męskie, denotując pewną nadmiarowość ściśle określonej cechy anatomicznej. W językach słowiańskich imiona o formie sugerującej rodzaj żeński często oznaczają mężczyzn, we wschodniosłowiańskich jest to nagminne – np. Nikita czy Sawa.

Inną odmianą interesującego nas imienia, także wskazującą nadmiarowość męskiej cechy anatomicznej – odrębnej wszakże od sugerowanej przed chwilą – jest Ondrążek; zauważmy dla porządku, iż to miano niekiedy występuje w ekstremalnej formie Ondrąga.

Ekstremalne nagromadzenie charakterologicznych wad męskich objawianych przez kobiety przyczyniło się do pojawienia się kolejnej formy żeńskiej imienia Andrzej czyli Jędzy. Zamiast Jędza mawiano też wymiennie Herod baba. Osoby bez wykształcenia klasycznego może dziwić taka wymienność, gdyż mają opory z kojarzeniem Andrzeja z Herodem. Dla znawców greki nie jest to jednak niczym osobliwym, jako że oba imiona pochodzą od słów bliskoznacznych, bo w starożytności chętnie przyjmowano, że męski to przede wszystkim dzielny. \[Nawiasem mówiąc, pokrewieństwo Andrzeja z Herodem powinno zainteresować osoby parające się katechezą w placówkach oświatowych. Przecież jedną z podstawowych metod katechezy jest wpajanie młodzieży prawd wiary i zasad chrześcijańskiej moralności za pomocą analizy przypowieści biblijnych, zaś podstawową trudnością w tym dziele jest to, że starożytni bohaterzy owych przypowieści zdają się uczniom obcy, a przez to niekiedy niezrozumiali. Gdybyśmy zastąpili obcego Heroda swojskim Andrzejem, wpłynęłoby to korzystnie na proces dydaktyczny. Przyjmijmy zatem od tej pory, że to Andrzej odpowiedzialny jest za rzeź niewiniątek.\]

W dawnych wiekach męskie imię Andrzej występowało zwykle w formie Jędrzej czy Ondrzej, albo pochodnych jak Ondraszek. Warto zwrócić uwagę na Jędrzeja, bo u źródeł tej formy znajdujemy typowo męską skłonność do przeraźliwie głośnego skarżenia się na prawdziwe czy rzekome krzywdy oraz cierpienia. Aby stłumić atawistyczne ciągoty Andrzejów, dobrze i wychowawczo jest przestrzegać ich przed wydzieraniem się i jęczeniem. Jako środki dydaktyczne sprawdziły się mokra ściera i sucha kopyść.