---
id: 253
title: 'Jatka polityczna'
date: '2014-11-14T15:24:09+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=253'
permalink: /2014/11/jatka-polityczna/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Artykuły naukowe'
tags:
    - 'kiełbasa wyborcza'
---

**JATKA POLITYCZNA**

(studium politologiczne)

Przed wyborami rozkwita rzeźnictwo polityczne, bowiem stara, czcigodna, demokratyczna tradycja każe, by kandydaci raczyli elektorat kiełbasą wyborczą. Doświadczenie minionych stuleci poucza, ze nakarmiony kiełbasą lud z ochotą bieży do urn i z czystej wdzięczności wieńczy najbardziej udatnych kiełbaśników koroną, której listki z mandatów są uczynione.  
Wielka to sztuka owo rzeźnictwo polityczne, a nawet cała gałąź wiedzy i umiejętności praktycznej. Trzeba przecież coś zarżnąć, by otrzymać kiełbasę, a jak się coś zarżnie, to tego nie ma, a jak czego nie ma, to tego szkoda, o ile nie było paskudne.  
Aby ominąć ową kwadraturę koła, wymyślono tzw. sposób na dżdżownicę. Dżdżownica to takie bydlę, któremu jak się obetnie, to mu odrośnie i śladu nie ma. Jednakże dżdżownic nie konsumują nawet Francuzi, więc z dżdżownicy kiełbasy nie będzie. Ale jest przecież budżet, który też corocznie odrasta na nowo, choćby się go nawet spożyło bez reszty. Sprytnie jest zatem i modnie oberżnąć budżetowi ochłap i z niego ukręcić kiełbasę wyborczą. Jednak w naszym pięknym kraju nawet sprawdzone i sprytne sposoby czasami zawodzą. Okazało się, że polski budżet da się zarżnąć i co gorsza, budżetu wszystkim szkoda, nawet tym, co przedtem o nim mówili, że paskudny.  
Tak, proszę państwa, sposób na dżdżownicę jest świetny, lecz zawodny. Lepszy jest sposób na kanibala. Trzeba pomóc rzeźnikowi politycznemu wycinać ochłap z budżetu i cierpliwie czekać, aż ktoś odkryje, że jest dziura i że jaka wielka. Każdy wie z samoobserwacji, że jak się dziurę odkryje, to się zrobi smród i w tym zaduchu nawet najświeższa kiełbasa zaśmierdnie. Oj, nie zanęci pechowy rzeźnik elektoratu zaśmierdłą kiełbasą i obszczeka go elektorat, że zawiódł i że jest paskudny. I nie żal będzie elektoratowi, kiedy pechowca zaszlachtuje sprytniejszy odeń rzeźnik polityczny i przyrządziwszy go zgrabnie, poda nie we flaku, lecz dowcipnie nabitego w butelkę.

Adam Boruta, Wasz specjalista gastronomiczny.