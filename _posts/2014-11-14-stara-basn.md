---
id: 260
title: 'Stara baśń'
date: '2014-11-14T15:47:35+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=260'
permalink: /2014/11/stara-basn/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Artykuły naukowe'
tags:
    - euroentuzjazm
    - eurorealizm
    - eurosceptycyzm
    - 'wchodzenie do Europy'
    - 'wspólnota europejska'
---

**STARA BAŚŃ**

(rozprawka historyczna)

Modne jest ostatnio porównywanie naszego obecnego wchodzenia do Europy z tym, które się dokonało w X wieku. Dokonują takich porównań niedokształceni publicyści wyraźnie motywowani politycznie. A jak to wygląda w świetle najnowszych badań naukowych?  
Kiedy Mieszko I ogłosił zamiar wstąpienia do europejskiej wspólnoty chrześcijańskiej, wielkie tym wywołał w kraju zamieszanie. Prawdę powiedziawszy, książę prezentował jeno wizję polityczną i przemawiał ogólnikami. Te ogólniki dostojnicy dworscy przekazywali wedle własnego rozumienia swoim podwładnym, a ci z kolei roznosili je po gnieźnieńskich i poznańskich gospodach, skąd wędrowały dalej do reszty Polan. A nad Wisłą i dolnym Bugiem, wśród mało pojętnych z natury Mazowszan, przekształcały się one w istne bajdy. Cóż dopiero mówić o dzikusach znad ruskiej czy pruskiej rubieży!  
Ci, którym się zdawało, że zrozumieli, o co chodzi z tą akcesją, byli częścią za, częścią zaś przeciw. Wedle sondaży Ibrahima ibn Jakuba wśród euroentuzjastów przodowali drużynnicy i to nie tylko dlatego, że książę tak im przykazał. Obiecywali oni sobie, że się przesiądą z tarpanów na rumaki, a wielu marzyło też o lukratywnych transferach do zachodnich drużyn. Również piastowska służba dyplomatyczna popierała gorącym sercem nowe, gdyż posłom wizytującym chrześcijańskie dwory przejadło się już siedzenie w tylnych ławach podczas uczt ceremonialnych i roboczych, gdzie udźce były przypalone a piwo kwaśne; denerwowały ich też niby to przypadkowe skrapiania wodą święconą i ironiczne zapytania o listy uwierzytelniające po łacinie. Wszyscy oni wołali na wiecach „Jeśli nie chrześcijańskie nacje, to kto – Jaćwierz i Prusy?” i drwiąco pukali w czoła eurosceptyków obuszkami lub też kozikami na tychże czołach rysowali kpiarskie kółka.  
Na przeciwnym biegunie znajdowali się liczni kapłani Swaroga i Dadźboga, Dziewanny i Marzanny oraz innych bóstw, a także rzecz jasna ich najbardziej gorliwi parafianie. Dla nich zamiary książęce były zatratą ojcowizny, wiary przodków, rodzimych zwyczajów i obyczajów. Na wiecach straszyli nadciągającą niewolą i tyranią papieży i cesarzy oraz napływem cudzoziemskich duchownych, którzy będą się panoszyć i żądać dla siebie dziesięciny. – Rzym nam prawa stanowić będzie. Już tam dziesięć przykazań na kamieniu w Gnieźnie ryją! – krzyczeli i szyderczo wywalali jęzory oderżnięte adwersażom w ferworze walki politycznej.  
Krążyły dziwne plotki na temat chrztu, wody święconej i kropidła. Lud domyślał się w tym wszystkim niezdrowej manii polewania, mycia i płukania, danin na łaźnie publiczne, przeziębień, kataru i reumatyzmu. Urzędnicy książęcy, gardłując głośno za akcesją, po cichu zastanawiali się, czy nie wróży to akcji „czyste ręce”. I ich, i większość podgrodzian nazwać by można eurorealistami. Na wiecach przychylali się do piastowskiej racji stanu, nawołując wszakże do darowania krasnalom i przymykania oczu na zwyczaj puszczania wianków, byle dyskretnie, dajmy na to na sianie w stodole. Jako zwolennicy umiaru chętnie też puszczali dla uspokojenia krew co bardziej zapalczywym ekstremistom.  
Było też kilku bywałych w świecie mędrców, którzy mieli szersze horyzonty. Ci głównie radzili nad tym, czy przyjęcie chrześcijaństwa nie zagrozi władztwu Piastów, bo zasada miłości bliźniego zdaje się sprzeciwiać hierarchii społecznej i obronności kraju. Eurosceptycy byli tego pewni. – Chrześcijanin – powiadali – miast za broń chwycić i wrogów ubijać, podejmie ich podpłomykiem i solą i nazwie „braćmi”. Miast ojcowizny strzec, sam na nią obcych sprowadzi, by ich nakarmić, odziać i dach zapewnić. Wyczekiwał będzie cały czas wieści, czy kto granic nie chce znosić i tego znosiciela poprze, aby wojnom zapobiec. Winy najeźdźcom każe darowywać tylko po to, by pokój czynić. Sławę wojenną będzie miał za nic, a czcił będzie przegranych i pokonanych za to, że nienawiścią nie zapłonęli i zabójstw się ustrzegli. Swoje przykazania ponad rozkazy księcia przedłoży i prawu się sprzeciwi, jeśli tylko uzna, że w niezgodzie są. Toć to istna piąta kolumna – wołali w wieszczym natchnieniu.  
Decydująca była postawa mieszkańców opoli. Jako typowi wieśniacy niechętni byli wszelkim zmianom. Nie cieszyło ich, że Europa da im w niedalekiej przyszłości, góra za jakie trzysta lat, trójpolówkę, pług, kosę, wiatrak i młyn wodny. Na mamidła wyrównania poziomów życia na wschodzie i zachodzie w ciągu co najwyżej pięciu stuleci reagowali wzruszeniem ramion. Narzekali na krótkie okresy przejściowe, choć zezwolenie na topienie marzanny wywalczył im książę aż do dziewiętnastego wieku włącznie. A jednak w większości nie sprzeciwili się akcesji. Ibrahim ibn Jakub pisze, że zwyciężyła w nich opolna pazerność, kiedy im obiecano co roku dwa różańce na dym.  
W końcu zrealizowano śmiałą wizję księcia. Kilka lat później, już po chrzcie, rozegrano bitwę pod Cedynią, której rocznicę obchodziliśmy w czerwcu. Mężowie stanu odetchnęli. Nie sprawdziły się obawy zwolenników pogaństwa, że chrześcijaństwo złamie bojowego ducha pancernych i tarczowników. Euroentuzjazm nie przeszkodził im w nadziewaniu na dzidy wojaków margrabiego Hodona i rozbieraniu ich na porcje toporami. Nie nadstawili im policzka, nie przywitali chlebem i solą, i nie wystrzelili z proc podpłomyków w odpowiedzi na salwy kamieni polnych. A opolnicy po staremu żłopali cienkie piwsko i zagryzali pieczoną rzepą. Idee ideami, a życie życiem.