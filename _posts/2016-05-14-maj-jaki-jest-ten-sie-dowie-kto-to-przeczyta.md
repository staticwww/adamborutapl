---
id: 595
title: 'Maj jaki jest, ten się dowie, kto to przeczyta'
date: '2016-05-14T17:31:55+02:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=595'
permalink: /2016/05/maj-jaki-jest-ten-sie-dowie-kto-to-przeczyta/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - demografia
    - 'Dzień bez Prezerwatywy'
    - 'Dzień bez Stanika'
    - 'egzamin dojrzałości'
    - flaga
    - forum
    - grill
    - koedukacja
    - konstytucja
    - maj
    - manifestacja
    - naród
    - pielesze
    - pochód
    - procesja
    - prokreacja
    - 'przyrost naturalny'
---

*W ostatnich miesiącach wizytowałem placówki polskiej służby zdrowia. Być może zdam w swoim czasie raport z tej wizytacji*

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2016/05/Maj-jaki-jest.pdf)

MAJ JAKI JEST, TEN SIĘ DOWIE, KTO TO PRZECZYTA

Maj to miesiąc aktywności, w którym naród otrząsa się ostatecznie z zimowego snu i wylega z pieleszy na rozmaite łona albo też na fora niezadaszone i zadaszone. Czyż nie jest nieprzeniknioną tajemnicą powód, który skłania rozmaite służby do twórczego księgowania przepływów narodu przez fora niezadaszone?

Maj to miesiąc zorganizowanej ruchliwości pieszej tłumnej, w którym naród wylega poza pielesze na pochody i procesje. Czyż nie jest oczywiste, że jeśli żądający demokracji manifestanci przeżyli pochód w dobrej kondycji, to automatycznie są dowodem na dobrą kondycję demokracji, tak jak uczestnicy procesji modlący się o łaskę niebios są dowodem na zstąpienie na nich łaski, jeśli tylko po procesji zstąpili do domu, a nie do piekła?

Maj to miesiąc flagi, w którym naród, wylegając z pieleszy, chętnie macha i powiewa. Zdarza się, że oszczędzając flagę, z dystansem macha ręką na takich lub owakich albo daje do zrozumienia, że to lub owo mu powiewa. Innym razem z zaangażowaniem przeciąga przez fora niezadaszone powiewając proporcami w imię tego czy owego i bywa, że zamachnie się drzewcem na takiego czy owakiego. Czyż mylą się optymiści sądząc, że powiewanie i machanie jest za takim czy owakim albo za tym czy owym? Czyż błądzą pesymiści mniemając, że machanie i powiewanie jest przeciw takim lub owakim lub przeciw temu czy owemu? Czyż nie mają racji beznamiętni obserwatorzy tacy jak my, kiedy wyrażają przekonanie, że nie da się tak machać i powiewać, by to nie było zarówno za kimś lub za czymś jak i przeciw innemu komuś lub czemuś innemu?

Maj to miesiąc konstytucji, w którym naród przypomina sobie, że istnieje coś takiego jak pielesze wszystkich pieleszy wymagające, by je urządzić ustawą zasadniczą. Czyż nie jest to typowe dla naszego narodu, że wobec konstytucji przyjmuje skrajne postawy? Gdy jedni trzęsą się nad każdym jej dotknięciem, lękając się, że delikatna jest i zwiędnie, to drudzy bez obaw łamią ją na kolanie zapewniając, że niezniszczalna być musi, bo co by z nią nie wyrabiać, trwa na papierze niezmieniona.

Maj to miesiąc wzmożenia patriotycznego, w którym naród porzuca ciasny partykularyzm pieleszy i zwraca się ku dobru wspólnemu, ofiarnie podejmując trud poprawiania wskaźników demograficznych. Czyż nie jest przykre, że ustawa 500+ piękno tego bezinteresownego zrywu chce zastąpić kiczem przyziemnego dorobkiewiczostwa?

Maj to miesiąc postępowego ruchu po spirali, w którym naród wyrywa się z pieleszy, by do pieleszy –niekoniecznie swoich – powrócić na wyższym, patriotycznym, prokreacyjnym poziomie, często realizując przy okazji cywilizacyjnie zaawansowaną ideę koedukacji. Czyż nie dziwi, że dominująca ideologia w kwestii prokreacji nie jest konsekwentna, bo obrzydzając spiralę, jednocześnie restrykcyjnymi warunkami krępuje koedukację w pieleszach?

Maj to miesiąc z przerywnikiem, w którym pewnego dnia Zośka lub Pankracy (albo Bonifacy) stają się zimni i dają do zrozumienia narodowi, że pora, by opuścił ich pielesze i wrócił na swoje, bo nawet najszczytniejsze porywy patriotyczne nie mogą trwać bez ustanku i przerwa być musi; na szczęście ta przerwa nie trwa długo, zapał odrasta i czyn społeczny na rzecz przyrostu naturalnego zostaje wznowiony. Czyż nie jest symptomatyczne, że naród do trudu prokreacyjnego ma stosunek przerywany?

Maj to miesiąc współpracy ponad podziałami światopoglądowymi, w którym wierzący i niewierzący – czy to w pieleszach czy poza nimi – jednoczą się duchowo w promocji przyrostu naturalnego celebrując Dzień bez Prezerwatywy i Dzień bez Stanika. Czyż nie jest tak, że Dzień Matki współgra w subtelny sposób z tą inicjatywą?

Maj to miesiąc ognia, w którym naród, wyległszy z pieleszy, masowo rozpala ogniska i grille. Czyż nie warto pamiętać o ciekawostce, że pieczeniarze to nie ci, co pocą się przy rusztach, lecz ci, co doskakują do grillów z boku?

Maj to miesiąc Anieli, bo zaczyna się i kończy jej imieninami, toteż naród w pieleszach Anieli wita się z majem i z majem się rozstaje. Czyż to prawda, że wśród rozbudzonych politycznie rodziców rodzi się moda na to imię, bo wróży dziewczynkom karierę w Sejmie?

Maj to miesiąc maturalny, w którym naród musi wyleźć z pieleszy, by zaliczyć egzamin dojrzałości. Czyż mu się uda?