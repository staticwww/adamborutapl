---
id: 250
title: 'Zaburzenia atmosferyczne'
date: '2014-11-14T15:16:06+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=250'
permalink: /2014/11/zaburzenia-atmosferyczne/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony z lat 90-tych'
tags:
    - 'atmosfera polityczna'
---

**ZABURZENIA ATMOSFERYCZNE**  
(sierpień 2001)

W wieku atomu i inżynierii genetycznej chcemy, aby nam rozjaśniano, tłumaczono i uprzystępniano. Od tego mamy ekspertów, besserwisserów i mass media.  
Ostatnio wiele uwagi przyciągają zaburzenia atmosferyczne, które nam chętnie uprzystępnia obiektywna rzeczywistość, jednakowoż czyni to niezbyt profesjonalnie, bo bez należytego wytłumaczenia i rozjaśnienia. Od tego jednak my jesteśmy, by takim niedopatrzeniom zaradzić. Otóż, proszę państwa, zaburzenia atmosferyczne polegają na braku umiarkowania w grzaniu, wianiu i laniu wody. Z mrozem i upałem, bezwietrznym zaduchem i tornadami, suszą i powodziami sprawa jest prosta: winne są gazy cieplarniane, którym patronują górnicy czepiający się kopalni, prezydent Bush czepiający się Kioto oraz wszyscy ci, którzy nie czepiają się tych, co puszczają bąki (niezorientowanym chętnie wyjaśniamy, że materię bączną tworzy metan współodpowiedzialny za efekt cieplarniany).  
Zaburzenia atmosfery politycznej polegają w istocie na tym samym, tyle że ich przyczyn ortodoksi szukają nie w emisji gazów cieplarnianych, lecz w nadchodzących wyborach. Ale że nie sami konformiści zaludniają świat boży, bez trudu znaleźlibyśmy zwolenników teorii cieplarnianej, którzy zgodziliby się z poglądem, że zaburzenia polityczne, jakich jesteśmy świadkami, biorą się z zaczepiania górników, czepiania się poły prezydenta Busha oraz z parlamentarnego zbijania bąków po uprzednim ich poczęciu.  
Ale jakież to zaburzenia mamy na myśli? Każdy odbiornik telewizyjny powie nam, że atmosferę polityczną kształtują dzisiaj wichry dziejowe, które wywiewają z koalicji partie, z rządu ministrów, ze skarbu pieniądze, a z pieleszy czarne charaktery. Znani nam już ortodoksi na pytanie, dlaczego dują wichry, odpowiadają z niezachwianą pewnością, że to bierze się stąd, iż zbliżają się wybory i atmosfera dla klasy politycznej się zagęszcza.  
Psychologia od dawna interesuje się wpływem zjawisk atmosferycznych na psychikę ludzką. Z jej badań wynika, że im atmosfera gęstsza, tym radości mniej. Jeśli by zatem zagęszczanie miało związek z wyborami, to wynikałby stąd paradoksalny wniosek, że nadchodzące wybory nie napawają całej klasy politycznej radością. Jest to niemożliwe w kraju demokratycznym, bo w takowym politykę uprawiają demokraci, dla których wybory są głosem ludu, a lud demokraci miłują nade wszystko, a jeśli lud, to i głos jego miłują. Mało jest przecież takich anomalii seksualnych jak pan Zdzicho, który kocha małżonkę, gdy owa trafia doń przez żołądek, lecz gdy ta sama niewiasta usiłuje trafić doń przez uszy, to jego trafia.  
Jest zatem zupełnie jasne, że wybory nie mogą smucić polityków, a zatem wzmiankowane wyżej wichry dziejowe nie mogą być wynikiem zagęszczenia atmosfery, lecz muszą być efektem całkiem innych przypadłości. Znajomy fizyk jest zdania, że wichry polityczne wzmagają się przed wyborami po prostu dlatego, że z politycznego punktu odniesienia czas przedwyborczy płynie szybciej i to, co kiedy indziej było łagodnym zefirkiem, w okresie kampanii nagle przyspiesza i staje się huraganem. Nie wydaje nam się to trafne, bo wyczytaliśmy w podręczniku, że czas płynie wolniej tam, gdzie masa, a zatem gdyby dla polityków czas płynął szybciej, oznaczałoby to, że oderwali się od mas. Jest to niemożliwe w kraju demokratycznym, bo w takowym politykę uprawiają demokraci, którzy nade wszystko miłują masy, a przecież jeśli się kogo miłuje, to się do niego garnie. Mało jest przecież takich anomalii seksualnych jak pan Zdzicho, który miłuje małżonkę, lecz garnie się do knajpy, bo tam mu czas płynie szybciej.  
Logika przekonuje nas zatem, że obecne zaburzenia polityczne nic nie mają wspólnego z wyborami. Kolejne partie rozstają się z koalicją, a ministrowie z rządem, bo wiedzą, że tak czy owak parlament będzie uchwalał, a rząd – rządził. Chodzi o to, by Goliata lewicy obskoczyło zewsząd mrowie prawicowych Dawidków z procami w ręku. I wcale nie są winne wybory, lecz życiowy trend. Biologowie mówią, że mamy tu do czynienia z klasyczną radiacją adaptacyjną: rodzą się nowe gatunki polityczne i zajmują kolejne nisze. Niedługo każde środowisko będzie miało swoją partię (prawicową bądź centrową), której działacze szczególnie sobie w nim upodobali. Rozrośnie się społeczeństwo obywatelskie i ogarnie jednostki dotąd apolityczne. Minister Biernacki przyobiecał zasilić kadrowo nawet szczególnie niechętną dotąd polityce zbiorowość bywalców zakładów karnych. Podobno wspomagają go w tym dziele zresocjalizowani gangsterzy, wskazując co prężniejsze osobistości z pierwszych stron gazet, zdolne tchnąć politycznego ducha w społeczność rodaków siedzących. Seria aresztowań przestępców, która poruszyła opinię publiczną, da się wytłumaczyć zapotrzebowaniem na tego rodzaju konsultantów.