---
id: 62
title: 'Kronika Chichrały'
date: '2014-09-20T16:19:45+02:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=62'
permalink: /2014/09/kronika-chichraly/
interface_sidebarlayout:
    - default
categories:
    - 'Literatura daj Boże piękna'
    - Powieść
tags:
    - chrześcijaństwo
    - 'fantasy polityczne'
    - 'intrygi dworskie'
    - pogaństwo
    - 'political fiction'
    - 'słowiański książę'
    - 'średniowieczny dwór'
    - 'Warczysław kneź'
    - 'wchodzenie do Europy'
---

[![Przypadki magistra N.](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/Kronika-Chichrały.pdf)

ZAMIAST WSTĘPU

*Zeszyty Naukowe Uniwersytetu III Wieku, Seria: Filozofia, R4,2012*

*dr Sławomir Pućko, Przyczynek do badań nad epistemologicznymi podstawami starożytnictwa \[Fragmenty\]*

\[…\]

Są myśliciele pewni tego, iż zjawiska badać należy w ich najprostszej postaci, kiedy zaciemniające obraz uboczne czynniki słabo oddziałują, albo wręcz nikną, przez co prawidłowości, o których zbadanie chodzi, łatwiejsze są do uchwycenia.

Prawie wszyscy miłośnicy nauk sądzą z dużą dozą pewności, iż im badacz wol­niejszy jest od pozanaukowych nastawień do przedmiotu badania, tym zwykle lepszy wynik osiąga. Dystans i wyciszenie emocji zalecane są badaczowi.

Zarówno wybitni filozofowie jak i przeciętnie wykształceni laicy zgadzają się w większości, iż współczesne społeczeństwa przedstawiają sobą ogromnie skompliko­wany splot przeróżnych zbiorowości, instytucji i tendencji. Obserwacja, nagromadzona wiedza i intuicja sugerują, że im dalej w przeszłość, tym prostsze musiało być życie społeczne.

\[…\]

Wiele trudności odczuwa człowiek współczesny, gdy usiłuje zgłębić politykę, z jaką się styka. Politycy rywalizują, przez co informując, argumentując i snując wizje obyć się nie mogą bez zarzutów nierzetelności i interesowności stawianych adwersa­rzom. Wiele jest zatem politycznych wizji społeczeństwa i wszystkie podważane. Wy­gląda na to, iż w praktyce nie da się uprawiać i rozważać polityki bez ocen, zaś oceny zdają się zawsze angażować zarówno rozum jak i uczucia. Jak ma człowiek współcze­sny odsiać racjonalne od emocjonalnego? Chociażby stanął z boku i usilnie starał się o bezstronność, nie może przecież całkiem odciąć się od swoich dążeń i ideałów, które przecież – w zasadzie nieuchronnie – znajdują się w nieobojętnych relacjach do każdego z programów politycznych. Niemożność nabrania dystansu i zdławienia emocji bardzo człowiekowi współczesnemu przeszkadza w analizie polityki.

A brak orientacji w otaczającej rzeczywistości przyprawia go o dyskomfort.

\[…\]

Czy w tych okolicznościach dziwić może rosnące zainteresowanie starożytno­ścią i średniowieczem? Spodziewają się liczni współcześni, że pradawni, żyjąc prościej, nie zdołali zagmatwać istoty swego bycia społecznością i istota owa przezierać musi spoza nieskomplikowanych ich uczynków i naiwnych wypowiedzi. Spodziewają się też, że skupienie się na pradziejach oderwie ich od teraźniejszych emocji, jakie nimi targają i od popędów, jakie im zaszczepiła cywilizacja. Myślą, że będą w stanie przyjrzeć się pradawnym chłodno, bo mało co ich z nimi wiąże.

Wierzą, że jeśli w ogóle potrafią zrozumieć ludzkość, to najłatwiej tę zaprzeszłą. Żywią się przy tym nadzieją, że jeśli natura ludzka wspólna jest wszystkim pokoleniom, to zrozumienie pradawnych stanie się dla nich punktem wyjścia do pojmowania teraź­niejszych. Jeśli nawet nie da im pełnego obrazu współczesności, to przynajmniej od­kryje przed nimi ważne jego fragmenty albo chociaż wytyczy kierunki poszukiwań.

Rachuby swoje potrafią poprzeć opiniami autorytetów. Chętnie cytują znanego historyka idei Zdzisława Nowaka: „Ludy wkraczające na arenę historii jawią się jako prostoduszne. Reagują spontanicznie, nie wdając się w skomplikowane analizy. Rzeczy zabawne kwitują śmiechem, a smutne – płaczem. Mile widzianych witają dobrym sło­wem, nielubianych obrzucają obelgami. Swoje serca ofiarują przyjaciołom, serca wrogów – niedożywionym zwierzętom domowym. Nie widać u nich hipokryzji, obca jest im idea tak zwanej politycznej poprawności”.

Jak widać, Zdzisław Nowak wiąże przejrzystość więzi społecznych pośród lu­dów zaprzeszłych z prostotą ich motywów i spontanicznością ekspresji. Uczynki pra­dawnych są łatwe do zinterpretowania, bo powodowane naturalnymi popędami, zaś komunikaty oczywiste, bo naiwnie szczere.

Łatwość interpretowania czynów i słów pradawnych oraz zasadność przenosze­nia wniosków na społeczności późniejsze znajduje swoje wytłumaczenie między innymi w fundamentalnym założeniu, iż ludzkość od zarania po dziś ma niezmienne w podsta­wowych zarysach wyczucie dobra i zła. Analogiczne założenie dotyczy zresztą wyczu­cia piękna i brzydoty.

*Magazyn „Starożytnik”. Miesięcznik Polskiego Towarzystwa Miłośników Słowiańszczyzny, R.271, 2, 2012.*

*Sławomira Pućko, Nie tylko aspekt poznawczy, Doktorze Pućko! \[Fragment\]*

\[…\]

Nie sposób zapominać, że percepcja czynów i wypowiedzi przodków może być nie tylko płodna poznawczo, lecz i ożywcza aksjologicznie. Wyjaśnia to, że obok miło­śników dziejów zaprzeszłych napędzanych żądzą wiedzy napotykamy na rzesze amato­rów pragnących się dziejami owymi zbudować albo też zachwycić. Ceniony historyk idei, Zdzisław Nowak, tak oto pisze: „Ludy wkraczające na arenę historii jawią się jako zdecydowane w osądach i zdeterminowane w dążeniu do zaprowadzania sprawiedli­wego porządku. Tak zwany barbarzyńca odważnie rusza z białą bronią na agresora, który motyką zadrasnął jego terytorium. Kiedy zatyka jego głowę na tyczce, dba o to, by powstała kompozycja o środkach wyrazu na tyle przejmujących, aby przestrzec in­nych przed występkiem. Gdy słońce zajdzie, zasypia spokojnie snem sprawiedliwego, bo ma poczucie, że przeżył dzień pięknie”. A cóż czyni dzisiaj niewolnik tak zwanej poprawności politycznej, kiedy współczesny barbarzyńca zadraśnie mu samochód? Wynajmuje adwokata, a sąsiadów przestrzega przed nietypowymi przyzwyczajeniami seksualnymi barbarzyńcy. Nie mogąc zasnąć, chwyta za powieść, by podziwiać odważ­nych przodków, którzy czynili dobro, zatykając głowy zbrodniarzy na drzewca. Od­waga i bezkompromisowość przodków są w jego oczach nie tylko słuszne, ale i piękne.

*Zeszyty Koła Naukowego Studentów AWF w Poznaniu „Górą Lech Kolejorz”, Nr 222, 2012*

*Sławek Pućko, Czego naprawdę uczą nas przodkowie nasi \[Fragmenty\]*

Przyjrzyjmy się wnikliwie fragmentowi wiekopomnego dzieła Zdzisława No­waka, historyka idei: „Ludy wkraczające na arenę historii jawią się jako przepełnione żądzą czynu, tych sobie najwyżej cenią, co odwagą i mieczem wyrąbują dla siebie kró­lestwa. Wielką dla nich chwałą jest zwyciężać mocarzy, odbierać smokom skarby, kłaść pokotem olbrzymów. Gotowe są przepływać morza, przekraczać góry i przebijać przez najdziksze gęstwiny”.

Te słowa genialnego uczonego zwracają uwagę na motyw, który najskuteczniej dziś pomnaża szeregi wielbicieli dziejów zaprzeszłych. Są oto ludzie, w których tkwi pęd do działania, żądza czynu, chęć przeżycia przygody. Na masową skalę ludzie ci mogą się dziś realizować tylko w sferze sportu. Nie mogą tego czynić jako zawodnicy, bo na przeszkodzie staje szczupłość kadry; ich niepokorny duch nie zniósłby zresztą niewolniczego reżimu treningowego i despotyzmu szkoleniowców. Zostają kibicami. Kult wyczynu powoduje jednak, że nie wystarcza im bierne przyglądanie się sportowej rywalizacji, lecz chcą czynnie uczestniczyć w zmaganiach.

\[…\]

Słyszą, jak niektórzy politycy nakłaniają ich do tego, by brali sprawy w swoje ręce, by osobistą aktywnością zmieniali otoczenie społeczne. Jednak ci sami politycy gęstą siecią ustaw pętają ich spontaniczną działalność.

\[…\]

Ludzie czynu, którzy – dajmy na to – powzięli zamysł, by otoczenie społeczne kibiców przeciwnej drużyny zmienić na szpitalne albo też cmentarne, zderzają się z niekonse­kwencją klasy politycznej ucieleśniającą się pod postacią sił policyjnych. Zrażeni do politycznych realiów uciekają w rzeczywistość zamierzchłą. W odróżnieniu od pasjonatów, których motywy analizują wybitny filozof Sławomir Pućko i zasłużona popularyzatorka nauki Sławo­mira Pućko, nie szukają tam koniecznie prawdy dziejowej, która ich oświeci, zbuduje oraz zachwyci. Chcą znaleźć się w idealnej krainie wojowników przepojonej duchem rywalizacji, pełnej niebezpieczeństw i przeszkód, które mogliby z satysfakcją przezwy­ciężać, a zwłaszcza pełnej wrogów, nad którymi mogliby zatriumfować. Kraina ta w ich oczach jawi się jako piękna i dobra, obojętne jest im natomiast, czy jest prawdziwa. Można zresztą przypuszczać, że na spełnienie ich wygórowanych kryteriów więtszą szansę mają rzeczywistości fantastyczne niż te zrekonstruowane przez historyków.

\*

Drodzy wydawcy! Kochani czytelnicy! Poszukiwacze prawdy o bycie społecz­nym, wyznawcy prostoty moralnej, miłośnicy naturalnego piękna oraz ludzie nieskrę­powanego czynu domagają się wszystkiego, co wiąże się ze starożytnością i średnio­wieczem. Nie może dziwić zatem popularność literatury przedstawiającej owe czasy – czy to rzeczywiste, czy to fantastyczne. Ten stan rzeczy daje pewność powodzenia na­szego przedsięwzięcia, które polega na udostępnieniu rzeszom czytelników wyjątków z tzw. „Kroniki Chichrały”. Nierozsądne byłoby pogrzebać sukces w zarodku, wyma­wiając się wyimaginowanym ryzykiem biznesowym czy nieuzasadnioną oszczędnością.

ZAMIAST PRZEDMOWY

„Kronika Chichrały” zgodnie ze średniowiecznym obyczajem nie została opa­trzona imieniem autora. Przecież nie swoją chwałę miał dziejopis głosić, lecz chwałę władców czy świętych. Slavomil Pučka, historyk amator i kolekcjoner, który wydobył omszały rękopis spod beczki na wino w byłym klasztorze benedyktynów w Malym Platku, przypisał autorstwo trefnisiowi Chichrale, a tym samym prawem odkrywcy na­dał dziełu nazwę, którą dotąd nosi.

Hipoteza Slavomila Pučki nadal jest popularna w gronie mediewistów i slawi­stów, lecz przecież nie jedyna. Konkuruje z nią pogląd Sława Puczkina wskazujący brata Michała jako rzeczywistego twórcę „Kroniki Chichrały”, zaś Ruhmfried Putschke podał niebagatelne racje za autorstwem setnika Brunona; owe racje Putschkego zda­niem Sławoja Pućki błyskotliwe są i ważkie, jednakowoż przemawiają raczej za księ­ciem Warczysławem niż za Brunem. Dwaj ostatni dyskutanci, zdając sobie sprawę ze wczesnośredniowiecznej niechęci ludzi czynu do parania się piórem, zastrzegają, że ich kandydaci raczej inspirowali niż osobiście kaligrafowali litery; nie wykluczają przy tym, że korzystali z pisarskich umiejętności czy to Chichrały czy brata Michała, co ich zdaniem ewidentnie obraca na ich korzyść niektóre kontrargumenty pracowicie wycy­zelowane przez Pučkę i Puczkina. Krewki Sław Puczkin nazwał ten sposób korzystania z dorobku adwersarzy raubritterstwem intelektualnym, jednak Putschke i Pućko nie ze­szli ni na jotę ze swoich pozycji. Atmosfera dyskusji była i jest nader gorąca. Znany historyk idei Zdzisław Nowak, którego geniusz manifestował się nie tylko w działalno­ści naukowej, lecz i w anegdociarstwie, pisze w „Pamiętniku” o plotkach mediewi­stycznych wiążących nienaturalną łysinę Puczkina, braki w uzębieniu Pućki oraz ubytki w podniebieniu Putschkego z ich starciem na sympozjum w Poczdamie.

„Kronika Chichrały” nie została napisana ani po łacinie, ani po grecku, ani też w języku starocerkiewnosłowiańskim, czego można byłoby się spodziewać, znając ówcze­sne obyczaje. Nie przynależy też językowo ani do kultury arabskiej, ani do skandynaw­skiej. Nie znajdujemy w niej wtrętów ani awarskich, ani bułgarskich. Brak też madzia­ryzmów, bałtyzmów i germanizmów w ilości pozwalającej na jakieś dalej idące speku­lacje. Tekst jest niewątpliwie słowiański, lecz trudno go powiązać z jakimś konkretnym językiem, dialektem czy narzeczem ze względu na szczupłość, a właściwie brak mate­riału porównawczego z tej samej epoki. Aby zilustrować trudności, jakie stoją przed uczonymi w tego rodzaju badaniach, warto może wspomnieć, iż Zakład Lingwistyki Stosowanej z pomocą innych pracowników Uniwersytetu Zielonogórskiego opracował nowatorski program komputerowy, który miał dokonywać porównawczej analizy styli­styki tekstów niezależnie od języków, w jakich zostały zapisane – analiza miała być efektywna w zakresie większości języków indoeuropejskich (z wyłączeniem celtyckich i albańskiego). Objęcie badaniem „Kroniki Chichrały” przyniosło absurdalny wniosek, że pod względem stylistycznym dzieło to spokrewnione jest najbliżej z dorobkiem pi­sarskim znanego historyka idei Zdzisława Nowaka oraz wielojęzykowej familii Pučków – Puczkinów – Pućków – Putschków; ten wynik wstrząsnął reputacją skądinąd zasłużo­nej placówki naukowej i skłonił pozostałe ośrodki do ostrożności w podejmowaniu tego tematu.

Krój liter „Kroniki Chichrały” jest nietypowy, jednak nie zyskały większego uznania sugestie Famefreda Putsona, że wykazuje on pewne podobieństwo do znaków runicznych, co mogłoby kierować uwagę badaczy na setnika Sigurda. Dalsze badania są w toku od lat, szerzej akceptowanych rezultatów jak dotąd nie ma. Istotne znaczenie dla rozwoju paleografii chichralnej miał zapewne incydent, jaki wydarzył się podczas kon­ferencji w Karlovych Varach. Znany historyk idei, Zdzisław Nowak, zaprezentował tam jako ciekawostkę maszynę do pisania, którą w latach dziecięcych wystrugał był kozi­kiem z drewna lipowego. Kilku uczonych zwróciło uwagę na łudzące podobieństwo kroju czcionek tej maszyny do znaków, jakimi zapisana została „Kronika Chichrały”. Do dziś na tym przykładzie pokazuje się początkującym mediewistom, jak łatwo przy­padkowe podobieństwo może sprowadzić paleografa na manowce.

ZAMIAST PROLOGU

Nie wiemy, gdzie znajdowały się włości księcia Warczysława, którego panowa­nie upamiętnia „Kronika Chichrały”. Dziejopis umiejscawia je pomiędzy „bałwany morskiemi a góry niebosiężnemi”. Mogło tu chodzić o tereny, nad którymi Warczysław panował, ale mogło też chodzić o ziemie, do których rościł sobie pretensje. Cytowany fragment wyblakł, a pergamin w tym miejscu został rozcięty. Niektórzy czytają „bał­wany modremi a góry sążnistemi”. W gronie uczonych panuje zgoda, że należy loko­wać państwo Warczysławowe pomiędzy zbiornikiem wodnym a wzniesieniem o wyso­kości względnej co najmniej jednego sążnia (ok. 2 m). Geografowie historyczni wspo­magani przez zdjęcia satelitarne wskazali trzydzieści pięć tysięcy dwieście dwadzieścia trzy możliwe lokalizacje. Slavomil Pučka był pewien, że Warczysławia rozciągała się pomiędzy wschodnimi Alpami i Rodopami a Bałtykiem z ośrodkiem w Malym Platku. Wyśmiał to Sławoj Pućko jako przejaw nacjonalistycznej tęsknoty za Wielkimi Cze­chami i zaproponował terytorium pomiędzy Bałtykiem a Sudetami i północnymi Kar­patami. Slavko Pucić argumentował za obszarem od Gór Dynarskich do Balatonu, zaś Slavica Pucewa umiejscawiała Warczyslawię między Bałkanem a Morzem Egejskim. Pojawiły się też skromniejsze interpretacje. Znany historyk idei, Zdzisław Nowak, wspomniał niegdyś o Warczysławii jako o krainie polskiej graniczącej z jednej strony z Moczydłem a z drugiej z Psią Górką w gminie Doruchów w powiecie ostrzeszowskim; nie było to podczas obrad, lecz na raucie dla uczestników Kongresu Slawistycznego w Sofii, gdzie uczeni regenerowali siły po pasjonującej dyskusji i nie bardzo wiadomo, czy znakomity erudyta rzeczywiście nawiązywał w swej konstatacji do „Kroniki Chi­chrały”. Tym niemniej myśl podchwycono, co zaowocowało dwunastoma pracami ma­gisterskimi i dwoma doktoratami wysokich lotów, nie licząc szeregu prac pośledniej­szych.

\*

Zasygnalizowaliśmy oto niektóre z ważnych elementów, które konstytuują my­ślowy i praktycznospołeczny kontekst, w jakim należałoby osadzić treść „Kroniki Chi­chrały”, jeśli by się chciało przestudiować ją z należytą starannością i dogłębnym zro­zumieniem. Życzymy Czytelnikom takiego studiowania. Oddajemy głos dziejopisowi.

*Za Komitet Redakcyjny*

*prof. dr hab. Zdzisław Nowak (kierownik naukowy)*

*dr Sławomira Pućko (sekretarz redakcji)*

*dr Sławomir Pućko (członek)*

*Sławka Pućko (praktykantka)*

**KRONIKA**

**CHICHRAŁY**

=========================================================

**Księga pierwsza**

Warczysław, kneź, zacną krainą władał, co pięknie się pomiędzy bałwany mor­skiemi a góry niebosiężnemi rozpościerała. Pełna była ona puszcz nadobnych, rzek ryb­nych a też pól pracowicie uprawianych. Grody w niej były srogie i dla napastników nie do przemożenia, a przy nich osady rzemieślników i kupców pełne. Kneź barzo po chrześcijańsku rządy nad nią sprawował, a też w bitwie był dzielny i w wojnie szczę­śliwy, przez co ją na świat cały rozsławiał. Dla onej sławy słodkiej drużyną liczną wo­jów dzielnych ją obdarował, by ich karmiła, odziewała a zbroiła, ile ino wydoli. A że do niedawna jeszcze w mrokach pogaństwa tkwiła, barzo tego pilnował, by w niej za­szczepiać obyczaje chwalebne, za­równo w ościennych mocarstwach chrześcijańskich pospolicie od lat się objawiające, jak też nowo we dworach szacownych umyślone. Stąd urząd dworski był ustanowił przedtem w kraju swojskim niebywały, a to trefnisia, ina­czej błazna, z najdostojniej­szymi pany w pierwszeństwie zrównanego. Na oną godność nową Chichrałę wyniósł, człeka rodu nieznanego, choć słowa znającego, wszelako nie­swojaka, jawnie z ościen­nych jakowychś krain się wywodzącego.

Rzeczony Chichrała, trefniś, barzo był w świecie bywały, wszelkie dwory ościenne obszedłszy a grody znaczne; nawet w najdalsze krainy był pono wstąpił, co prawdzie podobnym się zdawało, bo kiedy trunki pijał, prawił wonczas razy wiele o takowych na południu miejscowościach, kędy pancerze wojom na ciele z gorącości miękły, a też o takich za północnym morzem, kędy znów zbroje ode mrozu kruszały na proszek i na padół z takiej przyczyny opadały, mężom wojennym stopy przysypując znienacka. Inne też dziwy powiadał, a też przypowieści rozmaite, a tego Warczysław, kneź, chętnie barzo słuchał.

Nie dziwota przeto, co nastąpiło, kiedy przesławny Warczysław na wał wylazł i na błonie, pola a puszcze za nimi pozie­rał, z zieloności się ciesząc, a z żółci zbożowej obfitej barzej jeszcze. Gorącością upalną utrapiony, kołpaka książęcego na rozumie nie trzymał, jak to przystoi wedle ceremoniału, jeno nim machał, wiatr rzeźwiący tym spo­sobem czyniąc, ale przecie na ustroniu był, od spojrzeń gawiedzi z dala, ino w trefnisia poufałego przytomności. Słonko barzo gorliwie majestatowi przyświecało, przez co czupryna pomazańca wielce się mimo machania nagrzała, no i też myśl się wykluła pode nią niespodziana.

– Czy godzi się w ten ciepły czas po izbach kisnąć, miast w puszczę cienistą, miłą ru­szyć a członki zastałe rozciągnąć? – ozwał się natenczas monarcha, sam niby siebie pytając.

– Izby barzo cieniste mamy, a w nich trunki i jadło, co bez nas skisnąć jak amen w pacierzu może – statecznie Chichrała mruknął. – Zasię członki same się do dziewek dwor­skich wyciągają, a tych po puszczach nie uświadczysz.

– Wspomniałem właśnie na przypowieść twoją, mój Chichrało – ciągnął kneź swoje, nijak na ględzenie błazna nie bacząc – co to ją żeś wczoraj przy wieczerzy wy­głosił. Czyś nie bajał czasem o władcy zamorskim, co za prostaka był się przebrał i po dziedzinie swojej samotrzeć wędrował, żeby ludu swojego troski rozeznać?

– Dobrześ to nazwał, sławny Warczysławie – trefniś westchnął przeczuciem do­tknięty nieprzyjemnym – takie to ino bajanie było. A co się ludu twojego tyczy, to ów za sprawą rzą­dów światłych nijakich trosk nie ma i w szczęśliwości błogiej żyje. Ale jeśliś uparty, jasny kneziu, tedy odziej się zgrzebnie, kaptur zaciągnij na gębę, a po dworcu przejdź i uszów nadstaw, to może troski miejscowe rozeznasz.

Ale kneź przesławny – abo książę, jak niektórzy gadają – zatem rzeczony tak czy owak Warczysław już ze szczytu umocnienia na gród ryczał, by migiem radę książęcą składać, bo pilne postanowienia ma.

\*

Rajcy kneziowi marudnie do izby paradnej ściągali, sarkając pode wą­siskami na służby dla tronu nadmiar niemożliwy. Dzień ów przecież do wypocznienia być miał przeznaczony, bo do wczora przeze nie­dzieli pięć rokowania ciężkie z poselstwem Iza­sława, księcia wschodniego, bez wy­tchnienia nijakiego prowadzono. Toteż wielmożów kilku w dyplomacyji najciężej pra­cujących pachoły na ręcach wnieśli, a za nimi stągwie mleka ukisłego, którem na prze­mian wątroby dostojne smarowano z wierzchu, zasię gar­dziele a kiszki ode środka.

Wtenczas to kneź Warczysław przemówił ku zadziwieniu Chichrały, trefnisia, o utrapieniu nieznośnym jakowe zbójowie liczni po gościńcach czynią i o tym, że pora ostatnia na to, by onych zbrodniów występki ukrócić. Na koniec wyrzekł z żalem:

– Wielem już razów to, co tu wam gadam, we łby kasztelanów kładł, a widzę, że nic oni nie czynią przeciw złości zbrodniów przeklętych, bo zbójowie butnie po księ­stwie się panoszą i ze władzy mojej śmieją.

– Jakże to? Nie może być! – zakrzyknęli wielmoże akuratnie – Najgorszy zbro­dzień gęby by na majestat rozewrzeć się nie ośmielił!

– Wyście rozwarli – wytknął im pomazaniec boży, Warczysław, na co cisza zapa­dła.

– A zbójowie mimo kasztelanów w pachołów mrowie uposażonych wałęsają się, kędy ino chcą, pono w grody warowne nawet wstępując bez obaw – podjął majestat marudnie – kto wie, czy z rzeczonymi kasztelany miodów a piw książęcych nie wysu­szają w dzień biały, dziedzinę moją miłą rujnując pomału.

– Ploty jakieś to niedorzeczne przeze nieprzyjacioły nasze rozpuszczane! – wy­rwał się Przecław, podkomorzy i cisza głębsza jeszcze zapadła.

– Może i nie do rzeczy prawię, co mi wybaczcie, rajcowie moi – wyrzekł książę Warczysław głosem chłodnym, czoło marszcząc.

Izba aże zatrzęsła się od protestacji, o umiłowaniu majestatu świadcząc powszech­nym, ale też o łagodnym a miłosiernym knezia Warczysława rządzie, bo po całym chrześcijaństwie ziemi jednej trudno znaleźć, by w niej hurmem takowym słowu monarszemu można było oponować.

– Nigdy na to zgoda moja nie dawam – zahuczał na ten przykład basem setnik Sigurd, dźwięki barzej niż zwykle ze wzburzenia wykręcając – żeby mowa wodza moja niedorzeczna przezywać; dajcie mi taki oszczerca, a kiszki jego wszytkie mały ino palic Sigurda wydłubie i na ony kiszki ścierwo jego z gałęzi obwiesza! – Tu na gębie sczer­wieniał i łapą w ławę jął walić aż statki poleciały po całej izbie.

Przecław, podkomorzy, pod ławy nura dał i stamtąd zaszczekał potulnie, a z nim psi liczni na ucztę czekający. Sigurd, setnik, na majestat się oglądnął, czy dłubać każe, wszelako kneź miłosierny, Warczysław, ogarowi któremu kość starą z mordy oślinionej wydarł i w Przecławową wetknął, łaskę onemu okazując.

– Kneź, jako zwykle, pobłażać skłonny – ozwał się stryjec książęcy, Zbysław, ze zgryźliwością sobie przyrodzoną – pewne, iż zbójowie, co ich chce tępić, o onej łaska­wości słyszą i przez to bez obawy się mnożą a prawo obrażają.

– Książę barzo po chrześcijańsku się sprawuje – wyrzekł zaraz Ulryk, wojewoda – bo przykładem świecić musi tym duszom pogubionym, choć rodu najprzedniejszego, co za pogaństwem po cichu wzdychają, a kiedy dzwony kościelne milkną, a z mrokiem diabelstwo łeb podnosi, pono zabobony pogańskie odprawują skrycie.

Powietrze w izbie ugęstniało i jęli się rajcy ku debacie stanu sposobić, często gęsto osełkami po toporkach i kozikach ciągnąc, wszelako Warczysław, kneź, owe służby monarchii chwalebne na czas lepszy odroczyć umyślił, bo garncem o ławę wal­nął i zapowiedział gromko: – Tera języków nie strzępmy, bo do czynów przystąpić nam trzeba, niezwłocznie własną osobą lustracyję gościńców uczynim i nie odradzać mi po próżnicy, bom postanowił.

Za czym majestat z zydla podniósł i w izby swoje wstąpił, na Chichrałę, trefni­sia, palcem kiwając.

\*

Niechybnie cię, kneziu, Warczysławem Krotochwilnym obwołają – Chichrała, błazen, sarknął zjadliwie – i nareszcie na urzędy będziem się mogli pomieniać bez obawy żadnej, iż poddani odmianę wyczują. Gdzie ty zbójców poszukiwać będziesz? Dwa roki temu kniejeśmy czesali i prócz zbrodnia jednego co jako pień był głuchy i bielmo na oku miał, nikogośmy nie ucapili.

– Miarkuj się, Chichrało, błaźnie! – burknął majestat rozeźlony – bo nie sam jeno immunitet ci odjąć każę, ale też członka którego!

Wszelako zafrasował się widocznie i kudły na rozumie palcyma mieszał.

– Trza radzić – wyrzekł trefniś ugodowo, a Warczysław, kneź, rozejm skinie­niem zatwierdził i w dłonie na straż klasnął. Zaraz też zbrojny Łamignat się opowiedział do służb gotowy.

– Setnika Sigurda migiem mi wołaj – nakazał kneź, ale nim zbrojny w te pędy ruszył, wstrzymał onego niespodzianie.

– Rzeknij mi, cny Łamignacie – przemówił wolno – czy tam u was w szeregach jest w tym rozeznanie, jako to ze zbójectwem bywa?

Pokazało się, że nie są to dla drużynników arkana żadne. Każdy to wie, że nawet krainie najmężniejszej się to przydarza, iż czasem znienacka najazd zdradziecki ją opadnie, a wrogi tchórzliwe z łupieżą zbiec zdołają przed pobiciem. Tako też bywa, że swojska wyprawa dzielna w kraj ościenny pociągnie, wszelako przez zajadłość tambyl­ców i nie­cne onych podstępy nazad z pustymi juki powraca. W takowych razach skarb stęka, podkomorzy złorzeczy, zasię wojewoda drużynę częścią rozpuszcza, to rozpuszcza­nym przykazując, by w gotowości byli i rzemiosło wojenne i sztuki przetrwania ćwi­czyli, bo kiedyś bieda popuści i zaciąg nowy nastąpi. Znający pismo nazywają owo „re­zerwa”. Gdzie zaś przetrwanie i walki lepiej ćwiczyć jak nie w zbójnictwie? Kto mo­narchii mi­łej żywot swój poświęcić gotów, zbójowanie ćwiczy pilnie, na zew czekając – prawił Łamignat, zbrojny, wielce wiernopoddańczo. Kiedy dwa roki temu – dodał – Warczy­sław, kneź najsławniejszy, zbójnictwo wyplenić postanowił, zaciąg uczyniono jak nigdy dotąd wielki, by wszelkie chaszcze a komysze obstawić. Jednemu tylko zbójcy głu­chemu i jednookiemu znachorzy wojenni zaciągu odmówili, reszta zbrod­niów na do­bro się nawróciła, poczciwie na prawa straży stanąwszy.

– Samem się naonczas zaciągnął – zakończył Łamignat, drużynnik, ze szczerego serca. – Z Piąchą, dziesiętnikiem, znaczy się, cośmy go na ćwiczeniu hersztem zwali.

Warczysław, kneź, potargał dla odmiany kudły na gębie.

– Biegaj mi po Sigurda, a i tego Piąchę przyzwij – warknął.

\*

Setnik Sigurd do onych rycerzów błędnych się zaliczał, co to w świat się puściw­szy szeroki, sławy i majątku u władców obcych szukali, a że w rzeczonym sze­rokim świecie wojowie z północy powszechnie za naj­więtsze rębajły uchodzili, chętnie go kneź przesławny Warczysław na dworzec swój przygarnął. Teraz do rady barzo się majestatowi wydał sposobny, bo nie tylko z prze­wag bitewnych północni wojowie sły­nęli, ale i z tego, że do ćwiczenia w zbójnictwie pilnie nadzwyczaj się przykładali, większe niźli inni onego rzemiosła rozumienie osią­gając.

Jeno że setnik Sigurd, co ze zbójowaniem był obyty, trudności widział:

– Rzecz prawie niemożebna zbój lustrować bez obława i siła znaczna. A też za­sadzka potrzebna. A książę w samopięć iść chcą.

– Zbójce kupą chadzają – bąknął Piącha, dziesiętnik – przeto też prędzej oni nas zlustrują, niźli my onych. Zasię proceder zbójecki na gościńcach się odprawuje, toteż gościńców trza się trzymać.

– Zbój nie po trakta parada, ino u brzega w knieja skrywa – zahuczał Sigurd, set­nik, raz jeszcze – minąć zbój można bez wiedza.

– Chyba, że kupa minąć się nie zezwoli i opadnie znienacka – trefniś dodał – a na to bacząc, człek w rzemiośle rządowym doświadczony to skalkulować musi, czy skarb koszta pogrzebu monarszego i stypy wystawnej uniesie, o ceremoniach kościel­nych i inszych nie wspominając. I nad tym zamyślić się wypada, czy stryjec Zbysław tu przytomnego knezia Warczysława na stolcu monarszym godnie zastąpić potrafi.

Tu rajcom koncepta się pokończyły, bo pomilkli hurmem i łby pospuszczali.

– Krakanie od was jeno słyszę – burknął Warczysław, kneź. – A tu rozumy wy­silić trzeba. Słowo władcze z ust moich padło, zbójnictwo zlustrowane być musi i to samopięć, ażeby niezgułowatość kasztelanów i inszych urzędników pokazać jasno.

W to akuratnie przytomni nie wątpili, uszyma duszy słysząc, jako zamiaru kne­ziowego zarzucenie, abo też chybienie celu lustracyjnego szeptaniem wśród ludu pro­stodusznego się objawia, że cosik Warczysław, kneź, oby żył długo i szczęśliwie, na­zbyt pochopny jest i gorączka, przez co sprawy na manowce idą. A w łepetyny utru­dzone głębiej wchodząc, wzrokiem dusznym to jeszcze przytomni uwidzieli, że cmo­kają poniektórzy ludkowie z zadziwieniem i głowinami nad tym kręcą, że przecie z tego samego rodu pochodzący Zbysław, stryjec knezia gorączki, całkiem innej jest natury, bo nad podziw stateczny i w pomyśle rozważny. A tam inni mruczą, że gdyby kneź ja­sny, miast przybłędom z oddali ucha nakłaniać, rad swojaka Guzdrały słuchał, co go wiece obrały, barzej by było rządzenie wydarzone. Może lepiej klucznicy Matyldy byłby zaufał, gadają znowu inni, bo owa od przemyślnych rzemieślników i kupców ob­rotnych myśli mądre chętnie czerpie i na dworcu je przedstawiać gotowa.

Toteż zadumali się milczkiem Sigurd, setnik, z Piąchą, dziesiętnikiem, a też sam majestat z Chichrałą, trefnisiem. Drużynnik Łamignat stanu był najniższego, przeto z myślenia zwolniony, wszelako ku pomocy skorym będąc, po stągiew piwa był się udał, aby dumanie lekciej upływało.

Dopiero kiedy w trzeciej stągwi dno się pokazało, a inni w pomieszanie tępe po­padnęli, z nagła Chichrała, błazen, poruszenie był uczynił.

– Mam! – zakrzyknął z triumfem wielkim. – I zaraz was w rzeczy objaśnię – do­dał, gdy inni podskoczyli. – Zali nie utrapienie lustrować mamy, którego zbójowie przysparzają? Czy do tego zbójowie potrzebni? Miast onych zbrodniów bez nadziei łowić, niepoczciwe sługi kasztelanowe ułówmy, co to o nich pode lada gospodą potknąć się można, jak popici walają się gnuśnie.

– Jakże to? – zadziwił się majestat, nie do końca pojmując, a też inni gęby prze­pastnie rozdziawili.

– A tak to, że co będzie, jako się tłumom pokaże, iż rabusie podróżnych pobi­tych środkiem gościńca do samego grodu wloką? To będzie, że kasztelany łajna warci ze sługi swojemi. A to będzie utrapienia zbójniczego lustracyja pokazowa. Będziesz się mógł Warczysławie, knieziu, srożyć do woli na pachoły i urzędniki w peregrynan­tów obronie a troskę swoją o maluczkich pode niebiosa wynosić.

Teraz wszyscy pojęli, zasię Łamignat, drużynnik, z Piąchą, dziesiętnikiem, do komi­syji werbunkowej migiem się kopnęli, by rynsztunki a przybrania zbójowe wyfasować, co z poborowych wedle regulaminu zdjęte zostały.

\*

A rankiem, kiedy woje, dworzany i sługi rozmaite zebrali się z mozo­łem, z gorliwością pokrzy­kując należytą o rumaki, by świtę lustra­cyjną godną żywo ufor­mować, to się pokazało ku ich pomieszaniu, że majestatu w grodzie nie ma, a też Chi­chrała, błazen, z setnikiem Sigurdem, dziesięt­nikiem Piąchą i wojem Łamignatem kędyś przepadli.

Spór stary zaraz rozgorzał, kto ma zwierzchność za księcia sprawować, a też i nowy, czy za majestatem pomocnie ruszyć, czy doma ostać i grodu pilnować. Wielce byłby kneź Warczysław tym udowolniony, gdyby to dojrzeć był w możności, jak rajce i dworscy powszechny zapał do spraw koronnych rozstrzygania objawiają! W partiach Ulryka, wojewody i Zbysława, stryjca kneziowego kto żyw jął się kupić pode ich chorągwie, abo też pod skromniejsze znaki się garnął przy Guzdrale, co go wiecow­nicy opolni wystawili i przy klucznicy Matyldzie, co na stan swój niewieści nie po­mnąc, w sprawy koronne chętnie się mieszała. To wszakże oddać wszytkim im należy, iż zrazu poczciwie i w zgodzie chrześcijańskiej do biskupa Piotra o sąd się zwrócili, a tenże, jako już drzewiej bywało, w pergaminach rozstrzygnięć jasnych nie znalazł, bo też sta­tuta nie jasnością zadziwiały, jeno dostojnością łacińskiej mowie przyrodzoną. Trza było ku procedurom starodawnie utartym się zwrócić, toteż po debacie krótkiej herol­dowie liczbę urżniętych i poszczerbionych ogłosili i ono głosowanie jawnie poka­zało, że wojewoda Ulryk w prawach jest. Zaraz też wojewoda rząd ustanowił i trzy hufce po pięćdziesiąt chłopa na poszukiwanie majestatu w trzy strony wypuścił, to im przykazu­jąc, by ślady knezia odkrywszy, w jego okolicy podążali w oddaleniu stosow­nym, rze­czonemu w oczy nie włażąc, ale w gotowości będąc ku pomocy szybkiej, gdyby przy­krością jaką władca był dotknięty.

\*

Zasię z górskiej przełęczy dwaj oberwańcy się spuszczali. Ścieżka opa­dała na dół stromo pomiędzy głazy wielgachnymi. Niżej, jeśli jeno świerków gęstwa na to pozwalała, co czas jakowyś skraj równiny wi­dać było, jakby polany, do której stru­mień dopływał wartki, burzliwy i pienisty. Sły­szało się szum wodospadu gdzieś wyżej, z boku.

Jeden z idących, co był odziany w habit mnisi, połatany, widać strzygł pilnie uszami, bo ozwał się naraz:

– Cosik pohukuje po gąszczach, zali nie zbójce aby? Jako mniemasz, Obijmordo, bracie?

– Huczy cosik jako żywo, bracie Michale – przytwierdził drugi, wielki bardzo i ko­smaty, z pałą wielką w garści i znów zmilczeli.

Kroczyli ciężko w dół, raz to odchylając się w tył, raz opuszczając się bokiem i widać było, że dużo dziś drogi uszli. Na otwartej przestrzeni słońce przypiekało, gdzie­niegdzie po­wietrze drgało nawet nad głazami, jeśli nie w cieniu były. A przysiąc by można, że strumień na polanie chłodny jest i rześki aż do bólu kości. Jed­nocześnie od południa ciągnęła chmura, a poprzedzała ją duchota; chmura miała kolor po­nury.

– Gorąc, ale niezadługo nas ochłodzi – przepowiedział kosmaty, zadzierając łeb jasno­brudno obrosły i spotniały.

\*

Mało znośne stało się dłużenie i znużenie bezczynne. Jeno Sigurd, setnik, za ba­gatelę to brał, wedle jałowca pochrapując. Dziesiętnik Piącha w niebo patrzył o pień który oparty, zasię Chichrała, tref­niś, muchy ospale tłukł a komarzyska bzykające nud­nie.

– Gorąc – wyrzekł kneź z ziewaniem – wszelako na nawałnicę jakby się zanosi.

– Barzo kneź nasz w pogodzie obeznany jako i we wszytkim – przytwierdził Chichrała, trefniś – toteż wie niezawodnie, iż zara kąpieli chłodnej zażyjemy, co się nam strzykaniem w kościach jesienną porą wspomni. Może ku chatce niedaleko spo­strzeżonej się cofnąć i żywioł przeczekać?

Wszelako sam wiedział Chichrała, że gadanie jego próżne, bo wielce był kneź Warczysław w postanowieniach zapamiętały. Wnet zresztą wszytko się było odmieniło, bo oto woj Łamignat z gałęziów drzewa strzelistego się ozwał, zbliżanie się wędrow­ców przepowiadając donośnie.

Sigurd, setnik, oko odemknął, wszelako do przymknięcia onego nazad się uspo­sobił, o podłej kondycyji nadciągających usłyszawszy. Taką to obserwacyją Łamignat, drużynnik, przytomnych uraczył, kiedy z gałęziów zlazł.

– Zbójcy chudopachołka grabić nie ochotni – zamruczał Piącha, dziesiętnik – bo łup nikczemny.

Zadumali się wszyscy na to, bo prawda była. Wszelako jasnym tak samo się wy­dawało, iż kupce i peregrynanci zasobni przeze nawałnicę przedzierać się raczej nie będą, jeno w krze zaszyją, szałasy a namioty stawiając i pod płachty chroniąc.

– Że gnuśność kasztelanów i sług ich gościńców pilnujących uprzytomnić chcemy, może nawet akuratniej będzie krzywdę prostaczków bosych gawiedzi pode oczy podetknąć – wyrzekł Chichrała, błazen, statecznie. – Wielce lud nasz poczciwy na dolę maluczkich wrażliwym bywa.

– Jako żywo – zgodził się majestat ucieszony – i tym właśnie sposobem urzędni­ków nieudolnych pognębim. O garść denarów stoję, że w zbójniczym przebraniu do samego grodu pojmanych zawleczem w dzień biały, zasię nijaki zbrojny pachołek pal­cem w łapciu nie kiwnie, by areszt na nas położyć.

– Gdybyśmy bogaczów wlekli, ruszyć by onym paluchem mogli pachołkowie, nagrodę wietrząc – Chichrała, błazen, rzecz dopełnił – a nie to pokazać umyśliliśmy.

– Blisko już są – uprzedził Łamignat, woj, słuchami strzygąc.

– Mnie ich ostawcie – ozwał się nagle Warczysław, kneź chrobry – niechajże sam ich w pęta wezmę. Nie barzo politycznie by się stało, kiejbyśmy z przewagą wielką na łachmytów dwóch w pięciu zbrojnych ruszyli. Nie wiedzieć, czy pod którym krza­kiem zausznik Zbysławowy nie dybie.

\*

Daleki odgłos gromu zabrzmiał, do pośpiechu wędrowców nakłania­jąc. Przeto oba wyciągnęli nogi barzej, bo też i ze stromizmy na równe już zeszli, gdzie ścieżka do traktu dołączyła. Polana, którą z góry dostrzegli, wnet znów w gąszcze przeszła. Nie świerki tu już królowały, jeno ra­czej buki i insze drzewa liściem obrosłe. Wtem wiatr nagle poruszył powietrzem i za­szumiał lasem. Zaraz też trzask się rozległ potężny i z łomotem drzewo na drogę wąską upadło, zagradzając przejście. Zakonnik dał krok do tyłu, na inne drzewa spozierając, zaś Obijmorda pałę ścisnął.

– Auu! – zabrzmiało przeraźliwie – bij, morduj! – Postać dziwna, chrzęszcząc rdzą kolczugi, wzniosła ku nim miecz pogięty. – Auu! – zawyła raz jeszcze, bardzo groźnie.

– A kysz, zjawo! –mnich wrzasnął – precz z drogi, zawalidrogo przeklęty!

– Sakiewkę albo życie! – odkrzyknął na to mąż pordzewiały po namyśle małym.

– Sakiewkę? – zdumiał się zakonnik – my, chudziny boże głodem uciśnione? Skądże? Jeśliś z piekła rodem, to idź, przeklętniku, w pokoju, pókim znaku krzyża nie użył, albo też wody święconej z bukłaczka! Idź, zjawo nieszczęsna.

– Kłaść mi wszytko! – zbój ryknął. – Gołymi was, przecie bez szwanku do niewoli biorę! Dalejże, bo zasiekam! – Widać było, że do oporu niczyjego nie przywyknął jesz­cze.

– Wieleś musiał w pieskim życiu nabroić – pokiwał mnich głową, popatrując nie­znacznie na boki – że tak głupio musisz się wałęsać. I widać, żeś leń wielki. Nie le­piej byłoby ci pancerz cegłówką poskrobać? Więcej skorzystałbyś niż na zaczepianiu sługi bożego. Idź w pokoju, to pacierz za cię zmówię.

– Dość kpin! – wrzasnęła zjawa – nie będę mnichów głupich słuchał! Dalejże się rozdziewać, bo na pasztet zsiekam! – Widać, że się zbój rozsierdził, bo zaświstał mieczem. Gęba mu sczerwieniała, wąs się najeżył, a spomiędzy warg grubych wy­chy­nęły zębiska spore.

– Kąsać gotów – mruknął braciszek – zróbże coś, bracie Obijmordo, albo ścią­gajmy łachy; niewiele warte są.

– A bo to goły łaził będę? – burknął kosmaty i w dłonie plunąwszy raz jeszcze, do przodu ruszył. Zatrzymał się jeno dla gatek podciągnięcia.

– Cóż ty, śmierci w oczy leziesz? – wyrzekł zbój osłupiały – z człekiem w bi­twach za­prawionym mierzyć się chcesz?

– A bo to mam się czego bać? – odparł Obijmorda. – Ale puszczę cię wolnym, jeśli zejdziesz z drogi.

– Prostakiem będąc, winieneś bać się miecza! – upomniał go raubritter, zaś wi­dząc, że wróg kroku jednak nie wstrzymuje, dodał zaraz – albo opowiedz imię swoje, bym ci mógł swoje opowiedzieć i uwiadomić, że nie z byle ręki legniesz!

W tejże chwili pociemniało z nagła. Chmura przesłoniła słońce, posłyszeli grzmot da­leki. Hałasy leśne ścichły, stwory powpełzały do nor swoich.

– Czas nam w drogę, póki czas – ozwał się Obijmorda sentencjonalnie, zamie­rzając się srodze. I miast opowiedzieć miano swoje, znienacka grzmotnął zbója w hełm spiczasty, który zaraz wgiąwszy się, rdzą sypnął. Człek pod nim, wyszczerzywszy zęby silniej, nogi ugiął z ostrogami przy łapciach z łyka. Nie padł wszakże, jak przystało po tak silnym ciosie, lecz otrząsnąwszy się na kształt psa, machnął żelazem, godząc w przeciwnika. Miecz natknął się na pałę Obij­mordy i prysł w kawały, co zniesmaczyło zbója widać, bo krzywiąc się, dał tyły i po odskoku ku zaroślom oczył, klnąc na barzo wojacką modłę. Do brata Michała to jeszcze odeń jakby dobiegło, iż między klątwy onymi gniewnie na kasztelanów lenistwo wyrzekał, w tym się zwłaszcza pospolicie objawiające, że bezpieczności na drogach nijak nie pilnują; baty też onym kasztelanom leniwym obiecował. A może się zakonnikowi co przesłyszało, choć słuch zawsze nad podziwienie miał bystry?

Czasu nie było się nad tym zastanowić, bo oto z gąszcza banda zbójów wychy­nęła i jakby tego mało było, z nagła z pięćdziesięciu jezdnych skądeś wypadło i z wo­jennym zawołaniem końmi peregrynantów obalili i kupą do ziem przycisnęli, powró­słami a rzemieniami obwijając.

\*

Jezdni tyczki sposobne w jukach mieli, a też płachty z trokami, przeto wnet jako takie schronienie uczynili od drzew wysokich z dala, do co grub­szych krzewów je przywiązując, a gałęźmi grubymi przysypując, by ku niebiosom nie wzleciało. Cóż z tego skoro nawałnica się rozszalała, błyski i gromy do środka się wdzierały, ślepiąc i głusząc mężów przytomnych, a zdradzieckie podmuchy wichrowe płachty znienacka zadrzeć potrafiły, strugami chluszczącymi rzeczonych mo­cząc.

Zgodnie z prawem przyrodzonym wilgocie padół sobie upodobały, kędy Obij­morda i brat Michał w pętach złożeni byli, a kikuty pieńków pospiesznie wyciętych w zady im się wbijały a też i w inne okolice. Wszelako pożyteczne te niewygody dla nich się stały, bo orzeź­wieni pluchą i ożywieni kłuciem ze skupieniem należytym majestat War­czysława, kne­zia przesławnego, kontemplować mogli, dostojeństwa rycerzów znamie­nitych i wiel­moży Chichrały, błazna, bynajmniej nie pomijając.

Warczysław, kneź, w szatach przystojnych się prezentował, wszelako w szy­szaku zbójeckim na rozumie, bo szyszak owy, szpetnie wgięty, zdjąć się póki co wzbra­niał, na zejście opuchłości najpewniej wyczekując. Monarcha okiem naburmuszonym łypał, surowość władczą obserwować racząc.

Setnik Bruno, co konnym przewodził, pomieszany był, bowiem ani mowy ciele­snej majestatu, ani spraw wymiarkować nie mógł tajemniczych, co się na oczach jego stały. Wszelako łask się spodziewał.

– Wielkie niebu dzięki zanoszę – westchnął z miłością do dynastii jawną – żem zbrodniów na czas pojmał, co się na majestat pałą zamachnęli i żem tym sposobem nie­szczęście niepojęte od korony oddalił.

Cisza na to zapadła, zasię przytomni łypać ku sobie w zamyśleniu jęli, bo nie­pewne było, czy to tępota żołdacka się u Brunona pokazała, czy też może stronnika Zbysławowego chytrość podstępna.

– O jakim to nieszczęściu prawisz, setniku luby – po długiej chwili Chichrała, błazen, ozwał się serdecznie – przecie kraina nasza w szczęściu kwitnie, zasię miło­ściwy kneź, co nam panuje, w zdrowiu i tężyźnie przestaje na ręki ode ciebie wycią­gnięcie.

– Ale miłościwa knezia naszego łepetyna niecnie przez zbrodniów potłuczona, a nie daj Boże, co jeszcze stać by się mogło, gdybym z pomocą w czas nie ruszył – o sprawiedliwość Bruno, setnik, bełkotliwie się upomniał.

Szczęściem dla siebie gębę zawarł wreszcie, bo odium dziwne wyczuł w powie­trzu zawiśnięte, a też drużynnicy dysputę wielmożom przerwali, wygody rozmaite wno­sząc.

\*

Kneź przesławny, Warczysław, umościł majestat na zydlu z gałęziów naprędce przeze drużynników skleconym i derką spode siodła po­krytym, rycerstwo zasię na po­dobnych derach pokucało. Zachybotał się zydel wiotki pod majestatem na boki, przeto osiłki z warty, usiadłszy, po obu stro­nach plecyma onego podparli. Dla Brunona, set­nika, dery brakło, przeto wziął się do przeganiania Piąchy, dziesiętnika, wedle starszeń­stwa odeń mniejszego, wszelako sam kneź gestem poczynania owe wstrzymał.

– Poniechaj Piąchy, Brunonie, setniku miły – wyrzekł głosem chłodnym – mo­jego w wyprawie towarzysza wiernego i jako równego sobie go powitaj, bo on od tej pory do godności setnikowej jest podniesiony, co prawem mi przynależnym wiadomym czynię wszelkim poddanym moim w kupie, a tobie w osobności.

Wykrzyknęli przytomni sławę Piąchy, od teraz setnika, jako obyczaj kazał.

– Wielki to monarcha, przy którym kto żyw w potrzebie ochotnie staje, w tym ja, woj jako i Piącha, com jest, jako i Piącha, zawdy do służb gotowy – ozwał się Bruno, setnik, sposobność łatwą zaprzepaszczając, ażeby zmilczeć.

– Jaką to potrzebę w myśli oglądasz, setniku poczciwy, Brunonie? – dociekliwie monarcha zapytał.

Bruno, setnik, pomieszał się w sobie i język splątał, bo znowuż odium go ogar­nęło: – No bo, prawda przecie, zbrodzień zuchwały, prawda, się zamachnął pałą… – tu ślepia wybałuszył i gębę wstrzymał porażony.

Chichrała, trefniś, litościwie westchnął nade ułomnością setnika Brunona smutną i palec wzniósł, co we zwyczaju miał, kiedy nauki głosił. – Takowe w krajach gorących przypadki napotykałem – wyrzekł pouczająco – kiedy to podróżni słońcem popaleni przede sobą wodospady a gaje owoców pełne jako żywe widzieli, kiedy zasię do nich bez tchu dobiegali niby, naprawdę z górą piachu rozżarzonego się zderzali. Przypadłość owa omamem się zowie abo też pozorem i ludzkość za jej grzechy trapi, plotów a banialuków rozchodzenie się powodując. Takiż to omam Brunona, setnika tu przytomnego, opętał chyba, bo zda się, że roi, nieszczęsny, iże sam kneź przesławny, Warczysław, przeze niego zbawiony został.

Z nagła brat Michał, co spętany leżał, ku zadziwieniu przytomnych głos wydo­był z niegodnych ust swoich:

– Ośmielę się, prześwietny książę i wy, mężowie dostojni, przypowieść praw­dziwą przytoczyć, co myśl podeprze wielmoży onego, co właśnie mowę skończył. Owóż w mi znajomych okolicach podróżni zbłąkani barzo pewni są, omam świetlisty widząc, iż za pomocnie rozświetlającym mroki kagankiem bezpiecznie bieżą. A potem trwogę przeraźliwą wykrzykują, z nagła po uszy w błocko wpadłszy. Ale tu spieszę do­dać, by chrześcijańską troskę o żywoty bliźnich uspokoić, którą to troskę tak zacni mę­żowie, jak tu przytomni, niezawodnie objawiają, że tambylcy poczciwi w pocie czoła topiel­ców takowych wyciągają, słusznie za utrudzenie swoje mienie wyłowionych za­bierając. A na tym znoju tambylców nie koniec – rzekł mnich po chwili małej – bo po­tem im ta sama poczciwość gołych a bezbronnych topielców ostawić na zatracenie nie dozwala i stajami całymi ocalonych wiodą, aż ludzi dobrych napotkają, co golców pod swoją opiekę przejmą, by za wikt a przyodziewek w ich dobrach bezpiecznie i beztro­sko pra­cowali do końca szczęśliwych żywotów swoich. Rzecz jasna, ode onych dobro­czyńców sztuki srebra za każdego topielca biorą, iżby tą pewność mieć, że ludziom majętnym golców oddają, a nie biedakom co kosztom opieki podołać nie zdołają.

Najbliższy zbrojny słusznie zuchwalcowi żebra drzewcem zmacał za odezwanie się samowolne, wszelako kneź miłosierny, Warczysław, nie dość, że po chrześcijańsku despekt ścierpiał, to jeszcze mnicha usadzić kazał i do rady dopuścił, wymową onego najwyraźniej zaciekawion. Kazał nawet dziesiętnikowi któremu otrzepać godnie plecy rajcy nowo upieczonego z liściów i brudów rozmaitych, czego woj trzema pociągnię­ciami biczyska ze sprawnością wielką był dokonał.

Brat Michał za zaszczyt podziękował dwornie.

– Zdawasz się, bracie, wielce rozgarnięty i w kazaniach wyćwiczony – odparł majestat z dwornością równą – mowny jako ten oto Chichrała, błazen, a może i barzej. Wielce to nas zajmuje, bracie, jako że rzeczony Chichrała juże nam się osłuchał i bywa, że nudą trąci. Ostatnie rady jego jakoby świniakowi spode ogona wypadły. Mniemam, iż pastuch lada jaki wdzięcznie by go w radzie zastąpił, cóż dopiero mąż tak złotousty jako ty, braciszku.

– Nie śmiałbym z dostojnym Chichrałą na kazania w szranki stawać – pokornie brat Michał odparł.

– Nie poprzysiągłbym wcale – ozwał się Chichrała, trefniś, kwaśno – iż przypo­wieść, cośmy ją ode mnicha usłyszeli, do przypadłości Brunona, setnika, bez reszty się stosuje.

– Wypada się z przyganą zgodzić – rzekł zakonnik potulnie – przypowieść miała jeno pokazować w ogólności, iż omamienie zgubą grozi i życie na zawsze odmienić jest władne.

– A ja, że stosowna mowa była, widza – ozwał się Sigurd, setnik – bo Bruno za omam w bagno wpada, jeno kto go wyciąga, nie wiada, może on już utopiec?

– Jako to? – jęknął Brunon, setnik, jako żywo omamiony – za służby wierne?

\*

Warczysław, książę najdoskonalszy, prawicą o kolano prasnął.

– Dość już dysput, które prędzej mędrcom uczonym przystoją, niźli mężom rzemiosła wojenne i rządowe uprawiającym. Setnikowi Bruno­nowi dam pod rozwagę zwanego Pierdołą, magistra, co się filozofem zowie. Co z onym Pierdołą, setniku?

– Ano, w celi klasztornej przez biskupa Piotra osadzon, żeby błędy swoje prze­myślał – wyrzekł Bruno, setnik, a w omamionej łepetynie jego światełko zabłysło wątle.

Objaśnienie się tu należy, iże Mędrek, Izydorem ochrzczony, w Italii klasztor­nych lek­tur łyknął, przez co filozofem się był ogłosił. Lud prosty, co go filozof zacze­piał, by rozum jego próbować, przezwał onego Pierdołą. Pokazało się rychło, iże Zby­sław, stryjec kneziowy, karmił Pierdołą zwanego i odziewał, albowiem nauką onego poru­szony został o tym, że władca najlepszy to taki, co doświadczeniem a statecznością gó­ruje, zasię cnoty owe z wiekiem przychodzą. Przeciw onej doktrynie Odo, kapelan War­czysława, knezia, naukę wysunął o czasach, które cnót wojennych potrzebują, jasno wykładając, choć mową łamaną, że władca taki jest najzdatniejszy, co męstwem a ro­bieniem bronią góruje. Wywiązała się dysputa, chwałę dziedzinie Warczysławowej przynosząc słusznie, bowiem argumentów siła i wzloty niebosiężne mowy krasnej bla­skiem w niej zajaśniały tak wielkim, że i w najprzedniejszych chrześcijańskich krainach zdolne byłyby podziwienie wzbudzić. Nie dziwota przeto, iże gorączka debaty urosła pode niebiosa same, zasię filozofia nareszcie pod strzechy trafiła, kiedy tłumnie wdali się w nią poddani, osobliwie zaś sługi Zbysława, stryjca kneziowego, a też drużynnicy Warczy­sławowi. Kiedy pierwsi kozikami kółka oponentom na czołach rysowali w za­pale a też szyderczo pukali w czoła one obuszkami, drudzy wywalali na nich jęzory w ferworze dyskusyji adwersarzom oderżnięte. Wszędy gardłowano, wszędy debatę czy­niono! Po­ruszenie powszechne nastało nawet pośród prostaczków bosych, zważcie cho­ciażby, że tacy garncarze przyjęli uczniów nowych masę, bo wiele garnców z piwem na łbach po­tłukło się po gospodach, kędy fora debatowe ustanowiono i na stronę przeciwną najpo­spoliciej czatowano. Stany w onym tumulcie, zda się, pomieszały, kiedy to Zby­sławowi zapalili się do ko­pania glin, zasię przysposabianie zapaleńców do cechu ga­rowników barzo zajmowało Warczysławowych rycerzów. Popyta na naczynia, koziki, obuszki, sprzęta szynkarskie i trunki pode niebiosa skoczyły, wszelako do skarbu opłaty a myta należne nijak nie płynęły, bo też wyznawcy Pierdołowi na drużynę łożyć się nie kwa­pili, zasię drużynnicy na immuniteta a ulgi się powoływali. Toteż Warczysław, kneź, cier­pliwości swojej przyrodzonej odstąpił i tumult powściągnąć był zamierzył, do bi­skupa Piotra o osądzenie nauk się zwracając. Wzorem najświatlejszych monarchii bi­skup so­bór wszytkiego duchowieństwa obwołał, ażeby na onym soborze odolenie i pier­dolenie jako doktryny należycie zbadane zostały na okoliczność zgodności z Pismem. Kiedy wszakże zapał do dysputowania kapłanom a mnichom się udzielił i bez spo­czynku ba­dania czynili przez miesiąców parę, siłę z jadła wystawnego a trunków kne­ziowych nie­spożytą czerpiąc, a końca soboru widać nie było, wiecujące duchowieństwo Warczy­sław we wieży zamknąć kazał, by spokój i skupienie osiągnęło o chlebie i wo­dzie; po ogłoszeniu wyroku słusznego wie­rzeje rozewrzeć solennie kneź obiecował. Barzo Fry­deryk, poseł cesarski, Warczysła­wowi, kneziowi najroztropniejszemu, za to klaskał, zamysł chwaląc i głośno przekony­wał, iż się wśród kleru niechybnie przyjmie. Jakoż i skutek był szybki i już po dniu Piotr, biskup, to ogłosił, iż pierdolenie jawnie błędem się okazało i herezją wszeteczną, zasię Izydor zwan Pierdoła klątwą obłożony został. Do wyroku przyłączyli się bene­dyktyni znad morza i Leon, pustelnik, co przeze puszczów gęstość nieprzybytą dopiero na samo ogłoszenie nadciągnęli i by nie gadano, iż udziału nijakiego w procesie nie mieli, wniosek nowoczesny przedstawili, ażeby Pierdołę na stosie spalić. Propozycyją oną przyjęto wdzięcznie i nawet chrusta stosowne zebrane zostały, wszelako na celi klasztornej bez prawa wychodu się dla Pierdoły skoń­czyło szczęśliwie. A szkoda wielka, jak dotąd ludzie światli mniemają, bo wedle przy­słowia, które skrybowie gło­szą, brakło owej kropki nad i, co by o pełnym cywilizacyji chrze­ścijańskiej oswojeniu zaświadczyła. A też i lud szemrał widowiska pozbawiony.

Tajemnica to dla wszytkich, nawet dla wielmożów znacznych, że za sprawą Chichrały, błazna, Pierdoła oszczędzon został. A tak to się odbyło: – Cny kneziu War­czysławie – wyrzekł rzeczony Chichrała na ustroniu – zezwól, bym ci sekreta objawił, jakie skrybowie cesarza wschodniego w księgach zapisują ku nauce monarchów swoich a wielmożów, co się do rządów sposo­bią. Jako dziejopisy pokazują, jest tak z filozofią, że nauka ta czy owa, jak pierdolenie dla przykładu, raz uznanie zyskuje, zaś potem po­tępie­nie na się ściąga, by znów kiedy blask od­zyszczeć. Toczy się bowiem fortuny koło, jako Gre­kowie powiadają, abo może stare Rzymiany gadali i tym sposobem co teraz na spodzie, za czas jaki na wierzch wychynie. Dla chwały dziedziny miłej i dla poddanych szczę­śliwości sprawiedliwie Pierdołę osądzono, wszelako za roków tuzin abo też dwa, zwłaszcza po Zbysława, stryjca, odejściu nieodżałowanym, a dorośnięciu wnuka onego, Zbigniewa, ach, cóż to za ladaco pacholę! aż nie przystoi… owóż za czas jaki może zdać ci się, Warczysławie, kneziu, pierdolenie mniej wstrętne, a nawet cał­kiem udatne. Zważ, że miły sercom naszym, a szacowny Piotr, biskup, niech mu Pan błogosławi, co w Piśmie przeciw pierdoleniu ustępy wynalazł, mówiąc prawdę, ludz­kiego języka się nauczył, a też po cesarsku szprechania, wszelako nie dostojnej łaciny, jako że mniema niefrasobliwie, jakoby język ów mowie rzymskiej obecnej równy był, czemu mędrcy liczni przeczą. Następca jego, oby Piotr żył jak najdłużej, barzej może kształcony, jak się w rzeczone ustępy wczyta, a nuż pomyłkę żałosną odkryje, iż to Odo herezję ogłosił, zasię Pierdoła, czy lepiej Izydor, magister, w zgodzie z prawdą nauczał. Przy takowym rzeczy obrocie nie będzie dobrze wyglądało, żeśmy Pierdołę spalili. – One to argumenta wywiódł Chichrała, trefniś, zasię Warczysław, książę, jako chrześcijanin prawy, po ro­zebraniu sprawy w rozumie, pomimo racji przeciwnych miłosierdziem się pokierował. Kazał jeno Pierdołę, filozofa, do pokuty przymusić, a pilnować dobrze, by się na zba­wieniu duszy swojej skupiał i na manowce poza mury nie puszczał.

Dodajmy, by rzeczy dopełnić, że zaraz po odolenia pochwale prawdę stanu ob­jawiono o niezwyciężoności majestatu i mocarności onego nadludzkiej, które to przy­mioty w osobie zacnej Warczysława, knezia, jawnie się dla wszytkich ucieleśniają. Brak nale­życie silnej wiary w owo objawienie karami najwyższymi obłożono. Znak widomy onej prawdy stanu czyli też przymiotów ucieleśnienie nawet prostaczkowie najlichsi na oczyska własne dojrzeć mogli, kiedy Warczysław, kneź, czasami odtąd Mocarnym zwany, wszytkich przytomnych, co w dziedzinie jego a świecie przepastnym wokoło akuratnie przebywają, na walkę śmiertelną wyzwał. Przybieżeli śmiałkowie ze stron świata czterech, osobliwie Zbysławowe osiłki najtęższe, wszelako smutny ich ko­niec był, kiedy gnaty im Sigurd, setnik, połamał, a też członki abo i łepetyny poodej­mował, porykując dla ochoty więtszej. Kiedy zasię wszytkich onych śmiałków Sigurd po­szczerbił, jako mocarz niepokonany przeciwko samemu majestatowi wystąpił. Nieprzej­rzana ciżba stanu wszelkiego świadkami była, jako to małym palcem majestatu w czoło tknięty padł Sigurd niezwyciężony i w proch się pogrążył, podnieść się nie mogąc, aż pachołów sześciu w pocie do izby go zataszczyło, gdzie przeze niedziele dwie dziewek dworskich pięć i na zmianę, i w kupie winem z Italii go obmywało, zanim ku­racyji dopełniwszy, cudem ozdrowiał i na nogi nazad stanął.

\*

Nie dziwota przeto, iże o onych przygodach pomnąc, tego przytomni rozeznać nie mogli do końca, zali Brunon, setnik, umiłowanie rodu panującego najbarzej na Zby­sławie, stryjcu kneziowym, był skupił, czy może na rozum słabuje.

Tymczasem w dniach soboru sławnego, któregośmy przed chwilą wspomnieli, jakoż i wcześniej, Brunon, teraz setnik, jeszcze orszaku margrabiego Arnulfa ozdobą się był mienił. Dopiero rok i parę miesiąców później na dwór kneziowy przybył, służby swoje ofiarując. Jako do tego doszło, różnie ludziska gadali. Zdarzył się dla przykładu Wulf, kupiec zachodni, któren był z marchii nadciągnął i po gospodach obficie piwsko pociągał. Tenże przed każdym, co słuchać chciał, opowieść snuł o lewicy Brunonowej w spódnice margrafówny Berty nieszczęśliwie zaplątanej i o prawicy sakiewką bujają­cej gęsto denarami wypchaną. Plótł Wulf a to, że margrafówna na dostoj­nego Arnulfa płakała, iże kutwa jest, najbliższej rodzinie miedziaki szczędzący, a to, że denary z sa­kiewki dopiero co przesławny Warczysław, kneź ościenny, z zacnego srebra wybić ka­zał, a też gadał jeszcze, iż Bruno, dostojnego Arnulfa w odrzwiach zwidziawszy, z sza­cunku powstał, wszelako nogawice z nim nie powstały, jako należy, jeno na padole się ostały czemuś ode kaftana odtroczone. Nie dziwota, że dostojny Arnulf na ony despekt głowę ze grozą obłapił i odprowadzić Brunona, rycerza, do podziemi kazał, to jeszcze za nim wykrzykując, by loch tak ciemny mu znaleziono, jako możliwe, iżby wstydu widać nie było. Próżno Bruno tłumaczył, iż haniebnemu postępkowi nogawic zaradzić rady nie dał, obie ręce zajęte mając. Próżno się bożył, iże denary ościenne dla oglądania jeno zbiera, bo chce w chrześcijaństwie zwyczaj taki zaszczepić, któren kolekcyją prze­zwał. Dostojny Arnulf, margrabia, jeno ręcyma machał na troków nogawicznych lichotę rze­komą, a też na nowinki, co niepoczciwie obyczaj psowają, wybrzydzał głośno. Na­kazał, by Brunonowi odjęto Warczysławowe denary za umyślne zaufania do waluty ojczystej podważanie, zasię klejnoty rodowe – za rozwiązłość stroju nieobyczajną. Szczęściem dla Brunona klejnotów strażnicy, co go zawiedli do dom, by denary zbrod­nicze do jed­nego wyzbierać po zakamarkach izbów i ogrodu, zwolennikami postępu się okazali my­ślą o kolekcjonowaniu przejętymi, takoż i sprawiedliwie z jeńcem eksponata podzielili i udali się krain oświeconych szukać, kędy nowości powiewu się nie tępi tak jak w Ar­nulfowej dziedzinie zacofanej. Rycerzowi Brunonowi przypadło na wschód iść. Tako to Wulf, kupiec, gawiedzi bajał, cośmy Wam wiernie powtórzyli, ani słowu jednemu Wulfa, ożłopa, wiary nie dając.

Jako było, tako i było, dość, iż do drużyny Warczysława, knezia, Brunon przy­stąpił. Cieszył się był tu zaszczytami i poufałością książęcą jako spraw zachodnich i cesarskiej sztuki wojowania znawca wielki. A tu masz, sławą swoją i urzędami za­chwiał przez gębą ruszanie nieroztropne.

\*

Dzieje Pierdołowe Brunon, setnik, nie raz na uszy własne słyszał, wszelako na oczy swoje ich nie widziawszy, przepomniał był o onych w gorączce przygód ostatnich. Teraz wreszcie jasności doznał a ulgi, bowiem gruntu twardego pode nogami poczuł krzynę, z którego go widok maje­statu zepchnął w rynsztunki zbójeckie obleczonego.

– Wybaczcie, książę i wy, zacni wielmoże, i wojowie najdzielniejsi – ozwał się wesoło – a mimo uszu gadaninę moją mało składną puśćcie. Czy to mowa moja nie cał­kiem jeszcze udolna? Nie, prędzej ony omam winny, o którym roztropny Chichrała, błazen, tak złotouście kazał. Nie znajdzie się przecie człek żaden, co by przez chwilę zwątpił, że rycerz w przytomności Warczysława Mocarnego z otwartymi oczy przesta­jący, mógłby do głowy myśl przypuścić, iż na świecie mąż taki się pojawi, co rzeczo­nego księcia przemóc może i ukrzywdzić w bójce. Azali nie jest tak aby?

– Jako żywo! – przytwierdzili rycerze i wielmoże – każden wie!

– Wie i prawdę głosi, chyba że przedawczyk jest on zdradliwy – kwaśno dodał który.

– Nie ja – wyrzekł Brunon, setnik, barzo godnie – com samego margrabiego Ar­nulfa odstąpił, choć płakał i złotem nęcił, bym mu przykrości onej oszczędził. Czym mu nie rzekł: Arnulfie, jesteś ty wódz przesławny, aleć Warczysław sławniejszy? Dzielnyś, ale on najdzielniejszy? I poniechał margrabia dostojny błagania, bo w sercu tak samo czuł jako i ja, choć skrycie. Widzę, iże słowa moje żeście opacznie pojęli, aleć nie przed uszczerbkiem na ciele Warczysława, knezia mocarnego, bronić się rzuciłem, jeno przed brudnym prostaka dotknięciem co się w chrześcijaństwie profanacyją zowie i jako zbrodnia obrazy majestatu słusznie jest karane. No i, prawdę powiedziawszy, jeśli zba­wić kogo zamierzyłem, to zbrodnia zuchwałego, póki mu jeszcze moc Warczysławowa ducha nie odjęła, bom to w prędkości umyślił – zważcie, dostojni, czy aby słusznie – iż milej lu­dowi będzie nie zezwłok onego smętny oglądać na częstokole wywieszony, jeno tegoż egzekucyję wystawną gwoli zabawie i pouczeniu wszytkich.

Warczysław, kneź, na wywód owy nic nie rzekł, aleć czoło rozprostował.

– Dość mi już o omamach, filozofach a też dziejach najnowszych – stanowczo oznajmił. – O sprawie pilnej radzić trzeba, nie o nich. A że to stanu sprawa i poufała, przeto w kompanii wąskiej oną rozbierzemy, resztę rycerzów zacnych do szeregów zwalniając.

– Plucha podła – jęknął Piącha, od teraz setnik, bo mu, jak często to bywa, wy­wyższenie we łbie krzynkę zamieszało. Wszelako Warczysław, kneź wielkoduszny, zuchwalstwo zmilczał, a może i mu ustąpił, jako że rzeczony Piącha z Łamignatem, już dziesiętnikiem, strażników w utwierdzaniu stolca chybotliwego zastąpili. Tak to ostała się w namiocie niedawna drużyna zbójecka z jeńcami i Brunonem, setnikiem, pospołu.

\*

– Radzić trzeba – napoczął Chichrała, trefniś, jako często zwykł czynić. I tu w rzeczy stanie objaśnił Brunona, setnika, a też i pojmanych mi­mochodem. Gdy już przytomni do głów po rozum sięgać poczęli, a to kołtuny mierzwiąc, a to pazurami czerepy skrobiąc, to jeszcze dodał roztropnie:

– Co byście umyślić nie chcieli, na to najpierwej baczcie, iż wiele dziesiątków chłopa przypadki widziało nas obchodzące, a mniemam, że i setka może, bo zwiadowcy uwiadomić mnie zdążyli, iż za kneziem, tym oto sławnym Warczysławem, gapiów wielu podążało chyłkiem, najpewniej sługi Zbysławowe, Guzdrałowe, Matyldowe a też posłów władców ościennych wszytkich.

– Jako żywo – Bruno, setnik, ze smutkiem przytwierdził – samem mnicha uwi­dział, co się przy poselstwie cesarza wschodniego kręci stale; ówże na desce przygodę całą kolorami malował śpiesznie, w pokrzywie zaszytym będąc.

– Otóż i to – westchnął Chichrała, błazen – zatem za pewnik brać musimy, iże nowiny jako amen w pacierzu wyciekły, a jutro najlichszy pachoł one ozorem oślinio­nym roznosił dalej będzie. Trza by tako rzecz ludowi objaśnić, by gapiów świadectwom nie przeczyć nadto.

– Przeczucie takie trapić mię zaczyna, iż sprawa cała łacno się może pogmatwać i taki pozór na koniec przybrać, że go w tej chwili nijak wyprorokować się nie da – ozwał się majestat z nagła zatroskany. – Nie raz już tak bywało, że dwa gapie, co w tej samej przygodzie przytomni byli, przygodę ową tak jasno i oczywiście opowiadali, że słuchacze głowy chętnie by położyli za to, że prawdę rzetelną poznać było im dane. Co z tego, gdy owe opowieści prawdziwe całkiem do się nie przylegały i jedna drugiej kłam zadawała. A czy to, bokiem dodam, co tu nasi zacni rajcy o omamie kazali, do spraw takowych się nie stosuje?

Po przerwie krótkiej setnik Brunon z rozwagą wielką przemówił powoli:

– Nie pomnę, by to się nie spełniło, co kiedy bądź Warczysław, kneź nasz, wy­prorokować raczył, obawę przeto mieć należy, że i to się ciałem stanie, co mu przeczu­cie obecną porą podsuwa. Skoro tak jest, że nie wiadomo, co jest i co będzie, to nie wiadomo też, jako najsłuszniej jest i będzie czynić należało.

– Z twojego kazania, po prawdzie, wniosek taki wypływa, że najlepiej będzie póki co nic nie głosić i nijak nie postępować – Chichrała, błazen, westchnął. – Do gro­dziszcza przeto trza się prześliznąć, w izbach zamknąć, a potem sondów pośród ludu zapuścić kilka, ażeby rozeznać, co się w pospólstwa łepetynach tłucze. W spokoju rzecz całą trza obmyśleć i uradzić co rozważnie.

– Tedy takoż nie wiada, jako do gród jeńce wieźć – setnik Sigurd zarechotał – na siodło paradna czy ze łby na dzida nadziane. W powrozy owinięty czy futrem?

Cisza zapadła w namiocie, ale też w kniei hałasy żywiołu zelżały, bo tako to już z nawałnice bywa, iż taka co barzej gwałtowna, to barzej też krótka. Toteż kiedy gryźli się dalej przytomni, z pomysły zmagając się milczkiem, tego doczekali na koniec, iż słoneczko chmurzyska rozegnało, wichry potłumiło, a ptaszęta do kląskania zachęciło beztroskiego. Jednakowoż cienie długie o tym poświadczały, że czas bez litości płynie, na nikogo nie czekając.

Pojawił się dziesiętnik który z zapytaniem, czy aby nie pora ruszać, iżby przede mrokiem w grodzie stanąć.

– Do drogi się gotować, jeno beze gorączki – majestat zdecydował – my tu jesz­cze ze dwa pacierze zabawim.

– No i? – do rajców się zwrócił. – Jako mniemacie? Na powróz jeńce czy na sio­dło?

I tu się pokazało, że czasem myślenie proste nad kształcenie duchowne i obycie rządowe się przedkłada. Łamignat, dziesiętnik, jak majestat wzrok na niego był pokie­rował, wyrzekł na to posłusznie:

– Przeze łby czym brańców zdzielić, by ich duch odeszedł.

– Kto rozezna, czy bronią czy słabością są porażeni? – objaśnił myśl podwład­nego Piącha, setnik świeży. – I głupstw nijakich z gąb nie wypuszczą, co je wojowie po gospodach roznieść mogą – dodał barzo rozsądnie.

Łatwo słowo wylata, ciężej ciałem się stawa. Z bratem Michałem łatwo było po­szło, wszelako Obijmorda, po łbie prany, jeno ślepiami mrugał, póki się Sigurd, setnik, młotem bojowym nie zamachnął.

\*

W izbach kneziowych głuchą nocą dopiero wędrowce nasi spoczęli, a to z przyczyny Bruna, setnika, któren był podsunął, coby w dro­dze marudzić, bo gapie w mroku mniej niźli w słońcu widzą.

Człeka Obijmordą zwanego z bratem Michałem do izby osobnej złożono, gdzie zamek był zmyślny, z krajów dalekich sprowadzony. Zwada się z tego uczyniła wielka z Matyldą, klucznicą, co piekliła się gromko, iż z prawa i obyczaju piecza nad kluczem do zamka jej się przynależy. Sam majestat wydrzeć klucz Matyldzie musiał i Piąsze dać, setnikowi świeżemu, co onego za pazuchę wcisnął, drzwi zasię kromie zamka dwójką zbrojnych ubezpieczył.

Kiedy słońce stało już wysoko, Warczysław, kneź, na pachoła klasnął, by ów majestat w gacie i koszulę oblókł, co do posłuchań mniej ceremonialnych pasowne były. Szyszak wciąż na czerepie tkwiący w tkań zdobną owinąć zgrabnie kazał, by za znak godności mógł uchodzić. Śniadać szykować w izbie małej przy spiżarni zlecił. Tamże Chichrała, trefniś, z Brunem, setnikiem, zaraz nadciągnęli, czujność pierwszej wody dworską okazując. Wnet Sigurda, setnika, poproszono, a też Piącha, setnik świeży, z Łamignatem, dziesiętnikiem, brańców wczorajszych przywiedli po cichu.

– Witajcie, dworzanie mili, wojowie i znajomkowie – prosił majestat siadać – kosztujcie, czym spiżarnia bogata. Jaka noc była?

– Łepetyna moja trzeszczy czemuś – jęknął brat Michał, chustę zmoczoną na ro­zumie poprawiając.

– Bywa takie łepetyny oszołomienie, kiedy kto na nowem miejscu śpi – pocie­szył mnicha Warczysław, kneź, słusznie czasem Miłosiernym zwany.

W tejże chwili zbrojny weszedł, posła cesarskiego Fryderyka oznajmiając.

– Powiedzże onemu, iż sądy dzisiaj odprawuję i sprawy ościenne jutro abo poju­trze rozeznawał będę – niecierpliwie Warczysław, książę, wyburczał.

– Nie on jeden wyczekuje, gościów dziś hurma – ozwał się zbrojny – zasię rze­czony za plecyma mojemi wystawa. – Po czerepie się drapnął i z wahaniem dodał: – Są też niewolni a łachmaniarze liczni z kowalczykiem Prasłą…

– Wciórności! – majestat zaklął – czy to dzień posłuchań kto wyznaczył beze knezia zapytania? Zawrzyj drzwi na chwilę, gościa o wybaczenie proś i za pół pacierza puszczaj. – Co zaś? – po przytomnych okiem potoczył.

– Cóż, Warczysławie, kneziu – setnik Brunon odparł – proroctwo twoje speł­nione, sprawy się gmatwają, ploty nade wały grodowe urosły.

– Nie ma co gościów zbywać, od posłuchań się wymawiać, bo to ino ploty rze­czone powiętszy – Chichrała, błazen, z rozsądkiem wyrzekł – trza byka za rogi wziąć, mniej gadać, więcej słuchać, co też się po ludzie rozeszło.

– Posłuchań dzisiaj być nie miało, przeto nie na posłuchanie, ino śniadać go­ściów prośmy. Gadać jeden przeze drugiego będą, majestat tego słyszeć będzie, którego zechce. Jak kto przepytywać z uporem zamierzy, nie dojdzie niczego, jeśli uprzejmość witać gościa następnego kneziowi nakaże – setnik Bruno radził spiesznie. – Na uczcie jak na uczcie, harmider i swawola, przeto też bywa, choć nie przystoi, że miast maje­statu wielmoża ten czy ów do rozmowy przystąpi z nagła i wątek zamieni. Jak do kło­potów przyjdzie i w kozi róg nas zapędzą, woj który spośród nas w pysk komu da, ma­jestat rozsierdzi i pogawędkę miłą zerwie; po takiem postępku nie dziwota, jeśli go­ściów rozejść się do dom proszą.

– Ten oto Obijmorda i mnich z nim pojmany, w kąt ciemny siada i pyska na ja­dło ino kłapa – setnik Sigurd za majestat nakazał. – Łamignat, dziesiętnik, z onymi i jak który jęzor puszcza, on jemu kozikiem jęzor przycina. Wyjście inne nie ma, opodal Fryderyka, posła, nijak się nie przemykają.

\*

Fryderyk, poseł cesarski, gębę ode uciechy rozwartą miał, a też krok skoczny.

– Pokłon kneziowi, co roztropnością wszędy słynie – zawołał ode proga. – Wielka to radość usłyszeć, iże się tak zacny monarcha do cesarza zamiarów przyłącza, by zbrodnie na gościńcach chrześcijańskich tępić. Doszło mnie, iż pierwsze przewagi w onej wojnie są odniesione, a zbój ogromny, jakiego dotąd nie widziano, na oczach ludu sprawiedliwie będzie męczony.

– Siadaj, Fryderyku, pośle zacny i śniadaj z nami – odrzekł majestat na to – dzi­siaj na sprawy wielkie nie jest czas, jeno na przyjaciół pogawędki krotochwilne. – A któż to jeszcze zaszczyt nam czynić zamierza? – zapytał ciekawie.

– Otom ja, Guzdrała, opolnik prosty, proszę łaski majestatu – gość następny się ozwał nisko w pokłonie przygięty i nosisko w rękaw zgrzebny wydmuchał obyczajnie – ten, co się o ludu prostego krzywdę zawdy opomina.

– I zawdy z ręcyma otwartemi przeze dwór Warczysława, knezia sprawiedli­wego, witany będzie – Chichrała, błazen, powitał serdecznie. – Za jadło bierz się i na­pitki, dobry Guzdrało, boś pewnikiem zdrożon wielce o ludu szczęście zabieganiem. Chętnie się kneź nasz z tobą dobrami podzieli, bo także samo, jako i ty, ludu pomyśl­ność nade wszystko przedkładać ma we zwyczaju.

– Przez co słusznie innym chrześcijańskim monarchom za wzór może być sta­wiany – pochwałę dał Fryderyk, cesarski poseł, za czym zagadał z beztroską: – Wyla­złem ci ja po wieczerzy słowika słuchać przy fosie, aż tu widzę, jak Warczysław, kneź dzielny, ze świtą jeńca wiedzie w kolebce między rumaki, całkiem z nogów ściętego… Chyba że to tur jaki ułowiony, abo też miszka?

– I ja na dwór żem wylazł wtenczas – ponuro Guzdrała, opolnik, głosu dobył – jeno nie za słowikiem ino za potrzebą, bo też lud prosty na zbytki ptaszęce czasu nie ma i po znoju pokotem pada beze siły, ledwie do barłoga przyczłapie.

– Aleć ty żeś się z barłoga podniósł, miły Guzdrało – krotochwilnie ozwał się Bruno, setnik, piwa łykając.

Guzdrała, opolnik, obruszył się srodze: – Czym ja wielmoża jaki, bym mógł beze troski kupę przy barłogu uwalić pewnym będąc, że ledwie smrodu puści, zara pachoł służebny z szuflą nadleci i oną zeskrobie? Nic wy dworscy o niedoli ludu nie wiecie, a ten w mozole po nocy zrywać się musi.

– Słyszę ja, że w dworcu twoim, co go niektórzy z kneziowym równają, ze trzy­dziestu pachołów trzymasz, a owi przez dzionek cały jako w ukropie się uwijają – Bru­non, setnik, wesoło się przekomarzał – zali do kupów skrobania są oni niezdatni?

– Za dnia, nie w mroku, się uwijają – Guzdrała, opolnik, zamruczał – i wiedz, mój setniku, że nie przy robocie, ino przy bąków zbijaniu, a też nie pachoły to są, ino krewniaki ubogie, co ich żywić muszę, krwie własną utaczając. Nic wy, dworscy, o nie­doli…

– Prawda jawna – Fryderyk, poseł, przerwał – jeno żeś o widokach wieczornych prawił, zacny Guzdrało, a mnie to ciekawi, czy żeś to samo co ja uwidział?

– Com uwidział, tom uwidział – wyrzekł Guzdrała, opolnik – ale to słyszę, że lu­dowi na gościńcach krzywda się dzieje, bo są wędrowcy niewinni przeze zbójców krwawych opadani i nie dość, że bici, to potem w pęta brani przeze wojów, a to jest niesprawiedliwość o pomstę wołająca. Wie lud, iż są drużynniki ze zbójectwem trzy­mający, wie dobrze i na wiecach o porządek krzyczy.

Cisza między biesiadniki zapadła.

– Sam Warczysław, kneź miłościwy, o zdradzie kasztelanów ostatnim czasem prawi, jakiż przeto występek pośród wojów mniejszych kwitnąć musi! – dopełnił Guz­drała beze miłosierdzia.

\*

Warczysław, kneź, namarszczył się był na czole i może by i z wie­cami zadrzeć raczył, kiedy do izby z nagła kowalczyk Prasło wpadł, a z niem zbrojny za kark z trudem onego trzymający.

– My z tobą, Warczysławie, kneziu! – chrypiał Prasło – my z tobą! My z tobą! Jako żywo, z tobą lud cały! – aż wreszcie zuchwalca zbrojnemu ucapić się mocniej udało i za próg onego wywlókł, po łbie rękojeścią miecza okładając.

Na progu woj łepetynę obrócił jeszcze i Zbysława, krewniaka kneziowego, ze Ślizłem, zausznikiem, zapowiedział.

– Niechaj bogowie mają nas w opiece, znaczy, Jezu Kryst z Trójcą – zadyszał stryjec Warczysławowy ode prędkości. – Źle z ojcowizną miłą, kiedy rady się składa beze członków rodu panującego uwiadamiania. A ojcowizna płonie!

– Nijakiej rady nie ma – burknął majestat – śniadamy jeno sobie pośród przyja­cioły. A dziedzina moja nie płonie wcale, chyba że ode uciechy.

– Przyjacioły! – jęknął Zbysław, stryjec kneziowy – co by ojce powiedzieli, że posły mocarzów ościennych pomiędzy przyjacioły są liczone. Prawda, czasy teraz są ongi niebywałe… Wszakoż z tego się to bierze, co szepczą w kątach, iże rząd bratanka mego jako na smyczy dzierżysz, Fryderyku, pośle cesarzowy.

– Głupoty gadają i zdradę pewnikiem – ze smutkiem poseł wyrzekł – aleć ty, Zbysławie, jako mniemam, oszczerców takowych na paliki ponaciągać żeś kazał, boś księcia prześwietnego, Warczysława, krewniak, przyjaciel i poddany wierny?

– Ręcyma własnemi bym ich naciągać musiał – Zbysław, stryjec kneziowy, westchnął – a lata już nie te… Nie jest ci tajemnicą, Fryderyku, że Więcław, podskarbi, srebra na dwór mój skąpi przeraźliwie, przez co drużyna moja z wakatów uszykowana, nie ze zbrojnych… A też jakby za takowe obmowy, coś je usłyszał, poddanych nabijać, wnet by ludu zbrakło. A co takimi czynić, co gorsze rzeczy powiadają?

– Jakież to? – majestat się zaciekawił – jakie powiadają?

Zbysław, stryjec książęcy, ku niemu się nachylił i szeptem zaświstał:

– To się rozeszło… Aż gadać zgroza! Ty powiedz, Ślizło, przyjacielu jedyny.

– Przeze gębę nie przechodzi, nie przechodzi – ręcyma drżącemi Ślizło się za­stawił.

– Co zacz! – kułakiem po ławie majestat rymnął.

– Oto mówią, jako to Warczysław, kneź, ludowi przeciw się obrócił i bezbron­nych na kształt zbójcy opada. Póki co po puszczy to czyni, wszelako ino patrzeć jak na osady się rzuci… Maluczko a poddanych wszytkich wygubi w złości dziwnej… Tłuma­czą niektórzy, że czarem odmieniony i odtąd krwie pija jako wilkołak alibo strzyga – płakał Zbysław, stryjec kneziowy, rzewnie – o dolo, dolo, czemuś tak się na dziedzinę naszą zawzięła! – I wybiegł z izby w rozpaczy, a Ślizło, zausznik, za niem.

Przeze odrzwia odemknięte wnet na czworaki Prasło, kowalczyk, się był wczoł­gał, wszelako gęby rozewrzeć nie zdołał, migiem za nogi przeze straże odciągnięty.

\*

Weszła za to Matylda, klucznica, z Teofilem, kupcem najwiętszym i Omarem, mało co mniejszym ode niego. Cała trójca frasobliwie na majestat spoglądała, obronnie sakiewki do łonów przytulając.

– Prosim pięknie – ozwał się Bruno, setnik – kiedy kupce we dwór wstępują, ja­koby dostatek wstępował. Czemuśmy zasłużyli na to?

– Cieszą ci władce znamienici, co kupce szanują, bo gdzie kupcom się wiedzie, tam i monarchom nie zbywa – ostrożnie wyrzekł Omar, kupiec. – Tako i kalifowie czy­nią, przez co mocarze z nich są pod słońcem najwiętsi, a że dobrze to wiedzą, skąd ich potęga płynie, po kresy ziemskie o kupce swoje się upominają, by im się krzywda nie działa.

– Poczciwie w tej akuratnie mierze kalifowie czynią – Teofil przyznał – bo ku­bek w kubek także samo jako i cesarzowie, co z Konstantynopola chrześcijanom prze­wodzą.

– Chyba niektórym jeno chrześcijanom przewodzą, tym zresztą, co to ich wiara niekoniecznie za najczystszą uchodzi – wtrącił się Fryderyk, poseł, cierpko. – My tu na Zacho­dzie własne cesarstwo mamy.

Warczysław, kneź, oblicze rozpromienił.

– Barzośmy tym zaszczyceni, iż tak zacni mężowie pode skromną strzechą naszą porządek świata układają! A zdało mi się zrazu, jakoby mniejsza potrzeba gościów naszych do nas przywiodła.

– Gdzie nam, maluczkim, o świata układaniu prawić – kupiec Teofil westchnął – ot, mróweczki żeśmy, nie nasza to zasługa, iże za nami pierwsze na ziemi władce stoją…

– Nie urządzanie świata nam w głowie – dodał Omar, kupiec – jeno owe urzą­dzenie przykre, co światem nieszczęsnym trzęsie, a go fiskus zowią. – I tu na urok splu­nął, zasię Teofil znakiem krzyża po trzykroć wsparł onego.

Czas nastąpił, by klucznica Matylda głos wydała: – Przyszli ku mnie owi zacni mężowie, bo w nowem sposobie dani a myt pobierania chcą się rozeznać gruntownie. Jako słuchy donoszą, ode wczoraj zbrojni kneziowi po gościńcach się zasadzają, coby okupy zbierać, a peregrynantów takowych, co uiścić czym nie mają, w pętach do gro­dów zabierają na chłostę abo i stracenie. Trwoga bierze, czy aby kupiectwa kresu smęt­nego to nie zapowiada. Pono sam majestat osobą własną porządków nowych wdrożenia pilnować raczył?

. – My tu gadulstwo czynim, a w kubkach pusto – Chichrała, trefniś, zakrzyknął ode zgrozy – Niechaj pachoł który wina zacnemu Omarowi poleje, abo nie, sam onemu wleję! Warczysławie, kneziu, sługi zgań, nie chcesz chyba, by cię na języki w kalifacie wzięli?

– Aleć nie uchodzi – wzbronił się Omar – nie uchodzi!

– Wiem ci ja, że poddanym kalifa, tym, co z nim wiarę dzielą, wino dla uciechy wzbronione, wszelako wiem też, jako i wszytcy, iże ci je medyk na boleść zalecił. Lecz się przeto, a nie cierp, bo i my z tobą cierpieć z troski będziemy.

Wzbraniał się dalej Omar, kupiec sławetny, wszelako boleścią zniewolony dusz­kiem bukłak był opróżnił, za czym wzniósł piosnkę skoczną w mowie niemej i chucią z nagła poruszony, dziewkę służebną z pobliża za próg porwał.

– Wybacz, Panie, grzesznemu – westchnął Teofil, kupiec ościenny. – Co się zaś fiskusa tyczy…

Huk się porobił, kiedy Sigurd, setnik, o ławę łapskiem prasnął. Pokazało się, co mu pachoł źle usłużył. Ze dwa pacierze majestat gorzko mu obyczaj gruby wyrzucał, aż owy poprawę obiecał.

Matylda, klucznica, z cierpliwością czekała, aż Warczysław, kneź, gębę zawrzeć raczy i ku niej baczenie skłoni.

– Mniemam w fiskusa sprawie… – poczęła nawet.

– Ale, ale, Warczysławie, kneziu dobrotliwy, tyle tu o maluczkich gadamy, a tam za progiem Prasło, kowalczyk, o posłuchanie wnosi z łachmaniarzami swojemi – napo­mniał Bruno, setnik.

– Prawda, że ladaco, ale miłosierdzie pokazować trzeba – Chichrała, trefniś, głową pokiwał. – Może tem razem co do rzeczy zagada?

\*

Kowalczyka Prasłę Zakałą kneziowi zwali. A nie był owy pospolitym prostakiem jakich wielu. Razu pewnego Chichrała, trefniś, w przy­tomności knezia Warczysława będąc, sam się ze sobą na głos w ro­zumie mocował, czy aby Zakały do filozofów nie trza liczyć. Jak by w tym słuszność była, z Izydorem, Pierdołą zwanym, byłoby takowych dwu, a czy to by nie stawiało dziedziny Warczysławowej pośród najtęższych w chrześcijaństwie mocarzów w filozo­fii?

– Zali tak nie jest – pytał Chichrała siebie – iż filozof żywioły, prawidła abo po­pęda wynajduje, które w pojedynkę alibo w liczbie małej świat urządzają, przez co mę­drzec, co ich naturę rozezna, objaśnić wszytko jest w mocy, a jeśli nie wszytko nawet, to wielkie świata kawały jako to co przyrodzone, co boskie, co ludzkie abo co w duszy?

Prasło nauczał, iże wszytko jest kupa, choć może być, że kromie sików. Osobli­wie porządek ludzki z onego żywiołu ulepionym widział. Tak jak kupa, kiedy zwie­trzeje, jaśminem wonieć poczyna, tako też porządek ludzki, kiedy by się rozłożył, też by zapachniał mile. Jeno że sam z siebie nie jest struchleć zdolen, albowiem świeżej kupy na wyprzódki przybywa. Owa kupa świeża z poddaństwa rosnącego się bierze, w które biedacy popadają od bogaczów łupieżczych. Toteż ruchawkę pod zawołaniem jaśminu trza wzniecić, bogaczów pobić, a mienie im pobrać i na chudopachołków dobro obrócić.

Wielkich Prasło utrapień koronie przyczyniał, przez co cięgiem sądy nad niem trza było odprawować. Żebyż ino nauki głosił, na czym filozofowie zwykli poprzesta­wać, aleć tego mu mało było, przeto ucznie przy sobie kupił jakoby drużynników ja­kich, a tymże tumulta przykazował niecić i przeciw majętnym wykrzykiwać.

Wnosili pokornie do tronu najprzedniejsi poddani, by był Zakała za bunta na stracenie wzięty, jeno że przeciw racyji stanu by to było, a to temu, iże najprzedniejsze ostrza onże kuł i sam wojewoda Ulryk ręce załamywał, iż beze Prasły przemysł wo­jenny na amen sczeźnie. Kromie tego najwiętszych wielmożów garstka świadoma była tajemnicy stanu, że ten sam Prasło z miary srebra do pięciu miar denarów wybić umiał i nikto nigdy nie rozeznał, jakim to sposobem czynił, że na zacność kruszcu ludziska się nie wykrzywiali więcej niźli to zwyczajnym w świecie bożym było; nie dziwota, że za to przeze Wię­cława, podskarbiego, cichcem miłowanym był nad brata. Jednakowoż mimo zasługi, choć nie gardłem, to jakoś dla porządku za występki karać go należało, ino że kiedy już wszytkie członki Zakale oprawca odjął, co do kucia zbędne były, jeno w dyby zostało wi­chrzyciela wsadzać na chwile małe, choć to szemraniem na bezsiłę monarchii się odbi­jało. Pojawiła się nadzieja, iż da się kłopotu zbyć, kiedy Sigurd, setnik, umyślił Prasłę na roboty wieczyste zasądzić i do kowadła łańcuchem przytwierdzić. Wnet wszakoż nadzieja owa ugasła, a kłopot wrócił, bo skuty Zakała młota ani tknął, w kłębek się zwinął, ani głodem ani męczarnią przymusić się do kowania nie dał i odczepić było go trzeba. Wraz z więzy Prasło obaw się pozbył, a nawet zuchwalstwa nabrał, bo to ubrdał sobie, iż musi być, co sam kneź Warczysław skrycie jemu sprzyja.

\*

Po Prasły, kowalczyka, do śniadania przypuszczeniu pokazało się mi­giem, że pod wpływem wieści z wczora mniemanie się u tegoż wy­kluło, iże kneź nareszcie na zakalstwo się był nawrócił, skoro znak ja­śminu do góry wzniósł i porządek ludzki obalać napoczął na gościńcach. Teraz Prasło ośmielić majestat przyszedł, by rzeczy dopełnić i w grodach a podgrodziach także samo kupę sprzątnąć, w czem on, Zakałą zwany, z drużyną swoją dopomoże i w służby się odda.

Wielka niem ochota trzęsła i siedzieć bez podrygów nie potrafił. – Może by kupą trącącego Teofila, kupca, od razu rozpruć i na częstokole obwiesić, bo to dla braci na­szych znak widomy będzie, iże czas ruchawki nadeszedł – w zapale podsunął. – A też oną Matyldę, czarownicę, co do kupy modły wznosi.

Warczysław, kneź, pośród przymiotów chrześcijańskich osobliwie zgodę był umiłował, przeto też Praśle zalecił, iżby Teofila, kupca, z Matyldą, klucznicą, do sprawy zjednał i o posłużenie za znak widomy ruchawki uprosił. Ku zgorszeniu przy­tomnych rokowania tak poczęte nie całe w duchu chrześcijańskiej miłości były się po­toczyły, co razy parę stronom wytknięto z boku.

Przylazł wtenczas starszy rzemieślników drzewnych, zwan Dłubała; tenże War­czysława, knezia, pode niebiosa wyniósł za pielgrzymów ode zbójów zratowanie i omdlałych leczenie miłosierne.

– Cieśle miejscowe osobliwie dolą pielgrzymów się załzawili – poczciwie mowę ukończył – toteż mię posłali, iżbym z niebożąt miarę zdjął, bo gdyby dolo uchowaj, nie wydobrzeli, nieszczęśni, pewnikiem tak miłościwy monarcha jako ty, kneziu Warczy­sławie, zacne trumienki onym ufunduje. – I tu dziewkę służebną gdzie szczypnął, iżby go do chorych prowadziła.

– Obyśmy więcej tak poczciwych poddanych mieli – Bruno, setnik, zawołał i przykazał dziewce smakołyki dworskie Dłubale podawać, a gdyby miary jakie chciał z kogo brać, własnemi kształty onemu usłużyć.

\*

No i nim kto postrzegł, ze śniadania uczta robić się poczęła. Kowal­czyk Prasło z Teofilem, kupcem i Matyldą, klucznicą, przerwę w rokowaniach poczynili, iżby jadłem siły podeprzeć. Fryderyk, poseł cesarski, zadumany brodę gładził, zasię Guzdrała za pazuchę kiszki tkał dla ubogich krewnych swoich. Wszytcy tęgo pili a strawy nie odmawiali sobie.

Chichrała, trefniś, jął kneziowi znaki dawać, iż pora, by gościom dziękować, gdy wtem Teofil, kupiec, wrzask podniósł, że mu Zakała sakiewkę ode pasa podstępnie oderżnął.

– Prawda li to? – srogo majestat zapytał zbrodnia mniemanego.

– Co w tem dziwnego, że się zezwłok obszukuje, nim się onego na częstokole zawiesi? – zadziwił się Zakała prostodusznie. – No i co? – się najeżył. – Wiem, iż dwor­ski obyczaj delikatnym bywa, wszelako ruchawka jaśminowa nie zabawa, sama kupa się nie posprząta. Przecie słusznie z braciszkami mojemi tegośmy się domyślili, iże wczora Warczysław, kneź tutejszy, ruchawkę jaśminową wzniecił, wielmożę i przeora na gościńcu oprawiając?

Goście wszytkie uszów nadstawili.

– Jedno mnie ciekawi – Chichrała się wtrącił. – Nauka twoja, Prasło, kowal­czyku, taką oto prawdę podaje, iże się kupa z poddaństwa a uciśnienia biedaków rodzi, a skoro wiemy dobrze, iż z poddaństwa a uciśnienia bogacze srebro mają, przeto też wniosek sam wypływa, że srebro, a jako myślę, barzej jeszcze denary z niego, to kupa czysta. Czem przeto tłumaczyć, iż ci się do sakiewek tak ochotnie łapska wyciągają?

– Rzec bym ci mógł jako ów cesarz dawny, iże pieniądz nie śmierdzi – Prasło zaburczał. – Ale inaczej ci rzecz objaśnię, Chichrało, trefnisiu; owóż w krainach nowo­czesnych tu i ówdzie obyczaj się ulągł, by mierzwę na pola wygarniać, bo się pokazało, iż od tego plony są więtsze. Podobnie i my, zakalcy, oną kupę czystą, coś ją tako dow­cipnie nazwał, zbierać chcemy, iżby nią dolę biedaków użyźniać.

– Teraz wszakoż oddaj Teofilowi kupę jego, bo gdzie majestat knezia Warczy­sława jest przytomny, tam jeno z jego zezwolenia kupa właścicielowi odjęta być może – surowo setnik Bruno wyrzekł i na zbrojnych kiwnął.

Przemocą odbierać sakiewkę trza Praśle było, bo o sprawiedliwości ruchawko­wej a prawach jaśminowych wykrzykiwał, pazury drapał, a nawet zębem jedynym, co go czemuś kat oszczędził, wokoło kąsał zajadle. A że pieniążka jednego z sakiewki po­łknąć zdołał, toteż zbrojny który, by gorliwość przed majestatem pokazać, palic w zad jemu wraził, gaciów wprzódy upuściwszy i cosik z onego zadu wydłubał. Przyjrzał się palicowi Sigurd, setnik i gromko zaryczał:

– Zdrada! Zakalcy, zdrada! Oto wódz wasza z kupa uczyniona!

Próżno Prasło napominał gromko, iż nijaka zdrada, ino profanacyja się dzieje, bo miłość ku mądrości jest szargana wzniosła, kiedy zbrojne prostactwo kupę metafi­zyczną ze sraniem przaśnym przyrównuje. Porwali go woje na mądrość nieczuli i do ciemnicy unieśli, zasię majestat, zgorszeniem wzburzon, gościom za przyjście podziękował.

\*

Kiedy jeszcze pachołów, dziewki a straże majestat wygnał, znowuż sami ostali w izbie ci, co śniadać rozpoczęli.

Warczysław, kneź, ożywionym być raczył i po klepisku kroki stawiał zamaszyste. Wsze­lako o gościnności nie przepomniał.

– Pojedliście zacnie? – brata Michała i Obijmordę zapytał – usłużono wam w onym kącie ciemnym?

– Barzo wystawnie żeśmy pojedli – dwornie brat Michał odpowiedział – choć rzec trzeba, iż gardziele nasze ściśnięte były ode krępacji, bo względów monarszych w żaden sposób nie jesteśmy godni. Wszelako nikto na to nie zważał i nie dość, że nas karmiono, to jeszcze bliskością swoją osoby najgodniejsze nas zaszczycały.

– Aleście z niemi nie gadali? – upewniał się kneź Warczysław, brwie marszcząc surowo.

– Nie dla pogaduszek wielmoże w ciemnotę leźli, jeno dla ulżenia sobie – mnich westchnął – toteż aby raz czy dwa ten oto człek, co Obijmordą jest zwany, słówko grubsze wypuścił, kiedy go zmoczyli niechcący. Ale po cichu to czynił i nie w złości, podobnież jako i zacny Łamignat, dziesiętnik, co podobnych przygód doświadczył. Jam wedle ściany siedział szczęśliwie za ławą, przeto też suchy jestem.

– Tedy dobrze wszytko – majestat rzecz zakończył. – Teraz do tego przystąpmy nareszcie, od czego nas goście nieproszone odwiedli. A ty, Sigurdzie, setniku, przykaż strażom, by wierzeje w częstokole zawarły i nikogo ku nam nie wpuszczały, chyba by wojna była. Ino sługi co zwykle robiące niechaj się bez przeszkody kręcą. I tak do rana samego. A gdy kto, osobliwie łachmaniarz Prasłowy który, naprzykrzać się będzie nad miarę, niech mu straż do zadu ogon przyda ze strzały beze grotu uczyniony.

\*

Radźmy przeto – ozwał się Chichrała, trefniś, jako miał we zwyczaju – wszakoż dobrze od tego począć by było, żebyśmy sobie przedstawili, nad czym właściwie uradzać chcemy i ku czemu rady nasze prowadzić mają. Takie przedstawienie jeno ten uczynić jest władny, kto rajcom przewodzi, a jedyny Warczysław, kneź, za takiego uchodzić może.

– Dobrze zatem – ozwał się majestat – przedsięwzięcie lustracyjne to jedno, zasię dola gościów naszych, Obijmordą i Michałem zwanych, to drugie, nad czem debatę czynić będziem. Każden tego jest świadomy, jeśli czemuś nie ogłupiał do szczętu, iże sprawy owe mocno się łączą. Wiem z gruba, ku czemu dążyć bym chciał, lecz tego wam teraz gadał nie będę, byście rozumami ruszać giętko na wsze strony nie przestali. Ci z was, co są dworaki stare, i tak w sposób niepojęty życzenia kneziowe zawdy wyczuwają.

Trefniś Chichrała a też Bruno i Sigurd, setnicy, co stare dworaki byli, po sobie wejrzeli, zęby szczerząc.

Kneź Warczysław, ku wesołkom łypnął krzywo, za czym na gębę dostojność naciągnął i mową tronową dalej ciągnął:

– Cieszę się, kiedy rajce moje pogodę pokazują, bo bywa czasem, że to wyrokom dobrze wróży. Wszelako napominam, byście pamiętali, iż słowa a czyny wasze powagę monarchii podpierać mają, nie zaś krotochwilom służyć. Kiedy o racyję stanu chodzi, cnota nakazuje, iżby przychylności wszelakie a skłonności ku temu czy owemu na bok odłożyć stanowczo. Rajcom doświadczonym jeno o tem przypominam, a nowym za prawdę przedstawiam. Osobliwie do was się zwracam, więźniowie moi, byście stan swój jeniecki zapomnieli i tam się w słowy wznieśli, kędy prawda, rozum i cnoty jedyne królują, zasię pozór czyli też omam, chucie cielesne a przywary dostępu nie mają. Zatem na racyję stanu patrzajcie, nie na dolę swoją.

Teraz Chichrała, trefniś, ku jeńcom się nachylił.

– Objaśnię was, mili moi, iże kneź nasz, tu przytomny Warczysław, takie ma pragnienie i one w życie wciela, iżby rządy jego najwiętszą w chrześcijaństwie sprawiedliwością zajaśniały, a też by nad ich postanowień trafnością ludzie w świecie całym głowy swojemi z podziwieniem potrząsali. Osobliwie o to stoi, by zamiary wszelakie roztrząśnięciem mądrym poprzedzane były, jako dla przykładu, do lustracyji zbójectwa przystąpienie.

Majestat i insi łby czemuś opuścili, ino Brunon, setnik, oczyska swoje podniósłszy, z powały myśl wziął:

– Takie rządów jest przyrodzenie, iż raz migiem co uczynić trzeba, zasię razu innego czas jest, aby czyn debatą wnikliwą poprzedzić. Kneź nasz, sławny Warczysław, na oba sposoby doskonale jest w rządzie wyćwiczony.

– Powiadają niektórzy, iż kto na boki wybiega, ten późno dobiega – szybko brat Michał się wtrącił. – Ja zasię mam zapytanie, czy ów, co się racyją stanu kieruje, też prawdę szanować musi?

– Wiedz, bracie Michale zacny – majestat zapewnił – iż w dziedzinie mojej nic tak jak prawda cenne nie jest i nie będzie.

– Kromie ziem ojczystych szczęśliwości – Chichrała, błazen, uzupełnienie dał.

Rajce łepetynami pokiwali w zgodzie, zasię Brunon, setnik, to dodał jeszcze:

– Czem swój głos w radzie tłumaczysz, bracie Michale, jak nie tem, iże o sprawiedliwą prawdę tu przytomnym chodzi? Niebywała to świecie całym rzecz, kiedy jeńce na równi z tymi debatują, co ich w swej mocy mają.

– Radźmy – ponaglił Chichrała, błazen – A palców trzech u prawicy zagnijcie, dostojni, byście o tym pamiętali, iż ostatnich wypadków przedstawienie takie dla ludu prostego umyślić nam trzeba, co by ze świadectwem gapiów, prawdą rzetelną i dobrocią krainy umiłowanej w zgodzie przykładnej było.

\*

– Woj barzej na orężu polega, niż na narada – wyrzekł Sigurd, setnik. – Ale wódz, chocia woj, naradować potrzebuje kiedy. Jak Bruno powiada, kneź jedno migiem robić musi, inne onemu robić po namysłu wypada. Pytam, co w sprawa tu omawiana lepsze jest, migiem stanowić czy po namysłu?

– Cóże o tem mniemasz, Obijmordo, wędrowcze? – zapytał kneź Warczysław ciekawie.

Zapytany bary wzruszył.

– Kto kneziem jest, czynić wszytko może jako się jemu podoba. Migiem dobrze będzie i po rozwadze dobrze będzie, kiedy tako kneź umyśli sobie. Także samo, co się jemu podoba, w sprawie stanowić może, a na gadanie ludzkie baczyć nie musi. Gdybym kneziem był, to bym uczynił, co bym sobie zachciał.

– Jako poddany wierny powiadasz i sprawiedliwie – majestat pochwalił – i łacno za wzorzec człeka pospolitego mógłbyś być stawiany. Wszelako to żeś pewnie słyszał, iż władce wielmożów spomiędzy poddanych wywyższają, iżby owi z nimi trud rzemiosła rządowego dźwigali. Mają oni wielmoże nie ino tylko orężem, ale i radą im służyć. Skoro tak, to widać, że władce za wygodę swoją to mają, kiedy im kto zamysły podsuwa.

– Poddanych wysłuchując, władce uważanie onym pokazują i tak miłość ludu ku sobie wzbudzają, a gdy w jakim księstwie takowa miłość kwitnie, to w niem wszytkie sprawy gładko idą i bez zwłóczenia – setnik Brunon dodał lubo.

– A że na padole niedoskonałym życie nam przyszło pędzić – trefniś Chichrała ze zgryzotą westchnął – co raz to się przydarza, iż zamysły rządowe najświetniejsze diabłowie przekornie na łajna warte obracają. Kiedy co dobre być miało, w lada jakie czemuś się obraca, osobliwie zasię naonczas, kiedy nakazania światłe miast szczęśliwości zło sprowadzają i klęski a utrapienia rodzą, lud miast wyrozumienie pokazować sarkać poczyna i tako jakoś się stawa, iże wszytkie inne sprawy takoż się gmatwają a też dłużą. Natenczas dobrze jest rajcę nieudolnego na dół strącić, abo też na paliku zatknąć. Nic tak ludu nie weseli i miłości pospólnej nie przywraca.

Obijmordą zwany widać pożytki z radzenia pojął, bo oporu poniechał i do rzeczy powrócił: – No to na początku bym rzekł, iż tego, czy migiem, czy po namyśle lepiej bywa co uradzać, nikto z góry pewnym być nie może – wyrzekł. – Takoż jest i teraz pewnikiem. Bo też, o tem radzeniu, co je tu akuratnie czynić mamy, myśleć by można, że namyślenie barzej niźli skorość sprzyjać będzie owych celów utrafieniu, jakie wielmoża Chichrała dopiero co ukazał. Inaczej wszelako rzecz wygląda, kiedy pamiętać, iż mowy gościów sławnego Warczysława, księcia, o tem zaświadczają, iże mnogość sposobów się pokazało, na jakie poddani rzeczonego monarchy rozumieją przypadki, o jakich uradzać mamy. Przy takim myślów rozbieganiu może lepiej migiem jedno rozumienie wszytkim narzucić, póki się samo po ludziskach jakoweś całkiem inne nie utrze, co się z prawdą rzetelną i racyją stanu rozminie.

Majestat w dłonie zaklaskał.

– Oto się pokazało, jako zgrabnie Obijmorda, osiłek wędrowny, słowa składać potrafi i żem dobrze utrafił, kiedym go na rajcę wyznaczył. Cieszy mnie to i nadzieją napawa, iże radzenie nasze pięknym ćwiczeniem rozumów stać się może. Wielka to rzadkość pośród ciemnoty wszędobylnej, by móc się rozkoszować blaskami mowy krasnej a racyjów światłych zmaganiem. Już to za namysłem przemawia, a skorości przeciw.

Brunon, setnik głos dał na to:

– Nic to nowego, kiedy majestat Warczysława, knezia, w sedno utrafia, rzeknę nawet – wybaczcie zuchwalstwo – iże pospolitym to się stawa. Przeciw skorości a za namyśleniem to jeszcze podsunę, iż nie wiada, czy to, co się pośród poddanych pouciera, przeciw prawdzie i racyji stanu będzie, równie przecie na odwrót być może. – Tu głosu obniżył i cicho pociągnął:

– Barzej wszakoż mnie to zajmuje, co z ust Zbysława padło, stryjca kneziowego. Kto inny za gadanie takie na ćwiartowanie łacno mógłby być wydany. Czy rzeczony książę tako prawi, bo mu ode starości łepetyna osłabła, biedaczynie, czy też może tron wywrócić zamierza? Zdawa mnie się, że jest przewaga knezia Warczysława w dziedzinie, wszelako kneź Zbysław czuć inaczej może, abo też nie zważać na to mniemając, iże czas przeciw niemu płynie i stąd dla niego wóz albo przewóz… Czy nie dobrze będzie baczyć, jako się sprawa rozwija? A nóż ku temu pójdzie, iż Zbysław, chucią panowania omamiony, sposobność dla się dojrzy złudną i do zdrady przystąpi, jawnie złość swoją występną ludowi pokazując, przez co się nareszcie sprawy przemogą i gwiazda Warczysława jako jedyna nad dziedziną naszą zajaśnieje?

– Jako ty to pojmujesz, Piącho, setniku? – Warczysław, kneź, w zamyśleniu zapytał, po izbie tu i tam kroki stawiając.

– Po mojemu, jeśli się stanie wedle Bruna, setnika, to kneź Zbysław gadaniem o krzywdzie tych oto Obijmordy a Michała, mnicha, lud popróbuje burzyć, a wonczas my rzeczonych Obijmordę a mnicha takimi ludowi pokażemy, że się łgarstwo Zbysławowe odkryje jawne, przez co lud za nami stanie i się łeb zdrajcy obetnie… znaczy, Zbysławowi, kneziowi, oby w szczęśliwości żył, łepetynę się z uszanowaniem odejmie.

Przytomni z uznaniem pokiwali głowy nad łaską Warczysławową, co Piąsze, drużynnikowi, kromie podniesienia stanu rozumu rządowego przydała jednocześnie.

– Zatem nie migiem ino po namysłu naradowamy – rzecz Sigurd, setnik, zakończył.

– Byle ino diabłowie psotni mądrych zamysłów naszych na łajno nie obrócili – mruknął Chichrała, trefniś po cichu.

\*

Brat Michał na zydlu się zakręcił.

– Tego się boję – wyrzekł – iż na manowce radzenie nasze się zbłąkało. Oto choć prawdę, świadectwa i dobroć dziedziny Warczysławowej godzić zamierzyliśmy, zdawa się mi, że przede chwilą na onej dobroci jeno żeśmy się skupili. Tym to zagraża, iż co wedle racyji stanu uradzimy, prawdzie i świadectwom na przekór stanąć może.

Majestat Warczysława, knezia, wyrozumiale się uśmiechnął.

– Obawę twoją, bracie Michale, pojmuję, choć onej z tobą nie dzielę do końca. Wiele roków z rządem obycia to rozumienie przynosi, iże prawda i świadectwa zazwyczaj się do racyji stanu przypasować dają.

– Powiedzieliby mędrce którzy, iże jeno Bóg sam byłby w możności za każdem razem pogodzić prawdę i świadectwa z korzyścią władcy – mruknął brat Michał z przekorą nieśmiałą.

– I tuś utrafił! – majestat wyrzekł – bo zauważ, jak niezawodnie cię myśl twoja ku wielkiej prawdzie stanu prowadzi, iże opatrzność niebieska nade myśleniem książęcym i książęcych rajców czuwa, natchnieniem swoim tychże napełniając; bo przecie kiejby opatrzność nie czuwała i nie napełniała, skąd by to się brało, że zawdy prawdę, świadectwa i dobroć monarchii zgodzić rajce książęcy potrafią?

Bruno, setnik, podobnie sprawy widział, bo dodał:

– Opatrzność nade monarchami czuwa, stąd co dla nich dobre, to też prawdziwe jest i oczywiste zarazem. Wystarczy racyję stanu rozeznać, a potem się pokazuje, że prawda i świadectwa nagle zgodne z racyją stanu się jawią. Trzeba ino pozory a omamy rozproszyć, do czego wstrząsanie świadkami jest pożyteczne.

Tu majestat wątek uciął, powiadając łaskawie:

– Ale rozumiemy, że brat Michał i Obijmorda w rządach nieobyci, woleliby od prawdy i świadectw poczynać i do racyji stanu później dochodzić – miasto odwrotnie. Może i tak być przecie. Zali mogę w tym nadzieję położyć, że porządek taki zbytniego trudu w radzeniu wam nie przysporzy, rajcowie moi?

– Nijakiego utrudzenia w życzeniów twoich spełnianiu nie widzim, kneziu Warczysławie jasny – Piącha, setnik, gładko się ozwał za wszytkich i przez to świadectwo dał, iż do rady książęcej dojrzał już całkiem.

\*

– Jako tedy prawdę widzisz, bracie Michale zacny – ku jeńcowi się majestat zwrócił, zasię wszytcy inni uszów pilnie nadstawili.

– Powiadają mędrce którzy, iże by prawdę odsłonić, od pewników zacząć trzeba – napoczął brat Michał kazanie swoje. – Jakie tedy one pewniki są? Otóż mnie się barzo pewne wydawa, iżeśmy z tym oto, co Obijmordą zwany, peregrynowali w pokoju, nie wadząc nikomu. Pewne takoż, iże nam się w drodze wydało z nagła, jakobyśmy przeze zbójcę zawziętego opadnięci zostali. Tyle pewnego na początek rzec mogę. Zaraz potem chętnie wszelako przyznam, że choć pewne wydać się może, że się człekowi cóś wydawa, to jednakowoż to, co się wydawa, pewne nie musi być wcale, bo nikt nas nie upewni, iże to, co zmysły podsuwają, akuratnie omamem nie jest. Toteż mędrcy na rozumie i wierze polegać wolą, jeśli jednakowoż na świadectwie zmysłów oprzeć czują się zmuszeni, rzeczone świadectwa roztrząsają pilnie, najchętniej widząc jako się one z rozumem i wiarą zgadzają, ale też szukania nie zaniedbując, czy aby im kłamu nie zadają, abo też odwrotnie – czy przeze one podważane nie są. Takie to rozstrząsania przedsięwziąłem i to z nich wyszło, iż nic przeciw temu, żeśmy jako baranki niewinne gościńcem peregrynowali bez grzechu, świadczyć się nie wydawa. Wszelako w tym, że od zbójcy byliśmy opadnięci, omam być musi, jako że to się jawnie pokazało, iż zbójcy w okolicy nie było, jeno Warczysława, knezia, najdostojniejsza osoba tam przebywać raczyła. Dalsze wnioski w krótkości podam, by dostojności tu przytomnych nie unudzić. Owóż nie wszytko było, jako się zrazu wydawało, wszelako winy ludzkiej upatrywać w tym nie sposób, jeno omamu, którym najpewniej diabłowie zawiadują, a skoro tak, to sprawiedliwie będzie, jako się w pokoju rozejdziemy, zasię władze biskupie okolicę, gdzie przygoda zaszła, wodą święconą potraktują.

Przytomni wdzięcznie kazanie przyjęli przez brata Michała wygłoszone i nade kunsztem mowy krasnej onego zacmokali. Sam majestat dwa razy dłoniami klasnął.

– Pochwalić mowność wypada – ozwał się – a też i to, że prawdziwie słowa mówcy zabrzmiały.

– Amen – ukontentował się brat Michał poczciwie. – Mniemam przeto uniżenie, iżem trudu wam, dostojni, swym kazaniem oszczędził, bo to, com w nim powiedział, jawnie jest z prawdą i świadectwem zgodne. A i dla dobroci dziedziny Warczysława, księcia najdoskonalszego, rada moja zbawienna, jako że puściwszy nas w zdrowiu i diabelstwo tępić każąc, władca rzeczony łaskawością, sprawiedliwością i pobożnością na świat cały zasłynie.

Rajce znowuż zachwyt dla wywodu objawili, ręcyma przyklaskując.

\*

– Otośmy pomysł zacny usłyszeli – ozwał się majestat – dla porządku wszelako zapytam, czy aby kto innego nie ma?

– Że zacny pomysł brata Michała, po tem poznaję, iż jak palce jeden po drugim odginam, zasady, cośmy ich sobie przestrzegać obiecowali, zachowane przeze niego się wydają – ozwał się Chichrała, trefniś, pomału. – To wszakoż trapi, jakie to myśli gawiedź z takiegoż rzeczy przedstawienia wywieść zdoła.

Tu rajce czoła pomarszczyli, ku górze oczyska podnosząc.

– Wybacz mi, dzielny Łamignacie, dziesiętniku – wyrzekł po chwili Warczysław, kneź, z serdecznością wielką – a nie myśl aby, że ci stan twój nikczemny wypominam, wszelako zda mi się, iżeś z tu przytomnych człek najprostszy. Jako ty byś przypowieść brata Michała pojąć zdołał?

– Że kneź nasz, Warczysław, kiej łowić się zbójców wybrał, na wędrowca się natknął niewinnego, co jemu po łbie nakładł, bo majestatu nie był poznał omamem zaślepiony – odrzekł zapytany, prostactwo wedle przykazu objawiając wiernie.

Pokrzywili się rajce na obraz takowy.

– Raz jeszcze przenikliwość ta się pokazała, co z niej majestat Warczysława, księcia, po świecie słynie – mruknął Brunon, setnik, wzdychając – bo niezawodnie prostaka książę wyczuł w Łamignacie, dziesiętniku. Wszelako przymioty poczciwego Łamignata na bok odkładając, myśl smutną wyrazić wypada, iż już o to nieprzyjacioły nasze zadbają, by ciałem jego słowa grube się stały i takie właśnie wypadków przedstawienie na lewo i na prawo z gąb swoich wypuszczać będą.

– Miło to lud słucha, iże kneź łaskawa i pobożna, wtedy bo ciężary a prawa łamać się nie boją ludkowie. Lud wiedzieć muszą, co taki kneź jest: jak on zamiar ma, uczyni to; jak postanawia, tako się stawa. Że kneź zbójce łapać chce, to łapie, beze zbój do dom nie wraca – zahuczał ni stąd ni z owąd Sigurd, setnik.

– Gapie to rozpowiedzą innym, iże osiłek wędrowny na majestat z pałą był nastąpił, a nikto tego nie wyrzecze, iż się onże na omam zamachnął – smętnie dopełnił Piącha, setnik świeży.

Chichrała, trefniś, w pomieszaniu pięścią w kolano huknął.

– No i palce zagiąć na powrót trzeba – wyrzekł z żalem.

\*

– Ode początka tako myśleć było trza, miast bez potrzeba plątać a motać – Sigurd, setnik, głosem gromkim kuł żelastwo póki gorące. – Ode tego zacząć było, iże lud takowe przedstawienie słuchać musieć ma, jako kneź sławna, Warczysław, zamiar swój wszytki spełnił, bo zawsze cela sięga. Wybrał się Warczysław, kneź, zbóje lustrować, no też zbój ułapany przeze niego. Kasztelany gnuśne ode roków nijaki zbój nie straciły, a tu majestat w pół dnia zbrodzień ogromny spętał i cherlak ze zgrai, co przeszpiegi czynił. Olbrzym Oijmorda na widowisku się poćwiartowa, niech lud uciecha ma, mnich cherlak łaskę dostanie jako kamratem przymuszony do niecnoty. Jak kneź ulubił mnicha, niech i tak będzie. Abo mnichowe ucha abo nos się zgoli może. Lud to obaczy, co Warczysław, kneź, władca surowa i łaskawa, przecie lud i łaska, i siła szanowa barzo.

– Barzo zgrabnie żeś to umyślił, Sigurdzie, setniku roztropny – pochwalił majestat, rozumem kiwając – A ty co o tym mniemasz, braciszku Michale?

Zakonnik jakby czegoś nosem kręcił.

– Oponować trudno przeciw temu, co sam majestat zgrabnym nazywa – mruknął – wszelako zasady wspomnijmy, które był wielmożny Chichrała, trefniś, tak roztropnie ustanowił, a też słowa samego Warczysława, knezia najjaśniejszego, prawdę dobrem najpierwszym mianujące. Owóż całkiem zgrabnie jawiłoby się przedstawienie przemyślnego Sigurda, setnika, gdyby nie to, iż z ową prawdą się mija, żeśmy z tym oto, co jest Obijmordą zwany, poczciwie peregrynowali, nie zaś zbójowali, a co za tym idzie, sławny Warczysław, kneź, nie zbrodniów bynajmniej, lecz pielgrzymów raczej w pęta wziął.

– Barzo mało oględnie językiem obracasz, braciszku miły – Chichrała, trefniś, westchnął – bo jakby kto słyszał, pomyśleć by mógł, iż mniemasz, iże tu przytomny majestat Warczysława, pospolicie sprawiedliwym przezywanego, nie zbrodniów ino pielgrzymów pętać ma we zwyczaju. Za mniejsze obrazy ozory się potwarcom odejmuje.

– Jako kneź kogo pęta, to on zbrodzień jest, nie wędrowiec poczciwy – bąknął Łamignat, dziesiętnik, bo oko Warczysława, knezia, na niem zawisnęło.

– Racja twoja, bracie Michale, na tem się zasadza, iż za pewnik bierzesz, żeś nie zbójca jest, jako też i ten oto, co Obijmordą zwany. A skąd ona pewność, bracie? – Chichrała, błazen, pytanie zadał.

– Wiem, bom swoich postępków świadomy, jako i postępków Obijmordą zwanego – brat Michał burknął. – A dodam, że postronni, co myśli naszych znać nie mogą, poczciwość naszą stąd wyprowadzać winni, iż sprawiedliwość zasadę szanować każe, co mówi, iż jak się winy czyjej nie jest pewnym, niewinność tegoż uznawać wypada.

– Pierwsze słyszę, by w świecie chrześcijańskim nas otaczającym kto takie maksymy głosił – Chichrała się zdziwił.

– A mnie zasada rzeczona barzo się podoba – ozwał się Warczysław, kneź – toteż słyszeć chcę zaraz, co poczciwości brata Michała i człeka Obijmordą zwanego jawnie abo ogródkiem zaprzeczać może.

\*

Teraz na Chichrałę, trefnisia, wszytcy baczenie przenieśli.

– Przeto radźmy, jako majestat Warczysława, knezia naszego, był nakazał – wyrzekł onże. – Oto dwa pomysły roztropne mamy: jeden Michała, mnicha, drugi Sigurda, setnika, przyjaciela naszego. Kazanie brata Michała zdało nam się i prawdziwe, i z gapiami zgodne, a też dobroć dziedziny Warczysławowej zachowujące. Wszelako w tym jest utrapienie, iże błędnie pojęte przeze gawiedź będzie, w czym to nas niezawodnie upewnia, iże barzo dobrze znamy one chytre a przeniewiercze sposoby, na jakie nieprzyjacioły nasze knowają, a też prostoty ludu naszego poczciwego dogłębnie żeśmy są świadomi. Rzetelnie też prostotę rzeczoną zbadaliśmy w osobie dzielnego Łamignata, dziesiętnika, występującą, a próba owa obawy potwierdziła. Przeto też pilnie myśl Sigurda, setnika, zważyć musimy, czy aby lepsza nie jest. Może i z żalem to przedkładam, rajcowie zacni, bo na pierwsze wejrzenie myśl owa mniej zda się miła dla brata Michała oraz człeka Obijmordą zwanego, wszelako pewien jestem, iż goście nasze prawdę, rozum a monarchię nad marność życia doczesnego przedkładają.

– Wszelako jeśli dobra najwyższe z dobrami doczesnymi dają się pogodzić, to jest to stan tronowi najmilszy – ozwał się majestat barzo po chrześcijańsku. – Zalecam przeto, iżby sposobów pogodzenia takowego szukać.

– Ale w porządku radźmy, ode zbadania prawdziwości kazania Sigurdowego poczynając, a sprawy insze a pomysły później rozbierzemy – stanowczo Chichrała, błazen, oznajmił. – Jest li kazanie rzeczone prawdzie podobne?

Rajcowie po rozum do głów poszli, na chwilę milknąc.

– Jako dwa wodne krople podobne – orzekł Sigurd, setnik, basem, a inni przytaknęli onemu.

Jeno brat Michał włosa na czworo dzielić umyślił:

– Zanim o racje szczególne zapytam, o to bym upraszał, by mi kto w ogólności wyłożył, jako to być może, by tę samą przygodę różnie opisać, a oba opisy prawdziwe były? Wszak wielmoża Chichrała, trefniś, prawdy opisaniu mojemu nie był odmówił, zasię teraz oną prawdę przedstawieniu dostojnego Sigurda, setnika, równie chętnie przyznaje.

– Mownyś, uczony i rozumny, braciszku Michale, wszelako widać, żeś w rzemiośle rządowym obyty nie jest – życzliwie majestat westchnął. – Gdybyś obytym był, to wiedziałbyś, że prawdów tylu jest, co i w dyspucie czynnych głosicieli. Mamy ci w dziedzinie naszej w sprawie każdej prawdę Warczysławową obok prawdów Zbysławowej, Guzdrałowej i Matyldowej. A często gęsto inne się trafiają.

– Wszelako Kościół, matka nasza, że prawda jedna jest, głosi wiernym ode wieków – bronił brat Michał swojego.

– Toteż cierpliwie Warczysławowi, księciu naszemu, to przedkładamy od roków paru, iżby tym, co inne prawdy głoszą, sprawiedliwie jęzory poodejmował, jako to się pospolicie w chrześcijaństwie czyni – westchnął Brunon, setnik – jeno że przyrodzona rzeczonemu księciu łaskawość miłosierna na przeszkodzie póki co stoi. Cieszy mnie, bracie Michale zacny, iże stronnika w tobie znalazłem w przedłożeniach moich.

– Do rzeczy proszę, do rzeczy – Chichrała, błazen, przerwał. – Skoro już prawdę w kazaniu Sigurdowym dostrzegliśmy, dalej iść trzeba, jeno że brat Michał grzech wątpienia objawia, cierpliwość naszą chrześcijańską próbując. Czego znów nie pojmujesz, bracie miły?

– Owóż wydawa się, że przedstawienie peregrynantów poczciwych z przedstawieniem zbójców krwawych w zgodzie nijak być nie może, jeśli tych samych śmiertelników dotyczyć mają. Czy tak niezgodne przedstawienia równie prawdą być mogą?

– Oj, bracie Michale, bracie Michale! – Chichrała, błazen, palcem pogroził. – Jako Warczysław, kneź mądrości pełen, słusznie był zauważył, rozumnyś i kształconyś, co z kazań twoich jawnie wyziera. Teraz wszelako zagadki nam podtykasz na głupstwie zasadzone, na to pewnie licząc, iż mniej bystrymi będąc ode ciebie, zęby na onych połamiemy, a zaraz potem racyję twoją przyjmiemy bezradnie. Rozumiem to ja, iże do kaźni nie jest ci pilno wcale, wszelako smutkiem mnie to napawa, kiedy mędrzec nad wzloty rozumu przywiązanie do lichego cielska zdaje się przedkładać. Jednakowoż nie będę sędzią twoim w tej mierze. Wieczór, zda się, nadchodzi – o jakżeż ten czas bystro płynie – przeto na palcach migiem argumenta tobie wyłuszczę. Wiesz z pewnością doskonale, iż przedstawienia niezgodne wtedy jeno prawdą razem być nie mogą, kiedy sobie przeczą. I tu palec pierwszy: nic nie wadzi temu, by zbójce peregrynowali, przeciwnie, rzemiosło zbójeckie do peregrynacji skłania, przeto też zbójca peregrynantem być może, jak też i peregrynant zbójcą. Palec drugi: zwany Obijmordą z pałą na gościńcu na innego nastąpił, a takowi, co na gościńcach na innych następują, słusznie mogą być za zbójców brani. Palec trzeci: ci, co ze zbójcą w porozumieniu peregrynują i czynom zbójeckim się nie przeciwią, sprawiedliwie zbójcami mogą być nazwani. Palec czwarty: jeśli nawet kto jest w prawie, iżby poczciwym się przezywać, bo świadków pokazać może poczciwości swojej, po czynie zbójeckim na ścieżkę grzechu wkracza i poczciwość traci; tedy człek ten sam poczciwym peregrynantem musi być nazywany do czasu, kiedy w grzechu zbójectwa się pogrąży, a od pory owej miano zbójcy krwawego słusznie mu się należy. Bracie Michale, azali dalsze palce mam odginać? Bo mniemam, że ślepce nawet widzą, iże przedstawienie twoje Sigurdowemu niekoniecznie przeczy. Możeście bez grzechu peregrynowali nie wadząc nikomu, wszelako na majestat knezia Warczysława nastąpiwszy z pałą, zbrodniami żeście się stali.

\*

I tu ku bratu Michałowi się zwrócił. – Gapie to uwidzieli, że bitka była, co równo twoje, braciszku Michale, przedstawienie potwierdza, jak i przedstawienie Sigurdowe. Czy ty, czy on prawdę mówicie, od tego najbarzej zależy, kim jest zwany Obijmordą i co zamierzał, do bitki przystępując. Kto tobie uwierzy, niewinności waszej pewien będzie, kto zasię Sigurdowi posłuch da, za zbrodniów będzie was miał. Dla jednych przedstawienie twoje prawdę wyrażało będzie, dla drugich przedstawieniu Sigurdowemu ten zaszczyt będzie się należał. Sam Bóg jeno wejrzeć w mroki duszy Obijmordowej władny, aby dojrzeć, czy się tam chęć zbójecka skrywa. W tym rzeczy stanie sędziowie sprawiedliwi łacno przyznają, że równie prawdziwie brzmią przedstawienia oba, póki dodatkowe argumenta szali na stronę tę czy drugą nie przeważą. Kto krasno mówić lubi, to rzeknie, że dwie prawdy mamy. A teraz wspomnijmy na zasady nasze, cośmy ich przestrzegać mieli: gapiów świadectwa i dowody prawdy nijakiego rozstrzygnięcia nie dają, przeto też to przedstawienie uznać należy, co barzej dobru dziedziny miłej służy. Póki co, pode onym względem Sigurda kazanie lepsze się wydawa.

Pokazało się, iż u brata Michała kromie cnót rozlicznych ułomne pieniactwo niekiedy do głosu dochodzić może.

– O onych argumentach dodatkowych chciałbym słów kilka rzec, jeśli pozwolicie, dostojni – wyrzekł. – Co będzie, jak na szali świadectwo położymy, co je majestat niezrównany Warczysława, knezia, wystawić raczy? A o tym będzie świadectwo ono, iż rzeczony Warczysław peregrynantów poczciwych opieką swoją otoczyć zapragnął, w pęta na niby biorąc, iżby zbrojnych kasztelańskich zwieść? Mniemam, iż prawdziwości to kazaniu mojemu barzo w oczach sędziowskich przyda.

– Ale dobro dziedziny ucierpi barzo, a nieprzyjacioły nasze łby przebrzydłe wysoko podniosą, kiedy się pokaże, iże lustracyja zbójectwa się nie dokonała – wyrzekł Chichrała, trefniś, bez cierpliwości. – A na to też zważcie, mili, że krotochwilne zamiary Warczysława, knezia naszego, nijak się do rzeczy nie mają, bo co temu na przeszkodzie stoi, by chcąc peregrynanta lada jakiego spętać, na zbójcę trafem się natknąć?

– Wydawa mi się, iże brat Michał uporczywie tkwi w tym błędzie, że jeśli zarzut zbójectwa zbije, to niewinność swoją i Obijmordą zwanego wykaże bez wątpienia – rzekł Bruno, setnik, wzdychając smutno. – Zaprawdę, bracie Michale, powiadam ci, iż dla was za zbójectwo być sądzonym lepiej, jako że Obijmordą zwany na majestat z orężem nastąpił, a to już stanu zbrodnia. Każden oprawca doświadczony ci to powie, iż nade zbójcą ani ćwierci potu tego nie uroni, co nade zbrodniem stanu wyleje.

– Tedy na to wychodzi, iże zbójami was ogłaszając, jużeśmy przyjaźń wielką wam okazali, bo wszak za zbrodniów stanu sprawiedliwe moglibyście zostać wzięci – ozwał się znowuż Chichrała, trefniś. – Jak my z przyjaźni zbrodnię stanu litościwie skryjemy, tak wy, tąże samą przyjaźnią się powodując, do czynu zbójeckiego się przyznajcie, iżby Warczysław, kneź nasz przesławny, lustracyją zbójectwa mógł się przed ludem i możnymi pochwalić. Bez nagrody, mili, to się nie ostanie.

– Jak mówią, i wilk syty, i owca cała – wyrwał się Łamignat, dziesiętnik, z krotochwilą nie barzo stosowną i tym prostotę swoją niezrównaną potwierdził raz jeszcze.

– Żebyż ona owca prawdziwie całą się ostała – jęknął brat Michał wymową rajców pozostałych przytłoczony.

Warczysław, kneź dobrotliwy, poklepał go łaskawie po ramieniu chudym.

– Pókiś żyw, póty jeszcze wszytko może się odmienić. Kto w rzemiośle rządowym bywały, przy takich już obrotach spraw abo był przytomny, abo też wieści o nich miał, jakie by najtęższym bajarzom do głowów przyjść same nie mogły. Rzeknę ci, braciszku, iżem cię pokochał sercem i gdyby nie gapie, a prędzej wrogi obmierzłe, co z onych gapiów korzystają, ręcyma obydwiema bym na nastąpienie Obijmordowe machnął i o niem dawno już przepomniał. Razem z tymi tu przyjacioły nad tem pracować będziemy, iżby was z matni wybawić, jeśli jeno stanu racyja, prawo i prawda rzetelna na to nam zezwolą. Jeśli nam nie zezwolą, płaczem was pożegnamy, by w sercach naszych jako towarzyszów zachować, jako że i my wszytcy, jako tu stoimy, w chwili każdej za dziedzinę ojczystą lec żeśmy gotowi.

– Amen – przytwierdzili towarzysze kneziowe prawdziwie miłością ku dziedzinie przepełnieni.

**Księga druga**

Brunon, setnik, nastrój podniosły zakończyć umyślił i pogodą zastąpić.

– Jak to mówią, nosy w górę! – przyjaźnie ku jeńcom się ozwał – Zawszeć może się pokazać, iż to Dłubała, rzemieślnik, głosem ludu przemówił, a wonczas a nuż takowe się przedstawienia i gesta obmyśli, iże do drużyny Warczysławowej sławnej a wesołej przystąpić zaszczytnie będziecie mogli. Przecie tośmy uradzili, iż na dalsze przypadków postępy czekać będziem. Po prawdzie, na oko moje, to pewnikiem wam najbarzej póki co szkodzi, iż kneź Zbysław zda się za dolą waszą ujmować, a przecie przeciw stanu racyji byłoby, kiejby Zbysławowe na wierzch wyszło.

– Toteż płakać za wcześnie, lepiej jedzmy, pijmy a weselmy się – przyłączył się Piącha, setnik świeży, co mu się jeszcze uczty nie przejadły.

Warczysław, kneź, z przyrodzenia swego dalej widać niźli inni pozierał, bo o czem innem napomknął znienacka:

– A tak, po prawdzie – zapytał – gdzie to się wojewoda Ulryk podziewa, wszak już prawie wszytcy ludzie znaczni nas odwiedzili, zasię o Ulryku ani słychu?

Przytomni w siebie wejrzeli, wszelako niewiele tamże znaleźli, bo na dłuższą chwilę pomilkli.

– Może słabuje? – niepewnie Chichrała, trefniś, popróbował co odrzec.

Nie doszło wszakoż do radzenia nad Ulryka, wojewody, niebytnością tajemną, bo do izby wbiegła chyżo Ulca, księżna, najwyraźniej za Warczysławem stęskniona, małżonkiem swojem.

Wyjaśnienie się tu należy, iż Onej Wysokość Ulca z koczowniczego ludu się wywodziła, a że po stepach bez ustania z orężem owy lud gonił, nikogo nazbyt nie dziwiło, iże knezini takowe usposobienie pokazowała, jakie też imię nosiła, a przeto żwawe i do utarczków skore. Kiedy dwórki tutejsze warkocze zdobne z włosów swojech splatać zwykły, a też szatki pięknie szyć i kolorową nicią tkanie obrzucać, Ulca, onych księżna, warkoczami końskie grzywy zdobiła do galopu, szyła najchętniej z łuku, zasię do obrzucania arkan miała na karki, co się pojawiły mimo. Owo oplatanie arkanem ulubiła nad insze zabawki, co istną zmorą na dworcu Warczysławowym się stało, bo też księżna, oby w chwale żyła, nikomu zabawy nie odmawiała, czy to człekowi stanu dowolnego, czy bydlątku któremu. Szeptały też dwórki po cichu, iże z przyrodzenia tak do jazdy wierzchem usposobiona była, iże – przeze sen chyba – samego Warczysława, knezia niepokonanego, dosiadać śmiała na kształt ogiera, by niem cwałować. Doszły owe szepty do uszów Odona, kapelana i utrwożyły onego srodze, czy aby nie są to jakie praktyki pogańskie, co szatana przywiedą na chrześcijan zgubę. Toteż ująwszy różańca mężnie, pobiegł ku łożnicy kneziowej, by niem zło wysmagać. Pono Ulca, knezini chrobra, w dwa pacierze narowistego kapłana ujeździła, jego własnem różańcem onego batożąc. Odo, kapelan, szatana przy tem nie uwidział, a raczej jakby skrawek raju za mgłą, toteż póki co inkwizycyją wstrzymał. Jednakowoż czujności nie poniechał, bo też od początku był kazał, iże chrześcijańskiemu władcy brać pogańskiej księżniczki się nie barzo godzi, choćby nawet dało się oną po dobroci ochrzcić. I zanim do ślubów rzeczonych doszło, razem z Fryderykiem, cesarskim posłem, Warczysława, knezia, na to namawiali, iżby Ulcę na Gryzeldę zamienił, krewniaczkę cesarską bliską, bo przeze kuzyna krewniaka szwagra samego pomazańca spłodzoną. Wychwalali cnoty liczne Gryzeldy, osobliwie zasię przywiązanie do małżonka chwalebne i krzepę dobrze dzietności rokującą. Powiadali, jak to w pełnej zbroi za mężem swojem niegdysiejszem, Wilfredem, biegiem się rzuciła, gdy owy konno ze służką się był oddalił, jak to dogoniła onego i służkę z pożegnaniem, po czem nazad wróciła, letko ino się zadyszawszy. Warczysław, kneź, cóż kryć, z podziwieniem tego słuchał, jako że zawdy tężyznę sobie cenił i nawet myślów płochych dostał o Gryzeldzie niby rzepa krzepkiej, wszelako trafem na jaw wyszło, iż rzeczona aż tak tęga nie była, jako się zrazu wydawało, bo w jedną ino stronę w zbroi pełnej przebiegła, zasię nazad wróciła luźno, bowiem oręże w Wilfredzie, służce i koniu poutykała. Majestat trocha tem zawiedzionym był, na czem Ulca skorzystała. Odo, kapelan, z Fryderykiem, posłem cesarskim, poniechali o ożenek z Gryzeldą majestat nagabywać naonczas dopiero, kiedy posłyszeli odeń, iże chan, co Ulcę za mąż wydawał, kupiwszy sobie uczonego Angielczyka przeze Waregów rzeką w korabiu wiezionego, pode namową niewolnika kastyng zamiaruje ogłosić na wiarę nową, jaka by dla koczowników najsposobniejsza była. Spytał Warczysław, kneź, Oda, kapelana, a też Fryderyka, posła cesarskiego, jako mniemają, czy prawda jest, iże to chrześcijaństwo z wiarów wszytkich najlepsiejsze? – Jako żywo – zakrzyknęli owi. – Przeto też kastyng ani chybi wygra – Warczysław, kneź przemyślny, rzekł onym na to, za czym objawił marzenie swoje senne, co to w niem do chana orszak bogaty jedzie, a na czele Odo, biskup misyjny, iżby owieczki nowe chrzcić, a też Fryderyk, poseł cesarski, iżby ode nawróconego władcy hołd dla cesarza odebrać. Tak to kłody odsunięte zostały, co to je Ulcy, teraz knezini, pode nogi podtykano. No i dobrze dla dziedziny się stało, bo koczownicy poczciwości wielkiej ludźmi się byli okazali, z zapalczywością srogą na plemię wraże knezia Burczywoja następując, niechże miano nieczyste plemienia owego w zapomnienie pójdzie, bo też na co one komu, skoro ino niedobitki po puszczach a topielach do wyłapania koczownikom pozostały. A czyż nie tak było, iż kneź rzeczony – jeno czy kneziem zwać się onego godzi? – owy zatem Burczywoj w szaleństwie niepojętym ziemie Warczysławowe za ojcowiznę swoją miał?! A przecie pospolicie kozim synem onego przezywano, bo też świadków wielu i na Krysta, i na Dadźboga się klęło uroczyście, iże sodomickim sposobem przeze kozła poczętym został. Sam Odo, kapelan, pracowicie na pergaminach owe zeznania pospisywał, to dodając od mędrców leciwych, iż w tych stronach nigdy nie słyszano, iżby kozły majątki ziemne posiadać zwykły i potomstwu one przekazowywać.

Ale starczy już chyba nazad pamięcią sięgać.

No to, jako się już rzekło, Ulca, księżna, do izby wkroczyła i teraz żywo, arkana zapodziawszy, nade głową ścierą mokrą wywijała ku zabawie, aż trafem w łeb majestatu ucelowała nieszczęśliwie. Pomięszanie trwożne u przytomnych nastąpiło, zasię Warczysław, kneź sławny, zaryczał jakoby tur ubijany, bo szyszak, co na dostojnej łepetynie panoszył się nieproszony, na padół opadł z krwawiącym czupryny kawałem oberwanym. Pojęli wszytcy goście, iże Ulca, księżna, problemata dynastyczne rozwiązywać raczy życzyć sobie, przeto posłusznie z siedzisków powstali, by ku barłogom pomału spać podążać.

\*

Wszelako ujść z izby nie zdążyli, bo rumor wielki w sieni się poczynił, odrzwia z hukiem rozewarto, zaczym Ulryk, wojewoda, z toporem do izby wpadnął, Ciołka za sobą ciągnąc, co starszym nade strażą był. Za niemi ciżba poczciwych koczowników się cisnęła, Ulcy, księżnej, pobratymców – jedni ze strzałami na cięciwach, inni z mieczami krzywemi w garści.

– A tych kto puścił? – srogo ku Ciołkowi, starszemu straży, majestat się obrócił.

– Sam żeś, kneziu jasny, Warczysławie, przykazował, by nikomu nie otwierać, chyba by wojna była – posłusznie Ciołek odrzekł – Tom ich puścił podle przykazu, jak się opowiedzieli, że cię, Warczysławie Chrobry, zawojować przyszli.

– Aleć kto onych do grodu wprowadził? – zakrzyknął Warczysław, kneź.

– Jam jest! – ozwał się Ulryk, wojewoda, krótko.

Koczownik jeden, mniej trocha poczciwie ode inszych wyglądający, zajazgotał cosik po niemiecku.

– Ręcy do pętania wytykają wy, abo wami strzały przeszywają – Ulca, knezini, jazgoty niemieckie człowieczemi słowy wyraziła, jako ino umiała.

Trzeba to na dobro kneziowi i gościom jego zapisać, iże z chrześcijańską pokorą powiązać się dali, bo koczowników sojuszniczych, co przeze Ulcę powinowatymi majestatu byli, nie barzo im ubijać uchodziło.

\*

Powiązanych wielmożów koczownicy póki co poniechali i straż ostawiwszy, puścili się dworzec kneziowy zwiedzać, osobliwie zasię spiżarnie, kuchnie, komory z dostatkami, no i dwórek a dziewek służebnych legowiska. Ulryk, wojewoda, jako sam rzekł, udał się wiec zwoływać niezwyczajny, znowuż Ulca, knezini, o ile ją przytomni pojąć zdołali, obiecała wrócić, kiedy już łbów paru utnie, iżby ciekawskim peregrynantom to wpoić, iż ode izbów córki chana gapiom wara.

Ci, co się ostali, bo im wyjścia krępacja broniła, milczkiem zrazu w łepetynach obracali, co też by przypadki ostatnie znaczyć miały. Jednakowoż długo dumać po cichu nie wytrzymali – stąd choćby, iże radzenie we krwie mieli i dziedziną miłą zatroskanie.

Toteż Bruno, setnik, ku Obijmordą zwanemu, osiłkowi, się obrócił:

– Weźże, mój przyjacielu zacny, jako mocarz niezrównany z czerepa strażnikowi obok przyłóż, my zasię tutaj drugiego pospołu na kopniaki weźmiem.

Pokazało się, iże koczowniki oba, nie ino niemce ale i głuszce są.

– Barzo to chwalebne dla woja takowy spokój chłodny pokazować – majestat pochwalił – jakbym ręców nie miał powiązanych, zza pazuchy bym denarów dobył, by strażników naszych nagrodzić.

Koczowniki nadal chłodni byli.

– Cóże! Abo to niemce prawe, abo chytrusy nad podziwienie podstępne – Chichrała mruknął. – Tak czy owak, to czy owo może uradzić warto.

– Zda mi się – sposobność brat Michał, mnich, chętnie wykorzystał – iże wielmożny wojewoda… Czy nie Ulrykiem go zowią?.. Dobrze gadam, z gąb dostojnych waszych to widzę…. A przeto ony Ulryk, wojewoda, jakowąś złość do majestatu tu przytomnego knezia najwyraźniej czuje, a to by kryzysów rządowych nastąpienie znaczyło. Azali przy takowym rzeczy obrocie lustracyja zbójnictwa nie stała się bagatelą aby? Jak to bagatela, cóże wam, dostojni, po nas? Nie nam, prostaczkom lichym, mnie i Obijmordą zwanemu, do spraw rządowych się mieszać, przeto mniemam, iże może dobrze byłoby, kiejby wszytkim inszym stało się wiadome, cośmy są ludzie postronni, nic nieznaczący, trafem w przypadki zamieszani. Ową prawdę objawcie, zacni wielmoże, a my za to inną prawdę powiemy, jaką bądź nam wskażecie.

– Jakże onych inszych o lichocie stanu swego przekonasz, braciszku poczciwy, kiedy cię w poufałości z majestatem i wielmożami zastali, za jedną ławą z niemi siedzącego? – Piącha, setnik, zapytał ciekawie.

– Ano prosto – zakonnik odparł – wszak i Ulryk, wojewoda, i insi, co się jeszcze pokażą, przenigdy nas pośród możnych nie widzieli, a też słychu o nas mieć nie mogli. Łacno chyba w to uwierzą.

– Dzionek czy dwa nazad tu przytomny Łamignat, dziesiętnik, nikim był, zasię dzisiaj nad gawiedź widomie jest podniesiony; patrzaj też na mnie, com wonczas był taki jako rzeczony Łamignat teraz, a dzisiaj wielmożą mogą mnie nazywać – wesoło Piącha, setnik, odrzekł na to. – W chwili jednej łaska majestatu do stanu najwyższego człeka podźwignąć jest zdolna.

– Skoro tak – Obijmordą zwany się wtrącił – to ciężko zbrodniami nas z bratem Michałem od teraz nazywać, bo przecie knezie a wielmoże prawi ze zbrodniami w poufałości nie siadywają.

– Zgrabnieś to wywiódł – Chichrała, trefniś, pochwalił, a inni głowy pokiwali w namyśle.

– Woje a też rządowi jako prawda pierwsza to znają, iże na strona czyjaś stawać trzeba. Nie da się na nijakiej. Jasne wielmożom, iż kiedy na dobra strona staną, dostatek mają a urząd, zasię jako się na zła strona znajdą, bieda a poniżenie z nimi. Woje prosto z mosta rzeczą, iże dobra strona łba ucina, zasię zła strona łba ma ucięty – setnik Sigurd doświadczeniem się przyjaźnie podzielił i zaraz też radą usłużył: – Wy opowiadajcie szybko, czyja strona wasza.

– Czy też pewność mieć można przy takowym porządku, iż w ostatku prawda a sprawiedliwość zwycięży? – grzech zwątpienia brat Michał nie pierwszy raz objawił.

– Chyba żeśmy ci, braciszku miły, już pewność ową z jasnością wykazali – w wierze go Bruno, setnik, utwierdził stanowczo. – Wsłuchaj się w słowa Sigurda starannie, setnika w świecie bywałego: skoro strona dobra to dostatek a łeb przy karku, to czyż tak nie jest, iż to dobro na koniec zwycięża?

Na tem debaty ustały jenieckie, bo śpik przytomnych zmorzył.

\*

Po brzasku Ulca, knezini, nazad się pojawiła, a z nią zausznica Asude, co mowę dobrze znała, bo dwórki a pachoły rozporządzała.

– Księżna pyta, czy aby rzemienie nie piją – rzeczona Asude, zausznica, troskę knezini wyjawiła grzecznie.

Warczysław, kneź, ze świtą dzięki dworne za względy złożyli.

– Księżna pyta, czy aby majestat Warczysława, knezia, ciągle za krzywdę i zniewagę to ma, iże polewka wczora przypaloną mu się zdała – Asude znowuż się ozwała.

Wielmoże wesołość beztroską objawili.

– Że przymioty Ulcy, knezini nadobnej a też słodkości pełnej, wszędy majestat Warczysława, księcia, do zanudzenia z zapałem rozgłasza, nie bacząc na to wcale, czy słuchać kto chce czy nie, stąd najoczywiściej wiadomo, że to jeno przekomarzanki krotochwilne być mogły z onym przypaleniem rzekomym – Chichrała, błazen, odrzekł, gębę od ucha do ucha przyjaźnie rozciągając.

– Dobrze gada, duszko – majestat westchnął cicho, bo z ulubienia protokołów przepomniał, wszelako ocknął się zaraz, ceremoniał przywracając głośno: – Z pewnością Jej Wysokość Ulca, małżonka moja, to wie, iże bezmiar jej doskonałości tak wszytkim wokoło jest znany, iż nikto pomyśleć nie jest zdolen, by się owa doskonałość na zupy nie rozciągała przeze nią podawane. Tedy o owym przypaleniu krotochwile były.

– Księżna pyta, czy aby majestat Warczysława, księcia, wciąż nad tem wyrzeka, iż się ponoć incydentum dyplomacyjne porobiło z następstwy nieobliczalnemi, bo jaćwieski poseł arkanem zduszon został – niewzruszenie Asude, zausznica, ciągnęła.

– Smutny przypadek owy, wszelako ode początku zdanie o tem miałem, iże rzeczony poseł sam winien sobie – setnik Bruno z ubolewaniem głową pokiwał – po co przeze okiennice skoczył, kiedy pętlica mu na ramiony opadła? Zajęczego serca a nazbyt prędki być musiał, a też beze w dyplomacyji obycia.

– Owa Jaćwież barzo zacofana dziedzina być musi, skoro służbów takich jej brakuje, co obyczaje wykładają na tem dworze panujące, na jakowy udawać się posłom przychodzi. Abo też poseł leniwy nauk nie słuchał. Wiedzieć winien o Jej Wysokości Ulcy, zabawkach niewinnych. Nie wiem, czy niewiedzą swoją majestatu Warczysława, księcia, a też dziedziny naszej sławnej nie był aby obraził – Chichrała, błazen, znowuż przemówił.

– Skoro obraził, słusznie pokarany został, a zatem Jej Wysokość Ulca jakoby o sprawiedliwość zadbała – Piącha, setnik, bąknął.

– A też drużynników a pachołów do dziś ode śmiechu przy piwie skręca, kiedy owe jaćwieskie za okno kicnięcie wspominają – Łamignat, dziesiętnik, przede szeregi się wyrwał, a przytomni chichotem pobłażliwym prostotę onego nieposkromioną pominęli.

– Zapomnijmy oną przygodę przykrą, której wagę, jako się dziś wydawa, ponad miarę wszelką rozdmuchano – majestat westchnął. – Któż by śmiał o nią Jej Wysokość zaczepiać? W zabawce niewinnej chyba.

– Księżna pyta, zali nadal ogrom pracy rządowej majestat Warczysława, księcia, przytłaczał będzie, ode trudzenia się nad następstwem tronu onego odwodząc – cierpliwie Asude, zausznica, korale przesuwała, zasię przytomni dojrzeli z ulgą, że ino jeden paciorek jeszcze pozostał.

Toteż majestat odrzeknął krótko:

– Choćby w tej chwili się potrudzę z chęcią najmilejszą i do trudzenia takowego nikto mnie namawiać nie będzie musiał.

– Księżna pyta, co majestat Warczysława, knezia, o tych tu przypadkach sądzi, co ich przytomni jesteśmy, a też co czynić zamierza – Asude, zausznica, na koniec wyrzekła i korale schowała.

– Samem o to Jej Wysokość pytać chciał – westchnął książę.

\*

Ulca, knezini, z zausznicą, Asude, chwilę dłuższą po niemiecku poszeptały, iżby to po naradzie wyznać, że przede wpadnięciem koczowniczym niespodzianym o takowych zamierzeniach wpadywania nic nie wiedziały; tyle jeno dodały, iż nazad spoglądając, widzą wydarzenie, które bez wagi wprzódy się zdawało, a to takie, że Ulryk, wojewoda, co ostatnią porą do dwórki Dobrosławy skrycie był chadzał, tejże nocy odwiedzin poniechał, ino pachoła przysłał z wieścią, jakoby na łowy jedzie.

A że świta Warczysława, kniezia, a też i onże osobą własną niczego ważnego do dodania o przypadkach minionych nie mieli, wypadło przytomnym na teraźniejszym czasie się skupić.

– A czy czcigodna knezini a zacna Asude od poczciwych powinowatych naszych, koczowników, czego nie wyciągnęły o tym, co oni zamiarują? – zapytanie majestat ostrożne dał.

Tego się przytomni dowiedzieli, iż koczowników Demirkan wiedzie, wódz znany, wszelako przecież nie najznaczniejszy, nie taki, by go na wyprawę wielką posyłać. Zresztą potęgi nie wiódł, choć siłę jaką taką, bo dwie setki jeźdźców miał. Demirkana niewiasty tu przytomne przepytać nie mogły, bo ponoć z kneziem gadać podążył.

– Z kneziem! – Sigurd, setnik zawarczał. – Tedy wiada, kogo przeciw stoimy. Dziw jeno, że Ulryk, wojewoda, Zbysława pokumał, co mu zawdy kudły darł.

– Nie jest to w rzemiośle rządowym rzecz aż tak niebywała – majestat poczciwie westchnął. – Może nawet po chrześcijańsku cieszyć się z tegoż wypada, co się dwa wrogi zajadłe do zgody świętej skłoniły…

– Z płaczem a boleścią głowiny ich zacne ode karków odejmować nam przyjdzie – Brunon, setnik, smutno wyrzekł. – Zanim oni nasze odejmą – dodał.

– Niedawno całkiem gościom naszym, bratu Michałowi a Obijmordą zwanemu, majestat Warczysława, księcia, tłumaczyć raczył, iże bajarze tego umyślić nie są zdolni, jako się sprawy rządowe obrócić mogą – Chichrała, trefniś, mruknął. – Do obrotów znacznych gotowi bądźmy, może nawet owe ze Zbysławem, stryjcem kneziowym, niespodzianie nas połączą.

– Wszytko dla dziedziny miłej a też by prawda i dobroć insza górę brały – surowo majestat napomniał, na co świta onego gorliwie potaknęła.

\*

– Skoro jasne, iże za dobrocią dziedziny naszej murem stoimy, co zasię małżonka nasza, Jej Wysokość Ulca, uczyni? Azali z nami stanie? – do Ulcy, knezini, majestat się obrócił.

– Co słuszna uczyni – krótko księżna się ozwała.

Asude, zausznica, objaśnienie szersze dała:

– Jej Wysokość dobroci na równi godzić musi: a to takowe, co majestatowi małżonka są miłe, i te, co dziedzinie, nad jaką panuje, przyświecają, a też i owe, co oćcu, chanowi, po myśli się wydają.

– A jako się na równi nie da, jakie z onych dobroci najmilejsze? – prostodusznie Brunon, setnik, zapytał.

Asude, zausznica, uśmiech onemu pokazała:

– Kto wie jako knezini prześwietna nasza, Ulca, gorąca jest a prędka, a też jako nade wszytko swobodę miłuje, ten wie, iż tą porą nie dobroci przeróżne chłodno waży, jeno się w sercu burzy przeciw temu, że o koczowniczych zamiarach nikto jej uwiadomić nie raczył.

– Oj, znana jest nam przekorność Ulcy, księżnej, a przeciw przykazom wierzganie… Takowa przypadłość zwyczajnie u białogłowów nieznośna jest barzo, a że przy tem grzeszna, słusznie kapelani zalecają, by oną rózeczką tępić. Jednakowoż Jej Wysokość tako nade zwyczajność wyrasta, iże przywara rzeczona powabu Jej Wysokości przydawać się zdaje – Warczysław, książę, westchnął lubo.

\*

Na tem rokowania ostawić przyszło, bowiem szczęki orężne a tupoty z sieni dobiegające nadejście mężów wojennych zapowiedziały. No i Ulryk, wojewoda, a też Demirkan, wódz, z pocztem małym zara do izby weszli.

Pora pewnikiem, by o Ulryku, wojewodzie, co powiedzieć. Choć ludziska miejscowe pospolicie swojskiemi imionami wołani byli, zasię niemieckie miana, co je ze chrztem dostawali, jeno raz czy dwa w roku przy ceremoniałach których odkurzali, to wojewoda rzeczony taką miał przypadłość osobliwą, że na co dzień Ulrykiem wołać się przykazował. Poprzednik Piotra, biskupa, lud pogański chrzczący, kłopot mały z owem zawołaniem miał. No to i o tem kłopocie ździebko powiemy, bo choćby nawet skrybę to piszącego zganił kto za wątku rwanie, wszelako nad trudem misyjnym pochylić się mu wypada i pochwałę wtrącić z przypomnieniem, jako to kapłan pogany chrzący z zapałem wielkim posługi swoje wypełniał. Dość rzec, iż wytchnienie krótkie wtedy jeno ony miał, kiedy drużynnicy pół setki luda świeżo wodą polanego ze stawu wyganiali, a następne pół setki w odmęta wpychali, coby ich na łono kościelne można było przyjąć. Toteż może to i dobrze było, iż lud poczciwy grzechów swoich dobrze był świadomy i niegodnym chrzczenia się czując, opór dawał i przeciw wykrzykiwał, bo utarczki takowe odpocznienie nieco przedłużały; bez tego pewnikiem misjonarz na padół by z utrudzenia padł. I kiedy właśnie któregoś razu rzeczony kapłan takowy oddech utarczkowy krótki miał, mąż a niewiasta godnie odziani z dzieciątkiem w chusty zawiniętym doń przystąpili. Zachrypiał kapłan z przemoczenia, czego chcą pytając. Jak mu odrzekli, iże chłopię ochrzcić pragnienie mają, a mimo nijakich pachołów kneziowych nie dostrzegł, co by onych ku niemu popychali, popadł kapłan w poruszenie serdeczne i łzami szczęśliwymi spłynął, bo oto wreszcie świadkiem łaski nawrócenia zostać mu było dane. Przeto też z przychylnością wielką się do rodzicieli odniósł. A mieli owi inszą jeszcze prośbę na dodatek, by kapłan takie imię dziecięciu dał, co go inni chrzczeńcy nie mieli, bo tako pospołu sobie umyślili, iż miano niepospolite los niepospolity chłopięciu przyniesie. Ostatnie niedziele dwie miana Arnulfa a Irminy chrześcijanom nowym przydawano, zasię łońskiej dwuniedzieli Izydora a Izydory, wprzódy jeszcze Eustachiusa a Luciji. Ode niedzieli następnej Tomas a Matylda patronować mieli. – Na Tomasa się zgódźcie – misjonarz zachęcał, bo nie za wielu Tomasów w dziedzinie pojawić się mogło, jako że setnicy o tem zapewniali, iże już wszytki lud z osadów a siół do chrzcielnicy przywiedli i ode niedzieli takowi jeno się przytrafią, co na puszczy w sidła wpadną. Ale nie, uparli się rodziciele, iże takie miano chłopięciu życzą, co go nikto tutejszy miał nie będzie. Zasępił się misjonarz, zali w tym pychy nie ma, a też imiona mu się pokończyły w pamięci. Wszelako odmówić serca nie miał. Zadumał się i rozum natężył, aż to na koniec wspomniał, iż znał kiedy w rodzinnych stronach mężów trzech barzo poczciwych, co się Udalryk wołali, a skrótem Ulryk. Nie miał jeno pewności, zali święty który był, co by się tako samo mienił. Sprawdzić nie było jak, bo pergamin z mianami świętych, co go ze sobą kapłan przywiózł, nieszczęśliwie zaginął, a z niem krzyżyk pozłacany, różaniec z kulki koralowemi, krzesiwko zdobne a też inne sprzęta. Bokiem wspomnijmy, że kiedy przede tronem o stracie misjonarz zapłakał, zadziwił Sobiesława, knezia ówczesnego, iż się doń wybierając, przeze nikogo o zatrzęsieniu skrzatów uwiadomiony nie został, co w tutejszej dziedzinie łobuzują, cięgiem w szpary co bądź porywając, kiej nikto nie patrzy. Aleć za późno na przestrogi było. Ostawmy to, jako że nie o tem przecież nam gadać. Do rzeczy wróciwszy, to dopowiedzmy, iże na koniec misjonarz Ulrykiem malca ochrzcił, bo w rozumie wywiódł, iż skoro Ulrykowie jemu znani wielkiej poczciwości byli, to najpewniej mocnego patrona mieli, a jeśli nawet nie, to takowego zapowiedź proroczą stanowią. Takim oto sposobem wojewoda, którego losy kreślimy, miano swoje chrzestne posiadł, wszelako o tem, czemuż przeciw obyczajowi cięgiem one miano używał, niczego pewnego nie wiemy. Sam on tłomaczył, iże tako gorliwie za nową wiarą stoi, że przez to barzej niźli insi na wstręty poganów skrytych a diabłów jest narażony, toteż potrzebę nieustanną czuje, by patrona swojego ku się przywoływać; na to bowiem liczy, że owy święty, miano swoje słysząc, wspomagać go nie zapomni, a też o zasługach Bogu na ucho zaszepcze. A że nie od dziś wiadomo, jak barzo cnota człowiecza wrogi w oczy kole, nie zadziwi nikogo chyba, iże Ślizło, zausznik Zbysławowy, obmowę puszczał, że stąd wojewoda Ulrykiem się mieni, iże rodziciele Pieszczkiem onego nazwali, a to jest miano nijak do urzędu wojennego niepasowne.

\*

Brat Michał, zakonnik, wielmożów zwidziawszy, co do izby wstąpili, jak do modłów dłonie spętane złożył, pokornie głosu dobywając:

– Po tem, jak barzo mroki izdebne pojaśniały, wnosić można bez wątpienia, iże mężowie najgodniejsi nas nawiedzili. Pewnym być można, iże takowym na cnotach nie zbywa, a gdzie przymiotów jest bez liku, tam i miłosierdzie znajdować się musi. A też bystrość, co jednym oka rzutem zmierzyć pozwala, cośmy robaki liche są – ja i ten oto, co go Obijmordą zowią. Azali nie jest tak, że uczynkiem miłosiernym byłoby robaków nie zdeptać, ino zdrowiem darować i na powietrze puścić miast w pętach trzymać na stracenie?

Ulryk, wojewoda, ślepia wybałuszył na mnicha wymowę, zasię Demirkan, wódz, ku Asude zerknął, by po niemiecku jemu przetłomaczyła.

Sam majestat łepetyną pokręcił, iżby tego nie czyniła aby, a do brata Michała to wyrzekł:

– Widzę ja, żeś radzenie przyjaciół moich tu przytomnych mimo uszów przepuścił, bracie Michale i żeś jednak na postronność postawił przekornie. Raz ostatni z dobroci cię upominam, byś oną ścieżką nie szedł, bo na manowce zajdziesz. Weź, bracie miły, to choćby pod rozwagę, iż koczownicy, zacni powinowaci nasi, podobną poczciwość objawiają jak owi tambylcy bagienni, coś o nich drzewiej był rozprawiał, a przeto taką, iż zagubione dusze, co nic nie znaczą i beze majątku i znajomków po padole się błąkają, chętni są na kraj świata wieść, by je za srebro w opiekę litościwym bogaczom przekazować. Jeśliś krain nieznanych ciekawy, o swej postronności a lichocie przekonuj śmiało.

– A ja widzę, braciszku – Bruno, setnik, pocieszenie dał – że cię więzy trwożą, stracenie na myśl przywodząc. Wszakoż koczownicy, powinowaci kneziowi, między innemi z tej przyczyny na poczciwych miano zasłużyli sobie, iż prosto sobie poczynają, by spraw nie gmatwać; kiedy stracić kogo zamiar mają, to tracą zaraz, czasu na więzy nie marnując. Skoro nie stracili, to nie stracą, chyba by się co zmieniło.

– A jako się zmieni? – zakonnik jęknął. – I po co kogo więzić, kiedy się nań nie nastawa?

– Ja takoż widzę, iże w poczciwości koczowniczej prawidłach ni szczypty rozeznania nie posiadasz, bracie – wyrzekł Chichrała, trefniś. – No to ci wyjawię, iż ongi filozofy jako to Pierdoła z Zakałą, a też ja, skromny trefniś z tu przytomnym a zacnym Brunonem, setnikiem, seminaryją żeśmy otwartą zwołali, iżby arkana rzeczone rozebrać i objaśnić należycie. Zdania takie przeważyły, iże u koczowników z prędkości wszytko się bierze, a znowuż prędkość z gorącości serców wypływa. Tu pewnikiem, bracie Michale, o to zapytasz, skąd gorącość owa? Owóż koczownicze codzienne bycie pośród niepoliczonych stad bydląt rozmaitych ma miejsce, a owe krowy, kozy a owce, a też dziwne wielbłądy może, przodkiem trawy do się zagarniają, iżby do tyłu wiatrem dmuchać; taki to wiatr jest – uwierz, bracie Michale – co od pochodni przybliżonej ogniem bucha. Nie dziwota przeto, że od dmuchania bydlęcego tylnego powietrze ogniste się robi, a kto niem dycha, niezawodnie gorącości serca nabrać musi.

– Toteż koczowniki, zacni powinowaci kneziowi, bystro wszytko czynić chcą – podjął Brunon, setnik. – A tego są pewni z doświadczenia wiekowego, iż ugodę, co by po ich myśli była, wtedy jeno szybko zawrzeć mogą, kiedy rajców, co z niemi się ugadują, na powrozy wezmą alibo żelaziwo ostre onym do gardzieli przyłożą. Ponoć to droczeniu niepotrzebnemu, a też przewlekaniu barzo udatnie zapobiega. Taka dyplomacyjna sztuczka niewinna.

– Jeno pojąć trudno, gdzie tu poczciwość zachwalana się kryje – brat Michał zamarudził.

– A to też żeśmy rozebrali – objaśniał go Chichrała, trefniś, cierpliwie. – Owóż nie udziwisz się chyba wcale, kiedy ci powiem, iż z ugodami ten kłopot najwiętszy, iż strony obie zgryzota po zawarciu onych męczy, czy aby ci drudzy więcej na ugodzeniu nie skorzystali, a oni sami nie są aby na dudka wystrychnięci. Trud to bowiem często wielki – bywa, że i niemożebność nawet – iżby to wycenić, czy równa dobroć obu stronom przypadła w udziale. Ten jednak, co mu żelaziwo ode gardzieli odjęto, alibo powrozów się pozbył, pewien być powinien, iże korzyść jego więtsza, bo cóż nad zdrowie może być cenniejsze? Jeno może duszy zbawienie, aleć przecie kapelani nas zapewniają, iż na to nigdy nie za późno… Zali nie są koczownicy poczciwi, skoro na to się godzą, że co by w ugodzie nie zyskali, to stronie przeciwnej i tak więtsza dobroć się dostaje?

\*

Ulryk, wojewoda, cierpliwie końca uczonych kazań czekał, uszów jeno nadstawiając, czy aby co o zamiarach jenieckich się nie dowie, abo też o tym, kim nieznani mu zakonnik a olbrzym są. W końcu wszelako głosu dobył.

– Pora na majdan ruszać, gdzie juże się gawiedź zbiera – przykazał.

– A to po cóż? – Chichrała, trefniś, pytanie dał, ino że odpowiedzi się nie doczekał, bo poczciwi koczownicy i Chichrałę, i onego towarzyszów na nogi z padołu podnieśli, przyjacielskich kuksańców nie szczędząc, a też kopniaków figlarnych. Dalej, drogi wytłomaczyć jeńcom nie mogąc, bo po niemiecku tego by nie zdołali, za ręców pomocą pokierowali onymi, postronki ciągnąc na gardzielach jenieckich dla wygody obustronnej zadzierzgnięte.

Kiedy na słońce wyszli, krzyk podnieśli ludziska we ciżbie. Ci z bliska powrozy dojrzeli, zasię dalecy świtę zbrojną majestat otaczającą. Zaryczeli „Warczysław! Warczysław!” rząd popierający, aleć i ozwały się zrazu ciche, później coraz barzej gromkie opozycyji zawołania jako to „Na sąd! Na sąd”, „Zbysław, dziedzinę zbaw!” a też insze.

Na podwyższeniu Zbysław, stryjec kneziowy, był usadowiony i teraz prawicę uniósł, a gawiedź z ciekawości gęby pozawierała.

– Ludu, nasz ludu! – Zbysław, kneź, przenikliwie zakrzyczał. – Ileś ty się nacierpiał od rządów zepsowanych! Oto niemce z zachodu z niemcami ze wschodu nade łepetynami twojemi zmówili się, iżby ciebie ugnębić, obyczaj starodawny stłamsić i nowinkami skądeś zastąpić wstrętnemi. Czy nie uradzili zbrodnię takową, iżby mowy niemieckie zaszczepić obrzydłe, zasię mową prawdziwą pogardzić najmilej brzmiącą?! Co na to Warczysław, kneź, na kpinę Mocarnym nazwany? Zali na prześladowce ruszył z toporem i tarczą od onych się odgrodził? Nie, przeze niemą ze wschodu ujeżdżony, z posłem cesarzowym trunki popijał, przeciw przodków dziedzictwu knując. Niech by nowe porządki tylko szczepił w rządzie abo kupiectwie! Już za to w niesławie byłby pogrążony, aleć może pokutą którą by się odkupił. Ale ony na wiarę naszą najświętszą nastąpił pradawną! A czy tak nie jest, iże beze wiary przodków Słowianów nie ma, a beze Słowianów wiara święta ginie? Oto czas nadszedł, iżby obce porządki obalić, obcego boga panowanie skończyć, a danie na kościoły a zakony znieść!

– Danie znieść! – lud ryknął mile poruszony – myta znieść, wiarę wrócić!

Obok Zbysława, knezia, na podwyższenie kapłan wylazł z jemiołą na łbie, co go wprzódy ku niebu był zadzierał.

– Patrzajta, wierne Słowiany na Dadźboga słonecznego, czyż nie zachmurzon on obcego boga panoszeniem?! – trwożnie zawrzasnął. – Wyciągnijcie ręce z błaganiem, coby wam tę winę odpuścił.

Lud poczciwy posłuchał i ku powszechnej radości po chwili niejakiej obłoczek słońce odsłonił. Babiny, co mimo letniej pory czepcami wełnistymi rozumy rozgrzewały, w pisk uderzyły przeraźliwy, cud rozgłaszając, a starce zgrzybiałe z onemi pospołu zawodziły na kosturach wsparte. Chłopy rosłe z zawołaniem bitewnym porządkom niemieckim orężem pogroziły – bo też oręże najporęczniejsze ze sobą zabrały, jako że ktoś wici poroznosił – zasię białogłowy ozorami przeciw porządkom rzeczonym zaterkotały jadowicie. Dziewki na wydaniu przeze wzruszenie serdeczne gotowe byłyby wianki dla Dadźboga puścić, przeto też ku młokosom gołobrodym ślepkami zastrzygły, zali ich nad rzekę nie odprowadzą.

Zbysław, kneź, wolą ludu pooddychał i znowuż prawicę wzniósł, a przytomni gębom odpocząć dali.

– Jakżeż ludzie tutejsze pospołu ze mną do tego tęsknią, iżby zgoda a myśl jedna rodaki połączyła! – poczciwie kneź rozpoczął, coby dalej nadzieję w serca lać – Jak to dobrze przeto, że myśl jedna niechybnie rozkwitnąć musi na wierze starodawnej zasadzona. Jak wszakoż zgodę zaprowadzić, ludu zacny? Nie da się inaczej jak zdrajce tępiąc, co się ku niemcom garną, miast z rodakami bratać! Choćby serce krwawiło, że na krewniaków trza będzie złość wylać!

Przytomni ochotę ku zgodzie wyrazili gromko.

Warczysław, kneź, a też towarzysze jego po sobie spojrzeli.

– A to się kneź Zbysław rozpędził – Brunon, setnik, zamruczał – Czyżby umyślił nam łby poodejmować, nim onemu kto zapalczywy czerep strąci? W tym jeno nadzieja, że nasi koczownicy poczciwi jeńca łatwo się nie wyzbywają. Choć kto pewnikiem powiedziałby chętniej, iże nie tyle nie łatwo, co nie darmo.

– Nie aż tak ich wielu i nieroztropnie na błoniach ostawili wierzchowce swoje, a beze onych potęgę prawie całą tracą – smutno Chichrała, trefniś, westchnął.

– Jeszcze się pokaże, kto czyja łba ukręca – Sigurd, setnik, pogodę niespodzianie objawił, uszyma strzygąc.

Tupot gromady licznej jednym krokiem miarowym idącej i pozostałym jeńcom słyszeć się dał. Wszelako uwagi tłumu podnieconego póki co umknął.

\*

Kiedy roków temu sporo Sigurd, potem setnik, o gościnę poprosił Warczysława, knezia, Chichrała też dopiero co był przygarnięty i na nijakim urzędzie jeszcze nie był zasiadał. Wszelako juże w oko księciu wpadł i cięgiem onego bajaniem bawił, na krok nie odstępując. Widząc namysł kneziowy nad przybyszem nowym z północy, wyrzekł te słowa Chichrała:

– Zezwól, kneziu jasny, że ci radę dam, co z onej skorzystasz, abo też nie, podług woli swojej. Owóż czasy są takie, iże nowe idzie i ty to pojmujesz, do chrześcijanów lgnąc za przykładem poprzednika i przodka swojego, Sobiesława, oby pamięć onego zacna przeze wieki trwała. Wszelako lud poczciwy z obyczajem starym opornie barzo się rozstawa, tyle nowego ino biorąc, ile musi; ba, rzec nawet można, że kiedy daremność uporu czuje, często pozorem zadawalać próbuje księcia, co go do zmian przymusza. Nie dziwota przeto, iże książę, co pieczę nad tak psotnemi poddanymi sprawuje, co rusz po ojcowsku karcić onych musi, a przynajmniej rózeczkę bez ustanku pokazować uniesioną, iżby choć przez to od występków przeciw postępowi lud odwodzić. Z tej to przyczyny drużyna silna jest władcy potrzebna, stała i posłuszna. Wszelako młódź tubylczą do drużyny biorąc, z tym książę kłopotem się spotyka, iż ode drużynników ich oćce a starsi rodowi posłuchu nadal wymagają, na prawo krwi odwieczne się powołując. Pytanie się jawi, kogo przeto drużynnicy słuchać mają, księcia swego czy oćców a rodowców swoich? Iżby posłuch pełny u wojów mieć, kneź abo ze starszyzną uładzać się jest przymuszony, co stare ino kocha, abo synów przeciw oćcom buntować, co gniew straszny rozpala. Nie od dziś wiadomo, iże pomiędzy otroki zapalczywemi zawszeć takowe się znajdą, co im oćców władza bokiem wychodzi i onych mógłby kneź przy sobie kupić. Aleć darmo nic nie ma, jako kupce powiadają, bo nie dość tego, że za drużynę w taki sposób zebraną swarem bezustannym z rodami płacić trzeba, to jeszcze wątpić mądrość nakazuje, zali w owej drużynie posłuch bez granic władca na stałe mieć będzie, bo to jedni woje z czasem za zgodą ze swojakami zatęsknią, a drudzy to sobie ubrdają, iże to im decyzja przynależy, komu i za co posłuszni być mają. Wszytcy władce jak okiem po świecie sięgnąć z ową pułapką się zmagają. Przeto wielu już na to wpadło, iżby obcych, niemców najlepiej, do drużyn brać, bo owi ani z miejscowymi się przeciw władcy nie zmówią łatwo, ani wstrętu nie poczują, kiedy poddanych psotnych rózeczką przyjdzie wysmagać. A że nawet najwierniejszym poddanym miejscowym, co to księcia swego we wszytkim popierają, jakoś trudno niemych wojów do serców przytulić, woje owi jeno w rzeczonym księciu ostoję mają, a to ich do karności ślepej skłaniać powinno. To już są racje wystarczające, iżby takowego Sigurda abo kogo jemu podobnego do służb przyjmować. Ale cóś więcej uczynić można, co bym wymianą młodzi międzydziedzinową nazwał. Niech Sigurd pośle do jarla dziedziny swojej rodzonej, by wojów dzielnych do drużyny sławnej Warczysława, knezia, pode jego, Sigurda, rozkazy nadesłał, zasię Warczysław w zamian podobnie uczyni, jarlowi wojowników tęgich przysparzając. O denarów worek stoję, iż i kneź, i jarl podobną biedę z poddanymi mają, przez co oba równo na wymianie skorzystać mogą, takowych młokosów się pozbywając, co do swarów ze starszyzną się przyczyniają, bo ich więzy rodowe piją, a za to równie a nawet barzej jeszcze posłusznych drużynników zyskując w onych miejsce. Dodam, że kiedy woj który na obczyźnie za ziemią oćców zatęskni, to wróci, a kto inszy onego zastąpi. Takowy, co powrócił, doświadczenie miał będzie a obycie i władcy swojemu się przyda tem barzej, iż rodowcy, jeśli się nawet onego wcześniej nie wyrzekli, pewnikiem mniejszy na niego wpływ mieć będą.

Warczysław, kneź, barzo radę Chichrały docenił i oną w życie wcielił, a rajcę zmyślnego urzędem nagrodzić postanowił. Że o stare urzędy miejscowi zazdrośni wielce byli, dla spokoju nowy ustanowił, dotąd całkiem nieznany, a zatem trefnisia, inaczej błazna; ale owo już to czytający wiedzą przecie.

\*

Kiedy Ślizło, Zbysława zausznik, za kneziem swojem z uczty nam znanej krokiem chybkim uszedł, prosto do setni Sigurdowej się był udał, co na podgrodziu za wałem osobnym stację miała z bramą. Wtrącę bokiem, iże pytają liczni, czy ową gromadę wikińską setnią nazywać jest trafnie. Nie barzo dobre to słowo jest pewnikiem, bo trójsetnia lepszą by nazwą się wydawała, choć Przecław, podkomorzy, jęki wypuszczał, denary a dobra wydane licząc, iże Sigurdowym wojom takiej czwórsetni miano się by należało sprawiedliwie, co za osiem setek się obżera, a za dziesiątek opija. Na owe jęki tako Warczysław, kneź, odpowiadał do zanudzenia, że może i wikingi za dwóch się karmią, a więcej jeszcze piją, wszelako za pięciu w bitwie stoją, a zawziętymi będąc, przede nikim się nie cofają, chyba by taki przykaz ode wodza był. To słysząc, Przecław, podkomorzy, ręcyma machał, przeciwów niechając, wszelako jęki nawet i mnożył, ino że cichsze.

Poczciwie Sigurdowe woje czas pędzili, orężami ćwicząc, biegi w okolicy odprawiając abo zawody, a też ucztując i sakiewki a sakwy macając, zali nie za wolno puchną aby. Niechętnie barzo co z onych sakwów wyciągali, bo dobro całe chcieliby do dziedziny rodzonej wieźć wracając, choć i tacy się trafiali, co osiąść na stałe postanawiali na obczyźnie. Nieprzystojnie może mówić, aleć poczciwość osobliwie doskwierała owym junakom chwackim w tym oto, co kapelan kneziowy akuratnie chwalił, a przeto w tym, że na pokusy grzeszne z niewiastami nie barzo byli w czas pokoju wystawieni. Tako przecie było, iże rodowce niemcom dostępu do babów swojech bronili, choć – westchnąć tu nad słabością ludzką wypada – czasem która z ochoty własnej przeze zazdrośniki chyłkiem przemknąć się zdołała. Prócz tego jeno z niewolnice abo ladacznice raz kiedy pobyć mogli, co często być nie mogło, bo drogo, jako że usługi danią dla fiskusa niemiłosiernie obłożone były, zasię Odo, kapelan, zaciągania grzesznic do służbów pomocniczych podkomorzemu zabraniał dzielnie. Nie dziwota, iż woje słabości chętnie piwem przytłumiali, a winem czasem, kiej takowe na wojnie zdobyli, abo ode kupców wzięli, kruszców ciężkich odżałowawszy. Powiadali też ludziska którzy, że grzybków a ziela zażywali gęsto, a jako w tym przedobrzyli kiedy, to z przyczyny onej rozumu inszego dostawali, a wonczas, kiedy się bitwa zdarzyła, beze strachu nijakiego na zgraje wrogie w pojedynkę się rzucali – bywa, iż zbroi niechając, ino w skórę odziani abo i na golasa. Inni wszelako gadali, iże nie od czego zażytego, ino ode chuci nazbyt utłumionej tako z niemi na wojnie bywało.

No to do onych właśnie Ślizło, zausznik, się wybrał. Zrazu udziwili się owi z odwiedzin, aleć szybko pospólność utrapień im się pokazała jawna, przeto też udziwienie na bok odłożyli.

– Jako tu się ku starym bogom przybliżyć – trapił się Ślizło, o dziwo, po niemiecku zgrabnie całkiem jęzorem obracając – Odyna we łbie obaczyć, alibo Swaroga a Perkuna którego? Czego po temu zażyć, jak wszytko słabe: piwo kiejby woda, wino drogocenne beczkami trza by chłeptać, zasię zioła a grzyby jeno mało przywykłym szybko widoki dają.

– Ból to prawdziwy – przytwierdził Einar, dziesiętnik.

– Ja tako czynię, że łyk wina biorę, na to ziele, potem piwa i grzyb – Knut się zwierzył, takoż dziesiętnik.

– Nic jeno grzyb, piwo, ziele, wino na koniec – Toke się sprzeciwił, a potem wielu inszych arkana swoje poczciwie bliźnim wyjawiło.

Ślizło rozumem kiwał i wzdychał.

– Barzo to przemyślne a pracowicie odkryte sposoby, chwalebnie o dążeniu do bogów świadczące – z uznaniem wyrzekł. – Aleć w tym dziele ustawać nie wolno i nowego cięgiem trza próbować, bo to do Walhalli droga prosta.

– Zali ty, Ślizło, zauszniku, masz swój sposób tajemny? – Gudmund spytał.

– Owóż od Omara, kupca, napitek żem nabył nieznany u nas, a i u chrześcijany na południu barzo rzadki, co owego alchemiki warzą, a się on alkohl u Saracenów nazywa, zasię akwawita gdzie indziej.

– Cosik słychu o tem między żeglarze było – Einar, dziesiętnik, wspomniał, czoła namarszczywszy – wszelako tajemna rzecz to a cenna być miała.

– Prawdziwie takowy ów napitek jest – Ślizło przytwierdził – wszelako Zbysław, kneź, oby mu się we wszytkim darzyło, umyślił onego w darze wojownikom dać, iżby najdzielniejszych ku sobie zjednać, bo potrzebę wielką szykuje.

– A Warczysław, książę, co na to? – podejrzliwie Einar zapytał.

– Przeciwić się nie przeciwi – Ślizło zapewnił – a to stąd wiadomo, że jak wszytcy mniemają, nic beze onego Warczysława przyzwolenia w dziedzinie naszej się ziścić nie może, a o zakazie nic żeście chyba nie słyszeli. Pomyślcie też, co wadzić mu nie może przecie, że woje jego najlepsiejsi nagrodę dostaną. A że cenna ona nagroda, tedy po trochu ją zażywajcie, dla oszczędności zielem a grzybem pogryzając. Za dwie beczki mrowie niewolnika przyszło dać.

Poczciwe wikingi Ślizły posłuchały i żwawo, choć po trochu, do dna beczki opróżniły, co raz zielem a grzybem pogryzając.

W tem czasie Ślizło z takiemi samemi dary setnię Brunową nawiedził.

\*

Nadzieją Ślizło przemyślny się karmił, iże się Sigurdowa setnia na długo pośpi, bo wyrachował, jak barzo niestosownym byłoby, gdyby nieme wojsko lud w oczy zakłuło na wiecu, co stare a swojskie wraca, a nowe a obce potępia. A zasię druga strona rzeczy tako się jawiła, iż żal tak zacnych wojów stracić, przeto też lepiej byłoby dla Zbysława, knezia, onych ino na trochę schować i na to liczyć, iż knezia po staremu słuchać będą, na to nie bacząc, że imię władcy odmiany doznało.

Pewnikiem przysłowia owego Ślizło zabył, iż wiking potrafi, bo ze świtem Sigurdowi miasto dalej na padole legać, za akwawitą oczyma zropiałemi tęsknie potoczyli. Prędko do tego doszli, co ino w grodzie takowy napitek najść można. Tyle trunek nowy osiągnął, iże rozum inszy u więtszej niźli zwykle liczby wojów się objawił a u wszytkich drażliwość do bitki skłaniająca.

Dłużej nieco setnia Brunonowa się zbierała na nogi, choć jeno mięsiwem a placki akwawitę pogryzała. I tu rachuby zawiodły Ślizłowe, a to temu, że wiedźma Jaga, co wojom maku zadać miała, za akwawity przyczyną nabrała wdzięków wprzódy u niej dostrzec się nie dających, toteż do zabawów płochych bez przerw ciągana, maku posiać nie wiedziała jak.

Owa setnia Brunonowa cudzoziemską była nazywana, bo też na poły z obcych, a w połowie jeno z tutejszych się składała; jako jedni tako i drudzy w świecie obyci byli, przez co w mowach a obyczajach cudzych biegłość objawiający. Pożytecznie było takowych wojów trzymać, bo każden władca na ziemie ościenne abo dalsze wstępując, przewodniki a tłomacze zaufane potrzebował.

Brunonowi pobudzili się za łby trzymając, za pieszczotą którą a utuleniem utęsknieni rzewnie. Ino że wiedźmę Jagę wdzięki odeszły były, zasię trunków zbrakło, co by może biedzie owej zaradziły. Toteż na dworcu kneziowym, co gościnnością słynął, szukać pociechy woje umyślili; dziw to dla nich samych, że ze sobą oręże z nawyku pozabierali. Aleć nie dziwota dla nikogo, komu swawole zwyczajne, że setnia cudzoziemska koniów dosiadła; prawdę przecie nieprzemijającą gadają po karczmach, że kiej iść rady nie dajesz, ino jechać ci zostaje.

\*

Zbysław, kneź, myśl gawiedzi rzucił, co dobrze by być mogło, kiejby wrażą wiarę i porządki obrzydłe pospólnie na wiecu osądzić, a z niemi wielmożów swojskich a kapłanów niemieckich, co we świętokradztwie ręce ubrukali zdradziecko. Przewodzenie owym rokom rzetelne ofiarował osobą własną.

Tłum wrzawę pomieszaną uczynił, bo nie tylko za Zbysławem prostaczkowie krzyczeli, aleć i takowi byli, co nowe bronili. Kiedy wszakoż za wiarą starą stojący gromko przeważyli, na majdan pierwsze wikingi wpadły, co rozum inszy objawiały, a za niemi reszta w szyku jak do bitwy. Lud płochliwie przede berserki się rozstąpił, bo ploty chodziły, iże rozum inszy ciało na razy niewrażliwym czyni i berserka półsetny abo i setny cios dopiero obala; Sigurdowi woje po karczmach bajali to jeszcze miejscowym, że takowego Odyn chronić nie ustawał, póki owy we krwie wrażej należycie boga nie ukąpał.

Połowa niemców, Sigurda obaczywszy, ku niemu się obróciła, w to ufając, iże wódz jako zawsze wojów swoich szczęśliwie poprowadzi, przez co akwawita łupem padnie, zasię insi ku Zbysławowi i Śliźle się nakierowali, przeze mgłę pamiętając, co owi akwawitę dzierżą.

Nie sposób nie westchnąć nad ułomnością dusz ludzkich, kiedy to się widzi, iże miast dobro u bliźniego dostrzegać, ci abo tamci zła się w nim dopatrują. Tak też i teraz niemców zamiary niewinne krzywo pojęte zostały, bo się znaleźli ludziska niespolegliwi, co migiem wydumali, iż owi jeńca odbijać pragną, a też na Zbysława, knezia, orężnie nastąpić. Toteż setni kilka, co Ulryka słuchały, włócznie nastawiło, by Zbysława odgrodzić, zasię poczciwi koczownicy łuków się jęli. Bitwa by się może stała, bo miejscowi liczbą barzo przeważali, a wikingi o to nie zadbali, by za plecyma tłumu zbrojnego nie mieć. Kiedy okrzyki się zaczęły za wiarą świętą a niemcom przeciw, pojęli wikingowie, co tubylce tako akwawitę cenią, iż paść za nią gotowi. Jak miejscowi przewagą się zagrzewali, tako niemcom obycie bitewne to podpowiadało, iże jeno w tym nadzieja dla nich, coby błyskiem nagłym Ulrykowych przełamać nim się tłum na tyłach ogarnie, zasię później na zamieszanie i prostaczków utrwożenie liczyć. Toteż mur z tarczów uczynili, a ci co przeciw łukom byli wystawieni, biegiem ruszyli, akwawitę z potem obfitym wylewając. Koczownicy raz strzały wypuścili, tarcze wikińskie ozdabiając, potem poniechali strzelania, bo przykazu stosownego nie usłyszeli, a barzo do posłuchu przyuczeni byli. Przykaz od Demirkana, wodza, nie wyszedł, bo trafem niepojętym Sigurd, setnik, z nagła się obróciwszy, łokciem onego w nos trącił niechcący, a kiedy Demirkan nieszczęsny na padół się był osunął, w nieuwadze na gardle i klejnotach sam majestat Warczysława, księcia, z setnikiem Piąchą onemu stanęli. Niemce z północy wschodnich dopadli, może nawet których pocięli, wszelako kneź Warczysław i Sigurd, setnik, zapały rzeczonych ostudzili, każąc jeno więzów jenieckich zbyć. Demirkana, wodza, sam majestat z padołu dźwignął i nawet otrzepał trocha, nade krzywdą onego bolejąc ode losu ślepego przydarzoną. Z przyjaźni Warczysław, kneź, do swoich Demirkana puścił, kiedy koczownicy poczciwi z przyjaźni majestatowi kołczany swoje podarowali.

Tymczasem atak wikiński błyskiem czyniony na Ulrykowych utknął na mgnienie, kiedy niemce na widok Zbysława, knezia, z udziwienia zamarli, bo owy nie wiedzieć czemu na podwyższeniu po ojcowsku Ślizłę, zausznika swojego, ręcyma własnemi na gębie jął karcić. Chwila wahania się zemściła, prostaczkowie z tyłów ubrdali sobie, iż niemce od nich uchodzą, toteż gonić onych poczęli, do uszykowania w krąg przymuszając. Znowuż się wydało, iż Zbysławowi górą. Natenczas wpadła Bruna konnica, a gawiedź na boki pierzchła przede kopytami. Wszelako setnia cudzoziemska nie do bitwy usposobiona była, a też co się dzieje, nie barzo pojmowała. Toteż wszytcy po kolei oręża opuścili i wokoło rozglądali, kto swój i co knezie każą. A owi póki co nie kazali nic, ino żeby się przy nich kupić.

Powoli rozstąpili się przytomni i przy Warczysławie setnie Bruna i Sigurda stanęły, a też pogubione dziesiątki a pojedynczy woje insi. Z nimi Matyldowi stronnicy, co nowe polubili, a też takowi ludziska, co wprzódy poprawność noszeniem znaku krzyża podkreślali, a teraz złości poganów obawiać się mogli. Do owych ludzie kościelni przylgnęli trwożnie.

Ze Zbysławem setnie jego i Ulryka, wojewody były, a też Guzdrała ze swojakami a opolnicy z podgrodziany tłumni. Kapłani z jemiołą na łbach, co nie wiada skąd powyłazili, przywiedli ku nim hufce swoje z wełnistych czepców a kosturów uszykowane, a hufce owe pobożnie śpiewy zawodziły.

Z boku koczownicy poczciwi się przyglądali, a Prasło z łachmaniarzami jakby nie barzo wiedzieli, gdzie przystąpić.

Młodź nad rzekę się wymkła wianki puszczać.

\*

Majestat Warczysława, księcia, brata Michała po ramieniu klepnął:

– I po strachu, bracie – wyrzekł – trudów wiele przed nami, co przy nich lustracyja zbójnictwa iście bagatelą się stała; nie o was, mili, radzić będziem, tedy w pokoju spać chadzajcie.

– Jak to w pokoju – zakonnik mruknął – przecie szyki ustawione jak by zaraz wojska się zderzyć miały.

– Zezwól, bracie Michale, iże cię w arkana polityczne wprowadzę, coś ich jeszcze do końca nie zgłębił – kazać począł Chichrała, trefniś, statecznie – owóż tako zwyczajnie między knezie bywa, iże szarpią się owi o stolec monarszy, zasię lud poczciwy z boku się temu przygląda. Mędrce opolni, co młódź przy paleniskach wieczorami uczą, tako o tym powiadają: a niechże dzielniejszy przeważy, pewnie udatniejszym władcą będzie. W takowych to razach bitwę się czyni i ona lepsiejszego wynosi. Dodam bokiem, iż nie jeno cnoty wojenne zdolne są knezia wywyższyć, ale też dyplomacyjne, bo często bywa, iż o tym, kto przeciwnika pognębi, ściągnięcie posiłków od ościennego władcy rozstrzyga. Są to obyczaje od wieków utarte, chwalebne a pożyteczne, bo pośród rodu panującego selekcyją dokonują, niezgułów pozbywając, a łebskich zachowując – dziedzinie na pociechę.

Dziś wszakoż z czym innym do czynienia mamy: oto lud się na dwoje podzielił i nawet jak kneź który bitwę by wygrał, to i tak połowę ludu przeciw sobie by miał nastawioną. Aby ową połowę nieswoją do posłuchu skłonić i w niem utrzymać, tyle by się natrudzić musiał, iże na insze dzieła i czasu, i dóbr by onemu zbraknąć mogło. Wnet by się o tem władce ościenne zwiedziały i abo by obmyśliły to czy owo dla się złupić, abo za pomoc hołdów zażądać a uległości w rządzie.

Brunon, setnik, co rozumem kiwał słuchając, to dopowiedział jeszcze:

– Pomyśl przeze chwilę, bracie Michale, na niby, iże tu przytomny majestat Warczysława, knezia, na ostrzu miecza sprawę stawia i na Zbysława mężnie w tejże chwili rusza. Ku temu pewnikiem by poszło, iże by nas Zbysławowi pogromili, boć wiele od nas liczniejsi. Prawda jest, iże słodko a zaszczytnie za dziedzinę legnąć, wszelako jeszcze szczytniej byłoby dziedzinę ku chwale a szczęśliwości podnieść. A tako się składa, iże ino my dobrze dla onej ziemi chcemy, zasię Zbysławowi łacno by oną w zamęcie, ciemnocie a nieszczęściu pogrążyli. Jakże by nas do chwały bitewnej nie ciągnęło, to wszelako mitygować dzielność musimy i dla dobra dziedziny siły zachowywać, osobistej sławy niechając z żalem.

Tu setnik Brunon, śliny przełknął i dalej gadał:

– Skoro wszakoż okoliczności na niby się dziejące roztrząsamy, cóż temu szkodzi, by też na niby nasze zwycięstwo przyjąć. Nie jest to całkiem niemożebne, bo setnie nasze dzielniejsze a też barzej ćwiczone, zasię prostaczków więtszość w oszczepy ino abo toporki a koziki się uzbroiła. Po temu pewnikiem ta niefrasobliwość, że za przyczyną przytomnego tu Warczysława rządów dzielnych dawno już wrogi pode gród nasz nie podchodziły, toteż gawiedź czujności zbyła i choć wici do niej doszły, lekkie ino oręże ze sobą wzięła, do ciężkich tarczów a dzidów się leniąc.

– Aleć wysiłki nasze wielkie by być musiały a równo potu co krwi wylać by nam przyszło – wtrącił Piącha, setnik, mrukliwie – a donoszą, że woje twoje i Sigurdowe ode Ślizły czymś potruci. A jak by nawet plota to jeno była, to i tak wiele szczęścia byłoby trzeba.

– Praw żeś, Piącho zacny, wszelako na niby sprawy widzimy – przerwał Brunon, setnik, statecznie. – Owóż skoro tak sobie już wymarzyliśmy, żeśmy są niby górą, warto o to zapytać, jak by to się stać mogło i co by z tego wyniknęło.

– Krótko to wam powiem – sam majestat się wtrącił – tako by było, że woje Zbysławowe za czepcami a kosturami by stanęli. Hufce staruszkowe chętnie by niemcom czołów stawiły, boć przecie wiara silniejsza ode tarczów jest i Dadźbóg wrogów szeregi obalać palcem jest zdolen. Kiedy by niemce, miast bez ducha popadać, ruszyły jednak, za późno byłoby starcom po rozum do głowów iść, że przecie Dadźbóg sam se rady bez nich da i by się babcie a dziadki z wrogi zderzyć musiały. Nijak byłoby onych oszczędzić; przecie mniej liczny, do zwycięstwa dążąc, bronić się nie może, ino atakować pędem musi. Niechby nawet woje poniechali dziadków kłuć i nie cięli onych, to wszakoż tarczami by potłukli i potratowali. Nie mogliby szeregów rozrzedzić, by ludziów omijać, wszak to by ich zguba była.

A teraz wam powiem, co po zwycięstwie by było. Zbysław by uszedł, bo przecie nie okrążylibyśmy onego i pode nami dołki by kopał na sposoby przeróżne. Gruchnęłoby po ludzie, iże Warczysław nie ino wiarę, ale i lud zdradził i niemcami starce niewinne zgładził. Na palcach tych byśmy policzyli, co by przy nas szczerze stanęli. Samą siłą trza byłoby panować i drużynę na opolan po jadło prowadzić miast w ościennych dziedzinach sławy i drogocenności dobywać. Sigurdowe wikingi nie kruszce czy wyroby piękne do sakwów by ładowali, ino orzechy a rzepy wieśniakom pobrane. Ciekawość bierze jak długo by wytrzymali, iżby się u barzej szczęśliwych książąt nie zaciągnąć.

– Prawda kryć nie warto – Sigurd, setnik, wyrzekł, dotąd milczący – nie stanie wiking za wódz, jako widać jawno, iże jaki bądź bóg onego szczęściem nie darzy długo.

– Tedy widzisz jasno, braciszku, czemu to bitwy nie będzie – ku Michałowi, zakonnikowi, majestat się obrócił.

\*

– A ja nie widzę – wyrzekł Obijmordą zwany, osiłek, niespodzianie. – Pojmuję, czemu to Warczysław, kneź, do bitwy nie zatrąbi, wszelako nie widzę, czemu Zbysław tego by uczynić nie miał. Przeciwnie, wiele z tego, co tu żeście, dostojni, wyłożyli, mogłoby rzeczonego Zbysława, knezia, do trąbienia nakłonić.

– Jużeśmy wspomnieli, czemu żaden z kneziów szczęśliwy po zwycięstwie by być nie mógł – jął Chichrała, trefniś, z rozwagą tłomaczyć – i racji owych powtarzał nie będę. Mniemam, iż to masz w myśli, że Zbysław na pobicie Warczysława mógłby jednak liczyć, a przecie wonczas on by panującym się ostał, za czym ode lat tęskni. Obawiasz się pewnikiem, iże tęsknota owa tak może być przemożna, iż kneź oną powodowany, ręcyma na kłopota nieuniknione machnie, byle na tronie osiąść. Tako by być mogło ze Zbigniewem, synem Zbysława, bo młodość zapałów tłumić nie jest zwyczajna, wszelako sam Zbysław z rozwagi słynie. Wie on, że choćby wojska tu przytomnego Warczysława zwyciężył, to sam w istocie panować nie będzie, bo sił jemu nie stanie. Setni Sigurda i Bruna ani podgrodzianie a opolnicy nie przemogą, ani hufce Dadźbogowe onych kosturami nie popędzą. Na setnie rzeczone swoje najlepsze setki rzucić by musiał. Za jednego wikinga trzech swoich straci, za Brunowego dwóch może. Z tym, co mu ostanie, władzy nie utrzyma samodzierżawnej. Będzie jeden pośród wielu i władzę z Guzdrałą, Ulrykiem, kapłanami, Prasłą i podgrodziany podzieli.

– Ale przecie zamiarów swoich nie zbędzie, coś uczynić musi – Obijmorda, osiłek, się upierał – jakoś na tronie zasiąść chciał będzie, jako to uczyni, skoro miecza poniecha?

– Aby tron posiąść, nie mus mieczem go rąbać, ino dupsko na onym zawiesić – zgryźliwie Brunon, setnik, zamruczał.

Ani chybi musiał słowy owymi w kłopot utrafić, bo Warczysławowi a wielmożom chmurzyska na gęby powyłaziły i ze wzdychaniów skryciem poradzili sobie kiepsko. Aleć niedługo potem majestat dostał nagłego błyska w oku, co już go kiedy nawiedzał, kiedy w odmęta polityczne nura dawał, iżby potem na wierzch wypłynąć.

\*

Nadeszedł kapłan ważny, gałęzią zieloną kiwając, że może ugodzić się trzeba.

Majestat Warczysława, księcia, powitał onego dwornie:

– Zali mnie oczyska mylą, czy też samego Dadźbosława widzę, co starej wierze przewodzi? Zaszczyt to wielki dla nas wszytkich.

Wielmoże przytomni uradowanie wielkie objawili na gębach.

– Za przywitanie dzięki czynię, to wszakoż poczciwość mi nakazuje, bym się miana Dadźbosława wyparł, bom skromnym kapłanem jest, co rzeczonemu Dadźbosławowi piętów nie dorasta – odrzekł kapłan ze skromnością. – Swarochwał żem, jeśli miano moje komu potrzebne, prawdziwej wiary chwalca prosty – objawił.

– Wybacz, Swarochwale, kapłanie, jeślim postać piękną twoją a zachowanie przystojne, a też mowę składną z najwyższymi godnościami pomylił – przeprosił Warczysław, książę. – Wszelako jakoś tu szedł, mruczeliśmy pomiędzy sobą, że oto taki człek ku nam zmierza, co pozór daje, jakoby na najwyższych godnościach zasiadał.

– Majestat zacny Warczysława, knezia, jako mu czasem się przydarza, z przesadą jednomyślność naszą ogłasza – westchnął Chichrała, trefniś – bo prawda taka, żeśmy spór mieli, zali postawa zacnego Swarochwała, kapłana, barzej godności duchowej czy rządowej odpowiada.

Swarochwał, kapłan, pokłonił się bez uniżenia i wyrzekł na to:

– Jeszcze raz dzięki za dworskie przywitanie. Nie będę zmyślał, iż nie są dla mnie miłe słowa, jakowymi do mnie przemówiono. Ale też dodać czuję się zmuszony, żem prawdziwej wiary sługą niezłomnym i na chrześcijańskie plewy nikto mnie nie skusi. Nie po drodze nam, Warczysławie, kneziu, coś się fałszywej wierze zaprzedał. Zaprawdę powiadam ci, rzuć błędy i przed Dadźbogiem ukorz; wtenczas może być, iże w zgodzie pogadamy.

– O sprawach niebosiężnych a też w ziemskie skutki brzemiennych rozprawę napocząłeś – Warczysław, kneź, westchnął. – Przyjdzie nam być może do dysputy tak wielkiej wagi przystąpić, wszelako zgodzisz się niezawodnie, iż skupienia a spokojności to wymaga, co ich w gorączce czasu i miejsca obecnego odnaleźć nie sposób. Zstąpmy na padół przeto i do posłania twego wróćmy. Co owo zawiera w sobie?

– Zbysław ze Zbigniewem, knezie oba, kapłany prawdziwej wiary a też lud cały na wiec cię proszą, Warczysławie, kneziu, byśmy pokój uradzili pospołu i poczciwy w dziedzinie porządek nowy. Mir wiecowy ogłaszamy, iżby nikto z rajców na inszego ręców podnosić się nie ważył i ruchów nijakich onemu nie wzbraniał tak po majdanie jako i na drodze do dom i z powrotem, a też w odwiedziny, jeśli ino obradom owocnym służyć takowe będą.

– Barzo rozsądne posłanie żeś nam przyniósł, czcigodny Swarochwale – majestat pochwalił – tedy poczekamy, aż kapłani wszytkich wiar się zbiorą, iżby na mir przytomnych zaprzysiąc, a potem wiecować poczniemy jako obyczaje każą. Czy zgoda?

– Zgoda – westchnął Swarochwał, kapłan i pokłon oddawszy, nazad się puścił.

Chichrała, trefniś, z uszanowania kroków paru onego odprowadził. Kiedy pogodę już pochwalili a siebie nawzajem i rozstać się przyszło, zamruczał Chichrała myślą tknięty:

– A kiedy o nawracanie chodzi, zacny Swarochwale, do rozumu mi przyszło, iże chrześcijanów łacniej ku Swarogowi skłonić niźli ku Dadźbogowi, nie myślisz?

– Czemuż to? – ostrożnie Swarochwał zapytał.

– Temu, że jak Bóg chrześcijański ma Chrysta za syna, tak Swaróg Swarożyca i jakoś poręczniej chrześcijanowi z pary na parę cześć przenieść, niźli na boga we wszytkim inakszego.

Swarochwał, kapłan, oddalił się w zamyśleniu.

\*

Długo zeszło, zaczym kapłany wszelakie pozbierały się i porządek przysięgania uradziły. Ile z tego swarów a turbacji się porobiło! Kapłan Swaroga chciał równo z dadźbogowym iść, zasię owy widział swarogowego trzecim po sobie a Piotrze, biskupie. Szamana koczowniczego w trzeciej dopiero beczce od wina znaleziono, wikingi miast wodza którego wystawić, co to kulta dla wojów swojech zwyczajnie odprawował, skalda przysłali o Odynie a bogach inszych śpiewającego; w tym kłopot, że skalda rzeczonego mus był z rozumu berserkowego jak bądź wyleczyć. A na koniec kapłanka Dziewanny przepychała się przede marzannową juże na podwyższeniu majdanowym, przez co ta druga, zleciawszy, na łbie wojakowi się nieprzystojnie zatknęła w hełm spiczasty uposażonemu. Że wojak rzeczony kark skręcił, obawa się zrodziła, iże to źle mirowi wróży i przyszło urok odczynić, a to prędko być nie mogło. Kiedy ostatni prostaczek przysięgę złożył, miesiąc jeno przyświecał i wiec do południa dnia przyszłego odłożono.

Po prawdzie, to wszytcy prawie zadowolnieni ze zwłoki byli, bo to czas dawało, by się z tymi czy owymi rozmówić, rozmówców rozumienie przypadków rozeznać, może co pospólnie ugadać, abo choć wybadać, zali z sojusznikiem możebnym w której sprawie się ma do czynienia, abo kim ni takim ni siakim, czy wreszcie z wrogiem – a jak tak, to czy zajadłym, czy nie barzo nasrożonym.

Niemce północne a wschodnie do obozów swojech wróciły kromie naczelników, co do ugadywań mogli być przydatni. Woje insze także samo sobie poczęły, zostawiając ino dziesiątki cztery do porządku pilnowania; z onych dziesiątków dwie Warczysław, kneź, wystawił, zasię po jednej Ulryk, wojewoda a Zbysław, książę. Pachoły Guzdrałowe a Matyldowe takoż się kręciły, jako i sługi kościelne a kapłańskie insze, kupieckie, rzemieślnicze, opolne, z poselstwów ościennych i kto zaś wie, czyje jeszcze. Wszytcy oni serdecznie pieczą nade mirem byli przejęci, chociaż różnie oną pieczę sprawowali. Jedni do ludziów rozprawiających się podkradali, a też pode namioty, co migiem majdan upstrzyły, pode okiennice dworcowe i zakamarki wszelakie, w poczciwym zamierzeniu spisków przeciw mirowi wykrycia. Drudzy pierwszych zawracali, kiedy do takowych miejsc owi podchodzili, kędy by beze pożytku czas strwonić musieli, bo stamtąd zdrada nijaka wyjść nie mogła, jako że ludzie godni tamże uradzali, poza podejrzeniem będący przecie.

\*

W takim to zamieszaniu i rwetesie Obijmorda pospołu z bratem Michałem w osobności się znaleźli trafem, choć pewnikiem o osobności za dużo powiedziane.

Wyrzekł tedy w te słowa Obijmordą zwany:

– Cóż Michale, bracie, oto żeś się chwili doczekał, iż umknąć możesz, bo zda się, iż nikogo nie ma, co by wyjścia komu bronił.

Brat Michał brodę tarł wątpiący.

– Jakże to! – Obijmorda krzyknął osłupiały – Nie pomnisz, żeś przede południem o puszczenie na świat błagał, bośmy są robaki żyć tu niegodne?

– Wonczas w więzach nas trzymano i mogło być, iż kaźń gotowana była – wymruczał brat Michał, wzdychając – teraz Warczysław, kneź, sam wyrzekł, iż lustracyja nic to i wolno chadzać możemy.

– Tenże Warczysław kiedy indziej to kazał, iż takowych obrotów, jakowe się w rzemiośle rządowym przydarzają, najtężsi bajarze umyślić nie są zdolni. Oj, jeszcze lustracyja może do przepychanek rządowych nie raz posłużyć, a nie aż tak o to trudno, skoro zbójnictwo zarówno kupcom, jak wieśniakom na wątrobach leży, tym zwłaszcza opolnikom, co dalej ode grodów siedzą. Pomnij też, że kiedy pogaństwo do końca górę weźmie, za sam habit sprawić cię mogą. A czy kneź Zbysław abo Ulryk, wojewoda, jako do końca się umocnią, nie będą chcieli dla pokazu paru zdrajców umęczyć, co to Warczysławowi sprzyjali? Ci łacno na widowisko dla ludu się nadają, co za nimi żaden ród ani opole nie stoją.

– Myślałem, że lud męki wielmożów obalonych oglądać woli – brat Michał marudził.

– To cię wielmożą okrzykną, jeśli tako racyja stanu podpowie – bez cierpliwości Obijmorda wyrzekł. – Mów mi zaraz, co to ciebie tak tutaj trzyma?!

– Ano, rzeknę ci najszczerzej, iż to są owe wygody, co nikczemną doczesność umilają. Na barłogu spanie wolę – nie zaś na mchu. Kołaczy żucie a pieczeni z woła cenię – miasto podpłomyka wyproszonego abo szczawiu a jaszczurki ucapionej; oho, a ile to dniów wspominam niemile, kiedy ino powietrze żem miał do łykania. A tu jeszcze dziewki a pachoły cieszą, co szaty piorą, abo nowe podają, choćby i nawet zgrzebne dworakom mniejszym, ale przecie więtszym już nie zgrzebne jeno futrem nawet podszyte.

– Widzę, bracie Michale, iże majaki dostojności cię już wabią! Czyżbyś wiarę miał, iż urzędu dostąpisz? Czemu niby? Ty włóczęga znikąd?

– Owóż gadałem ja szczerze z zacnym Brunonem, setnikiem, na ustroniu, o to samo co ty teraz pytając; po tym to było, kiedy nam rzekł, byśmy ducha nie tracili, bo jeszcze tak się sprawy obrócić mogą, iż do drużyny sławnej Warczysława nas wezmą, pomnisz?

– I co ci odparł takiego?

– Prawił, iż aby prawdziwie wielkim władcą abo wielmożą się stać, trza mieć przymiot szybkiego a trafnego ludziów ocenienia: czy co potrafią i czy z innymi wespół działać są zdolni, a też czy zgrabnie dwie cnoty łączą, a to posłuszności a samodzielności; ta druga cnota wtedy jest potrzebna, kiedy przykazów znikąd, a działać trza. Gadał też Brunon, setnik, iże nie spotkał człeka takiego jeszcze, co by przymiot, o którym mowa, w więtszej mierze posiadł niźli sławny Warczysław, książę – samego margrabiego Arnulfa nie pomijając.

– I co, na to liczysz, że cnoty twoje zacne Warczysław pokochał?

– Nasze, bracie Obijmordo, nasze, jako sam kneź rzekł! Chyba żeś tego nie przepomniał, co? Skromność mi nie dozwala swoich cnót ogłaszać, przeto tyle jeno powiem, że w sztuce pisania i czytania mam w dziedzinie Warczysławowej aby czterech czy pięciu towarzyszów. Ty zasię masz rzadkie połączenie siły z rozumem; uwierz, że naprawdę rzadkie.

– Jeszcze stąd do urzędów daleko – Obijmorda studził.

– Kazał zacny Brunon, setnik, iże w tej części padołu ziemskiego, kędy Słowiany z innymi siedzą, to się rozgrywa, czy książęta pełnię władzy ucapią czy nadal oną dzielić będą przymuszeni ze starszymi plemiennymi i rodowymi z jednej strony a wiecami opolnymi ode strony drugiej.

– I co z tego?

– To z tego, iże pomału kneziowie próbują od urzędów wielmożów starych odsuwać, a nowych wywyższać, co jeno ode nich zależą, nie zaś od rodów i opoli, bo na ten przykład znikąd są. Chociaż ciężko to idzie, o czym ruchawka pogańska zaświadcza.

Obijmordą zwany rozumem pokiwał i wyrzekł:

– Dopuśćmy przeto, iże uda nam się urzęda obsadzić, jako ci się marzy. A co z racyją stanu zmienną, co takową postać przybrać ni z tego ni z owego może, iże dla dziedziny stracenie nasze zbawienne się okaże?

– O to samo żem zacnego Bruna, setnika, trwożnie dopytywał. Rzekł on na to, że rzecz jakoby denar oblicza dwa posiada: z jednej strony prawda to, że łatwo wielmożę zaufanego poświęcić, jeżeli owy za sobą rodowców a opolan nie ma; wszelako strona druga taka oto jest, że w nowym porządku, zwykle ze chrześcijaństwem się kojarzącym, to, co pomiędzy zwierzchnością a poddanym zachodzi, nie krewieństwo wyznacza – czy lepiej powiedzieć: rzadziej krewieństwo – niźli więź nowa, co na ślubowaniu wzajemnym się zasadza i takowy kształt ma, iż władca abo zwierzchnik inszy pieczę a obronę poddanemu przyrzeka, zasię poddany za to posłuszność, wierność i pomoc obiecuje. Racyja stanu nadrzędna, a przy tem – zważaj – niezmienna się pojawia w takim rzeczy ułożeniu, iż wtedy jeno pieczę nade poddanym znosić się godzi, kiedy owy śluby swoje połamie, a przeto zdrady się dopuści alibo nieposłuszności czy też gnuśności grubej w potrzebie. Kneź przeto, któren by dla racyji stanu mniejszej takowego poddanego poświęcał, co mu ślubów dochowuje, wnet by mir między swemi stracił i nikto by wierności jemu złamać się nie wzdragał, skoro on sam pierwszy ślub swój naruszył. Toteż kto z kneziem ślubował i wiernym stale jest a posłusznym, a też chętnym w służeniu, no i przydatnym, spać spokojnie może.

– Chyba by takowa racyja stanu się pojawiła, co by tak wielką była, iże by nade ową nadrzędną wyrosła.

– Bezpieczność bez granic jeno w niebiesiech znaleźć można, bracie Obijmordo. Dodaj nawet, iż służba wierna dla dziedziny też na zdrowie zaszkodzić jest w możności, choćby na wojnie odprawowywana. Wszytko to prawda istna, czy wszelako na puszczę się wymykając, zagrożeń zbędziemy? Zda mi się, że na rzeczonej puszczy barzej jeszcze co dnia na uszczerbek abo i zatratę narażać się trzeba.

Na tem przeto stanęło, coby przy kneziu Warczysławie póki co ostać, wszelako w gotowości do nogów za pas więcia, kiejby trwoga skąd bądź się pokazała.

\*

Rozejrzeli się brat Michał i Obijmorda, którędy by tu ku Warczysławowym bieżyć, nim wszelako kroku postąpili, zjawa w kapturze z cienia wychynąwszy, gestem onych powstrzymała.

– Wybaczcie, dobrzy ludzie, żem słowa wasze posłyszał, choć to beze chęci było – kaptur się ozwał przyjaźnie – rad byłbym z wami gawędę uciąć.

– Jakeś bez chęci co usłyszał, przechodniu zacny, przepomnij owo i pójdź drogą swoją, jakoby dźwięki nijakie cię nie doszły – brat Michał zamruczał.

– Trudno będzie, kiej jeden huczy, zasię drugi piska, bo to się we łbie jak w glinie odciska – dowcipnie kaptur słowa złożył.

– Nie przystoi wszelako, iżby co posłyszawszy, ludziów przez to zaczepiać i rozumy onym suszyć; w szczególności zwłaszcza, kiedy słowa dosłyszane grzechu nijakiego nie zdradzają – brat Michał zganił.

– A czy rzecz to pewna, iże słowa wasze nie są aby świadectwem grzechu, choć go występkiem nazywać wolę? Możeście niewolni, co pana odbiec zamyślają podstępnie? – po rozumie kaptur się skrobał zadumany.

– W świecie to nie widziane, iżby u władcy chrześcijańskiego mnich w niewolnym poprzestawał stanie; tedy płonne obawy twoje. Patrzaj na szaty moje.

– Jeśli nawet i nie w niewoli, to w posłudze pewnikiem. Zali to nie zdrada ode powinności jakich bądź umykać? – wesoło się kaptur przekomarzał – A też zakonnik brańcem przecie z wojny być może… No i niewolny suknie z mnicha zedrzeć zdolny jest pewnikiem, kiedy zbrodnię zbiegostwa umyśli sobie; kto wie, zali mnich prawdziwy nie pobity przy tym abo i ducha pozbawiony – u kaptura troski na kształt królików się mnożyły.

– Barzo się to chwali, iżeś grzech wszelaki tropić ochotny, wszelakoś tego nie zauważył, że przy tym sam w grzech popadasz, a to taki, iż do bliźniego ufności nie masz i złości jeno, nie poczciwości się doszukujesz usilnie. Jakie racje masz po temu, że z grzesznikami przestajesz?

– Nie mam ci ja i czepliwością brzydzę się własną, bom jest człek łagodny w sobie – poczciwie kaptur westchnął. – Jednakowoż mir jest zaprzysiężony i powinność na każdym ciąży, iżby badać wnikliwie, zali po majdanie zbrodnie jakowi się nie włóczą. Przyznaję wszelako, że ufność równie co ostrożność pochwałów jest godna, a może i barzej cenić ją należy; ale by rozumu nad tym nie łamać i tym się nie trapić, jaka z owych cnót właściwszym jest w tej przygodzie przewodnikiem, podążmy po prostu do Warczysława, knezia i jego spytajmy, czy co przeciw waszemu odejściu nie przemawia.

– Aleć nie odchodzić chcemy, jeno do knezia dołączyć, jeśliś dobrze posłyszał –ozwał się brat Michał dobrotliwie.

– Ale czy aby nie uradziliście opuścić onego niewdzięcznie, jako dola jego ku marnemu pójdzie? – kaptur się zasmucił.

– Że czepliwość swoją potępiasz, to przewiduję, że łacno dojdziesz, jeśli ino myśli skupisz, iżeś niesprawiedliwie słowa nasze pojął – spolegliwie Obijmorda przemówił. – Czemu zresztą, bokiem spytam, dola kneziowa miałaby ku marnemu iść? Aleć po co czas darmo trwonić, słowa rozumem żmudnie rozbierać, a knezia bez potrzeby niepokoić? Chciałeś gawędę uciąć, niech więc tak będzie. O czym gawęda owa?

– Wiedziałem ja, iż do zgody miłej dojdziemy – rozpromienił się człek tajemnego oblicza. – Od razu lubością braterską ku wam żem był zapałał, boście są poczciwi z wyglądu i z tej to przyczyny strapiony jestem wielce, od kiedym o zgubie żałosnej pomyślał, co was czekać może. A wielce zguba owa do prawdy podobna się wydaje, skoro Warczysława, knezia, się trzymacie wtedy akuratnie, kiedy gwiazda onego gaśnie, zasię kneź rzeczony ku przepaści za sobą ludzi swojech ciągnie. Biada, biada nieszczęśnikom!

– Skoro obawa taka serce twoje toczy, to czy umiłowanie nas niegodnych nie powinno cię do pochwały naszego zbiegostwa mniemanego skłaniać, miast do ucieczki bronienia popychać?

Kaptur prawością czystą zajaśniał, kiedy odpowiadał godnie:

– Chętnie widzę, jako się kto od zguby ratuje, jeno tako być nie może przecie, iże występkowi dzięki. Oj, nie! Nie tak dolę sprawiedliwi odmieniają, że źle czynią, ino że za dobrem stoją!

– Za jakim dobrem stanąć trzeba, iżby z owej matni wyjść? – prostodusznie mnich zapytał.

– Czyż ulubieniem na ulubienie prawość odpowiadać nie nakazuje? Umiłujcież mnie jakom i ja was umiłował – poczciwie kaptur odpowiedział.

– Serce roście, kiej się pomyśli, jako to lekce będzie w przepaść spadać o tym wiedząc, iże lubość braterska dusze nasze łączy – wyrzekł brat Michał rozczulony.

– Barzej jeszcze uroście, kiej się do myśli obraz takowy dopuści, iże spadającego braterska dłoń pochwyca i na opoce bezpiecznie sadza.

– Barzo to niebezpiecznie, kiedy kto na krawędzi spadającego chwyta – wyrzekł Obijmorda wdzięcznie. – Poświęcenie wonczas się objawiające tak wielkim się jawi, iż nie sposób jakowymś znamienitym uczynkiem nie odpowiedzieć na owe. Czym dobroć twoją nagrodzić możem?

– Nie dla nagrody miłością kogo obdarzam – wielkodusznie kaptur odrzekł. – Jakim bądź przyjaźni okruszkiem się zadowolę, spojrzeniem ciepłym a słówkiem miłym. Skoro wszakoż potrzebę mi dogodzenia czujecie, wiedzcie oto, iż śpiewaniem się param o czynach sławnych a dziejach niezwyczajnych, a czynię tako dla serców krzepienia i by pamięć nie ginęła. Czuję sobą i wy takoż chyba, iż to, czego świadkami jesteśmy, na wieki na Słowiany zaciąży, przeto mus w sobie czuję, by przypadki się tu jawiące rzetelnie w słowy ubierać i świadectwo onym wierne dawać. Przeto też radość mi sprawicie, kiedy o mowach a uczynkach w przytomności waszej się odbywających prawdziwie mi opowiecie. Że pamięć ulotna jest i beze przerwy co z niej umyka, uwiadamiajcie mnie najszybciej jako się wam uda i tego tylko od was pragnę. Dodam, iże dostatków nijak sobie nie cenię, przeto też radośnie podarki temu dawam, kto wieścią ciekawą mnie uraczy.[![Kronika Chichrały](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)