---
id: 189
title: 'Zima to dobra pora dla bałwanów'
date: '2014-11-11T23:12:37+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=189'
permalink: /2014/11/zima-to-dobra-pora-dla-balwanow/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony z lat 90-tych'
tags:
    - chirurdzy
    - górale
    - morsy
    - 'sporty zimowe'
    - 'turystyka zimowa'
    - 'zbieracze leśnego runa'
---

**ZIMA TO DOBRA PORA DLA BAŁWANÓW**

Idzie zima. Niektórzy się cieszą, niektórzy nie. Są turyści specjalizujący się w łazęgostwie białym i ci zimę lubią. Mniej lubią zimę turyści praktykujący zamaczanie, bo zamaczanie zimą nie dla każdego jest przyjemne; dla marynarzy i innych topielców nawet zgubne. Ale i tu są wyjątki w postaci tak zwanych morsów udających dzielnie, że moczenie się zimowe sprawia im przyjemność. Dodajmy na marginesie, że to, co morsom sprawia (rzekomo) przyjemność, nie nazywa się „moczeniem białym”. „Moczenie białe” to miano zarezerwowane raczej dla zmaczania pościeli, czemu oddają się z lubością niemowlęta, a nie morsy. Morsy zresztą też się temu oddają, ale nie z lubością, lecz chcąc nie chcąc ze względu na chroniczne zapalenie pęcherzy. Zimy nie lubią też naturyści i plażofile z powodów dość podobnych do powodów, na które powołują się zamaczacze nie będący morsami. Nie szaleją też za białym szaleństwem grzybiarze i inni zbieracze leśni.  
Wbrew rozpowszechnionym opiniom górale nie lubią zimy, lecz gotowiznę. Ponieważ lubią gotowiznę, nie piorą ciupagami narciarzy i saneczkarzy, czy łazęgów białych, a wręcz przeciwnie namawiają ich do sportów zimowych w warunkach górskich; a że ich namawiają, to muszą głosić, iż zima to najpiękniejsza pora roku. Wtórują im chirurdzy i wytwórcy gipsu, bo też żyją z turystyki białej. Tymczasem niedźwiedzie lubią zimę naprawdę, bo się mogą wyspać. Tak samo nauczyciele, młodzież szkolna i studenci – przynajmniej w okresie ferii.  
Urlopujący właściciele samochodów mają odmienne zdanie niż urlopujący narciarze i saneczkarze, bo samochody mają akumulatory i rozruszniki w odróżnieniu od nart i saneczek. Za komuny pracujący samochodziarze mieli podobne zdanie, co urlopujący narciarze i saneczkarze, gdyż nagminnie zaskakiwane zimą służby drogowe umożliwiały im urlopowanie w godzinach pracy. Dziś, w dobie kapitalizmu, urlopujący samochodziarze zgadzają się z pracującymi narciarzami i saneczkarzami co do tego, że zimą tylko bałwanom jest dobrze.  
W porze ciepłej wśród drzew, krzewów, traw i ziół wszelakich młodzież koedukacyjnie staczała się do poziomu, rodząc trudności wychowawcze. Zimą toczy jeno kule śniegowe, z czego poczynają się wspomniane już przed chwilą bałwany. I tu plus dla zimy. Z drugiej strony jednak, po co nam tyle bałwanów? Czy mamy ich za mało? I czy młodzież letnią porą nie mogłaby lepić babek z piasku, strugać lipowe figurki, albo uprawiać korzenioplastykę?  
Jak widać, zimą wszystko jest względne. Jedni się nią cieszą, inni – nie. Turyści uprawiający łazęgostwo białe nie cieszą się, gdy sobie odmrożą uszy. Nie sprawia im radości ani spadająca na łeb lawina, ani obudzenie śpiącego niedźwiedzia. Są tacy entuzjaści turystyki białej, którzy lubią łazęgostwo optyczne; ci się cieszą zarówno spoglądając na zamieć za oknem, jak i kontemplując lśniące w słońcu szczyty, gdy jednocześnie sączą sobie rum ze znajomym bernardynem w bliskości schroniskowego kominka. Co do morsów, to ich szczęście też jest względne. Istnieje taki podgatunek morsów, który osiąga rozkosz w momencie, kiedy wylezie wreszcie z przerębla, opatuli się w kożuszysko i łyknie herbaty ze spirytusem. Naturyści i plażofile nie lubią zimy, ale lubią niektóre jej skutki, jak na przykład konieczność spotykania się w pomieszczeniach zamkniętych czy solariach, co niewątpliwie sprzyja zacieśnianiu kontaktów. Zwłaszcza, jeśli szwankuje ogrzewanie i nie da się zacieśniać inaczej, jak pod pierzyną. A grzybiarze i inni zbieracze leśni rozpierają się białą porą w fotelach i konsumując owoce letniego trudu, zacinają sobie w brydżyka albo w baśkę – zależnie od upodobań. Głośno sarkają na brak kontaktu z gęstwą, ale z drugiej strony nie zacieka im za kołnierz, nie przemakają odnóża i nie kąsają ich komary ni kleszcze. Nie muszą się opędzać od wściekłych lisów, choć z drugiej strony brak nadziei na spotkanie z zieloną gąską. Cechuje ich w tej kwestii zmienność nastrojów, co może mieć coś wspólnego z wynikami, jakie osiągają w zmaganiach karcianych.  
Podejrzewamy, że również premier Balcerowicz podchodzi do zimy z tak zwaną ambiwalencją. Pora ta wydaje się, co prawda, całkiem odpowiednia do schładzania gospodarki, ale nieprzyjemna z powodu powarkiwania związków zawodowych na podwyżki w zakresie elektryki (zimą ciemno) i ciepłej wody (mało kto poza niektórymi Sybirakami i Skandynawami myje się śniegiem).  
Górale lubią raczej gotowiznę niż zimę, ale z drugiej strony, kiedy zawieje, nie muszą uprawiać ani kamienistych poletek, ani też owiec nad ruską granicą. Mogą za to uprawiać karczemne spotkania trzeciego stopnia i góralską muzykę. Mogą się też przewietrzyć podczas licznych wydłubywań zwolenników turystyki białej spod lawin i z oblodzonych stoków. Chirurdzy górscy uzyskują zimą, prócz dóbr materialnych, poczucie zawodowej wartości, jednakowoż zmuszeni są przed gipsowaniem wchodzić w kontakt z niezliczoną ilością śmierdzących skarpetek swoich pacjentów.  
Tym niemniej niedźwiedzie, studenci, nauczyciele i dziatwa chwalą sobie.