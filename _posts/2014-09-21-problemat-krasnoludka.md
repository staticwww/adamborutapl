---
id: 78
title: 'Problemat krasnoludka'
date: '2014-09-21T11:56:15+02:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=78'
permalink: /2014/09/problemat-krasnoludka/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Artykuły naukowe'
tags:
    - Arystoteles
    - 'elektryzowanie przez podcieranie'
    - filozofia
    - krasnoludek
    - powszechnik
    - skrzat
    - skrzatologia
    - 'Tomasz z Akwinu'
    - uniwersale
---

Adam Boruta

mgr nauk filozoficznych

**PROBLEMAT KRASNOLUDKA W DZIEJACH FILOZOFII**

{Praca napisana w ramach badań interdyscyplinarnych „Krasnal 99”)

Krasnoludek, istota jego, a także skrzatologia, czyli nauka o nim, to istna kopalnia problematów, stanowisk i dyskusji jakżeż pouczających dla filozofów! Z wielką szkodą jest dla ontologów, epistemologów, historyków nauki, teoretyków i anegdociarzów, że to bogactwo tak zapoznane zostało. Niechże więc choć ten szkic niewielki przyczyni się do zmiany owego stanu rzeczy.

Wspomniał już krasnoludków w swej „Zoologii” Arystoteles, ale tylko w aspekcie przyrodniczym. Za to uczeń jego, święty Tomasz, ugiął się pod brzemieniem paradoksu krasnoludka; nie mogąc mu poradzić, zmilczał o nim, jak o tym świadczy „Summa teologiae”. Bo też sprawa była trudna: krasnoludek nie posiadał powszechnika w sobie. Oskarża się scholastyków, iż empirię mieli w pogardzie, a ileż to oględzin i namacań przedsięwzięli! Na oczach całego uniwersytetu w Padwie mistrz Małodobry, anatom, rozbierał krasnoludka i nadaremno. Choć rozbierany wyłonił z siebie coś, co mistrz, w palce ująwszy, po słowiańsku nazwał („nic to” oddał tłumacz po łacinie), nie był to powszechnik. Wyglądało na to, że krasnoludki są konkretne jeno, zaś krasnoludka-w-ogóle nie ma wcale. „Nie masz ci krasnoludek powszechnika w sobie – pisał współczesny magister – przeto ci, co małej wiary, wywiedli, że i przed sobą nie ma”. W rzeczy samej pojawił się kacerz Anonim; głosił on, iż krasnoludek bytem jest niestworzonym i wiecznym. Nauka jego przetrwała, acz zniekształcona: stąd płynie źródło przesądu, że krasnoludek to bzdura niestworzona i że krasnoludka w ogóle nie ma. Jak widać, na gruncie tomizmu paradoks krasnoludka był nie do rozwiązania bez popadnięcia w herezję; wielu też się na tym sparzyło.

Problem podjął po latach dopiero Hegel. Rozstrzygnięcie takie jest oto: Niemający istoty, jest krasnoludek zjawiskiem tylko, pozorem przeto, a więc – niebytem; wszelako jako fenomen wyłania się z niebytu w byt i będąc wtenczas pozornie, jawi się jako zaprzeczenie krasnoludka nieistniejącego, jego innobyt. Staje się zatem jasnym, że usiłujący istnieć, stanowi nieistniejący krasnoludek oczywistą sprzeczność; nie dziwota przeto, iż odwraca się od bytu, nie kwapiąc się wszelako z powrotem w niebyt. I tak dokonuje się synteza: następuje zniesienie sprzecznego krasnoludka w odbyt, gdzie sobie odbytuje jako zgoła nowa jakość. Ogłoszona dopiero w latach dwudziestych naszego wieku, wielkiego ta koncepcja doznała odporu. „Merde” – wołali zacietrzewieni neopozytywiści ortodoksi, zaś ich umiarkowani konfratrzy zadawalali się stwierdzeniem „Nic to, paradoks krasnoludka to problem metafizyczny; odbyt nie ma sensu”. Protesty te ostatnio cichną, a słychać nieśmiało, że co prawda, nie godzi się ujmować realistycznie odbytu, ale wcale zeń dogodny instrument.

Rzecz prosta, problemu krasnoludka nie rozstrzygano spekulatywnie tylko; krasnoludkiem zajęły się i nauki empiryczne. Filozoficzne konsekwencje miały eksperymenty z elektryzowaniem krasnoludków przez podcieranie. Konsternację wzbudziło odkrycie, że niektóre krasnoludki ładują się przy tym dodatnio, inne zaś – ujemnie, choć oba rodzaje z równą mocą przyciągają okowitę. Indeterminiści triumfowali: oto był dowód przeciw wszechobecności związków przyczynowo-skutkowych. Determiniści bronili się słabo: nie wszystkich przekonywało, że mikroświat jeno jest anormalny; w końcu okowita przyciągana jest również przez obiekty makro. Sprawa długo nie dawała się wyjaśnić; tym bardziej, że znikała materia, którą podcierano. Kiedy się znalazła, zatriumfowali adwersarze z kolei, bo krasnoludki naładowane dodatnio podcierane były bezwyjątkowo materią miękką i gładką, zaś naładowane ujemnie – twardą i chropowatą.