---
id: 353
title: 'Pociecha ze smutnego początku roku'
date: '2015-01-25T13:58:50+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=353'
permalink: /2015/01/pociecha-ze-smutnego-poczatku-roku/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - górnictwo
    - 'Nowy Rok'
    - optymizm
    - pesymizm
    - 'Porozumienie Zielonogórskie'
    - 'służba zdrowia'
    - strajk
    - terroryzm
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/02/Pociecha-ze-smutnego-początku-roku.pdf)

POCIECHA ZE SMUTNEGO POCZĄTKU ROKU

Od wieków ludzkość absorbuje problem butelkowy: gdy jedni widzą, iż butelka jest już połowie pusta, to drudzy przekonują, że wciąż jest przecież w połowie pełna. Ci pierwsi zwani są pesymistami, zaś drudzy noszą miano optymistów.

Problem butelkowy różnie może być rozwiązywany. Za granicą jest tak na ogół, iż pesymiści ostrzegają, że zaraz zabraknie, zaś optymiści są zdania, że wystarczy. W związku z tym ci pierwsi postulują, by zapełnić pustkę grami towarzyskimi, zaś ci drudzy radzą, by wzbogacić imprezę o gry zespołowe. W Polsce pesymiści rwą się do biegu w kierunku sklepu, aby ratować imprezę przed klęską, zaś optymiści zmierzają żwawym spacerkiem do monopolowego, aby imprezie zapewnić świetlaną perspektywę.

Pan Zdzicho, wkraczając w Nowy Rok, minę miał rzadką. Włos się jeży – powiedział – bo lekarze wyrzucają pacjentów z gabinetów, gabinet narzuca górnikom redukcję, zaś redukcję satyryków zarzuca się islamistom. Jeśli na podstawie pierwszych dwunastu dni wyrokować o całym roku, to zapowiada się istne pasmo nieszczęść – dodał. Nie jest jednak do końca pewne, czy pan Zdzicho jest pesymistą czy optymistą, bo udał się do lokalu „U Henia”.

Znajomy obcokrajowiec okazał się sceptykiem w materii polskiej służby zdrowia. Kasa jest w połowie pusta – stwierdził – zatem trzeba postawić na niepubliczne placówki medyczne. Jego małżonka zachowała pogodę i nawoływała do tego samego, bo choć kasa jest przynajmniej w połowie pełna, to jednak płacąc za usługi, będziemy mogli czegoś wymagać i w tym sensie się nam poprawi. Do nas to nie przemówiło. Optymiści nadużyli za zdrowie medycyny w domowych pieleszach lub gościnnych lokalach ufając, że kryzys sam przejdzie, pesymiści spełnili toasty ze znajomymi funkcjonariuszami służby zdrowia pamiętając z młodości, że w sytuacji permanentnego niedoboru cokolwiek w garści ma ta tylko ręka, co inną rękę myje. Tak czy owak pomogło, gdyż lekarze na nowo otwarli podwoje. Pan Zdzicho rozpowiada, że pomogło, bo minister Arłukowicz, kiedy z kumplami z Zielonej Góry operował medycynę narodową, często gęsto wołał o płyny do odkażania narzędzi negocjacyjnych; ale w to nie wierzcie, bo pan Zdzicho to znany złośliwiec, a po kryjomu hejter.

Na początku prorządowy element etnicznie niepełnowartościowy wołał, by zamykać najgorsze kopalnie, żeby reszta nie upadła, albo też by zamykać, żeby się reszcie poprawiło. Myślący kategoriami racji stanu rodacy z opozycji oburzali się na takie sugestie, bo byli pewni, że dla dobra narodu trzeba fedrować do upadłego, iżby było czym palić, a Polska z braku energii nie zginęła. Dla rodaków spoza sfer popierających koalicję, którzy myślą kategoriami solidarności społecznej, było oczywiste, że trzeba fedrować na całego, bo Polacy kochają ciężko pracujących górników i chętnie zapłacą za tonę węgla te głupie kilka setek więcej, byle tylko było czym palić, a branża górnicza nie zginęła. No i proszę, ten optymistyczny pogląd na współczującą naturę Polaków okazał się słuszny: sondaże dobitnie wykazały, że większość stanęła za strajkującymi. Cyniczny rząd z jednej strony naciskany przez rację stanu, a z drugiej przez sumienie narodu po krótkim oporze padł na kolana, wyznał grzech niewiary i nawrócony zaufał górnikom, że zredukują się sami. Pan Zdzicho rozpowiada, że dlatego zaufał, bo przewodniczący Duda często gęsto podczas negocjacji wołał o płyn do rozpalania opon, ale temu nie wierzcie, bo pan Zdzicho to znany złośliwiec, a po kryjomu hejter.

Niektórzy naiwni Francuzi sądzą, że jeśli za dużo jest świętokradczych pism satyrycznych, to można je ograniczyć przestając je kupować, inni znów myślą, że na szczęście jest ich mało i można się zabezpieczyć przed niepożądaną ich ekspansją poprzez powstrzymywanie się od zakupów. Wojujący islamiści smutnego oblicza sądzą, że istnienie świętokradczej satyry jest wyzwaniem rzuconym Bogu, a taki grzech zmyć należy likwidując satyryków, wojujący islamiści pogodnego usposobienia uważają z kolei, że taka satyra jest doskonałą okazją do wykazania się odwagą w eliminowaniu satyryków. Wyzuci z krwiożerczości acz głęboko zaangażowani w wierze mahometanie i chrześcijanie stoją na wspólnym stanowisku, że egzekucja świętokradczych satyryków jest złem; jedni przy tym głoszą, że ofiary przynajmniej częściowo same są sobie winne, a inni – że ich godny ubolewania los ostrzeże być może innych przed występkiem. Pesymiści pośród państwowców ubolewają nad tym, że zamachowców zastrzelono dopiero po udanym zamachu, optymiści pocieszają się tym, że po wkrótce zamachu zamachowcy zginęli zastrzeleni. Nie będziecie specjalnie zdziwieni dowiadując się, że według pana Zdzicha wiele wspólnego z takim finałem ma płyn do konserwowania kałasznikowów; nie warto jednak traktować tych wynurzeń poważnie, bo pan Zdzicho to znany złośliwiec, a po kryjomu hejter.