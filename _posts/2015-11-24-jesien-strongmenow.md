---
id: 534
title: 'Jesień strongmenów'
date: '2015-11-24T07:52:31+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=534'
permalink: /2015/11/jesien-strongmenow/
interface_sidebarlayout:
    - default
categories:
    - 'Felietony XXI wieku'
tags:
    - jesień
    - 'kot w worku'
---

JESIEŃ STRONGMENÓW

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/11/Jesień-strongmenów.pdf)

Nastała definitywne jesień. To pora roku kojarząca się z szarością, schyłkiem i dołem. Nie dajcie się omamić mitem złotej jesieni. Tak zwana złota jesień to przerywnik, anomalia i odstępstwo. Chłód, mżawka, mgła i zdechłe liście to rutyna.  
Jesienią perspektywa niknie za oparami jak zakutana kobieta za parasolką. To kot w worku. Jesienią kot pozbawiony worka okazuje się wyleniały, miaukliwy i bez perspektyw – nawet mglistych.  
Słabnie życie towarzyskie. Rozpakowanie kota jest łatwe latem, jesienią dużo z tym zachodu i niektórzy zasypiają w trakcie. A też koty są mniej chętne do rozpakowywania, bo perspektywa mglista, a rozpakowujący senni.  
Jesienią słabnie zapał, a więc i dokonania. Nie bardzo się chce, bo się chce spać. Jak się pójdzie spać, to się spania odechciewa. Jak się wstanie, to się nie bardzo chce, ale za to zachciewa się spać. Półsen, półmrok i opad połowiczny – czyż to nie jesień aby?  
Jesienny marazm nie omija też sportowców. Dyscypliny letnie sposobią się do zimowego snu, zaś dyscypliny zimowe nie rozbudziły się jeszcze z letniej drzemki. Zastój, szarość, brak wyników.  
\*  
Kiedyś o tej porze liczyć można było trochę na strongmenów. Wymyślali oni od czasu do czasu specjalne konkurencje jesienne. Ciągnęli ciężarówki stąpając po mokrych liściach albo rozpraszali mgły nad miastami porywistym chuchem.  
Tej jesieni również strongmeni zawodzą. Potężny Litwin zgolił brodę, za pomocą której zaprzęgał się niegdyś do wyładowanych po brzegi tirów. Mocarz z Kolorado rozłupujący tomahawkiem sekwoje popadł w alkoholizm, a silny człowiek ze Szwecji porzucił zgniatanie samochodów rękoma i dzierga smętnie swetry na drutach. Krążą pogłoski o samobójstwie wyczynowca, który dwa lata temu złapał startującego F-16 w siatkę na motyle – a we mgle to było. Mnożą się wśród strongmenów oznaki depresji, słychać przepowiednie o końcu dyscypliny.  
Skąd te nastroje, pytacie. Cóż, podobno jeden gość z Polski zapowiedział, że wyciśnie półtora biliona z ruiny. Tego się nie przebije.