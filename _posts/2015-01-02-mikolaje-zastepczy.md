---
id: 339
title: 'Mikołaje zastępczy'
date: '2015-01-02T18:55:29+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=339'
permalink: /2015/01/mikolaje-zastepczy/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - ateista
    - bilokacja
    - choinka
    - gender
    - kilometrówka
    - prezenty
    - satanista
    - Sejm
    - 'święty Mikołaj'
---

MIKOŁAJE ZASTĘPCZY

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/01/Mikołaje-zastępczy.pdf)

Pan Balcerowicz zasugerował nam kiedyś, że święci Mikołaje przenieśli się do Sejmu i tam kultywują rozdawnictwo. To może wiele wyjaśniać! Na przykład to, że u rodziców, gdzie spędziłem wigilię, darów nie dobywał z wora wiekowy święty Mikołaj z brodą, lecz przydzielał je z rozdzielnika gołowąsy student prawa z mało używanym dowodem osobistym.

Pan Zdzicho też nie uświadczył w domu świętego, lecz znaną mu od lat małżonkę. Ma cichą pewność , że to ona podrzuca mu od lat pod choinkę rozmaite elementy garderoby; no przecież, gdyby prezenty dobierał święty Mikołaj, co zna najskrytsze chucie swoich podopiecznych, miast tekstyliów pan Zdzicho z pewnością znajdowałby pod drzewkiem akcyzowane wyroby przemysłu spożywczego lub gładkolice, sprawne studentki z mało używanymi dowodami osobistymi.

Rozumiemy doskonale, że Mikołajowie wolą się spełniać w Sejmie: i budżet mają tam wielki, i uszczęśliwiać mogą na skalę masową. Spełniając się na rodzinnych zgromadzeniach choinkowych, robiliby w detalu, gdzie choć pot zalewa czoło i trapi zadyszka, to wynik i tak okazuje się skromniejszy. Ale pana Zdzicha i wielu innych obywateli to rozumowanie nie przekonuje i jednak chcieliby – jak to elektorat – bezlitośnie orać świętymi Mikołajami. W okresie świątecznym sesji się nie odprawuje – powiadają – i to najlepsza pora na indywidualny kontakt z wyborcami, a zatem na wypełnianie tradycyjnych ról mikołajowych.

Nie wolno nam wątpić w ideowość świętych Mikołajów, lecz wiemy z dobrze poinformowanych źródeł, że na indywidualne podarunki choinkowe Sejm środków nie ma. I tu raz jeszcze wychodzi na jaw wstydliwe zakorzenienie Trzeciej Rzeczypospolitej w słusznie minionej epoce bezbożnego komunizmu, kiedy to patologicznie przedkładano konsumpcję zbiorową nad indywidualną. Stronnictwo pana Zdzicha ignoruje jednak ten problem głosząc, że nie zależy mu na budżetowym finansowaniu tradycji mikołajowej, bo satysfakcjonuje go uświęcone odwieczną praktyką czerpanie świętych Mikołajów ze źródeł nadprzyrodzonych; to nawet lepiej dla budżetu – stwierdza cynicznie!

Może jednak nie bądźmy dla pana Zdzicha i jego kompanii nadmiernie surowi. Może to nie cynizm ich trawi od środka, a zaledwie lekkomyślność. Nie zdają sobie sprawy, nieszczęśni, z naporu wojujących ateistów, satanistów, dżenderów i wódstoków, na których nadprzyrodzone pochodzenie niebieskie działa jak czerwona płachta na byka. Tradycję i świętość mają ci grzesznicy za nic, a profanacja jest dla nich sposobem na życie. Kto chciałby się znaleźć w skórze świętego Mikołaja przemykającego się z workiem na grzbiecie pośród patroli bezbożników tropiących faryzejsko towary niewiadomego pochodzenia wskazujące na nieudokumentowane źródła dochodów?

Kiedy powyższa analiza została przedstawiona publiczności w lokalu „U Henia”, okazało się, że gromadzą się tam twardogłowi wyznawcy pana Zdzicha, którzy dali natychmiast odpór. Jedni wołali, że nie usłyszeli w owej analizie żadnego usprawiedliwienia absencji świętych Mikołajów pod ich choinkami, bo parlamentarzysta musi mieć grubą skórę, a przynajmniej twarde siedzenie. Drudzy jęli się pukać w czoła pytając, gdzie kto widział owe hordy prześladowców zjawisk nadprzyrodzonych. Dżendery, wódstoki, sataniści i ateusze wegetują gdzieś na marginesie, z rzadka tylko wychylając się z podziemia – krzyczeli ze śmiechem.

Niech będzie, może zbyt grubą kreską naszkicowaliśmy zagrożenie, wszakże publicystyka ma przecież prawo do używania środków stylistycznych od lat sprawdzających się w dziele potęgowania siły przekazu. Jasne, że sataniści, dżendery i ateiści działają głównie w podziemiu lub w niszach, ale już wódstoki w biały dzień i w miejscu publicznym potrafią się zgromadzić w budzącej grozę liczbie. Zdarza się to zresztą sporadycznie i tym pozostałym grzesznikom, na szczęście występują mniej tłumnie i na krótko. Ale wróćmy do głównego wątku.

Sataniści, wódstoki, ateiści i dżendery działają może niejawnie, lecz skutki ich kreciej roboty widoczne są gołym okiem. Do niedawna święci Mikołajowie nie mieli trudności w pozyskiwaniu od Sejmu siana na renifery. Teraz nie wystarczają ich oświadczenia, żąda się od nich szczegółowych wyliczeń przebytych kilometrów. Obłudni oskarżyciele czynią im zarzuty nawet wtedy, kiedy dla ulżenia zwierzętom przesiadają się z sani na samoloty. Ci sami prześladowcy nie chcą słyszeć o tym, że święty Mikołaj może w tym samym czasie znajdować się w dwóch różnych miejscach. Proszę odpowiedzieć, jak nazwać kogoś, kto nie wierzy w nadprzyrodzone przypadki bilokacji? Czy to nie ateista aby?

A kto nie może uwierzyć w to, że temu czy innemu parlamentarzyście pojawiło się coś cudem na koncie albo w kopercie? Tylko dżenderowi, ateuszowi albo wódstokowi nie pomieści się w głowie, by na konto świętego Mikołaja przepłynęło coś z niepojętego dla bezbożnika źródła. Słysząc o czymś takim, popada ów bezbożnik w hipokrytyczne oburzenie, woła o CBA i postuluje komisje śledcze. Potem się dziwi, że święci Mikołaje nad rodzinne zgromadzenia choinkowe przedkładają sesje parlamentu. Nawiasem mówiąc, jedynie sataniści pośród wymienionej czeredy rokują jakieś nadzieje poprawy, bo przynajmniej nie przeczą zjawiskom nadprzyrodzonym. Może wystarczyłoby, gdyby po prostu zmienili przynależność klubową.

W te i na inne sposoby uniemożliwia się świętym Mikołajom wykonywanie ich misji. Czy można dziwić się potem, że zamiast prawdziwych pojawiają się Mikołajowie zastępczy, jak choćby studenci prawa lub gospodynie domowe sterane pożyciem z panem Zdzichem lub jemu podobnymi?