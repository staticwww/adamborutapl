---
id: 375
title: 'Kwestia zimowa w aspekcie politycznym'
date: '2015-03-15T19:06:27+01:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=375'
permalink: /2015/03/kwestia-zimowa-w-aspekcie-politycznym/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - 'brak zimy'
    - ludowcy
    - palikociarnia
    - 'paliwa kopalne'
    - Rosja
    - sankcje
    - Ukraina
    - 'Unia Europejska'
    - USA
    - zima
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/03/Kwestia-zimowa-w-aspekcie-politycznym.pdf)

KWESTIA ZIMOWA W ASPEKCIE POLITYCZNYM

Nie doczekaliśmy zimy i wszystko na to wskazuje, że to oczekiwanie wypadnie zakopcować w ogródku, jeśli się aby pragnie, by nie stajało do następnego roku.

Skąd ten brak zimy? – zapytuje pan Zdzicho z troską.

Jego znajomy, niejaki pan Rycho (za komuny w aparacie) zwraca uwagę na nadmiar akcesoriów zimowych w USA. Mrozy, śnieżyce i oblodzenia tego roku przekroczyły tam ponoć zwykłą miarę. Ani chybi imperializm jankeski zawłaszczył to, co należało się środkowo-wschodniej Europie, nie bacząc na płacz dziecin naszych za śnieżkami, bałwankami, saneczkami i łyżwami. A wszystko to z zajadłej chęci ograniczenia popytu na rosyjskie paliwa.

Znowuż pani Zocha, żona górnika, widzi tu raczej spisek unijno-platformiany wymierzony w przemysł węglowy. Wspiera się autorytetem opozycji, wedle której storpedowanie zimy miało na celu udowodnienie absurdalnego poglądu o nadmiarze gazów cieplarnianych w naszej atmosferze. Z Torunia słychać z kolei, że liberałowie z Komisji Europejskiej i zdegenerowanego salonu mieli dodatkowo na celu wspomożenie obcych nam ideowo wynaturzeń; jest wiadome z dawien dawna, że sroga zima nie sprzyja szkodnikom i zapewne spowodowałaby przetrzebienie hord dżenderów, satanistów, ateuszy i wódstoków. Rozumowanie owo podparto przypowieścią: azali nie jest prawdą, że gdyby był siarczysty mróz, pewien rozwścieczony dżender nie dałby rady parę godzin czaić się pod gmachem telewizji? Ale że mrozu zabrakło, radę dał i w końcu udało mu się ukąsić zaproszoną do programu posłankę; pod wpływem jadu nieszczęsna kobiecina długo bredziła przed kamerami o wyższości tacierzyństwa nad macierzyństwem w warunkach przewagi zarobków matczynych nad dochodami ojcowskimi – czyż nie jest wstrząsające, że poszkodowana nie wywodziła się bynajmniej z palikociarni?

W palikociarni – skoro już przy niej jesteśmy – nieobecność zimy tłumaczy się rozpadem połowicznym, jaki dotknął to szacowne grono i przybrał znamiona reakcji łańcuchowej; osoby wykształcone zdają sobie doskonale sprawę, że podczas takich procesów wydzielają się znaczne ilości energii, która podgrzewa atmosferę. Złośliwi działacze spod sztandarów konkurencyjnej partii lewicowej rozpowszechniają, co prawda, plotkę, iż w efekcie wspomnianej reakcji łańcuchowej wydzielił się raczej smród, a nie energia, lecz znajomy działacz ludowy objaśnił pana Zdzicha, że jedno nie przeczy drugiemu, bo odwieczne doświadczenie hodowcy podpowiada, że w smrodzie cieplej.

Ten sam ludowiec zapoznał pana Zdzicha z oficjalnym stanowiskiem swojego stronnictwa w kwestii zimowej: ograniczenie wymiany pomiędzy Rosją a Unią najwidoczniej rozciągnęło się na prądy atmosferyczne i stąd brak u nas rześkich powiewów znad Syberii. W ten sposób podtrzymywanie szkodliwych sankcji unijnych zemści się na nas niebawem globalnym ociepleniem i jego przykrymi symptomami w postaci powodzi, huraganów i trąb powietrznych.

Od tego poglądu zdystansowało się ministerstwo spraw zagranicznych, wedle którego ze wschodu wieje jednak chłodem, a podgrzanie atmosfery bierze się najpewniej z tarć pomiędzy członkami Unii nie tylko w kwestii ukraińskiej, lecz także finansowej; w szczególności Grecja, szarpiąc się w żelaznym uścisku międzynarodowej finansjery, rozgrzała się ostatnio do czerwoności.

Tyle o zimie. Są inne problemy. Czy inne pory roku, do których przywiązały nas obyczaj przodków, rodzima literatura i obce przesądy, też nie nadejdą? Pan Zdzicho ma najgorsze przeczucia.