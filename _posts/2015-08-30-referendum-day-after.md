---
id: 425
title: 'Referendum &#8211; Day After'
date: '2015-08-30T10:44:55+02:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=425'
permalink: /2015/08/referendum-day-after/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - Bralczyk
    - 'dyktatura jajogłowych'
    - 'lingwistyka jako czynnik polityczny'
    - 'logika a język'
    - lud
    - Miodek
    - populizm
    - Referendum
    - 'wojna jako instrument polityki postreferendalnej'
    - 'wola ludu'
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/09/Referendum-Day-After.pdf)

REFERENDUM – DAY AFTER

Obserwujemy wysyp pytań referendalnych. Jakaż to dobroć dla ludzi robiących w polityce owe referenda? Sądzą niektórzy, że dobroć wielka. Zważcie bowiem, że:  
Komu się uda doprowadzić do zarządzenia referendum, ten czuje się w prawie uchodzić za miłośnika ludu; czyż nie może mieć on nadziei na to, że z wdzięczności i lud jego pokocha?  
Komu nie uda się doprowadzić do zarządzenia referendum, ten czuje się w prawie uchodzić za ofiarę wrogów ludu; czyż nie może mieć nadziei na to, że wdzięczny lud postawi go na czele krucjaty przeciw swoim wrogom?  
Kto stał z boku, kiedy walczono o referendum, ten czuje się w prawie uchodzić za nieskażonego populizmem; czyż nie może mieć nadziei na to, że rozsądny lud uzna go za polityka, który nie boi się samodzielnego myślenia?  
Kto nie zdołał powstrzymać referendum, ten czuje się w prawie uchodzić za wizjonera, który posiadł nieoczywistą prawdę o tym, co dla ludu dobre; czyż nie może mieć nadziei na to, że kiedyś zagubiony lud obwoła go autorytetem?  
Kto zdołał utrącić referendum, ten czuje się w prawie uchodzić za polityka pewnego swych racji i skutecznego zarazem; czyż nie może mieć nadziei na to, że niezdecydowany lud obwoła go przywódcą?  
Jeżeli polityk nawoływał do referendum w nadziei, że do niego dojdzie i do niego doszło, może – jak już to wskazaliśmy – liczyć na miłość ludu, a oprócz tego:  
– jeśli władza znajdzie się rękach innych, będzie w prawie czuć się strażnikiem woli ludu i recenzować postępki rządzących; czyż nie będzie mógł mieć nadziei, że to zaprocentuje poparciem w kolejnych wyborach?  
– jeśli władza znajdzie się w jego rękach, będzie w prawie twierdzić, że lud niczego nie zdecydował , bo referendum było niewiążące, a zajadłość przeciwników i tak uniemożliwia realizację woli większości głosujących; czyż nie będzie mógł mieć nadziei na poparcie realistów?  
– jeśli władza znajdzie się w jego rękach, a referendum cudem okaże się wiążące, będzie w prawie twierdzić, że realizację woli ludu uniemożliwia koalicjant, z którym nie może przecież zerwać, gdyż dojście do władzy opozycji zatopi kraj w takiej niedoli, że wobec tego nieszczęścia fraszką będzie brak realizacji referendalnej woli ludu w nie najważniejszej przecież sprawie; czyż nie będzie mógł mieć nadziei na miano przywódcy tyleż ideowego co tragicznego i liczyć na uwielbienie licznego elektoratu romantyków?  
– jeśli władza znajdzie się w jego rękach, referendum cudem okaże się wiążące, zaś nielojalny koalicjant opowie się za realizacją woli ludu, będzie w prawie twierdzić, że wprowadzenie postanowień referendum jest niemożliwe ze względu na nieprecyzyjność pytań, która wyklucza odgadnięcie, czego właściwie chcieli wyborcy; czyż nie będzie mógł mieć nadziei na zrozumienie u przeciwników zmian?  
– jeśli władza znajdzie się w jego rękach, referendum jakimś cudem okaże się wiążące, koalicjant okaże się nielojalny, zaś pozbawieni wyczucia racji stanu profesorowie Miodek i Bralczyk ustalą obowiązującą wykładnię referendalnej woli ludu, będzie w prawie twierdzić, że lud nie posługuje się bynajmniej literacką polszczyzną, zaś polszczyzna – nawet w wersji literackiej – nie czyni zadość surowym kryteriom, jakie językom stawia logika; czyż nie będzie mógł mieć nadziei na zrozumienie elektoratu jajogłowych niebędących humanistami?  
– jeśli władza znajdzie się w jego rękach, koalicjant okaże się nielojalny, referendum i luminarze językoznawstwa narzucą mu wolę ludu, który okaże pogardę logice, zostanie mu ogłosić, że zagraniczne mocarstwa wywierają w sprawie postanowień referendalnych niedopuszczalną presję na ojczyznę i wypowiedzieć wojnę europejskim mocarstwom; czyż nie będzie mógł mieć nadziei, że po latach będzie czczony i honorowany pomnikami i nazwami skwerów na równi z Kościuszką, Trauguttem i Rydzem-Śmigłym?  
– jeśli władza znajdzie się w jego rękach, a koalicjant, wola ludu i profesorowie Bralczyk z Miodkiem, a także niedocenianie logiki zmuszą go do heroizmu, w wyniku czego francuskie czołgi podejdą pod Łódź, duńska flota wpłynie do Ustki, zdradzieccy pacyfiści rzucą broń, a do gmachów rządowych wedrą się z płonącymi oponami bojówki związkowe, będzie w oczywistym prawie ogłosić znużenie polityką i przejść do pracy naukowej w jakimś zacisznym instytucie nauk politycznych lub poświęcić się pracy organicznej w kameralnej radzie nadzorczej któregoś z podmiotów gospodarczych; czyż nie będzie mógł mieć nadziei, że po dwóch kadencjach sejmowych powróci do czynnej polityki jako mąż stanu, który dojrzał, przeszedł przemianę i jest jedynym remedium na nieudolność aktualnej ekipy?

Jeżeli polityk nawoływał do referendum w nadziei, że do niego nie dojdzie i do niego doszło, może – jak już to wskazaliśmy – liczyć na miłość ludu, a oprócz tego:  
– jeśli władza znajdzie się w rękach jego przeciwników, będzie w prawie czuć się strażnikiem woli ludu i recenzować postępki rządzących; czyż nie będzie mógł mieć nadziei, że to zaprocentuje poparciem w kolejnych wyborach?  
– jeśli władza znajdzie się w jego rękach, będzie w prawie twierdzić, że lud niczego nie zdecydował , bo referendum było niewiążące, a zajadłość przeciwników i tak uniemożliwia realizację woli większości głosujących; czyż nie będzie mógł mieć nadziei na poparcie realistów?  
-jeśli władza znajdzie się w jego rękach, a referendum jakiś cudem okaże się wiążące, będzie w prawie liczyć na lojalność koalicjanta, korzystną interpretację pytań referendalnych, wzrost poziomu wyrobienia politycznego profesorów Miodka i Bralczyka, przekonanie elektoratu, że logikę trzeba szanować z uwagi na to, że nie ma innej gałęzi nauki, w której rozwój Polacy wnieśliby tak wielki wkład, a także na to, że gnuśne mocarstwa europejskie wywieszą przed polską armią białe flagi, co pozwoli zapomnieć triumfującemu elektoratowi o nieszczęsnych decyzjach referendalnych; jeśli nawet to się nie uda i będzie musiał z godnością odejść, to czyż nie będzie mógł mieć nadziei, że po dwóch kadencjach sejmowych powróci do czynnej polityki jako mąż stanu, który dojrzał, przeszedł przemianę i jest jedynym remedium na nieudolność aktualnej ekipy?  
Jeżeli polityk przeciwstawiał się referendum w nadziei, że do niego nie dojdzie i do niego doszło, może – jak to już wskazaliśmy – liczyć na prestiż autorytetu moralnego, a oprócz tego:  
– jeśli władza znajdzie się w rękach jego przeciwników, będzie mógł ogłosić, że co prawda, nie był zwolennikiem referendum, jeśli jednak już do niego doszło, to ma niezłomne przekonanie, że wola ludu musi zostać uszanowana i dlatego stanie na jej straży i będzie surowo recenzował poczynania ekipy rządzącej w tej materii; czyż nie będzie mógł mieć nadziei, że zostanie ogłoszony polskim Katonem, co zaprocentuje w następnych wyborach?  
-jeśli władza znajdzie się w jego rękach, będzie w prawie twierdzić, że lud niczego nie zdecydował , bo referendum było niewiążące, a on sam nie był jego zwolennikiem; czyż nie będzie mógł mieć nadziei, że zostanie ogłoszony politykiem niezłomnych zasad, co zaprocentuje w przyszłości?  
-jeśli władza znajdzie się w jego rękach, a referendum jakimś cudem okaże się wiążące, będzie w prawie twierdzić, że realizację woli ludu uniemożliwia koalicjant, z którym nie może przecież zerwać, gdyż dojście do władzy opozycji zatopi kraj w takiej niedoli, że wobec tego nieszczęścia fraszką będzie brak realizacji referendalnej woli ludu w nie najważniejszej przecież sprawie; czyż nie będzie mógł mieć nadziei na miano przywódcy, który musi dokonywać wyboru mniejszego zła i liczyć na poparcie realistów?  
-jeśli władza znajdzie się w jego rękach, referendum cudem okaże się wiążące, zaś nielojalny koalicjant poprze wolę ludu, będzie w prawie twierdzić, że między innymi dlatego sprzeciwiał się referendum, że od początku było dla niego jasne, iż nieprecyzyjność pytań w nim zadanych wykluczy odgadnięcie, co właściwie postanowią wyborcy i w rezultacie uniemożliwi wprowadzenie w życie ich woli; czyż nie będzie mógł mieć nadziei na zrozumienie u przeciwników zmian i purystów językowych?  
-jeśli jeśli władza znajdzie się w jego rękach, referendum cudem okaże się wiążące, nielojalny koalicjant poprze wolę ludu, zaś autorytety językoznawcze w osobach profesorów Bralczyka i Miodka wesprą mimowolnie politycznego przeciwnika, który obrzydzi obywatelom logikę przez sprytne powiązanie tejże ze znienawidzoną przez lud matematyką, będzie w prawie twierdzić, że tylko zdecydowane posunięcia militarne wybawią kraj z opresji; gdyby to się nie udało i przyszło odejść z podniesioną głową, to czyż nie będzie mógł mieć nadziei, że po dwóch kadencjach sejmowych powróci do czynnej polityki jako mąż stanu, który dojrzał, przeszedł przemianę i jest jedynym remedium na nieudolność aktualnej ekipy?

Jeżeli polityk przeciwstawiał się referendum w nadziei, że do niego dojdzie i do niego doszło, może – jak to już wskazaliśmy – liczyć na prestiż autorytetu moralnego, a oprócz tego jak wyżej.

*UWAGA OBSERWATORZY RZECZYWISTOŚCI POLITYCZNEJ!*  
*We wszystkich prognozach napotykamy na nowy czynnik polityczny w postaci profesorów Bralczyka i Miodka; nieunikniona – jak wszystko na to wskazuje – i decydująca rola dziejowa tego czynnika rodzi u politologów poczucie kłopotliwego dysonansu w zestawieniu z brakiem mandatu demokratycznego czy jakiegokolwiek innego umocowania politycznego profesorów.*  
*RZUĆMY ŚMIAŁE NIE! DYKTATURZE JAJOGŁOWYCH!!*