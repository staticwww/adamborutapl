---
id: 641
title: 'Oblicza ludu polskiego'
date: '2017-07-23T20:01:11+02:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=641'
permalink: /2017/07/oblicza-ludu-polskiego/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - 'morda zdradziecka'
    - sądy
    - 'trójpodział władzy'
    - 'władza wykonawcza'
---

OBLICZA LUDU POLSKIEGO

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2017/07/Oblicza-ludu-polskiego.pdf)

Pan Henio, właściciel baru „U Henia”, restaurator ze specjalizacją w zakresie wyszynku, od dawna słynął jako animator gminnego życia kulturalnego. Tym razem jednak sprowokował dyskusję polityczną. A nie takim go znaliśmy.

Wczoraj mianowicie pan Henio plenum stałych bywalców obarczył podchwytliwym pytaniem o zasadność oddzielenia sądownictwa od władz ustawodawczej i wykonawczej.

Pan Janek, właściciel różowych okularów, wychylił się znad zlewozmywaka, przy którym odpracowywał zadłużenie wobec lokalu i stwierdził z właściwą sobie poczciwością, że skoro takie rozdzielenie się pojawiło, to widać jest pożyteczne.

– Ale czemu się pojawiło? – bąknął niepewnie pan Robert, znawca ufologii i gier komputerowych i jął mierzwić sobie czuprynę.

Wszystkie oczy spoczęły na pani Jadzi, uczonej konsumującej. Pani Jadzia wyjaśniła publiczności, że walka o niezależność sądów zaczęła się już w średniowieczu (starożytność zostawiła na boku). Chodziło o to, by król czy książę nie miał zbyt wielkiej władzy, jednocześnie wydając prawa, rządząc i sądząc. Wobec takiej potęgi poddany był kompletnie bezradny.

W sukurs przyszedł jej pan Stanisław, naukowiec nieodmawiający, zauważając, że przeciętny obywatel myśli przeważnie, że sądy są od tego przede wszystkim, by zwykłych zjadaczy chleba bronić przed przestępcami. Jednak drugą, równie ważną i podstawową funkcją sądów jest bronienie ludu przed rządem. Źli ludzie kradną, oszukują i terroryzują, a zły rząd nad miarę opodatkowuje, przedstawia niekorzystne dla obywateli zamierzenia jako słuszne i zmusza ich, by myśleli czy postępowali, jak jemu się podoba. W dzisiejszych czasach trwa swoiste przeciąganie liny, w którym chodzi o to, czy rząd będzie nad obywatelami czy odwrotnie. Wymyślono (już dawno temu) demokrację jako sposób na danie obywatelom przewagi nad rządem lub przynajmniej na osiągnięcie jako takiej równowagi.

Obywatele mogą w wyborach pozbawić jeden rząd władzy i dać ją innemu, ale co ich broni przed rządem pomiędzy wyborami? Otóż właśnie sądy, w tym zwłaszcza trybunały konstytucyjne, które mogą unieważniać prawa stanowione przez parlamenty, jeśli są one tylko w interesie rządzących, a sprzeciwiają się wolnościom, jakie chcą mieć obywatele. Te wolności zapisane są zwykle w konstytucjach. A zatem sądy są po to między innymi, by udaremniać zakusy parlamentu i rządu na wolności obywatelskie, a wywiązują się z tego w ten sposób, że starają się zapobiegać łamaniu konstytucji.

– Toteż słuszne jest stwierdzenie, że rząd podporządkowując sobie sądy, kończy z demokracją i twardo bierze obywateli za twarz – zakończył w demagogicznym uniesieniu pan Stanisław.

Pan Władek, działacz sportowy, zwrócił współbiesiadnikom uwagę, iż twarz, za którą pana Stanisława zapragnął ponoć wziąć niemiły mu rząd, sprawia wrażenie wiarołomnej. Dostrzegł w tym niebagatelny argument za poprawnością poglądu całkiem innego, wedle którego rozmamłanie, nazywane niekiedy demokracją, wraz z tyranią sądów nie służą niczemu innemu jak ubezwłasnowolnieniu rządu pragnącego naprawić zło społeczne dla dobra zdrowego jądra narodu polskiego. To rozmamłanie to nic innego jak owy imposybilizm, który w pismach ojców narodu wskazywany jest jako jeden z grzechów głównych. Toteż pan Władek gotów jest w służbę jedności władzy wykonawczej i sądowniczej oddać swoją wierną, a sławną w środowisku sportowym maczetę, która takiego spustoszenia dokonała w szeregach wrogów prawdy i prawości.

Pana Władka poparł stanowczo pan Zenek, dzielnicowy, który był dodatkowo zdania, że bestię imposybilizmu w ten sposób jedynie można poskromić, że się obejmie jednolitym nadzorem dochodzenie, oskarżanie i orzekanie, a także egzekucję lub ewentualną resocjalizację. Co do powierzchowności pana Stanisława wyraził zdanie nieco odrębne, zastępując określenie „twarz wiarołomna” terminem „facjata judaszowa”.

Pani Marta, podpora kółka różańcowego, dobrotliwie smagnęła obu dżentelmenów biczykiem krytyki za pominięcie w ich rozważaniach problematyki właściwego formowania dusz. Gdyby dusza pana Stanisława i jemu podobnych została należycie ukształtowana, po wygłoszeniu nieprzemyślanego poglądu pan Stanisław dzięki rutynowemu rachunkowi sumienia czym prędzej by się zreflektował, dokonał samopotępienia i aktu skruchy, zgromadził dowody swego postępku, spisał akt samooskarżenia, wydał na siebie wyrok i zgłosił się do najbliższego zakładu penitencjarnego. Ubocznym, a dobroczynnym skutkiem takiej postawy pana Stanisława byłyby zminimalizowanie kosztów czynności policyjnych, prokuratorskich i sądowych, a także skrócenie tych czasochłonnych postępowań. Uświadamia to nam, że z całą mocą należy postulować jedność władz ustawodawczej, wykonawczej, sądowej i oświatowej. Dodam, że miłosierdzie kazało pani Marcie określić oblicze pana Stanisława „faryzeuszowskim”.

Pan Józef, kościelny, uściślił, że mówiąc o władzach oświatowych, należy uwzględnić szczególnie referat katechetyczny. Ideę pani Marty radził propagować za pomocą chwytliwego hasła „sojuszu tronu i ołtarza”. Fizjonomię pana Stanisława oddał barwnie słowami „kalafa jaszczurcza”.

Pan Rycho, niegdyś w aparacie, o dziwo przychylił się z grubsza do opinii przedmówców, jednak w miejscu katechezy widział wpajanie światopoglądu naukowego, zaś rachunek sumienia zastąpiłby pryncypialną samokrytyką. Użył ponadto pojęcia „jadaczki burżuazyjnej”

Wielebny Szymon, wikary, także nie dysponował oryginalną teorią na temat pana Stanisława. Na marginesie rozważań nasunęła mu się tylko dygresja, iż sędziowie na miejscu konstytucji mogliby umieścić tablice mojżeszowe. Zwracając się pana Stanisława, nazwał jego cielesne logo „fizys bałamutną”.

Pan Witek, mistrz karaoke a capella, bąknął, iż sędziowie ugięliby się pewnie pod wagą tablic mojżeszowych. Co do meritum nie wypowiedział się jasno, przytłoczony być może dominującą osobowością pana Władka. Westchnął od rzeczy coś o tym, jak dobrze mu się śpiewało na wiecach w minionym tysiącleciu. Czyżby wybierał się na ulice z panem Stanisławem? Gębę miał jakby blagierską.

Oj, pani Misia, redaktorka „Aniołka Parafialnego” dostała buźki filuternej… Co z panem Biniem? Panie Biniu, pan przecież jest beneficjentem ustawy 500+! Czy wypada tak się afiszować z dziobem bałamutnym?

Kto zdoła wejrzeć w meandry psychiki pana Jacusia, znawcy architektury penitencjarnej? Jego stosunek do władzy sądowniczej jest specyficzny. Rok nie wyrok, dwa lata jak dla brata?

Czy środowisko wiecowe może przyciągać pana Gieńka, mistrza sztuki przetrwania?

Co z panem Robertem, ufologiem, panem Jankiem, właścicielem różowych okularów, co z panią Zochą, żoną górnika?

Co z panem Heniem, restauratorem ze specjalizacją w wyszynku? Właśnie wspaniałomyślnie obiecał, iż piętnaste piwo lub ósma setka będzie na koszt lokalu. Debata ożywiła się znowu. Co chce osiągnąć pan Henio. Jego twarz jest pokerowa. Ni to prostolinijne lico, ni to morda zdradziecka.