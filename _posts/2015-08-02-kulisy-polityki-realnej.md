---
id: 408
title: 'Kulisy polityki realnej'
date: '2015-08-02T15:49:05+02:00'
author: 'Adam Boruta'
layout: post
guid: 'http://adamboruta.pl/?p=408'
permalink: /2015/08/kulisy-polityki-realnej/
interface_sidebarlayout:
    - default
categories:
    - Artykuły
    - 'Felietony XXI wieku'
tags:
    - 'CIA w świadomości aparatu'
    - dopalacze
    - konsumpcja
    - 'niewidzialna ręka rynku'
    - 'polityka realna'
    - 'ręka sprawiedliwości'
    - sankcje
    - 'społeczne uwarunkowania konsumpcji'
    - UFO
    - 'uścisk jako ucisk'
    - 'zielone ludziki'
---

[![](https://adamboruta.pl/wp-content/uploads/2014/09/pdf-icon-e1410606068253.png)](https://adamboruta.pl/wp-content/uploads/2015/08/Kulisy-polityki-realnej2.pdf)

KULISY POLITYKI REALNEJ

Wy wszyscy, coście się dopytywali, gdzież to się podziewa pan Zdzicho, wiedzcie, iż odprawiał on wypoczynek wakacyjny w ostępach i głuszy leśnej, w pewnej leśniczówce, bądź też bacówce, a może altance, jeśli nie pod namiotem. Najpewniej w stodółce krewniaka wieśniaka albo też w szopce gościnnej kumpla z wojska. Towarzyszyła mu wiernie małżonka; wiecie przecież, że od pewnego czasu połączeni są oni nierozerwalnym uściskiem.  
\*  
Stali bywalcy lokalu „U Henia” zareagowali na wznowienie projekcji osoby pana Zdzicha. Odbyło się spotkanie na temat kroków, jakie podjąć należy w tej sytuacji. Przewodzili pan dzielnicowy, Zenek i znany działacz sportowy, pan Władek; obu uprawniał do tego autorytet przedstawicieli porządku i liderów lokalnej opinii publicznej. Dzielnicowy, pan Zenek, przypomniał raport ich autorstwa dotyczący aktualnego stanu małżeństwa pana Zdzicha.

Nikt z szacownych członków zgromadzenia nie zakwestionował tez tego raportu, wedle których uścisk pana Zdzicha jest jego własnym uściskiem i on, czyli pan Zdzicho, ze swojej własnej i nieprzymuszonej woli jest tego uścisku inicjatorem, realizatorem i beneficjentem. Nie oprotestowano też głośno interpretacji uścisku jako ucisku i jako środka ograniczenia wolności małżonki.

Pani Jadzia, konsumująca pracownica nauki, odpowiedziała na oczekiwanie większości i wystąpiła w dyskusji jako pierwsza. Zgadzając się pełni z raportem, zwróciła uwagę na nietypowość postawy pana Zdzicha. Otóż do tej pory znany był on jako ideowy konsument, szykanowany od czasu do czasu przez połowicę za swoje przekonania, zaś badania dziejów wyszynku i jego społecznego kontekstu sugerują wyraźnie, że próby ustanowienia nierozerwalnego uścisku małżeńskiego podejmowane są zwykle przez tę część stadła, która nie uległa powabowi konsumpcji szynkarskiej; to strona konsumująca w sposób naturalny dąży do rozluźnienia więzi i – kolokwialnie rzecz ujmując – wyrywa się na wolność. W omawianym przypadku – wedle raportu, którego rzetelności nie sposób kwestionować – stało się jednak inaczej, by nie powiedzieć: odwrotnie. Pani Jadzia jeszcze raz podkreśliła, że raport w pełni akceptuje, jednak wykazana w nim nietypowość skłania ją do profesjonalnej ostrożności i dlatego postuluje pogłębione badania pana Zdzicha, których za standardową opłatą gotowa jest dokonać. Do czasu ukończenia prac badawczych warto byłoby może wstrzymać decyzje w sprawie pana Zdzicha – dodała na koniec.

Pan Władek, lider kibiców sportowych, pochwalił panią Jadzię za profesjonalizm i przedsiębiorczość. Przyznał, że ze względów ściśle merytorycznych należałoby zapewne wdrożyć jej postulaty w życie, lecz to szacowne zgromadzenie ma w obowiązku uwzględniać też aspekt humanitarny, a ten nie pozwala na zwłokę w potępieniu pana Zdzicha i obłożeniu go sankcjami. Nie przeszkadza to w żadnej mierze temu, by pani Jadzia podjęła deklarowane przez nią pogłębione badania; ich efekty niewątpliwie ubogacą wiedzę i na pewno warte będą butelki koniaku, którą on, pan Władek, chętnie ufunduje.

Pani Jadzia ujęta humanitaryzmem pana Władka wycofała propozycję odroczenia decyzji w sprawie pana Zdzicha.

Pan Jacuś, ekspert w dziedzinie penitencjarnej, również wygłosił mowę pochwalną raportu panów Zenka i Władka. Z jego rozwlekłych wywodów wynikało w skrócie, iż rzeczony raport zadziwia go swoją kompetencją i generalnie jest do zaakceptowania z tymi tylko poprawkami, że to nie pan Zdzicho lecz małżonka pana Zdzicha jawi się w jego świetle jako inicjatorka, realizatorka i beneficjentka uścisku – bardzo słusznie zresztą przez autorów zinterpretowanego jako ucisk i metoda zniewolenia. – Jeśli nawet zaciśnięta ręka jest pana Zdzicha – zakonkludował pan Jacuś – to ani chybi małżonka zadała mu ziela, by palców rozewrzeć nie potrafił i takim to sposobem na zawsze go przy sobie zatrzyma. (Chyba by jad puścił – wtrącił się pan Janek znany z tego, że na świat spogląda przez różowe okulary).

Pan Władek i tym razem był pełen uznania dla dyskutanta za wprowadzenie pod obrady nowego, odświeżającego punktu widzenia. Zaproponował, by mimo to przejść nad nim do porządku dziennego, bo badania toksykologiczne krwi pana Zdzicha nie wykazały obecności żadnych podejrzanych substancji. Obiecał, że stosowny dokument zostanie niebawem dołączony do raportu.

Pan Jacuś ustąpił ze swoich pozycji, kiedy czystość intencji pana Władka dobitnie dała o sobie znać w postaci obietnicy, iż pan Władek wpłynie na pana Heńka, kibola, by ten darował panu Jacusiowi obiecane bęcki.

Różowe okulary, jak się okazało, współgrają u pana Janka z pozytywnym nastawieniem do bliźnich. – Może wina nie jest ani pana Zdzicha ani małżonki – zadumał się półgłosem – może ta zaciśnięta ręka to jest osławiona niewidzialna ręka rynku, o której ciągle mówią, że niewoli wszystkich. – Uwagę pana Witka, mistrza karaoke, że rękę na małżonce pana Zdzicha doskonale widać, odparł pan Janek argumentem, że widoczna nie jest ręka rynku sama w sobie, lecz jej fenomen czyli zjawisko, które wygląda jak kończyna pana Zdzicha, choć w istocie to ręka rynku ukrywa się pod tym kamuflażem.

Ugodowe nastawienie pana Janka tak wzruszyło pana Zenka, dzielnicowego, że ten obiecał mu darowanie mandatu za niewłaściwe parkowanie. Pan Janek odwdzięczył się za sympatię poparciem wszystkich propozycji autorów raportu.

Hipoteza niewidzialnej ręki podszywającej się pod kończynę pana Zdzicha spotkała się z powszechnym zainteresowaniem, by nie rzec – z aprobatą. Tylko pani Hela, która wpadła do lokalu na chwileczkę, by łyknąć naparstek wiśnióweczki, zaapelowała, by nie roztrząsać tego tematu, gdyż ręka może należeć do Opatrzności pilnującej trwałości więzi małżeńskiej i nie wypada Jej tykać nadaremno.

Apel pani Heli wybrzmiał bez echa, ponieważ bywalcom lokalu „U Henia” trudno było sobie wyobrazić, by zainteresowanie osobą pana Zdzicha i przyległościami mogło się udzielić czynnikom rzędów wyższych niż powiatowe. Tym niemniej pani Hela zyskała solenną obietnicę, iż pan Henio, restaurator, każdej niedzieli przed pierwszą kolejką przeżegna salę.

Pan Witek, artysta śpiewak, jął się zastanawiać, czy w interesującym wszystkich przypadku nie interweniuje czasem ręka sprawiedliwości. W odpowiedzi na pytanie, jakiej sprawiedliwości pilnuje ręka udająca prawicę pana Zdzicha, zasłonił się dyskrecją. Skomentował to pan Rycho, niegdyś w aparacie, konstatując ponuro, że za uszami to każdy coś ma, również pan Zdzicho i jego małżonka. – Raczej małżonka – zastanawiał się pan Henio, restaurator – bo to ją capnęła ręka.

Odciął się od tego domysłu, kiedy pan Zenek, dzielnicowy, zwrócił mu uwagę, że jest on równie pochopny jak dochodzące do komisariatu anonimowe skargi na oferowanie w lokalu „U Henia” towaru bez akcyzy.

– CIA długie ma ręce – przestrzegł obecnych pan Rycho, były aparatczyk. – Skrępowanie małżonków może być metodą szantażu, który ma ich skłonić do współpracy. – Jakiej współpracy – spytał naiwnie pan Janek, przecierając różowe okulary. – W szkodzeniu siłom postępu i demokracji ludowej – wyjaśnił pan Rycho.

Pan Władek, działacz sportowy i pan Zenek, dzielnicowy wykluczyli taką możliwość, zapewniając solennie, że wytępili w okolicy zarówno demokrację oddolną jak i postęp. Zadeklarowali jednak, że zachowają stosowną czujność. Mruczenia pana Rycha, że błędem jest utożsamianie demokracji ludowej z oddolną, nikt nie dosłyszał.

Mruczenie pana Rycha zagłuszył podekscytowany pan Robert, wielbiciel gier komputerowych, do niedawna niepełnoletni; dostrzegł on w omawianej ręce instrument zielonych ludzików, którzy ukryci bezpiecznie w latających talerzach eksperymentują beztrosko na ludzkości.

Zamilkł pod wpływem rozważań pana Zenka o kampanii przeciw dopalaczom.

Pani Hela zmieniła zdanie i zaczęła rozpatrywać możliwość zaczadzenia pana Zdzicha lub jego małżonki ideologią dżenderów albo też satanistów, masonów czy podstępnych syjonistów, a może wódstoków.

Pan Władek, kibic i pan Zenek, dzielnicowy, uznali możliwość zaczadzenia pana Zdzicha za interesującą.

Rozmowy odniosły sukces. Uznano, że sankcje na pana Zdzicha zostaną nałożone. W celu ustalenia ich rodzaju i zasięgu zwołano następne posiedzenie na wtorek.